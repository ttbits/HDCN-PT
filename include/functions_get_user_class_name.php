<?php

function get_user_class_name($class, $compact = false, $b_colored = false, $I18N = false) {
	static $en_lang_functions;
	static $current_user_lang_functions;
	if (!$en_lang_functions) {
		require(get_langfile_path("functions.php", false, "en"));
		$en_lang_functions = $lang_functions;
	}

	if (!$I18N) {
		$this_lang_functions = $en_lang_functions;
	} else {
		if (!$current_user_lang_functions) {
			require(get_langfile_path("functions.php"));
			$current_user_lang_functions = $lang_functions;
		}
		$this_lang_functions = $current_user_lang_functions;
	}

	$class_name = "";
	switch ($class) {
		case UC_PEASANT: {
				$class_name = $this_lang_functions['text_peasant'];
				break;
			}
		case UC_USER: {
				$class_name = $this_lang_functions['text_user'];
				break;
			}
		case UC_POWER_USER: {
				$class_name = $this_lang_functions['text_power_user'];
				break;
			}
		case UC_ELITE_USER: {
				$class_name = $this_lang_functions['text_elite_user'];
				break;
			}
		case UC_CRAZY_USER: {
				$class_name = $this_lang_functions['text_crazy_user'];
				break;
			}
		case UC_INSANE_USER: {
				$class_name = $this_lang_functions['text_insane_user'];
				break;
			}
		case UC_VETERAN_USER: {
				$class_name = $this_lang_functions['text_veteran_user'];
				break;
			}
		case UC_EXTREME_USER: {
				$class_name = $this_lang_functions['text_extreme_user'];
				break;
			}
		case UC_ULTIMATE_USER: {
				$class_name = $this_lang_functions['text_ultimate_user'];
				break;
			}
		case UC_NEXUS_MASTER: {
				$class_name = $this_lang_functions['text_nexus_master'];
				break;
			}
		case UC_VIP: {
				$class_name = $this_lang_functions['text_vip'];
				break;
			}
		case UC_DOWNLOADER: {
				$class_name = $this_lang_functions['text_downloader'];
				break;
			}
		case UC_FORWARD: {
				$class_name = $this_lang_functions['text_forward'];
				break;
			}
		case UC_UPLOADER: {
				$class_name = $this_lang_functions['text_uploader'];
				break;
			}
		case UC_RETIREE: {
				$class_name = $this_lang_functions['text_retiree'];
				break;
			}
		case UC_MODERATOR: {
				$class_name = $this_lang_functions['text_moderators'];
				break;
			}
		case UC_ADMINISTRATOR: {
				$class_name = $this_lang_functions['text_administrators'];
				break;
			}
		case UC_SYSOP: {
				$class_name = $this_lang_functions['text_sysops'];
				break;
			}
		case UC_STAFFLEADER: {
				$class_name = $this_lang_functions['text_staff_leader'];
				break;
			}
	}

	switch ($class) {
		case UC_PEASANT: {
				$class_name_color = $en_lang_functions['text_peasant'];
				break;
			}
		case UC_USER: {
				$class_name_color = $en_lang_functions['text_user'];
				break;
			}
		case UC_POWER_USER: {
				$class_name_color = $en_lang_functions['text_power_user'];
				break;
			}
		case UC_ELITE_USER: {
				$class_name_color = $en_lang_functions['text_elite_user'];
				break;
			}
		case UC_CRAZY_USER: {
				$class_name_color = $en_lang_functions['text_crazy_user'];
				break;
			}
		case UC_INSANE_USER: {
				$class_name_color = $en_lang_functions['text_insane_user'];
				break;
			}
		case UC_VETERAN_USER: {
				$class_name_color = $en_lang_functions['text_veteran_user'];
				break;
			}
		case UC_EXTREME_USER: {
				$class_name_color = $en_lang_functions['text_extreme_user'];
				break;
			}
		case UC_ULTIMATE_USER: {
				$class_name_color = $en_lang_functions['text_ultimate_user'];
				break;
			}
		case UC_NEXUS_MASTER: {
				$class_name_color = $en_lang_functions['text_nexus_master'];
				break;
			}
		case UC_VIP: {
				$class_name_color = $en_lang_functions['text_vip'];
				break;
			}
		case UC_DOWNLOADER: {
				$class_name_color = $en_lang_functions['text_downloader'];
				break;
			}
		case UC_FORWARD: {
				$class_name_color = $en_lang_functions['text_forward'];
				break;
			}
		case UC_UPLOADER: {
				$class_name_color = $en_lang_functions['text_uploader'];
				break;
			}
		case UC_RETIREE: {
				$class_name_color = $en_lang_functions['text_retiree'];
				break;
			}
		case UC_MODERATOR: {
				$class_name_color = $en_lang_functions['text_moderators'];
				break;
			}
		case UC_ADMINISTRATOR: {
				$class_name_color = $en_lang_functions['text_administrators'];
				break;
			}
		case UC_SYSOP: {
				$class_name_color = $en_lang_functions['text_sysops'];
				break;
			}
		case UC_STAFFLEADER: {
				$class_name_color = $en_lang_functions['text_staff_leader'];
				break;
			}
	}

	$class_name = ( $compact == true ? str_replace(" ", "", $class_name) : $class_name);
	if ($class_name)
		return ($b_colored == true ? "<b class='" . str_replace(" ", "", $class_name_color) . "_Name'>" . $class_name . "</b>" : $class_name);
}

function get_user_class_name_zh($class, $compact = false, $b_colored = false, $I18N = false) {
	static $zh_lang_functions, $en_lang_functions;
	static $current_user_lang_functions;
	if (!$zh_lang_functions) {
		require(get_langfile_path("functions.php", false, "chs"));
		$zh_lang_functions = $lang_functions;
	}
	if (!$en_lang_functions) {
		require(get_langfile_path("functions.php", false, "en"));
		$en_lang_functions = $lang_functions;
	}

	if (!$I18N) {
		$this_lang_functions = $zh_lang_functions;
	} else {
		if (!$current_user_lang_functions) {
			require(get_langfile_path("functions.php"));
			$current_user_lang_functions = $lang_functions;
		}
		$this_lang_functions = $current_user_lang_functions;
	}

	$class_name = "";
	switch ($class) {
		case UC_PEASANT: {
				$class_name = $this_lang_functions['text_peasant'];
				break;
			}
		case UC_USER: {
				$class_name = $this_lang_functions['text_user'];
				break;
			}
		case UC_POWER_USER: {
				$class_name = $this_lang_functions['text_power_user'];
				break;
			}
		case UC_ELITE_USER: {
				$class_name = $this_lang_functions['text_elite_user'];
				break;
			}
		case UC_CRAZY_USER: {
				$class_name = $this_lang_functions['text_crazy_user'];
				break;
			}
		case UC_INSANE_USER: {
				$class_name = $this_lang_functions['text_insane_user'];
				break;
			}
		case UC_VETERAN_USER: {
				$class_name = $this_lang_functions['text_veteran_user'];
				break;
			}
		case UC_EXTREME_USER: {
				$class_name = $this_lang_functions['text_extreme_user'];
				break;
			}
		case UC_ULTIMATE_USER: {
				$class_name = $this_lang_functions['text_ultimate_user'];
				break;
			}
		case UC_NEXUS_MASTER: {
				$class_name = $this_lang_functions['text_nexus_master'];
				break;
			}
		case UC_VIP: {
				$class_name = $this_lang_functions['text_vip'];
				break;
			}
		case UC_DOWNLOADER: {
				$class_name = $this_lang_functions['text_downloader'];
				break;
			}
		case UC_FORWARD: {
				$class_name = $this_lang_functions['text_forward'];
				break;
			}
		case UC_UPLOADER: {
				$class_name = $this_lang_functions['text_uploader'];
				break;
			}
		case UC_RETIREE: {
				$class_name = $this_lang_functions['text_retiree'];
				break;
			}
		case UC_MODERATOR: {
				$class_name = $this_lang_functions['text_moderators'];
				break;
			}
		case UC_ADMINISTRATOR: {
				$class_name = $this_lang_functions['text_administrators'];
				break;
			}
		case UC_SYSOP: {
				$class_name = $this_lang_functions['text_sysops'];
				break;
			}
		case UC_STAFFLEADER: {
				$class_name = $this_lang_functions['text_staff_leader'];
				break;
			}
	}

	switch ($class) {
		case UC_PEASANT: {
				$class_name_color = $en_lang_functions['text_peasant'];
				break;
			}
		case UC_USER: {
				$class_name_color = $en_lang_functions['text_user'];
				break;
			}
		case UC_POWER_USER: {
				$class_name_color = $en_lang_functions['text_power_user'];
				break;
			}
		case UC_ELITE_USER: {
				$class_name_color = $en_lang_functions['text_elite_user'];
				break;
			}
		case UC_CRAZY_USER: {
				$class_name_color = $en_lang_functions['text_crazy_user'];
				break;
			}
		case UC_INSANE_USER: {
				$class_name_color = $en_lang_functions['text_insane_user'];
				break;
			}
		case UC_VETERAN_USER: {
				$class_name_color = $en_lang_functions['text_veteran_user'];
				break;
			}
		case UC_EXTREME_USER: {
				$class_name_color = $en_lang_functions['text_extreme_user'];
				break;
			}
		case UC_ULTIMATE_USER: {
				$class_name_color = $en_lang_functions['text_ultimate_user'];
				break;
			}
		case UC_NEXUS_MASTER: {
				$class_name_color = $en_lang_functions['text_nexus_master'];
				break;
			}
		case UC_VIP: {
				$class_name_color = $en_lang_functions['text_vip'];
				break;
			}
		case UC_DOWNLOADER: {
				$class_name_color = $en_lang_functions['text_downloader'];
				break;
			}
		case UC_FORWARD: {
				$class_name_color = $en_lang_functions['text_forward'];
				break;
			}
		case UC_UPLOADER: {
				$class_name_color = $en_lang_functions['text_uploader'];
				break;
			}
		case UC_RETIREE: {
				$class_name_color = $en_lang_functions['text_retiree'];
				break;
			}
		case UC_MODERATOR: {
				$class_name_color = $en_lang_functions['text_moderators'];
				break;
			}
		case UC_ADMINISTRATOR: {
				$class_name_color = $en_lang_functions['text_administrators'];
				break;
			}
		case UC_SYSOP: {
				$class_name_color = $en_lang_functions['text_sysops'];
				break;
			}
		case UC_STAFFLEADER: {
				$class_name_color = $en_lang_functions['text_staff_leader'];
				break;
			}
	}

	$class_name = ( $compact == true ? str_replace(" ", "", $class_name) : $class_name);
	if ($class_name)
		return ($b_colored == true ? "<b class='" . str_replace(" ", "", $class_name_color) . "_Name'>" . $class_name . "</b>" : $class_name);
}
