<?php

if (!in_array($_SERVER['HTTP_HOST'], array('2.com', 'shenzhan.in', 'ahdbits.com', 'wocnm.com'))) {//不在array()里的都阻止
	header("Content-type:text/html;charset=utf-8");
	die("<div style='text-align:center; margin-top:150px; color:red; '>网址{$_SERVER['HTTP_HOST']}未经授权，请联系技术支持：mojie126@foxmail.com</div>");
}

if (!defined('IN_TRACKER'))
	die('Hacking attempt!');

function get_global_sp_state() {
	global $Cache;
	static $global_promotion_state;
	if (!$global_promotion_state) {
		if (!$global_promotion_state = $Cache->get_value('global_promotion_state')) {
			$res = mysql_query("SELECT * FROM torrents_state");
			$row = mysql_fetch_assoc($res);
			$global_promotion_state = $row["global_sp_state"];
			$Cache->cache_value('global_promotion_state', $global_promotion_state, 43200);
		}
	}
	return $global_promotion_state;
}

// IP Validation
function validip($ip) {
	if (!ip2long($ip)) //IPv6
		return true;
	if (!empty($ip) && $ip == long2ip(ip2long($ip))) {
		// reserved IANA IPv4 addresses
		// http://www.iana.org/assignments/ipv4-address-space
		$reserved_ips = array(
			array('192.0.2.0', '192.0.2.255'),
			array('192.168.0.0', '192.168.255.255'),
			array('255.255.255.0', '255.255.255.255')
		);

		foreach ($reserved_ips as $r) {
			$min = ip2long($r[0]);
			$max = ip2long($r[1]);
			if ((ip2long($ip) >= $min) && (ip2long($ip) <= $max))
				return false;
		}
		return true;
	} else
		return false;
}

function getip() {
	if (isset($_SERVER)) {
		if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && validip($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} elseif (isset($_SERVER['HTTP_CLIENT_IP']) && validip($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
	} else {
		if (getenv('HTTP_X_FORWARDED_FOR') && validip(getenv('HTTP_X_FORWARDED_FOR'))) {
			$ip = getenv('HTTP_X_FORWARDED_FOR');
		} elseif (getenv('HTTP_CLIENT_IP') && validip(getenv('HTTP_CLIENT_IP'))) {
			$ip = getenv('HTTP_CLIENT_IP');
		} else {
			$ip = getenv('REMOTE_ADDR');
		}
	}
	return $ip;
}

function sql_query($query) {
	global $query_name;
	$query_name[] = $query;
	return mysql_query($query);
}

function sqlesc($value) {
	// Stripslashes
	//$value = RemoveXSS($value);
	if (get_magic_quotes_gpc()) {
		$value = stripslashes($value);
	}
	// Quote if not a number or a numeric string
	if (!is_numeric($value)) {
		$value = "'" . mysql_real_escape_string($value) . "'";
	}
	return $value;
}

function TestIMG($value) {
	preg_match_all('/\[img=(.*?)\]/', $value, $temp1);
	foreach ($temp1[1] as $temp11) {
		preg_match_all('/(.*?).php/', $temp11, $temp2);
		foreach ($temp2[1] as $temp22) {
			if (strlen($temp22) > 0 && substr(strrev($temp22), 0, 5) != "rabym") {//mybar
				if (!function_exists('msgalert')) {
					stdhead();
				}
				stdmsg("入侵检测", "警告！请检查提交内容里是否存在SQL语句或带有参数链接等");
				stdfoot();
				exit;
			}
		}
	}
	preg_match_all('/\[img\](.*?)\[\/img\]/', $value, $temp1);
	foreach ($temp1[1] as $temp11) {
		preg_match_all('/(.*?).php/', $temp11, $temp2);
		foreach ($temp2[1] as $temp22) {
			if (strlen($temp22) > 0 && substr(strrev($temp22), 0, 5) != "rabym") {//mybar
				if (!function_exists('msgalert')) {
					stdhead();
				}
				stdmsg("入侵检测", "警告！请检查提交内容里是否存在SQL语句或带有参数链接等");
				stdfoot();
				exit;
			}
		}
	}
}

function RemoveXSS($val) {
	//$val = preg_replace('/([\x00-\x08,\x0b-\x0c,\x0e-\x19])/', '', $val); //貌似有问题
	$val = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]' .
			'|[\x00-\x7F][\x80-\xBF]+' .
			'|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*' .
			'|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})' .
			'|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S', '', $val);
	$val = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]' .
			'|\xED[\xA0-\xBF][\x80-\xBF]/S', '', $val);
	$search = 'abcdefghijklmnopqrstuvwxyz';
	$search .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$search .= '1234567890!@#$%^&*()';
	$search .= '~`";:?+/={}[]-_|\'\\';
	for ($i = 0; $i < strlen($search); $i++) {
		$val = preg_replace('/(&#[xX]0{0,8}' . dechex(ord($search[$i])) . ';?)/i', $search[$i], $val);
		$val = preg_replace('/(&#0{0,8}' . ord($search[$i]) . ';?)/', $search[$i], $val);
	}
	$ra1 = Array('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'blink', 'link', 'style', 'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 'layer', 'bgsound', 'title', 'base');
	$ra2 = Array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');
	$ra = array_merge($ra1, $ra2);
	$found = true;
	while ($found == true) {
		$val_before = $val;
		for ($i = 0; $i < sizeof($ra); $i++) {
			$pattern = '/';
			for ($j = 0; $j < strlen($ra[$i]); $j++) {
				if ($j > 0) {
					$pattern .= '(';
					$pattern .= '(&#[xX]0{0,8}([9ab]);)';
					$pattern .= '|';
					$pattern .= '|(&#0{0,8}([9|10|13]);)';
					$pattern .= ')*';
				}
				$pattern .= $ra[$i][$j];
			}
			$pattern .= '/i';
			//$replacement = substr($ra[$i], 0, 2) . '<x>' . substr($ra[$i], 2);
			$replacement = substr($ra[$i], 0, 2) . substr($ra[$i], 2);
			$val = preg_replace($pattern, $replacement, $val);
			if ($val_before == $val) {
				$found = false;
			}
		}
	}
	return $val;
}

function hash_pad($hash) {
	return str_pad($hash, 20);
}

function hash_where($name, $hash) {
	$shhash = preg_replace('/ *$/s', "", $hash);
	return "($name = " . sqlesc($hash) . " OR $name = " . sqlesc($shhash) . ")";
}
