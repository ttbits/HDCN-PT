<?php

function torrenttable($res, $variant = "torrent", $mode = "normal") {
	global $Cache, $lang_functions;
	global $CURUSER, $waitsystem, $browsecatmode, $specialcatmode;
	global $torrentmanage_class, $smalldescription_main, $enabletooltip_tweak, $showextinfo, $ratioless;

	$last_browse_res = mysql_fetch_array(sql_query("SELECT last_browse, last_music, limit_last_browse FROM users WHERE id = " . $CURUSER['id']));

	if ($variant == "torrent") {
		$last_browse = $last_browse_res['last_browse'];
		$sectiontype = $browsecatmode;
	} elseif ($variant == "music") {
		$last_browse = $last_browse_res['last_music'];
		$sectiontype = $specialcatmode;
	} else {
		$last_browse = $last_browse_res['last_browse'];
		$sectiontype = "";
	}

	$time_now = TIMENOW;
	if ($last_browse > $time_now) {
		$last_browse = $time_now;
	}

	if ($variant == "torrent") {
		$limit_last_browse = $last_browse_res['limit_last_browse'];
		$sectiontype = $browsecatmode;
	} elseif ($variant == "music") {
		$limit_last_browse = $last_browse_res['last_music'];
		$sectiontype = $specialcatmode;
	} else {
		$limit_last_browse = $last_browse_res['limit_last_browse'];
		$sectiontype = "";
	}

	if ($limit_last_browse > $time_now) {
		$limit_last_browse = $time_now;
	}

	if (get_user_class() < UC_VIP && $waitsystem == "yes") {
		$ratio = get_ratio($CURUSER["id"], false);
		$gigs = $CURUSER["downloaded"] / (1024 * 1024 * 1024);
		if ($gigs > 10) {
			if ($ratio < 0.2)
				$wait = 24;
			elseif ($ratio < 0.3)
				$wait = 12;
			elseif ($ratio < 0.4)
				$wait = 6;
			elseif ($ratio < 0.5)
				$wait = 3;
			else
				$wait = 0;
		} else
			$wait = 0;
	}
	?>
	<table class="torrents" cellspacing="0" cellpadding="5" width="100%">
		<tr>
			<?php
			$count_get = 0;
			$oldlink = "";
			foreach ($_GET as $get_name => $get_value) {
				$get_name = mysql_real_escape_string(strip_tags(str_replace(array("\"", "'"), array("", ""), $get_name)));
				$get_value = mysql_real_escape_string(strip_tags(str_replace(array("\"", "'"), array("", ""), $get_value)));

				if ($get_name != "sort" && $get_name != "type") {
					if ($count_get > 0) {
						$oldlink .= "&amp;" . $get_name . "=" . $get_value;
					} else {
						$oldlink .= $get_name . "=" . $get_value;
					}
					$count_get++;
				}
			}
			if ($count_get > 0) {
				$oldlink = $oldlink . "&amp;";
			}
			$sort = $_GET['sort'];
			$link = array();
			for ($i = 1; $i <= 9; $i++) {
				if ($sort == $i)
					$link[$i] = ($_GET['type'] == "desc" ? "asc" : "desc");
				else
					$link[$i] = ($i == 1 ? "asc" : "desc");
			}
			?>
			<td class="colhead" style="padding: 0px"><?php echo $lang_functions['col_type'] ?></td>
			<td class="colhead"><a href="?<?php echo $oldlink ?>sort=1&amp;type=<?php echo $link[1] ?>"><?php echo $lang_functions['col_name'] ?></a></td>
			<?php
			if ($wait) {
				print("<td class=\"colhead\">" . $lang_functions['col_wait'] . "</td>");
			}
			if ($CURUSER['showcomnum'] != 'no') {
				?>
				<td class="colhead"><?php echo $lang_functions['title_number_of_files'] ?></td>
				<td class="colhead"><a href="?<?php echo $oldlink ?>sort=3&amp;type=<?php echo $link[3] ?>"><img class="comments" src="pic/trans.gif" alt="comments" title="<?php echo $lang_functions['title_number_of_comments'] ?>" /></a></td>
			<?php } ?>

			<td class="colhead"><a href="?<?php echo $oldlink ?>sort=4&amp;type=<?php echo $link[4] ?>"><img class="time" src="pic/trans.gif" alt="time" title="<?php echo ($CURUSER['timetype'] != 'timealive' ? $lang_functions['title_time_added'] : $lang_functions['title_time_alive']) ?>" /></a></td>
			<td class="colhead"><a href="?<?php echo $oldlink ?>sort=5&amp;type=<?php echo $link[5] ?>"><img class="size" src="pic/trans.gif" alt="size" title="<?php echo $lang_functions['title_size'] ?>" /></a></td>
			<td class="colhead"><a href="?<?php echo $oldlink ?>sort=7&amp;type=<?php echo $link[7] ?>"><img class="seeders" src="pic/trans.gif" alt="seeders" title="<?php echo $lang_functions['title_number_of_seeders'] ?>" /></a></td>
			<td class="colhead"><a href="?<?php echo $oldlink ?>sort=8&amp;type=<?php echo $link[8] ?>"><img class="leechers" src="pic/trans.gif" alt="leechers" title="<?php echo $lang_functions['title_number_of_leechers'] ?>" /></a></td>
			<td class="colhead"><a href="?<?php echo $oldlink ?>sort=6&amp;type=<?php echo $link[6] ?>"><img class="snatched" src="pic/trans.gif" alt="snatched" title="<?php echo $lang_functions['title_number_of_snatched'] ?>" /></a></td>
			<td class="colhead"><a href="?<?php echo $oldlink ?>sort=9&amp;type=<?php echo $link[9] ?>"><?php echo $lang_functions['col_uploader'] ?></a></td>
			<?php
			if (get_user_class() >= $torrentmanage_class) {
				echo '<td class="colhead"></td>';
			}

			if (get_user_class() >= $torrentmanage_class) {
				?>
				<td class="colhead"><?php echo $lang_functions['col_action'] ?></td>
			<?php } ?>
		</tr>
		<?php
		$caticonrow = get_category_icon_row($CURUSER['caticon']);
		if ($caticonrow['secondicon'] == 'yes')
			$has_secondicon = true;
		else
			$has_secondicon = false;
		$counter = 0;
		if ($smalldescription_main == 'no' || $CURUSER['showsmalldescr'] == 'no')
			$displaysmalldescr = false;
		else
			$displaysmalldescr = true;
		while ($row = mysql_fetch_assoc($res)) {
			$id = $row["id"];
			$userid = $CURUSER['id'];
			$a = get_row_count("snatched", "WHERE userid = " . $userid . " AND torrentid = " . $id);
			$truckmarks = get_row_count("truckmarks", "WHERE userid = $userid AND torrentid = $id");
			$bookmarks = get_row_count("bookmarks", "WHERE userid = $userid AND torrentid = $id");
			if ($a > 0) {
				$b = get_row_count("peers", "WHERE userid = " . $userid . " AND torrent = " . $id . " AND seeder = 'yes'");
				$deltime = date("Y-m-d H:i:s", time() - 3600);
				$res11 = mysql_fetch_array(sql_query("SELECT uploaded, downloaded, finished, last_action FROM snatched WHERE userid = $userid AND torrentid = $id"));
				$res12 = mysql_fetch_array(sql_query("SELECT size FROM torrents WHERE id = " . $id));
				if ($truckmarks > 0) {
					print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table truckmarks\">");
				} elseif ($bookmarks > 0) {
					print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table bookmarks\">");
				} elseif ($b > 0) {
					print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table seeders\">"); //在做种的
				} elseif ($res11['downloaded'] >= $res12['size'] || $res11['finished'] == 'yes') {
					print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table finished\">"); //已下载完
				} elseif ($res11['downloaded'] < $res12['size'] && $res11['last_action'] >= $deltime) {
					print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table leechers\">"); //未下载完
				} elseif ($row['pos_state'] != 'normal') {
					print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table sticky\">");
				} elseif ($row['marrow'] != 'normal') {
					print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table sticky\">");
				} else {
					print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table\">");
				}
			} else {
				if ($truckmarks > 0) {
					print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table truckmarks\">");
				} elseif ($bookmarks > 0) {
					print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table bookmarks\">");
				} elseif ($row['pos_state'] != 'normal') {
					print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table sticky\">");
				} elseif ($row['marrow'] != 'normal') {
					print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table sticky\">");
				} else {
					print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table\">");
				}
			}

			//强制分享
			global $share, $sharesize, $sharenum, $sharemanage_class, $downhref;
			$ownercheck = mysql_fetch_assoc(sql_query("SELECT owner FROM torrents WHERE id = " . sqlesc($id))) or sqlerr(__FILE__, __LINE__);
			if ($share == "yes") {
				if ($CURUSER["id"] != $ownercheck['owner']) {
					$shareck = sql_query("SELECT size, status FROM torrents WHERE owner = " . $CURUSER['id']) or sqlerr(__FILE__, __LINE__);
					$userdonor = mysql_fetch_array(sql_query("SELECT donor FROM users WHERE id = " . $CURUSER['id'])) or sqlerr(__FILE__, __LINE__);
					if (get_user_class() >= $sharemanage_class || $userdonor['donor'] == 'yes') {
						$downhref = "download.php?id=" . $id . "&passkey=" . $CURUSER['passkey'];
					} else {
						$normalcount = 0;
						while ($sharecheck = mysql_fetch_assoc($shareck)) {
							if ($sharecheck['status'] == 'normal') {
								$normalcount++;
								$sharecheck_sizesum += $sharecheck['size'];
							}
						}
						if ($normalcount >= $sharenum && $sharecheck_sizesum >= $sharesize) {
							$downhref = "download.php?id=" . $id . "&passkey=" . $CURUSER['passkey'];
						} else {
							$downhref = "javascript:alert('你必须先成功分享 " . $sharenum . " 个大于" . mksize($sharesize) . "或多个种子总大小大于 " . mksize($sharesize) . " 的资源后才可以下载其它种子资源。');";
						}
					}
				} else {
					$downhref = "download.php?id=" . $id . "&passkey=" . $CURUSER['passkey'];
				}
			} else {
				$downhref = "download.php?id=" . $id . "&passkey=" . $CURUSER['passkey'];
			}

			print("<td id=\"torrents_td\" class=\"rowfollow nowrap\" valign=\"middle\" style='padding: 0px'>");
			if (isset($row["category"])) {
				if ($row["category"] < 424) {
					$link = "torrents.php?";
				} else {
					$link = "limit.php?";
				}
				print(return_category_image($row["category"], $link));
				print(return_sec_category_image($row['category'], $row["source"], $link)); //二级分类
				if ($has_secondicon) {
					print(get_second_icon($row, "pic/" . $catimgurl . "additional/"));
				}
			} else
				print("-");
			print("</td>");

			//torrent name
			$dispname = trim($row["name"]);
			$short_torrent_name_alt = "";
			$mouseovertorrent = "";
			$tooltipblock = "";
			$has_tooltip = false;
			if ($enabletooltip_tweak == 'yes')
				$tooltiptype = $CURUSER['tooltip'];
			else
				$tooltiptype = 'off';
			switch ($tooltiptype) {
				case 'minorimdb' : {
						if ($showextinfo['imdb'] == 'yes' && $row["url"]) {
							$url = $row['url'];
							$cache = $row['cache_stamp'];
							$type = 'minor';
							$has_tooltip = true;
						}
						break;
					}
				case 'medianimdb' : {
						if ($showextinfo['imdb'] == 'yes' && $row["url"]) {
							$url = $row['url'];
							$cache = $row['cache_stamp'];
							$type = 'median';
							$has_tooltip = true;
						}
						break;
					}
				case 'off' : break;
			}
			if (!$has_tooltip)
				$short_torrent_name_alt = "title=\"" . htmlspecialchars($dispname) . "\"";
			else {
				$torrent_tooltip[$counter]['id'] = "torrent_" . $counter;
				$torrent_tooltip[$counter]['content'] = "";
				$mouseovertorrent = "onmouseover=\"get_ext_info_ajax('" . $torrent_tooltip[$counter]['id'] . "','" . $url . "','" . $cache . "','" . $type . "'); domTT_activate(this, event, 'content', document.getElementById('" . $torrent_tooltip[$counter]['id'] . "'), 'trail', false, 'delay',600,'lifetime',6000,'fade','both','styleClass','nicetitle','fadeMax', 98, 'maxWidth', 500);\"";
			}
			$count_dispname = mb_strlen($dispname, "UTF-8");
			if (!$displaysmalldescr || $row["small_descr"] == "")// maximum length of torrent name
				$max_length_of_torrent_name = 85;
			elseif ($CURUSER['fontsize'] == 'large')
				$max_length_of_torrent_name = 70;
			elseif ($CURUSER['fontsize'] == 'small')
				$max_length_of_torrent_name = 100;
			else
				$max_length_of_torrent_name = 85;

			if ($count_dispname > $max_length_of_torrent_name)
				$dispname = mb_substr($dispname, 0, $max_length_of_torrent_name - 2, "UTF-8") . "...";

			if ($row['endsticky'] >= $row['endmarrow'] && $row['pos_state'] == 'sticky') {//如果置顶结束时间>=超级置顶结束时间，并且为置顶时，则显示置顶结束时间，否则显示超级置顶结束时间
				if ($row['pos_state'] == 'sticky' && $CURUSER['appendsticky'] == 'yes') {
					$stickyicon = "<img class=\"sticky\" src=\"pic/trans.gif\" alt=\"置顶\" title=\"" . $lang_functions['title_sticky'] . "至" . $row['endsticky'] . "\" />&nbsp;";
				} else {
					$stickyicon = "";
				}
			} else {
				//超级置顶
				if ($row['marrow'] == 'marrow1' || $row['marrow'] == 'marrow2' || $row['marrow'] == 'marrow3' && $CURUSER['appendsticky'] == 'yes') {
					$stickyicon = "<img class=\"sticky\" src=\"pic/trans.gif\" alt=\"置顶\" title=\"" . $lang_functions['title_sticky'] . "至" . $row['endmarrow'] . "\" />&nbsp;";
				} elseif ($row['pos_state'] == 'sticky' && $CURUSER['appendsticky'] == 'yes') {
					$stickyicon = "<img class=\"sticky\" src=\"pic/trans.gif\" alt=\"置顶\" title=\"" . $lang_functions['title_sticky'] . "至" . $row['endsticky'] . "\" />&nbsp;";
				} else {
					$stickyicon = "";
				}
			}

			$arry = sql_query("SELECT first, excl, official, bumpoff, releasedate, limitd, status FROM torrents WHERE  id=" . $id) or sqlerr();
			$rowicon = mysql_fetch_array($arry);
			if ($rowicon['first'] == 'yes') {
				$firsticon = "<span class=\"browse first\">首发</span>";
			} else {
				$firsticon = "";
			}

			if ($rowicon['excl'] == 'yes') {
				$exclicon = "<span class=\"browse excl\">禁转</span>";
			} else {
				$exclicon = "";
			}

			if ($rowicon['official'] == 'yes') {
				//$officialicon = "<img class=\"official\" src=\"pic/trans.gif\" alt=\"official\" title=\"做种魔力值+50%\" />";
				$officialicon = "<span class=\"browse official\">官方</span>";
			} else {
				$officialicon = "";
			}
			if ($rowicon['bumpoff'] == 'yes') {
				$bumpofficon = "<span class=\"browse bumpoff\">救活</span>";
			} else {
				$bumpofficon = "";
			}
			if ($rowicon['releasedate'] != '' && $rowicon['status'] == 'candidate') {
				$releasedateicon = "<span class=\"browse releasedate\">定时</span>";
			} else {
				$releasedateicon = "";
			}
			if ($rowicon['limitd'] == 'yes') {
				$limitdicon = "<span class=\"browse limitd\">限定</span>";
			} else {
				$limitdicon = "";
			}

			//标识没浏览的新发布种子-开始
			$script_name = $_SERVER["SCRIPT_FILENAME"];
			if (preg_match("/torrents/i", $script_name)) {
				$selected = "torrents";
			}
			if ($CURUSER['appendnew'] != 'no' && strtotime($row['added']) >= $last_browse && $selected == 'torrents') {
				$newtorrenticon = "<span class=\"browse new\">新</span>";
			} else {
				$newtorrenticon = "";
			}
			if (preg_match("/limit/i", $script_name)) {
				$selected = "limit";
			}
			if ($CURUSER['appendnew'] != 'no' && strtotime($row['added']) >= $limit_last_browse && $selected == 'limit') {
				$newlimiticon = "<span class=\"browse new\">新</span>";
			} else {
				$newlimiticon = "";
			}
			//标识没浏览的新发布种子-结束

			print("<td id=\"torrents_td\" class=\"rowfollow\" width=\"100%\" align=\"left\"><table class=\"torrentname\" width=\"100%\" style=\"background-color:transparent\"><td class=\"embedded\">" . $releasedateicon . $stickyicon . $newtorrenticon . $newlimiticon . $bumpofficon . $officialicon . $firsticon . $exclicon . $limitdicon . "<a $short_torrent_name_alt $mouseovertorrent href=\"details.php?id=" . $id . "&amp;hit=1\"><b>" . ($row['namecolor'] != '' && $row['namecolor'] != '#000000' ? "<span style=\"color: " . $row['namecolor'] . "\">" : "") . "" . htmlspecialchars($dispname) . "</span></b></a>");
			//判断种子促销/置顶时间到期
			if (Clean_Free($row['id'], $row['sp_state'], $row['endfree'], $row['name'])) {
				$row['sp_state'] = "1";
				$row['bumpoff'] = "no";
			}
			if (Clean_Sticky($row['id'], $row['pos_state'], $row['endsticky'], $row['name'])) {
				$row['pos_state'] = "normal";
				$row['bumpoff'] = "no";
			}
			if (Clean_Marrow($row['id'], $row['marrow'], $row['endmarrow'], $row['name'])) {
				$row['marrow'] = "normal";
				$row['bumpoff'] = "no";
			}
			$sp_torrent = get_torrent_promotion_append($row['sp_state'], "", true, $row['endfree'], $row['promotion_time_type'], $row['promotion_until']);
			$picked_torrent = "";
			if ($CURUSER['appendpicked'] != 'no') {
				if ($row['picktype'] == "hot")
					$picked_torrent = " <b>[<font class='hot'>" . $lang_functions['text_hot'] . "</font>]</b>";
				elseif ($row['picktype'] == "classic")
					$picked_torrent = " <b>[<font class='classic'>" . $lang_functions['text_classic'] . "</font>]</b>";
				elseif ($row['picktype'] == "recommended")
					$picked_torrent = " <b>[<font class='recommended'>" . $lang_functions['text_recommended'] . "</font>]</b>";
			}
			$banned_torrent = ($row["banned"] == 'yes' ? " <b>(<font class=\"striking\">" . $lang_functions['text_banned'] . "</font>)</b>" : "");
			print($banned_torrent . $picked_torrent . $sp_torrent);
			if ($displaysmalldescr) {
				//small descr
				$dissmall_descr = trim($row["small_descr"]);
				$count_dissmall_descr = mb_strlen($dissmall_descr, "UTF-8");
				$max_lenght_of_small_descr = $max_length_of_torrent_name; // maximum length
				if ($count_dissmall_descr > $max_lenght_of_small_descr) {
					$dissmall_descr = mb_substr($dissmall_descr, 0, $max_lenght_of_small_descr - 2, "UTF-8") . "...";
				}
				print($dissmall_descr == "" ? "<br />" . (!empty($row['albumid']) ? "&nbsp;<a href=\"album.php?action=view&cid=$row[albumid]\" target=\"_blank\"><img alt=\"专辑\" title=\"专辑\" src=\"styles/img/album.png\" /></a>" : "") : "<br />" . ($row['descrcolor'] != '' && $row['descrcolor'] != '#000000' ? "<span style=\"color: " . $row['descrcolor'] . "\">" : "") . "" . htmlspecialchars($dissmall_descr) . "</span>" . (!empty($row['albumid']) ? "&nbsp;<a href=\"album.php?action=view&cid=$row[albumid]\" target=\"_blank\"><img alt=\"专辑\" title=\"专辑\" src=\"styles/img/album.png\" /></a>" : ""));
			}
			print("</td>");
			//自动更新专辑
			/*
			  if (empty($row['albumid'])) {
			  $albumid = mysql_fetch_array(sql_query("SELECT albumid FROM albumseries WHERE torrentid = $row[id]"));
			  sql_query("UPDATE torrents SET albumid = $albumid[albumid] WHERE id IN (SELECT torrentid FROM albumseries)");
			  }
			 *
			 */
			$act = "";
			$subs = get_row_count("subs", "WHERE torrent_id = $id");
			if ($subs > 0) {
				$act .= "<a href=\"details.php?id=$id&hit=1\"><img alt=\"subs\" title=\"字幕\" src=\"styles/img/subs.png\" style='padding-bottom: 2px;' /></a>";
			}
			if ($CURUSER["dlicon"] != 'no' && $CURUSER["downloadpos"] != "no") {
				$act .= " <a href=\"" . $downhref . "\"><img class=\"download\" src=\"pic/trans.gif\" style='padding-bottom: 2px;' alt=\"download\" title=\"" . $lang_functions['title_download_torrent'] . "\" /></a>";
			}
			if ($CURUSER["bmicon"] == 'yes') {
				$bookmark = " href=\"javascript: bookmark(" . $id . "," . $counter . "," . $CURUSER['stylesheet'] . ");\"";
				$act .= " <a id=\"bookmark" . $counter . "\" " . $bookmark . " >" . get_torrent_bookmark_state($CURUSER['id'], $id) . "</a>";
			}
			//小货车开始
			$truckmark = " href=\"javascript: truckmark(" . $id . "," . $counter . "," . $CURUSER['stylesheet'] . ");\"";
			$act .= " <a id=\"truckmark" . $counter . "\" " . $truckmark . " >" . get_torrent_truckmark_state($CURUSER['id'], $id) . "</a><br />";
			//小货车结束
			//认领种子开始
			$act .= " <span>" . get_torrent_claimtor_state($CURUSER['id'], $id) . "</span>";
			//认领种子结束
			$sql = mysql_fetch_array(sql_query("SELECT url FROM torrents WHERE id = $id")) or sqlerr(__FILE__, __LINE__);
			$imdb_id = parse_imdb_id($sql['url']);
			if ($imdb_id && $showextinfo['imdb'] == 'yes' && $CURUSER['showimdb'] != 'no') {
				$thenumbers = $imdb_id;
				$movie = new imdb($thenumbers);
				if ($movie->rating() != 'N/A' && $imdb_id) {
					$act .= " <a href=\"details.php?id=$id&hit=1\"><img style='vertical-align: text-bottom;' title=\"IMDb评分：" . $movie->rating() . "\" src=\"styles/img/imdb.gif\" /> <span style='vertical-align: text-bottom;' >" . $movie->rating() . "</span></a>";
				}
			}

			print("<td width=\"80\" class=\"embedded\" style=\"text-align: right; \" valign=\"middle\">" . $act . "</td>");
			print("</tr></table>");
			//种子进度条
			if ($a > 0) {//显示进度条
				if ($b > 0 or $res11['downloaded'] >= $res12['size'] or $res11['finished'] == 'yes') {
					$d = 100;
				} else {
					$d = $res11['downloaded'] / $res12['size'] * 100;
					if (($res11['downloaded'] / $res12['size'] * 100) > 100)
						$d = 100;
				}
				if ($d < 100 && $res11['last_action'] >= $deltime) {//未下载完成种子，并且最后更新时间>=$deltime
					print("<table style='width:100%; margin-top:5px; background-color:transparent'><tr><td class='embedded'><div class='progressarea' title='已上传：" . mksize($res11['uploaded']) . "  已下载：" . mksize($res11['downloaded']) . "  已完成：" . round($d, 2) . "%' style='border: 1px #808080 solid;'><div  style='background-color:#36648B;height:2px;text-align:center;font-size:2px;width:" . round($d, 2) . "%;'></div></div></td></tr></tbody></table>");
				}
			}
			print("</td>");

			if ($wait) {
				$elapsed = floor((TIMENOW - strtotime($row["added"])) / 3600);
				if ($elapsed < $wait) {
					$color = dechex(floor(127 * ($wait - $elapsed) / 48 + 128) * 65536);
					print("<td id=\"torrents_td\" class=\"rowfollow nowrap\"><a href=\"faq.php\"><font color=\"" . $color . "\">" . number_format($wait - $elapsed) . $lang_functions['text_h'] . "</font></a></td>");
				} else
					print("<td id=\"torrents_td\" class=\"rowfollow nowrap\">" . $lang_functions['text_none'] . "</td>");
			}

			//numfiles
			$numfiles = mysql_fetch_array(sql_query("SELECT numfiles FROM torrents WHERE id=" . $id));
			print("<td id=\"torrents_td\" class=\"rowfollow\">" . $numfiles['numfiles'] . "</td>");

			if ($CURUSER['showcomnum'] != 'no') {
				print("<td id=\"torrents_td\" class=\"rowfollow\">");
				if (!$row["comments"]) {
					print("<a href=\"comment.php?action=add&amp;pid=" . $id . "&amp;type=torrent\" title=\"" . $lang_functions['title_add_comments'] . "\">" . $row["comments"] . "</a>");
				} else {
					if ($enabletooltip_tweak == 'yes' && $CURUSER['showlastcom'] != 'no') {
						if (!$lastcom = $Cache->get_value('torrent_' . $id . '_last_comment_content')) {
							$res2 = sql_query("SELECT user, added, text FROM comments WHERE torrent = $id ORDER BY id DESC LIMIT 1");
							$lastcom = mysql_fetch_array($res2);
							$Cache->cache_value('torrent_' . $id . '_last_comment_content', $lastcom, 1855);
						}
						$timestamp = strtotime($lastcom["added"]);
						$hasnewcom = ($lastcom['user'] != $CURUSER['id'] && $timestamp >= $last_browse);
						if ($lastcom) {
							if ($CURUSER['timetype'] != 'timealive')
								$lastcomtime = $lang_functions['text_at_time'] . $lastcom['added'];
							else
								$lastcomtime = $lang_functions['text_blank'] . gettime($lastcom["added"], true, false, true);
							$lastcom_tooltip[$counter]['id'] = "lastcom_" . $counter;
							$lastcom_tooltip[$counter]['content'] = ($hasnewcom ? "<b>(<font class='new'>" . $lang_functions['text_new_uppercase'] . "</font>)</b> " : "") . $lang_functions['text_last_commented_by'] . get_username($lastcom['user']) . $lastcomtime . "<br />" . format_comment(mb_substr($lastcom['text'], 0, 100, "UTF-8") . (mb_strlen($lastcom['text'], "UTF-8") > 100 ? " ......" : "" ), true, false, false, true, 600, false, false);
							$onmouseover = "onmouseover=\"domTT_activate(this, event, 'content', document.getElementById('" . $lastcom_tooltip[$counter]['id'] . "'), 'trail', false, 'delay', 500,'lifetime',3000,'fade','both','styleClass','nicetitle','fadeMax', 98,'maxWidth', 400);\"";
						}
					} else {
						$hasnewcom = false;
						$onmouseover = "";
					}
					print("<b><a href=\"details.php?id=" . $id . "&amp;hit=1&amp;cmtpage=1#startcomments\" " . $onmouseover . ">" . ($hasnewcom ? "<font class='new'>" : "") . $row["comments"] . ($hasnewcom ? "</font>" : "") . "</a></b>");
				}
				print("</td>");
			}

			$time = gettime($row["added"], false, true);
			print("<td id=\"torrents_td\" class=\"rowfollow nowrap\">" . $time . "</td>");

			//size
			print("<td id=\"torrents_td\" class=\"rowfollow\">" . mksize_compact($row["size"]) . "</td>");

			if ($row["seeders"]) {
				$ratio = ($row["leechers"] ? ($row["seeders"] / $row["leechers"]) : 1);
				$ratiocolor = get_slr_color($ratio);
				print("<td id=\"torrents_td\" class=\"rowfollow\"><b><a href=\"details.php?id=" . $id . "&amp;hit=1&amp;dllist=1#seeders\">" . ($ratiocolor ? "<font color=\"" . $ratiocolor . "\">" . number_format($row["seeders"]) . "</font>" : number_format($row["seeders"])) . "</a></b></td>");
			} else
				print("<td id=\"torrents_td\" class=\"rowfollow\"><span class=\"" . linkcolor($row["seeders"]) . "\">" . number_format($row["seeders"]) . "</span></td>");

			if ($row["leechers"]) {
				print("<td id=\"torrents_td\" class=\"rowfollow\"><b><a href=\"details.php?id=" . $id . "&amp;hit=1&amp;dllist=1#leechers\">" . number_format($row["leechers"]) . "</a></b></td>");
			} else
				print("<td id=\"torrents_td\" class=\"rowfollow\">0</td>");

			if ($row["times_completed"] >= 1)
				print("<td id=\"torrents_td\" class=\"rowfollow\"><a href=\"viewsnatches.php?id=" . $row[id] . "\"><b>" . number_format($row["times_completed"]) . "</b></a></td>");
			else
				print("<td id=\"torrents_td\" class=\"rowfollow\">" . number_format($row["times_completed"]) . "</td>");

			if ($row["anonymous"] == "yes") {
				if (get_user_class() >= $torrentmanage_class) {
					print("<td id=\"torrents_td\" class=\"rowfollow\"><i>" . $lang_functions['text_anonymous'] . "</i><br />" . (isset($row["owner"]) ? "(" . get_username($row["owner"]) . ")" : "<i>" . $lang_functions['text_orphaned'] . "</i>") . "</td>");
				} else {
					print("<td id=\"torrents_td\" class=\"rowfollow\"><i>" . $lang_functions['text_anonymous'] . "</i></td>");
				}
			} else {
				print("<td id=\"torrents_td\" class=\"rowfollow\">" . (isset($row["owner"]) ? get_username($row["owner"]) : "<i>" . $lang_functions['text_orphaned'] . "</i>") . "</td>");
			}

			if (get_user_class() >= $torrentmanage_class) {
				print("<td id=\"torrents_td\" class=\"rowfollow\"><input type=\"checkbox\" value=\"{$row['id']}\" name=\"id[]\" class=\"checkbox\"></td>");
				if ($mode == "normal") {
					print("<td id=\"torrents_td\" class=\"rowfollow\"><a href=\"" . htmlspecialchars("edit.php?id=" . $row[id]) . "\"><img class=\"staff_edit\" src=\"pic/trans.gif\" alt=\"D\" title=\"" . $lang_functions['text_edit'] . "\" /></a>");
				} else {
					print("<td id=\"torrents_td\" class=\"rowfollow\"><a href=\"" . htmlspecialchars("edit.php?id=" . $row[id]) . "\">编辑</a>");
					$checkrelease = mysql_fetch_array(sql_query("SELECT releasedate, status FROM torrents WHERE id = $row[id]"));
					if ($checkrelease['releasedate'] == '' && $checkrelease['status'] == 'candidate') {
						print("<br/><a href=\"" . htmlspecialchars("delete.php?id=" . $row[id]) . "&recycle_mode=release\">发布</a></td>");
					}
				}
			}
			print("</tr></tbody>");
			$counter++;
		}
		if (get_user_class() >= $torrentmanage_class) {
			print("<tr><td id=\"torrents_td\" class=\"rowfollow\" colspan=\"20\"><input type=\"button\" value=\"全选\" onclick=\"checkAll();\">");
			print("<input type=\"button\" value=\"反选\" onclick=\"reverseCheck();\">");
			print(" | <input type=\"button\" id=\"delete\" value=\"删除\">");
			if ($mode != "normal") {
				print("<input type=\"submit\" formaction=\"delete.php?recycle_mode=release\" value=\"发布\"></tr>");
			}
		}
		print("</table>");
		if ($CURUSER['appendpromotion'] == 'highlight') {
			if ($ratioless == 'no') {
				print("<p align=\"center\"> " . $lang_functions['text_promoted_torrents_note'] . "</p>");
			} else {
				print("<p align=\"center\"> " . $lang_functions['text_promoted_torrents_note_ratioless'] . "</p>");
			}
		}

		if ($enabletooltip_tweak == 'yes' && (!isset($CURUSER) || $CURUSER['showlastcom'] == 'yes'))
			create_tooltip_container($lastcom_tooltip, 400);
		create_tooltip_container($torrent_tooltip, 500);
	}

	function trucktorrenttable($res, $variant = "torrent") {
		global $Cache, $lang_functions;
		global $CURUSER, $waitsystem, $browsecatmode, $specialcatmode;
		global $torrentmanage_class, $smalldescription_main, $enabletooltip_tweak, $showextinfo, $ratioless;

		$last_browse_res = mysql_fetch_array(sql_query("SELECT last_browse, last_music, limit_last_browse FROM users WHERE id = " . $CURUSER['id']));

		if ($variant == "torrent") {
			$last_browse = $last_browse_res['last_browse'];
			$sectiontype = $browsecatmode;
		} elseif ($variant == "music") {
			$last_browse = $last_browse_res['last_music'];
			$sectiontype = $specialcatmode;
		} else {
			$last_browse = $last_browse_res['last_browse'];
			$sectiontype = "";
		}

		$time_now = TIMENOW;
		if ($last_browse > $time_now) {
			$last_browse = $time_now;
		}

		if ($variant == "torrent") {
			$limit_last_browse = $last_browse_res['limit_last_browse'];
			$sectiontype = $browsecatmode;
		} elseif ($variant == "music") {
			$limit_last_browse = $last_browse_res['last_music'];
			$sectiontype = $specialcatmode;
		} else {
			$limit_last_browse = $last_browse_res['limit_last_browse'];
			$sectiontype = "";
		}

		if ($limit_last_browse > $time_now) {
			$limit_last_browse = $time_now;
		}

		if (get_user_class() < UC_VIP && $waitsystem == "yes") {
			$ratio = get_ratio($CURUSER["id"], false);
			$gigs = $CURUSER["downloaded"] / (1024 * 1024 * 1024);
			if ($gigs > 10) {
				if ($ratio < 0.2)
					$wait = 24;
				elseif ($ratio < 0.3)
					$wait = 12;
				elseif ($ratio < 0.4)
					$wait = 6;
				elseif ($ratio < 0.5)
					$wait = 3;
				else
					$wait = 0;
			} else
				$wait = 0;
		}
		?>
		<table class="torrents" cellspacing="0" cellpadding="5" width="100%">
			<tr>
				<?php
				$count_get = 0;
				$oldlink = "";
				foreach ($_GET as $get_name => $get_value) {
					$get_name = mysql_real_escape_string(strip_tags(str_replace(array("\"", "'"), array("", ""), $get_name)));
					$get_value = mysql_real_escape_string(strip_tags(str_replace(array("\"", "'"), array("", ""), $get_value)));

					if ($get_name != "sort" && $get_name != "type") {
						if ($count_get > 0) {
							$oldlink .= "&amp;" . $get_name . "=" . $get_value;
						} else {
							$oldlink .= $get_name . "=" . $get_value;
						}
						$count_get++;
					}
				}
				if ($count_get > 0) {
					$oldlink = $oldlink . "&amp;";
				}
				$sort = $_GET['sort'];
				$link = array();
				for ($i = 1; $i <= 9; $i++) {
					if ($sort == $i)
						$link[$i] = ($_GET['type'] == "desc" ? "asc" : "desc");
					else
						$link[$i] = ($i == 1 ? "asc" : "desc");
				}
				?>
				<td class="colhead" style="padding: 0px"><?php echo $lang_functions['col_type'] ?></td>
				<td class="colhead"><a href="?<?php echo $oldlink ?>sort=1&amp;type=<?php echo $link[1] ?>"><?php echo $lang_functions['col_name'] ?></a></td>
				<?php
				if ($wait) {
					print("<td class=\"colhead\">" . $lang_functions['col_wait'] . "</td>");
				}
				if ($CURUSER['showcomnum'] != 'no') {
					?>
					<td class="colhead"><?php echo $lang_functions['title_number_of_files'] ?></td>
					<td class="colhead"><a href="?<?php echo $oldlink ?>sort=3&amp;type=<?php echo $link[3] ?>"><img class="comments" src="pic/trans.gif" alt="comments" title="<?php echo $lang_functions['title_number_of_comments'] ?>" /></a></td>
				<?php } ?>

				<td class="colhead"><a href="?<?php echo $oldlink ?>sort=4&amp;type=<?php echo $link[4] ?>"><img class="time" src="pic/trans.gif" alt="time" title="<?php echo ($CURUSER['timetype'] != 'timealive' ? $lang_functions['title_time_added'] : $lang_functions['title_time_alive']) ?>" /></a></td>
				<td class="colhead"><a href="?<?php echo $oldlink ?>sort=5&amp;type=<?php echo $link[5] ?>"><img class="size" src="pic/trans.gif" alt="size" title="<?php echo $lang_functions['title_size'] ?>" /></a></td>
				<td class="colhead"><a href="?<?php echo $oldlink ?>sort=7&amp;type=<?php echo $link[7] ?>"><img class="seeders" src="pic/trans.gif" alt="seeders" title="<?php echo $lang_functions['title_number_of_seeders'] ?>" /></a></td>
				<td class="colhead"><a href="?<?php echo $oldlink ?>sort=8&amp;type=<?php echo $link[8] ?>"><img class="leechers" src="pic/trans.gif" alt="leechers" title="<?php echo $lang_functions['title_number_of_leechers'] ?>" /></a></td>
				<td class="colhead"><a href="?<?php echo $oldlink ?>sort=6&amp;type=<?php echo $link[6] ?>"><img class="snatched" src="pic/trans.gif" alt="snatched" title="<?php echo $lang_functions['title_number_of_snatched'] ?>" /></a></td>
				<td class="colhead"><a href="?<?php echo $oldlink ?>sort=9&amp;type=<?php echo $link[9] ?>"><?php echo $lang_functions['col_uploader'] ?></a></td>
				<?php
				$script_name = $_SERVER["SCRIPT_FILENAME"];
				if (preg_match("/viewclaim/i", $script_name)) {
					print("<td class=\"colhead\">上传流量</td>");
					print("<td class=\"colhead\">做种时间</td>");
					print("<td class=\"colhead\">认领时间</td>");
				}
				?>
				<td class="colhead"><?php echo $lang_functions['col_action'] ?></td>
			</tr>
			<?php
			$caticonrow = get_category_icon_row($CURUSER['caticon']);
			if ($caticonrow['secondicon'] == 'yes')
				$has_secondicon = true;
			else
				$has_secondicon = false;
			$counter = 0;
			if ($smalldescription_main == 'no' || $CURUSER['showsmalldescr'] == 'no')
				$displaysmalldescr = false;
			else
				$displaysmalldescr = true;
			while ($row = mysql_fetch_assoc($res)) {
				$id = $row["id"];
				$userid = $CURUSER['id'];
				$a = get_row_count("snatched", "WHERE userid = " . $userid . " AND torrentid = " . $id);
				$truckmarks = get_row_count("truckmarks", "WHERE userid = $userid AND torrentid = $id");
				$bookmarks = get_row_count("bookmarks", "WHERE userid = $userid AND torrentid = $id");
				if ($a > 0) {
					$b = get_row_count("peers", "WHERE userid = " . $userid . " AND torrent = " . $id . " AND seeder = 'yes'");
					$deltime = date("Y-m-d H:i:s", time() - 3600);
					$res11 = mysql_fetch_array(sql_query("SELECT uploaded, downloaded, finished, last_action FROM snatched WHERE userid = $userid AND torrentid = $id"));
					$res12 = mysql_fetch_array(sql_query("SELECT size FROM torrents WHERE id = " . $id));
					if ($truckmarks > 0) {
						print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table truckmarks\">");
					} elseif ($bookmarks > 0) {
						print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table bookmarks\">");
					} elseif ($b > 0) {
						print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table seeders\">"); //在做种的
					} elseif ($res11['downloaded'] >= $res12['size'] || $res11['finished'] == 'yes') {
						print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table finished\">"); //已下载完
					} elseif ($res11['downloaded'] < $res12['size'] && $res11['last_action'] >= $deltime) {
						print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table leechers\">"); //未下载完
					} elseif ($row['pos_state'] != 'normal') {
						print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table sticky\">");
					} elseif ($row['marrow'] != 'normal') {
						print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table sticky\">");
					} else {
						print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table\">");
					}
				} else {
					if ($truckmarks > 0) {
						print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table truckmarks\">");
					} elseif ($bookmarks > 0) {
						print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table bookmarks\">");
					} elseif ($row['pos_state'] != 'normal') {
						print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table sticky\">");
					} elseif ($row['marrow'] != 'normal') {
						print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table sticky\">");
					} else {
						print("<tbody id=\"torrents_tbody\"><tr id=\"tid_$id\" class=\"torrent_table\">");
					}
				}

				//强制分享
				global $share, $sharesize, $sharenum, $sharemanage_class, $downhref;
				$ownercheck = mysql_fetch_assoc(sql_query("SELECT owner FROM torrents WHERE id = " . sqlesc($id))) or sqlerr(__FILE__, __LINE__);
				if ($share == "yes") {
					if ($CURUSER["id"] != $ownercheck['owner']) {
						$shareck = sql_query("SELECT size, status FROM torrents WHERE owner = " . $CURUSER['id']) or sqlerr(__FILE__, __LINE__);
						$userdonor = mysql_fetch_array(sql_query("SELECT donor FROM users WHERE id = " . $CURUSER['id'])) or sqlerr(__FILE__, __LINE__);
						if (get_user_class() >= $sharemanage_class || $userdonor['donor'] == 'yes') {
							$downhref = "download.php?id=" . $id . "&passkey=" . $CURUSER['passkey'];
						} else {
							$normalcount = 0;
							while ($sharecheck = mysql_fetch_assoc($shareck)) {
								if ($sharecheck['status'] == 'normal') {
									$normalcount++;
									$sharecheck_sizesum += $sharecheck['size'];
								}
							}
							if ($normalcount >= $sharenum && $sharecheck_sizesum >= $sharesize) {
								$downhref = "download.php?id=" . $id . "&passkey=" . $CURUSER['passkey'];
							} else {
								$downhref = "javascript:alert('你必须先成功分享 " . $sharenum . " 个大于" . mksize($sharesize) . "或多个种子总大小大于 " . mksize($sharesize) . " 的资源后才可以下载其它种子资源。');";
							}
						}
					} else {
						$downhref = "download.php?id=" . $id . "&passkey=" . $CURUSER['passkey'];
					}
				} else {
					$downhref = "download.php?id=" . $id . "&passkey=" . $CURUSER['passkey'];
				}

				print("<td id=\"torrents_td\" class=\"rowfollow nowrap\" valign=\"middle\" style='padding: 0px'>");
				if (isset($row["category"])) {
					if ($row["category"] < 424) {
						$link = "torrents.php?";
					} else {
						$link = "limit.php?";
					}
					print(return_category_image($row["category"], $link));
					print(return_sec_category_image($row['category'], $row["source"], $link)); //二级分类
					if ($has_secondicon) {
						print(get_second_icon($row, "pic/" . $catimgurl . "additional/"));
					}
				} else
					print("-");
				print("</td>");

				//torrent name
				$dispname = trim($row["name"]);
				$short_torrent_name_alt = "";
				$mouseovertorrent = "";
				$tooltipblock = "";
				$has_tooltip = false;
				if ($enabletooltip_tweak == 'yes')
					$tooltiptype = $CURUSER['tooltip'];
				else
					$tooltiptype = 'off';
				switch ($tooltiptype) {
					case 'minorimdb' : {
							if ($showextinfo['imdb'] == 'yes' && $row["url"]) {
								$url = $row['url'];
								$cache = $row['cache_stamp'];
								$type = 'minor';
								$has_tooltip = true;
							}
							break;
						}
					case 'medianimdb' : {
							if ($showextinfo['imdb'] == 'yes' && $row["url"]) {
								$url = $row['url'];
								$cache = $row['cache_stamp'];
								$type = 'median';
								$has_tooltip = true;
							}
							break;
						}
					case 'off' : break;
				}
				if (!$has_tooltip)
					$short_torrent_name_alt = "title=\"" . htmlspecialchars($dispname) . "\"";
				else {
					$torrent_tooltip[$counter]['id'] = "torrent_" . $counter;
					$torrent_tooltip[$counter]['content'] = "";
					$mouseovertorrent = "onmouseover=\"get_ext_info_ajax('" . $torrent_tooltip[$counter]['id'] . "','" . $url . "','" . $cache . "','" . $type . "'); domTT_activate(this, event, 'content', document.getElementById('" . $torrent_tooltip[$counter]['id'] . "'), 'trail', false, 'delay',600,'lifetime',6000,'fade','both','styleClass','nicetitle','fadeMax',98, 'maxWidth', 500);\"";
				}
				$count_dispname = mb_strlen($dispname, "UTF-8");
				if (!$displaysmalldescr || $row["small_descr"] == "")// maximum length of torrent name
					$max_length_of_torrent_name = 85;
				elseif ($CURUSER['fontsize'] == 'large')
					$max_length_of_torrent_name = 70;
				elseif ($CURUSER['fontsize'] == 'small')
					$max_length_of_torrent_name = 100;
				else
					$max_length_of_torrent_name = 85;

				if ($count_dispname > $max_length_of_torrent_name)
					$dispname = mb_substr($dispname, 0, $max_length_of_torrent_name - 2, "UTF-8") . "...";

				if ($row['endsticky'] >= $row['endmarrow'] && $row['pos_state'] == 'sticky') {//如果置顶结束时间>=超级置顶结束时间，并且为置顶时，则显示置顶结束时间，否则显示超级置顶结束时间
					if ($row['pos_state'] == 'sticky' && $CURUSER['appendsticky'] == 'yes') {
						$stickyicon = "<img class=\"sticky\" src=\"pic/trans.gif\" alt=\"置顶\" title=\"" . $lang_functions['title_sticky'] . "至" . $row['endsticky'] . "\" />&nbsp;";
					} else {
						$stickyicon = "";
					}
				} else {
					//超级置顶
					if ($row['marrow'] == 'marrow1' || $row['marrow'] == 'marrow2' || $row['marrow'] == 'marrow3' && $CURUSER['appendsticky'] == 'yes') {
						$stickyicon = "<img class=\"sticky\" src=\"pic/trans.gif\" alt=\"置顶\" title=\"" . $lang_functions['title_sticky'] . "至" . $row['endmarrow'] . "\" />&nbsp;";
					} else {
						$stickyicon = "";
					}
				}

				$arry = sql_query("SELECT first, excl, official, bumpoff, releasedate, limitd, status FROM torrents WHERE  id=" . $id) or sqlerr();
				$rowicon = mysql_fetch_array($arry);
				if ($rowicon['first'] == 'yes') {
					$firsticon = "<span class=\"browse first\">首发</span>";
				} else {
					$firsticon = "";
				}

				if ($rowicon['excl'] == 'yes') {
					$exclicon = "<span class=\"browse excl\">禁转</span>";
				} else {
					$exclicon = "";
				}

				if ($rowicon['official'] == 'yes') {
					//$officialicon = "<img class=\"official\" src=\"pic/trans.gif\" alt=\"official\" title=\"做种魔力值+50%\" />";
					$officialicon = "<span class=\"browse official\">官方</span>";
				} else {
					$officialicon = "";
				}
				if ($rowicon['bumpoff'] == 'yes') {
					$bumpofficon = "<span class=\"browse bumpoff\">救活</span>";
				} else {
					$bumpofficon = "";
				}
				if ($rowicon['releasedate'] != '' && $rowicon['status'] == 'candidate') {
					$releasedateicon = "<span class=\"browse releasedate\">定时</span>";
				} else {
					$releasedateicon = "";
				}
				if ($rowicon['limitd'] == 'yes') {
					$limitdicon = "<span class=\"browse limitd\">限定</span>";
				} else {
					$limitdicon = "";
				}

				//标识没浏览的新发布种子-开始
				if (preg_match("/torrents/i", $script_name)) {
					$selected = "torrents";
				}
				if ($CURUSER['appendnew'] != 'no' && strtotime($row['added']) >= $last_browse && $selected == 'torrents') {
					$newtorrenticon = "<span class=\"browse new\">新</span>";
				} else {
					$newtorrenticon = "";
				}
				if (preg_match("/limit/i", $script_name)) {
					$selected = "limit";
				}
				if ($CURUSER['appendnew'] != 'no' && strtotime($row['added']) >= $limit_last_browse && $selected == 'limit') {
					$newlimiticon = "<span class=\"browse new\">新</span>";
				} else {
					$newlimiticon = "";
				}
				//标识没浏览的新发布种子-结束

				print("<td id=\"torrents_td\" class=\"rowfollow\" width=\"100%\" align=\"left\"><table class=\"torrentname\" width=\"100%\" style=\"background-color:transparent\"><td class=\"embedded\">" . $releasedateicon . $stickyicon . $newtorrenticon . $newlimiticon . $bumpofficon . $officialicon . $firsticon . $exclicon . $limitdicon . "<a $short_torrent_name_alt $mouseovertorrent href=\"details.php?id=" . $id . "&amp;hit=1\"><b>" . ($row['namecolor'] != '' && $row['namecolor'] != '#000000' ? "<span style=\"color: " . $row['namecolor'] . "\">" : "") . "" . htmlspecialchars($dispname) . "</span></b></a>");
				//判断种子促销置顶时间到期
				if (Clean_Free($row['id'], $row['sp_state'], $row['endfree'], $row['name']))
					$row['sp_state'] = "1";
				if (Clean_Sticky($row['id'], $row['pos_state'], $row['endsticky'], $row['name']))
					$row['pos_state'] = "normal";
				if (Clean_Marrow($row['id'], $row['marrow'], $row['endmarrow'], $row['name']))
					$row['marrow'] = "normal";
				$sp_torrent = get_torrent_promotion_append($row['sp_state'], "", true, $row['endfree'], $row['promotion_time_type'], $row['promotion_until']);
				$picked_torrent = "";
				if ($CURUSER['appendpicked'] != 'no') {
					if ($row['picktype'] == "hot")
						$picked_torrent = " <b>[<font class='hot'>" . $lang_functions['text_hot'] . "</font>]</b>";
					elseif ($row['picktype'] == "classic")
						$picked_torrent = " <b>[<font class='classic'>" . $lang_functions['text_classic'] . "</font>]</b>";
					elseif ($row['picktype'] == "recommended")
						$picked_torrent = " <b>[<font class='recommended'>" . $lang_functions['text_recommended'] . "</font>]</b>";
				}
				$banned_torrent = ($row["banned"] == 'yes' ? " <b>(<font class=\"striking\">" . $lang_functions['text_banned'] . "</font>)</b>" : "");
				print($banned_torrent . $picked_torrent . $sp_torrent);
				if ($displaysmalldescr) {
					//small descr
					$dissmall_descr = trim($row["small_descr"]);
					$count_dissmall_descr = mb_strlen($dissmall_descr, "UTF-8");
					$max_lenght_of_small_descr = $max_length_of_torrent_name; // maximum length
					if ($count_dissmall_descr > $max_lenght_of_small_descr) {
						$dissmall_descr = mb_substr($dissmall_descr, 0, $max_lenght_of_small_descr - 2, "UTF-8") . "...";
					}
					print($dissmall_descr == "" ? "<br />" . (!empty($row['albumid']) ? "&nbsp;<a href=\"album.php?action=view&cid=$row[albumid]\" target=\"_blank\"><img alt=\"专辑\" title=\"专辑\" src=\"styles/img/album.png\" /></a>" : "") : "<br />" . ($row['descrcolor'] != '' && $row['descrcolor'] != '#000000' ? "<span style=\"color: " . $row['descrcolor'] . "\">" : "") . "" . htmlspecialchars($dissmall_descr) . "</span>" . (!empty($row['albumid']) ? "&nbsp;<a href=\"album.php?action=view&cid=$row[albumid]\" target=\"_blank\"><img alt=\"专辑\" title=\"专辑\" src=\"styles/img/album.png\" /></a>" : ""));
				}
				print("</td>");
				//自动更新专辑
				/*
				  if (empty($row['albumid'])) {
				  $albumid = mysql_fetch_array(sql_query("SELECT albumid FROM albumseries WHERE torrentid = $row[id]"));
				  sql_query("UPDATE torrents SET albumid = $albumid[albumid] WHERE id IN (SELECT torrentid FROM albumseries)");
				  }
				 *
				 */
				$act = "";
				$subs = get_row_count("subs", "WHERE torrent_id = $id");
				if ($subs > 0) {
					$act .= "<a href=\"details.php?id=$id&hit=1\"><img alt=\"subs\" title=\"字幕\" src=\"styles/img/subs.png\" style='padding-bottom: 2px;' /></a>";
				}
				if ($CURUSER["dlicon"] != 'no' && $CURUSER["downloadpos"] != "no") {
					$act .= " <a href=\"" . $downhref . "\"><img class=\"download\" src=\"pic/trans.gif\" style='padding-bottom: 2px;' alt=\"download\" title=\"" . $lang_functions['title_download_torrent'] . "\" /></a>";
				}
				if ($CURUSER["bmicon"] == 'yes') {
					$bookmark = " href=\"javascript: bookmark(" . $id . "," . $counter . "," . $CURUSER['stylesheet'] . ");\"";
					$act .= " <a id=\"bookmark" . $counter . "\" " . $bookmark . " >" . get_torrent_bookmark_state($CURUSER['id'], $id) . "</a>";
				}
				//小货车开始
				$truckmark = " href=\"javascript: truckmark(" . $id . "," . $counter . "," . $CURUSER['stylesheet'] . ");\"";
				$act .= " <a id=\"truckmark" . $counter . "\" " . $truckmark . " >" . get_torrent_truckmark_state($CURUSER['id'], $id) . "</a><br />";
				//小货车结束
				//认领种子开始
				$act .= " <span>" . get_torrent_claimtor_state($CURUSER['id'], $id) . "</span>";
				//认领种子结束
				$sql = mysql_fetch_array(sql_query("SELECT url FROM torrents WHERE id = $id")) or sqlerr(__FILE__, __LINE__);
				$imdb_id = parse_imdb_id($sql['url']);
				if ($imdb_id && $showextinfo['imdb'] == 'yes' && $CURUSER['showimdb'] != 'no') {
					$thenumbers = $imdb_id;
					$movie = new imdb($thenumbers);
					if ($movie->rating() != 'N/A' && $imdb_id) {
						$act .= " <a href=\"details.php?id=$id&hit=1\"><img style='vertical-align: text-bottom;' title=\"IMDb评分：" . $movie->rating() . "\" src=\"styles/img/imdb.gif\" /> <span style='vertical-align: text-bottom;' >" . $movie->rating() . "</span></a>";
					}
				}

				print("<td width=\"80\" class=\"embedded\" style=\"text-align: right; \" valign=\"middle\">" . $act . "</td>");
				print("</tr></table>");
				//种子进度条
				if ($a > 0) {//显示进度条
					if ($b > 0 or $res11['downloaded'] >= $res12['size'] or $res11['finished'] == 'yes') {
						$d = 100;
					} else {
						$d = $res11['downloaded'] / $res12['size'] * 100;
						if (($res11['downloaded'] / $res12['size'] * 100) > 100)
							$d = 100;
					}
					if ($d < 100 && $res11['last_action'] >= $deltime) {//未下载完成种子，并且最后更新时间>=$deltime
						print("<table style='width:100%; margin-top:5px; background-color:transparent'><tr><td class='embedded'><div class='progressarea' title='已上传：" . mksize($res11['uploaded']) . "  已下载：" . mksize($res11['downloaded']) . "  已完成：" . round($d, 2) . "%' style='border: 1px #808080 solid;'><div  style='background-color:#36648B;height:2px;text-align:center;font-size:2px;width:" . round($d, 2) . "%;'></div></div></td></tr></tbody></table>");
					}
				}
				print("</td>");

				if ($wait) {
					$elapsed = floor((TIMENOW - strtotime($row["added"])) / 3600);
					if ($elapsed < $wait) {
						$color = dechex(floor(127 * ($wait - $elapsed) / 48 + 128) * 65536);
						print("<td id=\"torrents_td\" class=\"rowfollow nowrap\"><a href=\"faq.php\"><font color=\"" . $color . "\">" . number_format($wait - $elapsed) . $lang_functions['text_h'] . "</font></a></td>");
					} else
						print("<td id=\"torrents_td\" class=\"rowfollow nowrap\">" . $lang_functions['text_none'] . "</td>");
				}

				//numfiles
				$numfiles = mysql_fetch_array(sql_query("SELECT numfiles FROM torrents WHERE id=" . $id));
				print("<td id=\"torrents_td\" class=\"rowfollow\">" . $numfiles['numfiles'] . "</td>");

				if ($CURUSER['showcomnum'] != 'no') {
					print("<td id=\"torrents_td\" class=\"rowfollow\">");
					if (!$row["comments"]) {
						print("<a href=\"comment.php?action=add&amp;pid=" . $id . "&amp;type=torrent\" title=\"" . $lang_functions['title_add_comments'] . "\">" . $row["comments"] . "</a>");
					} else {
						if ($enabletooltip_tweak == 'yes' && $CURUSER['showlastcom'] != 'no') {
							if (!$lastcom = $Cache->get_value('torrent_' . $id . '_last_comment_content')) {
								$res2 = sql_query("SELECT user, added, text FROM comments WHERE torrent = $id ORDER BY id DESC LIMIT 1");
								$lastcom = mysql_fetch_array($res2);
								$Cache->cache_value('torrent_' . $id . '_last_comment_content', $lastcom, 1855);
							}
							$timestamp = strtotime($lastcom["added"]);
							$hasnewcom = ($lastcom['user'] != $CURUSER['id'] && $timestamp >= $last_browse);
							if ($lastcom) {
								if ($CURUSER['timetype'] != 'timealive')
									$lastcomtime = $lang_functions['text_at_time'] . $lastcom['added'];
								else
									$lastcomtime = $lang_functions['text_blank'] . gettime($lastcom["added"], true, false, true);
								$lastcom_tooltip[$counter]['id'] = "lastcom_" . $counter;
								$lastcom_tooltip[$counter]['content'] = ($hasnewcom ? "<b>(<font class='new'>" . $lang_functions['text_new_uppercase'] . "</font>)</b> " : "") . $lang_functions['text_last_commented_by'] . get_username($lastcom['user']) . $lastcomtime . "<br />" . format_comment(mb_substr($lastcom['text'], 0, 100, "UTF-8") . (mb_strlen($lastcom['text'], "UTF-8") > 100 ? " ......" : "" ), true, false, false, true, 600, false, false);
								$onmouseover = "onmouseover=\"domTT_activate(this, event, 'content', document.getElementById('" . $lastcom_tooltip[$counter]['id'] . "'), 'trail', false, 'delay', 500,'lifetime',3000,'fade','both','styleClass','nicetitle','fadeMax', 98,'maxWidth', 400);\"";
							}
						} else {
							$hasnewcom = false;
							$onmouseover = "";
						}
						print("<b><a href=\"details.php?id=" . $id . "&amp;hit=1&amp;cmtpage=1#startcomments\" " . $onmouseover . ">" . ($hasnewcom ? "<font class='new'>" : "") . $row["comments"] . ($hasnewcom ? "</font>" : "") . "</a></b>");
					}
					print("</td>");
				}

				$time = gettime($row["added"], false, true);
				print("<td id=\"torrents_td\" class=\"rowfollow nowrap\">" . $time . "</td>");

				//size
				print("<td id=\"torrents_td\" class=\"rowfollow\">" . mksize_compact($row["size"]) . "</td>");

				if ($row["seeders"]) {
					$ratio = ($row["leechers"] ? ($row["seeders"] / $row["leechers"]) : 1);
					$ratiocolor = get_slr_color($ratio);
					print("<td id=\"torrents_td\" class=\"rowfollow\"><b><a href=\"details.php?id=" . $id . "&amp;hit=1&amp;dllist=1#seeders\">" . ($ratiocolor ? "<font color=\"" . $ratiocolor . "\">" . number_format($row["seeders"]) . "</font>" : number_format($row["seeders"])) . "</a></b></td>");
				} else
					print("<td id=\"torrents_td\" class=\"rowfollow\"><span class=\"" . linkcolor($row["seeders"]) . "\">" . number_format($row["seeders"]) . "</span></td>");

				if ($row["leechers"]) {
					print("<td id=\"torrents_td\" class=\"rowfollow\"><b><a href=\"details.php?id=" . $id . "&amp;hit=1&amp;dllist=1#leechers\">" . number_format($row["leechers"]) . "</a></b></td>");
				} else
					print("<td id=\"torrents_td\" class=\"rowfollow\">0</td>");

				if ($row["times_completed"] >= 1)
					print("<td id=\"torrents_td\" class=\"rowfollow\"><a href=\"viewsnatches.php?id=" . $row[id] . "\"><b>" . number_format($row["times_completed"]) . "</b></a></td>");
				else
					print("<td id=\"torrents_td\" class=\"rowfollow\">" . number_format($row["times_completed"]) . "</td>");

				if ($row["anonymous"] == "yes") {
					if (get_user_class() >= $torrentmanage_class) {
						print("<td id=\"torrents_td\" class=\"rowfollow\"><i>" . $lang_functions['text_anonymous'] . "</i><br />" . (isset($row["owner"]) ? "(" . get_username($row["owner"]) . ")" : "<i>" . $lang_functions['text_orphaned'] . "</i>") . "</td>");
					} else {
						print("<td id=\"torrents_td\" class=\"rowfollow\"><i>" . $lang_functions['text_anonymous'] . "</i></td>");
					}
				} else {
					print("<td id=\"torrents_td\" class=\"rowfollow\">" . (isset($row["owner"]) ? get_username($row["owner"]) : "<i>" . $lang_functions['text_orphaned'] . "</i>") . "</td>");
				}
				//认领数据展示--开始
				if (preg_match("/viewclaim/i", $script_name)) {
					$claim = mysql_fetch_array(sql_query("SELECT * FROM claim WHERE torrentid = $row[id] AND userid = $CURUSER[id]"));
					$userid = $claim['userid'];
					$torrentid = $claim['torrentid'];
					$snat = mysql_fetch_array(sql_query("SELECT SUM(uploaded) AS uploaded, SUM(seedtime) AS seedtime FROM snatched WHERE torrentid = $torrentid AND userid = $userid")); //计算该用户该种子的上传量和做种时间
					$upload = $snat['uploaded'] - $claim['uploaded'];
					$seedtime = $snat['seedtime'] - $claim['seedtime'];
					print("<td id=\"torrents_td\" class=\"rowfollow\">" . mksize($upload) . "</td>");
					print("<td id=\"torrents_td\" class=\"rowfollow\">" . mkprettytime($seedtime) . "</td>");
					print("<td id=\"torrents_td\" class=\"rowfollow\">" . $claim['added'] . "</td>");
				}
				//认领数据展示--结束
				print("<td id=\"torrents_td\" class=\"rowfollow\"><input type=\"checkbox\" value=\"{$row['id']}\" name=\"id[]\" class=\"checkbox\"></td>");
				print("</tr></tbody>");
				$counter++;
			}
			print("<tr><td id=\"torrents_td\" class=\"rowfollow\" colspan=\"20\"><input type=\"button\" value=\"全选\" onclick=\"checkAll();\">");
			print("<input type=\"button\" value=\"反选\" onclick=\"reverseCheck();\">");
			print(" | <input type=\"submit\" id=\"delete\" value=\"删除\">");
			print("</table>");
			if ($CURUSER['appendpromotion'] == 'highlight') {
				if ($ratioless == 'no') {
					print("<p align=\"center\"> " . $lang_functions['text_promoted_torrents_note'] . "</p>");
				} else {
					print("<p align=\"center\"> " . $lang_functions['text_promoted_torrents_note_ratioless'] . "</p>");
				}
			}

			if ($enabletooltip_tweak == 'yes' && (!isset($CURUSER) || $CURUSER['showlastcom'] == 'yes'))
				create_tooltip_container($lastcom_tooltip, 400);
			create_tooltip_container($torrent_tooltip, 500);
		}
