<?php

if (!defined('IN_TRACKER'))
	die('Hacking attempt!');
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);
include_once($rootpath . 'classes/class_cache.php'); //Require the caching class
$Cache = NEW CACHE(); //Load the caching class
$Cache->setLanguageFolderArray(get_langfolder_list());
define('TIMENOW', time());
$USERUPDATESET = array();
$query_name = array();

define("UC_PEASANT", 1); //0
define("UC_USER", 2); //1
define("UC_POWER_USER", 3); //2
define("UC_ELITE_USER", 4); //3
define("UC_CRAZY_USER", 5); //4
define("UC_INSANE_USER", 6); //5
define("UC_VETERAN_USER", 7); //6
define("UC_EXTREME_USER", 8); //7
define("UC_ULTIMATE_USER", 9); //8
define("UC_NEXUS_MASTER", 10); //9
define("UC_VIP", 11); //10
define("UC_RETIREE", 12); //1
define("UC_DOWNLOADER", 13); //12
define("UC_FORWARD", 14);
define("UC_UPLOADER", 15); //13
define("UC_MODERATOR", 16); //14
define("UC_ADMINISTRATOR", 17); //15
define("UC_SYSOP", 17); //15
define("UC_STAFFLEADER", 18); //16
ignore_user_abort(1);
@set_time_limit(60);

function strip_magic_quotes($arr) {
	foreach ($arr as $k => $v) {
		if (is_array($v)) {
			$arr[$k] = strip_magic_quotes($v);
		} else {
			$arr[$k] = stripslashes($v);
		}
	}
	return $arr;
}

if (function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) {
	if (!empty($_GET)) {
		$_GET = strip_magic_quotes($_GET);
	}
	if (!empty($_POST)) {
		$_POST = strip_magic_quotes($_POST);
	}
	if (!empty($_COOKIE)) {
		$_COOKIE = strip_magic_quotes($_COOKIE);
	}
}

function get_langfolder_list() {
	//do not access db for speed up, or for flexibility
	return array("en", "chs");
}
