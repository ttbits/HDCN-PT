<?php

# IMPORTANT: Do not edit below unless you know what you are doing!
if (!defined('IN_TRACKER'))
	die('Hacking attempt!');
include_once($rootpath . 'include/globalfunctions.php');
include_once($rootpath . 'include/functions_custom.php');
include_once($rootpath . 'include/functions_torrents.php');
include_once($rootpath . 'include/functions_split.php');
include_once($rootpath . 'include/functions_stdhead.php');
include_once($rootpath . 'include/functions_get_user_class_name.php');
include_once($rootpath . 'include/config.php');
include_once($rootpath . 'classes/class_advertisement.php');
require_once($rootpath . get_langfile_path("functions.php"));

function get_langfolder_cookie() {
	global $deflang;
	if (!isset($_COOKIE["c_lang_folder"])) {
		return $deflang;
	} else {
		$langfolder_array = get_langfolder_list();
		foreach ($langfolder_array as $lf) {
			if ($lf == $_COOKIE["c_lang_folder"])
				return $_COOKIE["c_lang_folder"];
		}
		return $deflang;
	}
}

function get_user_lang($user_id) {
	$lang = mysql_fetch_assoc(sql_query("SELECT site_lang_folder FROM language LEFT JOIN users ON language.id = users.lang WHERE language.site_lang = 1 AND users.id = " . sqlesc($user_id) . " LIMIT 1")) or $user_id;
	return $lang['site_lang_folder'];
}

function get_langfile_path($script_name = "", $target = false, $lang_folder = "") {
	global $CURLANGDIR;
	$CURLANGDIR = get_langfolder_cookie();
	if ($lang_folder == "") {
		$lang_folder = $CURLANGDIR;
	}
	return "lang/" . ($target == false ? $lang_folder : "_target") . "/lang_" . ( $script_name == "" ? substr(strrchr($_SERVER['SCRIPT_NAME'], '/'), 1) : $script_name);
}

function get_row_count($table, $suffix = "") {
	$r = sql_query("SELECT COUNT(*) FROM $table $suffix") or sqlerr(__FILE__, __LINE__);
	$a = mysql_fetch_row($r) or die(mysql_error());
	return $a[0];
}

function get_row_sum($table, $field, $suffix = "") {
	$r = sql_query("SELECT SUM($field) FROM $table $suffix") or sqlerr(__FILE__, __LINE__);
	$a = mysql_fetch_row($r) or die(mysql_error());
	return $a[0];
}

function get_single_value($table, $field, $suffix = "") {
	$r = sql_query("SELECT $field FROM $table $suffix LIMIT 1") or sqlerr(__FILE__, __LINE__);
	$a = mysql_fetch_row($r) or die(mysql_error());
	if ($a) {
		return $a[0];
	} else {
		return false;
	}
}

function stdmsg($heading, $text, $htmlstrip = false) {
	if ($htmlstrip) {
		$heading = htmlspecialchars(trim($heading));
		$text = htmlspecialchars(trim($text));
	}
	print("<table align = \"center\" class=\"main\" width=\"500\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td class=\"embeddeds\">\n");
	if ($heading)
		print("<h2>" . $heading . "</h2>\n");
	print("<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"10\"><tr><td class=\"text\">");
	print($text . "</td></tr></table></td></tr></table>\n");
}

function stderr($heading, $text, $htmlstrip = true, $head = true, $foot = true, $die = true) {
	if ($head)
		stdhead();
	stdmsg($heading, $text, $htmlstrip);
	if ($foot)
		stdfoot();
	if ($die)
		die;
}

function sqlerr($file = '', $line = '') {
	print("<table border=\"0\" bgcolor=\"blue\" align=\"left\" cellspacing=\"0\" cellpadding=\"10\" style=\"background: blue;\">" .
			"<tr><td class=\"embedded\"><font color=\"white\"><h1>SQL Error</h1>\n" .
			"<b>" . mysql_error() . ($file != '' && $line != '' ? "<p>in $file, line $line</p>" : "") . "</b></font></td></tr></table>");
	die;
}

function format_quotes($s) {
	global $lang_functions;
	preg_match_all('/\\[quote.*?\\]/i', $s, $result, PREG_PATTERN_ORDER);
	$openquotecount = count($openquote = $result[0]);
	preg_match_all('/\\[\/quote\\]/i', $s, $result, PREG_PATTERN_ORDER);
	$closequotecount = count($closequote = $result[0]);

	if ($openquotecount != $closequotecount) {
		return $s; // quote mismatch. Return raw string...
	}
// Get position of opening quotes
	$openval = array();
	$pos = -1;

	foreach ($openquote as $val) {
		$openval[] = $pos = strpos($s, $val, $pos + 1);
	}

// Get position of closing quotes
	$closeval = array();
	$pos = -1;
	foreach ($closequote as $val) {
		$closeval[] = $pos = strpos($s, $val, $pos + 1);
	}
	for ($i = 0; $i < count($openval); $i++) {
		if ($openval[$i] > $closeval[$i])
			return $s; // Cannot close before opening. Return raw string...
	}
	$s = preg_replace("/\\[quote\\]/i", "<fieldset><legend> " . $lang_functions['text_quote'] . " </legend>", $s);
	$s = preg_replace("/\\[quote=(.+?)\\]/i", "<fieldset><legend> " . $lang_functions['text_quote'] . "：\\1 </legend>", $s);
	$s = preg_replace("/\\[\\/quote\\]/i", "</fieldset><br />", $s);
	return $s;
}

function print_attachment($dlkey, $enableimage = true, $imageresizer = true) {
	global $Cache, $httpdirectory_attachment;
	global $lang_functions;
	if (strlen($dlkey) == 32) {
		if (!$row = $Cache->get_value('attachment_' . $dlkey . '_content')) {
			$res = sql_query("SELECT * FROM attachments WHERE dlkey = " . sqlesc($dlkey) . " LIMIT 1") or sqlerr(__FILE__, __LINE__);
			$row = mysql_fetch_array($res);
			$Cache->cache_value('attachment_' . $dlkey . '_content', $row, 86400);
		}
	}
	if (!$row) {
		return "<div style=\"text-decoration: line-through; font-size: 7pt\">" . $lang_functions['text_attachment_key'] . $dlkey . $lang_functions['text_not_found'] . "</div>";
	} else {
		$id = $row['id'];
		if ($row['isimage'] == 1) {
			if ($enableimage) {
				if ($row['thumb'] == 1) {
					$url = $httpdirectory_attachment . "/" . $row['location'] . ".thumb.jpg";
				} else {
					$url = $httpdirectory_attachment . "/" . $row['location'];
				}
				if ($imageresizer == true)
					$onclick = " onclick=\"Previewurl('" . $httpdirectory_attachment . "/" . $row['location'] . "')\"";
				else
					$onclick = "";
				$return = "<img id=\"attach" . $id . "\" alt=\"" . htmlspecialchars($row['filename']) . "\" src=\"" . $url . "\"" . $onclick . " onmouseover=\"domTT_activate(this, event, 'content', '" . htmlspecialchars("<strong>" . $lang_functions['text_size'] . "</strong>: " . mksize($row['filesize']) . "<br />" . gettime($row['added'])) . "', 'styleClass', 'attach', 'x', findPosition(this)[0], 'y', findPosition(this)[1]-58);\" />";
			} else
				$return = "";
		}
		else {
			switch ($row['filetype']) {
				case 'application/x-bittorrent': {
						$icon = "<img alt=\"torrent\" src=\"pic/attachicons/torrent.gif\" />";
						break;
					}
				case 'application/zip': {
						$icon = "<img alt=\"zip\" src=\"pic/attachicons/archive.gif\" />";
						break;
					}
				case 'application/rar': {
						$icon = "<img alt=\"rar\" src=\"pic/attachicons/archive.gif\" />";
						break;
					}
				case 'application/x-7z-compressed': {
						$icon = "<img alt=\"7z\" src=\"pic/attachicons/archive.gif\" />";
						break;
					}
				case 'application/x-gzip': {
						$icon = "<img alt=\"gzip\" src=\"pic/attachicons/archive.gif\" />";
						break;
					}
				case 'audio/mpeg': {

					}
				case 'audio/ogg': {
						$icon = "<img alt=\"audio\" src=\"pic/attachicons/audio.gif\" />";
						break;
					}
				case 'video/x-flv': {
						$icon = "<img alt=\"flv\" src=\"pic/attachicons/flv.gif\" />";
						break;
					}
				default: {
						$icon = "<img alt=\"other\" src=\"pic/attachicons/common.gif\" />";
					}
			}
			$return = "<div class=\"attach\">" . $icon . "&nbsp;&nbsp;<a href=\"" . htmlspecialchars("getattachment.php?id=" . $id . "&dlkey=" . $dlkey) . "\" target=\"_blank\" id=\"attach" . $id . "\" onmouseover=\"domTT_activate(this, event, 'content', '" . htmlspecialchars("<strong>" . $lang_functions['text_downloads'] . "</strong>: " . number_format($row['downloads']) . "<br />" . gettime($row['added'])) . "', 'styleClass', 'attach', 'x', findPosition(this)[0], 'y', findPosition(this)[1]-58);\">" . htmlspecialchars($row['filename']) . "</a>&nbsp;&nbsp;<font class=\"size\">(" . mksize($row['filesize']) . ")</font></div>";
		}
		return $return;
	}
}

function addTempCode($value) {
	global $tempCode, $tempCodeCount;
	$tempCode[$tempCodeCount] = $value;
	$return = "<tempCode_$tempCodeCount>";
	$tempCodeCount++;
	return $return;
}

function formatAdUrl($adid, $url, $content, $newWindow = true) {
	return formatUrl("adredir.php?id=" . $adid . "&amp;url=" . rawurlencode($url), $newWindow, $content);
}

function formatUrl($url, $newWindow = false, $text = '', $linkClass = '') {
	if (!$text) {
		$text = $url;
	}
	return addTempCode("<a" . ($linkClass ? " class=\"$linkClass\"" : '') . " href=\"$url\"" . ($newWindow == true ? " target=\"_blank\"" : "") . ">$text</a>");
}

function formatCode($text) {
	global $lang_functions;
	return addTempCode("<br /><div class=\"codetop\">" . $lang_functions['text_code'] . "</div><div class=\"codemain\">$text</div><br />");
}

function formatImg($src, $enableImageResizer, $image_max_width, $image_max_height) {
	/*
	  if (preg_match('/highbd/i', $src))
	  return addTempCode("<img alt=\"image\" src=\"pic/trans.gif\" data-echo=\"img.php?mm=$src\"" . ($enableImageResizer ? " onload=\"Scale(this,$image_max_width,$image_max_height);\" onclick=\"Preview(this);\"" : "") . " />");
	  else
	 *
	 */
	$script_name = $_SERVER["SCRIPT_FILENAME"];
	if (!preg_match("/fun/i", $script_name)) {
		return addTempCode("<img alt=\"image\" src=\"pic/trans.gif\" data-echo=\"$src\"" . ($enableImageResizer ? " onload=\"Scale(this,$image_max_width,$image_max_height);\" onclick=\"Preview(this);\"" : "") . " />");
	} else {
		return addTempCode("<img alt=\"image\" src=\"$src\"" . ($enableImageResizer ? " onload=\"Scale(this,$image_max_width,$image_max_height);\" onclick=\"Preview(this);\"" : "") . " />");
	}
}

function formatFlash($src, $width, $height) {
	if (!$width) {
		$width = 500;
	}
	if (!$height) {
		$height = 300;
	}
	return addTempCode("<object width=\"$width\" height=\"$height\"><param name=\"movie\" value=\"$src\" /><embed src=\"$src\" width=\"$width\" height=\"$height\" type=\"application/x-shockwave-flash\"></embed></object>");
	//return addTempCode("<embed src=\"$src\" width=\"$width\" height=\"$height\" type=\"application/x-shockwave-flash\" />");
}

/*
  function formatFlv($src, $width, $height) {
  if (!$width) {
  $width = 320;
  }
  if (!$height) {
  $height = 240;
  }
  return addTempCode("<object width=\"$width\" height=\"$height\"><param name=\"movie\" value=\"flvplayer.swf?file=$src\" /><param name=\"allowFullScreen\" value=\"true\" /><embed src=\"flvplayer.swf?file=$src\" type=\"application/x-shockwave-flash\" allowfullscreen=\"true\" width=\"$width\" height=\"$height\"></embed></object>");
  }
 *
 */

function format_urls($text, $newWindow = false) {
	global $BASEURL;
	if (preg_match("/" . $BASEURL . "/", $text)) {
		return preg_replace("/((http|https|ftp|gopher|news|telnet|mms|rtsp):\/\/[^()\[\]<>\s]+)/ei", "formatUrl('\\1', " . ($newWindow == true ? 1 : 0) . ", '', 'faqlink')", $text);
	} else {
		return preg_replace("/((http|https|ftp|gopher|news|telnet|mms|rtsp):\/\/[^()\[\]<>\s]+)/ei", "formatUrl('\\1', 'true', '', 'faqlink')", $text);
	}
}

function format_comment($text, $strip_html = true, $xssclean = false, $newtab = false, $imageresizer = true, $image_max_width = 950, $enableimage = true, $enableflash = true, $imagenum = -1, $image_max_height = 0, $adid = 0) {
	global $SITENAME, $BASEURL, $imgReplaceCount;
	global $tempCode, $tempCodeCount;
	$tempCode = array();
	$tempCodeCount = 0;
	$imageresizer = $imageresizer ? 1 : 0;
	$s = $text;
	if ($strip_html) {
		$s = htmlspecialchars($s);
	}
// Linebreaks
	$s = nl2br($s);
	if (strpos($s, "[code]") !== false && strpos($s, "[/code]") !== false) {
		$s = preg_replace("/\[code\](.+?)\[\/code\]/eis", "formatCode('\\1')", $s);
	}
	$originalBbTagArray = array('[siteurl]', '[site]', '[*]', '[b]', '[/b]', '[i]', '[/i]', '[u]', '[/u]', '[pre]', '[/pre]', '[/color]', '[/font]', '[/size]', "[ul]", "[/ul]", "[ol]", "[/ol]", "[li]", "[/li]", "[s]", "[/s]", "[sub]", "[/sub]", "[sup]", "[/sup]", "[hr]", "[table]", "[/table]", "[tr]", "[/tr]", "[td]", "[/td]", "[br]");
	$replaceXhtmlTagArray = array(get_protocol_prefix() . $BASEURL, $SITENAME, '<img class="listicon listitem" src="pic/trans.gif" alt="list" />', '<b>', '</b>', '<i>', '</i>', '<u>', '</u>', '<pre>', '</pre>', '</span>', '</font>', '</font>', '<ul>', '</ul>', '<ol>', '</ol>', '<li>', '</li>', '<s>', '</s>', '<sub>', '</sub>', '<sup>', '</sup>', '<hr>', '<table>', '</table>', '<tr>', '</tr>', '<td>', '</td>', '<br />');
	$s = str_replace($originalBbTagArray, $replaceXhtmlTagArray, $s);

	$originalBbTagArray = array("/\[font=\s*(.*?)\s*\]/i", "/\[color=\s*(.*?)\s*\]/is", "/\[size=([1-7])\]/is", "/\[left\]/i", "/\[\/left\]/i", "/\[right\]/i", "/\[\/right\]/i", "/\[center\]/i", "/\[\/center\]/i");
	$replaceXhtmlTagArray = array("<font face=\"\\1;\">", "<span style=\"color: \\1;\">", "<font size=\"\\1;\">", "<div align=\"left\">", "</div>", "<div align=\"right\">", "</div>", "<div align=\"center\">", "</div>");
	$s = preg_replace($originalBbTagArray, $replaceXhtmlTagArray, $s);

	//if ($enableattach_attachment == 'yes' && $imagenum != 1) {
	if ($imagenum != 1) {
		$limit = 20;
		$s = preg_replace("/\[attach\]([0-9a-zA-z]*)\[\/attach\]/ies", "print_attachment('\\1', " . ($enableimage ? 1 : 0) . ", " . ($imageresizer ? 1 : 0) . ")", $s, $limit);
	}

	if ($enableimage) {
		$s = preg_replace("/\[img\]([^\<\r\n\"']+?)\[\/img\]/ei", "formatImg('\\1', " . $imageresizer . ", " . $image_max_width . ", " . $image_max_height . ")", $s, $imagenum, $imgReplaceCount);
		$s = preg_replace("/\[img=(\d*)\,(\d*)\]([^\<\r\n\"']+?)\[\/img\]/ei", "formatImg('\\3', " . $imageresizer . ", '\\1', '\\2')", $s, ($imagenum != -1 ? max($imagenum - $imgReplaceCount, 0) : -1));
	} else {
		$s = preg_replace("/\[img\]([^\<\r\n\"']+?)\[\/img\]/i", '', $s, -1);
		$s = preg_replace("/\[img=(\d*)\,(\d*)\]([^\<\r\n\"']+?)\[\/img\]/i", '\\3', $s, -1);
	}

// [flash,500,400]http://www/image.swf[/flash]
	if (strpos($s, "[flash") !== false) { //flash is not often used. Better check if it exist before hand
		if ($enableflash) {
			$s = preg_replace("/\[flash\]([^\<\r\n\"']+?)\[\/flash\]/ei", "formatFlash('\\1')", $s);
			$s = preg_replace("/\[flash=([1-9][0-9]*)\,([1-9][0-9]*)\]([^\<\r\n\"']+?)\[\/flash\]/ei", "formatFlash('\\3', '\\1', '\\2')", $s);
		} else {
			$s = preg_replace("/\[flash\]([^\<\r\n\"']+?)\[\/flash\]/i", '', $s);
			$s = preg_replace("/\[flash=([1-9][0-9]*)\,([1-9][0-9]*)\]([^\<\r\n\"']+?)\[\/flash\]/i", '\\3', $s);
		}
	}
	/*
	  //[flv,320,240]http://www/a.flv[/flv]
	  if (strpos($s, "[flv") !== false) { //flv is not often used. Better check if it exist before hand
	  if ($enableflash) {
	  $s = preg_replace("/\[flv\]([^\<\r\n\"']+?)\[\/flv\]/ei", "formatFlv('\\1')", $s);
	  $s = preg_replace("/\[flv=([1-9][0-9]*)\,([1-9][0-9]*)\]([^\<\r\n\"']+?)\[\/flv\]/ei", "formatFlv('\\3', '\\1', '\\2')", $s);
	  } else {
	  $s = preg_replace("/\[flv\]([^\<\r\n\"']+?)\[\/flv\]/i", '', $s);
	  $s = preg_replace("/\[flv=([1-9][0-9]*)\,([1-9][0-9]*)\]([^\<\r\n\"']+?)\[\/flv\]/i", '\\3', $s);
	  }
	  }
	 *
	 */

// [url=http://www.example.com]Text[/url]
	if ($adid) {
		$s = preg_replace("/\[url=([^\[\s]+?)\](.+?)\[\/url\]/ei", "formatAdUrl(" . $adid . " ,'\\1', '\\2', " . ($newtab == true ? 1 : 0) . ", 'faqlink')", $s);
	} else {
		$s = preg_replace("/\[url=([^\[\s]+?)\](.+?)\[\/url\]/ei", "formatUrl('\\1', true, '\\2', 'faqlink')", $s);
	}

// [url]http://www.example.com[/url]
	$s = preg_replace("/\[url\]([^\[\s]+?)\[\/url\]/ei", "formatUrl('\\1', true, '', 'faqlink')", $s);

	$s = format_urls($s, $newtab);
// Quotes
	if (strpos($s, "[quote") !== false && strpos($s, "[/quote]") !== false) { //format_quote is kind of slow. Better check if [quote] exists beforehand
		$s = format_quotes($s);
	}
	/* 加入@格式化 */

	if (strpos($s, "[@") !== false) {
		$s = at_format($s);
	}

	$s = preg_replace("/\[em([1-9][0-9]*)\]/ie", "(\\1 < 77 ? '<img style=\"width:64px;height:64px;padding-bottom:auto;\" src=\"pic/smilies/\\1.png\" alt=\"[em\\1]\" />' : '[em\\1]')", $s);
	reset($tempCode);
	$j = 0;
	while (count($tempCode) || $j > 5) {
		foreach ($tempCode as $key => $code) {
			$s = str_replace("<tempCode_$key>", $code, $s, $count);
			if ($count) {
				unset($tempCode[$key]);
				$i = $i + $count;
			}
		}
		$j++;
	}
	return $s;
}

function highlight($search, $subject, $hlstart = '<b><font class="striking">', $hlend = "</font></b>") {

	$srchlen = strlen($search); // lenght of searched string
	if ($srchlen == 0)
		return $subject;
	$find = $subject;
	while ($find = stristr($find, $search)) { // find $search text in $subject -case insensitiv
		$srchtxt = substr($find, 0, $srchlen); // get new search text
		$find = substr($find, $srchlen);
		$subject = str_replace($srchtxt, "$hlstart$srchtxt$hlend", $subject); // highlight founded case insensitive search text
	}
	return $subject;
}

function get_user_class() {
	global $CURUSER;
	return $CURUSER["class"];
}

function is_valid_user_class($class) {
	return is_numeric($class) && floor($class) == $class && $class >= UC_PEASANT && $class <= UC_STAFFLEADER;
}

function int_check($value, $stdhead = true, $stdfood = true, $die = true, $log = true) {
	global $lang_functions;
	global $CURUSER;
	if (is_array($value)) {
		foreach ($value as $val)
			int_check($val);
	} else {
		if (!is_valid_id($value)) {
			$msg = "无效的ID：用户名： " . $CURUSER["username"] . " - 用户ID： " . $CURUSER["id"] . " - 用户IP：" . getip();
			if ($log)
				write_log($msg, 'mod');

			if ($stdhead)
				stderr($lang_functions['std_error'], $lang_functions['std_invalid_id']);
			else {
				print ("<h2>" . $lang_functions['std_error'] . "</h2><table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"10\"><tr><td class=\"text\">");
				print ($lang_functions['std_invalid_id'] . "</td></tr></table>");
			}
			if ($stdfood)
				stdfoot();
			if ($die)
				die;
		} else
			return true;
	}
}

function is_valid_id($id) {
	return is_numeric($id) && ($id > 0) && (floor($id) == $id);
}

//-------- Begins a main frame
function begin_main_frame($caption = "", $center = false, $width = 95) {
	$tdextra = "";
	if ($caption) {
		print("<h2>" . $caption . "</h2>");
	}
	if ($center) {
		$tdextra .= " align=\"center\"";
	}
	print("<table class=\"main\" width=\"" . $width . "%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" . "<tr><td class=\"embeddeds\" $tdextra>");
}

function end_main_frame() {
	print("</td></tr></table>\n");
}

function begin_frame($caption = "", $center = false, $padding = 10, $width = "100%", $caption_center = "left") {
	$tdextra = "";

	if ($center)
		$tdextra .= " align=\"center\"";

	print(($caption ? "<h2 align=\"" . $caption_center . "\">" . $caption . "</h2>" : "") . "<table width=\"" . $width . "\" border=\"1\" cellspacing=\"0\" cellpadding=\"" . $padding . "\">" . "<tr><td class=\"text\" $tdextra>\n");
}

function end_frame() {
	print("</td></tr></table>\n");
}

function begin_table($fullwidth = false, $padding = 5) {
	$width = "";

	if ($fullwidth)
		$width .= " width=50%";
	print("<table class=\"main" . $width . "\" border=\"1\" cellspacing=\"0\" cellpadding=\"" . $padding . "\">");
}

function end_table() {
	print("</table>\n");
}

//-------- Inserts a smilies frame
//         (move to globals)

function insert_smilies_frame() {
	global $lang_functions;
	begin_frame($lang_functions['text_smilies'], true);
	begin_table(false, 5);
	print("<tr><td class=\"colhead\">" . $lang_functions['col_type_something'] . "</td><td class=\"colhead\">" . $lang_functions['col_to_make_a'] . "</td></tr>\n");
	for ($i = 1; $i < 77; $i++) {
		print("<tr><td>[em$i]</td><td><img style=\"width:64px;height:64px;padding-bottom:auto;\" src=\"pic/smilies/" . $i . ".png\" alt=\"[em$i]\" /></td></tr>\n");
	}
	end_table();
	end_frame();
}

function get_ratio_color($ratio) {
	if ($ratio < 0.1)
		return "#ff0000";
	if ($ratio < 0.2)
		return "#ee0000";
	if ($ratio < 0.3)
		return "#dd0000";
	if ($ratio < 0.4)
		return "#cc0000";
	if ($ratio < 0.5)
		return "#bb0000";
	if ($ratio < 0.6)
		return "#aa0000";
	if ($ratio < 0.7)
		return "#990000";
	if ($ratio < 0.8)
		return "#880000";
	if ($ratio < 0.9)
		return "#770000";
	if ($ratio < 1)
		return "#660000";
	return "";
}

function get_slr_color($ratio) {
	if ($ratio < 0.025)
		return "#ff0000";
	if ($ratio < 0.05)
		return "#ee0000";
	if ($ratio < 0.075)
		return "#dd0000";
	if ($ratio < 0.1)
		return "#cc0000";
	if ($ratio < 0.125)
		return "#bb0000";
	if ($ratio < 0.15)
		return "#aa0000";
	if ($ratio < 0.175)
		return "#990000";
	if ($ratio < 0.2)
		return "#880000";
	return "";
}

function write_log($text, $security = "normal") {
	/*
	 * normal 非机密
	 * mod 机密
	 */
	$text = sqlesc($text);
	$added = sqlesc(date("Y-m-d H:i:s"));
	$security = sqlesc($security);
	sql_query("INSERT INTO sitelog (added, txt, security_level) VALUES ($added, $text, $security)") or sqlerr(__FILE__, __LINE__);
}

function get_elapsed_time($ts, $shortunit = false) {
	global $lang_functions;
	$mins = floor(abs(TIMENOW - $ts) / 60);
	$hours = floor($mins / 60);
	$mins -= $hours * 60;
	$days = floor($hours / 24);
	$hours -= $days * 24;
	$months = floor($days / 30);
	$days2 = $days - $months * 30;
	$years = floor($days / 365);
	$months -= $years * 12;
	$t = "";
	if ($years > 0)
		return $years . ($shortunit ? $lang_functions['text_short_year'] : $lang_functions['text_year'] . add_s($year)) . "&nbsp;" . $months . ($shortunit ? $lang_functions['text_short_month'] : $lang_functions['text_month'] . add_s($months));
	if ($months > 0)
		return $months . ($shortunit ? $lang_functions['text_short_month'] : $lang_functions['text_month'] . add_s($months)) . "&nbsp;" . $days2 . ($shortunit ? $lang_functions['text_short_day'] : $lang_functions['text_day'] . add_s($days2));
	if ($days > 0)
		return $days . ($shortunit ? $lang_functions['text_short_day'] : $lang_functions['text_day'] . add_s($days)) . "&nbsp;" . $hours . ($shortunit ? $lang_functions['text_short_hour'] : $lang_functions['text_hour'] . add_s($hours));
	if ($hours > 0)
		return $hours . ($shortunit ? $lang_functions['text_short_hour'] : $lang_functions['text_hour'] . add_s($hours)) . "&nbsp;" . $mins . ($shortunit ? $lang_functions['text_short_min'] : $lang_functions['text_min'] . add_s($mins));
	if ($mins > 0)
		return $mins . ($shortunit ? $lang_functions['text_short_min'] : $lang_functions['text_min'] . add_s($mins));
	return "&lt; 1" . ($shortunit ? $lang_functions['text_short_min'] : $lang_functions['text_min']);
}

/*
  function textbbcode($form, $text, $content = "") {
  global $lang_functions;
  global $BASEURL, $enableattach_attachment;
  ?>
  <script type="text/javascript">
  var b_open = 0;
  var i_open = 0;
  var u_open = 0;
  var color_open = 0;
  var list_open = 0;
  var quote_open = 0;
  var html_open = 0;
  var myAgent = navigator.userAgent.toLowerCase();
  var myVersion = parseInt(navigator.appVersion);
  var is_ie = ((myAgent.indexOf("msie") != -1) && (myAgent.indexOf("opera") == -1));
  var is_nav = ((myAgent.indexOf('mozilla') != -1) && (myAgent.indexOf('spoofer') == -1) && (myAgent.indexOf('compatible') == -1) && (myAgent.indexOf('opera') == -1) && (myAgent.indexOf('webtv') == -1) && (myAgent.indexOf('hotjava') == -1));
  var is_win = ((myAgent.indexOf("win") != -1) || (myAgent.indexOf("16bit") != -1));
  var is_mac = (myAgent.indexOf("mac") != -1);
  var bbtags = new Array();
  function cstat() {
  var c = stacksize(bbtags);
  if ((c < 1) || (c == null)) {
  c = 0;
  }
  if (!bbtags[0]) {
  c = 0;
  }
  document.<?php echo $form ?>.tagcount.value = "Close last, Open " + c;
  }
  function stacksize(thearray) {
  for (i = 0; i < thearray.length; i++) {
  if ((thearray[i] == "") || (thearray[i] == null) || (thearray == 'undefined')) {
  return i;
  }
  }
  return thearray.length;
  }
  function pushstack(thearray, newval) {
  arraysize = stacksize(thearray);
  thearray[arraysize] = newval;
  }
  function popstackd(thearray) {
  arraysize = stacksize(thearray);
  theval = thearray[arraysize - 1];
  return theval;
  }
  function popstack(thearray) {
  arraysize = stacksize(thearray);
  theval = thearray[arraysize - 1];
  delete thearray[arraysize - 1];
  return theval;
  }
  function closeall() {
  if (bbtags[0]) {
  while (bbtags[0]) {
  tagRemove = popstack(bbtags)
  if ((tagRemove != 'color')) {
  doInsert("[/" + tagRemove + "]", "", false);
  eval("document.<?php echo $form ?>." + tagRemove + ".value = ' " + tagRemove + " '");
  eval(tagRemove + "_open = 0");
  } else {
  doInsert("[/" + tagRemove + "]", "", false);
  }
  cstat();
  return;
  }
  }
  document.<?php echo $form ?>.tagcount.value = "Close last, Open 0";
  bbtags = new Array();
  document.<?php echo $form ?>.<?php echo $text ?>.focus();
  }
  function add_code(NewCode) {
  document.<?php echo $form ?>.<?php echo $text ?>.value += NewCode;
  document.<?php echo $form ?>.<?php echo $text ?>.focus();
  }
  function alterfont(theval, thetag) {
  if (theval == 0)
  return;
  if (doInsert("[" + thetag + "=" + theval + "]", "[/" + thetag + "]", true))
  pushstack(bbtags, thetag);
  document.<?php echo $form ?>.color.selectedIndex = 0;
  cstat();
  }

  function tag_url(PromptURL, PromptTitle, PromptError) {
  var FoundErrors = '';
  var enterURL = prompt(PromptURL, "http://");
  var enterTITLE = prompt(PromptTitle, "");
  if (!enterURL || enterURL == "") {
  FoundErrors += " " + PromptURL + ",";
  }
  if (!enterTITLE) {
  FoundErrors += " " + PromptTitle;
  }
  if (FoundErrors) {
  alert(PromptError + FoundErrors);
  return;
  }
  doInsert("[url=" + enterURL + "]" + enterTITLE + "[/url]", "", false);
  }

  function tag_list(PromptEnterItem, PromptError) {
  var FoundErrors = '';
  var enterTITLE = prompt(PromptEnterItem, "");
  if (!enterTITLE) {
  FoundErrors += " " + PromptEnterItem;
  }
  if (FoundErrors) {
  alert(PromptError + FoundErrors);
  return;
  }
  doInsert("[*]" + enterTITLE + "", "", false);
  }

  function tag_image(PromptImageURL, PromptError) {
  var enterURL = prompt(PromptImageURL, "http://");
  if (!enterURL || enterURL == "http://") {
  alert(PromptError + PromptImageURL);
  return;
  }
  doInsert("[img]" + enterURL + "[/img]", "", false);
  }

  function tag_extimage(content) {
  doInsert(content, "", false);
  }

  function tag_email(PromptEmail, PromptError) {
  var emailAddress = prompt(PromptEmail, "");
  if (!emailAddress) {
  alert(PromptError + PromptEmail);
  return;
  }
  doInsert("[email]" + emailAddress + "[/email]", "", false);
  }

  function doInsert(ibTag, ibClsTag, isSingle)
  {
  var isClose = false;
  var obj_ta = document.<?php echo $form ?>.<?php echo $text ?>;
  if ((myVersion >= 4) && is_ie && is_win)
  {
  if (obj_ta.isTextEdit)
  {
  obj_ta.focus();
  var sel = document.selection;
  var rng = sel.createRange();
  rng.colapse;
  if ((sel.type == "Text" || sel.type == "None") && rng != null)
  {
  if (ibClsTag != "" && rng.text.length > 0)
  ibTag += rng.text + ibClsTag;
  else if (isSingle)
  isClose = true;
  rng.text = ibTag;
  }
  }
  else
  {
  if (isSingle)
  isClose = true;
  obj_ta.value += ibTag;
  }
  }
  else if (obj_ta.selectionStart || obj_ta.selectionStart == '0')
  {
  var startPos = obj_ta.selectionStart;
  var endPos = obj_ta.selectionEnd;
  obj_ta.value = obj_ta.value.substring(0, startPos) + ibTag + obj_ta.value.substring(endPos, obj_ta.value.length);
  obj_ta.selectionEnd = startPos + ibTag.length;
  if (isSingle)
  isClose = true;
  }
  else
  {
  if (isSingle)
  isClose = true;
  obj_ta.value += ibTag;
  }
  obj_ta.focus();
  obj_ta.value = obj_ta.value.replace(/ /, " ");
  return isClose;
  }

  function winop()
  {
  windop = window.open("moresmilies.php?form=<?php echo $form ?>&text=<?php echo $text ?>", "mywin", "height=500,width=500,resizable=no,scrollbars=yes");
  }

  function simpletag(thetag)
  {
  var tagOpen = eval(thetag + "_open");
  if (tagOpen == 0) {
  if (doInsert("[" + thetag + "]", "[/" + thetag + "]", true))
  {
  eval(thetag + "_open = 1");
  eval("document.<?php echo $form ?>." + thetag + ".value += '*'");
  pushstack(bbtags, thetag);
  cstat();
  }
  }
  else {
  lastindex = 0;
  for (i = 0; i < bbtags.length; i++) {
  if (bbtags[i] == thetag) {
  lastindex = i;
  }
  }

  while (bbtags[lastindex]) {
  tagRemove = popstack(bbtags);
  doInsert("[/" + tagRemove + "]", "", false)
  if ((tagRemove != 'COLOR')) {
  eval("document.<?php echo $form ?>." + tagRemove + ".value = '" + tagRemove.toUpperCase() + "'");
  eval(tagRemove + "_open = 0");
  }
  }
  cstat();
  }
  }
  </script>
  <table width="100%" cellspacing="0" cellpadding="5" border="0">
  <tr><td align="left" colspan="2">
  <table cellspacing="1" cellpadding="2" border="0">
  <tr>
  <td class="embedded"><input style="font-weight: bold;font-size:11px; margin-right:3px" type="button" name="b" value="加粗" onclick="javascript: simpletag('b')" /></td>
  <td class="embedded"><input class="codebuttons" style="font-style: italic;font-size:11px;margin-right:3px" type="button" name="i" value="斜体" onclick="javascript: simpletag('i')" /></td>
  <td class="embedded"><input class="codebuttons" style="text-decoration: underline;font-size:11px;margin-right:3px" type="button" name="u" value="下划线" onclick="javascript: simpletag('u')" /></td>
  <?php
  print("<td class=\"embedded\"><input class=\"codebuttons\" style=\"font-size:11px;margin-right:3px\" type=\"button\" name='url' value='URL' onclick=\"javascript:tag_url('" . $lang_functions['js_prompt_enter_url'] . "','" . $lang_functions['js_prompt_enter_title'] . "','" . $lang_functions['js_prompt_error'] . "')\" /></td>");
  print("<td class=\"embedded\"><input class=\"codebuttons\" style=\"font-size:11px;margin-right:3px\" type=\"button\" name=\"IMG\" value=\"IMG\" onclick=\"javascript: tag_image('" . $lang_functions['js_prompt_enter_image_url'] . "','" . $lang_functions['js_prompt_error'] . "')\" /></td>");
  print("<td class=\"embedded\"><input type=\"button\" style=\"font-size:11px;margin-right:3px\" name=\"list\" value=\"List\" onclick=\"tag_list('" . addslashes($lang_functions['js_prompt_enter_item']) . "','" . $lang_functions['js_prompt_error'] . "')\" /></td>");
  ?>
  <td class="embedded"><input class="codebuttons" style="font-size:11px;margin-right:3px" type="button" name="quote" value="QUOTE" onclick="javascript: simpletag('quote')" /></td>
  <td class="embedded"><input style="font-size:11px;margin-right:3px" type="button" onclick='javascript:closeall();' name='tagcount' value="Close all tags" /></td>
  <td class="embedded"><select class="med codebuttons" style="margin-right:3px" name='color' onchange="alterfont(this.options[this.selectedIndex].value, 'color')">
  <option value='0'>--- <?php echo $lang_functions['select_color'] ?> ---</option>
  <option style="background-color: black" value="Black">Black</option>
  <option style="background-color: sienna" value="Sienna">Sienna</option>
  <option style="background-color: darkolivegreen" value="DarkOliveGreen">Dark Olive Green</option>
  <option style="background-color: darkgreen" value="DarkGreen">Dark Green</option>
  <option style="background-color: darkslateblue" value="DarkSlateBlue">Dark Slate Blue</option>
  <option style="background-color: navy" value="Navy">Navy</option>
  <option style="background-color: indigo" value="Indigo">Indigo</option>
  <option style="background-color: darkslategray" value="DarkSlateGray">Dark Slate Gray</option>
  <option style="background-color: darkred" value="DarkRed">Dark Red</option>
  <option style="background-color: darkorange" value="DarkOrange">Dark Orange</option>
  <option style="background-color: olive" value="Olive">Olive</option>
  <option style="background-color: green" value="Green">Green</option>
  <option style="background-color: teal" value="Teal">Teal</option>
  <option style="background-color: blue" value="Blue">Blue</option>
  <option style="background-color: slategray" value="SlateGray">Slate Gray</option>
  <option style="background-color: dimgray" value="DimGray">Dim Gray</option>
  <option style="background-color: red" value="Red">Red</option>
  <option style="background-color: sandybrown" value="SandyBrown">Sandy Brown</option>
  <option style="background-color: yellowgreen" value="YellowGreen">Yellow Green</option>
  <option style="background-color: seagreen" value="SeaGreen">Sea Green</option>
  <option style="background-color: mediumturquoise" value="MediumTurquoise">Medium Turquoise</option>
  <option style="background-color: royalblue" value="RoyalBlue">Royal Blue</option>
  <option style="background-color: purple" value="Purple">Purple</option>
  <option style="background-color: gray" value="Gray">Gray</option>
  <option style="background-color: magenta" value="Magenta">Magenta</option>
  <option style="background-color: orange" value="Orange">Orange</option>
  <option style="background-color: yellow" value="Yellow">Yellow</option>
  <option style="background-color: lime" value="Lime">Lime</option>
  <option style="background-color: cyan" value="Cyan">Cyan</option>
  <option style="background-color: deepskyblue" value="DeepSkyBlue">Deep Sky Blue</option>
  <option style="background-color: darkorchid" value="DarkOrchid">Dark Orchid</option>
  <option style="background-color: silver" value="Silver">Silver</option>
  <option style="background-color: pink" value="Pink">Pink</option>
  <option style="background-color: wheat" value="Wheat">Wheat</option>
  <option style="background-color: lemonchiffon" value="LemonChiffon">Lemon Chiffon</option>
  <option style="background-color: palegreen" value="PaleGreen">Pale Green</option>
  <option style="background-color: paleturquoise" value="PaleTurquoise">Pale Turquoise</option>
  <option style="background-color: lightblue" value="LightBlue">Light Blue</option>
  <option style="background-color: plum" value="Plum">Plum</option>
  <option style="background-color: white" value="White">White</option>
  </select></td>
  <td class="embedded">
  <select class="med codebuttons" name='font' onchange="alterfont(this.options[this.selectedIndex].value, 'font')">
  <option value="0">--- <?php echo $lang_functions['select_font'] ?> ---</option>
  <option value="Arial">Arial</option>
  <option value="Arial Black">Arial Black</option>
  <option value="Arial Narrow">Arial Narrow</option>
  <option value="Book Antiqua">Book Antiqua</option>
  <option value="Century Gothic">Century Gothic</option>
  <option value="Comic Sans MS">Comic Sans MS</option>
  <option value="Courier New">Courier New</option>
  <option value="Fixedsys">Fixedsys</option>
  <option value="Garamond">Garamond</option>
  <option value="Georgia">Georgia</option>
  <option value="Impact">Impact</option>
  <option value="Lucida Console">Lucida Console</option>
  <option value="Lucida Sans Unicode">Lucida Sans Unicode</option>
  <option value="Microsoft Sans Serif">Microsoft Sans Serif</option>
  <option value="Palatino Linotype">Palatino Linotype</option>
  <option value="System">System</option>
  <option value="Tahoma">Tahoma</option>
  <option value="Times New Roman">Times New Roman</option>
  <option value="Trebuchet MS">Trebuchet MS</option>
  <option value="Verdana">Verdana</option>
  </select>
  </td>
  <td class="embedded">
  <select class="med codebuttons" name='size' onchange="alterfont(this.options[this.selectedIndex].value, 'size')">
  <option value="0">--- <?php echo $lang_functions['select_size'] ?> ---</option>
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
  <option value="5">5</option>
  <option value="6">6</option>
  <option value="7">7</option>
  </select></td></tr>
  </table>
  </td>
  </tr>
  <?php
  if ($enableattach_attachment == 'yes') {
  ?>
  <tr>
  <td colspan="2" valign="middle">
  <iframe src="<?php echo get_protocol_prefix() . $BASEURL ?>/attachment.php" width="100%" height="24" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
  </td>
  </tr>
  <?php
  }
  print("<tr>");
  print("<td align=\"left\"><textarea class=\"bbcode\" cols=\"100\" style=\"width: 650px;\" name=\"" . $text . "\" id=\"" . $text . "\" rows=\"20\" onkeydown=\"ctrlenter(event,'compose','qr')\">" . $content . "</textarea>");
  ?>
  </td>
  <td align="center" width="99%">
  <table cellspacing="1" cellpadding="3">
  <tr>
  <?php
  $i = 0;
  $quickSmilies = array(1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 13, 16, 17, 19, 20, 21, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 39, 40);
  foreach ($quickSmilies as $smily) {
  if ($i % 4 == 0 && $i > 0) {
  print('</tr><tr>');
  }
  print("<td class=\"embedded\" style=\"padding: 3px;\">" . getSmileIt($form, $text, $smily) . "</td>");
  $i++;
  }
  ?>
  </tr></table>
  <br />
  <a href="javascript:winop();"><?php echo $lang_functions['text_more_smilies'] ?></a>
  </td></tr></table>
  <?php
  }
 * 原始编辑器
 */

function begin_compose($title = "", $type = "new", $body = "", $hassubject = true, $subject = "", $maxsubjectlength = 100, $onlyauthor = 0) {
	global $lang_functions;
	if ($title)
		print("<h1 align=\"center\">" . $title . "</h1>");
	switch ($type) {
		case 'new': {
				$framename = $lang_functions['text_new'];
				break;
			}
		case 'reply': {
				$framename = $lang_functions['text_reply'];
				break;
			}
		case 'quote': {
				$framename = $lang_functions['text_quote'];
				break;
			}
		case 'edit': {
				$framename = $lang_functions['text_edit'];
				break;
			}
		default: {
				$framename = $lang_functions['text_new'];
				break;
			}
	}
	begin_frames($framename, true);
	print("<table class=\"main\" width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\">\n");
	if ($hassubject) {
		$script_name = $_SERVER["SCRIPT_FILENAME"];
		print("<tr><td class=\"rowhead\">" . $lang_functions['row_subject'] . "</td><td class=\"rowfollow\" align=\"left\"><input type=\"text\" style=\"width: 650px;\" name=\"subject\" maxlength=\"" . $maxsubjectlength . "\" value=\"" . $subject . "\" />" . (preg_match("/fun|news|sendmessage|contactstaff/i", $script_name) ? "" : "<input name=\"onlyauthor\" type=\"checkbox\"" . ($onlyauthor == 1 ? " checked" : "") . " value=\"1\" />" . $lang_functions['text_onlyauthor']) . "</td></tr>\n");
	}
	print("<tr><td class=\"rowhead\" valign=\"top\">" . $lang_functions['row_body'] . "</td><td class=\"rowfollow\" align=\"left\"><span style=\"display: none;\" id=\"previewouter\"></span><div id=\"editorouter\">");
	//textbbcode("compose", "body", $body, false);
	print("<textarea id='sceditor' name='body' onkeydown='ctrlenter(event,'compose','qr')' style='width:100%;height:300px;'>$body</textarea>");
	print("</div></td></tr>");
	if ($type == "quote")
		print("<tr><td colspan=\"2\" align=\"center\">给引用到的<select name=\"quotenum\"><option value=\"0\">0</option><option value=\"1\" selected>1</option><option value=\"2\">2</option><option value=\"3\">3</option></select>个用户发站内信提醒</td></tr>");
}

function end_compose() {
	global $lang_functions;
	print("<tr><td colspan=\"2\" align=\"center\"><table><tr><td class=\"embedded\"><input id=\"qr\" type=\"submit\" class=\"btn\" value=\"" . $lang_functions['submit_submit'] . "\" /></td><td class=\"embedded\">");
	print("<input type=\"button\" class=\"btn2\" name=\"previewbutton\" id=\"previewbutton\" value=\"" . $lang_functions['submit_preview'] . "\" onclick=\"javascript:preview(this.parentNode);\" />");
	print("<input type=\"button\" class=\"btn2\" style=\"display: none;\" name=\"unpreviewbutton\" id=\"unpreviewbutton\" value=\"" . $lang_functions['submit_edit'] . "\" onclick=\"javascript:unpreview(this.parentNode);\" />");
	print("</td></tr></table>");
	print("</td></tr>");
	print("</table>\n");
	end_frame();
	print("<p align=\"center\"><a href=\"tags.php\" target=\"_blank\">" . $lang_functions['text_tags'] . "</a> | <a href=\"smilies.php\" target=\"_blank\">" . $lang_functions['text_smilies'] . "</a></p>\n");
}

function insert_suggest($keyword, $userid, $pre_escaped = true) {
	if (mb_strlen($keyword, "UTF-8") >= 2) {
		$userid = 0 + $userid;
		if ($userid)
			sql_query("INSERT INTO suggest(keywords, userid, adddate) VALUES (" . ($pre_escaped == true ? "'" . $keyword . "'" : sqlesc($keyword)) . "," . sqlesc($userid) . ", NOW())") or sqlerr(__FILE__, __LINE__);
	}
}

function insert_limitsuggest($keyword, $userid, $pre_escaped = true) {
	if (mb_strlen($keyword, "UTF-8") >= 2) {
		$userid = 0 + $userid;
		if ($userid)
			sql_query("INSERT INTO limitsuggest(keywords, userid, adddate) VALUES (" . ($pre_escaped == true ? "'" . $keyword . "'" : sqlesc($keyword)) . "," . sqlesc($userid) . ", NOW())") or sqlerr(__FILE__, __LINE__);
	}
}

function get_external_tr($imdb_url = "") {
	global $lang_functions;
	global $showextinfo;
	$imdbNumber = parse_imdb_id($imdb_url);
	($showextinfo['imdb'] == 'yes' ? tr($lang_functions['row_imdb_url'], "<input type=\"text\" style=\"width: 650px;\" name=\"imdburl\" value=\"" . ($imdbNumber ? "http://www.imdb.com/title/tt" . parse_imdb_id($imdb_url) : "") . "\" /><br /><font class=\"medium\">" . $lang_functions['text_imdb_url_note'] . "</font>", 1) : "");
}

function get_dbexternal_tr($db_url = "") {
	global $lang_functions;
	global $showextinfo;
	$dbNumber = parse_douban_id($db_url);
	($showextinfo['imdb'] == 'yes' ? tr($lang_functions['row_douban_url'], "<input type=\"text\" style=\"width: 650px;\" name=\"dburl\" value=\"" . ($dbNumber ? "http://movie.douban.com/subject/" . parse_douban_id($db_url) : "") . "\" /><br /><font class=\"medium\">" . $lang_functions['text_douban_url_note'] . "</font>", 1) : "");
}

function get_torrent_extinfo_identifier($torrentid) {
	$torrentid = 0 + $torrentid;

	$result = array('imdb_id');
	unset($result);

	if ($torrentid) {
		$res = sql_query("SELECT url FROM torrents WHERE id=" . $torrentid) or sqlerr(__FILE__, __LINE__);
		if (mysql_num_rows($res) == 1) {
			$arr = mysql_fetch_array($res) or sqlerr(__FILE__, __LINE__);
			$imdb_id = parse_imdb_id($arr["url"]);
			$result['imdb_id'] = $imdb_id;
		}
	}
	return $result;
}

// it's a stub implemetation here, we need more acurate regression analysis to complete our algorithm
function get_torrent_2_user_value($user_snatched_arr) {
// check if it's current user's torrent
	$torrent_2_user_value = 1.0;

	$torrent_res = sql_query("SELECT * FROM torrents WHERE id = " . $user_snatched_arr['torrentid']) or sqlerr(__FILE__, __LINE__);
	if (mysql_num_rows($torrent_res) == 1) { // torrent still exists
		$torrent_arr = mysql_fetch_array($torrent_res) or sqlerr(__FILE__, __LINE__);
		if ($torrent_arr['owner'] == $user_snatched_arr['userid']) { // owner's torrent
			$torrent_2_user_value *= 0.7; // owner's torrent
			$torrent_2_user_value += ($user_snatched_arr['uploaded'] / $torrent_arr['size'] ) - 1 > 0 ? 0.2 - exp(-(($user_snatched_arr['uploaded'] / $torrent_arr['size'] ) - 1)) : ($user_snatched_arr['uploaded'] / $torrent_arr['size'] ) - 1;
			$torrent_2_user_value += min(0.1, ($user_snatched_arr['seedtime'] / 37 * 60 * 60 ) * 0.1);
		} else {
			if ($user_snatched_arr['finished'] == 'yes') {
				$torrent_2_user_value *= 0.5;
				$torrent_2_user_value += ($user_snatched_arr['uploaded'] / $torrent_arr['size'] ) - 1 > 0 ? 0.4 - exp(-(($user_snatched_arr['uploaded'] / $torrent_arr['size'] ) - 1)) : ($user_snatched_arr['uploaded'] / $torrent_arr['size'] ) - 1;
				$torrent_2_user_value += min(0.1, ($user_snatched_arr['seedtime'] / 22 * 60 * 60 ) * 0.1);
			} else {
				$torrent_2_user_value *= 0.2;
				$torrent_2_user_value += min(0.05, ($user_snatched_arr['leechtime'] / 24 * 60 * 60 ) * 0.1); // usually leechtime could not explain much
			}
		}
	} else { // torrent already deleted, half blind guess, be conservative
		if ($user_snatched_arr['finished'] == 'no' && $user_snatched_arr['uploaded'] > 0 && $user_snatched_arr['downloaded'] == 0) { // possibly owner
			$torrent_2_user_value *= 0.55; //conservative
			$torrent_2_user_value += min(0.05, ($user_snatched_arr['leechtime'] / 31 * 60 * 60 ) * 0.1);
			$torrent_2_user_value += min(0.1, ($user_snatched_arr['seedtime'] / 31 * 60 * 60 ) * 0.1);
		} else if ($user_snatched_arr['downloaded'] > 0) { // possibly leecher
			$torrent_2_user_value *= 0.38; //conservative
			$torrent_2_user_value *= min(0.22, 0.1 * $user_snatched_arr['uploaded'] / $user_snatched_arr['downloaded']); // 0.3 for conservative
			$torrent_2_user_value += min(0.05, ($user_snatched_arr['leechtime'] / 22 * 60 * 60 ) * 0.1);
			$torrent_2_user_value += min(0.12, ($user_snatched_arr['seedtime'] / 22 * 60 * 60 ) * 0.1);
		} else
			$torrent_2_user_value *= 0.0;
	}
	return $torrent_2_user_value;
}

function cur_user_check() {
	global $lang_functions;
	global $CURUSER;
	if ($CURUSER) {
		sql_query("UPDATE users SET lang=" . get_langid_from_langcookie() . " WHERE id = " . $CURUSER['id']);
		stderr($lang_functions['std_permission_denied'], $lang_functions['std_already_logged_in']);
	}
}

function KPS($type = "+", $point = "1.0", $id = "") {
	global $bonus_tweak;
	if ($point != 0) {
		$point = sqlesc($point);
		if ($bonus_tweak == "enable" || $bonus_tweak == "disablesave") {
			sql_query("UPDATE users SET seedbonus = seedbonus$type$point WHERE id = " . sqlesc($id)) or sqlerr(__FILE__, __LINE__);
		}
	} else
		return;
}

function get_agent($peer_id, $agent) {
	return substr($agent, 0, (strpos($agent, ";") == false ? strlen($agent) : strpos($agent, ";")));
}

function EmailBanned($newEmail) {
	$newEmail = trim(strtolower($newEmail));
	$list = mysql_fetch_array(sql_query("SELECT * FROM bannedemails"));
	$addresses = explode(' ', preg_replace("/[[:space:]]+/", " ", trim($list[value])));

	if (count($addresses) > 0) {
		foreach ($addresses as $email) {
			$email = trim(strtolower(preg_replace('/\./', '\\.', $email)));
			if (strstr($email, "@")) {
				if (preg_match('/^@/', $email)) {// Any user @host?
// Expand the match expression to catch hosts and
// sub-domains
					$email = preg_replace('/^@/', '[@\\.]', $email);
					if (preg_match("/" . $email . "$/", $newEmail))
						return true;
				}
			}
			elseif (preg_match('/@$/', $email)) { // User at any host?
				if (preg_match("/^" . $email . "/", $newEmail))
					return true;
			}
			else { // User@host
				if (strtolower($email) == $newEmail)
					return true;
			}
		}
	}

	return false;
}

function EmailAllowed($newEmail) {
	global $restrictemaildomain;
	if ($restrictemaildomain == 'yes') {
		$newEmail = trim(strtolower($newEmail));
		$list = mysql_fetch_array(sql_query("SELECT * FROM allowedemails"));
		$addresses = explode(' ', preg_replace("/[[:space:]]+/", " ", trim($list[value])));

		if (count($addresses) > 0) {
			foreach ($addresses as $email) {
				$email = trim(strtolower(preg_replace('/\./', '\\.', $email)));
				if (strstr($email, "@")) {
					if (preg_match('/^@/', $email)) {// Any user @host?
// Expand the match expression to catch hosts and
// sub-domains
						$email = preg_replace('/^@/', '[@\\.]', $email);
						if (preg_match('/' . $email . '$/', $newEmail))
							return true;
					}
				}
				elseif (preg_match('/@$/', $email)) { // User at any host?
					if (preg_match("/^" . $email . "/", $newEmail))
						return true;
				}
				else { // User@host
					if (strtolower($email) == $newEmail)
						return true;
				}
			}
		}
		return false;
	} else
		return true;
}

function allowedemails() {
	$list = mysql_fetch_array(sql_query("SELECT * FROM allowedemails"));
	return $list['value'];
}

function redirect($url, $second = 0) {
	echo "<meta http-equiv=\"refresh\" content=\"$second;url=$url\">";
	exit;
}

function set_cachetimestamp($id, $field = "cache_stamp") {
	sql_query("UPDATE torrents SET $field = 255 WHERE id = $id") or sqlerr(__FILE__, __LINE__);
}

function reset_cachetimestamp($id, $field = "cache_stamp") {
	sql_query("UPDATE torrents SET $field = 0 WHERE id = $id") or sqlerr(__FILE__, __LINE__);
}

function cache_check($file = 'cachefile', $endpage = true, $cachetime = 600) {
	global $lang_functions;
	global $rootpath, $cache, $CURLANGDIR;
	$cachefile = $rootpath . $cache . "/" . $CURLANGDIR . '/' . $file . '.html';
	if (file_exists($cachefile) && (time() - $cachetime < filemtime($cachefile))) {
		include($cachefile);
		if ($endpage) {
			print("<p align=\"center\"><font class=\"small\">" . $lang_functions['text_page_last_updated'] . date('Y-m-d H:i:s', filemtime($cachefile)) . "</font></p>");
			end_main_frame();
			stdfoot();
			exit;
		}
		return false;
	}
	ob_start();
	return true;
}

function cache_save($file = 'cachefile') {
	global $rootpath, $cache;
	global $CURLANGDIR;
	$cachefile = $rootpath . $cache . "/" . $CURLANGDIR . '/' . $file . '.html';
	$fp = fopen($cachefile, 'w');
// save the contents of output buffer to the file
	fwrite($fp, ob_get_contents());
// close the file
	fclose($fp);
// Send the output to the browser
	ob_end_flush();
}

function get_email_encode($lang) {
	if ($lang == 'chs' || $lang == 'cht')
		return "gbk";
	else
		return "utf-8";
}

function change_email_encode($lang, $content) {
	return iconv("utf-8", get_email_encode($lang) . "//IGNORE", $content);
}

function safe_email($email) {
	$email = str_replace(array("<", ">", "\'", "\"", "\\\\"), array("", "", "", "", ""), $email);
	return $email;
}

function check_email($email) {
	if (preg_match('/^[A-Za-z0-9][A-Za-z0-9_.+\-]*@[A-Za-z0-9][A-Za-z0-9_+\-]*(\.[A-Za-z0-9][A-Za-z0-9_+\-]*)+$/', $email))
		return true;
	else
		return false;
}

function sent_mail($to, $fromname, $fromemail, $subject, $body, $type = "confirmation", $showmsg = true, $multiple = false, $multiplemail = '', $hdr_encoding = 'UTF-8', $specialcase = '') {
	global $lang_functions;
	global $rootpath, $SITEEMAIL, $smtptype, $smtp, $smtp_host, $smtp_port, $smtp_from, $smtpaddress, $smtpport, $accountname, $accountpassword;
	# Is the OS Windows or Mac or Linux?
	if (strtoupper(substr(PHP_OS, 0, 3) == 'WIN')) {
		$eol = "\r\n";
		$windows = true;
	} elseif (strtoupper(substr(PHP_OS, 0, 3) == 'MAC'))
		$eol = "\r";
	else
		$eol = "\n";
	if ($smtptype == 'none')
		return false;
	if ($smtptype == 'default') {
		mail($to, "=?" . $hdr_encoding . "?B?" . base64_encode($subject) . "?=", $body, "From: " . $SITEEMAIL . $eol . "Content-type: text/html; charset=" . $hdr_encoding . $eol, "-f $SITEEMAIL") or senderror($lang_functions['std_error'], $lang_functions['text_unable_to_send_mail']);
	} elseif ($smtptype == 'advanced') {
		//$fromname_encode = "=?UTF-8?B?" . base64_encode($fromname) . "?="; //部分邮箱会再次base64编码，所以弃用
		$mid = md5(getip() . $fromname);
		$name = $_SERVER["SERVER_NAME"];
		/*
		  $headers .= "From: $fromname_encode <$fromemail>" . $eol;
		  $headers .= "Reply-To: $fromname_encode <$fromemail>" . $eol;
		  $headers .= "Return-Path: $fromname_encode <$fromemail>" . $eol;
		 * 部分邮箱会再次base64编码，所以弃用
		 */
		$headers .= "From: $fromname <$fromemail>" . $eol;
		$headers .= "Reply-To: $fromname <$fromemail>" . $eol;
		$headers .= "Return-Path: $fromname <$fromemail>" . $eol;
		$headers .= "Message-ID: <$mid admin@$name>" . $eol;
		$headers .= "X-Mailer: PHP v" . phpversion() . $eol;
		$headers .= "MIME-Version: 1.0" . $eol;
		$headers .= "Content-type: text/html; charset=" . $hdr_encoding . $eol;
		$headers .= "X-Sender: PHP" . $eol;
		if ($multiple) {
			$bcc_multiplemail = "";
			foreach ($multiplemail as $toemail)
				$bcc_multiplemail = $bcc_multiplemail . ( $bcc_multiplemail != "" ? "," : "") . $toemail;
			$headers .= "BCC: $multiplemail.$eol";
		}
		if ($smtp == "yes") {
			ini_set('SMTP', $smtp_host);
			ini_set('smtp_port', $smtp_port);
			if ($windows)
				ini_set('sendmail_from', $smtp_from);
		}
		mail($to, "=?" . $hdr_encoding . "?B?" . base64_encode($subject) . "?=", $body, $headers) or senderror($lang_functions['std_error'], $lang_functions['text_unable_to_send_mail']);
		ini_restore(SMTP);
		ini_restore(smtp_port);
		if ($windows)
			ini_restore(sendmail_from);
	} elseif ($smtptype == 'external') {
		require_once ($rootpath . 'include/smtp/smtp.lib.php');
		$mail = new smtp($hdr_encoding, 'eYou');
		//$mail->debug(true);
		$mail->open($smtpaddress, $smtpport);
		$mail->auth($accountname, $accountpassword);
		//$mail->bcc($multiplemail);
		$mail->from($SITEEMAIL);
		$mail->from($accountname);
		if ($multiple) {
			$mail->multi_to_head($to);
			foreach ($multiplemail as $toemail)
				$mail->multi_to($toemail);
		} else
			$mail->to($to);
		$mail->mime_content_transfer_encoding();
		$mail->mime_charset('text/html', $hdr_encoding);
		$mail->subject($subject);
		$mail->body($body);
		if ($specialcase == 'noerror') {
			if ($mail->send()) {
				return true;
			} elseif ($mail->close()) {
				return false;
			}
		} else {
			$mail->send() or senderror($lang_functions['std_error'], $lang_functions['text_unable_to_send_mail']);
		}
		$mail->close();
	}
	if ($showmsg) {
		if ($type == "confirmation")
			sendsuccess($lang_functions['std_success'], $lang_functions['std_confirmation_email_sent'] . "<b>" . htmlspecialchars($to) . "</b>，" . $lang_functions['std_please_wait'], false);
		elseif ($type == "details")
			sendsuccess($lang_functions['std_success'], $lang_functions['std_account_details_sent'] . "<b>" . htmlspecialchars($to) . "</b>，" . $lang_functions['std_please_wait'], false);
	} else
		return true;
}

function senderror($title, $msg) {
	if (!function_exists('msgalert')) {
		stdhead();
	}
	stdmsg($title, $msg);
	stdfoot();
	exit;
}

function sendsuccess($title, $msg) {
	if (!function_exists('msgalert')) {
		stdhead();
	}
	stdmsg($title, $msg);
	stdfoot();
	exit;
}

function failedloginscheck($type = 'Login') {
	global $lang_functions;
	global $maxloginattempts;
	$total = 0;
	$added = sqlesc(date("Y-m-d H:i:s"));
	$ip = sqlesc(getip());
	$Query = sql_query("SELECT SUM(attempts) FROM loginattempts WHERE ip = $ip") or sqlerr(__FILE__, __LINE__);
	list($total) = mysql_fetch_array($Query);
	if ($total >= $maxloginattempts) {
		sql_query("UPDATE loginattempts SET banned = 'yes', added = $added WHERE ip = $ip") or sqlerr(__FILE__, __LINE__);
		stderr($type . $lang_functions['std_locked'] . $type . $lang_functions['std_attempts_reached'], $lang_functions['std_your_ip_banned']);
	}
}

function failedlogins($type = 'login', $recover = false, $head = true) {
	global $lang_functions;
	$ip = sqlesc(getip());
	$added = sqlesc(date("Y-m-d H:i:s"));
	$a = (@mysql_fetch_row(@sql_query("SELECT COUNT(*) FROM loginattempts WHERE ip = $ip"))) or sqlerr(__FILE__, __LINE__);
	if ($a[0] == 0)
		sql_query("INSERT INTO loginattempts (ip, added, attempts) VALUES ($ip, $added, 1)") or sqlerr(__FILE__, __LINE__);
	else
		sql_query("UPDATE loginattempts SET attempts = attempts + 1 where ip = $ip") or sqlerr(__FILE__, __LINE__);
	if ($recover)
		sql_query("UPDATE loginattempts SET type = 'recover' WHERE ip = $ip") or sqlerr(__FILE__, __LINE__);
	if ($type == 'silent')
		return;
	elseif ($type == 'login') {
		stderr($lang_functions['std_login_failed'], $lang_functions['std_login_failed_note'], false);
	} else
		stderr($lang_functions['std_failed'], $type, false, $head);
}

function login_failedlogins($type = 'login', $recover = false, $head = true) {
	global $lang_functions;
	$ip = sqlesc(getip());
	$added = sqlesc(date("Y-m-d H:i:s"));
	$a = (@mysql_fetch_row(@sql_query("SELECT COUNT(*) FROM loginattempts WHERE ip = $ip"))) or sqlerr(__FILE__, __LINE__);
	if ($a[0] == 0)
		sql_query("INSERT INTO loginattempts (ip, added, attempts) VALUES ($ip, $added, 1)") or sqlerr(__FILE__, __LINE__);
	else
		sql_query("UPDATE loginattempts SET attempts = attempts + 1 WHERE ip = $ip") or sqlerr(__FILE__, __LINE__);
	if ($recover)
		sql_query("UPDATE loginattempts SET type = 'recover' WHERE ip = $ip") or sqlerr(__FILE__, __LINE__);
	if ($type == 'silent')
		return;
	elseif ($type == 'login') {
		stderr($lang_functions['std_login_failed'], $lang_functions['std_login_failed_note'], false);
	} else
		stderr($lang_functions['std_recover_failed'], $type, false, $head);
}

function remaining($type = 'login') {
	global $maxloginattempts;
	$total = 0;
	$ip = sqlesc(getip());
	$Query = sql_query("SELECT SUM(attempts) FROM loginattempts WHERE ip = $ip") or sqlerr(__FILE__, __LINE__);
	list($total) = mysql_fetch_array($Query);
	$remaining = $maxloginattempts - $total;
	if ($remaining <= 2)
		$remaining = "<font color=\"red\" size=\"2\">[" . $remaining . "]</font>";
	else
		$remaining = "<font color=\"green\" size=\"2\">[" . $remaining . "]</font>";

	return $remaining;
}

function registration_check($type = "invitesystem", $maxuserscheck = true, $ipcheck = true, $invitenumber) {
	/*
	 * 官邀关闭，注册关闭时不可注册
	 * 官邀开启，注册关闭时可以注册
	 * 官邀关闭，注册开启时可以注册
	 * 官邀开启，注册开启时可以注册
	 * 官邀关闭，注册关闭时权贵可发邀请并且只能使用权贵发的邀请码注册
	 */
	global $lang_functions;
	global $invitesystem, $registration, $cardreg, $maxusers, $maxip, $officialinvites, $Tsendinvite_class;
	if ($officialinvites == 'no') {
		if ($type == "invitesystem") {
			if ($invitesystem == "no" && get_user_class() < $Tsendinvite_class) {//关邀期间，指定等级及以上可发邀
				$res = sprintf("SELECT inviter FROM invites WHERE hash = '%s'", mysql_real_escape_string($invitenumber));
				$inviter = mysql_fetch_array(sql_query($res));
				$row = mysql_fetch_array(sql_query("SELECT class FROM users WHERE id = " . $inviter['inviter']));
				if ($row['class'] < $Tsendinvite_class) {
					stderr($lang_functions['std_oops'], $lang_functions['std_invite_system_disabled'], 0);
				}
			}
		}
	}

	if ($type == "normal") {
		if ($registration == "no") {
			stderr($lang_functions['std_sorry'], $lang_functions['std_open_registration_disabled'], 0);
		}
	}

	if ($type == "cardreg") {
		if ($cardreg == "no") {
			stderr($lang_functions['std_sorry'], $lang_functions['std_open_cardreg_disabled'], 0);
		}
	}

	if ($maxuserscheck) {
		$res = sql_query("SELECT COUNT(*) FROM users") or sqlerr(__FILE__, __LINE__);
		$arr = mysql_fetch_row($res);
		if ($arr[0] >= $maxusers)
			stderr($lang_functions['std_sorry'], $lang_functions['std_account_limit_reached'], 0);
	}

	if ($ipcheck) {
		$ip = getip();
		$a = (@mysql_fetch_row(@sql_query("SELECT COUNT(*) FROM users WHERE ip = '" . mysql_real_escape_string($ip) . "'"))) or sqlerr(__FILE__, __LINE__);
		if ($a[0] > $maxip)
			stderr($lang_functions['std_sorry'], $lang_functions['std_the_ip'] . "<b>" . htmlspecialchars($ip) . "</b>" . $lang_functions['std_used_many_times'], false);
	}
	return true;
}

function random_str($length = "6") {
	$set = array("A", "B", "C", "D", "E", "F", "G", "H", "P", "R", "M", "N", "1", "2", "3", "4", "5", "6", "7", "8", "9");
	$str;
	for ($i = 1; $i <= $length; $i++) {
		$ch = rand(0, count($set) - 1);
		$str .= $set[$ch];
	}
	return $str;
}

function image_code() {
	$randomstr = random_str();
	$imagehash = md5($randomstr);
	$dateline = time();
	$sql = 'INSERT INTO `regimages` (`imagehash`, `imagestring`, `dateline`) VALUES (\'' . $imagehash . '\', \'' . $randomstr . '\', \'' . $dateline . '\');';
	sql_query($sql) or die(mysql_error());
	return $imagehash;
}

function check_code($imagehash, $imagestring, $where = 'signup.php', $maxattemptlog = false, $head = true) {
	global $lang_functions;
	$query = sprintf("SELECT * FROM regimages WHERE imagehash='%s' AND imagestring='%s'", mysql_real_escape_string($imagehash), mysql_real_escape_string($imagestring));
	$sql = sql_query($query);
	$imgcheck = mysql_fetch_array($sql);
	if (!$imgcheck['dateline']) {
		$delete = sprintf("DELETE FROM regimages WHERE imagehash='%s'", mysql_real_escape_string($imagehash));
		sql_query($delete);
		if (!$maxattemptlog)
			bark($lang_functions['std_invalid_image_code'] . "<a href=\"" . htmlspecialchars($where) . "\">" . $lang_functions['std_here_to_request_new']);
		else
			failedlogins($lang_functions['std_invalid_image_code'] . "<a href=\"" . htmlspecialchars($where) . "\">" . $lang_functions['std_here_to_request_new'], true, $head);
	}else {
		$delete = sprintf("DELETE FROM regimages WHERE imagehash='%s'", mysql_real_escape_string($imagehash));
		sql_query($delete);
		return true;
	}
}

function show_image_code() {
	global $lang_functions;
	global $iv;
	if ($iv == "yes") {
		unset($imagehash);
		$imagehash = image_code();
		print ("<tr><td class=\"rowhead\">" . $lang_functions['row_security_image'] . "</td>");
		print ("<td align=\"left\"><img src=\"" . htmlspecialchars("image.php?action=regimage&imagehash=" . $imagehash) . "\" border=\"0\" alt=\"CAPTCHA\" /></td></tr>");
		print ("<tr><td class=\"rowhead\">" . $lang_functions['row_security_code'] . "</td><td align=\"left\">");
		print("<input type=\"text\" autocomplete=\"off\" style=\"width: 180px; border: 1px solid gray\" name=\"imagestring\" value=\"\" />");
		print("<input type=\"hidden\" name=\"imagehash\" value=\"$imagehash\" /></td></tr>");
	}
}

function get_ip_location($ip) {
	global $lang_functions;
	global $Cache;
	if (!$ret = $Cache->get_value('location_list')) {
		$ret = array();
		$res = sql_query("SELECT * FROM locations") or sqlerr(__FILE__, __LINE__);
		while ($row = mysql_fetch_array($res))
			$ret[] = $row;
		$Cache->cache_value('location_list', $ret, 152800);
	}
//$location = array($lang_functions['text_unknown'],"");
	$location = array(convertip($ip), "");

	foreach ($ret AS $arr) {
		if (in_ip_range(false, $ip, $arr["start_ip"], $arr["end_ip"])) {
			$location = array($arr["name"], $lang_functions['text_user_ip'] . ":&nbsp;" . $ip . ($arr["location_main"] != "" ? "&nbsp;" . $lang_functions['text_location_main'] . ":&nbsp;" . $arr["location_main"] : "") . ($arr["location_sub"] != "" ? "&nbsp;" . $lang_functions['text_location_sub'] . ":&nbsp;" . $arr["location_sub"] : "") . "&nbsp;" . $lang_functions['text_ip_range'] . ":&nbsp;" . $arr["start_ip"] . "&nbsp;~&nbsp;" . $arr["end_ip"]);
			break;
		}
	}
	return $location;
}

function in_ip_range($long, $targetip, $ip_one, $ip_two = false) {
// if only one ip, check if is this ip
	if ($ip_two === false) {
		if (($long ? (long2ip($ip_one) == $targetip) : ( $ip_one == $targetip))) {
			$ip = true;
		} else {
			$ip = false;
		}
	} else {
		if ($long ? ($ip_one <= ip2long($targetip) && $ip_two >= ip2long($targetip)) : (ip2long($ip_one) <= ip2long($targetip) && ip2long($ip_two) >= ip2long($targetip))) {
			$ip = true;
		} else {
			$ip = false;
		}
	}
	return $ip;
}

function validip_format($ip) {
	$ipPattern = '/\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.' .
			'(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.' .
			'(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.' .
			'(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/';

	return preg_match($ipPattern, $ip);
}

function maxslots() {
	global $lang_functions;
	global $CURUSER, $maxdlsystem;
	$gigs = $CURUSER["uploaded"] / (1024 * 1024 * 1024);
	$ratio = (($CURUSER["downloaded"] > 0) ? ($CURUSER["uploaded"] / $CURUSER["downloaded"]) : 1);
	if ($gigs > 10) {
		if ($ratio < 0.2)
			$max = 1;
		elseif ($ratio < 0.3)
			$max = 2;
		elseif ($ratio < 0.4)
			$max = 3;
		elseif ($ratio < 0.5)
			$max = 4;
		else
			$max = 0;
	}
	if ($maxdlsystem == "yes") {
		if (get_user_class() < UC_VIP) {
			if ($max > 0)
				print ("<font class='color_slots'>" . $lang_functions['text_slots'] . "</font><a href=\"faq.php\">$max</a>");
			else
				print ("<font class='color_slots'>" . $lang_functions['text_slots'] . "</font>" . $lang_functions['text_unlimited']);
		} else
			print ("<font class='color_slots'>" . $lang_functions['text_slots'] . "</font>" . $lang_functions['text_unlimited']);
	} else
		print ("<font class='color_slots'>" . $lang_functions['text_slots'] . "</font>" . $lang_functions['text_unlimited']);
}

function WriteConfig($configname = NULL, $config = NULL) {
	global $lang_functions, $CONFIGURATIONS;
	if (file_exists('config/allconfig.php')) {
		require('config/allconfig.php');
	}
	if ($configname) {
		$$configname = $config;
	}
	$path = './config/allconfig.php';
	if (!file_exists($path) || !is_writable($path)) {
		stdmsg($lang_functions['std_error'], $lang_functions['std_cannot_read_file'] . "[<b>" . htmlspecialchars($path) . "</b>]" . $lang_functions['std_access_permission_note']);
	}
	$data = "<?php\n";
	foreach ($CONFIGURATIONS as $CONFIGURATION) {
		$data .= "\$$CONFIGURATION=" . getExportedValue($$CONFIGURATION) . ";\n";
	}
	$fp = @fopen($path, 'w');
	if (!$fp) {
		stdmsg($lang_functions['std_error'], $lang_functions['std_cannot_open_file'] . "[<b>" . htmlspecialchars($path) . "</b>]" . $lang_functions['std_to_save_info'] . $lang_functions['std_access_permission_note']);
	}
	$Res = fwrite($fp, $data);
	if (empty($Res)) {
		stdmsg($lang_functions['std_error'], $lang_functions['text_cannot_save_info_in'] . "[<b>" . htmlspecialchars($path) . "</b>]" . $lang_functions['std_access_permission_note']);
	}
	fclose($fp);
	return true;
}

function getExportedValue($input, $t = null) {
	switch (gettype($input)) {
		case 'string':
			return "'" . str_replace(array("\\", "'"), array("\\\\", "\'"), $input) . "'";
		case 'array':
			$output = "array(\n";
			foreach ($input as $key => $value) {
				$output .= $t . "\t" . getExportedValue($key, $t . "\t") . ' => ' . getExportedValue($value, $t . "\t");
				$output .= ",\n";
			}
			$output .= $t . ')';
			return $output;
		case 'boolean':
			return $input ? 'true' : 'false';
		case 'NULL':
			return 'NULL';
		case 'integer':
		case 'double':
		case 'float':
			return "'" . (string) $input . "'";
	}
	return 'NULL';
}

function dbconn($autoclean = false) {
	global $lang_functions;
	global $mysql_host, $mysql_user, $mysql_pass, $mysql_db;
	global $useCronTriggerCleanUp;

	$conn = mysql_connect($mysql_host, $mysql_user, $mysql_pass);
	if (!mysql_ping($conn)) {
		mysql_close($conn);
		$conn = mysql_connect($mysql_host, $mysql_user, $mysql_pass);
		if (!$conn) {
			switch (mysql_errno()) {
				case 1045:
					echo("<html><head><meta http-equiv=refresh content=\"10 $_SERVER[REQUEST_URI]\"><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head><body><table border=0 width=100% height=100%><tr><td><h3 align=center>不能连接数据库，用户名或密码错误</h3></td></tr></table></body></html>");
				default:
					echo("<html><head><meta http-equiv=refresh content=\"10 $_SERVER[REQUEST_URI]\"><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head><body><table border=0 width=100% height=100%><tr><td><h3 align=center>" . $lang_functions['std_server_load_very_high'] . "</h3></td></tr></table></body></html>");
			}
		}
	}
	mysql_query("SET NAMES UTF8");
	mysql_query("SET collation_connection = 'utf8_general_ci'");
	mysql_query("SET sql_mode=''");
	mysql_select_db($mysql_db) or die();

	userlogin();

	if (!$useCronTriggerCleanUp && $autoclean) {
		register_shutdown_function("autoclean");
	}
}

function get_user_row($id) {
	global $Cache, $CURUSER;
	static $curuserRowUpdated = false;
	static $neededColumns = array('id', 'noad', 'class', 'enabled', 'privacy', 'avatar', 'signature', 'uploaded', 'downloaded', 'last_access', 'username', 'donor', 'leechwarn', 'warned', 'title', 'seedbonus', 'big', 'seedtime', 'leechtime');
	if ($id == $CURUSER['id']) {
		$row = array();
		foreach ($neededColumns as $column) {
			$row[$column] = $CURUSER[$column];
		}
		if (!$curuserRowUpdated) {
			$Cache->cache_value('user_' . $CURUSER['id'] . '_content', $row, 900);
			$curuserRowUpdated = true;
		}
	} elseif (!$row = $Cache->get_value('user_' . $id . '_content')) {
		$res = sql_query("SELECT " . implode(',', $neededColumns) . " FROM users WHERE id = " . sqlesc($id)) or sqlerr(__FILE__, __LINE__);
		$row = mysql_fetch_array($res);
		$Cache->cache_value('user_' . $id . '_content', $row, 900);
	}

	$row['seedbonus'] = (int) $row['seedbonus'];

	if (!$row)
		return false;
	else
		return $row;
}

function userlogin() {
	global $lang_functions;
	global $Cache;
	global $SITE_ONLINE, $oldip;
	global $enablesqldebug_tweak, $sqldebug_tweak;
	unset($GLOBALS["CURUSER"]);

	$ip = getip();
	$nip = ip2long($ip);
	if ($nip) { //$nip would be false for IPv6 address
		$res = sql_query("SELECT * FROM bans WHERE $nip >= first AND $nip <= last") or sqlerr(__FILE__, __LINE__);
		if (mysql_num_rows($res) > 0) {
			header("HTTP/1.0 403 Forbidden");
			print("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head><body>" . $lang_functions['text_unauthorized_ip'] . "</body></html>\n");
			die;
		}
	}

	if (empty($_COOKIE["c_secure_pass"]) || empty($_COOKIE["c_secure_uid"]) || empty($_COOKIE["c_secure_login"]))
		return;
	if ($_COOKIE["c_secure_login"] == base64("yeah")) {
//if (empty($_SESSION["s_secure_uid"]) || empty($_SESSION["s_secure_pass"]))
//return;
	}
	$b_id = base64($_COOKIE["c_secure_uid"], false);
	$id = 0 + $b_id;
	if (!$id || !is_valid_id($id) || strlen($_COOKIE["c_secure_pass"]) != 32)
		return;

	if ($_COOKIE["c_secure_login"] == base64("yeah")) {
//if (strlen($_SESSION["s_secure_pass"]) != 32)
//return;
	}

	$res = sql_query("SELECT * FROM users WHERE users.id = " . sqlesc($id) . " AND users.enabled='yes' AND users.status = 'confirmed' LIMIT 1");
	$row = mysql_fetch_array($res);
	if (!$row)
		return;

	$sec = hash_pad($row["secret"]);

//die(base64_decode($_COOKIE["c_secure_login"]));

	if ($_COOKIE["c_secure_login"] == base64("yeah")) {

		if ($_COOKIE["c_secure_pass"] != md5($row["passhash"] . $_SERVER["REMOTE_ADDR"]))
			return;
	}
	else {
		if ($_COOKIE["c_secure_pass"] !== md5($row["passhash"]))
			return;
	}

	if ($_COOKIE["c_secure_login"] == base64("yeah")) {
//if ($_SESSION["s_secure_pass"] !== md5($row["passhash"].$_SERVER["REMOTE_ADDR"]))
//return;
	}
	if (!$row["passkey"]) {
		$passkey = md5($row['username'] . date("Y-m-d H:i:s") . $row['passhash']);
		sql_query("UPDATE users SET passkey = " . sqlesc(mysql_real_escape_string($passkey)) . " WHERE id=" . sqlesc($row["id"])); // or die(mysql_error());
	}

	$oldip = $row['ip'];
	$row['ip'] = $ip;
	$GLOBALS["CURUSER"] = $row;
	if ($_GET['clearcache'] && get_user_class() >= UC_MODERATOR) {
		$Cache->setClearCache(1);
	}
	if ($enablesqldebug_tweak == 'yes' && get_user_class() >= $sqldebug_tweak) {
		error_reporting(E_ALL & ~E_NOTICE);
	}
}

function autoclean() {
	global $autoclean_interval_one, $rootpath;
	$now = TIMENOW;

	$res = sql_query("SELECT value_u FROM avps WHERE arg = 'lastcleantime1'");
	$row = mysql_fetch_array($res);
	if (!$row) {
		sql_query("INSERT INTO avps (arg, value_u, value_i) VALUES ('lastcleantime1', $now, '$autoclean_interval_one')") or sqlerr(__FILE__, __LINE__);
		return false;
	}
	$ts = $row[0];
	if ($ts + $autoclean_interval_one > $now) {
		return false;
	}
	sql_query("UPDATE avps SET value_u = $now, value_i = " . sqlesc($autoclean_interval_one) . " WHERE arg = 'lastcleantime1' AND value_u = $ts") or sqlerr(__FILE__, __LINE__);
	if (!mysql_affected_rows()) {
		return false;
	}
	require_once($rootpath . 'include/cleanup.php');
	return docleanup();
}

function unesc($x) {
	if (get_magic_quotes_gpc())
		return stripslashes($x);
	return $x;
}

function getsize_int($amount, $unit = "G") {
	if ($unit == "B")
		return floor($amount);
	elseif ($unit == "K")
		return floor($amount * 1024);
	elseif ($unit == "M")
		return floor($amount * 1048576);
	elseif ($unit == "G")
		return floor($amount * 1073741824);
	elseif ($unit == "T")
		return floor($amount * 1099511627776);
	elseif ($unit == "P")
		return floor($amount * 1125899906842624);
}

function mksize_compact($bytes) {
	if ($bytes < 1024 * 1024)
		return number_format($bytes / 1024, 2) . "<br />KB";
	elseif ($bytes < 1024 * 1048576)
		return number_format($bytes / 1048576, 2) . "<br />MB";
	elseif ($bytes < 1024 * 1073741824)
		return number_format($bytes / 1073741824, 2) . "<br />GB";
	elseif ($bytes < 1024 * 1099511627776)
		return number_format($bytes / 1099511627776, 3) . "<br />TB";
	else
		return number_format($bytes / 1125899906842624, 3) . "<br />PB";
}

function mksize_loose($bytes) {
	if ($bytes < 1024 * 1024)
		return number_format($bytes / 1024, 2) . " KB";
	elseif ($bytes < 1024 * 1048576)
		return number_format($bytes / 1048576, 2) . " MB";
	elseif ($bytes < 1024 * 1073741824)
		return number_format($bytes / 1073741824, 2) . " GB";
	elseif ($bytes < 1024 * 1099511627776)
		return number_format($bytes / 1099511627776, 3) . " TB";
	else
		return number_format($bytes / 1125899906842624, 3) . " PB";
}

function mksize($bytes) {
	if ($bytes < 1024 * 1024)
		return number_format($bytes / 1024, 2) . " KB";
	elseif ($bytes < 1024 * 1048576)
		return number_format($bytes / 1048576, 2) . " MB";
	elseif ($bytes < 1024 * 1073741824)
		return number_format($bytes / 1073741824, 2) . " GB";
	elseif ($bytes < 1024 * 1099511627776)
		return number_format($bytes / 1099511627776, 3) . " TB";
	else
		return number_format($bytes / 1125899906842624, 3) . " PB";
}

function mksizeint($bytes) {
	$bytes = max(0, $bytes);
	if ($bytes < 1024)
		return floor($bytes) . " B";
	elseif ($bytes < 1024 * 1024)
		return floor($bytes / 1024) . " KB";
	elseif ($bytes < 1024 * 1048576)
		return floor($bytes / 1048576) . " MB";
	elseif ($bytes < 1024 * 1073741824)
		return floor($bytes / 1073741824) . " GB";
	elseif ($bytes < 1024 * 1099511627776)
		return floor($bytes / 1099511627776) . " TB";
	else
		return floor($bytes / 1125899906842624) . " PB";
}
