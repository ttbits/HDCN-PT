<?php

# IMPORTANT: Do not edit below unless you know what you are doing!
if (!defined('IN_TRACKER'))
	die('Hacking attempt!');

$CONFIGURATIONS = array('ACCOUNT', 'ADVERTISEMENT', 'ATTACHMENT', 'AUTHORITY', 'BASIC', 'BONUS', 'CODE', 'MAIN', 'SECURITY', 'SMTP', 'TORRENT', 'TWEAK', 'CHECK');

function ReadConfig($configname = NULL) {
	global $CONFIGURATIONS;
	if ($configname) {
		$configname = basename($configname);
		$tmp = oldReadConfig($configname);
		WriteConfig($configname, $tmp);
		@unlink('./config/' . $configname);
		return $tmp;
	} else {
		foreach ($CONFIGURATIONS as $CONFIGURATION) {
			$GLOBALS[$CONFIGURATION] = ReadConfig($CONFIGURATION);
		}
	}
}

function oldReadConfig($configname) {
	if (strstr($configname, ',')) {
		$configlist = explode(',', $configname);
		foreach ($configlist as $key => $configname) {
			ReadConfig(trim($configname));
		}
	} else {
		$configname = basename($configname);
		$path = './config/' . $configname;
		if (!file_exists($path)) {
			die("Error! File <b>" . htmlspecialchars($configname) . "</b> doesn't exist!</font><br /><font color=blue>Before the setup starts, please ensure that you have properly configured file and directory access permissions. Please see below.</font><br /><br />chmod 777 config/<br />chmod 777 config/" . $configname);
		}

		$fp = fopen($path, 'r');
		$content = '';
		while (!feof($fp)) {
			$content .= fread($fp, 102400);
		}
		fclose($fp);

		if (empty($content)) {
			return array();
		}
		$tmp = @unserialize($content);

		if (empty($tmp)) {
			die("Error! <font color=red>Cannot read configuration file <b>" . htmlspecialchars($configname) . "</b></font><br /><font color=blue>Before the setup starts, please ensure that you have properly configured file and directory access permissions. For *nix system, please see below.</font><br />chmod 777 config <br />chmod 777 config/" . $configname . "<br /><br /> If access permission is alright, perhaps there's some misconfiguration or the configuration file is corrupted. Please check config/" . $configname);
		}
		$GLOBALS[$configname] = $tmp;
		return $tmp;
	}
}

if (file_exists('config/allconfig.php')) {
	require('config/allconfig.php');
} else {
	ReadConfig();
}

$SITENAME = $BASIC['SITENAME'];
$BASEURL = $BASIC['BASEURL'];
//$BASEURL = $_SERVER['HTTP_HOST'];
$announce_urls = array();
$announce_urls[] = $BASIC['announce_url'];
$mysql_host = $BASIC['mysql_host'];
$mysql_user = $BASIC['mysql_user'];
$mysql_pass = $BASIC['mysql_pass'];
$mysql_db = $BASIC['mysql_db'];

$autorelease = $MAIN['autorelease'];
$ratioless = $MAIN['mode'];
$SITE_ONLINE = $MAIN['site_online'];
$ipv6signup = $MAIN['ipv6signup'];
$bet = $MAIN['betsystem'];
$betsrobotid = $MAIN['betsrobotid'];
$betsforumid = $MAIN['betsforumid'];
$betsadmin = $MAIN['betsadmin'];
$officialinvites = $MAIN['officialinvites'];
$nonass = $MAIN['nonassinvitesystem'];
$nonassbonus = $MAIN['nonassbonus'];
$reward = $MAIN['reward'];
$rewardnum = $MAIN['rewardnum'];
$rewarduser = $MAIN['rewarduser'];
$max_torrent_size = $MAIN['max_torrent_size'];
$announce_interval = (int) $MAIN['announce_interval'];
$annintertwoage = (int) $MAIN['annintertwoage'];
$annintertwo = (int) $MAIN['annintertwo'];
$anninterthreeage = (int) $MAIN['anninterthreeage'];
$anninterthree = (int) $MAIN['anninterthree'];
$signup_timeout = $MAIN['signup_timeout'];
$minoffervotes = $MAIN['minoffervotes'];
$offervotetimeout_main = $MAIN['offervotetimeout'];
$offeruptimeout_main = $MAIN['offeruptimeout'];
$maxsubsize_main = $MAIN['maxsubsize'];
$maxnewsnum_main = $MAIN['maxnewsnum'];
$forumpostsperpage = $MAIN['postsperpage'];
$forumtopicsperpage_main = $MAIN['topicsperpage'];
$torrentsperpage_main = (int) $MAIN['torrentsperpage'];
$max_dead_torrent_time = $MAIN['max_dead_torrent_time'];
$maxusers = $MAIN['maxusers'];
$robotusers = $MAIN['robotusers'];
$torrent_dir = $MAIN['torrent_dir'];
$iniupload_main = $MAIN['iniupload'];
$iniseedbonus_main = $MAIN['iniseedbonus'];
$SITEEMAIL = $MAIN['SITEEMAIL'];
$ACCOUNTANTID = (int) $MAIN['ACCOUNTANTID'];
$ALIPAYACCOUNT = $MAIN['ALIPAYACCOUNT'];
$PAYPALACCOUNT = $MAIN['PAYPALACCOUNT'];
$SLOGAN = $MAIN['SLOGAN'];
$icplicense_main = $MAIN['icplicense'];
$autoclean_interval_one = $MAIN['autoclean_interval_one'];
$autoclean_interval_two = $MAIN['autoclean_interval_two'];
$autoclean_interval_three = $MAIN['autoclean_interval_three'];
$autoclean_interval_four = $MAIN['autoclean_interval_four'];
$autoclean_interval_five = $MAIN['autoclean_interval_five'];
$REPORTMAIL = $MAIN['reportemail'];
$invitesystem = $MAIN['invitesystem'];
$registration = $MAIN['registration'];
//魔力值充值
$recharge = $MAIN['rgenablead'];
$checkfreenum = $MAIN['checkfreenum'];
$claim = $MAIN['claim'];
$claimbonus = $MAIN['claimbonus'];
$claimtime = $MAIN['claimtime'];
$claimmul = $MAIN['claimmul'];
$claimnum = $MAIN['claimnum'];
$claimmaxnum = $MAIN['claimmaxnum'];
$share = $MAIN['share'];
$sharesize = $MAIN['sharesize'];
$sharenum = $MAIN['sharenum'];
$hr = $MAIN['hr'];
$stronghr = $MAIN['stronghr'];
$hrhour = $MAIN['hrhour'];
$hrday = $MAIN['hrday'];
$hrhit = $MAIN['hrhit'];
$hrbonus = $MAIN['hrbonus'];
$hrradio = $MAIN['hrradio'];
$hrstartradio = $MAIN['hrstartradio'];
$newuser = $MAIN['newuser'];
$dl = $MAIN['newuser_dl'];
$ul = $MAIN['newuser_ul'];
$sb = $MAIN['newuser_sb'];
$ra = $MAIN['newuser_ra'];
$dltime = $MAIN['newuser_dltime'] * 86400;
$ultime = $MAIN['newuser_ultime'] * 86400;
$tr = $MAIN['newuser_tr'];
$fa = $MAIN['newuser_fa'];
$kstime = $MAIN['newuser_kstime'];
$bump = $MAIN['bump'];
$bumptime = $MAIN['bumptime'];
$bumpaward = $MAIN['bumpaward'];
$bumptimeaward = $MAIN['bumptimeaward'];
$bumptop = $MAIN['bumptop'];
$cuxiao = $MAIN['cuxiao'];
$cuxiaoselect = $MAIN['cuxiaoselect'];
$cuxiaofree = $MAIN['cuxiaofree'];
$cuxiaosticky = $MAIN['cuxiaosticky'];
$autosticky = $MAIN['sticky'];
$autostickytime = $MAIN['stickytime'];
$autoend = $MAIN['autoend'];
$autoendtime = $MAIN['autoendtime'];
$endfree = $MAIN['autoendfree'];
$endsticky = $MAIN['autoendsticky'];
$endlimit = $MAIN['autoendlimit'];
$cardreg = $MAIN['cardreg'];
$showmovies['hot'] = $MAIN['showhotmovies'];
$showmovies['classic'] = $MAIN['showclassicmovies'];
$showextinfo['imdb'] = $MAIN['showimdbinfo'];
$enablenfo_main = $MAIN['enablenfo'];
$showschool = $MAIN['enableschool'];
$restrictemaildomain = $MAIN['restrictemail'];
$showpolls_main = $MAIN['showpolls'];
$showstats_main = $MAIN['showstats'];
//$showlastxforumposts_main = $MAIN['showlastxforumposts'];
$showlastxtorrents_main = $MAIN['showlastxtorrents'];
$loginadd = $MAIN['loginadd'];
$showtrackerload = $MAIN['showtrackerload'];
$showshoutbox_main = $MAIN['showshoutbox'];
$showfunbox_main = $MAIN['showfunbox'];
$enableoffer = $MAIN['showoffer'];
$sptime = $MAIN['sptime'];
$showhelpbox_main = $MAIN['showhelpbox'];
$enablebitbucket_main = $MAIN['enablebitbucket'];
$smalldescription_main = $MAIN['smalldescription'];
$enableextforum = $MAIN['extforum'];
$extforumurl = $MAIN['extforumurl'];
$deflang = $MAIN['defaultlang'];
$stylesheetoff = $MAIN['stylesheetoff'];
$defcss = $MAIN['defstylesheet'];
$enabledonation = $MAIN['donation'];
$browsecatmode = (int) $MAIN['browsecat'];
$waitsystem = $MAIN['waitsystem'];
$maxdlsystem = $MAIN['maxdlsystem'];
$bitbucket = $MAIN['bitbucket'];
$torrentnameprefix = $MAIN['torrentnameprefix'];
$showforumstats_main = $MAIN['showforumstats'];
$verification = $MAIN['verification'];
$invite_count = $MAIN['invite_count'];
$invite_timeout = $MAIN['invite_timeout'];
$seeding_leeching_time_calc_start = $MAIN['seeding_leeching_time_calc_start'];
$logo_main = $MAIN['logo'];
$logo_width = $MAIN['logowidth'];
$logo_height = $MAIN['logoheight'];

$mailtouser = $SMTP['mailtouser'];
$emailnotify_smtp = $SMTP['emailnotify'];
$smtptype = $SMTP['smtptype'];
$smtp_host = $SMTP['smtp_host'];
$smtp_port = $SMTP['smtp_port'];
if (strtoupper(substr(PHP_OS, 0, 3) == 'WIN'))
	$smtp_from = $SMTP['smtp_from'];
$smtpaddress = $SMTP['smtpaddress'];
$smtpport = $SMTP['smtpport'];
$accountname = $SMTP['accountname'];
$accountpassword = $SMTP['accountpassword'];

$securelogin = $SECURITY['securelogin'];
$securetracker = $SECURITY['securetracker'];
$https_announce_urls = array();
$https_announce_urls[] = $SECURITY['https_announce_url'];
$iv = $SECURITY['iv'];
$maxip = $SECURITY['maxip'];
$maxloginattempts = $SECURITY['maxloginattempts'];
$disableemailchange = $SECURITY['changeemail'];
$onsecurity = $SECURITY['onsecurity'];
$limitsecurity = $SECURITY['limitsecurity'];
$viewlimit = $SECURITY['viewlimit'];
$freeviewlimit = $SECURITY['freeviewlimit'];
$limitbonus = $SECURITY['limitbonus'];
$cheaterdet_security = $SECURITY['cheaterdet'];
$nodetect_security = $SECURITY['nodetect'];
$authoff = $SECURITY['authoff'];
$appid = $SECURITY['appid'];
$appkey = $SECURITY['appkey'];
$authid = $SECURITY['authid'];
$authtype = $SECURITY['authtype'];
$ttkoff = $SECURITY['ttkoff'];
$ttktoken = $SECURITY['ttktoken'];
$bindrewardoff = $SECURITY['bindrewardoff'];
$bindrewardnum = $SECURITY['bindrewardnum'];
$loginrewardoff = $SECURITY['loginrewardoff'];
$loginrewardmul = $SECURITY['loginrewardmul'];

$defaultclass_class = $AUTHORITY['defaultclass'];
$staffmem_class = $AUTHORITY['staffmem'];
$newsmanage_class = $AUTHORITY['newsmanage'];
$newfunitem_class = $AUTHORITY['newfunitem'];
$funmanage_class = $AUTHORITY['funmanage'];
$sbmanage_class = $AUTHORITY['sbmanage'];
$pollmanage_class = $AUTHORITY['pollmanage'];
$applylink_class = $AUTHORITY['applylink'];
$linkmanage_class = $AUTHORITY['linkmanage'];
$postmanage_class = $AUTHORITY['postmanage'];
$commanage_class = $AUTHORITY['commanage'];
$forummanage_class = $AUTHORITY['forummanage'];
$viewuserlist_class = $AUTHORITY['viewuserlist'];
$torrentmanage_class = $AUTHORITY['torrentmanage'];
$torrentsticky_class = $AUTHORITY['torrentsticky'];
$torrentonpromotion_class = $AUTHORITY['torrentonpromotion'];
$askreseed_class = $AUTHORITY['askreseed'];
$viewnfo_class = $AUTHORITY['viewnfo'];
$torrentstructure_class = $AUTHORITY['torrentstructure'];
$sendinvite_class = $AUTHORITY['sendinvite'];
$Tsendinvite_class = $AUTHORITY['Tsendinvite'];
$viewhistory_class = $AUTHORITY['viewhistory'];
$topten_class = $AUTHORITY['topten'];
$log_class = $AUTHORITY['log'];
$confilog_class = $AUTHORITY['confilog'];
$userprofile_class = $AUTHORITY['userprofile'];
$torrenthistory_class = $AUTHORITY['torrenthistory'];
$prfmanage_class = $AUTHORITY['prfmanage'];
$cruprfmanage_class = $AUTHORITY['cruprfmanage'];
$uploadsub_class = $AUTHORITY['uploadsub'];
$delownsub_class = $AUTHORITY['delownsub'];
$submanage_class = $AUTHORITY['submanage'];
$updateextinfo_class = $AUTHORITY['updateextinfo'];
$viewanonymous_class = $AUTHORITY['viewanonymous'];
$beanonymous_class = $AUTHORITY['beanonymous'];
$addoffer_class = $AUTHORITY['addoffer'];
$offermanage_class = $AUTHORITY['offermanage'];
$upload_class = $AUTHORITY['upload'];
$chrmanage_class = $AUTHORITY['chrmanage'];
$viewinvite_class = $AUTHORITY['viewinvite'];
$buyinvite_class = $AUTHORITY['buyinvite'];
$seebanned_class = $AUTHORITY['seebanned'];
$againstoffer_class = $AUTHORITY['againstoffer'];
$userbar_class = $AUTHORITY['userbar'];
$hrmanage_class = $AUTHORITY['hrmanage'];
$newusermanage_class = $AUTHORITY['newusermanage'];
$sharemanage_class = $AUTHORITY['sharemanage'];

$where_tweak = $TWEAK['where'];
$iplog1 = $TWEAK['iplog1'];
$snow = $TWEAK['snow'];
$bonus_tweak = $TWEAK['bonus'];
$titlekeywords_tweak = $TWEAK['titlekeywords'];
$metakeywords_tweak = $TWEAK['metakeywords'];
$metadescription_tweak = $TWEAK['metadescription'];
$datefounded = $TWEAK['datefounded'];
$enablelocation_tweak = $TWEAK['enablelocation'];
$enablesqldebug_tweak = $TWEAK['enablesqldebug'];
$sqldebug_tweak = $TWEAK['sqldebug'];
$cssdate_tweak = $TWEAK['cssdate'];
$enabletooltip_tweak = $TWEAK['enabletooltip'];
$prolinkimg = $TWEAK['prolinkimg'];
$analyticscode_tweak = $TWEAK['analyticscode'];
$groupchat = $TWEAK['groupchat'];

$enableattach_attachment = $ATTACHMENT['enableattach'];
$classone_attachment = $ATTACHMENT['classone'];
$countone_attachment = $ATTACHMENT['countone'];
$sizeone_attachment = $ATTACHMENT['sizeone'];
$extone_attachment = $ATTACHMENT['extone'];
$classtwo_attachment = $ATTACHMENT['classtwo'];
$counttwo_attachment = $ATTACHMENT['counttwo'];
$sizetwo_attachment = $ATTACHMENT['sizetwo'];
$exttwo_attachment = $ATTACHMENT['exttwo'];
$classthree_attachment = $ATTACHMENT['classthree'];
$countthree_attachment = $ATTACHMENT['countthree'];
$sizethree_attachment = $ATTACHMENT['sizethree'];
$extthree_attachment = $ATTACHMENT['extthree'];
$classfour_attachment = $ATTACHMENT['classfour'];
$countfour_attachment = $ATTACHMENT['countfour'];
$sizefour_attachment = $ATTACHMENT['sizefour'];
$extfour_attachment = $ATTACHMENT['extfour'];
$savedirectory_attachment = $ATTACHMENT['savedirectory'];
$httpdirectory_attachment = $ATTACHMENT['httpdirectory'];
$savedirectorytype_attachment = $ATTACHMENT['savedirectorytype'];
$thumbnailtype_attachment = $ATTACHMENT['thumbnailtype'];
$thumbquality_attachment = $ATTACHMENT['thumbquality'];
$thumbwidth_attachment = $ATTACHMENT['thumbwidth'];
$thumbheight_attachment = $ATTACHMENT['thumbheight'];
$watermarkpos_attachment = $ATTACHMENT['watermarkpos'];
$watermarkwidth_attachment = $ATTACHMENT['watermarkwidth'];
$watermarkheight_attachment = $ATTACHMENT['watermarkheight'];
$watermarkquality_attachment = $ATTACHMENT['watermarkquality'];
$altthumbwidth_attachment = $ATTACHMENT['altthumbwidth'];
$altthumbheight_attachment = $ATTACHMENT['altthumbheight'];


$enablead_advertisement = $ADVERTISEMENT['enablead'];
$enablenoad_advertisement = $ADVERTISEMENT['enablenoad'];
$noad_advertisement = $ADVERTISEMENT['noad'];
$enablebonusnoad_advertisement = $ADVERTISEMENT['enablebonusnoad'];
$bonusnoad_advertisement = $ADVERTISEMENT['bonusnoad'];
$bonusnoadpoint_advertisement = $ADVERTISEMENT['bonusnoadpoint'];
$bonusnoadtime_advertisement = $ADVERTISEMENT['bonusnoadtime'];
$adclickbonus_advertisement = $ADVERTISEMENT['adclickbonus'];

$mainversion_code = $CODE['mainversion'];
$subversion_code = $CODE['subversion'];
$releasedate_code = $CODE['releasedate'];
$website_code = $CODE['website'];

$donortimes_bonus = $BONUS['donortimes'];
$perseeding_bonus = $BONUS['perseeding'];
$maxseeding_bonus = $BONUS['maxseeding'];
$tzero_bonus = $BONUS['tzero'];
$nzero_bonus = $BONUS['nzero'];
$bzero_bonus = $BONUS['bzero'];
$l_bonus = $BONUS['l'];
$uploadtorrent_bonus = $BONUS['uploadtorrent'];
$uploadsubtitle_bonus = $BONUS['uploadsubtitle'];
$starttopic_bonus = $BONUS['starttopic'];
$makepost_bonus = $BONUS['makepost'];
$addcomment_bonus = $BONUS['addcomment'];
$pollvote_bonus = $BONUS['pollvote'];
$offervote_bonus = $BONUS['offervote'];
$funboxvote_bonus = $BONUS['funboxvote'];
$saythanks_bonus = $BONUS['saythanks'];
$receivethanks_bonus = $BONUS['receivethanks'];
$funboxreward_bonus = $BONUS['funboxreward'];
$onegbupload_bonus = $BONUS['onegbupload'];
$fivegbupload_bonus = $BONUS['fivegbupload'];
$tengbupload_bonus = $BONUS['tengbupload'];
$hundredgbupload_bonus = $BONUS['hundredgbupload'];
$ratiolimit_bonus = $BONUS['ratiolimit'];
$dlamountlimit_bonus = $BONUS['dlamountlimit'];
$oneinvite_bonus = $BONUS['oneinvite'];
$customtitle_bonus = $BONUS['customtitle'];
$vipstatus_bonus = $BONUS['vipstatus'];
$bonusgift_bonus = $BONUS['bonusgift'];
$basictax_bonus = 0 + $BONUS['basictax'];
$taxpercentage_bonus = 0 + $BONUS['taxpercentage'];
$prolinkpoint_bonus = $BONUS['prolinkpoint'];
$prolinktime_bonus = $BONUS['prolinktime'];
$buyvpn = $BONUS['buyvpn'];
$vpn = $BONUS['vpn'];
$vpnclass = $BONUS['vpnclass'];

$neverdelete_account = $ACCOUNT['neverdelete'];
$neverdeletepacked_account = $ACCOUNT['neverdeletepacked'];
$deletepacked_account = $ACCOUNT['deletepacked'];
$deleteunpacked_account = $ACCOUNT['deleteunpacked'];
$deletenotransfer_account = $ACCOUNT['deletenotransfer'];
$deletenotransfertwo_account = $ACCOUNT['deletenotransfertwo'];
$deletepeasant_account = $ACCOUNT['deletepeasant'];
$psdlone_account = $ACCOUNT['psdlone'];
$psratioone_account = $ACCOUNT['psratioone'];
$psdltwo_account = $ACCOUNT['psdltwo'];
$psratiotwo_account = $ACCOUNT['psratiotwo'];
$psdlthree_account = $ACCOUNT['psdlthree'];
$psratiothree_account = $ACCOUNT['psratiothree'];
$psdlfour_account = $ACCOUNT['psdlfour'];
$psratiofour_account = $ACCOUNT['psratiofour'];
$psdlfive_account = $ACCOUNT['psdlfive'];
$psratiofive_account = $ACCOUNT['psratiofive'];
$putime_account = $ACCOUNT['putimer'];
$pudl_account = $ACCOUNT['pudlr'];
$puprratio_account = $ACCOUNT['puprratior'];
$puderatio_account = $ACCOUNT['puderatior'];
$eutime_account = $ACCOUNT['eutimer'];
$eudl_account = $ACCOUNT['eudlr'];
$euprratio_account = $ACCOUNT['euprratior'];
$euderatio_account = $ACCOUNT['euderatior'];
$cutime_account = $ACCOUNT['cutimer'];
$cudl_account = $ACCOUNT['cudlr'];
$cuprratio_account = $ACCOUNT['cuprratior'];
$cuderatio_account = $ACCOUNT['cuderatior'];
$iutime_account = $ACCOUNT['iutimer'];
$iudl_account = $ACCOUNT['iudlr'];
$iuprratio_account = $ACCOUNT['iuprratior'];
$iuderatio_account = $ACCOUNT['iuderatior'];
$vutime_account = $ACCOUNT['vutimer'];
$vudl_account = $ACCOUNT['vudlr'];
$vuprratio_account = $ACCOUNT['vuprratior'];
$vuderatio_account = $ACCOUNT['vuderatior'];
$exutime_account = $ACCOUNT['exutimer'];
$exudl_account = $ACCOUNT['exudlr'];
$exuprratio_account = $ACCOUNT['exuprratior'];
$exuderatio_account = $ACCOUNT['exuderatior'];
$uutime_account = $ACCOUNT['uutimer'];
$uudl_account = $ACCOUNT['uudlr'];
$uuprratio_account = $ACCOUNT['uuprratior'];
$uuderatio_account = $ACCOUNT['uuderatior'];
$nmtime_account = $ACCOUNT['nmtimer'];
$nmdl_account = $ACCOUNT['nmdlr'];
$nmprratio_account = $ACCOUNT['nmprratior'];
$nmderatio_account = $ACCOUNT['nmderatior'];
$getInvitesByPromotion_class = $ACCOUNT['getInvitesByPromotion'];

$prorules_torrent = $TORRENT['prorules'];
$randomhalfleech_torrent = $TORRENT['randomhalfleech'];
$randomfree_torrent = $TORRENT['randomfree'];
$randomtwoup_torrent = $TORRENT['randomtwoup'];
$randomtwoupfree_torrent = $TORRENT['randomtwoupfree'];
$randomtwouphalfdown_torrent = $TORRENT['randomtwouphalfdown'];
$randomthirtypercentdown_torrent = $TORRENT['randomthirtypercentdown'];
$largesize_torrent = $TORRENT['largesize'];
$largepro_torrent = $TORRENT['largepro'];
$expirehalfleech_torrent = $TORRENT['expirehalfleech'];
$expirefree_torrent = $TORRENT['expirefree'];
$expiretwoup_torrent = $TORRENT['expiretwoup'];
$expiretwoupfree_torrent = $TORRENT['expiretwoupfree'];
$expiretwouphalfleech_torrent = $TORRENT['expiretwouphalfleech'];
$expirethirtypercentleech_torrent = $TORRENT['expirethirtypercentleech'];
$expirenormal_torrent = $TORRENT['expirenormal'];
$hotdays_torrent = $TORRENT['hotdays'];
$hotseeder_torrent = $TORRENT['hotseeder'];
$halfleechbecome_torrent = $TORRENT['halfleechbecome'];
$freebecome_torrent = $TORRENT['freebecome'];
$twoupbecome_torrent = $TORRENT['twoupbecome'];
$twoupfreebecome_torrent = $TORRENT['twoupfreebecome'];
$twouphalfleechbecome_torrent = $TORRENT['twouphalfleechbecome'];
$thirtypercentleechbecome_torrent = $TORRENT['thirtypercentleechbecome'];
$normalbecome_torrent = $TORRENT['normalbecome'];
$uploaderdouble_torrent = $TORRENT['uploaderdouble'];
$deldeadtorrent_torrent = $TORRENT['deldeadtorrent'];

//新增加功能开始
//自定义考核
$assessment = $CHECK['enablead'];
$assessmentconvert = $CHECK['convert'];
$assessmentmanage = $CHECK['checkmanage'];
$assessmentname = $CHECK['checkname'];
$assessmenttime = $CHECK['checktime'];
$assessmentstart = $CHECK['checkstart'];
$assessmentdate = $CHECK['checkdate'];
$assessmentul = $CHECK['checkul'];
$assessmentdl = $CHECK['checkdl'];
$assessmentradio = $CHECK['checkradio'];
$assessmentbonus = $CHECK['checkbonus'];
$assessmentultime = $CHECK['checkultime'] * 86400;
$assessmentdltime = $CHECK['checkdltime'] * 86400;
$assessmenttr = $CHECK['checktr'];
$assessmentfa = $CHECK['checkfa'];
//新增加功能结束
//自定义全局变量
$stronghruntil = 14 * 86400;
$stronghr_time = 1 * 86400;
$stronghr_radio = 2;

foreach ($CONFIGURATIONS as $CONFIGURATION) {
	unset($GLOBALS[$CONFIGURATION]);
}

//Directory for subs
$SUBSPATH = "subs";

//定时更新Tracker的方式
$useCronTriggerCleanUp = TRUE;

//some promotion rules
//$promotionrules_torrent = array(0 => array("mediumid" => array(1), "promotion" => 5), 1 => array("mediumid" => array(3), "promotion" => 5), 2 => array("catid" => array(402), "standardid" => array(3), "promotion" => 4), 3 => array("catid" => array(403), "standardid" => array(3), "promotion" => 4));
$promotionrules_torrent = array();
