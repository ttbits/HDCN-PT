<?php

function stdhead($title = "", $msgalert = true) {
	global $lang_functions;
	global $CURUSER, $CURLANGDIR, $USERUPDATESET, $iplog1, $oldip, $SITE_ONLINE, $SITENAME, $SLOGAN, $logo_main, $logo_width, $logo_height, $offlinemsg, $showversion, $enabledonation, $staffmem_class, $titlekeywords_tweak, $metakeywords_tweak, $metadescription_tweak, $cssdate_tweak, $deletenotransfertwo_account, $neverdelete_account, $iniupload_main, $registration, $recharge, $assessment, $hr, $hrhit, $claim, $claimnum, $ipv6signup, $officialinvites, $viewlimit, $freeviewlimit, $newuser, $onsecurity, $limitsecurity, $ratioless, $authoff, $ttkoff, $bindrewardoff, $bindrewardnum, $loginrewardoff, $loginrewardmul, $stronghr, $stronghr_radio;
	global $tstart, $snow;
	global $Cache;
	global $Advertisement;
	$Cache->setLanguage($CURLANGDIR);
	$Advertisement = new ADVERTISEMENT($CURUSER['id']);
// Variable for Start Time
	$tstart = getmicrotime(); // Start time
//Insert old ip into iplog
	if ($CURUSER) {
		if ($iplog1 == "yes") {
			if (($oldip != $CURUSER["ip"]) && $CURUSER["ip"])
				sql_query("INSERT INTO iplog (ip, userid, access) VALUES (" . sqlesc($CURUSER['ip']) . ", " . $CURUSER['id'] . ", '" . $CURUSER['last_access'] . "')");
		}
		$USERUPDATESET[] = "last_access = " . sqlesc(date("Y-m-d H:i:s"));
		$USERUPDATESET[] = "ip = " . sqlesc($CURUSER['ip']);
	}
	header("Content-Type: text/html; charset=utf-8; Cache-control:private");
//header("Pragma: No-cache");
	if ($title == "")
		$title = $SITENAME;
	else
		$title = $SITENAME . " :: " . htmlspecialchars($title);
	if ($titlekeywords_tweak)
		$title .= " " . htmlspecialchars($titlekeywords_tweak);
	$title .= $showversion;
	if ($SITE_ONLINE == "no") {
		if (get_user_class() < UC_ADMINISTRATOR) {
			die($lang_functions['std_site_down_for_maintenance']);
		} else {
			$offlinemsg = true;
		}
	}
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<?php
			if ($metakeywords_tweak) {
				?>
				<meta name="keywords" content="<?php echo htmlspecialchars($metakeywords_tweak) ?>" />
				<?php
			}
			if ($metadescription_tweak) {
				?>
				<meta name="description" content="<?php echo htmlspecialchars($metadescription_tweak) ?>" />
				<?php
			}
			?>
			<meta name="generator" content="<?= PROJECTNAME ?>" />
			<?php
			print(get_style_addicode());
			$css_uri = get_css_uri();
			$cssupdatedate = ($cssdate_tweak ? "?v=" . htmlspecialchars($cssdate_tweak) : "");
			?>
			<title><?= $title ?></title>
			<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
			<link rel="alternate" type="application/rss+xml" title="Latest Torrents" href="torrentrss.php" />
			<link rel="search" type="application/opensearchdescription+xml" title="<?= $SITENAME ?> Torrents" href="opensearch.php" />
			<link rel="stylesheet" href="userAutoTips.css" type="text/css"  />
			<link rel="stylesheet" href="<?= get_font_css_uri() . $cssupdatedate ?>" type="text/css" />
			<link rel="stylesheet" href="<?= get_forum_pic_folder() . "/forumsprites.css" . $cssupdatedate ?>" type="text/css" />
			<link rel="stylesheet" href="<?= $css_uri . "theme.css" . $cssupdatedate ?>" type="text/css" />
			<link rel="stylesheet" href="<?= $css_uri . "DomTT.css" . $cssupdatedate ?>" type="text/css" />
			<link rel="stylesheet" href="styles/curtain_imageresizer.css<?= $cssupdatedate ?>" type="text/css" />
			<link rel="stylesheet" href="styles/sprites.css<?= $cssupdatedate ?>" type="text/css" />
			<link rel="stylesheet" href="jquerylib/jquery.alerts.css<?= $cssupdatedate ?>" type="text/css" />
			<link rel="stylesheet" href="jquerylib/sceditor/minified/themes/modern.min.css<?= $cssupdatedate ?>" type="text/css" />
			<link rel="stylesheet" href="jquerylib/cxcolor/css/jquery.cxcolor.css<?= $cssupdatedate ?>" type="text/css" />
			<link rel="stylesheet" href="jquerylib/buttons.css<?= $cssupdatedate ?>" type="text/css" />
			<?php
			if ($CURUSER) {
				$caticonrow = get_category_icon_row($CURUSER['caticon']);
				if ($caticonrow['cssfile']) {
					print("<link rel=\"stylesheet\" href=" . htmlspecialchars($caticonrow['cssfile']) . $cssupdatedate . " type=\"text/css\" />");
				}
			}
			?>
			<script type="text/javascript" src="curtain_imageresizer.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="ajaxbasic.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="common.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="domLib.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="domTT.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="domTT_drag.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="fadomatic.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="userAutoTips.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="jquerylib/jquery-1.7.2.min.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="jquerylib/jquery.alerts.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="jquerylib/jquery.ui.draggable.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="jquerylib/jquery.caretInsert.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="jquerylib/echo.min.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="jquerylib/scrolltofixed-min.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="jquerylib/date/WdatePicker.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="jquerylib/sceditor_1.4.5/minified/jquery.sceditor.bbcode.min.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="jquerylib/sceditor_1.4.5/languages/cn.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="jquerylib/tietuku/tietuku.dialog.js<?= $cssupdatedate ?>"></script>
			<script type="text/javascript" src="jquerylib/cxcolor/cxcolor.min.js<?= $cssupdatedate ?>"></script>
			<?php if ($snow == 'yes') { ?>
				<script type="text/javascript" src="jquerylib/xue/snowfall.js<?= $cssupdatedate ?>"></script>
			<?php } ?>
		</head>
		<body>
			<script type="text/javascript">
				//图片懒加载
				var echo;
				echo.init({offsetBottom: 1000});
				function ClickDisplay(targetid) {
					var target = document.getElementById(targetid);
					if (target.style.display == "block" || target.style.display == "") {
						target.style.display = "none";
					} else {
						target.style.display = "block";
					}
				}
				function ClickVisible(targetid) {
					var target = document.getElementById(targetid);
					if (target.style.visibility == "visible" || target.style.visibility == "") {
						target.style.visibility = "hidden";
					} else {
						target.style.visibility = "visible";
					}
				}

				$(document).ready(function () {
					//导航跟随--开始
					$('#nav').scrollToFixed();
					//导航跟随--结束
					$('#roll_top').click(function () {
						$('html,body').animate({scrollTop: '0px'}, 800);
					});
					$('#roll_bottom').click(function () {
						$('html,body').animate({scrollTop: $('#footer').offset().top}, 800);
					});
					$('#roll_danmu').click(function () {
						ClickVisible('danmuku');
					});
					t_ids = $('textarea');
					if (t_ids) {
						for (var i = 0; i < t_ids.length; i++) {
							userAutoTips({id: t_ids[i].id});
						}
					}
					//编辑器--开始
	<?php if ($ttkoff == 'yes') { ?>
						//自定义图片上传按钮--开始
						$.sceditor.command.set("tietuku_upload", {
							exec: function () {
								var jq = jQuery.noConflict();
								//jq.dialog.showIframeDialog(900, '', '<iframe frameborder="0" width="880" height="450" marginheight="0" marginwidth="0" src="//u.shenzhan.in"></iframe>');//贴图库插件
								jq.dialog.showIframeDialog(900, '', '<iframe frameborder="0" width="880" height="450" marginheight="0" marginwidth="0" src="tietuku_upload.php"></iframe>'); //贴图库插件
								//jq.dialog.showIframeDialog(900, '', '<iframe frameborder="0" width="880" height="650" marginheight="0" marginwidth="0" src="image_upload.php"></iframe>');//SM.MS插件
								var $ = jQuery.noConflict();
							},
							tooltip: "使用贴图库图片上传"
						});
						//自定义图片上传按钮--结束
	<?php } ?>
					//自定义图片上传按钮--开始
					$.sceditor.command.set("image_upload", {
						exec: function () {
							var jq = jQuery.noConflict();
							//jq.dialog.showIframeDialog(900, '', '<iframe frameborder="0" width="880" height="450" marginheight="0" marginwidth="0" src="//u.shenzhan.in"></iframe>');//贴图库插件
							//jq.dialog.showIframeDialog(900, '', '<iframe frameborder="0" width="880" height="650" marginheight="0" marginwidth="0" src="image_upload.php"></iframe>');//SM.MS插件
							//jq.dialog.showIframeDialog(900, '', '<iframe frameborder="0" width="880" height="650" marginheight="0" marginwidth="0" src="//u.shenzhan.in/image_upload"></iframe>');//SM.MS插件
							window.open('http://www.clantu.com/register/i/dAszQ1');
							var $ = jQuery.noConflict();
						},
						//tooltip: "图片上传"
						tooltip: "ClanTu图床"
					});
					//自定义图片上传按钮--结束
					$("#sceditor").sceditor({
						plugins: "bbcode",
						locale: "cn",
						toolbar: "bold,italic,underline,strike,subscript,superscript,font,size,color,removeformat,pastetext,bulletlist,orderedlist,code,quote,horizontalrule,image,image_upload,<?= $ttkoff == 'yes' ? 'tietuku_upload,' : '' ?>link,unlink,date,time,maximize,source",
						style: "jquerylib/sceditor/minified/jquery.sceditor.default.min.css",
						bbcodeTrim: true,
						autoUpdate: true
					});
					//编辑器--结束
					(function () {
						var color = $('#cxcolortitle'); //标题颜色选择器
						var title = $('#name');
						color.bind('change', function () {
							title.css('color', this.value);
							$('#cxcolortitle').attr("value", this.value);
						});
						color.cxColor(function (api) {
							$('#btn_show').bind('click', function () {
								api.show();
							});
						});
					})();
					(function () {
						var color = $('#cxcolordesc'); //副标题颜色选择器
						var title = $('#small_descr');
						color.bind('change', function () {
							title.css('color', this.value);
							$('#cxcolordesc').attr("value", this.value);
						});
						color.cxColor(function (api) {
							$('#btn_show').bind('click', function () {
								api.show();
							});
						});
					})();
				});
			</script>
			<style type="text/css">
				html body {
					_background-image:url(about:blank);
					_background-attachment:fixed;
				}
				#roll_top, #roll_bottom, #roll_danmu {
					position:relative;
					cursor:pointer;
					height:50px;
					width:50px;
				}
				#roll_top {
					background:url(pic/up.png) no-repeat;
				}
				#roll_bottom {
					background:url(pic/down.png) no-repeat;
				}
				#roll_danmu {
					background:url(pic/danmu.png) no-repeat;
				}
				#roll {
					display:block;
					position:fixed;
					width:50px;
					right:20px;
					bottom:210px;
				}
			</style>
			<!--<div id="roll"><div title="回到顶部" id="roll_top"></div><div title="转到底部" id="roll_bottom"></div><?php if ($CURUSER['danmu'] == 0) { ?><div title="弹幕" id="roll_danmu"></div><?php } ?></div>-->
			<div id="roll"><div title="回到顶部" id="roll_top"></div><div title="转到底部" id="roll_bottom"></div></div>
			<!--有道API-->
			<!--
			<div id="YOUDAO_SELECTOR_WRAPPER" style="display:none; margin:0; border:0; padding:0; width:320px; height:240px;"></div>
			<script type="text/javascript" src="jquerylib/youdaoapi.js"></script>
			-->
			<!--有道API-->
			<div class="snow-container" style="position:fixed;top:0;left:0;width:100%;height:100%;pointer-events:none;z-index:100001;"></div>
			<table class="head" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td class="clear">
						<?php if ($logo_main == "") { ?>
							<div class="logo"><?php echo htmlspecialchars($SITENAME) ?></div>
							<div class="slogan"><?php echo htmlspecialchars($SLOGAN) ?></div>
						<?php } else { ?>
							<div><a href="index.php"><img style="width: <?= $logo_width ?>px;height: <?= $logo_height ?>px" src="<?= $logo_main . $cssupdatedate ?>" alt="<?php echo htmlspecialchars($SITENAME) ?>" title="<?php echo htmlspecialchars($SITENAME) ?> - <?php echo htmlspecialchars($SLOGAN) ?>" /></a></div>
						<?php } ?>
					</td>
					<td class="clear nowrap" align="right" valign="middle">
						<?php
						if ($Advertisement->enable_ad()) {
							$headerad = $Advertisement->get_ad('header');
							if ($headerad) {
								echo "<span id=\"ad_header\">" . $headerad[0] . "</span>";
							}
						}
						if ($enabledonation == 'yes') {
							?>
							<a href="donate.php"><img src="<?php echo get_forum_pic_folder() ?>/donate.gif" alt="捐助" style="margin-left: 5px; margin-top: 50px;" /></a>
							<?php
						}
						?>
					</td>
				</tr>
			</table>

			<table class="mainouter" width="90%" cellspacing="0" cellpadding="5" align="center">
				<tr><td id="nav_block" class="text" align="center">
						<?php
						if (!$CURUSER) {
							if ($authoff == 'yes') {
								?>
								<a href="auth.php"><font class="big"><b>扫码安全登录</b></font></a>｜<?php } ?><a href="login.php"><font class="big"><b><?php echo $lang_functions['text_login'] ?></b></font></a><?php if ($registration == 'yes') { ?>｜<a href="signup.php"><font class="big"><b><?php echo $lang_functions['text_signup'] ?></b></font></a><?php } if ($ipv6signup == 'yes' && $registration != 'yes') { ?> ｜<a href="signup_ipv6.php"><font class="big"><b>高校IPv6注册通道</b></font></a><?php
							} if ($officialinvites == 'yes' && $registration == 'no') {
								print(" ｜ <a href=\"invitebox.php\"><font class=\"big\"><b>官邀</b></font></a>");
							}
						} else {
							begin_main_frame();
							menu();
							end_main_frame();

							$datum = getdate();
							$datum["hours"] = sprintf("%02.0f", $datum["hours"]);
							$datum["minutes"] = sprintf("%02.0f", $datum["minutes"]);
							$ratio = get_ratio($CURUSER['id']);

							//// check every 15 minutes //////////////////
							$messages = $Cache->get_value('user_' . $CURUSER["id"] . '_inbox_count');
							if ($messages == "") {
								$messages = get_row_count("messages", "WHERE receiver=" . sqlesc($CURUSER["id"]) . " AND location<>0");
								$Cache->cache_value('user_' . $CURUSER["id"] . '_inbox_count', $messages, 900);
							}
							$outmessages = $Cache->get_value('user_' . $CURUSER["id"] . '_outbox_count');
							if ($outmessages == "") {
								$outmessages = get_row_count("messages", "WHERE sender=" . sqlesc($CURUSER["id"]) . " AND saved='yes'");
								$Cache->cache_value('user_' . $CURUSER["id"] . '_outbox_count', $outmessages, 900);
							}
							if (!$connect = $Cache->get_value('user_' . $CURUSER["id"] . '_connect')) {
								$row = mysql_fetch_row(sql_query("SELECT connectable FROM peers WHERE userid=" . sqlesc($CURUSER["id"]) . " LIMIT 1"));
								if ($row)
									$connect = $row[0];
								else
									$connect = 'unknown';
								$Cache->cache_value('user_' . $CURUSER["id"] . '_connect', $connect, 900);
							}

							if ($connect == "yes")
								$connectable = "<b><font color=\"green\">" . $lang_functions['text_yes'] . "</font></b>";
							elseif ($connect == 'no')
								$connectable = "<a href=\"faq.php\"><b><font color=\"red\">" . $lang_functions['text_no'] . "</font></b></a>";
							else
								$connectable = $lang_functions['text_unknown'];

							//// check every 60 seconds //////////////////
							$activeseed = $Cache->get_value('user_' . $CURUSER["id"] . '_active_seed_count');
							if ($activeseed == "") {
								$activeseed = get_row_count("peers", "WHERE userid=" . sqlesc($CURUSER["id"]) . " AND seeder='yes'");
								$Cache->cache_value('user_' . $CURUSER["id"] . '_active_seed_count', $activeseed, 60);
							}
							$activeleech = $Cache->get_value('user_' . $CURUSER["id"] . '_active_leech_count');
							if ($activeleech == "") {
								$activeleech = get_row_count("peers", "WHERE userid=" . sqlesc($CURUSER["id"]) . " AND seeder='no'");
								$Cache->cache_value('user_' . $CURUSER["id"] . '_active_leech_count', $activeleech, 60);
							}
							$unread = $Cache->get_value('user_' . $CURUSER["id"] . '_unread_message_count');
							if ($unread == "") {
								$unread = get_row_count("messages", "WHERE receiver=" . sqlesc($CURUSER["id"]) . " AND unread='yes'");
								$Cache->cache_value('user_' . $CURUSER["id"] . '_unread_message_count', $unread, 60);
							}

							$inboxpic = "<img class=\"" . ($unread ? "inboxnew" : "inbox") . "\" src=\"pic/trans.gif\" alt=\"inbox\" title=\"" . ($unread ? $lang_functions['title_inbox_new_messages'] : $lang_functions['title_inbox_no_new_messages']) . "\" />";
							if ($connect == 'no') {
								if (!$Cache->get_value('connectfaq_' . $CURUSER['id'])) {
									$Cache->cache_value('connectfaq_' . $CURUSER['id'], '1', 60);
								}
							}
							$hrstatus = mysql_fetch_array(sql_query("SELECT hr FROM users WHERE id=" . $CURUSER['id'])) or sqlerr(__FILE__, __LINE__);
							$claimstatus = mysql_num_rows(sql_query("SELECT id FROM claim WHERE userid=" . $CURUSER['id']));
							?>
							<table id="info_block" cellpadding="4" cellspacing="0" border="0" width="100%"><tr>
									<td><table width="100%" cellspacing="0" cellpadding="0" border="0"><tr>
												<td class="bottom" align="left"><span class="medium"><?php echo $lang_functions['text_welcome_back'] ?>，<?= get_username($CURUSER['id']) ?> [<a href="usercp.php">控制台</a>] [<a href="logout.php"><?php echo $lang_functions['text_logout'] ?></a>]<?php
														if (get_user_class() == UC_UPLOADER || get_user_class() == UC_FORWARD) {
															echo" [<a href='uploaders.php'>发布转载组考核</a>]";
														}
														if (get_user_class() == $staffmem_class) {
															echo" [<a href='moderators.php'>管理员考核</a>]";
														}
														if (get_user_class() >= $staffmem_class) {
															echo" [<a href='newuser.php'>查看新用户</a>]";
														}
														if (get_user_class() >= $staffmem_class) {
															echo" [<a href='notice.php'>小公告设置</a>]";
														}
														if (get_user_class() >= $staffmem_class) {
															echo " [<a href='staffpanel.php'>" . $lang_functions['text_staff_panel'] . "</a>]";
														}
														if (get_user_class() >= UC_SYSOP) {
															echo " [<a href='settings.php'>" . $lang_functions['text_site_settings'] . "</a>]";
														}
														?> [<a href="mytruck.php" ><font style="color: #EE0000">小货车</font></a>] [<?php echo $lang_functions['text_bookmarks'] ?><a href="torrents.php?inclbookmarked=1&amp;allsec=1&amp;incldead=0">综合</a><?php (($limitsecurity == 'no' || get_user_class() >= UC_ADMINISTRATOR) ? (($onsecurity == 'no' || get_user_class() >= UC_ADMINISTRATOR) ? (get_user_class >= $viewlimit ? ((get_user_class() >= $freeviewlimit || $CURUSER['onlimit'] == 'yes') ? print(" / <a href=\"limit.php?inclbookmarked=1&amp;allsec=1&amp;incldead=0\">禁区</a>") : "") : "") : "") : "") ?>] <font class = 'color_bonus'><?php echo $lang_functions['text_bonus'] ?></font>[<a href="mybonus.php"><?php echo $lang_functions['text_use'] ?></a><?php if ($recharge == 'yes') { ?>丨<a href="recharge.php"><font style="color: #0000FF">充值</font></a><?php } ?>丨<a href="giftmanager.php"><b style="color: #EE0000">红包</b></a>丨<a href="usebonus.php"><?= $lang_functions['text_app'] ?></a>丨<a href="blackjack.php"><font style="color: #EE0000"><b>21点对战版</b></font></a>]：<?php echo number_format((int) $CURUSER['seedbonus'], 0) ?> <font class = 'color_bonus'>荣誉值：</font><?= number_format($CURUSER['big']) ?> <font class = 'color_invite'><?php echo $lang_functions['text_invite'] ?></font>[<a href="invite.php?id=<?php echo $CURUSER['id'] ?>"><?php echo $lang_functions['text_send'] ?></a>]：<?php echo ($CURUSER['invites'] < 0 ? "∞" : $CURUSER['invites']) ?> | <a href="2048.php">[<b style="color:red">智 商 签 到</b>]</a><br />
														<?php if ($ratioless == 'no') { ?><font class='color_ratio'><?php echo $lang_functions['text_ratio'] ?></font> <?php echo $ratio ?><?php } ?>
														<font class='color_uploaded'><?php echo $lang_functions['text_uploaded'] ?></font> <?php echo mksize($CURUSER['uploaded']) ?>
														<?php if ($ratioless == 'no') { ?><font class='color_downloaded'> <?php echo $lang_functions['text_downloaded'] ?></font> <?php echo mksize($CURUSER['downloaded']) ?><?php } ?>
														<font class='color_active'><?php echo $lang_functions['text_active_torrents'] ?></font> <a href="userdetails.php?id=<?php echo $CURUSER['id']; ?>&show=seeding"><img class="uploading" alt="Torrents seeding" title="<?php echo $lang_functions['title_torrents_seeding'] ?>"  src="pic/trans.gif" /> <?php echo $activeseed ?></a>
														<a href="userdetails.php?id=<?php echo $CURUSER['id']; ?>&show=leeching"><img class="downloading" alt="Torrents leeching" title="<?php echo $lang_functions['title_torrents_leeching'] ?>" src="pic/trans.gif" /> <?php echo $activeleech ?></a>
														<!--<a href="userdetails.php?id=<?php echo $CURUSER['id']; ?>&show=completed" title="已完成"><img class="completed" alt="completed" title="已完成"  src="pic/trans.gif" />--</a>
														<a href="userdetails.php?id=<?php echo $CURUSER['id']; ?>&show=uploaded" title="已发布"><img class="uploaded" alt="uploaded" title="已发布"  src="pic/trans.gif" />--</a>-->
														<?php if ($hr == 'yes') { ?><font style="color: #EE0000">已得H&R：</font> <a href="hr.php"><?php echo $hrstatus['hr'] . "/" . ($stronghr == 'no' ? $hrhit : $stronghr_radio) ?></a><?php } ?>
														<?php if ($claim == 'yes') { ?><font style="color: #0000FF">我的认领：</font> <a href="viewclaim.php"><?php echo $claimstatus . "/" . $claimnum ?></a><?php } ?>
														<font class='color_connectable'><?php echo $lang_functions['text_connectable'] ?></font><?php echo $connectable ?> <?php echo maxslots(); ?></span><?php if ($authoff == 'yes' && $CURUSER['secken'] == 'no') { ?>｜<a href="auth.php"><font style="color:red">绑定扫码安全登录<?= ($bindrewardoff == 'yes' && !empty($bindrewardnum) ? "。现在绑定可一次性获得 $bindrewardnum 个魔力值" : "") ?><?= ($loginrewardoff == 'yes' && !empty($loginrewardmul) ? "，且登录奖励翻 $loginrewardmul 倍哦" : "") ?></font></a><?php } ?></td>
												<td class="bottom" align="right"><span class="medium"><?php echo $lang_functions['text_the_time_is_now'] ?><?php echo $datum[hours] . ":" . $datum[minutes] ?><br />
														<?php
														if (get_user_class() >= $staffmem_class) {
															$totalreports = $Cache->get_value('staff_report_count');
															if ($totalreports == "") {
																$totalreports = get_row_count("reports");
																$Cache->cache_value('staff_report_count', $totalreports, 900);
															}
															$totalsm = $Cache->get_value('staff_message_count');
															if ($totalsm == "") {
																$totalsm = get_row_count("staffmessages");
																$Cache->cache_value('staff_message_count', $totalsm, 900);
															}
															$totalcheaters = $Cache->get_value('staff_cheater_count');
															if ($totalcheaters == "") {
																$totalcheaters = get_row_count("cheaters");
																$Cache->cache_value('staff_cheater_count', $totalcheaters, 900);
															}
															print("<a href = \"cheaterbox.php\"><img class=\"cheaterbox\" alt=\"cheaterbox\" title=\"" . $lang_functions['title_cheaterbox'] . "\" src=\"pic/trans.gif\" />  </a>" . $totalcheaters . "  <a href=\"reports.php\"><img class=\"reportbox\" alt=\"reportbox\" title=\"" . $lang_functions['title_reportbox'] . "\" src=\"pic/trans.gif\" />  </a>" . $totalreports . "  <a href=\"staffbox.php\"><img class=\"staffbox\" alt=\"staffbox\" title=\"" . $lang_functions['title_staffbox'] . "\" src=\"pic/trans.gif\" />  </a>" . $totalsm . "  ");
														}

														print("<a href=\"messages.php\">" . $inboxpic . "</a> " . ($messages ? $messages . " (" . $unread . $lang_functions['text_message_new'] . ")" : "0"));
														print("  <a href=\"messages.php?action=viewmailbox&amp;box=-1\"><img class=\"sentbox\" alt=\"sentbox\" title=\"" . $lang_functions['title_sentbox'] . "\" src=\"pic/trans.gif\" /></a> " . ($outmessages ? $outmessages : "0"));
														print(" <a href=\"friends.php\"><img class=\"buddylist\" alt=\"Buddylist\" title=\"" . $lang_functions['title_buddylist'] . "\" src=\"pic/trans.gif\" /></a>");
														print(" <a href=\"getrss.php\"><img class=\"rss\" alt=\"RSS\" title=\"" . $lang_functions['title_get_rss'] . "\" src=\"pic/trans.gif\" /></a>");
														?>

													</span></td>
											</tr></table></td>
								</tr></table>
						</td></tr>

					<tr><td id="outer" align="center" class="outer" style="padding-top: 20px; padding-bottom: 20px" width="95%">
							<?php
							//小公告
							$res = mysql_fetch_array(sql_query("SELECT noticeoff, notice FROM notice ORDER BY id DESC LIMIT 1"));
							if ($res['noticeoff'] == 'yes') {
								?>
								<table style="border:2px solid #000000; background:none repeat scroll;" align="center">
									<td>
										<div style="margin:10px;">
											<?php
											echo format_comment($res['notice']);
											?>
										</div>
									</td>
								</table><br />
								<?php
							}
							//新人考核
							if ($newuser == 'yes') {
								global $kstime, $dl, $ul, $sb, $ra, $dltime, $ultime, $tr, $fa, $newusermanage_class;
								$userstatus = mysql_fetch_array(sql_query("SELECT donor,  vip_added FROM users WHERE id = " . $CURUSER['id'])) or sqlerr(__FILE__, __LINE__);
								$newusercheck = mysql_fetch_array(sql_query("SELECT newuser FROM users WHERE id = " . $CURUSER['id'])) or sqlerr(__FILE__, __LINE__);
								$fanum = sql_query("SELECT COUNT(*) FROM users WHERE owner = " . $CURUSER['id']);
								if (!empty($fanum)) {
									$fanum = $fanum;
								} else {
									$fanum = 0;
								}
								$examcheck = mysql_fetch_array(sql_query("SELECT downloaded, uploaded, seedbonus, seedtime, leechtime, added, newuser FROM users WHERE id = " . $CURUSER['id'])) or sqlerr(__FILE__, __LINE__);
								$ks = date("Y-m-d H:i:s", strtotime($examcheck['added'] . " + " . $kstime . " days"));
								if ($userstatus['donor'] == 'no') {
									if ($newusercheck['newuser'] == 'yes') {
										if (get_user_class() < $newusermanage_class || $userstatus['vip_added'] == 'yes' && (round((strtotime($ks) - strtotime(date("Y-m-d H:i:s"))) / 86400)) > "0") {
											?>
											<table style="border:2px solid #000000; background:none repeat scroll;" align="center">
												<td><div style="margin:10px">
														<?php if (!empty($dl)) { ?>
															<p style="font-size: 10pt; line-height: 1.5;"><b>目前下载量：<?php
																	echo mksize($examcheck['downloaded']) . "/" . mksize($dl);
																	$examcheck['downloaded'] >= $dl ? "" : print " (未达标)";
																	?></b></p>
														<?php } if (!empty($ul)) { ?>
															<p style="font-size: 10pt; line-height: 1.5;"><b>目前上传量：<?php
																	echo mksize($examcheck['uploaded']) . "/" . mksize($ul);
																	$examcheck['uploaded'] >= $ul ? "" : print " (未达标)";
																	?></b></p>
														<?php } if (!empty($sb)) { ?>
															<p style="font-size: 10pt; line-height: 1.5;"><b>目前魔力值：<?php
																	echo number_format($examcheck['seedbonus']) . "/" . $sb;
																	$examcheck['seedbonus'] >= $sb ? "" : print " (未达标)";
																	?></b></p>
														<?php } if (!empty($ultime)) { ?>
															<p style="font-size: 10pt; line-height: 1.5;"><b>目前做种时间：<?php
																	echo mkprettytime($examcheck['seedtime']) . "/" . mkprettytime($ultime);
																	$examcheck['seedtime'] >= $ultime ? "" : print " (未达标)";
																	?></b></p>
														<?php } if (!empty($dltime)) { ?>
															<p style="font-size: 10pt; line-height: 1.5;"><b>目前下载时间：<?php
																	echo mkprettytime($examcheck['leechtime']) . "/" . mkprettytime($dltime);
																	$examcheck['leechtime'] >= $dltime ? "" : print " (未达标)";
																	?></b></p>
														<?php } if (!empty($tr)) { ?>
															<p style="font-size: 10pt; line-height: 1.5;"><b>目前做种/下载时间比率：<?php
																	echo round($examcheck['seedtime'] / $examcheck['leechtime'], 3) . "/$tr";
																	($examcheck['seedtime'] / $examcheck['leechtime']) >= $tr ? "" : print " (未达标)";
																	?></b></p>
														<?php } if (!empty($ra)) { ?>
															<p style="font-size: 10pt; line-height: 1.5;"><b>目前分享比率：<?php
																	echo round($examcheck['uploaded'] / $examcheck['downloaded'], 3) . "/$ra";
																	($examcheck['uploaded'] / $examcheck['downloaded']) >= $ra ? "" : print " (未达标)";
																	?></b></p>
														<?php } if (!empty($fa)) { ?>
															<p style="font-size: 10pt; line-height: 1.5;"><b>目前发种数量：<?php
																	echo "$fanum/$fa";
																	$fanum >= $fa ? "" : print " (未达标)";
																	?></b></p>
														<?php } ?>
														<p style="font-size: 10pt; line-height: 1.5;"><b>距离新人考核结束还有<?php echo round((strtotime($ks) - strtotime(date("Y-m-d H:i:s"))) / 86400); ?>天，请保持达标状态直到通过，加油！</b></p>
														<p style="font-size: 10pt; line-height: 1.5;"><b>注意：自注册之日起开始考核分享率，请谨慎下载。</b></p>
													</div></td>
											</table><br />
											<?php
										}
									}
								}
							}
							?>

							<?php
							//自定义考核
							if ($assessment == 'yes') {
								global $assessmentbonus, $assessmentdate, $assessmentdl, $assessmentdltime, $assessmentname, $assessmentradio, $assessmenttime, $assessmenttr, $assessmentul, $assessmentultime, $assessmentmanage, $assessmentstart, $assessmentfa;
								$fanum = sql_query("SELECT COUNT(*) FROM users WHERE owner = " . $CURUSER['id']);
								if (!empty($fanum)) {
									$fanum = $fanum;
								} else {
									$fanum = 0;
								}
								$userstatus = mysql_fetch_array(sql_query("SELECT donor,  vip_added FROM users WHERE id = " . $CURUSER['id'])) or sqlerr(__FILE__, __LINE__);
								$customcheck = mysql_fetch_array(sql_query("SELECT added, (downloaded - exdownloaded) AS downloaded, (uploaded - exuploaded) AS uploaded, (seedbonus - exseedbonus) AS seedbonus, (seedtime - exseedtime) AS seedtime, (leechtime - exleechtime) AS leechtime, ($fanum - extorrent) AS fatorrent FROM users WHERE id=" . $CURUSER['id'])) or sqlerr(__FILE__, __LINE__);
								$assks = date("Y-m-d", strtotime($assessmentstart . " + " . $assessmenttime . " days"));
								if ($userstatus['donor'] == 'no') {
									if ($newusercheck['newuser'] == 'no') {
										if (date("Y-m-d", time()) >= $assessmentstart) {
											if (get_user_class() < $assessmentmanage || $userstatus['vip_added'] == 'yes' && date("Y-m-d", strtotime($customcheck['added'])) <= $assessmentdate && (round((strtotime($ks) - strtotime(date("Y-m-d"))) / 86400)) > "0") {
												?>
												<table style="border:2px solid #000000; background:none repeat scroll;" align="center">
													<td><div style="margin:10px">
															<?php if (!empty($assessmentdl)) { ?>
																<p style="font-size: 10pt; line-height: 1.5;"><b>目前下载量增量：<?php
																		echo mksize($customcheck['downloaded']) . "/" . mksize($assessmentdl);
																		$customcheck['downloaded'] >= $assessmentdl ? "" : print " (未达标)";
																		?></b></p>
															<?php } if (!empty($assessmentul)) { ?>
																<p style="font-size: 10pt; line-height: 1.5;"><b>目前上传量增量：<?php
																		echo mksize($customcheck['uploaded']) . "/" . mksize($assessmentul);
																		$customcheck['uploaded'] >= $assessmentul ? "" : print " (未达标)";
																		?></b></p>
															<?php } if (!empty($assessmentbonus)) { ?>
																<p style="font-size: 10pt; line-height: 1.5;"><b>目前魔力值增量：<?php
																		echo number_format($customcheck['seedbonus']) . "/" . $assessmentbonus;
																		$customcheck['seedbonus'] >= $assessmentbonus ? "" : print " (未达标)";
																		?></b></p>
															<?php } if (!empty($assessmentultime)) { ?>
																<p style="font-size: 10pt; line-height: 1.5;"><b>目前做种时间增量：<?php
																		echo mkprettytime($customcheck['seedtime']) . "/" . mkprettytime($assessmentultime);
																		$customcheck['seedtime'] >= $assessmentultime ? "" : print " (未达标)";
																		?></b></p>
															<?php } if (!empty($assessmentdltime)) { ?>
																<p style="font-size: 10pt; line-height: 1.5;"><b>目前下载时间增量：<?php
																		echo mkprettytime($customcheck['leechtime']) . "/" . mkprettytime($assessmentdltime);
																		$customcheck['leechtime'] >= $assessmentdltime ? "" : print " (未达标)";
																		?></b></p>
															<?php } if (!empty($assessmenttr)) { ?>
																<p style="font-size: 10pt; line-height: 1.5;"><b>目前做种/下载时间比率增量：<?php
																		echo round($customcheck['seedtime'] / $customcheck['leechtime'], 3) . "/$assessmenttr";
																		($customcheck['seedtime'] / $customcheck['leechtime']) >= $assessmenttr ? "" : print " (未达标)";
																		?></b></p>
															<?php } if (!empty($assessmentradio)) { ?>
																<p style="font-size: 10pt; line-height: 1.5;"><b>目前分享比率增量：<?php
																		echo round($customcheck['uploaded'] / $customcheck['downloaded'], 3) . "/$assessmentradio";
																		($customcheck['uploaded'] / $customcheck['downloaded']) >= $assessmentradio ? "" : print " (未达标)";
																		?></b></p>
															<?php } if (!empty($assessmentfa)) { ?>
																<p style="font-size: 10pt; line-height: 1.5;"><b>目前发种数量增量：<?php
																		echo $customcheck['fatorrent'] . "/" . $assessmentfa;
																		$customcheck['fatorrent'] >= $assessmentfa ? "" : print " (未达标)";
																		?></b></p>
															<?php } ?>
															<p style="font-size: 10pt;
															   line-height: 1.5;
															   "><b>距离<?php echo $assessmentname ?>结束还有<?php echo round((strtotime($assks) - strtotime(date("Y-m-d"))) / 86400); ?>天，请保持达标状态直到通过，加油！</b></p>
														</div></td>
												</table><br />
												<?php
											}
										}
									}
								}
							}
							//每日登录奖励
							global $loginadd;
							if ($loginadd == 'yes') {
								$res = sql_query("SELECT salary, salarynum FROM users WHERE id = " . $CURUSER['id']) or sqlerr(__FILE__, __LINE__);
								$arr = mysql_fetch_assoc($res);
								$showtime = date("Y-m-d", time());
								$d1 = strtotime($showtime);
								$d2 = strtotime($arr['salary']);
								$Days = round(($d1 - $d2) / 3600 / 24);
								$addbonus = 10;
								if ($Days == 1) {
									$salarynum = $arr['salarynum'];
									if ($salarynum == 30) {
										if ($loginrewardoff == 'yes' && !empty($loginrewardmul) && $CURUSER['secken'] == 'yes') {
											$addbonus = 100 * $loginrewardmul;
										} else {
											$addbonus = 100;
										}
									} elseif ($salarynum == 60) {
										if ($loginrewardoff == 'yes' && !empty($loginrewardmul) && $CURUSER['secken'] == 'yes') {
											$addbonus = 500 * $loginrewardmul;
										} else {
											$addbonus = 500;
										}
									} elseif ($salarynum == 90) {
										if ($loginrewardoff == 'yes' && !empty($loginrewardmul) && $CURUSER['secken'] == 'yes') {
											$addbonus = 1000 * $loginrewardmul;
										} else {
											$addbonus = 1000;
										}
									} elseif ($salarynum > 90) {
										if ($loginrewardoff == 'yes' && !empty($loginrewardmul) && $CURUSER['secken'] == 'yes') {
											$addbonus = 100 * $loginrewardmul;
										} else {
											$addbonus = 100;
										}
									} else {
										if ($loginrewardoff == 'yes' && !empty($loginrewardmul) && $CURUSER['secken'] == 'yes') {
											$addbonus = ($addbonus + $salarynum) * $loginrewardmul;
										} else {
											$addbonus = $addbonus + $salarynum;
										}
									}
									mysql_query("UPDATE users SET seedbonus = seedbonus + $addbonus, salary = now(), salarynum = salarynum + 1 WHERE id = " . $CURUSER['id']);
									?>
									<script type="text/javascript">jAlert('<font color=red>连续登录<?= $salarynum ?>天奖励，恭喜你获得<?= $loginrewardoff == 'yes' && !empty($loginrewardmul) && $CURUSER['secken'] == 'yes' ? $addbonus / $loginrewardmul : $addbonus ?>点魔力值<?= $loginrewardoff == 'yes' && !empty($loginrewardmul) && $CURUSER['secken'] == 'yes' ? "，绑定安全登录额外获得" . $addbonus / $loginrewardmul . "点魔力值" : "" ?>，继续保持哦</font>', '每日登录奖励');</script>
									<?php
								} elseif ($Days > 0) {
									if ($loginrewardoff == 'yes' && !empty($loginrewardmul) && $CURUSER['secken'] == 'yes') {
										$addbonus = $addbonus * $loginrewardmul;
									}
									mysql_query("UPDATE users SET seedbonus = seedbonus + $addbonus, salary = now(), salarynum = 1 WHERE id = " . $CURUSER['id']);
									?>
									<script type="text/javascript">jAlert('<font color=red>恭喜你获得<?= $loginrewardoff == 'yes' && !empty($loginrewardmul) && $CURUSER['secken'] == 'yes' ? $addbonus / $loginrewardmul : $addbonus ?>点魔力值<?= $loginrewardoff == 'yes' && !empty($loginrewardmul) && $CURUSER['secken'] == 'yes' ? "，绑定安全登录额外获得" . $addbonus / $loginrewardmul . "点魔力值" : "" ?>，连续登录会有更多奖励哦</font>', '每日登录奖励');</script>
									<?php
								}
							}

							if ($Advertisement->enable_ad()) {
								$belownavad = $Advertisement->get_ad('belownav');
								if ($belownavad)
									echo "<div align = \"center\" style=\"margin-bottom: 10px\" id=\"ad_belownav\">" . $belownavad[0] . "</div>";
							}

							//信息色块提示
							if ($msgalert) {
								if ($CURUSER['leechwarn'] == 'yes') {
									$kicktimeout = gettime($CURUSER['leechwarnuntil'], false, false, true);
									$text = $lang_functions['text_please_improve_ratio_within'] . $kicktimeout . $lang_functions['text_or_you_will_be_banned'];
									msgalert("faq.php", $text, "orange");
								}
								if ($deletenotransfertwo_account) { //inactive account deletion notice
									if ($CURUSER['downloaded'] == 0 && ($CURUSER['uploaded'] == 0 || $CURUSER['uploaded'] == $iniupload_main)) {
										$neverdelete_account = ($neverdelete_account <= UC_VIP ? $neverdelete_account : UC_VIP);
										if (get_user_class() < $neverdelete_account) {
											$secs = $deletenotransfertwo_account * 24 * 60 * 60;
											$addedtime = strtotime($CURUSER['added']);
											if (TIMENOW > $addedtime + ($secs / 3)) { // start notification if one third of the time has passed
												$kicktimeout = gettime(date("Y-m-d H:i:s", $addedtime + $secs), false, false, true);
												$text = $lang_functions['text_please_download_something_within'] . $kicktimeout . $lang_functions['text_inactive_account_be_deleted'];
												msgalert("rules.php", $text, "gray");
											}
										}
									}
								}
								if ($CURUSER['showclienterror'] == 'yes') {
									$text = $lang_functions['text_banned_client_warning'];
									msgalert("faq.php", $text, "black");
								}

								if ($unread) {
									$unreadidres = sql_query("SELECT id FROM messages WHERE receiver = " . $CURUSER['id'] . " AND unread = 'yes'") or sqlerr(__FILE__, __LINE__);
									$unreadidrow = mysql_fetch_assoc($unreadidres);
									$text = $lang_functions['text_you_have'] . $unread . $lang_functions['text_new_message'] . add_s($unread) . $lang_functions['text_click_here_to_read'];
									msgalert("messages.php?action=viewmessage&id=" . $unreadidrow['id'], $text, "indigo");
								}

								$script_name = $_SERVER["SCRIPT_FILENAME"];
								if (!preg_match("/index/i", $script_name)) {
									$new_news = $Cache->get_value('user_' . $CURUSER["id"] . '_unread_news_count');
									if (empty($new_news)) {
										$new_news = get_row_count("news", "WHERE notify = 'yes' AND added > " . sqlesc($CURUSER['last_home']));
										$Cache->cache_value('user_' . $CURUSER["id"] . '_unread_news_count', $new_news, 300);
									}
									if ($new_news > 0) {
										$text = $lang_functions['text_there_is'] . is_or_are($new_news) . $new_news . $lang_functions['text_new_news'];
										msgalert("index.php", $text, "green");
									}
								}

								if (get_user_class() >= $staffmem_class) {
									$numreports = $Cache->get_value('staff_new_report_count');
									if ($numreports == "") {
										$numreports = get_row_count("reports", "WHERE dealtwith = 0");
										$Cache->cache_value('staff_new_report_count', $numreports, 900);
									}
									if ($numreports) {
										$text = $lang_functions['text_there_is'] . is_or_are($numreports) . $numreports . $lang_functions['text_new_report'] . add_s($numreports);
										msgalert("reports.php", $text, "blue");
									}
									$nummessages = $Cache->get_value('staff_new_message_count');
									if ($nummessages == "") {
										$nummessages = get_row_count("staffmessages", "WHERE answered = 'no'");
										$Cache->cache_value('staff_new_message_count', $nummessages, 900);
									}
									if ($nummessages > 0) {
										$text = $lang_functions['text_there_is'] . is_or_are($nummessages) . $nummessages . $lang_functions['text_new_staff_message'] . add_s($nummessages);
										msgalert("staffbox.php", $text, "blue");
									}
									$numcheaters = $Cache->get_value('staff_new_cheater_count');
									if ($numcheaters == "") {
										$numcheaters = get_row_count("cheaters", "WHERE dealtwith = 0");
										$Cache->cache_value('staff_new_cheater_count', $numcheaters, 900);
									}
									if ($numcheaters) {
										$text = $lang_functions['text_there_is'] . is_or_are($numcheaters) . $numcheaters . $lang_functions['text_new_suspected_cheater'] . add_s($numcheaters);
										msgalert("cheaterbox.php", $text, "blue");
									}
								}
							}
							if ($offlinemsg) {
								print("<p><table width=\"740\" border=\"1\" cellspacing=\"0\" cellpadding=\"10\"><tr><td style='padding: 10px; background: red' class=\"text\" align=\"center\">\n");
								print("<font color=\"white\">" . $lang_functions['text_website_offline_warning'] . "</font>");
								print("</td></tr></table></p><br />\n");
							}
						}
					}
