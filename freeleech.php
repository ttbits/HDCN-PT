<?php

require "include/bittorrent.php";
dbconn();
loggedinorreturn();
require_once(get_langfile_path());
if (get_user_class() < UC_ADMINISTRATOR)
	stderr("错误", "权限不足");
if (!empty($_POST['globaltime'])) {
	sql_query("UPDATE torrents_state SET global_endfree = '" . $_POST['globaltime'] . "'");
	stderr('成功', '全局促销时效到 ' . $_POST['globaltime'] . '');
}
$action = isset($_POST['action']) ? htmlspecialchars($_POST['action']) : (isset($_GET['action']) ? htmlspecialchars($_GET['action']) : 'main');
if ($action == 'setallfree') {
	sql_query("UPDATE torrents_state SET global_sp_state = 2");
	$Cache->delete_value('global_promotion_state');
	stderr('成功', '所有种子均设为Free');
} elseif ($action == 'setall2up') {
	sql_query("UPDATE torrents_state SET global_sp_state = 3");
	$Cache->delete_value('global_promotion_state');
	stderr('成功', '所有种子均设为2x');
} elseif ($action == 'setall2up_free') {
	sql_query("UPDATE torrents_state SET global_sp_state = 4");
	$Cache->delete_value('global_promotion_state');
	stderr('成功', '所有种子均设为2xFree');
} elseif ($action == 'setallhalf_down') {
	sql_query("UPDATE torrents_state SET global_sp_state = 5");
	$Cache->delete_value('global_promotion_state');
	stderr('成功', '所有种子均设为50%');
} elseif ($action == 'setall2up_half_down') {
	sql_query("UPDATE torrents_state SET global_sp_state = 6");
	$Cache->delete_value('global_promotion_state');
	stderr('成功', '所有种子均设为2x50%');
} elseif ($action == 'setall30_down') {
	sql_query("UPDATE torrents_state SET global_sp_state = 7");
	$Cache->delete_value('global_promotion_state');
	stderr('成功', '所有种子均设为30%');
} elseif ($action == 'setallnormal') {
	sql_query("UPDATE torrents_state SET global_sp_state = 1");
	$Cache->delete_value('global_promotion_state');
	stderr('成功', '所有种子均设正常');
} elseif ($action == 'main') {
	if ($ratioless == 'no') {
		stderr(
				$lang_freeleech['head_freeleech'], $lang_freeleech['text_free'] . ' <a class=altlink href=freeleech.php?action=setallfree>' . $lang_freeleech['text_go'] . '</a><hr>' .
				$lang_freeleech['text_2xup'] . ' <a class=altlink href=freeleech.php?action=setall2up>' . $lang_freeleech['text_go'] . '</a><hr>' .
				$lang_freeleech['text_2xup_free'] . ' <a class=altlink href=freeleech.php?action=setall2up_free>' . $lang_freeleech['text_go'] . '</a><hr>' .
				$lang_freeleech['text_halfdown'] . '<a class=altlink href=freeleech.php?action=setallhalf_down>' . $lang_freeleech['text_go'] . '</a><hr>' .
				$lang_freeleech['text_2xup_halfdown'] . '<a class=altlink href=freeleech.php?action=setall2up_half_down>' . $lang_freeleech['text_go'] . '</a><hr>' .
				$lang_freeleech['text_30down'] . '<a class=altlink href=freeleech.php?action=setall30_down>' . $lang_freeleech['text_go'] . '</a><hr>' .
				$lang_freeleech['text_normal'] . '<a class=altlink href=freeleech.php?action=setallnormal>' . $lang_freeleech['text_go'] . '</a><hr>' .
				"<form method=post>全局促销时效：" . "<input type=text id=globaltime name=globaltime style=\"width: 150px\" class=\"Wdate\" onfocus='WdatePicker({dateFmt: \"yyyy-MM-dd HH:mm:ss\", isShowWeek: \"true\", minDate: \"%y-%M-%d %H:%m:%s\"})' value=\"" . date("Y-m-d H:i:s", time() + 86400 * 1) . "\" /><i>（默认 1 天）</i><input type=submit name=submit value='提交（只需点击一次）' /></form>"
				, false
		);
	} else {
		stderr(
				$lang_freeleech['head_freeleech'], $lang_freeleech['text_2xup'] . ' <a class=altlink href=freeleech.php?action=setall2up>' . $lang_freeleech['text_go'] . '</a><hr>' .
				"<form method=post>全局促销时效：" . "<input type=text id=globaltime name=globaltime style=\"width: 150px\" class=\"Wdate\" onfocus='WdatePicker({dateFmt: \"yyyy-MM-dd HH:mm:ss\", isShowWeek: \"true\", minDate: \"%y-%M-%d %H:%m:%s\"})' value=\"" . date("Y-m-d H:i:s", time() + 86400 * 1) . "\" /><i>（默认 1 天）</i><input type=submit name=submit value='提交（只需点击一次）' /></form>"
				, false
		);
	}
}