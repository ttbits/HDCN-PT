<?php
require_once("include/bittorrent.php");
dbconn();
loggedinorreturn();
parked();

function orderid() {
	$u = substr(str_replace('0.', '', microtime()), 0, 4);
	$i = mt_rand(1000, 9999);
	return date("YmdHis") . $u . $i;
}

if ($_GET['pay'] == '1') {
	stderr("恭喜", "充值成功！");
}
if ($_GET['pushkey'] == '') {
	stderr("错误", "无效的充值链接！");
}
$url = json_decode(decrypt($_GET['pushkey']));
$item = $url->item;
$recharge_key = $url->recharge_key;
$account = $url->account;
$a = $url->qid;
$payKey = $url->payKey;
$userId = $url->userId;
$playerId = $url->playerId;

$order = orderid();
$time = time();
$bonus = $CURUSER['seedbonus']; //魔力值
$big = $CURUSER['big']; //荣誉值
$class = $CURUSER['class']; //等级
$reg = $CURUSER['added']; //注册日期
$do = 0;
stdhead("魔力值支付系统");
?>
<div>
	<h1 style="width: 95%"><?= PROJECTNAME ?> 魔力值支付系统</h1>
	<table border="1" cellpadding="10" cellspacing="0" width="95%"><tbody><tr><td>
					<ul>
						<li>本系统可支付<?= PROJECTNAME ?>专属服的游戏代币购买</li>
						<li><font style="color: #5D478B">魔力值</font>兑换游戏代币比例为10：1，即10点魔力值=1点游戏代币</li>
						<li><font style="color: #CD0000">荣誉值</font>兑换游戏代币比例为1：10，即1点荣誉值=10点游戏代币</li>
						<li><?= get_user_class_name_zh(3, FALSE, TRUE) ?>兑换倍率为 1.3，以后等级依次加 0.1，最高为 2；<?= get_user_class_name_zh(11, FALSE, TRUE) ?>及以上倍率一律为 2</li>
						<li>荣誉值充值不享用倍率算法</li>
						<li>《攻城掠地》游戏不享受以上算法，魔力值兑换比例一律为10：1；荣誉值兑换比例一律为1：1</li>
					</ul>
				</td></tr></tbody></table>
</div><br />
<div>
	<form method="POST" onsubmit="if (confirm('你确定要这样做吗？')) {
				return true;
			} else {
				return false;
			}">
		<?php $item == 'fyws' ? print("风云无双充值") : print("攻城掠地充值") ?><br /><br />
		<?php echo "魔力充值数量：" ?><input type="text" name="bonusnum" style="width: 100px" onkeyup="this.value = this.value.replace(/\D/g, '');" />
		<input type="submit" name="bonus" value="魔力充值" /><br /><br />
		<?php echo "荣誉充值数量：" ?><input type="text" name="bignum" style="width: 100px" onkeyup="this.value = this.value.replace(/\D/g, '');" />
		<input type="submit" name="big" value="荣誉充值" /><br /><br />
	</form>
</div>
<?php
if ($_POST['bonus']) {
	if ($item == 'fyws') {
		if ($_POST['bonusnum'] == '' || $_POST['bonusnum'] < 10) {
			echo "<script>alert('错误！请输入大于 10 的正整数！');location.href='payto.php?pushkey=pushkey=" . $_GET['pushkey'] . "';</script>";
		} else {
			switch ($class) {
				case UC_POWER_USER:
					$rate = "1.3";
					break;
				case UC_ELITE_USER:
					$rate = "1.4";
					break;
				case UC_CRAZY_USER:
					$rate = "1.5";
					break;
				case UC_INSANE_USER:
					$rate = "1.6";
					break;
				case UC_VETERAN_USER:
					$rate = "1.7";
					break;
				case UC_EXTREME_USER:
					$rate = "1.8";
					break;
				case UC_ULTIMATE_USER:
					$rate = "1.9";
					break;
				case UC_NEXUS_MASTER:
					$rate = "2";
					break;
				default :
					$rate = "1";
			}
			if ($class >= UC_VIP) {
				$rate = "2";
			}
		}
	} elseif ($item == 'gcld') {
		$check = 0;
		if ($_POST['bonusnum'] == '' || $_POST['bonusnum'] < 10) {
			echo "<script>alert('错误！请输入大于 10 的正整数，且为 10 的倍数！');location.href='payto.php?pushkey=" . $_GET['pushkey'] . "';</script>";
			$check = 1;
		}
		if ($_POST['bonusnum'] % 10 != 0) {
			echo "<script>alert('错误！请输入大于 10 的正整数，且为 10 的倍数！');location.href='payto.php?pushkey=" . $_GET['pushkey'] . "';</script>";
			$check = 1;
		}
	}
	$PayMoney = $_POST['bonusnum'];
	$PayGold = $PayMoney / 10 * $rate;
	if ($PayMoney > $bonus) {
		echo "<script>alert('错误！魔力值不足，请重新输入！');location.href='payto.php?pushkey=" . $_GET['pushkey'] . "';</script>";
	} else {
		$do = 1;
	}
	if ($do == 1 && $check == 0) {
		if ($item == 'fyws') {
			$itemname = "风云无双";
			$serverurl = "fyws.arcadiahd.org";
			$serverport = "30011";
			$time = time();
			$sid = '1';
			$order_id = orderid();
			$dj = $PayGold;
			$data = array(
				'qid' => $a,
				'server_id' => $sid,
				'order_id' => $order_id,
				'order_amount' => $dj,
				'sign' => md5("{$a}{$dj}{$order_id}{$sid}{$recharge_key}")
			);
			$url = 'http://' . $serverurl . ':' . $serverport . '/gm99/recharge?' . http_build_query($data);
			$ch = curl_init();
			$timeout = 30;
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			$file_contents = curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			if ($httpCode == 200) {
				$ok = 1;
			}
		} elseif ($item == 'gcld') {
			$itemname = "攻城掠地";
			$orderId = orderid();
			$tp = time();
			$dj = $PayMoney / 10;
			$data = array(
				'yx' => $item,
				'userId' => $userId,
				'playerId' => $playerId,
				'orderId' => $orderId,
				'gold' => $dj,
				'tp' => $tp,
				'ticket' => md5($item . $userId . $orderId . $dj . $tp . $payKey)
			);
			$serverurl = "gcld.hdhello.com";
			$serverport = "9001";
			$url = 'http://' . $serverurl . ':' . $serverport . '/root/gateway.action?command=pay&' . http_build_query($data);
			$ch = curl_init();
			$timeout = 30;
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			$file_contents = curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			if ($httpCode == 200) {
				$ok = 1;
			}
		}
	}
	if ($ok == 1) {
		sql_query("UPDATE users SET seedbonus = seedbonus - $PayMoney WHERE id = " . $CURUSER['id'] . "");
		writeBonusComment($CURUSER['id'], "用于充值《{$itemname}》游戏代币消耗了 $PayMoney 个魔力值");
		echo "<script>alert('恭喜，成功充值 $dj 个《{$itemname}》游戏代币！');location.href='payto.php?pay=1';</script>";
	} else {
		echo "<script>alert('抱歉，充值失败！');location.href='payto.php';location.href='payto.php?pushkey=" . $_GET['pushkey'] . "';</script>";
	}
} elseif ($_POST['big']) {
	if ($_POST['bignum'] == '' || $_POST['bignum'] < 1) {
		echo "<script>alert('错误！请输入大于 1 的正整数！');location.href='payto.php?pushkey=" . $_GET['pushkey'] . "';</script>";
	} else {
		$PayMoney = $_POST['bignum'];
		$PayGold = $PayMoney * 10;
		if ($PayMoney > $big) {
			echo "<script>alert('错误！荣誉值不足，请重新输入！');location.href='payto.php?pushkey=" . $_GET['pushkey'] . "';</script>";
		} else {
			$do = 1;
		}
	}
	if ($do == 1) {
		if ($item == 'fyws') {
			$itemname = "风云无双";
			$serverurl = "fyws.arcadiahd.org";
			$serverport = "30011";
			$time = time();
			$sid = '1';
			$order_id = orderid();
			$dj = $PayGold;
			$data = array(
				'qid' => $a,
				'server_id' => $sid,
				'order_id' => $order_id,
				'order_amount' => $dj,
				'sign' => md5("{$a}{$dj}{$order_id}{$sid}{$recharge_key}")
			);
			$url = 'http://' . $serverurl . ':' . $serverport . '/gm99/recharge?' . http_build_query($data);
			$ch = curl_init();
			$timeout = 30;
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			$file_contents = curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			if ($httpCode == 200) {
				$ok = 1;
			}
		} elseif ($item == 'gcld') {
			$itemname = "攻城掠地";
			$orderId = orderid();
			$tp = time();
			$dj = $PayMoney;
			$data = array(
				'yx' => $item,
				'userId' => $userId,
				'playerId' => $playerId,
				'orderId' => $orderId,
				'gold' => $dj,
				'tp' => $tp,
				'ticket' => md5($item . $userId . $orderId . $dj . $tp . $payKey)
			);
			$serverurl = "gcld.hdhello.com";
			$serverport = "9001";
			$url = 'http://' . $serverurl . ':' . $serverport . '/root/gateway.action?command=pay&' . http_build_query($data);
			$ch = curl_init();
			$timeout = 30;
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			$file_contents = curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			if ($httpCode == 200) {
				$ok = 1;
			}
		}
	}
	if ($ok == 1) {
		sql_query("UPDATE users SET big = big - $PayMoney WHERE id = " . $CURUSER['id'] . "");
		writeBonusComment($CURUSER['id'], "用于充值《{$itemname}》游戏代币消耗了 $PayMoney 个荣誉值");
		echo "<script>alert('恭喜，成功充值 $dj 个《{$itemname}》游戏代币！');location.href='payto.php?pay=1';</script>";
	} else {
		echo "<script>alert('抱歉，充值失败！');location.href='payto.php?pushkey=" . $_GET['pushkey'] . "';</script>";
	}
}
stdfoot();
