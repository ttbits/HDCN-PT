<?php

//$_NO_COMPRESS = true; //== For pdq's improvements mods
//ob_start("ob_gzhandler");
require_once "include/bittorrent.php";
dbconn(false);
loggedinorreturn();
//$lang = array_merge( load_language('global') );
if ($CURUSER['class'] < UC_USER) {
	stderr("抱歉...", "您的等级太低");
	exit;
}

function begin_table_bjstats($fullwidth = false, $padding = 5) {
	$width = "";
	if ($fullwidth)
		$width .= " width=50%";
	return("<table class='main" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'>");
}

function end_table_bjstats() {
	return("</table>");
}

function begin_frame_bjstats($caption = "", $center = false, $padding = 10, $width = "100%", $caption_center = "left") {
	$tdextra = "";
	if ($center)
		$tdextra .= " align='center'";
	return(($caption ? "<h2 align='" . $caption_center . "'>" . $caption . "</h2>" : "") . "<table width='" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'>" . "<tr><td class='text' $tdextra>");
}

function end_frame_bjstats() {
	return("</td></tr></table>");
}

function bjtable($res, $frame_caption) {
	$mb = 200;
	$htmlout = '';
	$htmlout .= begin_frame_bjstats($frame_caption, true);
	$htmlout .= begin_table_bjstats();
	$htmlout .="<tr><td class='colhead'>排名</td><td class=' colhead ' align='left'>用户名</td><td class='colhead' align='right'>赢场</td><td class='colhead' align='right'>输场</td><td class='colhead' align='right'>总场数</td><td class='colhead' align='right'>胜率</td><td class='colhead' align='right'>赢/输</td></tr>";
	$num = 0;
	while ($a = mysql_fetch_assoc($res)) {
		++$num;
		//==Calculate Win %
		$win_perc = number_format(($a['wins'] / $a['games']) * 100, 1);
		//==Add a user' s +/- statistic
		$plus_minus = $a['wins'] - $a['losses'];
		if ($plus_minus >= 0) {
			$plus_minus = ($a['wins'] - $a['losses']) * 0.95 * $mb;
		} else {
			$plus_minus = "-";
			$plus_minus .= ($a['losses'] - $a['wins']) * $mb;
		}
		$htmlout .="<tr><td>$num</td><td align='left'>" .
				"<b><a href='userdetails.php?id=" . $a['id'] . "'>" . get_username($a["id"]) . "</a></b></td>" .
				"<td align='right'>" . number_format($a['wins'], 0) . "</td>" .
				"<td align='right'>" . number_format($a['losses'], 0) . "</td>" .
				"<td align='right'>" . number_format($a['games'], 0) . "</td>" .
				"<td align='right'>$win_perc%</td>" .
				"<td align='right'>$plus_minus</td>" .
				"</tr>";
	}
	$htmlout .= end_table_bjstats();
	$htmlout .= end_frame_bjstats();
	return $htmlout;
}

$cachetime = 60 * 30; // 30 minutes
//$cachetime = 10 * 3;
$Cache->new_page('bjstats', $cachetime, true);
if (!$Cache->get_page()) {
	$Cache->add_whole_row();
	$mingames = 5;
	$HTMLOUT = '';
	$HTMLOUT .="<h1 align='center'>21点 统计</h1>";
	$HTMLOUT .="<p align='center'>统计数据有缓存，统计数据每30分钟更新一次。你至少需要有 $mingames 场比赛，才会统计结果。</p>";
	$HTMLOUT .="<br />";
	//==Most Games Played
	$res = sql_query("SELECT id, username, bjwins AS wins, bjlosses AS losses, bjwins + bjlosses AS games FROM users WHERE bjwins + bjlosses > $mingames ORDER BY games DESC LIMIT 10") or sqlerr(__FILE__, __LINE__);
	$HTMLOUT .= bjtable($res, "最多上场榜", "Users");
	$HTMLOUT .="<br /><br />";
	//==Most Games Played
	//==Highest Win %
	$res = sql_query("SELECT id, username, bjwins AS wins, bjlosses AS losses, bjwins + bjlosses AS games, bjwins / (bjwins + bjlosses) AS winperc FROM users WHERE bjwins + bjlosses > $mingames ORDER BY winperc DESC LIMIT 10") or sqlerr(__FILE__, __LINE__);
	$HTMLOUT .= bjtable($res, "最佳胜率榜", "Users");
	$HTMLOUT .="<br /><br />";
	//==Highest Win %
	//==Most Credit Won
	$res = sql_query("SELECT id, username, bjwins AS wins, bjlosses AS losses, bjwins + bjlosses AS games, bjwins - bjlosses AS winnings FROM users WHERE bjwins + bjlosses > $mingames ORDER BY winnings DESC LIMIT 10") or sqlerr(__FILE__, __LINE__);
	$HTMLOUT .= bjtable($res, "收入最高榜", "Users");
	$HTMLOUT .="<br /><br />";
	//==Most Credit Won
	//==Most Credit Lost
	$res = sql_query("SELECT id, username, bjwins AS wins, bjlosses AS losses, bjwins + bjlosses AS games, bjlosses - bjwins AS losings FROM users WHERE bjwins + bjlosses > $mingames ORDER BY losings DESC LIMIT 10") or sqlerr(__FILE__, __LINE__);
	$HTMLOUT .= bjtable($res, "损失最多榜", "Users");
	//==Most Credit Lost
	$HTMLOUT .="<br /><br />";
	print $HTMLOUT;
	$Cache->end_whole_row();
	$Cache->cache_page();
}
stdhead('21点 统计');
echo $Cache->next_row();
stdfoot();
