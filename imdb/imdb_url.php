<?php

$type = $_GET["type"];
$title = $_GET["title"];
$res = $_GET["res"];

if ($res == "imdb") {
	switch ($type) {
		case '401':
		case '402':
		case '404':
		case '405':
		case '414':
			echo searchImdb($title);
			break;
		default:
			break;
	}
}/* else if ($res == "douban") {
  switch ($type) {
  case '401':
  case '402':
  case '404':
  case '405':
  case '414':
  echo searchDoubanMovie($title);
  break;
  default:
  break;
  }
  }
 *
 */

function searchDoubanMovie($q, $count = 3) {
	$basicUrl = "http://api.douban.com/v2/movie/search";
	$requestUrl = $basicUrl . "?q=" . $q . "&count=" . $count;
	$data = file_get_contents($requestUrl);
	$data = json_decode($data);
	if ($data->total == 0) {
		return FALSE;
	}
	$html = '<div id="select_douban"><p>请在所列出的信息里选择一个正确的信息，没有正确的可以不选取</p><table>';
	$html .= '<tr><th>序号</th><th>电影名</th><th>原始名称</th><th>上映时间</th><th>选择</th></tr>';
	$count = 0;
	foreach ($data->subjects as $item) {
		$count ++;
		$html .= "<tr class=\"douban_item\"><td><a href=\"{$item->alt}\">$count</a></td><td>{$item->title}</td><td>{$item->original_title}</td><td>{$item->year}</td><td><input type=\"button\" value=\"是这个\"></td></tr>";
	}
	$html .= '</table></div>';
	return $html;
}

function searchIMDB($title, $limit = 3) {
	$requestUrl = "http://www.imdb.com/xml/find?json=1&q=$title";
	$data = file_get_contents($requestUrl);
	$data = json_decode($data);
	if ($data->title_popular == '') {
		return FALSE;
	}
	$html = '<div id="select_imdb"><p>请在所列出的信息里选择一个正确的信息，没有正确的可以不选取</p><table>';
	$html .= '<tr><th>序号</th><th>电影名</th><th>上映时间</th><th>选择</th></tr>';
	$limit = 0;
	foreach ($data->title_popular as $item) {
		$limit ++;
		$year = explode(",", $item->description);
		$html .= "<tr class=\"imdb_item\"><td><a href=\"http://www.imdb.com/title/{$item->id}/\">$limit</a></td><td>{$item->title}</td><td>$year[0]</td><td><input type=\"button\" value=\"是这个\"></td></tr>";
	}
	$html .= '</table></div>';
	return $html;
}
