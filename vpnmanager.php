<?php
require_once("include/bittorrent.php");
require('config/allconfig.php');
dbconn();
loggedinorreturn();
parked();
secrity();
if (get_user_class() != UC_STAFFLEADER) {
	stderr("错误", "权限不足");
} elseif ($buyvpn != 'yes') {
	stderr("错误", "当前VPN购买服务未启用");
}
stdhead("VPN后台管理");
/*
 * 可以设置每月默认流量
 * 可以设置1-3个流量加油包
 * 可以查看所有VPN用户及信息
 * 可以删除指定用户的VPN帐号
 */
$defaulttraffic = get_single_value("radgroupreply", "value", "WHERE groupname = 'user' AND attribute = 'Max-Monthly-Traffic'");
$planA = get_single_value("radgroupreply", "value", "WHERE groupname = 'planA-D' AND attribute = 'Plan'");
$trafficA = get_single_value("radgroupreply", "value", "WHERE groupname = 'planA' AND attribute = 'Max-Monthly-Traffic'");
$bonusA = get_single_value("radgroupreply", "value", "WHERE groupname = 'planA-D' AND attribute = 'Bonus'");
$planB = get_single_value("radgroupreply", "value", "WHERE groupname = 'planB-D' AND attribute = 'Plan'");
$trafficB = get_single_value("radgroupreply", "value", "WHERE groupname = 'planB' AND attribute = 'Max-Monthly-Traffic'");
$bonusB = get_single_value("radgroupreply", "value", "WHERE groupname = 'planB-D' AND attribute = 'Bonus'");
$planC = get_single_value("radgroupreply", "value", "WHERE groupname = 'planC-D' AND attribute = 'Plan'");
$trafficC = get_single_value("radgroupreply", "value", "WHERE groupname = 'planC' AND attribute = 'Max-Monthly-Traffic'");
$bonusC = get_single_value("radgroupreply", "value", "WHERE groupname = 'planC-D' AND attribute = 'Bonus'");
if (get_user_class() == UC_STAFFLEADER) {
	if ($_POST['default']) {
		sql_query("UPDATE radgroupreply SET value = " . $_POST['defaulttraffic'] . " WHERE groupname = 'user' AND attribute = 'Max-Monthly-Traffic'");
		echo "<script>alert('更改每月默认流量成功');location.href='vpnmanager.php';</script>";
	} elseif ($_POST['plan']) {
		if (isset($_POST['planA'])) {
			sql_query("UPDATE radgroupreply SET value = " . $_POST['trafficA'] . " + $defaulttraffic WHERE groupname = 'planA' AND attribute = 'Max-Monthly-Traffic'");
			sql_query("UPDATE radgroupreply SET value = " . $_POST['bonusA'] . " WHERE groupname = 'planA-D' AND attribute = 'Bonus'");
			sql_query("UPDATE radgroupreply SET value = '" . $_POST['planA'] . "' WHERE groupname = 'planA-D' AND attribute = 'Plan'");
		}
		if (isset($_POST['planB'])) {
			sql_query("UPDATE radgroupreply SET value = " . $_POST['trafficB'] . " + $defaulttraffic WHERE groupname = 'planB' AND attribute = 'Max-Monthly-Traffic'");
			sql_query("UPDATE radgroupreply SET value = " . $_POST['bonusB'] . " WHERE groupname = 'planB-D' AND attribute = 'Bonus'");
			sql_query("UPDATE radgroupreply SET value = '" . $_POST['planB'] . "' WHERE groupname = 'planB-D' AND attribute = 'Plan'");
		}
		if (isset($_POST['planC'])) {
			sql_query("UPDATE radgroupreply SET value = " . $_POST['trafficC'] . " + $defaulttraffic WHERE groupname = 'planC' AND attribute = 'Max-Monthly-Traffic'");
			sql_query("UPDATE radgroupreply SET value = " . $_POST['bonusC'] . " WHERE groupname = 'planC-D' AND attribute = 'Bonus'");
			sql_query("UPDATE radgroupreply SET value = '" . $_POST['planC'] . "' WHERE groupname = 'planC-D' AND attribute = 'Plan'");
		}
		echo "<script>alert('更改套餐成功');location.href='vpnmanager.php';</script>";
	}
} else {
	stdmsg("错误", "权限不足");
	stdfoot();
	exit;
}
?>
<script type="text/javascript">
	function checkAll()
	{
		$(".checkbox").each(function () {
			this.checked = true;
		});
	}

	function reverseCheck()
	{
		$(".checkbox").each(function () {
			this.checked = !this.checked;
		});
	}
</script>
<div>
	<h1><a href="vpn.php">返回VPN</a></h1>
	<form  method="post">
		<table border="1" cellpadding="10" cellspacing="0" width="300px">
			<tr><td align="right">每月默认流量：</td><td align="left"><input name="defaulttraffic" type="text" style="width: 150px" value="<?= ($defaulttraffic ? $defaulttraffic : "209715200") ?>" /><br />单位字节，100M=104857600</td></tr>
		</table><br />
		<input name="default" type="submit" value="更改" />
	</form><hr>
	<h1>流量加油包设置</h1>
	当套餐计划名称为'0'或'空'则该套餐无效，和每月默认流量叠加
	<form  method="post">
		<table border="1" cellpadding="10" cellspacing="0" width="600px">
			<tr><td align="center">套餐</td><td align="center">套餐计划名称</td><td align="center">套餐流量额度</td><td align="center">套餐流量价格</td></tr>
			<tr><td align="center">套餐A</td><td align="center"><input name="planA" type="text" style="width: 98%" value="<?= $planA ?>" /></td><td align="center"><input name="trafficA" type="text" style="width: 98%" onkeyup="this.value = this.value.replace(/\D/g, '');" value="<?= $trafficA - $defaulttraffic ?>" /></td><td align="center"><input name="bonusA" type="text" style="width: 98%" onkeyup="this.value = this.value.replace(/\D/g, '');" value="<?= $bonusA ?>" /></td></tr>
			<tr><td align="center">套餐B</td><td align="center"><input name="planB" type="text" style="width: 98%" value="<?= $planB ?>" /></td><td align="center"><input name="trafficB" type="text" style="width: 98%" onkeyup="this.value = this.value.replace(/\D/g, '');" value="<?= $trafficB - $defaulttraffic ?>" /></td><td align="center"><input name="bonusB" type="text" style="width: 98%" onkeyup="this.value = this.value.replace(/\D/g, '');" value="<?= $bonusB ?>" /></td></tr>
			<tr><td align="center">套餐C</td><td align="center"><input name="planC" type="text" style="width: 98%" value="<?= $planC ?>" /></td><td align="center"><input name="trafficC" type="text" style="width: 98%" onkeyup="this.value = this.value.replace(/\D/g, '');" value="<?= $trafficC - $defaulttraffic ?>" /></td><td align="center"><input name="bonusC" type="text" style="width: 98%" onkeyup="this.value = this.value.replace(/\D/g, '');" value="<?= $bonusC ?>" /></td></tr>
		</table><br />
		<input name="plan" type="submit" value="提交" />
	</form><hr>
</div>
<?php

function begin_table_vpn($fullwidth = false, $padding = 5) {
	$width = "";
	if ($fullwidth)
		$width .= " width=50%";
	return("<table class='main" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'><tr align=\"center\"><form method=\"POST\" onsubmit=\"if(confirm('确定要删除吗？')){return true;}else{return false;}\"></tr>");
}

function end_table_vpn() {
	return("<tr align=\"center\"><td class=\"rowfollow\" colspan=\"10\"><input type=\"button\" value=\"全选\" onclick=\"checkAll();\"><input type=\"button\" value=\"反选\" onclick=\"reverseCheck();\"> | <input type=\"submit\" id=\"delete\" value=\"删除\"></form></td></tr></table>");
}

function begin_frame_vpn($caption = "", $center = false, $padding = 5, $width = "100%", $caption_center = "left") {
	$tdextra = "";
	if ($center)
		$tdextra .= " align='center'";
	return(($caption ? "<h2 align='" . $caption_center . "'>" . $caption . "</h2>" : "") . "<table width='" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'>" . "<tr><td class='text' $tdextra>");
}

function end_frame_vpn() {
	return("</td></tr></table>");
}

function bjtable_vpn($res, $frame_caption) {
	$htmlout = '';
	$htmlout .= begin_frame_vpn($frame_caption, true);
	$htmlout .= begin_table_vpn();
	$htmlout .="<tr><td class='colhead'>ID</td><td class='colhead' align='left'>用户名</td><td class='colhead' align='left'>VPN帐号</td><td class='colhead' align='right'>VPN套餐</td><td class='colhead' align='right'>VPN注册时间</td><td class='colhead' align='right'>剩余时间</td><td class='colhead' align='right'>剩余流量</td><td class='colhead' align='right'>行为</td></tr>";
	while ($a = mysql_fetch_assoc($res)) {
		$ks = date("Y-m-d H:i:s", strtotime($a['creationdate'] . " + 30 days")); //使用时间
		$time = mkprettytime(round((strtotime($ks) - strtotime(date("Y-m-d H:i:s"))))); //剩余使用时间
		$usetraffic = mysql_fetch_array(sql_query("SELECT SUM(acctoutputoctets) AS aout FROM radacct WHERE username = '" . $a['username'] . "'")); //已经使用流量
		$groupname = mysql_fetch_array(sql_query("SELECT groupname FROM radusergroup WHERE username = '" . $a['username'] . "'")); //取出VPN用户所在的用户组
		$totaltraffic = mysql_fetch_array(sql_query("SELECT value FROM radgroupreply WHERE groupname = '" . $groupname['groupname'] . "' AND attribute = 'Max-Monthly-Traffic'")); //全部可用流量
		$user = mysql_fetch_array(sql_query("SELECT id, class FROM users WHERE username = '" . $a['firstname'] . "' AND email = '" . $a['email'] . "'"));
		$group = mysql_fetch_array(sql_query("SELECT value FROM radgroupreply WHERE groupname = '" . $groupname['groupname'] . "-D' AND attribute = 'Plan'"));
		if ($groupname['groupname'] == 'user') {
			$plan = "无";
		} elseif ($groupname['groupname'] == 'planA') {
			$plan = $group['value'];
		} elseif ($groupname['groupname'] == 'planB') {
			$plan = $group['value'];
		} elseif ($groupname['groupname'] == 'planC') {
			$plan = $group['value'];
		}
		$htmlout .="<tr class='torrent_table'><td>$a[id]</td><td align='left'>" . //ID
				"<b><a class=\"" . get_user_class_name($user['class']) . "_Name\" href=\"userdetails.php?id=" . $user['id'] . "\">" . get_username($user['id']) . "</a></b></td>" . //用户名
				"<td align='center'><b>" . $a['username'] . "</b></td>" . //VPN帐号
				"<td align='center'><b>$plan</b></td>" . //VPN套餐
				"<td align='center'><b>" . $a['creationdate'] . "</b></td>" . //VPN帐号注册时间
				"<td align='center'><b>${time}</b></td>" . //VPN帐号剩余时间
				"<td align='center'><b>" . mksize($totaltraffic['value'] - (0 + $usetraffic['aout'])) . "</b></td>" . //VPN帐号剩余流量
				"<td class=\"rowfollow\"><input type=\"checkbox\" value=\"{$a['id']}\" name=\"id[]\" class=\"checkbox\"></td>" .
				"</tr>";
	}
	$htmlout .= end_table_vpn();
	$htmlout .= end_frame_vpn();
	return $htmlout;
}

$dt = sqlesc(date("Y-m-d H:i:s", (TIMENOW - 86400 * 7)));
$HTMLOUT .="<h1 align='center'><a href='vpnmanager.php'>查看VPN用户</a></h1>";
$count = get_row_count("userinfo");
list($pagertop, $pagerbottom, $limit) = pager(25, $count, "?");
$res = sql_query("SELECT * FROM userinfo ORDER BY id DESC $limit") or sqlerr(__FILE__, __LINE__); //降序排列
$HTMLOUT .= bjtable_vpn($res, "VPN用户信息", "Users");
$HTMLOUT .="<br /><br />";
if ($count) {
	print($pagertop);
	print $HTMLOUT;
	print($pagerbottom);
} else {
	print $HTMLOUT;
}
if ($_POST['id'] != '') {
	$id = implode(",", $_POST['id']);
	$res = sql_query("SELECT username FROM radcheck WHERE id IN ($id)");
	while ($row = mysql_fetch_array($res)) {
		$username = $row['username'];
		sql_query("DELETE FROM radacct WHERE username = '$username'");
	}
	sql_query("DELETE FROM userinfo WHERE id IN ($id)");
	sql_query("DELETE FROM radcheck WHERE id IN ($id)");
	sql_query("DELETE FROM radusergroup WHERE id IN ($id)");
	echo "<script>location.href='vpnmanager.php';</script>";
}