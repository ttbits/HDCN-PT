<?php
require_once("include/bittorrent.php");
dbconn();
if ($ipv6signup != 'yes') {
	stderr("抱歉", "当前已关闭高校IPv6免考通道的注册方式");
}
$ip = getip();
if (strpos($ip, '2001:') === FALSE) {//如果没有匹配到"2001:"，则拒绝
	stderr("抱歉", "只有属于高校的IPv6用户才可以在此页面注册");
}

$langid = 0 + $_GET['sitelanguage'];
if ($langid) {
	$lang_folder = validlang($langid);
	if (get_langfolder_cookie() != $lang_folder) {
		set_langfolder_cookie($lang_folder);
		header("Location: " . $_SERVER['REQUEST_URI']);
	}
}
require_once(get_langfile_path("", FALSE, $CURLANGDIR));
cur_user_check();
$type = $_GET['type'];
if ($type == 'invite') {
	registration_check("invitesystem", TRUE, TRUE, $_GET['invitenumber']);
	failedloginscheck("邀请注册");
	$code = $_GET["invitenumber"];
	$checkass = $_GET["nonass"];
	if ($code == "") {
		stdhead($lang_signup['head_invite_signup']);
		?>
		<br /><br />
		<form method="get" action="<?= $_SERVER['PHP_SELF'] ?>">
			<input type="hidden" name="type" value="invite">
			<table border="1" cellspacing="0" cellpadding="10">
				<tr><td><?= $lang_signup['row_invite'] ?></td>
					<td><input type="text" name="invitenumber"></td></tr>
				<tr><td colspan=2 align=center><input type=submit value=<?= $lang_signup['submit_invite'] ?>></td></tr>
			</table>
		</form>
		<?php
		stdfoot();
		exit(0);
	}
	$sq = sprintf("SELECT inviter FROM invites WHERE hash = '%s'", mysql_real_escape_string($code));
	$res = sql_query($sq) or sqlerr(__FILE__, __LINE__);
	$inv = mysql_fetch_assoc($res);
	$inviter = htmlspecialchars($inv["inviter"]);
	if (!$inv) {
		stderr("错误", "不存在的邀请码", 0);
	}
	stdhead($lang_signup['head_invite_signup']);
	print("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\">");
} else {
	//registration_check("normal");
	failedloginscheck("自由注册");
	stdhead($lang_signup['head_signup']);
}
$s = "<select name=\"sitelanguage\" onchange='submit()'>\n";
$langs = langlist("site_lang");
foreach ($langs as $row) {
	if ($row["site_lang_folder"] == get_langfolder_cookie())
		$se = " selected=\"selected\"";
	else
		$se = "";
	$s .= "<option value=\"" . $row["id"] . "\"" . $se . ">" . htmlspecialchars($row["lang_name"]) . "</option>\n";
}
$s .= "\n</select>";
print("<form method=\"get\" action=\"" . $_SERVER['PHP_SELF'] . "\"><div align=\"right\">" . $lang_signup['text_select_lang'] . $s . "</div></form>");
if ($type == 'invite') {
	print("<form method=\"get\" action=\"" . $_SERVER['PHP_SELF'] . "\">");
	print("<input type=hidden name=type value='invite'><input type=hidden name=invitenumber value='" . $code . "'><input type=hidden name=nonass value='" . $checkass . "'>");
	print("</form>");
}
?>
<br /><br />
<form method="post" action="takesignup_ipv6.php">
	<?php if ($type == 'invite') print("<input type=\"hidden\" name=\"inviter\" value=\"" . $inviter . "\" /><input type=hidden name=type value='invite' />"); ?>
	<table border="1" cellspacing="0" cellpadding="10">
		<?php print("<tr><td class=text align=center colspan=2>" . $lang_signup['text_cookies_note'] . "</td></tr>"); ?>
		<tr><td class=rowhead width=80><?php echo $lang_signup['row_desired_username'] ?></td><td class=rowfollow align=left><input type="text" style="width: 200px" name="wantusername" /><br /><font class=small><?php echo $lang_signup['text_allowed_characters'] ?></font></td></tr>
		<tr><td class=rowhead><?php echo $lang_signup['row_pick_a_password'] ?></td><td class=rowfollow align=left><input type="password" style="width: 200px" name="wantpassword" /><br /><font class=small><?php echo $lang_signup['text_minimum_six_characters'] ?></font></td></tr>
		<tr><td class=rowhead><?php echo $lang_signup['row_enter_password_again'] ?></td><td class=rowfollow align=left><input type="password" style="width: 200px" name="passagain" /></td></tr>
		<?php show_image_code(); ?>
		<tr><td class=rowhead><?php echo $lang_signup['row_email_address'] ?></td><td class=rowfollow align=left><input type="text" style="width: 200px" name="email" /><table width=250 border=0 cellspacing=0 cellpadding=0><tr><td class=embedded><font class=small><?php echo ($restrictemaildomain == 'yes' ? $lang_signup['text_email_note'] . allowedemails() : "") ?></font></td></tr></table></td></tr>
		<?php
		$countries = "<option value=\"1\">---- " . $lang_signup['select_none_selected'] . " ----</option>n";
		$ct_r = sql_query("SELECT id, name FROM countries ORDER BY name") or die;
		while ($ct_a = mysql_fetch_array($ct_r))
			$countries .= "<option value=$ct_a[id]" . ($ct_a['id'] == 1 ? " selected" : "") . ">$ct_a[name]</option>n";
		tr($lang_signup['row_country'], "<select name=country>{$countries}</select>", 1);
		//school select
		if ($showschool == 'yes') {
			$schools = "<option value=35>---- " . $lang_signup['select_none_selected'] . " ----</option>n";
			$sc_r = sql_query("SELECT id,name FROM schools ORDER BY name") or die;
			while ($sc_a = mysql_fetch_array($sc_r))
				$schools .= "<option value=$sc_a[id]" . ($sc_a['id'] == 35 ? " selected" : "") . ">$sc_a[name]</option>n";
			tr($lang_signup['row_school'], "<select name=school>$schools</select>", 1);
		}
		?>
		<tr><td class=rowhead><?php echo $lang_signup['row_gender'] ?></td><td class=rowfollow align=left><input type=radio name=gender value=Male><?php echo $lang_signup['radio_male'] ?><input type=radio name=gender value=Female><?php echo $lang_signup['radio_female'] ?></td></tr>
		<tr><td class=rowhead><?php echo $lang_signup['row_verification'] ?></td><td class=rowfollow align=left><input type=checkbox name=rulesverify value=yes><?php echo $lang_signup['checkbox_read_rules'] ?><br /><input type=checkbox name=faqverify value=yes><?php echo $lang_signup['checkbox_read_faq'] ?> <br /><input type=checkbox name=ageverify value=yes><?php echo $lang_signup['checkbox_age'] ?></td></tr>
		<input type=hidden name=hash value=<?php echo $code ?>><input type=hidden name=checkass value=<?php echo $checkass ?>>
		<tr><td class=toolbox colspan="2" align="center"><font color=red><b><?php echo $lang_signup['text_all_fields_required'] ?></b></font><br /><input type=submit style="height: 25px" value=<?php echo $lang_signup['submit_sign_up'] ?>></td></tr>
	</table>
</form>
<?php
stdfoot();
