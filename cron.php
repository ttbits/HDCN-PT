<?php

require_once("include/bittorrent.php");
dbconn();
if ($useCronTriggerCleanUp === TRUE) {
	//默认最短更新间隔为900秒，即15分钟
	$return = autoclean();
	if ($return) {
		echo $return . "\n";
	} else {
		echo "更新未能触发\n";
	}
} else {
	echo "错误，更新被设定为使用浏览器才能触发\n";
}