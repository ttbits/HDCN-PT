<?php

ob_start();
header("Content-type:text/html;charset=utf-8");
require_once("include/bittorrent.php");
dbconn();

if (get_user_class() < UC_ADMINISTRATOR) {
	stderr("错误", "权限不足");
}
stdhead("清除memcached缓存");
$memcache_obj = memcache_connect('127.0.0.1', 11211) or die("Could not connect");
if (memcache_flush($memcache_obj)) {
	echo "已清除所有memcache缓存";
}
stdfoot();
