<?php

require_once("include/bittorrent.php");
dbconn();
loggedinorreturn();
if ($hr == 'no') {
	stderr("错误", "当前H&R系统未开启");
}
stdhead("H&R查看统计");
?>
<script>
	$(function () {//checkbox只能选取一个
		var allBox = $(":checkbox");
		allBox.click(function () {
			allBox.removeAttr("checked");
			$(this).attr("checked", "checked");
		});
	});
</script>
<?php

function get_torrents_name($torrentid) {
	if ($torrentid) {
		$res = sql_query("SELECT name, small_descr FROM torrents WHERE id = $torrentid");
		while ($row = mysql_fetch_array($res)) {
			$torrentname = "<a href=\"details.php?id=$torrentid&hit=1\" target=\"_blank\"><b>" . $row['name'] . "</b></a><br />" . $row['small_descr'] . "";
		}
	}
	return $torrentname;
}

function begin_table_hr($fullwidth = false, $padding = 5) {
	$width = "";
	if ($fullwidth)
		$width .= " width=50%";
	return("<table class='main" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'>");
}

function end_table_hr() {
	return("</table>");
}

function begin_frame_hr($caption = "", $center = false, $padding = 10, $width = "100%", $caption_center = "left") {
	$tdextra = "";
	if ($center)
		$tdextra .= " align='center'";
	return(($caption ? "<h2 align='" . $caption_center . "'>" . $caption . "</h2>" : "") . "<table width='" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'>" . "<tr><td class='text' $tdextra>");
}

function end_frame_hr() {
	return("</td></tr></table>");
}

function bjtable_hr($res, $frame_caption) {
	global $hrradio, $hrhour, $hrday, $hrstartradio, $stronghr, $stronghruntil, $stronghr_radio, $stronghr_time;
	$htmlout = '';
	$htmlout .= begin_frame_hr($frame_caption, true);
	$htmlout .= begin_table_hr();
	if ($stronghr == 'no') {
		print("<table border=\"1\" cellpadding=\"10\" cellspacing=\"0\" width=\"40%\" colspan=\"10\"><tbody><tr><td><ul><li>发布时间 > 30天</li><li>下载量 ＜ 种子体积的 {$hrstartradio}%</li><li>单种分享率 ≥ $hrradio</li><li>默认情况下，单种做种时间在 $hrday 天内 ≥ $hrhour 小时（特殊种子请参见种子详情页提示）</li><li style=color:red>符合以上条件的均不计算H&R</li></ul></td></tr></tbody></table>");
	} else {
		print("<table border=\"1\" cellpadding=\"10\" cellspacing=\"0\" width=\"40%\" colspan=\"10\"><tbody><tr><td><ul><li>发布时间 > 30天</li><li>单种分享率 ≥ $stronghr_radio</li><li>从开始下载时，单种做种时间在 " . ($stronghruntil / 86400) . " 天内 ≥ " . ($stronghr_time / 3600) . " 小时</li><li style=color:red>符合以上条件的均不计算H&R</li></ul></td></tr></tbody></table>");
	}
	$htmlout .= "<div><form method=\"GET\">过滤：<input type=checkbox name=hr value=1 " . ($_GET['hr'] == 1 ? "checked" : "") . " /><a href='?hr=1'>H&R</a> <input type=checkbox name=all value=1 " . ($_GET['all'] == 1 ? "checked" : "") . " /><a href='?all=1'>全部</a> <input type=submit value=提交 /></form></div><br />";
	$htmlout .= "<tr><td class='colhead'>种子</td><td class='colhead' align='center'>上传量</td><td class='colhead' align='center'>下载量</td><td class='colhead' align='center'>进度</td><td class='colhead' align='center'>分享率</td><td class='colhead' align='center'><a href='?hrtime=1'>H&R时间</a></td><td class='colhead' align='center'>做种时间</td><td class='colhead' align='center'>状态</td><td class='colhead' align='center'><a href='?hr=1'>H&R</a></td></tr>";
	while ($a = mysql_fetch_assoc($res)) {
		$b = get_row_count("peers", "WHERE userid = " . $a['userid'] . " AND torrent = " . $a['torrentid'] . " AND seeder = 'yes'");
		$c_value = mysql_fetch_array(sql_query("SELECT size FROM torrents WHERE id = " . $a['torrentid']));
		$c = $c_value[0];
		$deltime = date("Y-m-d H:i:s", time() - 1800);
		if ($stronghr == 'no') {
			$htmlout .= "<tr><td>" . get_torrents_name($a['torrentid']) . "</td>" .
					"<td align='center'>" . mksize($a['uploaded']) . "</td>" .
					"<td align='center'>" . mksize($a['downloaded']) . "</td>" .
					"<td align='center'>" . ($a['downloaded'] >= $c ? "100" : round($a['downloaded'] / $c * 100, 2)) . "%</td>" .
					"<td align='center'>" . (round($a['uploaded'] / $a['downloaded'], 3) == 0 ? "---" : round($a['uploaded'] / $a['downloaded'], 3)) . "</td>" .
					"<td align='center'>" . (($a['seedtime'] >= $a['hr_time'] || ($a['uploaded'] / $a['downloaded']) >= $hrradio) ? "完成" : mkprettytime($a['hr_time'] - $a['seedtime'])) . "</td>" .
					"<td align='center'>" . ($a['seedtime'] == 0 ? "---" : mkprettytime($a['seedtime'])) . "</td>" .
					"<td align='center'>" . ($b > 0 ? "在做种" : ($a['finished'] == 'yes' || $a['downloaded'] >= $a['size'] ? "已完成" : ($a['downloaded'] < $a['size'] && $a['last_action'] >= $deltime ? "在下载" : "---"))) . "</td>" .
					"<td align='center'>" . ($a['hr'] != 1 ? "<font style=\"color:green\">否</font>" : "<font style=\"color:red\">是</font>") . "</td>" .
					"</tr>";
		} else {
			$htmlout .= "<tr><td>" . get_torrents_name($a['torrentid']) . "</td>" .
					"<td align='center'>" . mksize($a['uploaded']) . "</td>" .
					"<td align='center'>" . mksize($a['downloaded']) . "</td>" .
					"<td align='center'>" . ($a['downloaded'] >= $c ? "100" : round($a['downloaded'] / $c * 100, 2)) . "%</td>" .
					"<td align='center'>" . (round($a['uploaded'] / $a['downloaded'], 3) == 0 ? "---" : round($a['uploaded'] / $a['downloaded'], 3)) . "</td>" .
					"<td align='center'>" . (($a['seedtime'] >= $stronghr_time || ($a['uploaded'] / $a['downloaded']) >= $stronghr_radio) ? "完成" : mkprettytime($stronghr_time - $a['seedtime'])) . "</td>" .
					"<td align='center'>" . ($a['seedtime'] == 0 ? "---" : mkprettytime($a['seedtime'])) . "</td>" .
					"<td align='center'>" . ($b > 0 ? "在做种" : ($a['finished'] == 'yes' || $a['downloaded'] >= $a['size'] ? "已完成" : ($a['downloaded'] < $a['size'] && $a['last_action'] >= $deltime ? "在下载" : "---"))) . "</td>" .
					"<td align='center'>" . ($a['hr'] != 1 ? "<font style=\"color:green\">否</font>" : "<font style=\"color:red\">是</font>") . "</td>" .
					"</tr>";
		}
	}
	$htmlout .= end_table_hr();
	$htmlout .= end_frame_hr();
	return $htmlout;
}

$startdat = sqlesc(date("Y-m-d H:i:s", time() - 86400 * 30)); //统计N天内的下载记录
$checkadded = sqlesc(date("Y-m-d H:i:s", time() - 86400 * 30)); //超过N天的不计算H&R
$HTMLOUT .= "<h1 align='center'>H&R查看统计</h1>";
if ($_GET['all'] == 1) {
	$count = get_row_count("snatched", "WHERE userid = " . $CURUSER['id']);
	list($pagertop, $pagerbottom, $limit) = pager(25, $count, "?all=1&");
	$res = sql_query("SELECT snatched.torrentid, snatched.userid, snatched.uploaded, snatched.downloaded, snatched.seedtime, snatched.finished, snatched.hr, snatched.last_action, torrents.hr_time, torrents.size FROM snatched INNER JOIN torrents ON snatched.torrentid = torrents.id WHERE snatched.userid = $CURUSER[id] AND snatched.torrentid IN (SELECT id FROM torrents WHERE status = 'normal') ORDER BY snatched.id DESC $limit") or sqlerr(__FILE__, __LINE__);
} elseif ($_GET['hr'] == 1) {
	$count = get_row_count("snatched", "WHERE hr = 1 AND userid = " . $CURUSER['id']);
	list($pagertop, $pagerbottom, $limit) = pager(25, $count, "?hr=1&");
	$res = sql_query("SELECT snatched.torrentid, snatched.userid, snatched.uploaded, snatched.downloaded, snatched.seedtime, snatched.finished, snatched.hr, snatched.last_action, torrents.hr_time, torrents.size FROM snatched INNER JOIN torrents ON snatched.torrentid = torrents.id WHERE snatched.userid = $CURUSER[id] AND snatched.hr = 1 ORDER BY snatched.id DESC $limit") or sqlerr(__FILE__, __LINE__);
} elseif ($_GET['hrtime'] == 1) {
	$count = mysql_num_rows(sql_query("SELECT snatched.*, torrents.* FROM snatched INNER JOIN torrents ON snatched.torrentid = torrents.id WHERE snatched.userid = $CURUSER[id] AND snatched.torrentid IN (SELECT id FROM torrents WHERE status = 'normal' AND added > $checkadded) AND snatched.seedtime < torrents.hr_time  AND snatched.uploaded / snatched.downloaded < $hrradio"));
	list($pagertop, $pagerbottom, $limit) = pager(25, $count, "?hrtime=1&");
	$res = sql_query("SELECT snatched.torrentid, snatched.userid, snatched.uploaded, snatched.downloaded, snatched.seedtime, snatched.finished, snatched.hr, snatched.last_action, torrents.hr_time, torrents.size FROM snatched INNER JOIN torrents ON snatched.torrentid = torrents.id WHERE snatched.userid = $CURUSER[id] AND snatched.torrentid IN (SELECT id FROM torrents WHERE status = 'normal' AND added > $checkadded) AND snatched.seedtime < torrents.hr_time  AND snatched.uploaded / snatched.downloaded < $hrradio ORDER BY (torrents.hr_time - snatched.seedtime) DESC, snatched.id DESC $limit") or sqlerr(__FILE__, __LINE__);
} else {
	$count = get_row_count("snatched", "WHERE startdat > $startdat AND userid = " . $CURUSER['id']);
	list($pagertop, $pagerbottom, $limit) = pager(25, $count, "?");
	$res = sql_query("SELECT snatched.torrentid, snatched.userid, snatched.uploaded, snatched.downloaded, snatched.seedtime, snatched.finished, snatched.hr, snatched.last_action, torrents.hr_time, torrents.size FROM snatched INNER JOIN torrents ON snatched.torrentid = torrents.id WHERE snatched.startdat > $startdat AND snatched.userid = $CURUSER[id] AND snatched.torrentid IN (SELECT id FROM torrents WHERE status = 'normal' AND added > $checkadded) ORDER BY snatched.id DESC $limit") or sqlerr(__FILE__, __LINE__);
}
$HTMLOUT .= bjtable_hr($res, "我的H&R统计", "H&R");
$HTMLOUT .= "<br /><br />";
if ($count) {
	print($pagertop);
	print $HTMLOUT;
	print($pagerbottom);
} else {
	print $HTMLOUT;
}
stdfoot();

