<?php
require "include/bittorrent.php";
dbconn();
require_once(get_langfile_path());
loggedinorreturn();

if (get_user_class() < UC_MODERATOR)
	permissiondenied();

$operate = 30; //操作总数
$salary = 20000; //工资

function searchlog($text) {
	global $year;
	global $month;
	$timestart = strtotime($year . "-" . $month . "-01 00:00:00");
	$sqlstarttime = date("Y-m-d H:i:s", $timestart);
	$timeend = strtotime("+1 month", $timestart);
	$sqlendtime = date("Y-m-d H:i:s", $timeend);
	$res = sql_query("SELECT COUNT(*) FROM sitelog WHERE txt LIKE '%$text%'  AND sitelog.added >= " . sqlesc($sqlstarttime) . " AND sitelog.added <= " . sqlesc($sqlendtime) . "") or sqlerr(__FILE__, __LINE__);
	$href = "log.php?query=" . rawurlencode($text) . "&search=all&action=dailylog";
	$row = mysql_fetch_array($res);
	$logcount = $row[0];
	$result = array("<a href=\"$href\" >$logcount </a>", "$logcount");
	return $result;
}

$unpassname = "";
$passname = "";
$year = 0 + $_GET['year'];
if (!$year || $year < 2000)
	$year = date('Y');
$month = 0 + $_GET['month'];
if (!$month || $month <= 0 || $month > 12)
	$month = date('m');


$order = $_GET['order'];
if (!in_array($order, array('username', 'torrent_size', 'torrent_count')))
	$order = 'username';
if ($order == 'username')
	$order .=' ASC';
else
	$order .= ' DESC';
stdhead($lang_uploaders['head_uploaders']);
begin_main_frame();
?>
<tr align="center">
	<td>
		<div>
			<?php
			$year2 = substr($datefounded, 0, 4);
			$yearfounded = ($year2 ? $year2 : 2007);
			$yearnow = date("Y");

			$timestart = strtotime($year . "-" . $month . "-01 00:00:00");
			$sqlstarttime = date("Y-m-d H:i:s", $timestart);
			$timeend = strtotime("+1 month", $timestart);
			$sqlendtime = date("Y-m-d H:i:s", $timeend);

			print("<h1 align=\"center\">" . $lang_uploaders['text_uploaders'] . " - " . date("Y-m", $timestart) . "月 - 考核情况</h1>");
			$date = date("Y-m", $timestart);

			$yearselection = "<select name=\"year\">";
			for ($i = $yearfounded; $i <= $yearnow; $i++)
				$yearselection .= "<option value=\"" . $i . "\"" . ($i == $year ? " selected=\"selected\"" : "") . ">" . $i . "</option>";
			$yearselection.="</select>";

			$monthselection = "<select name=\"month\">";
			for ($i = 1; $i <= 12; $i++)
				$monthselection .= "<option value=\"" . $i . "\"" . ($i == $month ? " selected=\"selected\"" : "") . ">" . $i . "</option>";
			$monthselection.="</select>";
			?>
			<table border="1" cellpadding="10" cellspacing="0" width="750"><tbody><tr><td>
							<h1 style="width: 60%">现行考核标准为：</h1>
							<ul>
								<li>每月操作数需要 >= <?= $operate ?>，工资为 <?= $salary ?> 魔力值。</li>
							</ul>
						</td></tr></tbody></table><br />
			<div>
				<form method="get">
					<span>
						<?php echo $lang_uploaders['text_select_month'] ?><?php echo $yearselection ?>&nbsp;&nbsp;<?php echo $monthselection ?>&nbsp;&nbsp;<input type="submit" value="<?php echo $lang_uploaders['submit_go'] ?>" />
					</span>
				</form>
			</div>

			<?php
			$numbers = get_row_count("users", "WHERE class = " . UC_MODERATOR);
			if (!$numbers)
				print("<p align=\"center\">" . $lang_uploaders['text_no_uploaders_yet'] . "</p>");
			else {
				?>

				<div style="margin-top: 8px">
					<?php
					print("<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" width=\"950\"><tr>");
					print("<td class=\"colhead\" align=\"center\">用户名</td>");
					print("<td class=\"colhead\" align=\"center\">编辑种子</td>");
					print("<td class=\"colhead\" align=\"center\">删除种子</td>");
					print("<td class=\"colhead\" align=\"center\">操作候选</td>");
					print("<td class=\"colhead\" align=\"center\">操作求种</td>");
					print("<td class=\"colhead\" align=\"center\">操作回收站</td>");
					print("<td class=\"colhead\" align=\"center\">删除字幕</td>");
					print("<td class=\"colhead\" align=\"center\">是否合格</td>");
					if (get_user_class() >= UC_ADMINISTRATOR)
						print("<td class=\"colhead\" align=\"center\">状态</td>");
					print("</tr>");
					$res = sql_query("SELECT id, username, class FROM users WHERE class >= " . UC_MODERATOR . " AND class <" . UC_STAFFLEADER . " ORDER BY class ASC, id ASC") or sqlerr(__LINE__, __FILE__);
					while ($row = mysql_fetch_array($res)) {
						$editlog = searchlog("管理员 $row[username] 编辑");
						$dellog = searchlog("管理员 $row[username] 删除");
						$offerlog = searchlog("管理员 $row[username] 发布了 候选");
						$reqlog = searchlog("求种：管理员 $row[username]");
						$uptitlelog = searchlog("管理员 $row[username] 移入回收站");
						$deltitlelog = searchlog("字幕：管理员 $row[username] 删除了字幕");

						$all = $editlog[1] + $dellog[1] + $offerlog[1] + $reqlog[1] + $uptitlelog[1] + $deltitlelog[1];
						print("<tr>");
						print("<td class=\"colfollow\">" . get_username($row['id'], false, true, true, false, false, true) . "</td>");
						print("<td class=\"colfollow\">" . $editlog[0] . "</td>");
						print("<td class=\"colfollow\">" . ($dellog[0]) . "</td>");
						print("<td class=\"colfollow\">" . ($offerlog[0]) . "</td>");
						print("<td class=\"colfollow\">" . ($reqlog[0]) . "</td>");
						echo ("<td class=\"colfollow\">$uptitlelog[0]</td>");
						echo ("<td class=\"colfollow\">$deltitlelog[0]</td>");
						echo ("<td class=\"colfollow\">" . ($all >= $operate ? "<b style=\"color:green\">合格 (操作数$all)</b>" : "<b style=\"color:red\">不合格 (操作数$all)</b>") . "</td>");
						if (get_user_class() >= UC_ADMINISTRATOR)
							if ($all >= $operate) {
								print("<td class=\"colfollow\">统一发放</td>");
								$passname .= $row['username'] . ",";
							} else {
								if ($row['class'] < UC_ADMINISTRATOR) {
									print("<td class=\"colfollow\">统一提醒</td>");
									$unpassname .= $row['username'] . ",";
								} else
									print("<td class=\"colfollow\">无需考核</td>");
							}
						print("</tr>");
					}
					print("</table><br />");
					if (get_user_class() >= UC_ADMINISTRATOR) {
						$passname = rtrim($passname, ",");
						$unpassname = rtrim($unpassname, ",");
						print(
								"<table style=\"width: 950px\">"
								. "<tr>"
								. "<td class='colhead' align=\"center\">给管理员发提醒</td>"
								. "<td class='colhead' align=\"center\">给管理员发工资</td>"
								. "</tr>"
								. "<tr>"
								. "<td class=\"colfollow\">"
								. "<form method=\"post\" action=\"amountbonus.php\" /><br/>"
								. "魔力：<input type='text' value='5000' name='seedbonus' style='width: 80%' /><br/><br/>"
								. "人员：<input type='text' value='$unpassname' name='username' style='width: 80%' readonly /><br/><br/>"
								. "原因：<input type='text' value='很遗憾 $date 月管理员考核没有通过，如果在岗的话请对自己的版块负责。特此提醒' name='reason_1' style='width: 80%' /><br/><br/>"
								. "<input type=\"submit\" value=\"给他们警告\" class=\"btn\" /><br/>"
								. "</form><br/>"
								. "</td>"
								. "<td class=\"colfollow\">"
								. "<form method=\"post\" action=\"amountbonus.php\" /><br/>"
								. "魔力：<input type='text' value='$salary' name='seedbonus' style='width: 80%' /><br/><br/>"
								. "人员：<input type='text' value='$passname' name='username' style='width: 80%' readonly /><br/><br/>"
								. "原因：<input type='text' value='恭喜 $date 月管理员考核通过，工资奉上' name='reason_1' style='width: 80%' /><br/><br/>"
								. "<input type=\"submit\" value=\"给他们发工资\" class=\"btn\" /><br/>"
								. "</form><br/>"
								. "</td>"
								. "</tr>"
								. "</table><br/>"
						);
					}
					?>
				</div>
				<?php
			}
			?>
		</div>
	</td>
</tr>
<?php
end_main_frame();
stdfoot();
