<?php
header("Expires: Mon, 26 Jul 1970 05:00:00 GMT");

require_once("include/bittorrent.php");
dbconn();
require_once(get_langfile_path());
loggedinorreturn();
parked();
secrity();
if ($CURUSER["uploadpos"] == 'no') {
	stderr($lang_upload['std_sorry'], $lang_upload['std_unauthorized_to_upload'], false);
}

if ($enableoffer == 'yes') {
	$has_allowed_offer = get_row_count("offers", "WHERE allowed = 'allowed' AND userid = " . sqlesc($CURUSER["id"]));
} else {
	$has_allowed_offer = 0;
}
$uploadfreely = user_can_upload("torrents");
$allowtorrents = ($has_allowed_offer || $uploadfreely) || true;
$allowspecial = user_can_upload("music");

if (!$allowtorrents && !$allowspecial) {
	stderr($lang_upload['std_sorry'], $lang_upload['std_please_offer'], false);
}
$allowtwosec = ($allowtorrents && $allowspecial);
$brsectiontype = $browsecatmode;
$spsectiontype = $specialcatmode;
$showsource = (($allowtorrents && get_searchbox_value($brsectiontype, 'showsource')) || ($allowspecial && get_searchbox_value($spsectiontype, 'showsource'))); //whether show sources or not
$showmedium = (($allowtorrents && get_searchbox_value($brsectiontype, 'showmedium')) || ($allowspecial && get_searchbox_value($spsectiontype, 'showmedium'))); //whether show media or not
$showcodec = (($allowtorrents && get_searchbox_value($brsectiontype, 'showcodec')) || ($allowspecial && get_searchbox_value($spsectiontype, 'showcodec'))); //whether show codecs or not
$showstandard = (($allowtorrents && get_searchbox_value($brsectiontype, 'showstandard')) || ($allowspecial && get_searchbox_value($spsectiontype, 'showstandard'))); //whether show standards or not
$showprocessing = (($allowtorrents && get_searchbox_value($brsectiontype, 'showprocessing')) || ($allowspecial && get_searchbox_value($spsectiontype, 'showprocessing'))); //whether show processings or not
$showteam = (($allowtorrents && get_searchbox_value($brsectiontype, 'showteam')) || ($allowspecial && get_searchbox_value($spsectiontype, 'showteam'))); //whether show teams or not
$showaudiocodec = (($allowtorrents && get_searchbox_value($brsectiontype, 'showaudiocodec')) || ($allowspecial && get_searchbox_value($spsectiontype, 'showaudiocodec'))); //whether show languages or not

stdhead($lang_upload['head_upload']);
//加入做种版权类问题公告信息

/* ?>
  <table align="center">
  <tr><td><?=$lang_upload['text_zhuyi']?></td></tr>
  <tr><td></td></tr>
  </table> */
print("<div align=\"center\"><form method=\"get\" action=\"torrents.php?\" target=\"_blank\">" . $lang_upload['text_search_offer_note'] . "&nbsp;&nbsp;<input type=\"text\" name=\"search\">&nbsp;&nbsp;<input type=\"hidden\" name=\"incldead\" value=0>");
print("<input type=\"submit\" class=\"btn\" value=\"" . $lang_upload['submit_search'] . "\" /></form></div>");
?>
<form id="compose" enctype="multipart/form-data" action="takeupload.php" method="post" name="upload">
	<table border="1" cellspacing="0" cellpadding="5" width="940">
		<tr>
			<td class='colhead' colspan='2' align='center'>
				<?php echo $lang_upload['text_tracker_url'] ?>：<b><?php echo get_protocol_prefix() . $announce_urls[0] ?></b>
				<?php
				if (!is_writable($torrent_dir))
					print("<br /><br /><b>ATTENTION</b>: Torrent directory isn't writable. Please contact the administrator about this problem!");
				if (!$max_torrent_size)
					print("<br /><br /><b>ATTENTION</b>: Max. Torrent Size not set. Please contact the administrator about this problem!");
				?>
			</td>
		</tr>
		<tr>
			<td class='colhead' colspan='2' align='center'>
				<?php print($lang_upload['text_notice']); ?>
			</td>
		</tr>
		<script type="text/javascript">
			$(document).ready(function () {
				uplist("source_sel", new Array(['0', '请先选择一级类型']));
				$("#browsecat").change(function () {
					secondtype($("#browsecat").val());
					removeSubcat();
				});

				function catChange() {
					secondtype($("#browsecat").val());
					removeSubcat();
				}

				$("#source_sel").change(function () {
					//start modified by SamuraiMe,2013.05.17
					//用于自动生成种子标题
					removeSubcat();
					$.getJSON("guize.php?id=" + $("#browsecat").val() + "&source_sel=" + $(this).val() + "&t=" + new Date(), function (result) {
						//$("#gstishi").html(result[0]);//标题下方规范事例
						$("td td:has('#name')").prepend(result[1]);
						$("#subcat").slideDown();
					});
				});

				$("#subcat input:checkbox, #subcat input:radio").live("click", function () {
					var name = $(this).attr("name");
					var index = $(this).index("#subcat input[name=" + name + "]");
					var length = $("#subcat input[name=" + name + "]").length;
					if ($(this).next().next().is("[id=temp_input]")) {
						$(this).next().toggle().next().toggle();
					} else {
						if (index == length - 1) {
							var input = $(this).next().text();
							$(this).next().hide().after('<input type="text" id="temp_input" value="' + input + '"/>').focus();
							$(this).val($(this).next().next().val());
							$(this).next().text($(this).next().next().val());
						} else {
							removeTempInput();
						}
					}
				});

				$("#temp_input").live("blur", function () {
					$(this).prev().text($(this).val()).show();
					$(this).prev().prev().val($(this).val());
					$(this).remove();
					generateName();
				});


				$("#subcat input").live("change", function () {
					generateName();
				});

				function removeSubcat() {
					if ($("#subcat").length > 0) {
						$("#subcat").slideUp(function () {
							$(this).remove();
						});
					}
				}

				function removeTempInput() {
					$("#temp_input").prev().text($("#temp_input").val()).prev().val($("#temp_input").val());
					$("#temp_input").prev().show().end().remove();
					//$("#temp_input").remove();
				}

				//自动标题生成
				function generateName() {
					var names = new Array();
					var tempName = '';
					var name = '';
					$("#subcat input:checked, #subcat input[value!=''][id!=temp_input]:text, #subcat input[type=hidden]").each(function () {
						if ($(this).attr("name") == tempName) {
							names[names.length - 1] += "/" + $(this).val();
						} else {
							names[names.length] = $(this).val();
						}
						tempName = $(this).attr("name");
					});

					for (var i = 0; i < names.length; i++) {
						name += "[" + names[i] + "]";
					}
					//$("#name").val(name);//生成最终标题
				}
				//end modified by SamuraiMe,2013.05.17

				//modified by SamuraiMe,2013.05.19 获取豆瓣与IMDB的URL
				//获取豆瓣url
				$("#browsecat").change(function () {
					var cat = $(this).val();
					if (401 != cat && 402 != cat && 405 != cat && 414 != cat && 404 != cat) {
						$("#select_douban").remove();
						$("#reselect_douban").remove();
					}
				});
				//中文名改变时刷新显示可能的豆瓣链接
				$("[name=chinese_name]").live("blur", function () {
					$("#reselect_douban").remove();
					displayDoubanItem();
				});
				$("#reselect_douban").live("click", function () {
					$(this).remove();
					displayDoubanItem();
				});
				//豆瓣链接被选中时
				$(".douban_item").live("click", function () {
					$a = $(this).find("a");
					var url = $a.first().attr("href");
					$("[name=dburl]").val(url);
					$("#select_douban").remove();
					$("[name=dburl]").after("<input type=\"button\" id=\"reselect_douban\"value=\"重新选择\"/>");
				});

				function displayDoubanItem() {
					var q = $("[name=chinese_name]").val();
					var cat = $("#browsecat").val();
					if (q && (401 == cat || 402 == cat || 405 == cat || 414 == cat || 404 == cat)) {
						var requestUrl = "imdb/imdb_url.php?res=douban&title=" + q + "&type=" + $("#browsecat").val();
						$.get(requestUrl, function (data) {
							if ($("#select_douban").length > 0) {
								$("#select_douban").remove();
							}
							if (data.length > 0) {
								$("[name=dburl]").after(data);
							}
						});
					}
				}


				//英文名改变时刷新显示可能的imdb链接
				$("[name=english_name]").live("blur", function () {
					$("#reselect_imdb").remove();
					displayImdbItem();
				});
				$("#reselect_imdb").live("click", function () {
					$(this).remove();
					displayImdbItem();
				});
				//imdb链接被选中时
				$(".imdb_item").live("click", function () {
					$a = $(this).find("a");
					var url = $a.first().attr("href");
					$("[name=imdburl]").val(url);
					$("#select_imdb").remove();
					$("[name=imdburl]").after("<input type=\"button\" id=\"reselect_imdb\"value=\"重新选择\"/>");
				});

				function displayImdbItem() {
					var q = $("[name=english_name]").val();
					var cat = $("#browsecat").val();
					if (q && (401 == cat || 402 == cat || 405 == cat || 414 == cat || 404 == cat)) {
						var requestUrl = "imdb/imdb_url.php?res=imdb&title=" + q + "&type=" + $("#browsecat").val();
						$.get(requestUrl, function (data) {
							if ($("#select_imdb").length > 0) {
								$("#select_imdb").remove();
							}
							if (data.length > 0) {
								$("[name=imdburl]").after(data);
							}
						});
					}
				}
				//获取豆瓣与IMDB的URL结束

				//引用发布功能开始
				if ($("#cite_torrent").val() != "") {
					citeTorrent();
				}
				$("#cite_torrent_btn").click(function () {
					citeTorrent();
				});
				function citeTorrent() {
					var id = $("#cite_torrent").val();
					//需要判断ID是否合法
					if (id != '') {
						if ($("#cite_hint").length > 0) {
							$("#cite_hint").remove();
						}
						$.getJSON("citetorrent.php?torrent_id=" + id, function (data) {
							if (data["exist"] == "yes") {
								$("#browsecat").val(data["category"]);
								catChange();
								$("#source_sel").val(data["source"]);
								//$("#name").val(data["name"]);
								$("input[name=small_descr]").val(data["small_descr"]);
								$("input[name=imdburl]").val(data["url"]);
								$("input[name=dburl]").val(data["dburl"]);
								//$("#descr").text(data["descr"]);//原始编辑器
								$('#sceditor').sceditor('instance').val("");//引用种子时先清空编辑器
								$('#sceditor').sceditor('instance').insertText(data["descr"]);
							} else {
								$("#cite_torrent_btn").after("<span id=\"cite_hint\">所引用的种子不存在...<span>");
							}
						});
					}
				}
				//引用发布功能结束

				$("#qr").click(function () {
					var err = "";
					if ($("#browsecat").val() == 0)
						err += "请选择[类型]\n\n";
					if ($("#source_sel").val() == 0)
						err += "请选择[子类型]\n\n";
					if ($("#torrent").val() == "")
						err += "请选择[种子文件]\n\n";
					if ($("#name").val().length < 10)
						err += "[标题]内容不得少于10个字符\n\n";
					//if ($("#descr").val().length < 10)//原始编辑器
					if ($('#sceditor').sceditor('instance').val().length < 10)
						err += "[简介]内容不得少于10个字符\n\n";
					//if ($("#descr").val().search(/attach|img/) == -1)//原始编辑器
					if ($('#sceditor').sceditor('instance').val().search(/attach|img/) == -1)
						err += "[简介]内容必须包含图片\n\n";
					if (err == "")
						return true;
					jAlert(err);
					return false;
				});
			});

			function uplist(name, list) {
				var childRet = document.getElementById(name);
				for (var i = childRet.childNodes.length - 1; i >= 0; i--) {
					childRet.removeChild(childRet.childNodes.item(i));
				}
				for (var j = 0; j < list.length; j++) {
					var ret = document.createDocumentFragment();
					var newop = document.createElement("option");
					newop.id = list[j][0];
					newop.value = list[j][0];
					newop.appendChild(document.createTextNode(list[j][1]));
					ret.appendChild(newop);
					document.getElementById(name).appendChild(ret);
				}
			}

			function secondtype(value) {
<?php
$cats = genrelist($browsecatmode);
foreach ($cats as $row) {
	$catsid = $row['id'];
	$secondtype = searchbox_item_list("sources", $catsid);
	$secondsize = count($secondtype, 0);
	$cachearray = $cachearray . "var lid" . $catsid . " = new Array(['0','请选择子类型']";
	for ($i = 0; $i < $secondsize; $i++) {
		$cachearray = $cachearray . ",['" . $secondtype[$i]['id'] . "','" . $secondtype[$i]['name'] . "']";
	}
	$cachearray = $cachearray . ");\n";
}
$cachearray = $cachearray . "switch(value){\n";
foreach ($cats as $row) {
	$catsid = $row['id'];
	$cachearray = $cachearray . "\tcase \"" . $catsid . "\": ";
	$cachearray = $cachearray . "uplist(\"source_sel\",lid" . $catsid . ");";
	$cachearray = $cachearray . "break;\n";
}
$cachearray = $cachearray . "}\n";
print($cachearray);
?>
			}
		</script>
		<?php
		$torrent_id = isset($_GET["cite_torrent_id"]) ? 0 + $_GET["cite_torrent_id"] : "";
		$_SESSION['upload'] = mt_rand(1000000, 9999999);
		print("<input type=hidden name=upload value='" . $_SESSION['upload'] . "' />");
		tr("引用发布", "<input type=\"text\" id=\"cite_torrent\" name=\"cite_torrent\" value=\"$torrent_id\" /><input type=\"button\" id=\"cite_torrent_btn\" value=\"引用\"/>" . "  填写种子ID", 1);
		//tr($lang_upload['row_torrent_file']."<font color=\"red\">*</font>", "<input type=\"file\" class=\"file\" id=\"torrent\" name=\"file\" />\n", 1);
		tr($lang_upload['row_torrent_file'] . "<font color=\"red\">*</font>", "<input type=\"file\" class=\"file\" id=\"torrent\" name=\"file\" onchange=\"getname()\" />\n", 1);
		if ($allowtorrents) {
			$disablespecial = " onchange=\"disableother('browsecat','specialcat')\"";
			$s = "<select name=\"type\" id=\"browsecat\" " . ($allowtwosec ? $disablespecial : "") . ">\n<option value=\"0\">" . $lang_upload['select_choose_one'] . "</option>\n";
			$cats = genrelist($browsecatmode);
			foreach ($cats as $row)
				$s .= "<option value=\"" . $row["id"] . "\">" . htmlspecialchars($row["name"]) . "</option>\n";
			$s .= "</select>\n";
		} else
			$s = "";
		if ($allowspecial) {
			$disablebrowse = " onchange=\"disableother('specialcat','browsecat')\"";
			$s2 = "<select name=\"type\" id=\"specialcat\" " . $disablebrowse . ">\n<option value=\"0\">" . $lang_upload['select_choose_one'] . "</option>\n";
			$cats2 = genrelist($specialcatmode);
			foreach ($cats2 as $row)
				$s2 .= "<option value=\"" . $row["id"] . "\">" . htmlspecialchars($row["name"]) . "</option>\n";
			$s2 .= "</select>\n";
		} else
			$s2 = "";
		tr($lang_upload['row_type'] . "<font color=\"red\">*</font>", ($allowtwosec ? $lang_upload['text_to_browse_section'] : "") . $s . ($allowtwosec ? $lang_upload['text_to_special_section'] : "") . $s2 . ($allowtwosec ? $lang_upload['text_type_note'] : ""), 1);

		if ($showsource || $showmedium || $showcodec || $showaudiocodec || $showstandard || $showprocessing) {
			if ($showsource) {
				$source_select = torrent_selection($lang_upload['text_source'], "source_sel", "sources");
			} else
				$source_select = "";

			if ($showmedium) {
				$medium_select = torrent_selection($lang_upload['text_medium'], "medium_sel", "media");
			} else
				$medium_select = "";

			if ($showcodec) {
				$codec_select = torrent_selection($lang_upload['text_codec'], "codec_sel", "codecs");
			} else
				$codec_select = "";

			if ($showaudiocodec) {
				$audiocodec_select = torrent_selection($lang_upload['text_audio_codec'], "audiocodec_sel", "audiocodecs");
			} else
				$audiocodec_select = "";

			if ($showstandard) {
				$standard_select = torrent_selection($lang_upload['text_standard'], "standard_sel", "standards");
			} else
				$standard_select = "";

			if ($showprocessing) {
				$processing_select = torrent_selection($lang_upload['text_processing'], "processing_sel", "processings");
			} else
				$processing_select = "";

			tr($lang_upload['row_quality'] . "<font color=red>*</font>", "<select id='source_sel' name='source_sel'></select>", 1);
		}

		//tr($lang_upload['row_torrent_file']."<font color=\"red\">*</font>", "<input type=\"file\" class=\"file\" id=\"torrent\" name=\"file\" onchange=\"getname()\" />\n", 1);
		//tr($lang_upload['row_torrent_file']."<font color=\"red\">*</font>", "<input type=\"file\" class=\"file\" id=\"torrent\" name=\"file\" />\n", 1);
		//tr($lang_upload['row_torrent_name'], "<input type=\"text\" style=\"width: 650px;\" id=\"name\" name=\"name\" /><br /><font class=\"medium\">" . $lang_upload['text_torrent_name_note'] . "</font>", 1);
		tr($lang_upload['row_torrent_name'], "<input type=\"text\" style=\"width: 650px;\" id=\"name\" name=\"name\" />" . (get_user_class() >= $torrentmanage_class ? " <input id=\"cxcolortitle\" name=\"cxcolortitle\" class=\"input_cxcolor\" type=\"text\" value=\"#000000\" style=\"background-color: rgb(0, 0, 0);\" />" : "") . "<br /><font class=\"medium\">" . $lang_upload['text_torrent_name_note'] . "</font>", 1);
		if ($smalldescription_main == 'yes')
			tr($lang_upload['row_small_description'], "<input type=\"text\" style=\"width: 650px;\" id=\"small_descr\" name=\"small_descr\" />" . (get_user_class() >= $torrentmanage_class ? " <input id=\"cxcolordesc\" name=\"cxcolordesc\" class=\"input_cxcolor\" type=\"text\" value=\"#000000\" style=\"background-color: rgb(0, 0, 0);\" />" : "") . "<br /><font class=\"medium\">" . $lang_upload['text_small_description_note'] . "</font>", 1);
		//tr($lang_upload['row_description_note'],"<br /><font size=+1 color=brown>".$lang_upload['text_description_note']."</font>", 1);
		get_external_tr();
		//get_dbexternal_tr();
		if ($enablenfo_main == 'yes')
			tr($lang_upload['row_nfo_file'], "<input type=\"file\" class=\"file\" name=\"nfo\" /><br /><font class=\"center\">" . $lang_upload['text_only_viewed_by'] . get_user_class_name_zh($viewnfo_class, false, true, true) . $lang_upload['text_or_above'] . "</font>", 1);
		print("<tr><td class=\"rowhead\" style='padding: 3px' valign=\"top\">" . $lang_upload['row_description'] . "<font color=\"red\">*</font></td><td class=\"rowfollow\">");
		//textbbcode("upload", "descr", "", false); //原始编辑器
		$uploaddescr = "
这里插入海报或封面图片真实链接并删除或替换此行

[b][color=blue]※※※※※※※　影片信息　※※※※※※※[/color][/b]

◎译      名：
◎片      名：
◎年      代：
◎国      家：
◎类      别：
◎语      言：
◎上映日期：
◎IMDb评分：
◎IMDb链接：
◎片      长：
◎导      演：
◎主      演：

◎剧情简介：

◎幕后花絮：

[b][color=blue]※※※※※※※　参数信息　※※※※※※※[/color][/b]

[quote]文件名：
体　积：
时　长：
片　源：
帧　率：
分辨率：
视　频：
音　频：
字　幕：
压制组：
压制者：[/quote]

[b][color=blue]※※※※※※※　截图信息　※※※※※※※[/color][/b]

这里插入截图信息并删除或替换此行
";
		//print("<textarea id='sceditor' name='descr' onkeydown='ctrlenter(event,'compose','qr')' style='width:100%;height:400px;'>$uploaddescr</textarea>");
		print("<textarea id='sceditor' name='descr' onkeydown='ctrlenter(event,'compose','qr')' style='width:100%;height:400px;'></textarea>");
		print("</td></tr>\n");

		tr($lang_upload['row_info'], $source_select . $medium_select . $codec_select . $audiocodec_select . $standard_select . $processing_select, 1);

		if ($showteam) {
			if ($showteam) {
				$team_select = torrent_selection($lang_upload['text_team'], "team_sel", "teams");
			} else
				$showteam = "";

			tr($lang_upload['row_content'], $team_select, 1);
		}

//==== offer dropdown for offer mod  from code by S4NE
		$offerres = sql_query("SELECT id, name FROM offers WHERE userid = " . sqlesc($CURUSER[id]) . " AND allowed = 'allowed' ORDER BY name ASC") or sqlerr(__FILE__, __LINE__);
		if (mysql_num_rows($offerres) > 0) {
			$offer = "<select name=\"offer\"><option value=\"0\">" . $lang_upload['select_choose_one'] . "</option>";
			while ($offerrow = mysql_fetch_array($offerres))
				$offer .= "<option value=\"" . $offerrow["id"] . "\">" . htmlspecialchars($offerrow["name"]) . "</option>";
			$offer .= "</select>";
			tr($lang_upload['row_your_offer'] . (!$uploadfreely && !$allowspecial ? "<font color=red>*</font>" : ""), $offer . $lang_upload['text_please_select_offer'], 1);
		}
//===end
//促销开始
		$pickcontent = "<b style=\"color:red\">请注意，“置顶”类设置项的时长不填写时默认为6小时；超级置顶等级1<2<3，建议使用1级超级置顶</b><br />";
		if (get_user_class() >= $torrentonpromotion_class || $CURUSER['id'] == $robotusers) {
			$pickcontent .= "<b>" . $lang_upload['row_special_torrent'] . ":&nbsp;</b>" . "<select name=\"sel_spstate\" style=\"width: 100px;\">" . promotion_selection($row["sp_state"], 0) . "</select>&nbsp;&nbsp;&nbsp;<b>" . $lang_upload['row_special_torrent_time'] . ":&nbsp;</b><input type=\"text\" name=\"freetime\" size=3 maxlength=3 /><b>天(留空不修改，-1为永久)<input type=\"text\" name=\"freetimeh\" size=3 maxlength=2 />小时，当前至$row[endfree]</b><br />";
		}
		if (get_user_class() >= $torrentsticky_class || $CURUSER['id'] == $robotusers) {
			$pickcontent .= "<b>" . $lang_upload['row_torrent_position'] . ":&nbsp;</b>" . "<select name=\"sel_posstate\" style=\"width: 100px;\">" .
					"<option" . (($row["pos_state"] == "normal") ? " selected=\"selected\"" : "" ) . " value=\"0\">" . $lang_upload['select_normal'] . "</option>" .
					"<option" . (($row["pos_state"] == "sticky") ? " selected=\"selected\"" : "" ) . " value=\"1\">" . $lang_upload['select_sticky'] . "</option>" .
					"</select>&nbsp;&nbsp;&nbsp;<b>" . $lang_upload['row_torrent_position_time'] . ":&nbsp;</b><input type=\"text\" name=\"stickytime\" size=3 maxlength=3 /><b>天(留空不修改，-1为永久)<input type=\"text\" name=\"stickytimeh\" size=3 maxlength=2 />小时，当前至$row[endsticky]</b><br />";
		}
		//超级置顶
		if (get_user_class() >= $torrentsticky_class || $CURUSER['id'] == $robotusers) {
			$pickcontent .= "<b>" . $lang_upload['row_marrow'] . ":&nbsp;</b>" . "<select name=\"sel_marrow\" style=\"width: 100px;\">" .
					"<option" . (($row["marrow"] == "normal") ? " selected=\"selected\"" : "" ) . " value=\"0\">" . $lang_upload['select_normal'] . "</option>" .
					"<option" . (($row["marrow"] == "marrow1") ? " selected=\"selected\"" : "" ) . " value=\"1\">" . $lang_upload['select_marrow1'] . "</option>" .
					"<option" . (($row["marrow"] == "marrow2") ? " selected=\"selected\"" : "" ) . " value=\"2\">" . $lang_upload['select_marrow2'] . "</option>" .
					"<option" . (($row["marrow"] == "marrow3") ? " selected=\"selected\"" : "" ) . " value=\"3\">" . $lang_upload['select_marrow3'] . "</option>" .
					"</select>&nbsp;&nbsp;&nbsp;<b>" . $lang_upload['row_torrent_position_time'] . ":&nbsp;</b><input type=\"text\" name=\"marrowtime\" size=3 maxlength=3 /><b>天(留空不修改，-1为永久)<input type=\"text\" name=\"marrowtimeh\" size=3 maxlength=2 />小时，当前至$row[endmarrow]</b><br />";
		}
		//增加自定义H&R
		if ($hr == 'yes' && get_user_class() >= $torrentonpromotion_class || $CURUSER['id'] == $robotusers) {
			$pickcontent .= "<b>" . $lang_upload['row_torrent_hr'] . ": </b>" . "<select name=\"hr_posstate\" style=\"width: 100px;\">" .
					"<option" . (($row["hr_state"] == "normal") ? " selected=\"selected\"" : "" ) . " value=\"0\">" . $lang_upload['hr_select_normal'] . "</option>" .
					"<option" . (($row["hr_state"] == "sticky") ? " selected=\"selected\"" : "" ) . " value=\"1\">" . $lang_upload['hr_select_sticky'] . "</option>" .
					"<option" . (($row["hr_state"] == "no") ? " selected=\"selected\"" : "" ) . " value=\"2\">" . $lang_upload['hr_select_no'] . "</option>" .
					"</select>   ";
			$pickcontent .= "&nbsp;&nbsp;<b>" . $lang_upload['text_hr_time'] . ": </b>" .
					"<input type=\"text\"  name=\"hrtime\" size=3 value=\"" . (empty($row["hr_time"]) ? "$hrhour" : $row["hr_time"] / 3600) . "\" /><b>" . $lang_upload['text_minutes'] . "，</b>";
			$pickcontent .= "<b>" . $lang_upload['text_hr_until'] . ": </b>" .
					"<input type=\"text\"  name=\"hruntil\" size=3 value=\"" . (empty($row["hr_until"]) ? "$hrday" : $row["hr_until"] / 86400) . "\" /><b>" . $lang_upload['text_hours'] . "(留空不修改)</b><br />";
		} else {//无设置H&R权限的账户发布种子时，使用默认的H&R规则
			print("<input type=\"hidden\" name=\"hr_posstate\" value=\"0\" /><input type=\"hidden\" name=\"hrtime\" value=\"$hrhour\" /><input type=\"hidden\" name=\"hruntil\" value=\"$hrday\" />");
		}
		if (get_user_class() >= $torrentmanage_class || $CURUSER['id'] == $robotusers) {
			$pickcontent .= "<b>" . $lang_upload['row_recommended_movie'] . ":&nbsp;</b>" . "<select name=\"sel_recmovie\" style=\"width: 100px;\">" .
					"<option" . (($row["picktype"] == "normal") ? " selected=\"selected\"" : "" ) . " value=\"0\">" . $lang_upload['select_normal'] . "</option>" .
					"<option" . (($row["picktype"] == "hot") ? " selected=\"selected\"" : "" ) . " value=\"1\">" . $lang_upload['select_hot'] . "</option>" .
					"<option" . (($row["picktype"] == "classic") ? " selected=\"selected\"" : "" ) . " value=\"2\">" . $lang_upload['select_classic'] . "</option>" .
					"<option" . (($row["picktype"] == "recommended") ? " selected=\"selected\"" : "" ) . " value=\"3\">" . $lang_upload['select_recommended'] . "</option>" .
					"</select>";
		}
		if (get_user_class() >= $torrentmanage_class || $CURUSER['id'] == $robotusers) {
			tr($lang_upload['row_pick'], $pickcontent, 1);
		}
//促销结束
		tr("<font style=\"color:red\">注意</font>", "<font style=\"color:red\">不要随意勾选“首发”和“禁转”，除非这是全网首发或是你的独有资源。</font>" . (get_user_class() >= UC_UPLOADER ? "<font style=\"color:blue\">“限定”资源只有被授权的用户才可以下载。</font>" : ""), 1);
		tr("首发/禁转", "<input type=\"checkbox\" name=\"first\" value=\"no\" />" . $lang_upload['checkbox_first'] . "<input type=\"checkbox\" name=\"excl\" value=\"no\" />" . $lang_upload['checkbox_excl'] . (get_user_class() >= UC_UPLOADER || $CURUSER['id'] == $robotusers ? "<input type=\"checkbox\" name=\"official\"" . ($row["official"] == "yes" ? " checked=\"checked\"" : "" ) . " value=\"1\" />官方<input type=\"checkbox\" name=\"limitd\"" . ($row["limitd"] == "yes" ? " checked=\"checked\"" : "" ) . " value=\"1\" />限定" : ""), 1);
		tr("定时发布", "预计发布时间: <input type='text' id=releasedate name=releasedate style=\"width: 120px\" class=\"Wdate\" onfocus=\"WdatePicker({doubleCalendar:true,isShowWeek:'true',minDate:'%y-%M-{%d+1}'})\" value='" . ($row['releasedate'] ? $row['releasedate'] : "") . "' /> 用以同时发布过多种子，导致上传带宽不足出种困难时使用", 1);
		if (get_user_class() >= $beanonymous_class) {
			if (get_user_class() < $upload_class) {//小于发布权限，则不显示发布到候选
				tr($lang_upload['row_show_uploader'], "<input type=\"checkbox\" name=\"uplver\"" . ($CURUSER['anon'] == 'yes' ? " checked=\"checked\" " : " ") . "value=\"yes\" />" . $lang_upload['checkbox_hide_uploader_note'], 1);
			} else {
				tr($lang_upload['row_show_uploader'], "<input type=\"checkbox\" name=\"uplver\"" . ($CURUSER['anon'] == 'yes' ? " checked=\"checked\" " : " ") . "value=\"yes\" />" . $lang_upload['checkbox_hide_uploader_note'] . "<input type=\"checkbox\" name=\"offerzone\" value=\"yes\" />" . $lang_upload['checkbox_offer_note'], 1);
			}
		}
		print("<tr><td class=\"toolbox\" align=\"center\" colspan=\"2\"><input id=\"qr\" type=\"submit\" class=\"btn\" value=\"" . $lang_upload['submit_upload'] . "\" /><input id=\"preDIv\" type=\"button\" class=\"btn\" onClick=\"preview_torrent();return false;\" value=\"预览\" /><input id=\"EditDIv\" type=\"button\" style=\"display:none;\" onClick=\"edit_torrent();return false;\" class=\"btn\" value=\"继续编辑\" /></td></tr></table></form>");
		print("<br /><button id='sceditorsave' class=\"button button-3d button-primary button-rounded\">保存编辑器内容及草稿</button>&nbsp;&nbsp;<button id='sceditorclear' class=\"button button-3d button-primary button-rounded\">清除编辑器内容及草稿</button>");
		?>
		<script type="text/javascript">
			(function () {
				var demo = document.getElementById('sceditor');
				function supportsLocalStorage() {
					return typeof (Storage) !== 'undefined';
				}
				if (!supportsLocalStorage()) {
					demo.value = 'Word天，你的浏览器竟然不支持HTML5？！';
				} else {
					try {
						setInterval(function () {
							localStorage.setItem('autosave', demo.value);
						}, 100);
					} catch (e) {
						if (e == QUOTA_EXCEEDED_ERR) {
							alert('卧槽，超出最大限制了！');
						}
					}
					if (localStorage.getItem('autosave')) {
						demo.value = localStorage.getItem('autosave');
					}
					document.getElementById('sceditorclear').onclick = function () {
						$('#sceditor').sceditor('instance').val("");
						demo.value = '';
						localStorage.removeItem('autosave');
						localStorage.clear();
					};
					document.getElementById('sceditorsave').onclick = function () {
						var demo = document.getElementById('sceditor');
						localStorage.setItem('autosave', demo.value);
					};
				}
			})();
		</script>
		<?php
		stdfoot();
