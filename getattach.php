<?php
/*
 * 用来免论坛虚拟币下载附件的页面
 * 主要针对CMCT外站Discuz X2版本
 */
define('ADMIN_USERNAME', 'admin');  // Admin Username
define('ADMIN_PASSWORD', 'shenzhan');   // Admin Password
if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW']) ||
		$_SERVER['PHP_AUTH_USER'] != ADMIN_USERNAME || $_SERVER['PHP_AUTH_PW'] != ADMIN_PASSWORD) {
	header("WWW-Authenticate: Basic realm=\"CMCT GetAttachLink\"");
	header("HTTP/1.0 401 Unauthorized");

	echo <<<EOB
				<html><body>
				<h1>Rejected!</h1>
				<big>Wrong Username or Password!</big>
				</body></html>
EOB;
	exit;
}
?>
<div align="center" style="padding-top: 100px">
	<form method="post">
		请输入要下载的附件链接：<input name="attachlink" type="text" style="width: 600px" />
		<input name="submit" type="submit" value="查询" />
	</form>
</div>
<?php

function getKeyValue($url) {
	$result = array();
	$mr = preg_match_all('/(\?|&)(.+?)=([^&?]*)/i', $url, $matchs);
	if ($mr !== FALSE) {
		for ($i = 0; $i < $mr; $i++) {
			$result[$matchs[2][$i]] = $matchs[3][$i];
		}
	}
	return $result;
}

if ($_POST['submit']) {
	$url = $_POST['attachlink'];
	$arr = getKeyValue($url);
	$host = parse_url($url);
	$aid = $arr['aid'] . "|74680613|1456921031|310196|" . $arr['tid'];
	$link = $host['scheme'] . '://' . $host['host'] . $host['path'] . '?mod=attachment&aid=' . base64_encode($aid);
	print("<div align=\"center\" style=\"padding-top: 100px\">");
	print("<textarea style=\"height:50px;width:65%;overflow:hidden\" onmouseover=\"this.select()\">$link</textarea>");
	print("</div>");
}