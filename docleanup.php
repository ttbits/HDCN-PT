<?php

ob_start();
header("Content-type:text/html;charset=utf-8");
require_once("include/bittorrent.php");
require ("imdb/imdb.class.php");
require ("imdb/douban.php");
dbconn();
if (get_user_class() < UC_SYSOP) {
	die('权限不足');
}
stdhead("更新Tracker状态");
echo "正在执行更新...请稍候<br />";
ob_flush();
flush();
if ($_GET['forceall']) {
	$forceall = 1;
} else {
	$forceall = 0;
	echo "你可以通过在URL中添加参数 <a href=\"docleanup.php?forceall=1\"><b><font color='#FF0000'>?forceall=1</font></b></a> 来强制性的进行全面更新<br />";
}
echo "</p>";
$tstart = getmicrotime();
require_once("include/cleanup.php");
print("<p>" . docleanup($forceall, TRUE) . "</p>");
$tend = getmicrotime();
$totaltime = ($tend - $tstart);
printf("耗时：%f 秒<br />", $totaltime);
echo "完成<br />";
stdfoot();
