<?php

require_once("include/bittorrent.php");
dbconn();
loggedinorreturn();
parked();
if ($CURUSER['class'] < UC_MODERATOR) {
	stderr("抱歉...", "您的等级太低");
	exit;
}

stdhead("统计用户");

function begin_table_totaluser($fullwidth = false, $padding = 5) {
	$width = "";
	if ($fullwidth)
		$width .= " width=50%";
	return("<table class='main" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'><tr align=\"center\"></tr>");
}

function end_table_totaluser() {
	return("<tr align=\"center\"></tr></table>");
}

function begin_frame_totaluser($caption = "", $center = false, $padding = 5, $width = "100%", $caption_center = "left") {
	$tdextra = "";
	if ($center)
		$tdextra .= " align='center'";
	return(($caption ? "<h2 align='" . $caption_center . "'>" . $caption . "</h2>" : "") . "<table width='" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'>" . "<tr><td class='text' $tdextra>");
}

function end_frame_totaluser() {
	return("</td></tr></table>");
}

function bjtable_totaluser($res, $frame_caption) {
	global $buyvpn, $authoff;
	$htmlout = '';
	$htmlout .= begin_frame_totaluser($frame_caption, true);
	$htmlout .= begin_table_totaluser();
	$htmlout .="<tr><td class='colhead'>ID</td><td class='colhead' align='left'>用户名</td>" . ($buyvpn == 'yes' ? "<td class='colhead' align='center'><a href='?vpn=1'>VPN帐号</a></td>" : "") . ($authoff == 'yes' ? "<td class='colhead' align='center'><a href='?authoff=1'>绑定手机</a></td>" : "") . "<td class='colhead' align='right'>等级</td><td class='colhead' align='right'>邀请人</td><td class='colhead' align='right'>地理位置</td><td class='colhead' align='right'>注册时间</td><td class='colhead' align='right'>注册邮箱</td><td class='colhead' align='right'>IP</td><td class='colhead' align='right'>上传量</td><td class='colhead' align='right'>下载量</td><td class='colhead' align='right'><a href='?bonus=1'>魔力值</a></td></tr>";
	while ($a = mysql_fetch_assoc($res)) {
		list($loc_pub) = get_ip_location($a['ip']);
		if (school_ip_location($a['ip']) == '') {
			$school = "未知";
		} else {
			$school = school_ip_location($a['ip']);
		}
		$vpnaccount = mysql_fetch_array(sql_query("SELECT username FROM userinfo WHERE firstname = " . sqlesc($a['username']) . " AND email = " . sqlesc($a['email']) . ""));
		$htmlout .="<tr class='torrent_table'><td>$a[id]</td>" . //ID
				"<td align='left'>" . get_username($a['id'], FALSE, TRUE, TRUE, TRUE) . "</td>" . //用户名
				($buyvpn == 'yes' ? "<td align='center'><b>" . ($vpnaccount['username'] ? $vpnaccount['username'] : "无") . "</b></td>" : "") . //VPN帐号
				($authoff == 'yes' ? "<td align='center'><b>" . ($a['secken'] == 'yes' ? "<font style='color:green'>是</font>" : "<font style='color:red'>否</font>") . "</b></td>" : "") . //洋葱API绑定
				"<td align='right'>" . get_user_class_name_zh($a['class'], FALSE, TRUE, TRUE) . "</td>" . //等级
				"<td align='right'>" . ($a['invited_by'] != 0 ? get_username($a['invited_by'], FALSE, TRUE, TRUE, TRUE) : "无") . "</td>" . //邀请人
				"<td align='right'>" . (!ip2long($a['ip']) ? "$school" : "$loc_pub") . "</td>" . //地理位置
				"<td align='right'>" . $a['added'] . "</td>" . //注册时间
				"<td align='right'>" . $a['email'] . "</td>" . //注册邮箱
				"<td align='right'>" . $a['ip'] . "</td>" . //IP
				"<td align='right'>" . mksize($a['uploaded']) . "</td>" . //上传量
				"<td align='right'>" . mksize($a['downloaded']) . "</td>" . //下载量
				"<td align='right'>" . round($a['seedbonus']) . "</td>" . //魔力值
				"</tr>";
	}
	$htmlout .= end_table_totaluser();
	$htmlout .= end_frame_totaluser();
	return $htmlout;
}

$HTMLOUT .="<h1 align='center'><a href='totaluser.php'>统计用户</a></h1>";
if ($_GET['vpn'] == 1) {
	$count = get_row_count("userinfo");
	list ($pagertop, $pagerbottom, $limit) = pager(25, $count, "?vpn=1&");
	$res = sql_query("SELECT * FROM users WHERE email IN (SELECT email FROM userinfo) AND username IN (SELECT firstname FROM userinfo) ORDER BY id DESC $limit") or sqlerr(__FILE__, __LINE__); //降序排列
} elseif ($_GET['authoff'] == 1) {
	$count = get_row_count("users", "WHERE secken = 'yes'");
	list ($pagertop, $pagerbottom, $limit) = pager(25, $count, "?authoff=1&");
	$res = sql_query("SELECT * FROM users WHERE secken = 'yes' ORDER BY id DESC $limit") or sqlerr(__FILE__, __LINE__); //降序排列
} elseif ($_GET['bonus'] == 1) {
	$count = get_row_count("users");
	list ($pagertop, $pagerbottom, $limit) = pager(25, $count, "?bonus=1&");
	$res = sql_query("SELECT * FROM users ORDER BY seedbonus DESC $limit") or sqlerr(__FILE__, __LINE__); //降序排列
} else {
	$count = get_row_count("users");
	list($pagertop, $pagerbottom, $limit) = pager(25, $count, "?");
	$res = sql_query("SELECT * FROM users ORDER BY id DESC $limit") or sqlerr(__FILE__, __LINE__); //降序排列
}
$HTMLOUT .= bjtable_totaluser($res, "统计用户", "Users");
$HTMLOUT .="<br /><br />";
if ($count) {
	print($pagertop);
	print $HTMLOUT;
	print($pagerbottom);
} else {
	print $HTMLOUT;
}
stdfoot();
