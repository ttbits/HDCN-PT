<?php
require_once("include/bittorrent.php");
require('config/allconfig.php');
dbconn();
loggedinorreturn();
parked();
secrity();
if (get_user_class() < $vpnclass) {
	stderr("权限不足", "需要 " . get_user_class_name_zh($vpnclass) . " 等级");
} elseif ($buyvpn != 'yes') {
	stderr("错误", "当前VPN购买服务未启用");
}
stdhead("VPN管理");
/*
 * 购买按钮
 * 创建VPN帐号并设置密码
 * 重置密码、修改密码
 * 可查询剩余时间和流量
 */
if ($_POST['create']) {//创建VPN帐号
	$username = sqlesc($_POST['vpnaccount']); //帐号
	$password = sqlesc($_POST['vpnpasswd1']); //密码
	$creationdate = sqlesc(date("Y-m-d H:i:s", time())); //创建时间
	$creationby = sqlesc(PROJECTNAME); //创建者
	$firstname = sqlesc($CURUSER['username']); //PT用户名
	$email = sqlesc($CURUSER['email']); //PT邮箱
	$attribute = sqlesc('Cleartext-Password'); //明文密码
	$op = sqlesc(':='); //Radius运算符
	if (mysql_num_rows(sql_query("SELECT id FROM userinfo WHERE firstname = $firstname AND email = $email")) == 0) {
		if (preg_match('/^[a-zA-Z0-9]{4,18}$/', $_POST['vpnaccount'])) {//判断帐号是否符合要求
			if ($_POST['vpnpasswd1'] == $_POST['vpnpasswd2']) {//判断两次密码是否一致
				if (preg_match('/^[a-zA-Z0-9]{6,32}$/', $_POST['vpnpasswd1'])) {//判断密码是否符合要求
					$checkaccount1 = mysql_num_rows(sql_query("SELECT id FROM userinfo WHERE username = $username"));
					$checkaccount3 = mysql_num_rows(sql_query("SELECT id FROM radcheck WHERE username = $username"));
					$checkaccount4 = mysql_num_rows(sql_query("SELECT id FROM radusergroup WHERE username = $username"));
					if ($checkaccount1 + $checkaccount2 + $checkaccount3 + $checkaccount4 == 0) {
						sql_query("INSERT userinfo (username, creationdate, creationby, firstname, email) VALUES ($username, $creationdate, $creationby, $firstname, $email)") or sqlerr(__FILE__, __LINE__); //帐号表
						sql_query("INSERT radcheck (username, attribute, op, value) VALUES ($username, $attribute, $op, $password)") or sqlerr(__FILE__, __LINE__); //密码表
						sql_query("INSERT radusergroup (username, groupname) VALUES ($username, 'user')") or sqlerr(__FILE__, __LINE__); //用户组表
						echo "<script>alert('帐号创建成功');location.href='vpn.php';</script>";
					} else {
						echo "<script>alert('该帐号已存在');location.href='?setup=1';</script>";
					}
				} else {
					echo "<script>alert('密码必须是由最少6个字符，最多32个字符的数字或字母组成');location.href='?setup=1';</script>";
				}
			} else {
				echo "<script>alert('两次输入的密码不一致');location.href='?setup=1';</script>";
			}
		} else {
			echo "<script>alert('帐号必须是由最少4个字符，最多18个字符的数字或字母组成');location.href='?setup=1';</script>";
		}
	} else {
		stdmsg("错误", "你已经拥有VPN帐号了！");
		stdfoot();
		exit;
	}
} elseif ($_POST['edit']) {//修改密码
	$res = sql_query("SELECT radcheck.value, radcheck.id FROM radcheck INNER JOIN userinfo ON radcheck.id = userinfo.id WHERE userinfo.firstname = " . sqlesc($CURUSER['username']) . " AND userinfo.email = " . sqlesc($CURUSER['email']) . "");
	if (mysql_num_rows($res) > 0) {
		$oldpasswd = mysql_fetch_array($res);
		if ($_POST['vpnpasswd'] == $oldpasswd['value']) {
			if ($_POST['vpnpasswd'] != $_POST['vpnpasswd1']) {//新密码不能和旧密码一样
				if ($_POST['vpnpasswd1'] == $_POST['vpnpasswd2']) {//判断两次密码是否一致
					if (preg_match('/^[a-zA-Z0-9]{6,32}$/', $_POST['vpnpasswd1'])) {//判断密码是否符合要求
						sql_query("UPDATE radcheck SET value = " . sqlesc($_POST['vpnpasswd1']) . " WHERE username = " . sqlesc($CURUSER['username']) . " AND value = " . sqlesc($oldpasswd['value']) . " AND id = " . sqlesc($oldpasswd['id']) . "") or sqlerr(__FILE__, __LINE__);
						echo "<script>alert('密码修改成功');location.href='vpn.php';</script>";
					} else {
						echo "<script>alert('密码必须是由最少6个字符，最多32个字符的数字或字母组成');location.href='?setup=2';</script>";
					}
				} else {
					echo "<script>alert('两次输入的新密码不一致');location.href='?setup=2';</script>";
				}
			} else {
				echo "<script>alert('新密码不可和旧密码一样');location.href='?setup=2';</script>";
			}
		} else {
			echo "<script>alert('旧密码不正确');location.href='?setup=2';</script>";
		}
	} else {
		echo "<script>alert('你还没有VPN帐号');location.href='?setup=2';</script>";
	}
} elseif ($_POST['reset']) {//重置密码
	if ($_POST['mi'] == 1) {
		$res = sql_query("SELECT username FROM userinfo WHERE firstname = " . sqlesc($CURUSER['username']) . " AND email = " . sqlesc($CURUSER['email']) . "");
		if (mysql_num_rows($res) > 0) {
			foreach (generate() as $arr) {//随机生成标签码
				sql_query("UPDATE radcheck SET value = " . sqlesc($arr) . " WHERE username = " . sqlesc($CURUSER['username']) . "") or sqlerr(__FILE__, __LINE__);
				sendMessage(0, $CURUSER['id'], "VPN密码重置成功", "VPN密码重置为：${arr}，[url=vpn.php?setup=2]点击这里修改密码[/url]");
				echo "<script>alert('密码重置成功');location.href='vpn.php';</script>";
			}
		} else {
			echo "<script>alert('你还没有VPN帐号');location.href='vpn.php';</script>";
		}
	}
} elseif ($_POST['plan']) {//流量加油包
	$res = sql_query("SELECT username FROM userinfo WHERE firstname = " . sqlesc($CURUSER['username']) . " AND email = " . sqlesc($CURUSER['email']) . "");
	if (mysql_num_rows($res) > 0) {
		$vpnname = sqlesc($_POST['username']);
		if ($CURUSER['seedbonus'] >= $_POST['bonus'] || get_user_class() >= $staffmem_class) {
			if ($_POST['con'] == 1) {
				sql_query("UPDATE radusergroup SET groupname = 'planA' WHERE username = $vpnname");
			} elseif ($_POST['con'] == 2) {
				sql_query("UPDATE radusergroup SET groupname = 'planB' WHERE username = $vpnname");
			} elseif ($_POST['con'] == 3) {
				sql_query("UPDATE radusergroup SET groupname = 'planC' WHERE username = $vpnname");
			}
			if (get_user_class() < $staffmem_class) {
				sql_query("UPDATE users SET seedbonus = seedbonus - " . $_POST['bonus'] . " WHERE id = " . $CURUSER['id']);
				writeBonusComment($CURUSER['id'], "购买套餐 " . $_POST['planname'] . " 花费了魔力值 " . $_POST['bonus']);
			}
			echo "<script>alert('成功购买套餐 " . $_POST['planname'] . "');location.href='vpn.php';</script>";
		}
	} else {
		echo "<script>alert('你还没有VPN帐号');location.href='vpn.php';</script>";
	}
} else {
	if ($_POST['buy'] == 1) {
		if (get_user_class() < $staffmem_class) {
			if ($CURUSER['seedbonus'] >= $vpn && get_user_class() >= $vpnclass) {
				sql_query("UPDATE users SET seedbonus = seedbonus - $vpn WHERE id = " . $CURUSER['id'] . "");
				writeBonusComment($CURUSER['id'], "因购买VPN帐号花费 ${vpn} 个魔力值");
			} else {
				stdmsg("错误", "魔力值或等级不足");
				stdfoot();
				exit;
			}
		}
	}
}
$row = mysql_fetch_array(sql_query("SELECT username FROM userinfo WHERE firstname = " . sqlesc($CURUSER['username']) . " AND email = " . sqlesc($CURUSER['email']) . ""));
$groupname = mysql_fetch_array(sql_query("SELECT groupname FROM radusergroup WHERE username = '" . $row['username'] . "'")); //取出VPN用户所在的用户组
$totaltraffic = mysql_fetch_array(sql_query("SELECT value FROM radgroupreply WHERE groupname = '" . $groupname['groupname'] . "' AND attribute = 'Max-Monthly-Traffic'")); //全部可用流量
$defaulttraffic = get_single_value("radgroupreply", "value", "WHERE groupname = 'user' AND attribute = 'Max-Monthly-Traffic'"); //默认可用流量
?>
<script language="javascript">
	window.onload = function () {
		document.getElementById("vpnaccount").focus();
	};
</script>
<div>
	<h1 style="width: 95%"><a href="vpn.php">VPN使用声明</a></h1>
	<table border="1" cellpadding="10" cellspacing="0" width="60%">
		<tr>
			<td>
				<ul>
					<li>本站提供的VPN服务是PPTP的方式，请自行查找使用方法</li>
					<li>本站提供的VPN服务<b><font style="color:red">禁止(包括但不限于)</font></b>用于BT、PT、大文件下载、测速、看网络视频等，仅作为翻墙访问GMail，FaceBook，Google搜索和查阅学术资料等</li>
					<li>购买VPN使用权限将消耗你 <?= $vpn ?> 个魔力值，期限为购买日起的30天内，流量为<?= mksize($defaulttraffic) ?>上行，同一时间只能登录一个终端。</li>
					<li>请不要公开和共享你的VPN帐号和密码，该行为将导致你的VPN和<?= $SITENAME ?>帐号双双被BAN。</li>
					<li>套餐在VPN帐号存在时间内只可以购买一次，跟开通帐号的基础流量叠加。</li>
					<li>最后，请低调使用！</li>
					<li>PS：目前有两个节点可供使用，服务器地址分别为：sg1.ahdbits.com 和 sg2.ahdbits.com</li>
				</ul>
			</td>
		</tr>
	</table><br />
	<?php
	$res = sql_query("SELECT username, creationdate FROM userinfo WHERE firstname = " . sqlesc($CURUSER['username']) . " AND email = " . sqlesc($CURUSER['email']) . "");
	if (mysql_num_rows($res) > 0) {
		$row = mysql_fetch_array($res);
		$ks = date("Y-m-d H:i:s", strtotime($row['creationdate'] . " + 30 days")); //使用时间
		$time = mkprettytime(round((strtotime($ks) - strtotime(date("Y-m-d H:i:s"))))); //剩余使用时间
		$usetraffic = mysql_fetch_array(sql_query("SELECT SUM(acctoutputoctets) AS aout FROM radacct WHERE username = '" . $CURUSER['username'] . "'")); //已经使用流量
		$group = mysql_fetch_array(sql_query("SELECT value FROM radgroupreply WHERE groupname = '" . $groupname['groupname'] . "-D' AND attribute = 'Plan'"));
		if ($groupname['groupname'] == 'user') {
			$plan = "无";
		} elseif ($groupname['groupname'] == 'planA') {
			$plan = $group['value'];
		} elseif ($groupname['groupname'] == 'planB') {
			$plan = $group['value'];
		} elseif ($groupname['groupname'] == 'planC') {
			$plan = $group['value'];
		}
		print("<table border = \"1\" cellpadding = \"5\" cellspacing = \"0\" width = \"20%\">");
		print("<tr><td>帐号</td><td>剩余时间</td><td>剩余流量</td><td>流量加油包</td></tr>");
		print("<tr><td>" . $row['username'] . "</td><td>${time}</td><td>" . mksize($totaltraffic['value'] - (0 + $usetraffic['aout'])) . "</td><td>$plan</td></tr>");
		print("</table><br />");
	}
	if ($_GET['setup'] == 1) {
		if (mysql_num_rows(sql_query("SELECT username FROM userinfo WHERE firstname = " . sqlesc($CURUSER['username']) . " AND email = " . sqlesc($CURUSER['email']) . "")) == 0) {
			?>
			<form  method="post">
				<table border="1" cellpadding="10" cellspacing="0" width="300px">
					<tr><td align="right">帐号：</td><td align="left"><input id="vpnaccount" name="vpnaccount" type="text" style="width: 150px"  /><br />(由4 ~ 18位的数字或字母组成)</td></tr>
					<tr><td align="right">密码：</td><td align="left"><input name="vpnpasswd1" type="password" style="width: 150px" /><br />(由6 ~ 32位的数字或字母组成)</td></tr>
					<tr><td align="right">再次输入密码：</td><td align="left"><input name="vpnpasswd2" type="password" style="width: 150px" /></td></tr>
				</table><br />
				<input name="create" type="submit" value="提交" />
			</form><br />
			<?php
		} else {
			stdmsg("错误", "已有帐号或魔力值不足");
			stdfoot();
			exit;
		}
	} elseif ($_GET['setup'] == 2) {
		if (mysql_num_rows(sql_query("SELECT username FROM userinfo WHERE firstname = " . sqlesc($CURUSER['username']) . " AND email = " . sqlesc($CURUSER['email']) . "")) > 0) {
			?>
			<form  method="post">
				<table border="1" cellpadding="10" cellspacing="0" width="300px">
					<tr><td align="right">旧密码：</td><td align="left"><input name="vpnpasswd" type="password" style="width: 150px" /></td></tr>
					<tr><td align="right">新密码：</td><td align="left"><input name="vpnpasswd1" type="password" style="width: 150px" /></td></tr>
					<tr><td align="right">再次输入新密码：</td><td align="left"><input name="vpnpasswd2" type="password" style="width: 150px" /></td></tr>
				</table><br />
				<input name="edit" type="submit" value="提交" />
			</form><br />
			<?php
		} else {
			stdmsg("错误", "首先，你得有个VPN帐号");
			stdfoot();
			exit;
		}
	} elseif ($_GET['setup'] == 3) {
		if (get_user_class() >= UC_MODERATOR) {
			?>
			<form method="post"><input name="mi" type="hidden" value="1" /><input name="reset" type="submit" value="重置密码" style="width: 120px;height: 40px" /></form><br />
				<?php
			} else {
				stdmsg("权限不足", "权限不足");
				stdfoot();
				exit;
			}
		} else {
			if (get_user_class() < $staffmem_class ? mysql_num_rows(sql_query("SELECT username FROM userinfo WHERE firstname = " . sqlesc($CURUSER['username']) . " AND email = " . sqlesc($CURUSER['email']) . "")) == 0 && $CURUSER['seedbonus'] >= $vpn : mysql_num_rows(sql_query("SELECT username FROM userinfo WHERE firstname = " . sqlesc($CURUSER['username']) . " AND email = " . sqlesc($CURUSER['email']) . "")) == 0) {
				print("<form method=\"post\" action=\"?setup=1\" onsubmit=\"if(confirm('你确定要购买30天的VPN使用权限么？你将会失去 ${vpn} 个魔力值')){return true;}else{return false;}\"><input name=\"buy\" type=\"hidden\" value=\"1\" /><input type=\"submit\" value=\"创建VPN帐号\" style=\"width: 150px;height: 40px\" /></form><br />");
			} else {
				print("<form method=\"post\" action=\"?setup=1\"><input type=\"submit\" value=\"已有帐号或魔力值不足\" style=\"width: 150px;height: 40px\" disabled /></form><br />");
				$planA = get_single_value("radgroupreply", "value", "WHERE groupname = 'planA-D' AND attribute = 'Plan'");
				$trafficA = get_single_value("radgroupreply", "value", "WHERE groupname = 'planA' AND attribute = 'Max-Monthly-Traffic'");
				$bonusA = get_single_value("radgroupreply", "value", "WHERE groupname = 'planA-D' AND attribute = 'Bonus'");
				$planB = get_single_value("radgroupreply", "value", "WHERE groupname = 'planB-D' AND attribute = 'Plan'");
				$trafficB = get_single_value("radgroupreply", "value", "WHERE groupname = 'planB' AND attribute = 'Max-Monthly-Traffic'");
				$bonusB = get_single_value("radgroupreply", "value", "WHERE groupname = 'planB-D' AND attribute = 'Bonus'");
				$planC = get_single_value("radgroupreply", "value", "WHERE groupname = 'planC-D' AND attribute = 'Plan'");
				$trafficC = get_single_value("radgroupreply", "value", "WHERE groupname = 'planC' AND attribute = 'Max-Monthly-Traffic'");
				$bonusC = get_single_value("radgroupreply", "value", "WHERE groupname = 'planC-D' AND attribute = 'Bonus'");
				$vpnname = get_single_value("userinfo", "username", "WHERE firstname = " . sqlesc($CURUSER['username']) . " AND email = " . sqlesc($CURUSER['email']) . "");
				if (!empty($planA) || !empty($planB) || !empty($planC)) {
					?>
				<hr><h1>流量加油包</h1>
				<table border="1" cellpadding="10" cellspacing="0" width="600px">
					<tr><td align="center">套餐计划名称</td><td align="center">套餐流量额度</td><td align="center">套餐流量价格</td><td align="center">购买</td></tr>
					<?php
					if ($groupname['groupname'] == 'user' || get_user_class() >= $staffmem_class) {
						if (!empty($planA)) {
							?>
							<form  method="post"><tr><td align="center"><?= $planA ?></td><td align="center"><?= mksize($trafficA - $defaulttraffic) ?></td><td align="center"><?= $bonusA ?></td><td align="center"><input type="hidden" name="username" value="<?= $vpnname ?>" /><input type="hidden" name="planname" value="<?= $planA ?>" /><input type="hidden" name="bonus" value="<?= $bonusA ?>" /><input type="hidden" name="con" value="1" /><?= ($CURUSER['seedbonus'] >= $bonusA || get_user_class() >= $staffmem_class ? "<input type=\"submit\" name=\"plan\" value=\"购买\" />" : "<input type=\"submit\" name=\"plan\" value=\"魔力值不足\" disabled />") ?></td></tr></form>
						<?php } if (!empty($planB)) { ?>
							<form  method="post"><tr><td align="center"><?= $planB ?></td><td align="center"><?= mksize($trafficB - $defaulttraffic) ?></td><td align="center"><?= $bonusB ?></td><td align="center"><input type="hidden" name="username" value="<?= $vpnname ?>" /><input type="hidden" name="planname" value="<?= $planB ?>" /><input type="hidden" name="bonus" value="<?= $bonusB ?>" /><input type="hidden" name="con" value="2" /><?= ($CURUSER['seedbonus'] >= $bonusB || get_user_class() >= $staffmem_class ? "<input type=\"submit\" name=\"plan\" value=\"购买\" />" : "<input type=\"submit\" name=\"plan\" value=\"魔力值不足\" disabled />") ?></td></tr></form>
						<?php } if (!empty($planC)) { ?>
							<form  method="post"><tr><td align="center"><?= $planC ?></td><td align="center"><?= mksize($trafficC - $defaulttraffic) ?></td><td align="center"><?= $bonusC ?></td><td align="center"><input type="hidden" name="username" value="<?= $vpnname ?>" /><input type="hidden" name="planname" value="<?= $planC ?>" /><input type="hidden" name="bonus" value="<?= $bonusC ?>" /><input type="hidden" name="con" value="3" /><?= ($CURUSER['seedbonus'] >= $bonusC || get_user_class() >= $staffmem_class ? "<input type=\"submit\" name=\"plan\" value=\"购买\" />" : "<input type=\"submit\" name=\"plan\" value=\"魔力值不足\" disabled />") ?></td></tr></form>
							<?php
						}
					} else {
						if (!empty($planA)) {
							?><tr><td align="center"><?= $planA ?></td><td align="center"><?= mksize($trafficA) ?></td><td align="center"><?= $bonusA ?></td><td align="center"><input type="hidden" name="username" value="<?= $vpnname ?>" /><input type="hidden" name="planname" value="<?= $planA ?>" /><input type="hidden" name="bonus" value="<?= $bonusA ?>" /><input type="submit" name="plan" value="已有套餐" disabled /></td></tr>
												<?php } if (!empty($planB)) { ?>
							<tr><td align="center"><?= $planB ?></td><td align="center"><?= mksize($trafficB) ?></td><td align="center"><?= $bonusB ?></td><td align="center"><input type="hidden" name="username" value="<?= $vpnname ?>" /><input type="hidden" name="planname" value="<?= $planB ?>" /><input type="hidden" name="bonus" value="<?= $bonusB ?>" /><input type="submit" name="plan" value="已有套餐" disabled /></td></tr>
						<?php } if (!empty($planC)) { ?>
							<tr><td align="center"><?= $planC ?></td><td align="center"><?= mksize($trafficC) ?></td><td align="center"><?= $bonusC ?></td><td align="center"><input type="hidden" name="username" value="<?= $vpnname ?>" /><input type="hidden" name="planname" value="<?= $planC ?>" /><input type="hidden" name="bonus" value="<?= $bonusC ?>" /><input type="submit" name="plan" value="已有套餐" disabled /></td></tr>
							<?php
						}
					}
					?>
				</table>
				<?php
			}
		}
	}
	?>
	<br /><a href="?setup=3">重置密码</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="?setup=2">修改密码</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="vpn.php">返回</a><?= (get_user_class() == UC_STAFFLEADER ? "&nbsp;&nbsp;|&nbsp;&nbsp;<a href=\"vpnmanager.php\">VPN管理</a>" : "") ?>
</div>
<?php
stdfoot();
