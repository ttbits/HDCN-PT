<?php
require_once("include/bittorrent.php");
dbconn();
loggedinorreturn();
if ($CURUSER['class'] < UC_MODERATOR) {
	stderr("抱歉...", "您的等级太低");
	exit;
}
$class = 0 + $_POST["class"];
if ($class)
	int_check($class, true);
$or = $_POST["or"];
stdhead("发放限时邀请");
?>
<div>
	<p><table border=0 class=main cellspacing=0 cellpadding=0><tr>
		<font size=3><b>发放限时邀请</b></font>
		</tr></table></p>
<table border=1 cellspacing=0 cellpadding=5>
	<form method=POST>
		<?php
		if (get_user_class() == UC_MODERATOR && $CURUSER["class"] > UC_POWER_USER)
			printf("<input type=hidden name=class value=$CURUSER[class]\n");
		else {
			print("<tr><td class=rowhead>用户等级</td><td colspan=2 align=left><select name=or><option value='>='>>=<option value='<='><=<option value='='>=<option value='>'>><option value='<'><</select> <select name=class>\n");
			if (get_user_class() >= UC_MODERATOR)
				$maxclass = UC_UPLOADER;
			for ($i = 1; $i <= $maxclass; ++$i)
				print("<option value=$i" . ($CURUSER["class"] == $i ? " selected" : "") . ">$prefix" . get_user_class_name_zh($i, false, true, true) . "\n");
			print("</select></td></tr>\n");
		}
		?>
		<tr><td class = rowhead>发放数量</td><td><input type = text name = num size = 20 onkeyup = "this.value = this.value.replace(/\D/g, '');"></td></tr>
		<tr><td class = rowhead>到期时间</td><td><input type = text name = date size = 20 class = "Wdate" onfocus = "WdatePicker({isShowWeek: 'true', minDate: '%y-%M-{%d+1}'})"></td></td></tr>
		<tr><td colspan = 2 align = center><?php echo "默认发放 <b>1</b> 个 <b>7</b> 天时效的限时邀请" ?></td></tr>
		<tr><td colspan=2 align=center><input type=submit name=submit value="发放" class=btn></td></tr>
	</form>
</table>
</div>
<?php
if ($_POST['num'] == '' || $_POST['num'] == 0) {
	$gennum = '1';
} else {
	$gennum = $_POST['num'];
}
if ($_POST['date'] == '' || $_POST['date'] == 0) {
	$gendate = date("Y-m-d", time() + 86400 * 7);
} else {
	$gendate = $_POST['date'];
}
if ($_POST['submit']) {
	$res = sql_query("SELECT id, invites FROM users WHERE class $or " . mysql_real_escape_string($class)) or sqlerr(__FILE__, __LINE__);
	while ($arr = mysql_fetch_assoc($res)) {
		if ($arr['invites'] >= 0) {
			sql_query("INSERT INTO limitinvite (userid, invites, limited) VALUE ('" . $arr['id'] . "', '$gennum', '$gendate')") or sqlerr(__FILE__, __LINE__);
			sql_query("UPDATE users SET invites = invites + $gennum WHERE invites >= 0 AND enabled = 'yes' AND id = " . $arr['id']) or sqlerr(__FILE__, __LINE__);
		}
	}
	echo "<script>alert('发放成功！');location.href='invitelimi.php';</script>";
	write_log("邀请：" . $CURUSER['username'] . " 给 " . get_user_class_name_zh($_POST['class']) . " 等级及以上用户发放了 $gennum 个限时邀请，过期时间为 $gendate 。");
}
stdfoot();
