<?php

require_once("include/bittorrent.php");
dbconn();
header("Content-Type: text/html; charset=utf-8");
$id = 0 + $_GET["id"];
if (!$id) {
	stderr("错误", "不存在的ID");
}

$row = mysql_fetch_array(sql_query("SELECT passhash, secret, editsecret, status FROM users WHERE id = $id")) or sqlerr(__FILE__, __LINE__);
if ($row["status"] != "pending") {
	header("Refresh: 0; url=ok.php?type=confirmed");
	exit();
}

$confirm_md5 = $_GET["secret"];
$confirm_sec = hash_pad($row["secret"]);
if ($confirm_md5 != md5($confirm_sec)) {
	stderr("错误", "不存在的验证码");
}

sql_query("UPDATE users SET status = 'confirmed', editsecret = '' WHERE id = $id AND status = 'pending'") or sqlerr(__FILE__, __LINE__);
if (!mysql_affected_rows()) {
	stderr("错误", "验证失败，请联系管理员");
}

if ($securelogin == "yes") {
	$securelogin_indentity_cookie = true;
	$passh = md5($row["passhash"] . $_SERVER["REMOTE_ADDR"]);
} else {
	$securelogin_indentity_cookie = false;
	$passh = md5($row["passhash"]);
}
logincookie($row["id"], $passh, 1, 0x7fffffff, $securelogin_indentity_cookie);
//sessioncookie($row["id"], $passh,false);

header("Refresh: 0; url=ok.php?type=confirm");
