<?php
require "include/bittorrent.php";
dbconn();
loggedinorreturn();
require_once(get_langfile_path());
if (get_user_class() < UC_MODERATOR)
	stderr("错误", "权限不足");
stdhead("搜索用户", false);
?>
<table class=main width=750 border=0 cellspacing=0 cellpadding=0>
	<tr>
		<td class=embeddeds>
			<div align=center>
				<h1><?php echo $lang_searchuser['head_search'] ?></h1>
				<form method=post action=searchuser.php>
					<?php if ($_GET["returnto"] || $_SERVER["HTTP_REFERER"]) { ?>
						<input type=hidden name=returnto value="<?php echo htmlspecialchars($_GET["returnto"]) ? htmlspecialchars($_GET["returnto"]) : htmlspecialchars($_SERVER["HTTP_REFERER"]) ?>">
					<?php } ?>
					<table cellspacing=0 cellpadding=5>
						<?php
						if (isset($_POST["sear"]) && !empty($_POST["sear"])) {
							$username = explode(";", $_POST["username"]);
							$clases = $_POST["clases"];
							for ($i = 0; $username[$i]; $i++) {
								$res = sql_query("SELECT id, username, class FROM users WHERE username LIKE '%" . mysql_real_escape_string($username[$i]) . "%'");
								while ($a = mysql_fetch_assoc($res)) {
									$count++;
									?>
									<tr><td colspan=2 class="text" width="300" align="left"><b><a class="<?= get_user_class_name($a['class']) ?>_Name" href=userdetails.php?id=<?= $a["id"] ?>><?= $a["username"] ?></a></b></td></tr>
									<?php
								}
							}
							for ($i = 0; $clases[$i]; $i++) {
								$res = sql_query("SELECT id, username, class FROM users WHERE class = '" . mysql_real_escape_string($clases[$i]) . "'");
								while ($a = mysql_fetch_assoc($res)) {
									$count++;
									?>
									<tr><td colspan=2 class="text" width="300" align="left"><b><a class="<?= get_user_class_name($a['class']) ?>_Name" href=userdetails.php?id=<?= $a["id"] ?>><?= $a["username"] ?></a></b></td></tr>
									<?php
								}
							}
							if ($count <= 0) {
								?>
								<tr><td colspan=2 class="text" width="300" align="left"><b><?php echo $lang_searchuser['text_noresult'] ?></b></td></tr>
							<?php } ?>
							<tr><td><p align=center><a href=searchuser.php><?php echo $lang_searchuser['submit_return'] ?></a></p></td></tr>
						<?php } else { ?>
							<tr><td class="rowhead" valign="top"><?php echo $lang_searchuser['text_username'] ?></td><td class="rowfollow"><input type=text name=username size=20><?php echo $lang_searchuser['text_notice'] ?></td></tr>
							<tr>
								<td class="rowhead" valign="top"><?php echo $lang_searchuser['text_usergroup'] ?></td><td class="rowfollow">
									<table style="border: 0" width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="1"></td>
											<td style="border: 0"><?php echo $lang_functions['text_peasant'] ?></td>

											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="2"></td>
											<td style="border: 0"><?php echo $lang_functions['text_user'] ?></td>

											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="3"></td>
											<td style="border: 0"><?php echo $lang_functions['text_power_user'] ?></td>

											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="4"></td>
											<td style="border: 0"><?php echo $lang_functions['text_elite_user'] ?></td>

											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="5"></td>
											<td style="border: 0"><?php echo $lang_functions['text_crazy_user'] ?></td>

											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="6"></td>
											<td style="border: 0"><?php echo $lang_functions['text_insane_user'] ?></td>
										</tr>
										<tr>
											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="7"></td>
											<td style="border: 0"><?php echo $lang_functions['text_veteran_user'] ?></td>

											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="8"></td>
											<td style="border: 0"><?php echo $lang_functions['text_extreme_user'] ?></td>

											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="9"></td>
											<td style="border: 0"><?php echo $lang_functions['text_ultimate_user'] ?></td>

											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="10"></td>
											<td style="border: 0"><?php echo $lang_functions['text_nexus_master'] ?></td>

											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="11"></td>
											<td style="border: 0"><?php echo $lang_functions['text_vip'] ?></td>

											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="12"></td>
											<td style="border: 0"><?php echo $lang_functions['text_retiree'] ?></td>
										</tr>
										<tr>
											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="13"></td>
											<td style="border: 0"><?php echo $lang_functions['text_downloader'] ?></td>

											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="14"></td>
											<td style="border: 0"><?php echo $lang_functions['text_forward'] ?></td>

											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="15"></td>
											<td style="border: 0"><?php echo $lang_functions['text_uploader'] ?></td>

											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="16"></td>
											<td style="border: 0"><?php echo $lang_functions['text_moderators'] ?></td>

											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="17"></td>
											<td style="border: 0"><?php echo $lang_functions['text_administrators'] ?></td>

											<td style="border: 0" width="20"><input type="checkbox" name="clases[]" value="18"></td>
											<td style="border: 0"><?php echo $lang_functions['text_staff_leader'] ?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr><td class="rowfollow" colspan=2 align=center>
									<input name="sear" type="hidden" value="1" />
									<input type=submit value="<?php echo $lang_searchuser['submit_search'] ?>" class=btn></td></tr>
						</table>
					<?php } ?>
				</form>
			</div>
		</td>
	</tr>
</table>
<table class=main width=750 border=0 cellspacing=0 cellpadding=0>
	<tr>
		<td class=embeddeds>
			<div align=center>
				<h1>PassKey搜索</h1>
				<form method=post action=searchuser.php>
					<?php if ($_GET["returnto"] || $_SERVER["HTTP_REFERER"]) { ?>
						<input type=hidden name=returnto value="<?php echo htmlspecialchars($_GET["returnto"]) ? htmlspecialchars($_GET["returnto"]) : htmlspecialchars($_SERVER["HTTP_REFERER"]) ?>">
					<?php } ?>
					<table cellspacing=0 cellpadding=5>
						<?php
						if (isset($_POST["passkeysear"]) && !empty($_POST["passkeysear"])) {
							$res = sql_query("SELECT id, username, class FROM users WHERE passkey = " . sqlesc(mysql_real_escape_string($_POST['passkey'])));
							while ($a = mysql_fetch_assoc($res)) {
								?>
								<tr><td colspan=2 class="text" width="300" align="left"><b><a class="<?= get_user_class_name($a['class']) ?>_Name" href=userdetails.php?id=<?= $a["id"] ?>><?= $a["username"] ?></a></b></td></tr>
								<?php
							}
							if (mysql_num_rows($res) <= 0) {
								?>
								<tr><td colspan=2 class="text" width="300" align="left"><b>PassKey不存在</b></td></tr>
							<?php } ?>
							<tr><td><p align=center><a href=searchuser.php><?php echo $lang_searchuser['submit_return'] ?></a></p></td></tr>
						<?php } else { ?>
							<tr><td class="rowhead" valign="top">PassKey</td><td class="rowfollow"><input type=text name=passkey style="width:250px"> 请输入PassKey</td></tr>
							<tr><td class="rowfollow" colspan=2 align=center>
									<input name="passkeysear" type="hidden" value="1" />
									<input type=submit value="<?php echo $lang_searchuser['submit_search'] ?>" class=btn></td></tr>
						</table>
					<?php } ?>
				</form>
			</div>
		</td>
	</tr>
</table>
<table class=main width=750 border=0 cellspacing=0 cellpadding=0>
	<tr>
		<td class=embeddeds>
			<div align=center>
				<h1>E-Mail搜索</h1>
				<form method=post action=searchuser.php>
					<?php if ($_GET["returnto"] || $_SERVER["HTTP_REFERER"]) { ?>
						<input type=hidden name=returnto value="<?php echo htmlspecialchars($_GET["returnto"]) ? htmlspecialchars($_GET["returnto"]) : htmlspecialchars($_SERVER["HTTP_REFERER"]) ?>">
					<?php } ?>
					<table cellspacing=0 cellpadding=5>
						<?php
						if (isset($_POST["emailsear"]) && !empty($_POST["emailsear"])) {
							$pattern = "/[A-Za-z0-9][A-Za-z0-9_.+\-]*@[A-Za-z0-9][A-Za-z0-9_+\-]*(\.[A-Za-z0-9][A-Za-z0-9_+\-]*)+/";
							if (!preg_match($pattern, $_POST["email"])) {
								print("<tr><td colspan=2 class=\"text\" width=\"300\" align=\"left\"><b>错误的E-Mail格式</b></td></tr>");
							} else {
								$res = sql_query("SELECT id, username, class FROM users WHERE email LIKE '%" . mysql_real_escape_string($_POST['email']) . "%'");
								while ($a = mysql_fetch_assoc($res)) {
									?>
									<tr><td colspan=2 class="text" width="300" align="left"><b><a class="<?= get_user_class_name($a['class']) ?>_Name" href=userdetails.php?id=<?= $a["id"] ?>><?= $a["username"] ?></a></b></td></tr>
									<?php
								}
								if (mysql_num_rows($res) <= 0) {
									?>
									<tr><td colspan=2 class="text" width="300" align="left"><b>E-Mail不存在</b></td></tr>
									<?php
								}
							}
							?>
							<tr><td><p align=center><a href=searchuser.php><?php echo $lang_searchuser['submit_return'] ?></a></p></td></tr>
						<?php } else { ?>
							<tr><td class="rowhead" valign="top">E-Mail地址</td><td class="rowfollow"><input type=text name=email style="width:250px"> 请输入E-Mail地址</td></tr>
							<tr><td class="rowfollow" colspan=2 align=center>
									<input name="emailsear" type="hidden" value="1" />
									<input type=submit value="<?php echo $lang_searchuser['submit_search'] ?>" class=btn></td></tr>
						</table>
					<?php } ?>
				</form>
			</div>
		</td>
	</tr>
</table>
<table class=main width=750 border=0 cellspacing=0 cellpadding=0>
	<tr>
		<td class=embeddeds>
			<div align=center>
				<h1>用户IP地址搜索</h1>
				<b>必须开启“网站设置”->“次要设定”->“记录用户IP地址”时才能使用</b>
				<form method=post action=searchuser.php>
					<?php if ($_GET["returnto"] || $_SERVER["HTTP_REFERER"]) { ?>
						<input type=hidden name=returnto value="<?php echo htmlspecialchars($_GET["returnto"]) ? htmlspecialchars($_GET["returnto"]) : htmlspecialchars($_SERVER["HTTP_REFERER"]) ?>">
					<?php } ?>
					<table cellspacing=0 cellpadding=5>
						<?php
						if (isset($_POST["ipsear"]) && !empty($_POST["ipsear"])) {
							$pattern = "/\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/";
							if (!preg_match($pattern, $_POST["ip"])) {
								print("<tr><td colspan=2 class=\"text\" width=\"300\" align=\"left\"><b>错误的IP格式</b></td></tr>");
							} else {
								$res = sql_query("SELECT DISTINCT(userid) FROM iplog WHERE ip LIKE '%" . mysql_real_escape_string($_POST['ip']) . "%' GROUP BY userid");
								while ($a = mysql_fetch_assoc($res)) {
									?>
									<tr><td colspan=2 class="text" width="300" align="left"><b><?= get_username($a['userid']) ?></b></td></tr>
									<?php
								}
								if (mysql_num_rows($res) <= 0) {
									?>
									<tr><td colspan=2 class="text" width="300" align="left"><b>用户IP地址不存在</b></td></tr>
									<?php
								}
							}
							?>
							<tr><td><p align=center><a href=searchuser.php><?php echo $lang_searchuser['submit_return'] ?></a></p></td></tr>
						<?php } else { ?>
							<tr><td class="rowhead" valign="top">用户IP地址</td><td class="rowfollow"><input type=text name=ip style="width:250px"> 请输入用户IP地址</td></tr>
							<tr><td class="rowfollow" colspan=2 align=center>
									<input name="ipsear" type="hidden" value="1" />
									<input type=submit value="<?php echo $lang_searchuser['submit_search']
							?>" class=btn></td></tr>
						</table>
					<?php } ?>
				</form>
			</div>
		</td>
	</tr>
</table>
<table class=main width=750 border=0 cellspacing=0 cellpadding=0>
	<tr>
		<td class=embeddeds>
			<div align=center>
				<h1>客户端IP地址搜索</h1>
				<form method=post action=searchuser.php>
					<?php if ($_GET["returnto"] || $_SERVER["HTTP_REFERER"]) { ?>
						<input type=hidden name=returnto value="<?php echo htmlspecialchars($_GET["returnto"]) ? htmlspecialchars($_GET["returnto"]) : htmlspecialchars($_SERVER["HTTP_REFERER"]) ?>">
					<?php } ?>
					<table cellspacing=0 cellpadding=5>
						<?php
						if (isset($_POST["clentipsear"]) && !empty($_POST["clentipsear"])) {
							$pattern = "/\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/";
							if (!preg_match($pattern, $_POST["clientip"])) {
								print("<tr><td colspan=2 class=\"text\" width=\"300\" align=\"left\"><b>错误的IP格式</b></td></tr>");
							} else {
								$res = sql_query("SELECT DISTINCT(userid) FROM snatched WHERE ip LIKE '%" . mysql_real_escape_string($_POST['clientip']) . "%' GROUP BY userid");
								while ($a = mysql_fetch_assoc($res)) {
									?>
									<tr><td colspan=2 class="text" width="300" align="left"><b><?= get_username($a['userid']) ?></b></td></tr>
									<?php
								}
								if (mysql_num_rows($res) <= 0) {
									?>
									<tr><td colspan=2 class="text" width="300" align="left"><b>客户端IP地址不存在</b></td></tr>
									<?php
								}
							}
							?>
							<tr><td><p align=center><a href=searchuser.php><?php echo $lang_searchuser['submit_return'] ?></a></p></td></tr>
						<?php } else { ?>
							<tr><td class="rowhead" valign="top">客户端IP地址</td><td class="rowfollow"><input type=text name=clientip style="width:250px"> 请输入客户端IP地址</td></tr>
							<tr><td class="rowfollow" colspan=2 align=center>
									<input name="clentipsear" type="hidden" value="1" />
									<input type=submit value="<?php echo $lang_searchuser['submit_search']
							?>" class=btn></td></tr>
						</table>
					<?php } ?>
				</form>
			</div>
		</td>
	</tr>
</table>
<?php
stdfoot();
