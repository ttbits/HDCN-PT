<?php
require "include/bittorrent.php";
dbconn();
require_once(get_langfile_path("takeinvite.php", "", ""));
loggedinorreturn();
parked();
if (isset($_SESSION['viewinvitebox']) && isset($_POST['viewinvitebox']) && $_POST['viewinvitebox'] == $_SESSION['viewinvitebox']) {
	unset($_SESSION['viewinvitebox']);
}
if ($CURUSER ['class'] < UC_MODERATOR)
	stderr("错误", "您没有权限");

function bark($msg) {
	stdmsg('失败！', $msg);
	stdfoot();
	exit;
}

stdhead("邀请申请");
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if ($_POST['setdealt']) {
		$res = sql_query("SELECT Id FROM invitebox WHERE dealt_by='no' AND Id IN (" . implode(", ", $_POST['invitebox']) . ")");
		while ($arr = mysql_fetch_assoc($res))
			sql_query("UPDATE invitebox SET dealt_by = '忽略-" . $CURUSER[username] . "' WHERE Id = $arr[Id]") or sqlerr();
	} elseif ($_POST['delete']) {
		$res = sql_query("SELECT * FROM invitebox WHERE Id IN (" . implode(", ", $_POST[invitebox]) . ")");
		while ($arr = mysql_fetch_assoc($res)) {
			$file = str_replace("%20", " ", "$arr[pic]");
			unlink("$file");
			sql_query("DELETE from invitebox WHERE Id = $arr[Id]") or sqlerr();
		}
	} elseif ($_POST['invite']) {
		$res = sql_query("SELECT * FROM invitebox WHERE dealt_by='no' AND Id IN (" . implode(", ", $_POST['invitebox']) . ")");
		while ($arr = mysql_fetch_assoc($res)) {
			if ($arr ['dealt_by'] == 'no') {
				sql_query("UPDATE invitebox SET dealt_by = '邀请-" . $CURUSER[username] . "' WHERE Id = $arr[Id]") or sqlerr();
				$email = unesc(htmlspecialchars(trim($arr['email'])));
				invite($email);
				print "邀请'" . $email . "'成功";
			} else
				print "邀请'" . $email . "'失败，该申请已处理";
		}
	}
}
$url = "viewinvitebox.php?";
$count = get_row_count("invitebox");
$perpage = 10;
list($pagertop, $pagerbottom, $limit) = pager($perpage, $count, $url);
?>
<h1 style="width:95%">邀请申请</h1>
<table width="95%"><tbody><tr><td class="text" valign="top"><div style="margin-left: 16pt;">1.点击右面的复选框，勾选要处理的申请；<br/>2.该邀请方式不会占用你的邀请名额；<br/>3.请认真审核，仔细处理。优先考虑网络、硬盘条件较好以及经验丰富的用户加入。<br/></div></td></tr></tbody></table>
<table border="1" cellspacing="0" cellpadding="5" align="center" width="95%"><tbody><tr>
	<form method=post action=viewinvitebox.php>
		<td class="colhead">欲申请用户名</td>
		<td class="colhead">IP地址</td>
		<td class="colhead" align="center"> 邮箱 </td>
		<td class="colhead"> 网络情况</td>
		<td class="colhead" align="center"> 硬盘情况 </td>
		<td class="colhead" align="center"> 补充说明 </td>
		<td class="colhead" align="center"> 时间 </td>
		<td class="colhead" align="center"> 其他站点截图 </td>
		<td class="colhead" align="center"> 操作者 </td>
		<td class="colhead" align="center"> 行为 </td>
		</tr>
		<?php
		$res = sql_query("SELECT * FROM invitebox ORDER BY id desc $limit");
		while ($row = mysql_fetch_assoc($res)) {
			$Id = $row[Id];
			$ip = "<a href='ipsearch.php?ip=" . $row[ip] . "' target='_blank' class='faqlink'>$row[ip]</a>";
			$username = $row[username];
			$email = $row[email];
			$school = $row[school];
			$grade = $row[grade];
			$web = $row[web];
			$disk = $row[disk];
			$self_introduction = $row[self_introduction];
			$added = $row[added];
			$pic = $row[pic];
			$dealt_by = $row[dealt_by];
			print("<tr>
	<td class=\"rowfollow\" align=\"center\">$username</td>
	<td class=\"rowfollow\">$ip</td>
	<td class=\"rowfollow\">$email</td>
	<td class=\"rowfollow\">$web</td>
	<td class=\"rowfollow\">$disk</td>
	<td class=\"rowfollow\">$self_introduction</td>
	<td class=\"rowfollow\">$added</td>");
			if ($pic)
				print "<td class=\"rowfollow\"><a class=faqlink href=$pic target=_blank>点此查看</a></td>";
			else
				print "<td class=\"rowfollow\"></td>";
			print "<td class=\"rowfollow\">$dealt_by</td>
	<td class=\"rowfollow\"><input type=\"checkbox\" name=\"invitebox[]\" value=\"$Id\"></td>
	</tr>";
		}
		$_SESSION['viewinvitebox'] = mt_rand(1000000, 9999999);
		?>
		<tr><td class="colhead" colspan="10" align="right"><input class="btn" type="button" value="全选" onclick="this.value = check(form, '全选', '全不选')"><input type="submit" name="setdealt" value="忽略此申请" /><input type="submit" name="invite" value="邀请" /><input type="submit" name="delete" value="删除" /><input type="hidden" name="viewinvitebox" value=<?= $_SESSION['viewinvitebox'] ?>></td></tr>
	</form>
	<?php
	print "</table>";
	echo $pagerbottom;
	stdfoot();
