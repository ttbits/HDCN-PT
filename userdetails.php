<?php
require "include/bittorrent.php";
dbconn();
require_once(get_langfile_path());
loggedinorreturn();
parked();

function bark($msg) {
	global $lang_userdetails;
	stdhead();
	stdmsg($lang_userdetails['std_error'], $msg);
	stdfoot();
	exit;
}

if (isset($_SESSION['hr_clear']) && isset($_POST['hr_clear']) && $_POST['hr_clear'] == $_SESSION['hr_clear']) {
	unset($_SESSION['hr_clear']);
}

$id = 0 + $_GET["id"];
int_check($id, true);

if ($id != $CURUSER['id']) {
	$r = sql_query("SELECT * FROM users WHERE id=" . sqlesc($id)) or sqlerr(__FILE__, __LINE__);
	$user = mysql_fetch_array($r) or bark($lang_userdetails['std_no_such_user']);
} else {
	$user = $CURUSER;
}
$user['seedbonus'] = (int) $user['seedbonus'];
if ($user[added] == "0000-00-00 00:00:00")
	$joindate = $lang_userdetails['text_not_available'];
else
	$joindate = $user[added] . " (" . gettime($user["added"], true, false, true) . ")";
$big = round($user['big']);
$lastseen = $user["last_access"];
if ($lastseen == "0000-00-00 00:00:00")
	$lastseen = $lang_userdetails['text_not_available'];
else {
	$lastseen .= " (" . gettime($lastseen, true, false, true) . ")";
}
$res = sql_query("SELECT COUNT(*) FROM comments WHERE user=" . $user[id]) or sqlerr();
$arr3 = mysql_fetch_row($res);
$torrentcomments = $arr3[0];
$res = sql_query("SELECT COUNT(*) FROM posts WHERE userid=" . $user[id]) or sqlerr();
$arr3 = mysql_fetch_row($res);
$forumposts = $arr3[0];

$arr = get_country_row($user['country']);
if ($arr) {
	$country = "<img src=\"pic/flag/" . $arr['flagpic'] . "\" alt=\"" . $arr['name'] . "\" style='margin-left: 8pt' />";
}

$arr = get_downloadspeed_row($user[download]);
$download = "<img class=\"speed_down\" src=\"pic/trans.gif\" alt=\"Downstream Rate\" title=\"" . $lang_userdetails['title_download'] . $arr[name] . "\" /> " . $arr[name];

$arr = get_uploadspeed_row($user[upload]);
$upload = "<img class=\"speed_up\" src=\"pic/trans.gif\" alt=\"Upstream Rate\" title=\"" . $lang_userdetails['title_upload'] . $arr[name] . "\" /> " . $arr[name];

$arr = get_isp_row($user[isp]);
$isp = $arr[name];

if ($user["gender"] == "Male")
	$gender = "<img class='male' src='pic/trans.gif' alt='Male' title='" . $lang_userdetails['title_male'] . "' style='margin-left: 4pt' />";
elseif ($user["gender"] == "Female")
	$gender = "<img class='female' src='pic/trans.gif' alt='Female' title='" . $lang_userdetails['title_female'] . "' style='margin-left: 4pt' />";
elseif ($user["gender"] == "N/A")
	$gender = "<img class='no_gender' src='pic/trans.gif' alt='N/A' title='" . $lang_userdetails['title_not_available'] . "' style='margin-left: 4pt' />";

stdhead($lang_userdetails['head_details_for'] . $user["username"]);
$enabled = $user["enabled"] == 'yes';
$moviepicker = $user["picker"] == 'yes';

print("<h1 style='margin:0px'>" . get_username($user[id], true, false) . $country . "</h1>");
if ($user["status"] == "pending") {
	if ($CURUSER['class'] < UC_ADMINISTRATOR)
		stderr($lang_userdetails['std_sorry'], $lang_userdetails['std_user_not_confirmed']);
	if ($CURUSER['class'] >= UC_ADMINISTRATOR)
		echo "<b style=\"color:red;font-size:26\"> 此用户没有验证激活！你可以修改邮箱让其重新验证激活，或者<font style=\"color:blue\">手动确认</font>让其通过验证激活</b>";
}
if (!$enabled)
	print("<p><b>" . $lang_userdetails['text_account_disabled_note'] . "</b></p>");
elseif ($CURUSER["id"] != $user["id"]) {
	$r1 = sql_query("SELECT id FROM friends WHERE userid = $CURUSER[id] AND friendid = $id") or sqlerr(__FILE__, __LINE__);
	$friend = mysql_num_rows($r1);
	$r2 = sql_query("SELECT id FROM blocks WHERE userid = $CURUSER[id] AND blockid = $id") or sqlerr(__FILE__, __LINE__);
	$block = mysql_num_rows($r2);

	if ($friend)
		print("<p>(<a href=\"friends.php?action=delete&amp;type=friend&amp;targetid=" . $id . "\">" . $lang_userdetails['text_remove_from_friends'] . "</a>)</p>\n");
	elseif ($block)
		print("<p>(<a href=\"friends.php?action=delete&amp;type=block&amp;targetid=" . $id . "\">" . $lang_userdetails['text_remove_from_blocks'] . "</a>)</p>\n");
	else {
		print("<p>(<a href=\"friends.php?action=add&amp;type=friend&amp;targetid=" . $id . "\">" . $lang_userdetails['text_add_to_friends'] . "</a>)");
		print(" - (<a href=\"friends.php?action=add&amp;type=block&amp;targetid=" . $id . "\">" . $lang_userdetails['text_add_to_blocks'] . "</a>)</p>");
	}
}
begin_main_frame();
/*
  if ($CURUSER['id'] == $user['id'] || get_user_class() >= $cruprfmanage_class)
  print("<h2>" . $lang_userdetails['text_flush_ghost_torrents'] . "<a class=\"altlink\" href=\"takeflush.php?userid=$id\">" . $lang_userdetails['text_here'] . "</a></h2>");
 * 清理冗余种子
 */
?>
<table width="100%" border="1" cellspacing="0" cellpadding="5">
	<?php
	/*
	 * $prfmanage_class 管理用户重要档案权限
	 * 如果用户隐私不是“强”或是管理员或是自己，则不能查看详细信息
	 */
	if ($user['class'] < UC_MODERATOR || get_user_class() >= $prfmanage_class) {
		if ($user['privacy'] != "strong" || get_user_class() >= $prfmanage_class || $CURUSER['id'] == $user['id']) {
			if ($CURUSER['id'] == $user['id'] || get_user_class() >= $viewinvite_class) {
				if ($user["invites"] == 0) {
					tr_small($lang_userdetails['row_invitation'], "<a href=\"invite.php?id=" . $user['id'] . "\" title=\"" . $lang_userdetails['link_send_invitation'] . "\">" . $lang_userdetails['text_no_invitation'] . "</a>", 1);
				} elseif ($user["invites"] < 0) {
					tr_small($lang_userdetails['row_invitation'], "<a href=\"invite.php?id=" . $user['id'] . "\" title=\"" . $lang_userdetails['link_send_invitation'] . "\">∞</a>", 1);
				} else {
					tr_small($lang_userdetails['row_invitation'], "<a href=\"invite.php?id=" . $user['id'] . "\" title=\"" . $lang_userdetails['link_send_invitation'] . "\">" . $user['invites'] . "</a>", 1);
				}
			} else {
				if ($CURUSER['id'] != $user['id'] || get_user_class() != $viewinvite_class) {
					if ($user["invites"] == 0) {
						tr_small($lang_userdetails['row_invitation'], "<a href=\"invite.php?id=" . $user['id'] . "\" title=\"" . $lang_userdetails['link_send_invitation'] . "\">" . $lang_userdetails['text_no_invitation'] . "</a>", 1);
					} elseif ($user["invites"] < 0) {
						tr($lang_userdetails['row_invitation'], '∞', 1);
					} else {
						tr($lang_userdetails['row_invitation'], $user['invites'], 1);
					}
				}
			}
			if ($user["invited_by"] > 0) {
				$userinvited_by = mysql_fetch_array(sql_query("SELECT class FROM users WHERE id = " . $user['invited_by']));
				if ($userinvited_by['class'] < UC_VIP || get_user_class() > UC_NEXUS_MASTER) {
					if (!empty($userinvited_by['class'])) {
						tr_small($lang_userdetails['row_invited_by'], get_username($user['invited_by']), 1);
					}
				}
			}
			tr_small("荣誉值", $big, 1);
			tr_small($lang_userdetails['row_join_date'], $joindate, 1);
			tr_small($lang_userdetails['row_last_seen'], $lastseen, 1);
			if ($where_tweak == "yes") {
				tr_small($lang_userdetails['row_last_seen_location'], $user['page'], 1);
			}
			if (get_user_class() >= $userprofile_class || $user['privacy'] == "low") {
				tr_small($lang_userdetails['row_email'], "<a href=\"mailto:" . $user['email'] . "\">" . $user['email'] . "</a>", 1);
			}
			if (get_user_class() >= $userprofile_class) {
				$resip = sql_query("SELECT ip FROM iplog WHERE userid =$id GROUP BY ip") or sqlerr(__FILE__, __LINE__);
				$iphistory = mysql_num_rows($resip);

				if ($iphistory > 0)
					tr_small($lang_userdetails['row_ip_history'], $lang_userdetails['text_user_earlier_used'] . "<b><a href=\"iphistory.php?id=" . $user['id'] . "\">" . $iphistory . $lang_userdetails['text_different_ips'] . add_s($iphistory, true) . "</a></b>", 1);
			}
			if (get_user_class() >= $userprofile_class || $user["id"] == $CURUSER["id"]) {
				if ($enablelocation_tweak == 'yes') {
					list($loc_pub, $loc_mod) = get_ip_location($user['ip']);
					$locationinfo = "<span title=\"" . $loc_mod . "\">[" . $loc_pub . "]</span>";
				} else {
					$locationinfo = "";
				}
				if (school_ip_location($user['ip']) == '') {
					$school = "未知";
				} else {
					$school = school_ip_location($user['ip']);
				}
				if (!ip2long($user['ip'])) {
					tr_small($lang_userdetails['row_ip_address'], $user['ip'] . "[" . $school . "]", 1);
				} else {
					tr_small($lang_userdetails['row_ip_address'], $user['ip'] . $locationinfo, 1);
				}
			}

			$res = sql_query("SELECT agent, peer_id, ip, ipv6, port, connectable, SUM(seeder='yes') AS seeder, SUM(seeder='no') AS leecher FROM peers WHERE userid = $user[id] GROUP BY ip, agent") or sqlerr(__FILE__, __LINE__);
			if (mysql_num_rows($res) > 0) {
				$clientselect = "";
				while ($arr = mysql_fetch_array($res)) {
					$clientselect .= "<tr class=torrent_table style='height:25px'><td align='center'>&nbsp;&nbsp;" . get_agent($arr["peer_id"], $arr["agent"]) . "&nbsp;&nbsp;</td>"; //客户端
					if (get_user_class() >= $userprofile_class || $user["id"] == $CURUSER["id"]) {
						$ip = $arr['ip'] != '' ? $arr['ip'] : "N/A";
						$ipv6 = $arr['ipv6'] != '' ? $arr['ipv6'] : "N/A";
						$port = $arr['port'] != '' ? $arr['port'] : "N/A";
						$connectable = $arr['connectable'] == 'yes' ? "<font style='color:green'>是</font>" : "<font style='color:red'>否</font>";
						if (ip2long($ip)) {//IPv4
							$clientselect .= "<td align='center'>&nbsp;&nbsp;" . $ip . "&nbsp;&nbsp;</td><td align='center'>&nbsp;&nbsp;" . $ipv6 . "&nbsp;&nbsp;</td><td align='center'>&nbsp;&nbsp;" . $port . "&nbsp;&nbsp;</td><td align='center'>&nbsp;&nbsp;" . $connectable . "&nbsp;&nbsp;</td><td align='center'>&nbsp;&nbsp;" . $arr['seeder'] . "&nbsp;&nbsp;</td><td align='center'>&nbsp;&nbsp;" . $arr['leecher'] . "&nbsp;&nbsp;</td></tr>";
						} else {
							$clientselect .= "<td align='center'>&nbsp;&nbsp;N/A&nbsp;&nbsp;</td><td align='center'>&nbsp;&nbsp;" . $ip . "&nbsp;&nbsp;</td><td align='center'>&nbsp;&nbsp;" . $port . "&nbsp;&nbsp;</td><td align='center'>&nbsp;&nbsp;" . $connectable . "&nbsp;&nbsp;</td><td align='center'>&nbsp;&nbsp;" . $arr['seeder'] . "&nbsp;&nbsp;</td><td align='center'>&nbsp;&nbsp;" . $arr['leecher'] . "&nbsp;&nbsp;</td></tr>";
						}
					}
				}
			}

			if ($clientselect) {
				tr_small($lang_userdetails['row_bt_client'], "<table><tr class=rowfollow style='height:25px'><td class='colhead' align='center'>&nbsp;&nbsp;客户端&nbsp;&nbsp;</td><td class='colhead' align='center'>&nbsp;&nbsp;IPv4&nbsp;&nbsp;</td><td class='colhead' align='center'>&nbsp;&nbsp;IPv6&nbsp;&nbsp;</td><td class='colhead' align='center'>&nbsp;&nbsp;端口号&nbsp;&nbsp;</td><td class='colhead' align='center'>&nbsp;&nbsp;可连接&nbsp;&nbsp;</td><td class='colhead' align='center'>&nbsp;&nbsp;当前做种&nbsp;&nbsp;</td><td class='colhead' align='center'>&nbsp;&nbsp;当前下载&nbsp;&nbsp;</td></tr>$clientselect</table>", 1);
			}


			if ($user["downloaded"] > 0) {
				$sr = floor($user["uploaded"] / $user["downloaded"] * 1000) / 1000;
				$sr = "<tr><td class=\"embedded\"><strong>" . $lang_userdetails['row_share_ratio'] . "</strong>:  <font color=\"" . get_ratio_color($sr) . "\">" . number_format($sr, 3) . "</font></td><td class=\"embedded\">&nbsp;&nbsp;" . get_ratio_img($sr) . "</td></tr>";
			}

			$xfer = "<tr><td class=\"embedded\"><strong>" . $lang_userdetails['row_uploaded'] . "</strong>:  " . mksize($user["uploaded"]) . "</td><td class=\"embedded\">&nbsp;&nbsp;<strong>" . $lang_userdetails['row_downloaded'] . "</strong>:  " . mksize($user["downloaded"]) . "</td></tr>";

			tr_small($lang_userdetails['row_transfer'], "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" . $sr . $xfer . "</table>", 1);


			if ($user["leechtime"] > 0) {
				$slr = floor($user["seedtime"] / $user["leechtime"] * 1000) / 1000;
				$slr = "<tr><td class=\"embedded\"><strong>" . $lang_userdetails['text_seeding_leeching_time_ratio'] . "</strong>:  <font color=\"" . get_ratio_color($slr) . "\">" . number_format($slr, 3) . "</font></td><td class=\"embedded\">&nbsp;&nbsp;" . get_ratio_img($slr) . "</td></tr>";
			}

			$slt = "<tr><td class=\"embedded\"><strong>" . $lang_userdetails['text_seeding_time'] . "</strong>:  " . mkprettytime($user["seedtime"]) . "</td><td class=\"embedded\">&nbsp;&nbsp;<strong>" . $lang_userdetails['text_leeching_time'] . "</strong>:  " . mkprettytime($user["leechtime"]) . "</td></tr>";

			tr_small($lang_userdetails['row_sltime'], "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" . $slr . $slt . "</table>", 1);

			if ($user["download"] && $user["upload"])
				tr_small($lang_userdetails['row_internet_speed'], $download . "&nbsp;&nbsp;&nbsp;&nbsp;" . $upload . "&nbsp;&nbsp;&nbsp;&nbsp;" . $isp, 1);
			tr_small($lang_userdetails['row_gender'], $gender, 1);

			if (($user['donated'] > 0 || $user['donated_cny'] > 0 ) && (get_user_class() >= $userprofile_class || $CURUSER["id"] == $user["id"]))
				tr_small($lang_userdetails['row_donated'], "$" . htmlspecialchars($user[donated]) . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . htmlspecialchars($user[donated_cny]), 1);

			if ($user["avatar"])
				tr_small($lang_userdetails['row_avatar'], return_avatar_image(htmlspecialchars(trim($user["avatar"]))), 1);
			tr_small($lang_userdetails['row_class'], "<b>[" . get_user_class_name_zh($user['class'], false, true, true) . "]</b> " . ($user['title'] !== "" ? "&nbsp;" . htmlspecialchars(trim($user["title"])) . "" : ""), 1);
			//H&R显示和魔力值消除
			if ($hr == 'yes') {
				$userid = $user['id'];
				$hrstatus = mysql_fetch_array(sql_query("SELECT hr, donor, seedbonus FROM users WHERE id = $userid")) or sqlerr(__FILE__, __LINE__);
				if ($CURUSER['id'] == $userid && get_user_class() < $hrmanage_class && $hrstatus['donor'] == 'no') {
					tr_small($lang_userdetails['row_hr_num'], ($hrstatus['hr'] . "/" . ($stronghr == 'no' ? $hrhit : $stronghr_radio)), 1);
				} elseif (get_user_class() > UC_NEXUS_MASTER) {
					tr_small($lang_userdetails['row_hr_num'], ($hrstatus['hr'] . "/" . ($stronghr == 'no' ? $hrhit : $stronghr_radio)), 1);
				}
				//if ($CURUSER['id'] == $user['id'] && get_user_class() < $hrmanage_class && $hrstatus['donor'] == 'no' && $hrstatus['seedbonus'] > $hrbonus) {
				if ($CURUSER['id'] == $user['id'] && $hrstatus['seedbonus'] > $hrbonus && $stronghr == 'no') {
					$_SESSION['hr_clear'] = mt_rand(1000000, 9999999);
					tr_small($lang_userdetails['row_hr_clear'], "<form method=\"POST\" onsubmit=\"if(confirm('你确定要这样做吗？')){return true;}else{return false;}\"><input type=hidden name='hr_clear' value='" . $_SESSION['hr_clear'] . "'><input type=\"text\" name=\"hrclear\" style=\"width: 100px\" onkeyup=\"this.value=this.value.replace(/\D/g, '');\"><input type=\"submit\" value=\"确定\"/><br />注意：消除每个H&R会消耗<b> $hrbonus </b>个魔力值</form>", 1);
				}
				if ($_POST['hrclear'] != "" && $_POST['hrclear'] != "0" && $stronghr == 'no') {
					$checkhr = (0 + $_POST['hrclear']);
					if ($checkhr > $hrstatus['hr']) {
						echo "<script>alert('不能大于已得H&R数量');</script>";
					} elseif (!ereg("^[0-9]+$", $checkhr)) {
						echo "<script>alert('只能为正整数');</script>";
					} else {
						sql_query("UPDATE users SET seedbonus = seedbonus - $hrbonus * " . $_POST['hrclear'] . ", hr = hr - " . $_POST['hrclear'] . " WHERE id=$userid") or sqlerr(__FILE__, __LINE__);
						echo "<script>alert('成功的用 " . $hrbonus * $_POST['hrclear'] . " 魔力值消除 " . $_POST['hrclear'] . " 个H&R！');location.href='userdetails.php?id=$userid';</script>";
						writeBonusComment($userid, "花费 " . $hrbonus * $_POST['hrclear'] . " 个魔力值消除 " . $_POST['hrclear'] . " 个H&R");
					}
				}
			}

			if ($user['class'] >= $userbar_class) {
				tr_small('流量条', ("<img src = \"mybar.php?userid=" . $id . ".png\" /></br>如果你在控制台中将你的隐私等级设为“高”，则不能使用个性条。</br>更多流量条样式点击<a href='/promotionlink.php' class='faqlink'>→这里←</a>"), 1);
			}

			tr_small($lang_userdetails['row_torrent_comment'], ($torrentcomments && ($user["id"] == $CURUSER["id"] || get_user_class() >= $viewhistory_class) ? "<a href=\"userhistory.php?action=viewcomments&amp;id=" . $id . "\" title=\"" . $lang_userdetails['link_view_comments'] . "\">" . $torrentcomments . "</a>" : $torrentcomments), 1);

			tr_small($lang_userdetails['row_forum_posts'], ($forumposts && ($user["id"] == $CURUSER["id"] || get_user_class() >= $viewhistory_class) ? "<a href=\"userhistory.php?action=viewposts&amp;id=" . $id . "\" title=\"" . $lang_userdetails['link_view_posts'] . "\">" . $forumposts . "</a>" : $forumposts), 1);

			if ($user["id"] == $CURUSER["id"] || get_user_class() >= $viewhistory_class)
				tr_small($lang_userdetails['row_karma_points'], htmlspecialchars($user[seedbonus]) . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . "<a href='myhistory.php?id=" . $user['id'] . "' class='faqlink'>" . $lang_userdetails['show_myhistory'] . "</a>", 1);

			if ($user["ip"] && (get_user_class() >= $torrenthistory_class || $user["id"] == $CURUSER["id"])) {

				tr_small($lang_userdetails['row_uploaded_torrents'], "<a name=\"uploaded\" href=\"javascript: getusertorrentlistajax('" . $user['id'] . "', 'uploaded', 'ka'); klappe_news('a')\"><img class=\"plus\" src=\"pic/trans.gif\" id=\"pica\" alt=\"Show/Hide\" title=\"" . $lang_userdetails['title_show_or_hide'] . "\" />   <u>" . $lang_userdetails['text_show_or_hide'] . "</u></a><div id=\"ka\" style=\"display: none;\"></div>", 1);


				tr_small($lang_userdetails['row_current_seeding'], "<a name=\"seeding\" href=\"javascript: getusertorrentlistajax('" . $user['id'] . "', 'seeding', 'ka1'); klappe_news('a1')\"><img class=\"plus\" src=\"pic/trans.gif\" id=\"pica1\" alt=\"Show/Hide\" title=\"" . $lang_userdetails['title_show_or_hide'] . "\" />   <u>" . $lang_userdetails['text_show_or_hide'] . "</u></a><div id=\"ka1\" style=\"display: none;\"></div>", 1);


				tr_small($lang_userdetails['row_current_leeching'], "<a name=\"leeching\" href=\"javascript: getusertorrentlistajax('" . $user['id'] . "', 'leeching', 'ka2'); klappe_news('a2')\"><img class=\"plus\" src=\"pic/trans.gif\" id=\"pica2\" alt=\"Show/Hide\" title=\"" . $lang_userdetails['title_show_or_hide'] . "\" />   <u>" . $lang_userdetails['text_show_or_hide'] . "</u></a><div id=\"ka2\" style=\"display: none;\"></div>", 1);


				tr_small($lang_userdetails['row_completed_torrents'], "<a name=\"completed\" href=\"javascript: getusertorrentlistajax('" . $user['id'] . "', 'completed', 'ka3'); klappe_news('a3')\"><img class=\"plus\" src=\"pic/trans.gif\" id=\"pica3\" alt=\"Show/Hide\" title=\"" . $lang_userdetails['title_show_or_hide'] . "\" />   <u>" . $lang_userdetails['text_show_or_hide'] . "</u></a><div id=\"ka3\" style=\"display: none;\"></div>", 1);


				tr_small($lang_userdetails['row_incomplete_torrents'], "<a name=\"incomplete\" href=\"javascript: getusertorrentlistajax('" . $user['id'] . "', 'incomplete', 'ka4'); klappe_news('a4')\"><img class=\"plus\" src=\"pic/trans.gif\" id=\"pica4\" alt=\"Show/Hide\" title=\"" . $lang_userdetails['title_show_or_hide'] . "\" />   <u>" . $lang_userdetails['text_show_or_hide'] . "</u></a><div id=\"ka4\" style=\"display: none;\"></div>", 1);
				if ($_GET['show']) {
					switch ($_GET['show']) {
						case 'uploaded': $ka = 'ka';
							$a = 'a';
							break;
						case 'seeding' : $ka = 'ka1';
							$a = 'a1';
							break;
						case 'leeching' : $ka = 'ka2';
							$a = 'a2';
							break;
						case 'completed' : $ka = 'ka3';
							$a = 'a3';
							break;
						case 'incomplete' : $ka = 'ka4';
							$a = 'a4';
							break;
					}
					$url = $_SERVER['PHP_SELF'] . "?" . $_SERVER["QUERY_STRING"] . "#" . $_GET['show'];
					echo "<script language='javascript'>location = '" . $url . "'; getusertorrentlistajax('" . $user['id'] . "', '" . $_GET['show'] . "', '" . $ka . "'); klappe_news('" . $a . "')</script>";
				}
			}
			if ($user["info"]) {
				print("<tr><td align=\"left\" colspan=\"2\" class=\"text\">" . format_comment($user["info"], false) . "</td></tr>\n");
			}
		} else {
			print("<tr><td align=\"left\" colspan=\"2\" class=\"text\"><font color=\"blue\">" . $lang_userdetails['text_public_access_denied'] . $user['username'] . $lang_userdetails['text_user_wants_privacy'] . "</font></td></tr>\n");
		}
	} else {
		print("<tr><td align=\"left\" colspan=\"2\" class=\"text\"><font color=\"blue\">" . $lang_userdetails['text_public_access_denied'] . $user['username'] . $lang_userdetails['text_user_wants_privacy'] . "</font></td></tr>\n");
	}
	if ($CURUSER["id"] != $user["id"])
		if (get_user_class() >= $staffmem_class)
			$showpmbutton = 1;
		elseif ($user["acceptpms"] == "yes") {
			$r = sql_query("SELECT id FROM blocks WHERE userid=$user[id] AND blockid=$CURUSER[id]") or sqlerr(__FILE__, __LINE__);
			$showpmbutton = (mysql_num_rows($r) == 1 ? 0 : 1);
		} elseif ($user["acceptpms"] == "friends") {
			$r = sql_query("SELECT id FROM friends WHERE userid=$user[id] AND friendid=$CURUSER[id]") or sqlerr(__FILE__, __LINE__);
			$showpmbutton = (mysql_num_rows($r) == 1 ? 1 : 0);
		}
	//online or offline//
	$Cache->add_whole_row();
	$secs = 90;
	$dt = TIMENOW - $secs;
	//$dt = sqlesc(date("Y-m-d H:i:s",(TIMENOW - $secs)));//calculate time
	$res5 = sql_query("SELECT*FROM users WHERE users.status='confirmed' ORDER BY users.username") or sqlerr();
	$arr5 = mysql_fetch_assoc($res5);
	//$arr2 = get_user_row($CURUSER["id"]);
	if ($CURUSER["id"] != $user["id"]) {
		print("<tr><td colspan=\"2\" align=\"center\" valign=\"middle\">" . ("'" . $arr5['last_access'] . "'" > $dt && $arr5['class'] != UC_STAFFLEADER ? "<img class=\"f_online\" src=\"pic/trans.gif\" alt=\"Online\" title=\"" . $lang_userdetails['title_online'] . "\"/>" : "<img class=\"f_offline\" src=\"pic/trans.gif\" alt=\"Offline\" title=\"" . $lang_userdetails['title_offline'] . "\"/>") . "");

		//--end--//

		if ($showpmbutton) {
			if ($user['class'] == UC_STAFFLEADER) {
				print("<a href=\"contactstaff.php\"><img class=\"f_pm\" src=\"pic/trans.gif\" alt=\"PM\" title=\"" . $lang_userdetails['title_send_pm'] . "\" /></a>");
			} else {
				print("<a href=\"sendmessage.php?receiver=" . htmlspecialchars($user['id']) . "\"><img class=\"f_pm\" src=\"pic/trans.gif\" alt=\"PM\" title=\"" . $lang_userdetails['title_send_pm'] . "\" /></a>");
			}
		}

		print("<a href=\"report.php?user=" . htmlspecialchars($user['id']) . "\"><img class=\"f_report\" src=\"pic/trans.gif\" alt=\"Report\" title=\"" . $lang_userdetails['title_report_user'] . "\" /></a>");
		$usernamebonus = get_username($user['id'], false, false, false, false, false, false, false, false, true);
		?>
		<form id="giftform" action="mybonus.php?action=exchange" method="post">
			<input type="hidden" id="option" name="option" value="8"/>
			<input type="hidden" id="username" name="username" readonly="readonly" value="<?= $usernamebonus ?>" maxlength="24">
			<input type="hidden" id="bonus" name="bonus" value="<?= $CURUSER['seedbonus'] ?>">
			<input type="hidden" id="where" name="where" value="[url=userdetails.php]用户详情[/url]"/>
			赠送 <input type="text" name="bonusgift" id="bonusgift" style="width: 80px"/> 个魔力值给 <b><?= $usernamebonus ?></b>，原因：<input type="text" id="message" name="message" style="width: 150px" maxlength="100">
			<input type="button" value="赠送" id="giftsubmit"/>
		</form>
		<script type="text/javascript">
			$("#giftform #giftsubmit").click(function () {
				var bgift = $(this).siblings("#bonusgift").val();
				var uname = $(this).siblings("#username").val();
				var msg = $(this).siblings("#message").val();
				var where = $(this).siblings("#where").val();
				var bonus = $(this).siblings("#bonus").val();
				if (bgift >= 25 && bgift <= 10000) {
					jConfirm('确定要赠送 ' + bgift + ' 个魔力值给 ' + uname + ' 吗？', '提示', function (v) {
						if (v) {
							$.post("mybonus.php?action=exchange&t=" + new Date(), {
								username: uname,
								option: 8,
								bonusgift: bgift,
								message: msg,
								where: where}, function () {
								jConfirm('赠送成功，是否立即刷新？', '提示', function (v) {
									if (v) {
										window.location.reload();
									}
								});
							});
						}
					});
				} else {
					jAlert('每次只能赠送25至10000个魔力值', '提示');
				}
			});
		</script>
		<?php
		print("</td></tr>");
	}
	print("</table>\n");

	if (get_user_class() >= $prfmanage_class && $user['class'] < get_user_class() || get_user_class() == UC_STAFFLEADER) {
		//if (get_user_class() >= $prfmanage_class) {
		begin_frame($lang_userdetails['text_edit_user'], true);
		print("<form method=\"post\" action=\"modtask.php\">");
		print("<input type=\"hidden\" name=\"action\" value=\"edituser\" />");
		print("<input type=\"hidden\" name=\"userid\" value=\"" . $id . "\" />");
		print("<input type=\"hidden\" name=\"returnto\" value=\"" . htmlspecialchars("userdetails.php?id=$id") . "\" />");
		print("<table width=\"100%\" class=\"main\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\">\n");
		tr($lang_userdetails['row_title'], "<input type=\"text\" size=\"60\" name=\"title\" value=\"" . htmlspecialchars(trim($user[title])) . "\" />", 1);
		$avatar = htmlspecialchars(trim($user["avatar"]));

		tr($lang_userdetails['row_privacy_level'], "<input type=\"radio\" name=\"privacy\" value=\"low\"" . ($user["privacy"] == "low" ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_low'] . "<input type=\"radio\" name=\"privacy\" value=\"normal\"" . ($user["privacy"] == "normal" ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_normal'] . "<input type=\"radio\" name=\"privacy\" value=\"strong\"" . ($user["privacy"] == "strong" ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_strong'], 1);
		tr($lang_userdetails['row_avatar_url'], "<input type=\"text\" size=\"60\" name=\"avatar\" value=\"" . $avatar . "\" />", 1);
		$signature = trim($user["signature"]);
		tr($lang_userdetails['row_signature'], "<textarea cols=\"60\" rows=\"6\" name=\"signature\">" . $signature . "</textarea>", 1);

		if (get_user_class() >= UC_MODERATOR) {
			tr(新人考核, "<input type=\"radio\" name=\"newuser\" value=\"yes\"" . ($user["newuser"] == "yes" ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_yes'] . " <input type=\"radio\" name=\"newuser\" value=\"no\"" . ($user["newuser"] == "no" ? " checked=\"checked\"" : "") . ">" . $lang_userdetails['radio_no'], 1);
		}

		if (get_user_class() == UC_STAFFLEADER) {
			tr($lang_userdetails['row_donor_status'], "<input type=\"radio\" name=\"donor\" value=\"yes\"" . ($user["donor"] == "yes" ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_yes'] . " <input type=\"radio\" name=\"donor\" value=\"no\"" . ($user["donor"] == "no" ? " checked=\"checked\"" : "") . ">" . $lang_userdetails['radio_no'], 1);
			tr($lang_userdetails['row_donated'], "USD: <input type=\"text\" size=\"5\" name=\"donated\" value=\"" . htmlspecialchars($user[donated]) . "\" />&nbsp;&nbsp;&nbsp;&nbsp;CNY: <input type=\"text\" size=\"5\" name=\"donated_cny\" value=\"" . htmlspecialchars($user[donated_cny]) . "\" />&nbsp;&nbsp;&nbsp;&nbsp;" . $lang_userdetails['text_transaction_memo'] . " <input type=\"text\" size=\"50\" name=\"donation_memo\" />", 1);
		}
		if (get_user_class() == $prfmanage_class) {
			$maxclass = UC_MODERATOR;
		} elseif (get_user_class() == UC_STAFFLEADER) {
			$maxclass = get_user_class();
		} else {
			$maxclass = get_user_class() - 1;
		}
		$classselect = classlist('class', $maxclass, $user["class"]);
		tr($lang_userdetails['row_class'], $classselect, 1);
		tr($lang_userdetails['row_vip_by_bonus'], "<input type=\"radio\" name=\"vip_added\" value=\"yes\"" . ($user["vip_added"] == "yes" ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_yes'] . " <input type=\"radio\" name=\"vip_added\" value=\"no\"" . ($user["vip_added"] == "no" ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_no'] . "<br />" . $lang_userdetails['text_vip_by_bonus_note'], 1);
		tr($lang_userdetails['row_vip_until'], "<input type=\"text\" name=\"vip_until\" onfocus='WdatePicker({dateFmt: \"yyyy-MM-dd HH:mm:ss\", isShowWeek: \"true\", minDate: \"%y-%M-%d %H:%m:%s\"})' class='Wdate' style='width:160px' value=\"" . htmlspecialchars($user["vip_until"]) . "\" /> " . $lang_userdetails['text_vip_until_note'], 1);
		$supportlang = htmlspecialchars($user["supportlang"]);
		$supportfor = htmlspecialchars($user["supportfor"]);
		$pickfor = htmlspecialchars($user["pickfor"]);
		$staffduties = htmlspecialchars($user["stafffor"]);

		tr($lang_userdetails['row_staff_duties'], "<textarea cols=\"60\" rows=\"6\" name=\"staffduties\">" . $staffduties . "</textarea>", 1);
		//tr($lang_userdetails['row_support_language'], "<input type=\"text\" name=\"supportlang\" value=\"" . $supportlang . "\" />", 1);
		tr($lang_userdetails['row_support'], "<input type=\"radio\" name=\"support\" value=\"yes\"" . ($user["support"] == "yes" ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_yes'] . " <input type=\"radio\" name=\"support\" value=\"no\"" . ($user["support"] == "no" ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_no'], 1);
		tr($lang_userdetails['row_support_for'], "<textarea cols=\"60\" rows=\"6\" name=\"supportfor\">" . $supportfor . "</textarea>", 1);

		tr($lang_userdetails['row_movie_picker'], "<input name=\"moviepicker\" value=\"yes\" type=\"radio\"" . ($moviepicker ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_yes'] . "<input name=\"moviepicker\" value=\"no\" type=\"radio\"" . (!$moviepicker ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_no'], 1);
		tr($lang_userdetails['row_pick_for'], "<textarea cols=\"60\" rows=\"6\" name=\"pickfor\">" . $pickfor . "</textarea>", 1);

		if (get_user_class() >= $cruprfmanage_class) {
			$modcomment = htmlspecialchars($user["modcomment"]);
			tr($lang_userdetails['row_comment'], "<textarea cols=\"60\" rows=\"6\" name=\"modcomment\">" . $modcomment . "</textarea>", 1);
			$bonuscomment = htmlspecialchars($user["bonuscomment"]);
			tr($lang_userdetails['row_seeding_karma'], "<textarea cols=\"60\" rows=\"6\" name=\"bonuscomment\" readonly=\"readonly\">" . $bonuscomment . "</textarea>", 1);
		}
		$warned = $user["warned"] == "yes";

		print("<tr><td class=\"rowhead\">" . $lang_userdetails['row_warning_system'] . "</td><td class=\"rowfollow\" align=\"left\" ><table class=\"main\" cellspacing=\"0\" cellpadding=\"5\"><tr><td class=\"rowfollow\">" . ($warned ? "<input name=\"warned\" value=\"yes\" type=\"radio\" checked=\"checked\" />" . $lang_userdetails['radio_yes'] . "<input name=\"warned\" value=\"no\" type=\"radio\" />" . $lang_userdetails['radio_no'] : $lang_userdetails['text_not_warned'] ) . "</td>");

		if ($warned) {
			$warneduntil = $user['warneduntil'];
			if ($warneduntil == '0000-00-00 00:00:00')
				print("<td align=\"center\" class=\"rowfollow\">" . $lang_userdetails['text_arbitrary_duration'] . "</td>\n");
			else {
				print("<td align=\"left\" class=\"rowfollow\">" . $lang_userdetails['text_until'] . $warneduntil);
				print("<br />(" . mkprettytime(strtotime($warneduntil) - strtotime(date("Y-m-d H:i:s"))) . $lang_userdetails['text_to_go'] . ")</td>\n");
			}
			print("</tr>");
		} else {
			print("<td align=\"left\" class=\"rowfollow\">" . $lang_userdetails['text_warn_for'] . "<select name=\"warnlength\">\n");
			print("<option value=\"0\">------</option>\n");
			print("<option value=\"11\">1天</option>\n");
			print("<option value=\"33\">3天</option>\n");
			print("<option value=\"1\">1 " . $lang_userdetails['text_week'] . "</option>\n");
			print("<option value=\"2\">2 " . $lang_userdetails['text_weeks'] . "</option>\n");
			print("<option value=\"4\">4 " . $lang_userdetails['text_weeks'] . "</option>\n");
			print("<option value=\"8\">8 " . $lang_userdetails['text_weeks'] . "</option>\n");
			print("<option value=\"255\">" . $lang_userdetails['text_unlimited'] . "</option>\n");
			print("</select></td></tr>\n");
			print("<tr><td align=\"left\" class=\"rowfollow\">" . $lang_userdetails['text_reason_of_warning'] . "</td><td align=\"left\" class=\"rowfollow\"><input type=\"text\" size=\"60\" name=\"warnpm\" /></td></tr>");
		}

		$elapsedlw = get_elapsed_time(strtotime($user["lastwarned"]));
		print("<tr><td align=\"left\" class=\"rowfollow\">" . $lang_userdetails['text_times_warned'] . "</td><td align=\"left\" class=\"rowfollow\">" . $user[timeswarned] . "</td></tr>\n");

		if ($user["timeswarned"] == 0) {
			print("<tr><td align=\"left\" class=\"rowfollow\">" . $lang_userdetails['text_last_warning'] . "</td><td align=\"left\" class=\"rowfollow\">" . $lang_userdetails['text_not_warned_note'] . "</td></tr>\n");
		} else {
			if ($user["warnedby"] != "System") {
				$res = sql_query("SELECT id, username, warnedby FROM users WHERE id = " . $user['warnedby'] . "") or sqlerr(__FILE__, __LINE__);
				$arr = mysql_fetch_assoc($res);
				$warnedby = "<br />[" . $lang_userdetails['text_by'] . "<u>" . get_username($arr['id']) . "</u></a>]";
			} else {
				$warnedby = "<br />[" . $lang_userdetails['text_by_system'] . "]";
				print("<tr><td class=\"rowfollow\">" . $lang_userdetails['text_last_warning'] . "</td><td align=\"left\" class=\"rowfollow\"> $user[lastwarned] .(" . $lang_userdetails['text_until'] . "$elapsedlw)   $warnedby</td></tr>\n");
			}
			print("<tr><td class=\"rowfollow\">" . $lang_userdetails['text_last_warning'] . "</td><td align=\"left\" class=\"rowfollow\"> $user[lastwarned] ($elapsedlw" . $lang_userdetails['text_ago'] . ")   " . $warnedby . "</td></tr>\n");
		}

		$leechwarn = $user["leechwarn"] == "yes";
		print("<tr><td class=\"rowfollow\">" . $lang_userdetails['row_auto_warning'] . "<br /><i>(" . $lang_userdetails['text_low_ratio'] . ")</i></td>");

		if ($leechwarn) {
			print("<td align=\"left\" class=\"rowfollow\"><font color=\"red\">" . $lang_userdetails['text_leech_warned'] . "</font> ");
			$leechwarnuntil = $user['leechwarnuntil'];
			if ($leechwarnuntil != '0000-00-00 00:00:00') {
				print($lang_userdetails['text_until'] . $leechwarnuntil);
				print("<br />(" . mkprettytime(strtotime($leechwarnuntil) - strtotime(date("Y-m-d H:i:s"))) . $lang_userdetails['text_to_go'] . ")");
			} else {
				print("<i>" . $lang_userdetails['text_for_unlimited_time'] . "</i>");
			}
			print("</td></tr>");
		} else {
			print("<td class=\"rowfollow\">" . $lang_userdetails['text_no_warned'] . "</td></tr>\n");
		}
		print("</table></td></tr>");
		tr($lang_userdetails['row_enabled'], "<input name=\"enabled\" value=\"yes\" type=\"radio\"" . ($enabled ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_yes'] . "<input name=\"enabled\" value=\"no\" type=\"radio\"" . (!$enabled ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_no'] . "------>封禁用户功能挪到页底的删除一起执行，此处更改无效", 1);
		tr($lang_userdetails['row_forum_post_possible'], "<input type=\"radio\" name=\"forumpost\" value=\"yes\"" . ($user["forumpost"] == "yes" ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_yes'] . "<input type=\"radio\" name=\"forumpost\" value=\"no\"" . ($user["forumpost"] == "no" ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_no'] . "--------->如果被警告同时禁言、禁止上传、下载的话，截止日期与警告相同，一同恢复", 1);
		tr($lang_userdetails['row_upload_possible'], "<input type=\"radio\" name=\"uploadpos\" value=\"yes\"" . ($user["uploadpos"] == "yes" ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_yes'] . "<input type=\"radio\" name=\"uploadpos\" value=\"no\"" . ($user["uploadpos"] == "no" ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_no'], 1);
		tr($lang_userdetails['row_download_possible'], "<input type=\"radio\" name=\"downloadpos\" value=\"yes\"" . ($user["downloadpos"] == "yes" ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_yes'] . "<input type=\"radio\" name=\"downloadpos\" value=\"no\"" . ($user["downloadpos"] == "no" ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_no'], 1);
		tr($lang_userdetails['row_show_ad'], "<input type=\"radio\" name=\"noad\" value=\"no\"" . ($user["noad"] == "no" ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_yes'] . "<input type=\"radio\" name=\"noad\" value=\"yes\"" . ($user["noad"] == "yes" ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_no'], 1);
		tr($lang_userdetails['row_no_ad_until'], "<input type=\"text\" name=\"noaduntil\" onfocus='WdatePicker({dateFmt: \"yyyy-MM-dd HH:mm:ss\", isShowWeek: \"true\", minDate: \"%y-%M-%d %H:%m:%s\"})' class='Wdate' style='width:160px' value=\"" . htmlspecialchars($user["noaduntil"]) . "\" /> " . $lang_userdetails['text_no_ad_until_note'], 1);
		if (get_user_class() >= $cruprfmanage_class) {
			tr($lang_userdetails['row_change_username'], "<input type=\"text\" size=\"25\" name=\"cusername\" value=\"" . htmlspecialchars($user[username]) . "\" />", 1);
			tr($lang_userdetails['row_change_email'], "<input type=\"text\" size=\"80\" name=\"cuemail\" value=\"" . htmlspecialchars($user[email]) . "\" />", 1);
			if ($hr == 'yes' && $stronghr == 'yes') {
				tr("改变H&R数量", "<input type=\"text\" size=\"50\" name=\"cuhr\" onkeyup=\"this.value = this.value.replace(/\D/g, '');\" value=\"" . htmlspecialchars($user[hr]) . "\" />", 1);
			}
		}

		tr($lang_userdetails['row_change_password'], "<input type=\"password\" name=\"chpassword\" size=\"50\" />", 1);
		tr($lang_userdetails['row_repeat_password'], "<input type=\"password\" name=\"passagain\" size=\"50\" />", 1);

		if (get_user_class() >= $cruprfmanage_class) {
			tr($lang_userdetails['row_amount_uploaded'], "<input type=\"text\" size=\"60\" name=\"uploaded\" value=\"" . htmlspecialchars($user[uploaded]) . "\" /><input type=\"hidden\" name=\"ori_uploaded\" value=\"" . htmlspecialchars($user[uploaded]) . "\" />", 1);
			tr($lang_userdetails['row_amount_downloaded'], "<input type=\"text\" size=\"60\" name=\"downloaded\" value=\"" . htmlspecialchars($user[downloaded]) . "\" /><input type=\"hidden\" name=\"ori_downloaded\" value=\"" . htmlspecialchars($user[downloaded]) . "\" />", 1);
			tr($lang_userdetails['row_seeding_karma'], "<input type=\"text\" size=\"60\" name=\"bonus\" value=\"" . htmlspecialchars($user[seedbonus]) . "\" /><input type=\"hidden\" name=\"ori_bonus\" value=\"" . htmlspecialchars($user[seedbonus]) . "\" />", 1);
			if (get_user_class() < UC_ADMINISTRATOR) {
				tr($lang_userdetails['row_invites'], "<input type=\"text\" size=\"60\" name=\"invites\" value=\"" . htmlspecialchars($user[invites]) . "\" onkeyup=\"this.value=this.value.replace(/\D/g, '');\" />", 1);
			} else {
				tr($lang_userdetails['row_invites'], "<input type=\"text\" size=\"60\" name=\"invites\" value=\"" . htmlspecialchars($user[invites]) . "\" />", 1);
			}
		}
		tr($lang_userdetails['row_passkey'], "<input name=\"resetkey\" value=\"yes\" type=\"checkbox\" />" . $lang_userdetails['checkbox_reset_passkey'], 1);

		print("<tr><td class=\"toolbox\" colspan=\"2\" align=\"center\"><input type=\"submit\" class=\"btn\" value=\"" . $lang_userdetails['submit_okay'] . "\" /></td></tr>\n");
		print("</table>\n");
		print("</form>\n");
		end_frame();
		if (get_user_class() >= $cruprfmanage_class) {
			begin_frame("封禁、删除账号", true);
			print("<form method=\"post\" action=\"delacctadmin.php\" name=\"deluser\">
		<input name=\"userid\" size=\"10\" type=\"hidden\" value=\"" . $user["id"] . "\" />
		是否允许<input name=\"enabled\" value=\"yes\" type=\"radio\"" . ($enabled ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_yes'] . "<input name=\"enabled\" value=\"no\" type=\"radio\"" . (!$enabled ? " checked=\"checked\"" : "") . " />" . $lang_userdetails['radio_no'] . "<br/>
		<input name=\"changedisable\"  value='yes' id=\"changedisable\" type=\"checkbox\"  onclick=\"if (this.checked) {enabledel('如果确信的话，你可以更改用户的enable属性');}else{disabledel();}\" />封禁或解封用户<br/>
		<input name=\"delenable\" value='yes' id=\"delenable\" type=\"checkbox\" onclick=\"if (this.checked) {enabledel('" . $lang_userdetails['js_delete_user_note'] . "');}else{disabledel();}\" />删除用户<br/>
		<input name=\"opreason\" type=\"text\" />封禁或解封或删除原因。请认真填写，将显示在封禁日志！<br/>
		<input name=\"submit\" type=\"submit\" value=\"执行\" disabled=\"disabled\" /></form>");
			end_frame();
		}
	}
	end_main_frame();
	stdfoot();
