<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="UTF-8"/>
		<title>图片上传</title>
		<link href="//cdn.css.net/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
			<link href="jquerylib/image_upload/fileinput.css" media="all" rel="stylesheet" type="text/css" />
			<script src="//cdn.css.net/libs/jquery/2.1.4/jquery.min.js"></script>
			<script src="jquerylib/image_upload/fileinput.min.js" type="text/javascript"></script>
			<script src="jquerylib/image_upload/fileinput_locale_zh.js" type="text/javascript"></script>
			<script src="//cdn.css.net/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js" type="text/javascript"></script>
			<style>
				html{position:relative;min-height:100%}
				body{margin-bottom:60px}
				.footer{position:absolute;bottom:0;width:100%;height:60px;background-color:#f5f5f5}
				.container .text-muted{margin:20px 0}
				.footer > .container{padding-right:15px;padding-left:15px}
				code{font-size:80%}
			</style>
	</head>
	<body>
		<div class="container kv-main">
			<div class="page-header">单文件最大10M；最多可同时上传10个文件</div>
			<p class="text-muted">禁止上传违反中国大陆和港澳台法律的图片，禁止上传政治敏感和色情图片，包括但不限于以上，违者后果自负。</p>
			<div id="showurl" style="display: none;">
				<!--<pre><code id="bbcode"></code></pre>-->
				<pre><textarea id="bbcode" style="height:100px;width:100%" onmouseover="this.select()"></textarea></pre>
			</div>
			<form enctype="multipart/form-data">
				<div class="form-group">
					<input id="smfile" type="file" multiple class="file" data-overwrite-initial="false" data-min-file-count="1" data-max-file-count="10" name="smfile" accept="image/*">
				</div>
			</form>
			<script>
				$("#smfile").fileinput({
					uploadUrl: 'https://sm.ms/api/upload?inajax=1',
					allowedFileExtensions: ['jpeg', 'jpg', 'png', 'gif', 'bmp'],
					overwriteInitial: false,
					maxFileSize: 10240,
					maxFilesNum: 10
				});
				$('#smfile').on('fileuploaded', function (event, data, previewId, index) {
					var form = data.form, files = data.files, extra = data.extra, response = data.response, reader = data.reader;
					if (response.code == 'success') {
						if ($("showurl").css("display")) {
							$('#bbcode').append("[img]" + response.data.url + "[/img]" + "\n");
						} else if (response.data.url) {
							$("#showurl").show();
							$('#bbcode').append("[img]" + response.data.url + "[/img]" + "\n");
						}
					}
				});
			</script>
		</div>
	</body>
</html>