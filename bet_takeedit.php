<?php

require "include/bittorrent.php";
dbconn(false);
loggedinorreturn();
if ($CURUSER["class"] < UC_MODERATOR && $CURUSER['id'] != $betsadmin)
	stderr("错误", "权限不足");
$id = isset($_POST['id']) && is_valid_id($_POST['id']) ? $_POST['id'] : 0;
$heading = htmlspecialchars($_POST['heading']);
$heading = str_replace("'", "", $heading);
$undertext = htmlspecialchars($_POST['undertext']);
$undertext = str_replace("'", "", $undertext);
//$endtime = (int) $_POST['endtime'] + time();
$endtime = strtotime($_POST['endtime']);
$sort = (int) $_POST['sort'];
$res = "UPDATE betgames SET heading =" . sqlesc($heading) . ", undertext=" . sqlesc($undertext) . ", endtime=" . sqlesc($endtime) . ", sort=" . sqlesc($sort) . " WHERE id =" . sqlesc($id) . "";
sql_query($res) or sqlerr(__FILE__, __LINE__);
header("location: bet_admin.php");
