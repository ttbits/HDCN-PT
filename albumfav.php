<?php

require "include/bittorrent.php";
dbconn();

//Send some headers to keep the user's browser from caching the response.
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-Type: text/xml; charset=utf-8");

$albumid = 0 + $_GET['albumid'];
if (isset($CURUSER)) {
	$res_bookmark = sql_query("SELECT * FROM albumfav WHERE id=" . sqlesc($albumid) . " AND userid=" . sqlesc($CURUSER[id]));
	if (mysql_num_rows($res_bookmark) == 1) {
		sql_query("DELETE FROM albumfav WHERE id=" . sqlesc($albumid) . " AND userid=" . sqlesc($CURUSER['id'])) or sqlerr(__FILE__, __LINE__);
		$Cache->delete_value('user_' . $CURUSER['id'] . '_album_array');
		echo "deleted";
	} else {
		$row = mysql_fetch_array(sql_query("SELECT name, small_descr FROM album WHERE id = $albumid")) or sqlerr(__FILE__, __LINE__);
		sql_query("INSERT INTO albumfav (id, userid, name, small_descr) VALUES (" . sqlesc($albumid) . "," . sqlesc($CURUSER['id']) . "," . sqlesc($row['name']) . "," . sqlesc($row['small_descr']) . ")") or sqlerr(__FILE__, __LINE__);
		$Cache->delete_value('user_' . $CURUSER['id'] . '_album_array');
		echo "added";
	}
} else
	echo "failed";
