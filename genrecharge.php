<?php
require_once("include/bittorrent.php");
dbconn();
loggedinorreturn();
parked();
if ($CURUSER['class'] < UC_MODERATOR) {
	stderr("抱歉...", "您的等级太低");
}

stdhead("充值卡密生成");
?>
<script type="text/javascript">
	function checkAll()
	{
		$(".checkbox").each(function () {
			this.checked = true;
		});
	}

	function reverseCheck()
	{
		$(".checkbox").each(function () {
			this.checked = !this.checked;
		});
	}
</script>
<div>
	<form method="POST">
		<?php echo "可使用次数：" ?><input type="text" name="num" style="width: 100px" onkeyup="this.value = this.value.replace(/\D/g, '');" />
		<?php echo "充值卡面值：" ?><input type="text" name="bonus" style="width: 100px" onkeyup="this.value = this.value.replace(/\D/g, '');" />
		<?php echo "过期时间：" ?><input type="text" name="date"style="width: 100px" class="Wdate" onfocus="WdatePicker({isShowWeek: 'true', minDate: '%y-%M-{%d+1}'})" />
		<?php echo "受赠用户：" ?><input type="text" name="users" style="width: 100px" onkeyup="this.value = this.value.replace(/\D/g, '');" />
		<input type="submit" name="submit" value="生成" /><br /><br />
		<?php echo "可使用次数空或'0'为不限制使用次数；充值卡面值空或'0'为默认1000魔力值；过期时间空或'0'为永不过期；受赠用户请填写用户ID，填写后只有该ID用户才可使用" ?>
	</form>
</div>
<?php
if ($_POST['num'] == '' || $_POST['num'] == 0) {
	$gennum = '-1';
} else {
	$gennum = $_POST['num'];
}
if ($_POST['bonus'] == '' || $_POST['bonus'] == 0) {
	$genbonus = '1000';
} else {
	$genbonus = $_POST['bonus'];
}
if ($_POST['date'] == '' || $_POST['date'] == 0) {
	$gendate = "0000-00-00";
} else {
	$gendate = $_POST['date'];
}
if ($_POST['users'] == '' || $_POST['users'] == 0) {
	$genusers = "0";
} else {
	$genusers = $_POST['users'];
}
if ($_POST['submit']) {
	foreach (generate() as $arr) {
		sql_query("INSERT INTO recharge (users, cards, num, date, bonus) VALUE ('$genusers', '$arr', '$gennum', '$gendate', '$genbonus')") or sqlerr(__FILE__, __LINE__);
	}
	echo "<script>alert('生成成功！');location.href='genrecharge.php';</script>";
}

function begin_table_genrecharge($fullwidth = false, $padding = 5) {
	$width = "";
	if ($fullwidth)
		$width .= " width=50%";
	return("<table class='main" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'><tr align=\"center\"><form method=\"POST\" onsubmit=\"if(confirm('确定要删除吗？')){return true;}else{return false;}\"></tr>");
}

function end_table_genrecharge() {
	return("<tr align=\"center\"><td class=\"rowfollow\" colspan=\"10\"><input type=\"button\" value=\"全选\" onclick=\"checkAll();\"><input type=\"button\" value=\"反选\" onclick=\"reverseCheck();\"> | <input type=\"submit\" id=\"delete\" value=\"删除\"></form></td></tr></table>");
}

function begin_frame_genrecharge($caption = "", $center = false, $padding = 5, $width = "100%", $caption_center = "left") {
	$tdextra = "";
	if ($center)
		$tdextra .= " align='center'";
	return(($caption ? "<h2 align='" . $caption_center . "'>" . $caption . "</h2>" : "") . "<table width='" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'>" . "<tr><td class='text' $tdextra>");
}

function end_frame_genrecharge() {
	return("</td></tr></table>");
}

function bjtable_genrecharge($res, $frame_caption) {
	$htmlout = '';
	$htmlout .= begin_frame_genrecharge($frame_caption, true);
	$htmlout .= begin_table_genrecharge();
	$htmlout .="<tr><td class='colhead'>序号</td><td class='colhead' align='left'>充值卡密</td><td class='colhead' align='right'>可使用次数</td><td class='colhead' align='right'>已使用次数</td><td class='colhead' align='right'>充值卡面值</td><td class='colhead' align='right'>过期时间</td><td class='colhead' align='right'>受赠用户</td><td class='colhead' align='right'>行为</td></tr>";
	$num = 0;
	while ($a = mysql_fetch_assoc($res)) {
		++$num;
		$number = get_row_count("rechargelog", "WHERE cards = '" . $a['cards'] . "'");
		$htmlout .="<tr class='torrent_table'><td>$num</td><td align='left'>" .
				"<b>" . $a['cards'] . "</b></td>" .
				"<td align='right'>" . ($a['num'] == '-1' ? '∞' : $a['num']) . "</td>" .
				"<td align='right'>$number</td>" .
				"<td align='right'>" . round($a['bonus'], 3) . "</td>" .
				"<td align='right'>" . $a['date'] . "</td>" .
				"<td align='center'>" . ($a['users'] == 0 ? "无" : get_username($a['users'])) . "</td>" .
				"<td class=\"rowfollow\"><input type=\"checkbox\" value=\"{$a['id']}\" name=\"id[]\" class=\"checkbox\"></td>" .
				"</tr>";
	}
	$htmlout .= end_table_genrecharge();
	$htmlout .= end_frame_genrecharge();
	return $htmlout;
}

$count = get_row_count("recharge");
list($pagertop, $pagerbottom, $limit) = pager(25, $count, "?");
$HTMLOUT .="<h1 align='center'>充值卡统计</h1>";
$res = sql_query("SELECT * FROM recharge ORDER BY id DESC $limit") or sqlerr(__FILE__, __LINE__); //降序排列，最新生成的最先显示
$HTMLOUT .= bjtable_genrecharge($res, "所有充值卡", "Users");
$HTMLOUT .="<br /><br />";
if ($count) {
	print($pagertop);
	print $HTMLOUT;
	print($pagerbottom);
} else {
	print $HTMLOUT;
}

if ($_POST['id'] != '') {
	$id = implode(",", $_POST['id']);
	sql_query("DELETE FROM recharge WHERE id IN (" . $id . ")") or sqlerr(__FILE__, __LINE__);
	echo "<script>location.href='genrecharge.php';</script>";
}
stdfoot();
