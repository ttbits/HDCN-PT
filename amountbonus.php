<?php
require "include/bittorrent.php";
dbconn();
loggedinorreturn();
if (get_user_class() < UC_ADMINISTRATOR)
	stderr("出错啦！", "权限不足");

function bark($msg) {
	stdhead();
	stdmsg("提示信息", $msg);
	stdfoot();
	exit;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if (get_user_class() < UC_ADMINISTRATOR)
		stderr("出错啦！", "权限不足");

	if ($_POST['wage'] == 'yes') {
		$point = $_POST['amount'];
		$class = $_POST['class'];
		if (!is_numeric($point))
			stderr("别玩了！", "填写的魔力值不是正常的数值！");
		if ($point <= 0)
			stderr("别玩了！", "工资怎么会是负数呢？");
		$dt = sqlesc(date("Y-m-d H:i:s"));
		$reason_2 = $_POST["reason_2"];

		if ($class == "forummods") {//论坛版主
			$classname = "论坛版主";
			$query = sql_query("SELECT userid FROM forummods GROUP BY userid");
			while ($dat = mysql_fetch_assoc($query)) {
				sql_query("UPDATE users SET seedbonus = seedbonus + $point WHERE id=$dat[userid]");
				if (mysql_num_rows(sql_query("SELECT * FROM users WHERE id=$dat[userid]")) > 0)
					sql_query("INSERT INTO messages (sender, receiver, added,  subject, msg) VALUES (0, $dat[userid], $dt, '福利来啦！', '管理组为所有 $classname 发放了 $point 个魔力值，请笑纳！原因是：{$reason_2}')") or sqlerr(__FILE__, __LINE__);
				writeBonusComment($dat[userid], "魔力值增加 {$point}，原因是：{$reason_2}");
			}
		} elseif ($class == "picker") {//美工
			$classname = "美工/技术组";
			$query = sql_query("SELECT id FROM users WHERE picker = 'yes'");
			while ($dat = mysql_fetch_assoc($query)) {
				sql_query("UPDATE users SET seedbonus = seedbonus + $point WHERE id=" . $dat['id']);
				if (mysql_num_rows(sql_query("SELECT * FROM users WHERE id=" . $dat['id'])) > 0)
					sql_query("INSERT INTO messages (sender, receiver, added,  subject, msg) VALUES (0, $dat[id], $dt, '福利来啦！', '管理组为所有 $classname 发放了 $point 个魔力值，请笑纳！原因是：{$reason_2}')") or sqlerr(__FILE__, __LINE__);
				writeBonusComment($dat['id'], "魔力值增加 {$point}，原因是：{$reason_2}");
			}
		} elseif ($class == "support") {//客服
			$classname = "客服";
			$query = sql_query("SELECT id FROM users WHERE support = 'yes'");
			while ($dat = mysql_fetch_assoc($query)) {
				sql_query("UPDATE users SET seedbonus = seedbonus + $point WHERE id=" . $dat['id']);
				if (mysql_num_rows(sql_query("SELECT * FROM users WHERE id=" . $dat['id'])) > 0)
					sql_query("INSERT INTO messages (sender, receiver, added,  subject, msg) VALUES (0, $dat[id], $dt, '福利来啦！', '管理组为所有 $classname 发放了 $point 个魔力值，请笑纳！原因是：{$reason_2}')") or sqlerr(__FILE__, __LINE__);
				writeBonusComment($dat['id'], "魔力值增加 {$point}，原因是：{$reason_2}");
			}
		} elseif ($class == "all") {
			$classname = "用户";
			$query = sql_query("SELECT id FROM users");
			while ($dat = mysql_fetch_assoc($query)) {
				sql_query("UPDATE users SET seedbonus = seedbonus + $point WHERE id=" . $dat['id']);
				if (mysql_num_rows(sql_query("SELECT * FROM users WHERE id=" . $dat['id'])) > 0)
					sql_query("INSERT INTO messages (sender, receiver, added,  subject, msg) VALUES (0, $dat[id], $dt, '福利来啦！', '管理组为所有 $classname 发放了 $point 个魔力值，请笑纳！原因是：{$reason_2}')") or sqlerr(__FILE__, __LINE__);
				writeBonusComment($dat['id'], "魔力值增加 {$point}，原因是：{$reason_2}");
			}
		}

		else {
			$classname = ($class == 11 ? "客卿" : ($class == 12 ? "致仕" : ($class == 13 ? "保种员" : ($class == 14 ? "转载员" : ($class == 15 ? "发布员" : ($class == 16 ? "带刀侍卫" : ($class == 17 ? "太监总管" : "禁军统领")))))));
			sql_query("UPDATE users SET seedbonus = seedbonus + $point WHERE class = $class");
			$query = sql_query("SELECT id FROM users WHERE class  = $class");

			while ($dat = mysql_fetch_assoc($query)) {
				sql_query("INSERT INTO messages (sender, receiver, added,  subject, msg) VALUES (0, $dat[id], $dt, '福利来啦！', '管理组为所有 $classname 发放了 $point 个魔力值，请笑纳！原因是：{$reason_2}')") or sqlerr(__FILE__, __LINE__);
				writeBonusComment($dat[id], "魔力值增加 {$point}，原因是：{$reason_2}");
			}
		}
		stderr("成功", "成功给所有 $classname 发放 $point 个魔力值");
		die;
	}

	if ($_POST["username"] == "" || $_POST["seedbonus"] == "" || $_POST["seedbonus"] == "")
		stderr("错误！", "丢失了重要数据");
	$reason_1 = $_POST['reason_1'];
	$username = explode(",", $_POST["username"]);
	$count = count($username);
	for ($i = 0; $i < $count; $i++) {
		$username[$i] = sqlesc($username[$i]);
	}
	$seedbonus = sqlesc($_POST["seedbonus"]);
	for ($i = 0; $i < $count; $i++) {
		$arr = mysql_fetch_assoc(sql_query("SELECT id, bonuscomment FROM users WHERE username = {$username[$i]}"));
		if (!$arr) {
			echo "<script type='text/javascript'>alert('用户名出错，请重新输入！');history.go(-1) </script>";
			die;
		}
	}
	for ($i = 0; $i < $count; $i++) {
		$arr = mysql_fetch_assoc(sql_query("SELECT id, bonuscomment FROM users WHERE username = {$username[$i]}"));
		$bonuscomment = $arr[bonuscomment];
		$bonuscomment = date("Y-m-d") . " " . ($seedbonus >= 0 ? $seedbonus : -$seedbonus) . " 个魔力值 " . ($seedbonus >= 0 ? "增加" : "失去") . "，来自：" . $CURUSER["username"] . "\n" . htmlspecialchars($bonuscomment);
		sql_query("UPDATE users SET seedbonus = seedbonus + $seedbonus, bonuscomment = " . sqlesc($bonuscomment) . " WHERE username = {$username[$i]}") or sqlerr(__FILE__, __LINE__);
		$dt = date("Y-m-d H:i:s");
		if ($seedbonus > 0)
			$bonuschangedmsg = "您的魔力值被增加了[color=red][b]" . abs($seedbonus) . "[/b][/color]个，";
		elseif ($seedbonus < 0)
			$bonuschangedmsg = "您的魔力值被减少了[color=red][b]" . abs($seedbonus) . "[/b][/color]个，";
		else
			$bonuschangedmsg = "魔力值好像没有变化，今天不会是4月1日吧？！";
		if ($reason_1) {
			$bonuschangedmsg .= chr(10) . chr(10) . "原因是：" . $reason_1;
		}
		$bonuschangedmsg .= chr(10) . chr(10) . "操作人：" . ($CURUSER['class'] == UC_STAFFLEADER ? "机器人" : $CURUSER['username']);
		sql_query("INSERT INTO messages (sender, receiver, added, subject, msg) VALUES ( 0, '{$arr['id']}', '$dt', '魔力值变动', '$bonuschangedmsg'  )") or sqlerr(__FILE__, __LINE__);
		$res = mysql_fetch_row(sql_query("SELECT id FROM users WHERE username = {$username[$i]}"));
		if (!$res) {
			stderr("错误", "无法更新到账户");
			header("Location: " . get_protocol_prefix() . "$BASEURL/userdetails.php?id=" . htmlspecialchars($arr[0]));
			die;
		}
	}
	echo "<script type='text/javascript'>alert('操作成功，点确定返回上个页面');history.go(-1) </script>";
	die();
}
stdhead("添加魔力值");
?>
<h1>添加魔力值</h1>
<?php
begin_main_frames("", true, 500);
begin_main_frames("给指定用户发放魔力值", true, 500);
?>
<form method="post" action="amountbonus.php">
	<table width=100%>
		<tr><td class="rowhead">用户：</td><td class="rowfollow"><textarea type="text" name="username" style="width: 70%"></textarea>(多个用户用","隔开)</td></tr>
		<tr><td class="rowhead">数量：</td><td class="rowfollow"><input type="text" name="seedbonus" /></td></tr>
		<tr><td class="rowhead">原因：</td><td class="rowfollow"><textarea name="reason_1" style="width: 70%"></textarea></td></tr>
		<tr><td colspan="2" class="toolbox" align="center"><input type="submit" value="确定" class="btn" /></td></tr>
	</table>
</form>
<?php
end_main_frame();
if (get_user_class() >= UC_ADMINISTRATOR) {
	begin_main_frames("发工资", true, 500);
	?>
	<form action="amountbonus.php" method="post" >
		<table width=100%>
			<tr><td class="rowfollow" width="100%">
					给
					<select name="class">
						<option value="all">所有用户</option>
						<option value=11>客卿</option>
						<option value=12>致仕</option>
						<option value=13>保种员</option>
						<option value=14>转载员</option>
						<option value=15 selected="selected">发布员</option>
						<option value=16>带刀侍卫</option>
						<option value=17>太监总管</option>
						<option value=18>皇上</option>
						<option value="forummods">论坛版主</option>
						<option value="picker">美工/技术组</option>
						<option value="support">客服</option>
					</select>
					等级用户增加<input name="amount" value="5000"/>个魔力值
				</td>
			</tr>
			<tr><td class="rowfollow">原因：<textarea style="width: 85%" name="reason_2"></textarea></td></tr>
			<tr><td class="toolbox" align="center"><input type="hidden" name="wage" value="yes" /><input type="submit" class="btn" value="确定" /></td></tr>
		</table>
	</form>
	<?php
	end_main_frame();
}
end_main_frame();
stdfoot();
