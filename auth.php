<?php
require_once("include/bittorrent.php");
require_once('classes/secken.class.php');
dbconn();

// Create an API object using your credentials
$secken_api = new secken($appid, $appkey, $authid);

# Step 1 - Get an qrcode for binding
// 可选验证类型码：1(点击确定按钮，默认)、2(使用手势密码)、3(人脸验证)、4(声音验证)
$ret = $secken_api->getAuth($authtype);

//$ret = $secken_api->realtimeAuth('uid', 1, 1, 'https://callback.com/path', '123.123.123.123', '名字');
//$ret = $secken_api->offline_auth('2121','sd');
# Step 2 - Check the returned result
/*
  if ($secken_api->getCode() != 200) {
  //var_dump($secken_api->getCode(), $secken_api->getMessage());
  echo json_encode($secken_api);
  }
 * 不打印失败错误
 */
if ($secken_api->getCode() == 200) {
	$url = $ret['qrcode_url'];
	$event_id = $ret['event_id'];
}
stdhead("扫码登录");
?>
<div style="width: 600px; margin-top: 20px; margin-bottom: 20px">
	<div style="float: left; width: 45%; margin-top: 20px; margin-bottom: 20px">
		<p>请使用洋葱手机APP “安全扫一扫” 登录</p>
		<img style="width: 249px; height: 249px; border: 1px solid #000" src="<?= $url ?>" title="扫码安全登录" /><br />
		<a href="auth.php"><b style="font-size: 8pt">二维码每60秒失效，请点此刷新</b></a>
	</div>
	<div style="float: right; width: 45%; text-align: left">
		<p style="font-size: 20pt;">什么是扫码登录？</p>
		<p style="font-size: 12pt;">扫码登录是<?= $SITENAME ?>和洋葱安全验证合作推出的一个基于手机APP的登录安全验证服务。</p>
		<a href="//www.yangcong.com/download" target="_blank" title="下载洋葱手机APP"><img src="//dn-yangcong.qbox.me/images/outer/download/download_qr.png" style="height: 135px; width: 135px" /></a>
	</div>
</div>
<?php
$returnto = $_GET['returnto'];
if ($returnto != '') {
	$returnto = "&returnto=" . $returnto;
} else {
	$returnto = '';
}
?>
<script type="text/javascript">
	$(function () {
		var event_id = "<?= $event_id ?>";
		var returnto = "<?= $returnto ?>";
		ycqrt = setInterval(function () {
			$.getJSON('callback.php?event_id=' + event_id, function (data) {
				if (data.status == '200') {
					location.href = 'binding.php?secken=' + data.uid + returnto;
				}
				switch (data.code) {
					case 601:
						alert("你拒绝授权登录");
						location.href = 'auth.php?r=1' + returnto;
						break;
					case 603:
						alert("响应超时");
						location.href = 'auth.php?r=1' + returnto;
						break;
						/*
						 case 604:
						 alert("二维码失效");
						 location.href = 'auth.php?r=1' + returnto;
						 break;
						 */
					case 607:
						alert("该用户不存在");
						location.href = 'auth.php?r=1' + returnto;
						break;
					default:
						break;
				}
			});
		}, 2000);
	});
</script>
<?php
stdfoot();
