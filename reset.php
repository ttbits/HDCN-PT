<?php
require "include/bittorrent.php";
dbconn();
loggedinorreturn();
require_once(get_langfile_path());
if (get_user_class() < UC_ADMINISTRATOR)
	stderr("错误", "权限不足");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$username = trim($_POST["username"]);
	$newpassword = trim($_POST["newpassword"]);
	$newpasswordagain = trim($_POST["newpasswordagain"]);

	if (empty($username) || empty($newpassword) || empty($newpasswordagain))
		stderr("错误", "不要留下任何空的填写项目");

	if ($newpassword != $newpasswordagain)
		stderr("错误", "两次输入的密码不一致");

	if (strlen($newpassword) < 6)
		stderr("错误", "密码最少为6位");

	$res = sql_query("SELECT * FROM users WHERE username=" . sqlesc($username) . " ") or sqlerr();
	$arr = mysql_fetch_assoc($res);
	$id = $arr['id'];
	$wantpassword = $newpassword;
	$secret = mksecret();
	$wantpasshash = md5($secret . $wantpassword . $secret);
	sql_query("UPDATE users SET passhash=" . sqlesc($wantpasshash) . ", secret= " . sqlesc($secret) . " where id=$id");
	write_log("$username 的密码被 $CURUSER[username] 重置");
	if (mysql_affected_rows() != 1)
		stderr("错误", "无法为该帐号重置密码");
	stderr("成功", "$username 的密码已经成功重置", false);
}
stdhead("重置用户密码");
?>
<table border=1 cellspacing=0 cellpadding=5>
	<form method=post>
		<tr><td class=colhead align="center" colspan=2><?php echo $lang_reset['head_reset'] ?></td></tr>
		<tr><td class=rowhead align="right"><?php echo $lang_reset['text_username'] ?></td><td class=rowfollow><input size=40 name=username></td></tr>
		<tr><td class=rowhead align="right"><?php echo $lang_reset['text_passwd'] ?></td><td class=rowfollow><input type="password" size=40 name=newpassword><br /><font class=small><?php echo $lang_reset['text_minimum'] ?></font></td></tr>
		<tr><td class=rowhead align="right"><?php echo $lang_reset['text_repasswd'] ?></td><td class=rowfollow><input type="password" size=40 name=newpasswordagain></td></tr>
		<tr><td class=toolbox colspan=2 align="center"><input type=submit class=btn value='<?php echo $lang_reset['submit_reset'] ?>'></td></tr>
	</form>
</table>
<?php
stdfoot();
