<?php

require "include/bittorrent.php";
require "memcache.php";
dbconn();
loggedinorreturn();
parked();
//print_r("<script type=\"text/javascript\" >if(confirm(\"本次改名将花费，请确认！\")){}else{window.history.back(-1);}</script>");
stdhead($CURUSER['username'] . $lang_mybonus['head_karma_page']);
$action = htmlspecialchars($_POST['action']);


$changenamecharge = 10000; //默认改名价格
$changenamechargemax = 100000; //max
$ycharge = array(); //预设价格
/*
 * 日期是否为1-7日
 * 是->月份是否为10月，是->置顶打0.1折，其他打0.01折；否->不打折
 * 否->不打折
 */
if ((int) date("d") == 01 || (int) date("d") == 02 || (int) date("d") == 03 || (int) date("d") == 04 || (int) date("d") == 05 || (int) date("d") == 06 || (int) date("d") == 07) {
	if ((int) date("m") == 10) {
		$discount = 1 / 100; //折扣
		$ycharge['top'] = 4000 * 10 * $discount; //置顶
	} else {
		$discount = 100 / 100; //折扣
		$ycharge['top'] = 4000 * $discount; //置顶
	}
} else {
	$discount = 100 / 100; //折扣
	$ycharge['top'] = 4000 * $discount; //置顶
}
$ycharge['setallfree'] = 2000 * $discount; //Free
$ycharge['setall2up'] = 2000 * $discount; //2x
$ycharge['setall2up_free'] = 5000 * $discount; //2xFree
$ycharge['setallhalf_down'] = 1000 * $discount; //50%
$ycharge['setall2up_half_down'] = 3500 * $discount; //2x50%
$ycharge['setall30_down'] = 1500 * $discount; //30%

if (!$action) {
	print_r("<script type=\"text/javascript\" >alert(\"参数错误，返回前一个页面\");window.history.back(-1);</script>");
	echo "参数错误";
}
$userid = $CURUSER['id'];
$userbouns = (int) $CURUSER['seedbonus'];
// changename
if ($action == "changename") {
	$charge = -1;
	$newname = htmlspecialchars($_POST['newname']);
	$oldname = $CURUSER['username'];
	if (utf8_strlen($newname) > 14 || utf8_strlen($newname) < 4 || !validusername($newname)) {
		echo "名字不符合要求";
		die();
	}
	if ($newname == $oldname) {
		echo "新旧用户名一样，无需更改";
		die();
	}

	if ($res = sql_query("SELECT namecharge FROM bonusapp WHERE userid = $userid")or sqlerr(__FILE__, __LINE__)) {
		$row = mysql_fetch_array($res);
		$charge = $row['namecharge'];
	}
	if ($charge <= 0) {
		$charge = $changenamecharge;
		if (preg_match("/^[\d][\d]*[\d]$/", $oldname)) {
			$charge = $charge / 2;
		}
		if ($charge > $userbouns) {
			echo "魔力值不足，攒点魔力值再来吧^_^";
			die();
		}
		if (sql_query("UPDATE users SET username = '$newname' WHERE id = $userid")) {
			$res = mysql_fetch_array(sql_query("SELECT secken FROM users WHERE id = $userid"));
			if ($res['secken'] == 'yes') {
				sql_query("UPDATE seckenapi SET username = '$newname' WHERE id = $userid") or sqlerr(__FILE__, __LINE__);
			}
			sql_query("UPDATE users SET seedbonus = seedbonus - $charge WHERE id = $userid") or sqlerr(__FILE__, __LINE__);
			echo "改名成功，本次消费魔力值" . $charge . "，欢迎再次光临^_^";
			$charge = $charge * 2;
			sql_query("INSERT INTO bonusapp (userid, namecharge) VALUES (" . $userid . ", " . $charge . ")") or sqlerr(__FILE__, __LINE__);
		} else {
			echo "修改失败，该用户名可能已被占用";
			die();
		}
	} else {
		if ($charge > $changenamechargemax) {
			$charge = $changenamechargemax;
		}
		if (preg_match("/^[\d][\d]*[\d]$/", $oldname)) {
			$charge = $charge / 2;
		}
		if ($charge > $userbouns) {
			echo "魔力值不足，攒点魔力值再来吧^_^";
			die();
		}
		if (sql_query("UPDATE users SET username ='$newname' WHERE id = $userid")) {
			$res = mysql_fetch_array(sql_query("SELECT secken FROM users WHERE id = $userid"));
			if ($res['secken'] == 'yes') {
				sql_query("UPDATE seckenapi SET username = '$newname' WHERE id = $userid") or sqlerr(__FILE__, __LINE__);
			}
			sql_query("UPDATE users SET seedbonus = seedbonus - $charge WHERE id = $userid") or sqlerr(__FILE__, __LINE__);
			echo "改名成功，本次消费魔力值" . $charge . "，欢迎再次光临^_^";
			$charge = $charge * 2;
			sql_query("UPDATE bonusapp SET namecharge = $charge WHERE userid = $userid") or sqlerr(__FILE__, __LINE__);
		} else {
			echo "修改失败，该用户名可能已被占用";
			die();
		}
	}
	$charge = $charge / 2;
	sendshoutbox("薄情寡义的 [@$CURUSER[username]] 不喜欢自己的名字啦，花了 $charge 个魔力值给自己换了个新名字叫 [@$newname] 快来围观土豪吧¬_¬");
	writeBonusComment($CURUSER[id], "用户名由 $CURUSER[username] 更换为 {$newname}，花费魔力值 $charge");
	writeModComment($CURUSER[id], "用户名由 $CURUSER[username] 更换为 {$newname}，花费魔力值 $charge");
	record_op_log(0, $CURUSER[id], htmlspecialchars($CURUSER['username']), "change", $CURUSER['username'] . "--花费魔力值自助更名为--" . $newname);
	print("<br>建议重新登录，<a href=\"logout.php\" align=\"center\">点我去登录</a>");
}

function checktorrent($torrentid) {
	$row = mysql_fetch_array(sql_query("SELECT * FROM torrents WHERE id = $torrentid"));
	if ($row['pos_state'] == 'sticky')
		die('抱歉，不允许对置顶的种子进行编辑');
	return $row;
}

if ($action == "prm") {//促销
	$id = htmlspecialchars($_POST['torrentid']);
	$prmtype = htmlspecialchars($_POST['type']);
	$time = htmlspecialchars($_POST['time']);
	if (!preg_match("/^[\d]{1,8}$/", $id) || !preg_match("/^[\d]{1,8}$/", $time) || $prmtype < '2' || $prmtype > '8') {
		echo "再玩 BAN 了你";
		die();
	}
	$torrentid = (int) $id;
	$prmtime = (int) $time;
	$record = checktorrent($torrentid);
	if (!$record) {
		echo "不要玩了，没有该ID的种子";
		die();
	}
	if ((((int) $record['sp_state']) == 2 || ((int) $record['sp_state']) == 4) && ((int) $record['endfree']) == 0) {
		if ($prmtype != "8") {
			echo "你在玩我么，该种子已经永久免费了";
			die();
		}
	}
	$charge = 0;
	if ($prmtype == "2") {
		$charge = ((int) $prmtime) * $ycharge['setallfree'];
		if ($charge > $userbouns) {
			echo "魔力值不足，攒点魔力值再来吧^_^";
			die();
		}
		if (sql_query("UPDATE torrents SET sp_state = 2 WHERE id = $torrentid")) {
			echo $info1 = $CURUSER['username'] . " 花费了 " . $charge . " 魔力值，种子 " . $record['name'] . " 设置为【Free】 " . $prmtime . "天";
			$info = $CURUSER['username'] . " 花费了 " . $charge . " 魔力值，种子[url=details.php?id=$torrentid] " . $record['name'] . " [/url]设置为【Free】 " . $prmtime . "天";
		}
	} else if ($prmtype == "3") {
		$charge = ((int) $prmtime) * $ycharge['setall2up'];
		if ($charge > $userbouns) {
			echo "魔力值不足，攒点魔力值再来吧^_^";
			die();
		}
		if (sql_query("UPDATE torrents SET sp_state = 3 WHERE id = $torrentid")) {
			echo $info1 = $CURUSER['username'] . " 花费了 " . $charge . " 魔力值，种子 " . $record['name'] . " 设置为【2x】 " . $prmtime . "天";
			$info = $CURUSER['username'] . " 花费了 " . $charge . " 魔力值，种子[url=details.php?id=$torrentid] " . $record['name'] . " [/url]设置为【2x】 " . $prmtime . "天";
		}
	} else if ($prmtype == "4") {
		$charge = ((int) $prmtime) * $ycharge['setall2up_free'];
		if ($charge > $userbouns) {
			echo "魔力值不足，攒点魔力值再来吧^_^";
			die();
		}
		if (sql_query("UPDATE torrents SET sp_state = 4 WHERE id = $torrentid")) {
			echo $info1 = $CURUSER['username'] . " 花费了 " . $charge . " 魔力值，种子 " . $record['name'] . " 设置为【2xFree】 " . $prmtime . "天";
			$info = $CURUSER['username'] . " 花费了 " . $charge . " 魔力值，种子[url=details.php?id=$torrentid] " . $record['name'] . " [/url]设置为【2xFree】 " . $prmtime . "天";
		}
	} else if ($prmtype == "5") {
		$charge = ((int) $prmtime) * $ycharge['setallhalf_down'];
		if ($charge > $userbouns) {
			echo "魔力值不足，攒点魔力值再来吧^_^";
			die();
		}
		if (sql_query("UPDATE torrents SET sp_state = 5 WHERE id = $torrentid")) {
			echo $info1 = $CURUSER['username'] . " 花费了 " . $charge . " 魔力值，种子 " . $record['name'] . " 设置为【50%】" . $prmtime . "天";
			$info = $CURUSER['username'] . " 花费了 " . $charge . " 魔力值，种子[url=details.php?id=$torrentid] " . $record['name'] . " [/url]设置为【50%】 " . $prmtime . "天";
		}
	} else if ($prmtype == "6") {
		$charge = ((int) $prmtime) * $ycharge['setall2up_half_down'];
		if ($charge > $userbouns) {
			echo "魔力值不足，攒点魔力值再来吧^_^";
			die();
		}
		if (sql_query("UPDATE torrents SET sp_state = 6 WHERE id = $torrentid")) {
			echo $info1 = $CURUSER['username'] . " 花费了 " . $charge . " 魔力值，种子 " . $record['name'] . " 设置为【2x50%】 " . $prmtime . "天";
			$info = $CURUSER['username'] . " 花费了 " . $charge . " 魔力值，种子[url=details.php?id=$torrentid] " . $record['name'] . " [/url]设置为【2x50%】 " . $prmtime . "天";
		}
	} else if ($prmtype == "7") {
		$charge = ((int) $prmtime) * $ycharge['setall30_down'];
		if ($charge > $userbouns) {
			echo "魔力值不足，攒点魔力值再来吧^_^";
			die();
		}
		if (sql_query("UPDATE torrents SET sp_state = 7 WHERE id = $torrentid")) {
			echo $info1 = $CURUSER['username'] . " 花费了 " . $charge . " 魔力值，种子 " . $record['name'] . " 设置为【30%】 " . $prmtime . "天";
			$info = $CURUSER['username'] . " 花费了 " . $charge . " 魔力值，种子[url=details.php?id=$torrentid] " . $record['name'] . " [/url]设置为【30%】 " . $prmtime . "天";
		}
	} else if ($prmtype == "8") {//置顶
		if ($prtime > 24) {
			echo "不要贪心，置顶最多允许购买24 小时，已为你改变为购买24 小时，我聪明吧^_^";
			$prtime = 24;
		}
		$charge = ((int) $prmtime) * $ycharge['top'];
		$deadline = date('Y-m-d H:i:s', time() + (3600 * $prmtime));
		if ($charge > $userbouns) {
			echo "魔力值不足，攒点魔力值再来吧^_^";
			die();
		}
		if (sql_query("UPDATE torrents SET pos_state = 'sticky' WHERE id = $torrentid") or sqlerr(__FILE__, __LINE__)) {
			sql_query("UPDATE torrents SET endsticky = '" . $deadline . "' WHERE id = $torrentid") or sqlerr(__FILE__, __LINE__);
			echo $info1 = $CURUSER['username'] . " 花费了 " . $charge . " 魔力值，种子 " . $record['name'] . " 设置【置顶】 " . $prmtime . "小时";
			$info = $CURUSER['username'] . " 花费了 " . $charge . " 魔力值，种子[url=details.php?id=$torrentid] " . $record['name'] . " [/url]设置【置顶】 " . $prmtime . "小时";
		}
	}
	if ($prmtype >= '2' && $prmtype <= '7') {
		$oritime = $record['endfree'];
		$deadline = date('Y-m-d H:i:s', time() + (86400 * $prmtime));

		if ($prmtime >= 39) {
			if ($discount < 0.8) {
				sql_query("UPDATE torrents SET endfree = '" . date('Y-m-d H:i:s', time() + (86400 * 7)) . "' WHERE id = $torrentid") or sqlerr(__FILE__, __LINE__);
				echo ("BUT活动期间不允许购买永久，自动变更为7天。多的魔力值没收了~");
			} else {
				echo "你购买了永久促销";
				sql_query("UPDATE torrents SET endfree = '0000-00-00 00:00:00' WHERE id = $torrentid") or sqlerr(__FILE__, __LINE__);
			}
		} else {
			sql_query("UPDATE torrents SET endfree = '" . $deadline . "' WHERE id = $torrentid") or sqlerr(__FILE__, __LINE__);
		}
	}

	sql_query("UPDATE users SET seedbonus = seedbonus - $charge WHERE id = $userid") or sqlerr(__FILE__, __LINE__); //扣魔力值
	sendshoutbox("土豪君 [@" . $CURUSER['username'] . "] 花了 $charge 个魔力值给种子 [url=details.php?id=$torrentid]" . $record['name'] . "[/url] 购买了促销~来瞅瞅是什么种子吧");
	writeBonusComment($CURUSER['id'], "购买促销 ，花费魔力值$charge");
	write_log("自助购买促销：" . $info1);
	if ($CURUSER['id'] != $record['owner']) {
		sendMessage($CURUSER['id'], $record['owner'], "我给你的种子购买了促销", "" . $info . "\n此站内信由系统代替用户发送");
	}
	sql_query("INSERT INTO comments (user, torrent, added, text) VALUES ($robotusers, $torrentid, now(), '你好，我是替系统来留言的[em12]\n" . $info . "')") or sqlerr(__FILE__, __LINE__);
	sql_query("UPDATE torrents SET comments = comments + 1 WHERE id = $torrentid") or sqlerr(__FILE__, __LINE__);
}
stdfoot();
