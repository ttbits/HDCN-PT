<?php
require_once("include/bittorrent.php");
dbconn();
loggedinorreturn();
parked();
stdhead("魔力红包管理");
?>
<div>
	<form method="POST">
		<?php echo "领取人数：" ?><input type="text" name="num" style="width: 100px" onkeyup="this.value = this.value.replace(/\D/g, '');" />
		<?php echo "红包金额：" ?><input type="text" name="bonus" style="width: 100px" onkeyup="this.value = this.value.replace(/\D/g, '');" />
		<input type="submit" name="submit" value="生成" /><br /><br />
		<?php echo "领取人数至少5人；红包金额在 1000 ~ 100000 之间；以生成时间开始，24小时候后没被领取的部分将会退回" ?>
	</form>
</div>
<?php
if ($_POST['submit']) {
	if ($_POST['num'] == '' || $_POST['num'] == 0 || $_POST['num'] < 5) {
		//$gennum = '5';
		echo "<script>alert('领取人数不得少于5人！');location.href='giftmanager.php';</script>";
		die();
	} else {
		$gennum = $_POST['num'];
	}
	if ($_POST['bonus'] == '' || $_POST['bonus'] == 0 || $_POST['bonus'] < 1000) {
		//$genbonus = '1000';
		echo "<script>alert('红包金额不得少于1000！');location.href='giftmanager.php';</script>";
		die();
	} elseif ($_POST['bonus'] == '' || $_POST['bonus'] == 0 || $_POST['bonus'] > 100000) {
		//$genbonus = '100000';
		echo "<script>alert('红包金额不得高于100000！');location.href='giftmanager.php';</script>";
		die();
	} else {
		$genbonus = $_POST['bonus'];
		if ($genbonus > $CURUSER['seedbonus']) {
			echo "<script>alert('魔力值不足！！！');location.href='giftmanager.php';</script>";
			die();
		}
		if ($gennum > $genbonus) {
			echo "<script>alert('领取人数不得高于红包金额！');location.href='giftmanager.php';</script>";
			die();
		}
		sql_query("UPDATE users SET seedbonus = seedbonus - $genbonus WHERE id = $CURUSER[id]");
		writeBonusComment($CURUSER['id'], "发放魔力红包消耗 $genbonus 个魔力值");
	}
	foreach (generate() as $arr) {
		sql_query("INSERT INTO gift (userid, cards, num, date, bonus) VALUE ($CURUSER[id], '$arr', '$gennum', '" . date("Y-m-d H:i:s", time()) . "', '$genbonus')") or sqlerr(__FILE__, __LINE__);
	}
	echo "<script>alert('生成成功！');location.href='giftmanager.php';</script>";
}

function begin_table_giftmanager($fullwidth = false, $padding = 5) {
	$width = "";
	if ($fullwidth)
		$width .= " width=50%";
	return("<table class='main" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'>");
}

function end_table_giftmanager() {
	return("</table>");
}

function begin_frame_giftmanager($caption = "", $center = false, $padding = 5, $width = "100%", $caption_center = "left") {
	$tdextra = "";
	if ($center)
		$tdextra .= " align='center'";
	return(($caption ? "<h2 align='" . $caption_center . "'>" . $caption . "</h2>" : "") . "<table width='" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'>" . "<tr><td class='text' $tdextra>");
}

function end_frame_giftmanager() {
	return("</td></tr></table>");
}

function bjtable_giftmanager($res, $frame_caption) {
	global $BASEURL;
	$htmlout = '';
	$htmlout .= begin_frame_giftmanager($frame_caption, true);
	$htmlout .= begin_table_giftmanager();
	$htmlout .="<tr><td class='colhead'>序号</td><td class='colhead' align='center'>人数</td><td class='colhead' align='center'>面值</td><td class='colhead' align='center'>生成日期</td><td class='colhead' align='center'>红包链接</td><td class='colhead' align='center'>状态</td></tr>";
	$num = 0;
	$deltime = time() - 86400;
	while ($a = mysql_fetch_assoc($res)) {
		++$num;
		$ress = mysql_fetch_array(sql_query("SELECT SUM(bonus) AS bonus FROM giftlog WHERE cards = '" . $a['cards'] . "'"));
		$total = mysql_num_rows(sql_query("SELECT id FROM giftlog WHERE cards = '$a[cards]'"));
		$htmlout .="<tr class='torrent_table'>" .
				"<td>$num</td>" .
				"<td align='right'>" . $total . " / " . $a['num'] . "</td>" .
				"<td align='right'>" . round(($ress['bonus'] == 0 ? "0" : $ress['bonus'])) . " / " . round($a['bonus'], 3) . "</td>" .
				"<td align='right'>" . $a['date'] . "</td>" .
				//"<td style='width:450px'><textarea style=\"width:98%;overflow:hidden;\" onmouseover=\"this.select()\">" . get_protocol_prefix() . $BASEURL . "/gift.php?r=" . $a['cards'] . "</textarea></td>" .
				"<td align='left'><a href=\"" . get_protocol_prefix() . $BASEURL . "/gift.php?r=" . $a['cards'] . "\" target=\"_blank\">" . get_protocol_prefix() . $BASEURL . "/gift.php?r=" . $a['cards'] . "</a></td>" .
				"<td align='center'>" . ($deltime - strtotime($a['date']) >= 0 ? "失效" : "正常") . "</td>" .
				"</tr>";
	}
	$htmlout .= end_table_giftmanager();
	$htmlout .= end_frame_giftmanager();
	return $htmlout;
}

$HTMLOUT .="<h1 align='center'>红包统计</h1>";
$res = sql_query("SELECT * FROM gift WHERE userid = $CURUSER[id] ORDER BY id DESC"); //降序排列，最新生成的最先显示
$HTMLOUT .= bjtable_giftmanager($res, "所有红包");
$HTMLOUT .="<br /><br />";
print $HTMLOUT;
stdfoot();
