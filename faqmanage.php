<?php

require "include/bittorrent.php";
dbconn();
loggedinorreturn();

if (get_user_class() < UC_ADMINISTRATOR) {
	permissiondenied();
}

stdhead("FAQ管理");
begin_main_frame();

print("<h1 align=\"center\">FAQ管理</h1>");

// make the array that has all the faq in a nice structured
$res = sql_query("SELECT faq.id, faq.link_id, faq.lang_id, lang_name, faq.question, faq.flag, faq.order FROM faq LEFT JOIN language ON faq.lang_id = language.id WHERE type='categ' ORDER BY lang_name, `order` ASC");
while ($arr = mysql_fetch_array($res, MYSQL_BOTH)) {
	$faq_categ[$arr[lang_id]][$arr[link_id]][title] = $arr[question];
	$faq_categ[$arr[lang_id]][$arr[link_id]][flag] = $arr[flag];
	$faq_categ[$arr[lang_id]][$arr[link_id]][order] = $arr[order];
	$faq_categ[$arr[lang_id]][$arr[link_id]][id] = $arr[id];
	$faq_categ[$arr[lang_id]][$arr[link_id]][lang_name] = $arr[lang_name];
}

$res = sql_query("SELECT faq.id, faq.question, faq.lang_id, faq.flag, faq.categ, faq.order FROM faq WHERE type='item' ORDER BY `order` ASC");
while ($arr = mysql_fetch_array($res)) {
	$faq_categ[$arr[lang_id]][$arr[categ]][items][$arr[id]][question] = $arr[question];
	$faq_categ[$arr[lang_id]][$arr[categ]][items][$arr[id]][flag] = $arr[flag];
	$faq_categ[$arr[lang_id]][$arr[categ]][items][$arr[id]][order] = $arr[order];
}

if (isset($faq_categ)) {
// gather orphaned items
	foreach ($faq_categ as $lang => $temp2) {
		foreach ($temp2 as $id => $temp) {
			if (!array_key_exists("title", $temp2[$id])) {
				foreach ($temp2[$id][items] as $id2 => $temp) {
					$faq_orphaned[$lang][$id2][question] = $temp2[$id][items][$id2][question];
					$faq_orphaned[$lang][$id2][flag] = $temp2[$id][items][$id2][flag];
					unset($temp2[$id]);
				}
			}
		}
	}

	// print the faq table
	print("<form method=\"post\" action=\"faqactions.php?action=reorder\">");
	foreach ($faq_categ as $lang => $temp2) {
		foreach ($temp2 as $id => $temp) {
			print("<br />\n<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" width=\"95%\">\n");
			print("<tr><td class=\"colhead\" align=\"center\" colspan=\"2\">编号</td><td class=\"colhead\" align=\"left\">项目名称</td><td class=\"colhead\" align=\"center\">语言</td><td class=\"colhead\" align=\"center\">状态</td><td class=\"colhead\" align=\"center\">动作</td></tr>\n");

			print("<tr><td align=\"center\" width=\"40px\"><select name=\"order[" . $id . "]\">");
			for ($n = 1; $n <= count($temp2); $n++) {
				$sel = ($n == $temp2[$id][order]) ? " selected=\"selected\"" : "";
				print("<option value=\"$n\"" . $sel . ">" . $n . "</option>");
			}
			$status = ($temp2[$id][flag] == "0") ? "<font color=\"red\">隐藏</font>" : "显示";
			print("</select></td><td align=\"center\" width=\"40px\">&nbsp;</td><td><b>" . $temp2[$id][title] . "</b></td><td align=\"center\" width=\"60px\">" . $temp2[$id][lang_name] . "</td><td align=\"center\" width=\"60px\">" . $status . "</td><td align=\"center\" width=\"60px\"><a href=\"faqactions.php?action=edit&id=" . $temp2[$id][id] . "\">编辑</a> <a href=\"faqactions.php?action=delete&id=" . $temp2[$id][id] . "\">删除</a></td></tr>\n");

			if (array_key_exists("items", $temp2[$id])) {
				foreach ($temp2[$id][items] as $id2 => $temp) {
					print("<tr><td align=\"center\" width=\"40px\">&nbsp;</td><td align=\"center\" width=\"40px\"><select name=\"order[" . $id2 . "]\">");
					for ($n = 1; $n <= count($temp2[$id][items]); $n++) {
						$sel = ($n == $temp2[$id][items][$id2][order]) ? " selected=\"selected\"" : "";
						print("<option value=\"$n\"" . $sel . ">" . $n . "</option>");
					}
					if ($temp2[$id][items][$id2][flag] == "0")
						$status = "<font color=\"#FF0000\">隐藏</font>";
					elseif ($temp2[$id][items][$id2][flag] == "2")
						$status = "<font color=\"#0000FF\">更新</font>";
					elseif ($temp2[$id][items][$id2][flag] == "3")
						$status = "<font color=\"#008000\">新的</font>";
					else
						$status = "显示";
					print("</select></td><td>" . $temp2[$id][items][$id2][question] . "</td><td align=\"center\"></td><td align=\"center\" width=\"60px\">" . $status . "</td><td align=\"center\" width=\"60px\"><a href=\"faqactions.php?action=edit&id=" . $id2 . "\">编辑</a> <a href=\"faqactions.php?action=delete&id=" . $id2 . "\">删除</a></td></tr>\n");
				}
			}

			print("<tr><td colspan=\"6\" align=\"center\"><a href=\"faqactions.php?action=additem&inid=" . $id . "&langid=" . $lang . "\">增加新项目</a></td></tr>\n");
			print("</table>\n");
		}
	}
}

// print the orphaned items table
if (isset($faq_orphaned)) {
	print("<br />\n<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" width=\"95%\">\n");
	print("<tr><td align=\"center\" colspan=\"3\"><b style=\"color: #FF0000\">孤立的项目</b></td>\n");
	print("<tr><td class=\"colhead\" align=\"left\">项目名称</td><td class=\"colhead\" align=\"center\">状态</td><td class=\"colhead\" align=\"center\">动作</td></tr>\n");
	foreach ($faq_orphaned as $lang => $temp2) {
		foreach ($temp2 as $id => $temp) {
			if ($temp2[$id][flag] == "0")
				$status = "<font color=\"#FF0000\">隐藏</font>";
			elseif ($temp2[$id][flag] == "2")
				$status = "<font color=\"#0000FF\">更新</font>";
			elseif ($temp2[$id][flag] == "3")
				$status = "<font color=\"#008000\">新的</font>";
			else
				$status = "显示";
			print("<tr><td>" . $temp2[$id][question] . "</td><td align=\"center\" width=\"60px\">" . $status . "</td><td align=\"center\" width=\"60px\"><a href=\"faqactions.php?action=edit&id=" . $id . "\">编辑</a> <a href=\"faqactions.php?action=delete&id=" . $id . "\">删除</a></td></tr>\n");
		}
	}
	print("</table>\n");
}

print("<br />\n<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" width=\"95%\">\n<tr><td align=\"center\"><a href=\"faqactions.php?action=addsection\">增加新的部分</a></td></tr>\n</table>\n");
print("<p align=\"center\"><input type=\"submit\" name=\"reorder\" value=\"重新排序\"></p>\n");
print("</form>\n");
echo $pagerbottom;
end_main_frame();
stdfoot();
