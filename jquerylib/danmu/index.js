window.addEventListener('load', function () {
	// 在窗体载入完毕后再绑定
	var CM = new CommentManager($('#my-comment-stage'));
	CM.init();
	// 先启用弹幕播放（之后可以停止）
	CM.start();
	// 开放 CM 对象到全局这样就可以在 console 终端里操控
	window.CM = CM;

	var socket = io();
	socket.on('danmaku show', function (msg) {
		console.log(msg);
		$('#messages').append($('<li>').text(msg));
		var danmaku = JSON.parse(msg);
		CM.send(danmaku);
	});
});

var socket = io();
function RandomColor() {
	return ('00000' + (Math.random() * 0x1000000 << 0).toString(16)).substr(-6);
}

function MathNum(min_num, max_num) {
	return (((Math.random() * (max_num - min_num + 1)) + "").split(".")[0]) * 1 + min_num;
}

function DanMuEmit() {
	var e = window.event || arguments.callee.caller.arguments[0];
	if (e && e.keyCode == 13) {
		e.preventDefault();
		var danmaku = {
			"text": $('#msg').val(),
			"stime": 0, //开始时间
			"dur": MathNum(15000, 25000), //总生存时间
			"mode": 1, //滚动模式
			"size": MathNum(18, 36), //字幕大小
			"color": RandomColor()//字幕颜色
		};
		var msg = JSON.stringify(danmaku);
		console.log(msg);
		socket.emit('danmaku send', msg);
		$('#msg').val("");
	}
}

function Send() {
	var danmaku = {
		"text": $('#msg').val(),
		"stime": 0, //开始时间
		"dur": MathNum(15000, 25000), //总生存时间
		"mode": 1, //滚动模式
		"size": MathNum(18, 36), //字幕大小
		"color": RandomColor()//字幕颜色
	};
	var msg = JSON.stringify(danmaku);
	console.log(msg);
	socket.emit('danmaku send', msg);
	$('#msg').val("");
}