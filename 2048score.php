<?php

require "include/bittorrent.php";
require 'memcache.php';
dbconn();
checkserver();

$pi = 3.141592653589793;
$class = $CURUSER['class'];
switch ($class) {
	case UC_POWER_USER:
		$rate = "1.15";
		break;
	case UC_ELITE_USER:
		$rate = "1.2";
		break;
	case UC_CRAZY_USER:
		$rate = "1.25";
		break;
	case UC_INSANE_USER:
		$rate = "1.3";
		break;
	case UC_VETERAN_USER:
		$rate = "1.35";
		break;
	case UC_EXTREME_USER:
		$rate = "1.4";
		break;
	case UC_ULTIMATE_USER:
		$rate = "1.45";
		break;
	case UC_NEXUS_MASTER:
		$rate = "1.5";
		break;
	default :
		$rate = "1.1";
}
if ($class >= UC_VIP) {
	$rate = "1.6";
}
$timenow = time();
$sectoweek = 86400 * 7;
$count = 0;
$sqrtof2 = sqrt(2);
$logofpointone = log(0.1);
$value1 = $logofpointone / $tzero_bonus; //T0
$value2 = $bzero_bonus * ( 2 / $pi); //B0
$value3 = $logofpointone / ($nzero_bonus - 1); //N0
$value4 = $l_bonus; //L
$A = 0;
//参数部分--结束
$torrentres = sql_query("SELECT torrents.id, torrents.added, torrents.size, torrents.seeders, (SELECT COUNT(*) FROM torrents WHERE official = 'yes') AS official FROM torrents LEFT JOIN peers ON peers.torrent = torrents.id WHERE peers.userid = $CURUSER[id] AND peers.seeder = 'yes' GROUP BY torrents.id") or sqlerr(__FILE__, __LINE__);
while ($torrent = mysql_fetch_array($torrentres)) {
	$weeks_alive = ($timenow - strtotime($torrent['added'])) / $sectoweek;
	$gb_size = $torrent['size'] / 1073741824;
	if ($torrent['official'] > 0) {
		$official = $torrent['official'];
	} else {
		$official = 0;
	}
	$temp = (1 - exp($value1 * $weeks_alive)) * $gb_size * (1 + $sqrtof2 * exp($value3 * ($torrent['seeders'] - 1))) + $official * 0.5;
	$A += $temp;
	$count++;
}
if ($count > $maxseeding_bonus) {//固定获取魔力值
	$count = $maxseeding_bonus;
}
if ($A != 0) {
	$all_bonus = ($value2 * atan($A / $value4) + ($perseeding_bonus * $count)) * $rate;
} else {
	$all_bonus = 0;
}

$r = base64_decode($_POST['r']);
$json = json_decode($r, true);
$over = $json['over'];
$score = 0 + $json['RiLeGoScores']; //分数
$securityscore = 0 + $json['FangZuoBiScores']; //防作弊分数
$rand = $json['rand']; //随机数
$token = $json['token']; //令牌
$verifytoken = sha1($securityscore . $rand); //验证令牌
if (round($all_bonus, 3) >= 80) {//如果每小时做种积分超过80
	$finalscore = round($score * 0.1, 2); //获得分数的10%作为魔力值
} else {
	$finalscore = round($score * 0.01, 2); //获得分数的1%作为魔力值
}
$uid = 0 + $CURUSER['id'];
$username = sqlesc($CURUSER['username']);
$times = sqlesc(date('Y-m-d', time()));
//$z = $memcache->get($CURUSER['username'] . '_2048_' . $CURUSER['id'] . '_score');
if ($score != $securityscore) {
	echo 'FALSE 3' . ' ' . $times;
} elseif ($token != $verifytoken) {
	echo 'FALSE 4' . ' ' . $times;
//} elseif ($z != $score && $z != $securityscore) {
	//echo 'FALSE 5' . ' ' . $times;
} elseif (!empty($score) && $sign == $verifysign) {
	$row = mysql_fetch_array(sql_query("SELECT times FROM `2048` WHERE uid = $uid AND username = $username"));
	if ($times > sqlesc($row['times']) || empty($row['times'])) {//每天第一次游戏才能获得魔力值奖励
		sql_query("UPDATE users SET seedbonus = seedbonus + $finalscore WHERE id = $uid AND username = $username");
		writeBonusComment($uid, "因智商签到获得 $finalscore 个魔力值，游戏分数为：$score");
		echo 'TRUE' . ' ' . $times;
	} else {
		echo 'FALSE 1' . ' ' . $times;
	}
	if ($memcache->get($CURUSER['username'] . '_2048_' . $CURUSER['id']) == 1) {
		$memcache->set($CURUSER['username'] . '_2048_' . $CURUSER['id'], $times);
		if (empty($row['times'])) {
			sql_query("INSERT INTO `2048` (uid, username, score, times) VALUES ($uid, $username, $score, $times)");
		} else {
			sql_query("UPDATE `2048` SET score = $score, times = $times WHERE uid = $uid AND username = $username");
		}
	}
	/*
	 * 检查排行是否有该用户信息
	 * 如果没有，则插入一条记录
	 * 如果有，则检查score字段的值是否 < 提交上来的分数信息
	 * 1、如果小于当前分数，则不更新
	 * 2、如果大于当前分数，则更新
	 */
	$higher = mysql_fetch_array(sql_query("SELECT score FROM `2048_rank` WHERE uid = $uid AND username = $username"));
	if (empty($higher['score'])) {
		sql_query("INSERT INTO `2048_rank` (score, uid, username, times) VALUES ($score, $uid, $username, $times)");
	} else {
		if ($higher['score'] < $score) {
			sql_query("UPDATE `2048_rank` SET score = $score, times = $times WHERE uid = $uid AND username = $username");
		}
	}
} else {
	echo 'FALSE 2' . ' ' . $times;
}
$memcache->set($CURUSER['username'] . '_2048_' . $CURUSER['id'] . '_score', 0);
