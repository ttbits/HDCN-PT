<?php

require "include/bittorrent.php";
dbconn();
require_once(get_langfile_path());
loggedinorreturn();
parked();
$id = 0 + $_GET["id"];
$type = unesc($_GET["type"]);
$_SESSION['inviterand'] = mt_rand(100000, 999999);
registration_check('invitesystem', true, false);
if (($CURUSER[id] != $id && get_user_class() < $viewinvite_class) || !is_valid_id($id))
	stderr($lang_invite['std_sorry'], $lang_invite['std_permission_denied']);
if (get_user_class() < $sendinvite_class)
	stderr($lang_invite['std_sorry'], $lang_invite['std_only'] . get_user_class_name_zh($sendinvite_class, false, true, true) . $lang_invite['std_or_above_can_invite'], false);
$res = sql_query("SELECT username FROM users WHERE id = " . mysql_real_escape_string($id)) or sqlerr();
$user = mysql_fetch_assoc($res);
stdhead($lang_invite['head_invites']);
print("<table width=700 class=main border=0 cellspacing=0 cellpadding=0><tr><td class=embeddeds>");
print("<h1 align=center><a href=\"invite.php?id=" . $id . "\">" . $user['username'] . $lang_invite['text_invite_system'] . "</a></h1>");
print("<h2 align=center><form method=post action=invite.php?id=" . htmlspecialchars($id) . "&type=new><input type=submit " . ($CURUSER['invites'] == 0 ? "disabled " : "") . " value='" . $lang_invite['sumbit_invite_someone'] . "'></form></h2>");
$sent = htmlspecialchars($_GET['sent']);
if ($sent == 1) {
	$msg = $lang_invite['text_invite_code_sent'];
	print("<p align=center><font color=red>" . $msg . "</font></p>");
}
print("<p align=center><b style=color:red;font-size:12pt>被邀请人收不到激活验证邮件时，你可以帮其手动<font style=color:blue>确认用户</font>激活或联系管理员</b></p>");

$ress = sql_query("SELECT invites FROM users WHERE id = " . mysql_real_escape_string($id)) or sqlerr(__FILE__, __LINE__);
$inv = mysql_fetch_assoc($ress);

if ($type == 'new') {
	if ($CURUSER[invites] == 0) {
		stdmsg($lang_invite['std_sorry'], $lang_invite['std_no_invites_left'] .
				"<a class=altlink href=invite.php?id=$CURUSER[id]>" . $lang_invite['here_to_go_back'], false);
		print("</td></tr></table>");
		stdfoot();
		die;
	}
	$invitation_body = $lang_invite['text_invitation_body'] . $user['username'];
	//$invitation_body_insite = str_replace("<br />","\n",$invitation_body);
	print("<form method=post action=takeinvite.php?id=" . htmlspecialchars($id) . ">" .
			"<table border=1 width=740 cellspacing=0 cellpadding=5>" .
			"<tr align=center><td colspan=2><b>" . $lang_invite['text_invite_someone'] . "$SITENAME (" . ($inv['invites'] < 0 ? "∞" : $inv['invites']) . "" . $lang_invite['text_invitation'] . ")</b></td></tr>" .
			"<tr><td class=\"rowhead nowrap\" valign=\"top\" align=\"right\">" . $lang_invite['text_email_address'] . "</td><td align=left><input type=text size=40 name=email /><br /><font align=left class=small>" . $lang_invite['text_email_address_note'] . "</font>" . ($restrictemaildomain == 'yes' ? "<br />" . $lang_invite['text_email_restriction_note'] . allowedemails() : "") . "</td></tr>" .
			"" . ($nonass == 'yes' && $newuser == 'yes' ? "<tr><td class = \"rowhead nowrap\" valign=\"top\" align=\"right\">是否免考</td><td><input type=\"checkbox\" name=\"nonass\" />勾选此项将会花费 $nonassbonus 魔力值。</td></tr>" : "") . "" .
			"<tr><td class = \"rowhead nowrap\" valign=\"top\" align=\"right\">" . $lang_invite['text_message'] . "</td><td align=left><textarea name=body rows=8 cols=120>" . $invitation_body .
			"</textarea></td></tr>" .
			"<tr><td align=center colspan=2>" .
			"<input type=button onclick=\"$(this).attr('disabled','true');$(this).parents().filter('form').trigger('submit');\" value='" . $lang_invite['submit_invite'] . "'>" .
			"<input type=hidden name='inviterand' value='" . $_SESSION['inviterand'] . "'>" .
			"</td></tr>" .
			"</form></table></td></tr></table>");
} else {
	$count = get_row_count("users", "WHERE invited_by = " . mysql_real_escape_string($id) . "");
	list($pagertop, $pagerbottom, $limit) = pager(25, $count, "?id=" . mysql_real_escape_string($id) . "&");
	$ret = sql_query("SELECT id, username, email, uploaded, downloaded, status, warned, enabled, donor, email, nonass FROM users WHERE invited_by = " . mysql_real_escape_string($id) . " ORDER BY id DESC $limit") or sqlerr(__FILE__, __LINE__);
	$num = mysql_num_rows($ret); //取$limit限制的循环显示
	print("<table border=1 width=740 cellspacing=0 cellpadding=5>" .
			"<h2 align=center>" . $lang_invite['text_invite_status'] . " ($count)</h2><form method=post action=takeconfirm.php?id=" . htmlspecialchars($id) . ">");
	if ($count) {
		print($pagertop);
	}
	if (!$count) {
		print("<tr><td colspan=7 align=center>" . $lang_invite['text_no_invites'] . "</tr>");
	} else {
		print("<tr><td class=colhead><b>" . $lang_invite['text_username'] . "</b></td><td class=colhead><b>" . $lang_invite['text_email'] . "</b></td><td class=colhead><b>" . $lang_invite['text_uploaded'] . "</b></td><td class=colhead><b>" . $lang_invite['text_downloaded'] . "</b></td><td class=colhead><b>" . $lang_invite['text_ratio'] . "</b></td><td class=colhead><b>" . $lang_invite['text_status'] . "</b></td><td class=colhead><b>是否免考</b></td>");
		if ($CURUSER['id'] == $id || get_user_class() >= UC_SYSOP)
			print("<td class=colhead><b>" . $lang_invite['text_confirm'] . "</b></td>");
		print("</tr>");
		for ($i = 0; $i < $num; ++$i) {
			$arr = mysql_fetch_assoc($ret);
			$user = "<td class=rowfollow>" . get_username($arr[id]) . "</td>";

			if ($arr["downloaded"] > 0) {
				$ratio = number_format($arr["uploaded"] / $arr["downloaded"], 3);
				$ratio = "<font color=" . get_ratio_color($ratio) . ">$ratio</font>";
			} else {
				if ($arr["uploaded"] > 0) {
					$ratio = "Inf.";
				} else {
					$ratio = "---";
				}
			}
			if ($arr["status"] == 'confirmed') {
				$status = "<a href=userdetails.php?id=$arr[id]><font color=#1f7309>" . $lang_invite['text_confirmed'] . "</font></a>";
			} else {
				$status = "<a href=checkuser.php?id=$arr[id]><font color=#ca0226>" . $lang_invite['text_pending'] . "</font></a>";
			}
			if ($arr['nonass'] == 'yes') {
				$checknonass = '是';
			} else {
				$checknonass = '否';
			}
			print("<tr class=rowfollow>$user<td>$arr[email]</td><td class=rowfollow>" . mksize($arr[uploaded]) . "</td><td class=rowfollow>" . mksize($arr[downloaded]) . "</td><td class=rowfollow>$ratio</td><td class=rowfollow>$status</td><td class=rowfollow>$checknonass</td>");
			if ($CURUSER[id] == $id || get_user_class() >= UC_SYSOP) {
				print("<td>");
				if ($arr[status] == 'pending') {
					print("<input type=\"checkbox\" name=\"conusr[]\" value=\"" . $arr[id] . "\" />");
				}
				print("</td>");
			}
			print("</tr>");
		}
	}

	if ($CURUSER[id] == $id || get_user_class() >= UC_SYSOP) {
		$pendingcount = number_format(get_row_count("users", "WHERE  status='pending' AND invited_by=$CURUSER[id]"));
		if ($pendingcount) {
			print("<input type=hidden name=email value=$arr[email]>");
			print("<tr><td colspan=8 align=right><input type=submit style='height: 25px' value=" . $lang_invite['submit_confirm_users'] . "></td></tr>");
		}
		print("</form>");
		print("<tr><td colspan=8 align=center><form method=post action=invite.php?id=" . htmlspecialchars($id) . "&type=new><input type=submit " . ($CURUSER[invites] == 0 ? "disabled " : "") . " value='" . $lang_invite['sumbit_invite_someone'] . "'></form></td></tr>");
	}
	print("</table>");
	if ($count) {
		print($pagerbottom);
	}
	$counts = get_row_count("invites", "WHERE inviter = " . mysql_real_escape_string($id) . "");
	$rer = sql_query("SELECT id, invitee, hash, time_invited, nonass FROM invites WHERE inviter = " . mysql_real_escape_string($id) . " ORDER BY id DESC") or sqlerr(__FILE__, __LINE__);
	$nums = mysql_num_rows($rer); //取$limit限制的循环显示
	print("<table border=1 width=740 cellspacing=0 cellpadding=5><h2 align=center>" . $lang_invite['text_sent_invites_status'] . " ($counts)</h2>");
	if (!$counts) {
		print("<tr><td class=colhead>" . $lang_invite['text_email'] . "</td><td class=colhead>" . $lang_invite['text_hash'] . "</td><td class=colhead>" . $lang_invite['text_send_date'] . "</td><td class=colhead>是否免考</td></tr>");
		print("<tr align=center><td colspan=6>" . $lang_invite['text_no_invitation_sent'] . "</tr>");
	} else {
		print("<tr><td class=colhead>" . $lang_invite['text_email'] . "</td><td class=colhead>" . $lang_invite['text_hash'] . "</td><td class=colhead>" . $lang_invite['text_send_date'] . "</td><td class=colhead>是否免考</td><td class=colhead>回收邀请</td></tr>");
		for ($i = 0; $i < $nums; ++$i) {
			$arr1 = mysql_fetch_assoc($rer);
			print("<tr><td class=rowfollow>$arr1[invitee]<td class=rowfollow><input type=\"text\" style=\"width:98%\" onmouseover=\"this.select()\" value=\"" . get_protocol_prefix() . $BASEURL . "/signup.php?type=invite&invitenumber=" . $arr1[hash] . "&nonass=" . ($arr1[nonass] == 'yes' ? "yes" : "no") . "\" /></td><td class=rowfollow>$arr1[time_invited]</td><td class=rowfollow>" . ($arr1[nonass] == 'yes' ? "是" : "否") . "</td><td class=rowfollow><form method=\"POST\"><input type=\"hidden\" name=\"inviteid\" value=\"$arr1[id]\" /><input type=\"submit\" name=\"recover\" value=\"回收\" /></form></td></tr>");
		}
	}
	print("</table>");
	print("</td></tr></table>");
}
if ($_POST['recover']) {
	$inviteid = $_POST['inviteid'];
	sql_query("DELETE FROM invites WHERE id = $inviteid") or sqlerr(__FILE__, __LINE__);
	if ($inv['invites'] >= 0) {
		sql_query("UPDATE users SET invites = invites + 1 WHERE id = $id") or sqlerr(__FILE__, __LINE__);
	}
	echo "<script>alert('回收邀请成功！');location.href='invite.php?id=$id';</script>";
}
stdfoot();
die;
