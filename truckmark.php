<?php

require "include/bittorrent.php";
dbconn();

//Send some headers to keep the user's browser from caching the response.
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-Type: text/xml; charset=utf-8");

$torrentid = 0 + $_GET['torrentid'];
if (isset($CURUSER)) {
	$res_truckmark = sql_query("SELECT * FROM truckmarks WHERE torrentid=" . sqlesc($torrentid) . " AND userid=" . sqlesc($CURUSER[id]));
	if (mysql_num_rows($res_truckmark) == 1) {
		sql_query("DELETE FROM truckmarks WHERE torrentid=" . sqlesc($torrentid) . " AND userid=" . sqlesc($CURUSER['id'])) or sqlerr(__FILE__, __LINE__);
		$Cache->delete_value('user_' . $CURUSER['id'] . '_truckmark_array');
		echo "deleted";
	} else {
		sql_query("INSERT INTO truckmarks (torrentid, userid, added) VALUES (" . sqlesc($torrentid) . "," . sqlesc($CURUSER['id']) . "," . sqlesc(date("Y-m-d", time())) . ")") or sqlerr(__FILE__, __LINE__);
		$Cache->delete_value('user_' . $CURUSER['id'] . '_truckmark_array');
		echo "added";
	}
} else
	echo "failed";
