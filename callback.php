<?php

require_once("include/bittorrent.php");
require_once('classes/secken.class.php');
$event_id = $_GET['event_id'];

// Create an API object using your credentials
$secken_api = new secken($appid, $appkey, $authid);

# Step 1 - Get an qrcode for binding
$ret = $secken_api->getResult($event_id);

//$ret = $secken_api->realtimeAuth('uid', 1, 1, 'https://callback.com/path', '123.123.123.123', '名字');
//$ret = $secken_api->offline_auth('2121','sd');
# Step 2 - Check the returned result
if ($secken_api->getCode() != 200) {
	//var_dump($secken_api->getCode(), $secken_api->getMessage());
	echo json_encode($secken_api);
} else {
	echo json_encode($ret);
}