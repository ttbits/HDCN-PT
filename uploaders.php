<?php
require "include/bittorrent.php";
dbconn();
require_once(get_langfile_path());
loggedinorreturn();

if (get_user_class() < UC_FORWARD)
	permissiondenied();

$standardsize = 50; //单位为GB
$standardcount = 15; //发布种子数量
$oversize = 100; //单位为GB
$overcount = 25; //发布种子数量
$salary = 30000; //工资

function checkout($torrentsize, $torrentcount) {
	global $standardsize, $standardcount, $overcount, $oversize;
	if ($torrentsize < 1073741824)
		return false;
	if ($torrentcount >= $overcount)
		return true;
	elseif (mksize($torrentsize) >= $oversize)
		return true;
	elseif (mksize($torrentsize) >= $standardsize && $torrentcount >= $standardcount)
		return true;
	else
		return false;
}

$unpassname = "";
$passname = "";
$year = 0 + $_GET['year'];
if (!$year || $year < 2000)
	$year = date('Y');
$month = 0 + $_GET['month'];
if (!$month || $month <= 0 || $month > 12)
	$month = date('m');


$order = $_GET['order'];
if (!in_array($order, array('username', 'torrent_size', 'torrent_count')))
	$order = 'username';
if ($order == 'username')
	$order .=' ASC';
else
	$order .= ' DESC';
stdhead($lang_uploaders['head_uploaders']);
begin_main_frame();
?>
<tr align="center">
	<td>
		<div>
			<?php
			$year2 = substr($datefounded, 0, 4);
			$yearfounded = ($year2 ? $year2 : 2007);
			$yearnow = date("Y");

			$timestart = strtotime($year . "-" . $month . "-01 00:00:00");
			$sqlstarttime = date("Y-m-d H:i:s", $timestart);
			$timeend = strtotime("+1 month", $timestart);
			$sqlendtime = date("Y-m-d H:i:s", $timeend);

			print("<h1 align=\"center\">" . $lang_uploaders['text_uploaders'] . " - " . date("Y-m", $timestart) . "月 - 考核情况</h1>");
			$date = date("Y-m", $timestart);

			$yearselection = "<select name=\"year\">";
			for ($i = $yearfounded; $i <= $yearnow; $i++)
				$yearselection .= "<option value=\"" . $i . "\"" . ($i == $year ? " selected=\"selected\"" : "") . ">" . $i . "</option>";
			$yearselection.="</select>";

			$monthselection = "<select name=\"month\">";
			for ($i = 1; $i <= 12; $i++)
				$monthselection .= "<option value=\"" . $i . "\"" . ($i == $month ? " selected=\"selected\"" : "") . ">" . $i . "</option>";
			$monthselection.="</select>";
			?>
			<table border="1" cellpadding="10" cellspacing="0" width="750"><tbody><tr><td>
							<h1 style="width: 60%">现行考核标准为：</h1>
							<ul>
								<li>每月发布种子大小 >= <b><?= $standardsize ?> GB</b>，<b style="color:red">且</b>发布种子数量 >= <b><?= $standardcount ?></b> 个。则视为合格；</li>
								<li>如果发布种子大小 >= <b><?= $oversize ?> GB</b>，<b style="color:red">或</b>发布种子数量 >= <b><?= $overcount ?></b> 个，则无视另一项数据，视为合格。</li>
							</ul>
						</td></tr></tbody></table><br />
			<div>
				<form method="get">
					<span>
						<?php echo $lang_uploaders['text_select_month'] ?><?php echo $yearselection ?>&nbsp;&nbsp;<?php echo $monthselection ?>&nbsp;&nbsp;<input type="submit" value="<?php echo $lang_uploaders['submit_go'] ?>" />
					</span>
				</form>
			</div>

			<?php
			$numbers = get_row_count("users", "WHERE class >= " . UC_FORWARD . " AND class <=" . UC_UPLOADER);
			if (!$numbers)
				print("<p align=\"center\">" . $lang_uploaders['text_no_uploaders_yet'] . "</p>");
			else {
				?>

				<div style="margin-top: 8px">
					<?php
					print("<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" width=\"950\"><tr>");
					print("<td class=\"colhead\" align=\"center\">" . $lang_uploaders['col_username'] . "</td>");
					print("<td class=\"colhead\" align=\"center\">" . $lang_uploaders['col_torrents_size'] . "</td>");
					print("<td class=\"colhead\" align=\"center\">" . $lang_uploaders['col_torrents_num'] . "</td>");
					print("<td class=\"colhead\" align=\"center\">" . $lang_uploaders['col_last_upload_time'] . "</td>");
					print("<td class=\"colhead\" align=\"center\">是否合格</td>");
					if (get_user_class() >= UC_ADMINISTRATOR)
						print("<td class=\"colhead\" align=\"center\">状态</td>");
					print("</tr>");
					$res = sql_query("SELECT users.id AS userid, users.username AS username, COUNT(torrents.id) AS torrent_count, SUM(torrents.size) AS torrent_size FROM torrents LEFT JOIN users ON torrents.owner = users.id WHERE users.class >= " . UC_FORWARD . " AND users.class <= " . UC_UPLOADER . " AND torrents.added > " . sqlesc($sqlstarttime) . " AND torrents.added < " . sqlesc($sqlendtime) . " GROUP BY userid ORDER BY users.class DESC, userid ASC");
					while ($row = mysql_fetch_array($res)) {
						$ress = mysql_fetch_array(sql_query("SELECT id, name, added FROM torrents WHERE owner = " . $row['userid'] . " ORDER BY id DESC LIMIT 1"));
						print("<tr>");
						print("<td class=\"colfollow\">" . get_username($row['userid'], false, true, true, false, false, true) . "</td>");
						print("<td class=\"colfollow\">" . ($row['torrent_size'] ? mksize($row['torrent_size']) : "0") . "</td>");
						print("<td class=\"colfollow\">" . $row['torrent_count'] . "</td>");
						print("<td class=\"colfollow\">" . ($ress['added'] ? gettime($ress['added']) : $lang_uploaders['text_not_available']) . "</td>");
						print("<td class=\"colfollow\">" . (checkout($row['torrent_size'], $row['torrent_count']) ? "<b style=\"color:green\">合格</b>" : "<b style=\"color:red\">不合格</b>"));
						if (get_user_class() >= UC_ADMINISTRATOR) {
							if (checkout($row['torrent_size'], $row['torrent_count'])) {
								print("<td class=\"colfollow\">统一发放</td>");
								$passname .= $row['username'] . ",";
							} else {
								print("<td class=\"colfollow\">没有工资</td>");
								$unpassname .= $row['username'] . ",";
							}
						}
						print("</tr>");
					}
					print("</table><br />");
					if (get_user_class() >= UC_ADMINISTRATOR) {
						$passname = rtrim($passname, ",");
						$unpassname = rtrim($unpassname, ",");
						print(
								"<table style=\"width: 950px\">"
								. "<tr>"
								. "<td class='colhead' align=\"center\">发工资</td>"
								. "</tr>"
								. "<tr>"
								. "<td class=\"colfollow\">"
								. "<form method=\"post\" action=\"amountbonus.php\" /><br/>"
								. "魔力：<input type='text' value='$salary' name='seedbonus' style='width: 80%' /><br/><br/>"
								. "人员：<input type='text' value='$passname' name='username' style='width: 80%' readonly /><br/><br/>"
								. "原因：<input type='text' value='恭喜 $date 月考核通过，工资奉上' name='reason_1' style='width: 80%' /><br/><br/>"
								. "<input type=\"submit\" value=\"发工资\" class=\"btn\" /><br/>"
								. "</form><br/>"
								. "</td>"
								. "</tr>"
								. "</table><br/>"
						);
					}
					?>
				</div>
				<?php
			}
			?>
		</div>
	</td>
</tr>
<?php
end_main_frame();
stdfoot();
