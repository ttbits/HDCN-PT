<?php

require_once("include/bittorrent.php");
require_once(get_langfile_path());

if (isset($_SESSION['inviterand']) && isset($_POST['inviterand']) && $_POST['inviterand'] == $_SESSION['inviterand']) {
	unset($_SESSION['inviterand']);
} else {
	stderr($lang_takeinvite['std_error'], $lang_takeinvite['std_no_invite']);
}
dbconn();
registration_check('invitesystem', true, false);
if (get_user_class() < $sendinvite_class)
	stderr($lang_takeinvite['std_error'], $lang_takeinvite['std_invite_denied']);
if ($CURUSER['invites'] == 0) {
	stderr($lang_takeinvite['std_error'], $lang_takeinvite['std_no_invite']);
}

function bark($msg) {
	stdhead();
	stdmsg($lang_takeinvite['head_invitation_failed'], $msg);
	stdfoot();
	exit;
}

$id = $CURUSER[id];
$email = unesc(htmlspecialchars(trim($_POST["email"])));
$email = safe_email($email);
if (!$email)
	bark($lang_takeinvite['std_must_enter_email']);
if (!check_email($email))
	bark($lang_takeinvite['std_invalid_email_address']);
if (EmailBanned($email))
	bark($lang_takeinvite['std_email_address_banned']);
if (!EmailAllowed($email))
	bark($lang_takeinvite['std_wrong_email_address_domains'] . allowedemails());

$body = str_replace("<br />", "<br />", nl2br(trim(strip_tags($_POST["body"]))));
if (!$body)
	bark($lang_takeinvite['std_must_enter_personal_message']);

// check if email addy is already in use
$a = (@mysql_fetch_row(@sql_query("SELECT COUNT(*) FROM users WHERE email=" . sqlesc($email)))) or die(mysql_error());
if ($a[0] != 0)
	bark($lang_takeinvite['std_email_address'] . htmlspecialchars($email) . $lang_takeinvite['std_is_in_use']);
$b = (@mysql_fetch_row(@sql_query("SELECT COUNT(*) FROM invites WHERE invitee=" . sqlesc($email)))) or die(mysql_error());
if ($b[0] != 0)
	bark($lang_takeinvite['std_invitation_already_sent_to'] . htmlspecialchars($email) . $lang_takeinvite['std_await_user_registeration']);

$ret = sql_query("SELECT username FROM users WHERE id = " . sqlesc($id)) or sqlerr();
$arr = mysql_fetch_assoc($ret);

$hash = md5(mt_rand(1, 10000) . $CURUSER['username'] . TIMENOW . $CURUSER['passhash']);

$title = $SITENAME . $lang_takeinvite['mail_tilte'];

if ($securetracker == 'yes' || $securetracker == 'op')
	$tracker_ssl = true;
elseif ($_COOKIE["c_secure_tracker_ssl"] == base64("yeah"))
	$tracker_ssl = true;
else
	$tracker_ssl = false;
if ($tracker_ssl == true) {
	$ssl_invite = "https://";
} else {
	$ssl_invite = "http://";
}

$checknonass = $_POST['nonass'] ? 'yes' : 'no';
$message = <<<EOD
{$lang_takeinvite['mail_one']}{$arr[username]}{$lang_takeinvite['mail_two']}
<b><a href="$ssl_invite$BASEURL/signup.php?type=invite&invitenumber=$hash&nonass=$checknonass">$ssl_invite$BASEURL/signup.php?type=invite&invitenumber=$hash&nonass=$checknonass</a></b><br />
<br />{$lang_takeinvite['mail_three']}{$lang_takeinvite['mail_four']}{$arr[username]}{$lang_takeinvite['mail_five']}<br />
$body
<br /><br />{$lang_takeinvite['mail_six']}
EOD;

if ($CURUSER['invites'] > 0) {
	$checklimitinvites = mysql_fetch_array(sql_query("SELECT invites FROM limitinvite WHERE userid = $id"));
	if ($_POST['nonass']) {
		sql_query("UPDATE users SET invites = invites - 1, seedbonus = seedbonus - $nonassbonus WHERE id = " . mysql_real_escape_string($id) . "") or sqlerr(__FILE__, __LINE__);
		writeBonusComment(mysql_real_escape_string($id), "因发送免考邀请减少 $nonassbonus 个魔力值");
		//优先使用限时邀请
		if ($checklimitinvites['invites'] > 0) {
			sql_query("UPDATE limitinvite SET invites = invites - 1 WHERE userid = " . mysql_real_escape_string($id) . "") or sqlerr(__FILE__, __LINE__);
		}
	} else {
		sql_query("UPDATE users SET invites = invites - 1 WHERE id = " . mysql_real_escape_string($id) . "") or sqlerr(__FILE__, __LINE__);
		//优先使用限时邀请
		if ($checklimitinvites['invites'] > 0) {
			sql_query("UPDATE limitinvite SET invites = invites - 1 WHERE userid = " . mysql_real_escape_string($id) . "") or sqlerr(__FILE__, __LINE__);
		}
	}
}

sent_mail($email, $SITENAME, $SITEEMAIL, change_email_encode(get_langfolder_cookie(), $title), change_email_encode(get_langfolder_cookie(), $message), "invitesignup", false, false, '', get_email_encode(get_langfolder_cookie())); //this email is sent only when someone give out an invitation

sql_query("INSERT invites (inviter, invitee, hash, time_invited, nonass) VALUES ('" . mysql_real_escape_string($id) . "', '" . mysql_real_escape_string($email) . "', '" . mysql_real_escape_string($hash) . "', " . sqlesc(date("Y-m-d H:i:s")) . ", '" . $checknonass . "')");

header("Refresh: 0; url = invite.php?id=" . htmlspecialchars($id) . "&sent=1");
