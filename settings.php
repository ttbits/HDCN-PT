<?php

require "include/bittorrent.php";
dbconn();
require_once(get_langfile_path());
loggedinorreturn();
parked();

if (get_user_class() < UC_SYSOP)
	permissiondenied();

//read all configuration files
require('config/allconfig.php');

function go_back() {
	global $lang_settings;
	stdmsg($lang_settings['std_message'], $lang_settings['std_click'] . "<a class=\"altlink\" href=\"settings.php\">" . $lang_settings['std_here'] . "</a>" . $lang_settings['std_to_go_back']);
}

function yesorno($title, $name, $value, $note = "", $disabled = "") {
	global $lang_settings;
	tr($title, "<input type='radio' id='" . $name . "yes' name='" . $name . "'" . ($value == "yes" ? " checked=\"checked\"" : "") . " value='yes' $disabled /> <label for='" . $name . "yes'>" . $lang_settings['text_yes'] . "</label> <input type='radio' id='" . $name . "no' name='" . $name . "'" . ($value == "no" ? " checked=\"checked\"" : "") . " value='no' $disabled /> <label for='" . $name . "no'>" . $lang_settings['text_no'] . "</label><br />" . $note, 1);
}

$action = isset($_POST['action']) ? $_POST['action'] : 'showmenu';
if (get_user_class() == UC_STAFFLEADER) {
	$allowed_actions = array('basicsettings', 'mainsettings', 'smtpsettings', 'securitysettings', 'authoritysettings', 'tweaksettings', 'botsettings', 'codesettings', 'bonussettings', 'accountsettings', 'torrentsettings', 'attachmentsettings', 'advertisementsettings', 'savesettings_basic', 'savesettings_main', 'savesettings_smtp', 'savesettings_security', 'savesettings_authority', 'savesettings_tweak', 'savesettings_bot', 'savesettings_code', 'savesettings_bonus', 'savesettings_account', 'savesettings_torrent', 'savesettings_attachment', 'savesettings_advertisement', 'showmenu', 'savesettings_check', 'checksettings');
} else {
	$allowed_actions = array('mainsettings', 'smtpsettings', 'securitysettings', 'authoritysettings', 'tweaksettings', 'botsettings', 'codesettings', 'bonussettings', 'accountsettings', 'torrentsettings', 'attachmentsettings', 'advertisementsettings', 'savesettings_basic', 'savesettings_main', 'savesettings_smtp', 'savesettings_security', 'savesettings_authority', 'savesettings_tweak', 'savesettings_bot', 'savesettings_code', 'savesettings_bonus', 'savesettings_account', 'savesettings_torrent', 'savesettings_attachment', 'savesettings_advertisement', 'showmenu', 'savesettings_check', 'checksettings');
}
if (!in_array($action, $allowed_actions)) {
	$action = 'showmenu';
}
$notice = "<h1 align=\"center\"><a class=\"faqlink\" href=\"settings.php\">" . $lang_settings['text_website_settings'] . "</a></h1><table cellspacing=\"0\" cellpadding=\"10\" width=\"940\"><tr><td colspan=\"2\" style='padding: 10px; background: black' align=\"center\"><font color=\"white\">" . $lang_settings['text_configuration_file_saving_note'] . "</font></td></tr>";

$ks = date("Y-m-d", strtotime($assessmentstart . " + " . $assessmenttime . " days"));
if ($assessment == 'yes' && (round((strtotime($ks) - strtotime(date("Y-m-d"))) / 86400)) != 0) {
	$recheckstatus = 'yes';
} else {
	$recheckstatus = 'no';
}
$actiontime = date("Y-m-d H:i:s");
if ($action == 'savesettings_main') { // save main
	stdhead($lang_settings['head_save_main_settings']);
	$validConfig = array('autorelease', 'mode', 'site_online', 'max_torrent_size', 'announce_interval', 'annintertwoage', 'annintertwo', 'anninterthreeage', 'anninterthree', 'signup_timeout', 'minoffervotes', 'offervotetimeout', 'offeruptimeout', 'maxsubsize', 'postsperpage', 'topicsperpage', 'torrentsperpage', 'maxnewsnum', 'max_dead_torrent_time', 'maxusers', 'torrent_dir', 'iniupload', 'iniseedbonus', 'SITEEMAIL', 'ACCOUNTANTID', 'ALIPAYACCOUNT', 'PAYPALACCOUNT', 'SLOGAN', 'icplicense', 'autoclean_interval_one', 'autoclean_interval_two', 'autoclean_interval_three', 'autoclean_interval_four', 'autoclean_interval_five', 'reportemail', 'invitesystem', 'loginadd', 'registration', 'rgenablead', 'share', 'sharesize', 'sharenum', 'hr', 'stronghr', 'hrhour', 'hrday', 'hrhit', 'hrbonus', 'hrradio', 'hrstartradio', 'newuser', 'newuser_kstime', 'newuser_dl', 'newuser_ul', 'newuser_sb', 'newuser_ra', 'newuser_dltime', 'newuser_ultime', 'newuser_tr', 'newuser_fa', 'bump', 'bumptime', 'bumpaward', 'bumptimeaward', 'bumptop', 'cuxiao', 'cuxiaoselect', 'cuxiaofree', 'cuxiaosticky', 'autoend', 'autoendtime', 'autoendfree', 'autoendsticky', 'autoendlimit', 'sticky', 'stickytime', 'cardreg', 'showhotmovies', 'showclassicmovies', 'showimdbinfo', 'enablenfo', 'enableschool', 'restrictemail', 'showpolls', 'showstats', 'showlastxtorrents', 'showtrackerload', 'showshoutbox', 'showfunbox', 'showoffer', 'sptime', 'showhelpbox', 'enablebitbucket', 'smalldescription', 'extforum', 'extforumurl', 'defaultlang', 'stylesheetoff', 'defstylesheet', 'donation', 'browsecat', 'waitsystem', 'maxdlsystem', 'bitbucket', 'torrentnameprefix', 'showforumstats', 'verification', 'invite_count', 'invite_timeout', 'seeding_leeching_time_calc_start', 'startsubid', 'logo', 'logowidth', 'logoheight', 'checkfreenum', 'ipv6signup', 'betsystem', 'betsrobotid', 'betsforumid', 'betsadmin', 'officialinvites', 'nonassinvitesystem', 'nonassbonus', 'reward', 'rewardnum', 'rewarduser', 'claim', 'claimbonus', 'claimtime', 'claimmul', 'claimnum', 'claimmaxnum', 'robotusers');
	GetVar($validConfig);
	unset($MAIN);
	foreach ($validConfig as $config) {
		$MAIN[$config] = $$config;
	}
	WriteConfig('MAIN', $MAIN);
	$Cache->delete_value('recent_news', true);
	$Cache->delete_value('stats_users', true);
	$Cache->delete_value('stats_torrents', true);
	$Cache->delete_value('peers_count', true);
	write_log("主要设定被 $CURUSER[username] 在 $actiontime 更新。", 'mod');
	go_back();
} elseif ($action == 'savesettings_basic') {  // save basic
	stdhead($lang_settings['head_save_basic_settings']);
	$validConfig = array('SITENAME', 'BASEURL', 'announce_url', 'mysql_host', 'mysql_user', 'mysql_pass', 'mysql_db');
	GetVar($validConfig);
	if (!mysql_connect($mysql_host, $mysql_user, $mysql_pass)) {
		stdmsg($lang_settings['std_error'], $lang_settings['std_mysql_connect_error'] . $lang_settings['std_click'] . "<a class=\"altlink\" href=\"settings.php\">" . $lang_settings['std_here'] . "</a>" . $lang_settings['std_to_go_back']);
	} else {
		dbconn();
		unset($BASIC);
		foreach ($validConfig as $config) {
			$BASIC[$config] = $$config;
		}
		WriteConfig('BASIC', $BASIC);
		write_log("基础设定被 $CURUSER[username] 在 $actiontime 更新。", 'mod');
		go_back();
	}
} elseif ($action == 'savesettings_code') {  // save database
	stdhead($lang_settings['head_save_code_settings']);
	$validConfig = array('mainversion', 'subversion', 'releasedate', 'website');
	GetVar($validConfig);
	unset($CODE);
	foreach ($validConfig as $config) {
		$CODE[$config] = $$config;
	}
	WriteConfig('CODE', $CODE);
	write_log("代码设定被 $CURUSER[username] 在 $actiontime 更新。", 'mod');
	go_back();
} elseif ($action == 'savesettings_bonus') {  // save bonus
	stdhead($lang_settings['head_save_bonus_settings']);
	$validConfig = array('donortimes', 'perseeding', 'maxseeding', 'tzero', 'nzero', 'bzero', 'l', 'uploadtorrent', 'uploadsubtitle', 'starttopic', 'makepost', 'addcomment', 'pollvote', 'offervote', 'funboxvote', 'saythanks', 'receivethanks', 'funboxreward', 'onegbupload', 'fivegbupload', 'tengbupload', 'hundredgbupload', 'ratiolimit', 'dlamountlimit', 'oneinvite', 'customtitle', 'vipstatus', 'bonusgift', 'basictax', 'taxpercentage', 'prolinkpoint', 'prolinktime', 'buyvpn', 'vpn', 'vpnclass');
	GetVar($validConfig);
	unset($BONUS);
	foreach ($validConfig as $config) {
		$BONUS[$config] = $$config;
	}
	WriteConfig('BONUS', $BONUS);
	write_log("魔力值设定被 $CURUSER[username] 在 $actiontime 更新。", 'mod');
	go_back();
} elseif ($action == 'savesettings_account') {  // save account
	stdhead($lang_settings['head_save_account_settings']);
	$validConfig = array('neverdelete', 'neverdeletepacked', 'deletepacked', 'deleteunpacked', 'deletenotransfer', 'deletenotransfertwo', 'deletepeasant', 'psdlone', 'psratioone', 'psdltwo', 'psratiotwo', 'psdlthree', 'psratiothree', 'psdlfour', 'psratiofour', 'psdlfive', 'psratiofive', 'putime', 'pudl', 'puprratio', 'puderatio', 'eutime', 'eudl', 'euprratio', 'euderatio', 'cutime', 'cudl', 'cuprratio', 'cuderatio', 'iutime', 'iudl', 'iuprratio', 'iuderatio', 'vutime', 'vudl', 'vuprratio', 'vuderatio', 'exutime', 'exudl', 'exuprratio', 'exuderatio', 'uutime', 'uudl', 'uuprratio', 'uuderatio', 'nmtime', 'nmdl', 'nmprratio', 'nmderatio', 'putimer', 'pudlr', 'puprratior', 'puderatior', 'eutimer', 'eudlr', 'euprratior', 'euderatior', 'cutimer', 'cudlr', 'cuprratior', 'cuderatior', 'iutimer', 'iudlr', 'iuprratior', 'iuderatior', 'vutimer', 'vudlr', 'vuprratior', 'vuderatior', 'exutimer', 'exudlr', 'exuprratior', 'exuderatior', 'uutimer', 'uudlr', 'uuprratior', 'uuderatior', 'nmtimer', 'nmdlr', 'nmprratior', 'nmderatior', 'getInvitesByPromotion');
	GetVar($validConfig);
	unset($ACCOUNT);
	foreach ($validConfig as $config) {
		$ACCOUNT[$config] = $$config;
	}
	WriteConfig('ACCOUNT', $ACCOUNT);
	write_log("账号设定被 $CURUSER[username] 在 $actiontime 更新。", 'mod');
	go_back();
} elseif ($action == 'savesettings_torrent') {  // save account
	stdhead($lang_settings['head_save_torrent_settings']);
	$validConfig = array('prorules', 'randomhalfleech', 'randomfree', 'randomtwoup', 'randomtwoupfree', 'randomtwouphalfdown', 'largesize', 'largepro', 'expirehalfleech', 'expirefree', 'expiretwoup', 'expiretwoupfree', 'expiretwouphalfleech', 'expirenormal', 'hotdays', 'hotseeder', 'halfleechbecome', 'freebecome', 'twoupbecome', 'twoupfreebecome', 'twouphalfleechbecome', 'normalbecome', 'uploaderdouble', 'deldeadtorrent', 'randomthirtypercentdown', 'thirtypercentleechbecome', 'expirethirtypercentleech');
	GetVar($validConfig);
	unset($TORRENT);
	foreach ($validConfig as $config) {
		$TORRENT[$config] = $$config;
	}
	WriteConfig('TORRENT', $TORRENT);
	write_log("种子设定被 $CURUSER[username] 在 $actiontime 更新。", 'mod');
	go_back();
} elseif ($action == 'savesettings_smtp') {  // save smtp
	stdhead($lang_settings['head_save_smtp_settings']);
	$validConfig = array('smtptype', 'mailtouser', 'emailnotify', 'smtpaddress', 'smtpport', 'accountname', 'accountpassword', 'smtp_host', 'smtp_port', 'smtp_from');
	GetVar($validConfig);
	unset($SMTP);
	foreach ($validConfig as $config) {
		$SMTP[$config] = $$config;
	}
	WriteConfig('SMTP', $SMTP);
	write_log("SMTP设定被 $CURUSER[username] 在 $actiontime 更新。", 'mod');
	go_back();
} elseif ($action == 'savesettings_security') {  // save security
	stdhead($lang_settings['head_save_security_settings']);
	$validConfig = array('securelogin', 'securetracker', 'https_announce_url', 'iv', 'maxip', 'maxloginattempts', 'changeemail', 'onsecurity', 'limitsecurity', 'viewlimit', 'freeviewlimit', 'limitbonus', 'cheaterdet', 'nodetect', 'authoff', 'appid', 'appkey', 'authid', 'authtype', 'ttkoff', 'ttktoken', 'bindrewardoff', 'bindrewardnum', 'loginrewardoff', 'loginrewardmul');
	GetVar($validConfig);
	unset($SECURITY);
	foreach ($validConfig as $config) {
		$SECURITY[$config] = $$config;
	}
	WriteConfig('SECURITY', $SECURITY);
	write_log("安全设定被 $CURUSER[username] 在 $actiontime 更新。", 'mod');
	go_back();
} elseif ($action == 'savesettings_authority') {  // save user authority
	stdhead($lang_settings['head_save_authority_settings']);
	$validConfig = array('defaultclass', 'staffmem', 'newsmanage', 'newfunitem', 'funmanage', 'sbmanage', 'pollmanage', 'applylink', 'linkmanage', 'postmanage', 'commanage', 'forummanage', 'viewuserlist', 'torrentmanage', 'torrentsticky', 'torrentonpromotion', 'askreseed', 'viewnfo', 'torrentstructure', 'sendinvite', 'Tsendinvite', 'viewhistory', 'topten', 'log', 'confilog', 'userprofile', 'torrenthistory', 'prfmanage', 'cruprfmanage', 'uploadsub', 'delownsub', 'submanage', 'updateextinfo', 'viewanonymous', 'beanonymous', 'addoffer', 'offermanage', 'upload', 'chrmanage', 'viewinvite', 'buyinvite', 'seebanned', 'againstoffer', 'userbar', 'newusermanage', 'hrmanage', 'sharemanage');
	GetVar($validConfig);
	unset($AUTHORITY);
	foreach ($validConfig as $config) {
		$AUTHORITY[$config] = $$config;
	}
	WriteConfig('AUTHORITY', $AUTHORITY);
	write_log("权限设定被 $CURUSER[username] 在 $actiontime 更新。", 'mod');
	go_back();
} elseif ($action == 'savesettings_tweak') { // save tweak
	stdhead($lang_settings['head_save_tweak_settings']);
	$validConfig = array('where', 'iplog1', 'snow', 'bonus', 'datefounded', 'enablelocation', 'titlekeywords', 'metakeywords', 'metadescription', 'enablesqldebug', 'groupchat', 'sqldebug', 'cssdate', 'enabletooltip', 'prolinkimg', 'analyticscode');
	GetVar($validConfig);
	unset($TWEAK);
	foreach ($validConfig as $config) {
		$TWEAK[$config] = $$config;
	}
	WriteConfig('TWEAK', $TWEAK);
	write_log("次要设定被 $CURUSER[username] 在 $actiontime 更新。", 'mod');
	go_back();
} elseif ($action == 'savesettings_attachment') { // save attachment
	stdhead($lang_settings['head_save_attachment_settings']);
	$validConfig = array('enableattach', 'classone', 'countone', 'sizeone', 'extone', 'classtwo', 'counttwo', 'sizetwo', 'exttwo', 'classthree', 'countthree', 'sizethree', 'extthree', 'classfour', 'countfour', 'sizefour', 'extfour', 'savedirectory', 'httpdirectory', 'savedirectorytype', 'thumbnailtype', 'thumbquality', 'thumbwidth', 'thumbheight', 'watermarkpos', 'watermarkwidth', 'watermarkheight', 'watermarkquality', 'altthumbwidth', 'altthumbheight');
	GetVar($validConfig);
	unset($ATTACHMENT);
	foreach ($validConfig as $config) {
		$ATTACHMENT[$config] = $$config;
	}
	WriteConfig('ATTACHMENT', $ATTACHMENT);
	write_log("附件设定被 $CURUSER[username] 在 $actiontime 更新。", 'mod');
	go_back();
} elseif ($action == 'savesettings_check') { //自定义考核
	stdhead($lang_settings['head_save_check_settings']);
	$validConfig = array('enablead', 'convert', 'checkmanage', 'checkname', 'checktime', 'checkstart', 'checkdate', 'checkul', 'checkdl', 'checkradio', 'checkbonus', 'checkultime', 'checkdltime', 'checktr', 'checkfa');
	GetVar($validConfig);
	$a1 = $CHECK['checkstart'];
	$a2 = $CHECK['checkdate'];
	$a3 = $CHECK['checktime'];
	unset($CHECK);
	foreach ($validConfig as $config) {
		$CHECK[$config] = $$config;
	}
	if (empty($_POST['checkstart'])) {
		$CHECK['checkstart'] = date("Y-m-d", time() + 86400 * 1);
	} else {
		$CHECK['checkstart'] = $_POST['checkstart'];
	}
	if (empty($_POST['checkdate'])) {
		$CHECK['checkdate'] = date("Y-m-d", time());
	} else {
		$CHECK['checkdate'] = $_POST['checkdate'];
	}
	$ks = date("Y-m-d", strtotime($_POST['checkstart'] . " + " . $_POST['checktime'] . " days"));
	if ($_POST['checkstart'] != $a1 || $_POST['checkdate'] != $a2 || $_POST['checktime'] != $a3) {
		$res = mysql_fetch_array(sql_query("SELECT id FROM users WHERE enabled = 'yes' AND newuser = 'no' AND donor = 'no' AND class < $assessmentmanage AND added < '" . date("Y-m-d H:i:s", strtotime($_POST['checkdate'] . " - 1 days")) . "'"));
		$fanum = sql_query("SELECT COUNT(*) FROM users WHERE status = 'normal' AND owner = " . $res['id']);
		if ($fanum) {
			$fanum = $fanum;
		} else {
			$fanum = 0;
		}
		$ress = sql_query("SELECT id, downloaded, uploaded, seedbonus, seedtime, leechtime FROM users WHERE enabled = 'yes' AND newuser = 'no' AND donor = 'no' AND class < $assessmentmanage AND added < '" . date("Y-m-d H:i:s", strtotime($_POST['checkdate'] . " - 1 days")) . "'"); //找到注册时间在指定日期之前的所有帐号，即需要考核的帐号
		if ((round((strtotime($ks) - strtotime(date("Y-m-d"))) / 86400) - 1) == $_POST['checktime']) {//考核时间开始时，记录开始时的所有条件状态
			while ($ex = mysql_fetch_assoc($ress)) {
				sql_query("UPDATE users SET exdownloaded = downloaded, exuploaded = uploaded, exseedbonus = seedbonus, exseedtime = seedtime, exleechtime = leechtime, extorrent = $fanum WHERE id = '" . $ex['id'] . "'") or sqlerr(__FILE__, __LINE__);
			}
		}
	}
	WriteConfig('CHECK', $CHECK);
	write_log("自定义考核设定被 $CURUSER[username] 在 $actiontime 更新。", 'mod');
	go_back();
} elseif ($action == 'savesettings_advertisement') { // save advertisement
	stdhead($lang_settings['head_save_advertisement_settings']);
	$validConfig = array('enablead', 'enablenoad', 'noad', 'enablebonusnoad', 'bonusnoad', 'bonusnoadpoint', 'bonusnoadtime', 'adclickbonus');
	GetVar($validConfig);
	unset($ADVERTISEMENT);
	foreach ($validConfig as $config) {
		$ADVERTISEMENT[$config] = $$config;
	}
	WriteConfig('ADVERTISEMENT', $ADVERTISEMENT);
	write_log("广告设定被 $CURUSER[username] 在 $actiontime 更新。", 'mod');
	go_back();
} elseif ($action == 'tweaksettings') {  // tweak settings
	stdhead($lang_settings['head_tweak_settings']);
	print ($notice);
	print ("<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='savesettings_tweak' />");
	yesorno($lang_settings['row_save_user_location'], 'where', $TWEAK["where"], $lang_settings['text_save_user_location_note']);
	yesorno($lang_settings['row_log_user_ips'], 'iplog1', $TWEAK["iplog1"], $lang_settings['text_store_user_ips_note']);
	yesorno("下雪效果", 'snow', $TWEAK["snow"], "默认'否'。是否开启下雪效果。");
	tr($lang_settings['row_kps_enabled'], "<input type='radio' id='bonusenable' name='bonus'" . ($TWEAK["bonus"] == "enable" ? " checked='checked'" : "") . " value='enable' /> <label for='bonusenable'>" . $lang_settings['text_enabled'] . "</label> <input type='radio' id='bonusdisablesave' name='bonus'" . ($TWEAK["bonus"] == "disablesave" ? " checked='checked'" : "") . " value='disablesave' /> <label for='bonusdisablesave'>" . $lang_settings['text_disabled_but_save'] . "</label> <input type='radio' id='bonusdisable' name='bonus'" . ($TWEAK["bonus"] == "disable" ? " checked='checked'" : "") . " value='disable' /> <label for='bonusdisable'>" . $lang_settings['text_disabled_no_save'] . "</label> <br />" . $lang_settings['text_kps_note'], 1);
	yesorno($lang_settings['row_enable_location'], 'enablelocation', $TWEAK["enablelocation"], $lang_settings['text_enable_location_note']);
	yesorno($lang_settings['row_enable_tooltip'], 'enabletooltip', $TWEAK["enabletooltip"], $lang_settings['text_enable_tooltip_note']);
	tr($lang_settings['row_title_keywords'], "<input type='text' style=\"width: 300px\" name='titlekeywords' value='" . ($TWEAK["titlekeywords"] ? $TWEAK["titlekeywords"] : '') . "' /> <br />" . $lang_settings['text_title_keywords_note'], 1);
	tr($lang_settings['row_promotion_link_example_image'], "<input type='text' style=\"width: 300px\" name='prolinkimg' value='" . ($TWEAK["prolinkimg"] ? $TWEAK["prolinkimg"] : 'pic/prolink.png') . "' /> <br />" . $lang_settings['text_promotion_link_example_note'], 1);
	tr($lang_settings['row_meta_keywords'], "<input type='text' style=\"width: 300px\" name='metakeywords' value='" . ($TWEAK["metakeywords"] ? $TWEAK["metakeywords"] : '') . "' /> <br />" . $lang_settings['text_meta_keywords_note'], 1);
	tr($lang_settings['row_meta_description'], "<textarea cols=\"100\" style=\"width: 450px;\" rows=\"5\" name='metadescription'>" . ($TWEAK["metadescription"] ? $TWEAK["metadescription"] : '') . "</textarea> <br />" . $lang_settings['text_meta_description_note'], 1);
	tr($lang_settings['row_web_analytics_code'], "<textarea cols=\"100\" style=\"width: 450px;\" rows=\"5\" name='analyticscode'>" . ($TWEAK["analyticscode"] ? $TWEAK["analyticscode"] : '') . "</textarea> <br />" . $lang_settings['text_web_analytics_code_note'], 1);
	tr("群聊区公告", "<textarea cols=\"100\" style=\"width: 450px;\" rows=\"5\" name='groupchat'>" . ($TWEAK["groupchat"] ? $TWEAK["groupchat"] : '') . "</textarea> <br />设置首页群聊区公告内容，空为不显示。", 1);
	tr($lang_settings['row_see_sql_debug'], "<input type='checkbox' name='enablesqldebug' value='yes'" . ($TWEAK['enablesqldebug'] == 'yes' ? " checked='checked'" : "") . " />" . $lang_settings['text_allow'] . classlist('sqldebug', UC_STAFFLEADER, $TWEAK['sqldebug'], UC_MODERATOR) . $lang_settings['text_see_sql_list'] . get_user_class_name(UC_SYSOP, false, true, true), 1);
	tr($lang_settings['row_tracker_founded_date'], "<input type='text' style=\"width: 300px\" class=\"Wdate\" onfocus=\"WdatePicker({isShowWeek:'true'})\" name=datefounded value='" . ($TWEAK["datefounded"] ? $TWEAK["datefounded"] : '2007-12-24') . "'> <br />" . $lang_settings['text_tracker_founded_date_note'], 1);
	tr($lang_settings['row_css_date'], "<input type='text' style=\"width: 300px\" name=cssdate value='" . ($TWEAK["cssdate"] ? $TWEAK["cssdate"] : '') . "'> <br />" . $lang_settings['text_css_date'], 1);
	tr($lang_settings['row_save_settings'], "<input type='submit' name='save' value='" . $lang_settings['submit_save_settings'] . "'>", 1);
	print ("</form>");
} elseif ($action == 'smtpsettings') { // stmp settings
	stdhead($lang_settings['head_smtp_settings']);
	print ($notice);
	print("<tbody>");
	print ("<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='savesettings_smtp'>");
	//封禁/删除预警通知邮件
	yesorno("封禁/删除预警通知", 'mailtouser', $SMTP["mailtouser"], "是否给将要被封禁或删除帐号的用户发送邮件通知。默认'否'。");
	yesorno($lang_settings['row_enable_email_notification'], 'emailnotify', $SMTP["emailnotify"], $lang_settings['text_email_notification_note']);
	$smtp_select = "<input type=\"radio\" name=\"smtptype\" value=\"default\" onclick=\"document.getElementById('smtp_advanced').style.display='none'; document.getElementById('smtp_external').style.display='none';\"" . ($SMTP['smtptype'] == "default" ? " checked" : "") . "> " . $lang_settings['text_smtp_default'] . "<br /><input type=\"radio\" name=\"smtptype\" value=\"advanced\" onclick=\"document.getElementById('smtp_advanced').style.display=''; document.getElementById('smtp_external').style.display='none';\"" . ($SMTP['smtptype'] == "advanced" ? " checked" : "") . "> " . $lang_settings['text_smtp_advanced'] . "<br /><input type=\"radio\" name=\"smtptype\" value=\"external\" onclick=\"document.getElementById('smtp_advanced').style.display='none'; document.getElementById('smtp_external').style.display='';\"" . ($SMTP['smtptype'] == "external" ? " checked" : "") . "> " . $lang_settings['text_smtp_external'] . "<br /><input type=\"radio\" name=\"smtptype\" value=\"none\" onclick=\"document.getElementById('smtp_advanced').style.display='none'; document.getElementById('smtp_external').style.display='none';\"" . ($SMTP['smtptype'] == "none" ? " checked" : "") . "> " . $lang_settings['text_smtp_none'];
	tr($lang_settings['row_mail_function_type'], $smtp_select, 1);
	print("</tbody><tbody id=\"smtp_advanced\"" . ($SMTP['smtptype'] == "advanced" ? "" : " style=\"display: none;\"") . ">");
	print("<tr><td colspan=2 align=center><b>" . $lang_settings['text_setting_for_advanced_type'] . "</b></td></tr>");
	tr($lang_settings['row_smtp_host'], "<input type='text' style=\"width: 300px\" name=smtp_host value='" . ($SMTP['smtp_host'] ? $SMTP['smtp_host'] : "localhost") . "'> " . $lang_settings['text_smtp_host_note'], 1);
	tr($lang_settings['row_smtp_port'], "<input type='text' style=\"width: 300px\" name=smtp_port value='" . ($SMTP['smtp_port'] ? $SMTP['smtp_port'] : "25") . "'> " . $lang_settings['text_smtp_port_note'], 1);
	if (strtoupper(substr(PHP_OS, 0, 3) == 'WIN'))
		tr($lang_settings['row_smtp_sendmail_from'], "<input type='text' style=\"width: 300px\" name=smtp_from value='" . ($SMTP['smtp_from'] ? $SMTP['smtp_from'] : $MAIN["SITEEMAIL"]) . "'> " . $lang_settings['text_smtp_sendmail_from_note'] . $MAIN["SITEEMAIL"], 1);
	else
		tr($lang_settings['row_smtp_sendmail_path'], $lang_settings['text_smtp_sendmail_path_note'], 1);
	print("</tbody><tbody id=\"smtp_external\"" . ($SMTP['smtptype'] == "external" ? "" : " style=\"display: none;\"") . ">");
	print("<tr><td colspan=2 align=center><b>" . $lang_settings['text_setting_for_external_type'] . "</b></td></tr>");
	tr($lang_settings['row_outgoing_mail_address'], "<input type=text name=smtpaddress style=\"width: 300px\" " . ($SMTP['smtpaddress'] ? "value=\"" . $SMTP['smtpaddress'] . "\"" : "") . "> " . $lang_settings['text_outgoing_mail_address_note'], 1);
	tr($lang_settings['row_outgoing_mail_port'], "<input type=text name=smtpport style=\"width: 300px\" " . ($SMTP['smtpport'] ? "value=\"" . $SMTP['smtpport'] . "\"" : "") . "> " . $lang_settings['text_outgoing_mail_port_note'], 1);
	tr($lang_settings['row_smtp_account_name'], "<input type=text name=accountname style=\"width: 300px\" " . ($SMTP['accountname'] ? "value=\"" . $SMTP['accountname'] . "\"" : "") . "> " . $lang_settings['text_smtp_account_name_note'], 1);
	tr($lang_settings['row_smtp_account_password'], "<input type=password name=accountpassword style=\"width: 300px\" " . ($SMTP['accountpassword'] ? "value=\"" . $SMTP['accountpassword'] . "\"" : "") . "> " . $lang_settings['text_smtp_account_password_note'], 1);
	print("</tbody><tbody>");
	tr($lang_settings['row_save_settings'], "<input type='submit' name='save' value='" . $lang_settings['submit_save_settings'] . "'>", 1);
	print ("<tr><td colspan=2 align=center>" . $lang_settings['text_mail_test_note'] . "<a href=\"mailtest.php\" target=\"_blank\"><b>" . $lang_settings['text_here'] . "</b></a></td></tr>");
	print ("</form>");
	print("</tbody>");
} elseif ($action == 'securitysettings') { //security settings
	stdhead($lang_settings['head_security_settings']);
	print ($notice);
	$maxclass = UC_SYSOP;
	print ("<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='savesettings_security'>");
	tr($lang_settings['row_enable_ssl'], "<input type='radio' name='securelogin'" . ($SECURITY["securelogin"] == "yes" ? " checked" : "") . " value='yes'> " . $lang_settings['text_yes'] . " <input type='radio' name='securelogin'" . ($SECURITY["securelogin"] == "no" ? " checked" : "") . " value='no'> " . $lang_settings['text_no'] . " <input type='radio' name='securelogin'" . ($SECURITY["securelogin"] == "op" ? " checked" : "") . " value='op'> " . $lang_settings['text_optional'] . "<br />" . $lang_settings['text_ssl_note'], 1);
	tr($lang_settings['row_enable_ssl_tracker'], "<input type='radio' name='securetracker'" . ($SECURITY["securetracker"] == "yes" ? " checked" : "") . " value='yes'> " . $lang_settings['text_yes'] . " <input type='radio' name='securetracker'" . ($SECURITY["securetracker"] == "no" ? " checked" : "") . " value='no'> " . $lang_settings['text_no'] . " <input type='radio' name='securetracker'" . ($SECURITY["securetracker"] == "op" ? " checked" : "") . " value='op'> " . $lang_settings['text_optional'] . "<br />" . $lang_settings['text_ssl_note'], 1);
	tr($lang_settings['row_https_announce_url'], "<input type='text' style=\"width: 300px\" name=https_announce_url value='" . ($SECURITY["https_announce_url"] ? $SECURITY["https_announce_url"] : "") . "'> " . $lang_settings['text_https_announce_url_note'] . $_SERVER["HTTP_HOST"] . "/announce.php", 1);
	yesorno($lang_settings['row_enable_image_verification'], 'iv', $SECURITY["iv"], $lang_settings['text_image_verification_note']);
	yesorno($lang_settings['row_allow_email_change'], 'changeemail', $SECURITY["changeemail"], $lang_settings['text_email_change_note']);
	yesorno("启用安全模式", 'onsecurity', $SECURITY["onsecurity"], "默认'否'。设为'否'将展示成论坛，隐藏种子列表等。");
	yesorno("关闭禁区", 'limitsecurity', $SECURITY["limitsecurity"], "默认'否'。设为'否'将彻底关闭禁区的展示。");
	tr("查看禁区", $lang_settings['text_minimum_class'] . classlist('viewlimit', $maxclass, $SECURITY['viewlimit']) . $lang_settings['text_default'] . get_user_class_name(UC_USER, false, true, true) . "。允许的最低可以查看禁区的等级。", 1);
	tr("免费查看禁区", $lang_settings['text_minimum_class'] . classlist('freeviewlimit', $maxclass, $SECURITY['freeviewlimit']) . $lang_settings['text_default'] . get_user_class_name(UC_ULTIMATE_USER, false, true, true) . "。可以任意查看禁区内容。", 1);
	tr("查看禁区所需魔力值", "<input type='text' name=limitbonus style=\"width: 100px\" value='$SECURITY[limitbonus]' /> 单位为个。设置用户每次限时（24小时）查看禁区内容所要花费的魔力值。默认'200'。", 1);
	yesorno("开启扫码登录", 'authoff', $SECURITY['authoff'], "默认'否'。设为'是'时，下列设置项必须填写。请在 <a href='https://www.yangcong.com/' target='_blank' rel='nofollow'><b>洋葱API</b></a> 网站申请并创建应用。");
	if ($SECURITY['authoff'] == 'yes') {
		tr("洋葱应用ID", "<input type='text' style='width: 300px' name='appid' value='$SECURITY[appid]' />", 1);
		tr("洋葱应用KEY", "<input type='text' style='width: 300px' name='appkey' value='$SECURITY[appkey]' />", 1);
		tr("洋葱授权ID", "<input type='text' style='width: 300px' name='authid' value='$SECURITY[authid]' />", 1);
		tr("验证方式", "<input type='text' style='width: 100px' name='authtype' value='$SECURITY[authtype]' /> 可选验证类型码：1(点击确定按钮，默认)、2(使用手势密码)、3(人脸验证)、4(声音验证)", 1);
		yesorno("绑定奖励", 'bindrewardoff', $SECURITY['bindrewardoff'], "默认'否'。设置是否开启绑定洋葱API的一次性奖励。");
		tr("绑定奖励数量", "<input type='text' style='width: 100px' name='bindrewardnum' value='$SECURITY[bindrewardnum]' /> 单位'个'。一次性奖励的魔力值。", 1);
		yesorno("登录奖励", 'loginrewardoff', $SECURITY['loginrewardoff'], "默认'否'。设置是否开启绑定洋葱API后的每日登录奖励。");
		tr("登录奖励倍数", "<input type='text' style='width: 100px' name='loginrewardmul' value='$SECURITY[loginrewardmul]' /> 单位'倍'。每日登录奖励的倍数。", 1);
	} else {
		tr("洋葱应用ID", "<input type='text' style='width: 300px' name='appid' value='$SECURITY[appid]' disabled />", 1);
		tr("洋葱应用KEY", "<input type='text' style='width: 300px' name='appkey' value='$SECURITY[appkey]' disabled />", 1);
		tr("洋葱授权ID", "<input type='text' style='width: 300px' name='authid' value='$SECURITY[authid]' disabled />", 1);
		tr("验证方式", "<input type='text' style='width: 100px' name='authtype' value='$SECURITY[authtype]' disabled /> 可选验证类型码：1(点击确定按钮，默认)、2(使用手势密码)、3(人脸验证)、4(声音验证)", 1);
		yesorno("绑定奖励", 'bindrewardoff', $SECURITY['bindrewardoff'], "默认'否'。设置是否开启绑定洋葱API的一次性奖励。", "disabled");
		tr("绑定奖励数量", "<input type='text' style='width: 100px' name='bindrewardnum' value='$SECURITY[bindrewardnum]' disabled /> 单位'个'。一次性奖励的魔力值。", 1);
		yesorno("登录奖励", 'loginrewardoff', $SECURITY['loginrewardoff'], "默认'否'。设置是否开启绑定洋葱API后的每日登录奖励。", "disabled");
		tr("登录奖励倍数", "<input type='text' style='width: 100px' name='loginrewardmul' value='$SECURITY[loginrewardmul]' disabled /> 单位'倍'。每日登录奖励的倍数。", 1);
	}
	yesorno("开启贴图库上传插件", 'ttkoff', $SECURITY['ttkoff'], "默认'否'。设为'是'时，下列设置项必须填写。请在 <a href='http://open.tietuku.cn/createtoken/' target='_blank' rel='nofollow'><b>贴图库</b></a> 网站申请并创建相册。");
	if ($SECURITY['ttkoff'] == 'yes') {
		tr("贴图库上传Token", "<input type='text' style='width: 300px' name='ttktoken' value='$SECURITY[ttktoken]' />", 1);
	} else {
		tr("贴图库上传Token", "<input type='text' style='width: 300px' name='ttktoken' value='$SECURITY[ttktoken]' disabled />", 1);
	}
	tr($lang_settings['row_cheater_detection_level'], "<select name='cheaterdet'><option value=0 " . ($SECURITY["cheaterdet"] == 0 ? " selected" : "") . "> " . $lang_settings['select_none'] . " </option><option value=1 " . ($SECURITY["cheaterdet"] == 1 ? " selected" : "") . "> " . $lang_settings['select_conservative'] . " </option><option value=2 " . ($SECURITY["cheaterdet"] == 2 ? " selected" : "") . "> " . $lang_settings['select_normal'] . " </option><option value=3 " . ($SECURITY["cheaterdet"] == 3 ? " selected" : "") . "> " . $lang_settings['select_strict'] . " </option><option value=4 " . ($SECURITY["cheaterdet"] == 4 ? " selected" : "") . "> " . $lang_settings['select_paranoid'] . " </option></select> " . $lang_settings['text_cheater_detection_level_note'] . "<br />" . $lang_settings['text_never_suspect'] . " " . classlist('nodetect', $AUTHORITY['staffmem'], $SECURITY['nodetect']) . " " . $lang_settings['text_or_above'] . get_user_class_name(UC_UPLOADER, false, true, true) . ".", 1);
	tr($lang_settings['row_max_ips'], "<input type='text' style=\"width: 300px\" name=maxip value='" . ($SECURITY["maxip"] ? $SECURITY["maxip"] : "1") . "'> " . $lang_settings['text_max_ips_note'], 1);
	tr($lang_settings['row_max_login_attemps'], "<input type='text' style=\"width: 300px\" name=maxloginattempts value='" . ($SECURITY["maxloginattempts"] ? $SECURITY["maxloginattempts"] : "7") . "'> " . $lang_settings['text_max_login_attemps_note'], 1);

	tr($lang_settings['row_save_settings'], "<input type='submit' name='save' value='" . $lang_settings['submit_save_settings'] . "'>", 1);
	print ("</form>");
} elseif ($action == 'authoritysettings') { //Authority settings
	stdhead($lang_settings['head_authority_settings']);
	print ($notice);
	$maxclass = UC_SYSOP;
	print ("<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='savesettings_authority'>");
	tr($lang_settings['row_default_class'], $lang_settings['text_default_user_class'] . classlist('defaultclass', UC_STAFFLEADER, $AUTHORITY['defaultclass']) . $lang_settings['text_default'] . get_user_class_name(UC_USER, false, true, true) . $lang_settings['text_default_class_note'], 1);
	tr($lang_settings['row_staff_member'], $lang_settings['text_minimum_class'] . classlist('staffmem', UC_STAFFLEADER, $AUTHORITY['staffmem']) . $lang_settings['text_default'] . get_user_class_name(UC_MODERATOR, false, true, true) . $lang_settings['text_staff_member_note'], 1);
	tr($lang_settings['row_news_management'], $lang_settings['text_minimum_class'] . classlist('newsmanage', $maxclass, $AUTHORITY['newsmanage']) . $lang_settings['text_default'] . get_user_class_name(UC_ADMINISTRATOR, false, true, true) . $lang_settings['text_news_management_note'], 1);
	tr($lang_settings['row_post_funbox_item'], $lang_settings['text_minimum_class'] . classlist('newfunitem', $maxclass, $AUTHORITY['newfunitem']) . $lang_settings['text_default'] . get_user_class_name(UC_USER, false, true, true) . $lang_settings['text_post_funbox_item_note'], 1);
	tr($lang_settings['row_funbox_management'], $lang_settings['text_minimum_class'] . classlist('funmanage', $maxclass, $AUTHORITY['funmanage']) . $lang_settings['text_default'] . get_user_class_name(UC_MODERATOR, false, true, true) . $lang_settings['text_funbox_management_note'], 1);
	tr($lang_settings['row_shoutbox_management'], $lang_settings['text_minimum_class'] . classlist('sbmanage', $maxclass, $AUTHORITY['sbmanage']) . $lang_settings['text_default'] . get_user_class_name(UC_MODERATOR, false, true, true) . $lang_settings['text_shoutbox_management_note'], 1);
	tr($lang_settings['row_poll_management'], $lang_settings['text_minimum_class'] . classlist('pollmanage', $maxclass, $AUTHORITY['pollmanage']) . $lang_settings['text_default'] . get_user_class_name(UC_ADMINISTRATOR, false, true, true) . $lang_settings['text_poll_management_note'], 1);
	tr($lang_settings['row_apply_for_links'], $lang_settings['text_minimum_class'] . classlist('applylink', $maxclass, $AUTHORITY['applylink']) . $lang_settings['text_default'] . get_user_class_name(UC_USER, false, true, true) . $lang_settings['text_apply_for_links_note'], 1);
	tr($lang_settings['row_link_management'], $lang_settings['text_minimum_class'] . classlist('linkmanage', $maxclass, $AUTHORITY['linkmanage']) . $lang_settings['text_default'] . get_user_class_name(UC_ADMINISTRATOR, false, true, true) . $lang_settings['text_link_management_note'], 1);
	tr($lang_settings['row_forum_post_management'], $lang_settings['text_minimum_class'] . classlist('postmanage', $maxclass, $AUTHORITY['postmanage']) . $lang_settings['text_default'] . get_user_class_name(UC_MODERATOR, false, true, true) . $lang_settings['text_forum_post_management_note'], 1);
	tr($lang_settings['row_comment_management'], $lang_settings['text_minimum_class'] . classlist('commanage', $maxclass, $AUTHORITY['commanage']) . $lang_settings['text_default'] . get_user_class_name(UC_MODERATOR, false, true, true) . $lang_settings['text_comment_management_note'], 1);
	tr($lang_settings['row_forum_management'], $lang_settings['text_minimum_class'] . classlist('forummanage', $maxclass, $AUTHORITY['forummanage']) . $lang_settings['text_default'] . get_user_class_name(UC_ADMINISTRATOR, false, true, true) . $lang_settings['text_forum_management_note'], 1);
	tr($lang_settings['row_view_userlist'], $lang_settings['text_minimum_class'] . classlist('viewuserlist', $maxclass, $AUTHORITY['viewuserlist']) . $lang_settings['text_default'] . get_user_class_name(UC_POWER_USER, false, true, true) . $lang_settings['text_view_userlist_note'], 1);
	tr($lang_settings['row_torrent_management'], $lang_settings['text_minimum_class'] . classlist('torrentmanage', $maxclass, $AUTHORITY['torrentmanage']) . $lang_settings['text_default'] . get_user_class_name(UC_MODERATOR, false, true, true) . $lang_settings['text_torrent_management_note'], 1);
	tr($lang_settings['row_torrent_sticky'], $lang_settings['text_minimum_class'] . classlist('torrentsticky', $maxclass, $AUTHORITY['torrentsticky']) . $lang_settings['text_default'] . get_user_class_name(UC_ADMINISTRATOR, false, true, true) . $lang_settings['text_torrent_sticky_note'], 1);
	tr($lang_settings['row_torrent_on_promotion'], $lang_settings['text_minimum_class'] . classlist('torrentonpromotion', $maxclass, $AUTHORITY['torrentonpromotion']) . $lang_settings['text_default'] . get_user_class_name(UC_ADMINISTRATOR, false, true, true) . $lang_settings['text_torrent_promotion_note'], 1);
	tr($lang_settings['row_ask_for_reseed'], $lang_settings['text_minimum_class'] . classlist('askreseed', $maxclass, $AUTHORITY['askreseed']) . $lang_settings['text_default'] . get_user_class_name(UC_POWER_USER, false, true, true) . $lang_settings['text_ask_for_reseed_note'], 1);
	tr($lang_settings['row_view_nfo'], $lang_settings['text_minimum_class'] . classlist('viewnfo', $maxclass, $AUTHORITY['viewnfo']) . $lang_settings['text_default'] . get_user_class_name(UC_POWER_USER, false, true, true) . $lang_settings['text_view_nfo_note'], 1);
	tr($lang_settings['row_view_torrent_structure'], $lang_settings['text_minimum_class'] . classlist('torrentstructure', $maxclass, $AUTHORITY['torrentstructure']) . $lang_settings['text_default'] . get_user_class_name(UC_ULTIMATE_USER, false, true, true) . $lang_settings['text_view_torrent_structure_note'], 1);
	tr($lang_settings['row_send_invite'], $lang_settings['text_minimum_class'] . classlist('sendinvite', $maxclass, $AUTHORITY['sendinvite']) . $lang_settings['text_default'] . get_user_class_name(UC_POWER_USER, false, true, true) . $lang_settings['text_send_invite_note'], 1);
	tr("特权发送邀请", $lang_settings['text_minimum_class'] . classlist('Tsendinvite', $maxclass, $AUTHORITY['Tsendinvite']) . $lang_settings['text_default'] . get_user_class_name(UC_VIP, false, true, true) . "。关闭邀请期间仍然可以发送邀请的等级。", 1);
	tr($lang_settings['row_view_history'], $lang_settings['text_minimum_class'] . classlist('viewhistory', $maxclass, $AUTHORITY['viewhistory']) . $lang_settings['text_default'] . get_user_class_name(UC_VETERAN_USER, false, true, true) . $lang_settings['text_view_history_note'], 1);
	tr($lang_settings['row_view_topten'], $lang_settings['text_minimum_class'] . classlist('topten', $maxclass, $AUTHORITY['topten']) . $lang_settings['text_default'] . get_user_class_name(UC_POWER_USER, false, true, true) . $lang_settings['text_view_topten_note'], 1);
	tr($lang_settings['row_view_general_log'], $lang_settings['text_minimum_class'] . classlist('log', $maxclass, $AUTHORITY['log']) . $lang_settings['text_default'] . get_user_class_name(UC_INSANE_USER, false, true, true) . $lang_settings['text_view_general_log_note'], 1);
	tr($lang_settings['row_view_confidential_log'], $lang_settings['text_minimum_class'] . classlist('confilog', $maxclass, $AUTHORITY['confilog']) . $lang_settings['text_default'] . get_user_class_name(UC_MODERATOR, false, true, true) . $lang_settings['text_view_confidential_log_note'], 1);
	tr($lang_settings['row_view_user_confidential'], $lang_settings['text_minimum_class'] . classlist('userprofile', $maxclass, $AUTHORITY['userprofile']) . $lang_settings['text_default'] . get_user_class_name(UC_ADMINISTRATOR, false, true, true) . $lang_settings['text_view_user_confidential_note'], 1);
	tr($lang_settings['row_view_user_torrent'], $lang_settings['text_minimum_class'] . classlist('torrenthistory', $maxclass, $AUTHORITY['torrenthistory']) . $lang_settings['text_default'] . get_user_class_name(UC_POWER_USER, false, true, true) . $lang_settings['text_view_user_torrent_note'], 1);
	tr($lang_settings['row_general_profile_management'], $lang_settings['text_minimum_class'] . classlist('prfmanage', $maxclass, $AUTHORITY['prfmanage']) . $lang_settings['text_default'] . get_user_class_name(UC_MODERATOR, false, true, true) . $lang_settings['text_general_profile_management_note'], 1);
	tr($lang_settings['row_crucial_profile_management'], $lang_settings['text_minimum_class'] . classlist('cruprfmanage', $maxclass, $AUTHORITY['cruprfmanage']) . $lang_settings['text_default'] . get_user_class_name(UC_ADMINISTRATOR, false, true, true) . $lang_settings['text_crucial_profile_management_note'] . get_user_class_name(UC_STAFFLEADER, false, true, true) . $lang_settings['text_can_manage_donation'], 1);
	tr($lang_settings['row_upload_subtitle'], $lang_settings['text_minimum_class'] . classlist('uploadsub', $maxclass, $AUTHORITY['uploadsub']) . $lang_settings['text_default'] . get_user_class_name(UC_USER, false, true, true) . $lang_settings['text_upload_subtitle_note'], 1);
	tr($lang_settings['row_delete_own_subtitle'], $lang_settings['text_minimum_class'] . classlist('delownsub', $maxclass, $AUTHORITY['delownsub']) . $lang_settings['text_default'] . get_user_class_name(UC_POWER_USER, false, true, true) . $lang_settings['text_delete_own_subtitle_note'], 1);
	tr($lang_settings['row_subtitle_management'], $lang_settings['text_minimum_class'] . classlist('submanage', $maxclass, $AUTHORITY['submanage']) . $lang_settings['text_default'] . get_user_class_name(UC_MODERATOR, false, true, true) . $lang_settings['text_subtitle_management'], 1);
	tr($lang_settings['row_update_external_info'], $lang_settings['text_minimum_class'] . classlist('updateextinfo', $maxclass, $AUTHORITY['updateextinfo']) . $lang_settings['text_default'] . get_user_class_name(UC_EXTREME_USER, false, true, true) . $lang_settings['text_update_external_info_note'], 1);
	tr($lang_settings['row_view_anonymous'], $lang_settings['text_minimum_class'] . classlist('viewanonymous', $maxclass, $AUTHORITY['viewanonymous']) . $lang_settings['text_default'] . get_user_class_name(UC_UPLOADER, false, true, true) . $lang_settings['text_view_anonymous_note'], 1);
	tr($lang_settings['row_be_anonymous'], $lang_settings['text_minimum_class'] . classlist('beanonymous', $maxclass, $AUTHORITY['beanonymous']) . $lang_settings['text_default'] . get_user_class_name(UC_CRAZY_USER, false, true, true) . $lang_settings['text_be_anonymous_note'], 1);
	tr($lang_settings['row_add_offer'], $lang_settings['text_minimum_class'] . classlist('addoffer', $maxclass, $AUTHORITY['addoffer']) . $lang_settings['text_default'] . get_user_class_name(UC_PEASANT, false, true, true) . $lang_settings['text_add_offer_note'], 1);
	tr($lang_settings['row_offer_management'], $lang_settings['text_minimum_class'] . classlist('offermanage', $maxclass, $AUTHORITY['offermanage']) . $lang_settings['text_default'] . get_user_class_name(UC_MODERATOR, false, true, true) . $lang_settings['text_offer_management_note'], 1);
	tr($lang_settings['row_upload_torrent'], $lang_settings['text_minimum_class'] . classlist('upload', $maxclass, $AUTHORITY['upload']) . $lang_settings['text_default'] . get_user_class_name(UC_POWER_USER, false, true, true) . $lang_settings['text_upload_torrent_note'], 1);
	tr($lang_settings['row_chronicle_management'], $lang_settings['text_minimum_class'] . classlist('chrmanage', $maxclass, $AUTHORITY['chrmanage']) . $lang_settings['text_default'] . get_user_class_name(UC_MODERATOR, false, true, true) . $lang_settings['text_chronicle_management_note'], 1);
	tr($lang_settings['row_view_invite'], $lang_settings['text_minimum_class'] . classlist('viewinvite', $maxclass, $AUTHORITY['viewinvite']) . $lang_settings['text_default'] . get_user_class_name(UC_MODERATOR, false, true, true) . $lang_settings['text_view_invite_note'], 1);
	tr($lang_settings['row_buy_invites'], $lang_settings['text_minimum_class'] . classlist('buyinvite', $maxclass, $AUTHORITY['buyinvite']) . $lang_settings['text_default'] . get_user_class_name(UC_INSANE_USER, false, true, true) . $lang_settings['text_buy_invites_note'], 1);
	tr($lang_settings['row_see_banned_torrents'], $lang_settings['text_minimum_class'] . classlist('seebanned', $maxclass, $AUTHORITY['seebanned']) . $lang_settings['text_default'] . get_user_class_name(UC_UPLOADER, false, true, true) . $lang_settings['text_see_banned_torrents_note'], 1);
	tr($lang_settings['row_vote_against_offers'], $lang_settings['text_minimum_class'] . classlist('againstoffer', $maxclass, $AUTHORITY['againstoffer']) . $lang_settings['text_default'] . get_user_class_name(UC_USER, false, true, true) . $lang_settings['text_vote_against_offers_note'], 1);
	tr($lang_settings['row_allow_userbar'], $lang_settings['text_minimum_class'] . classlist('userbar', $maxclass, $AUTHORITY['userbar']) . $lang_settings['text_default'] . get_user_class_name(UC_POWER_USER, false, true, true) . $lang_settings['text_allow_userbar_note'], 1);
	tr($lang_settings['row_hr_management'], $lang_settings['minimum_class'] . classlist('hrmanage', $maxclass, $AUTHORITY['hrmanage']) . $lang_settings['text_default'] . get_user_class_name(UC_VIP, false, true, true) . $lang_settings['text_hr_management'], 1);
	tr($lang_settings['row_newuser_management'], $lang_settings['minimum_class'] . classlist('newusermanage', $maxclass, $AUTHORITY['newusermanage']) . $lang_settings['text_default'] . get_user_class_name(UC_VIP, false, true, true) . $lang_settings['text_newuser_management'], 1);
	tr($lang_settings['row_share_management'], $lang_settings['share_minimum_class'] . classlist('sharemanage', $maxclass, $AUTHORITY['sharemanage']) . $lang_settings['text_default'] . get_user_class_name(UC_VIP, false, true, true) . $lang_settings['text_share_management'], 1);
	tr($lang_settings['row_save_settings'], "<input type='submit' name='save' value='" . $lang_settings['submit_save_settings'] . "'>", 1);
	print ("</form>");
} elseif ($action == 'basicsettings') { // basic settings
	stdhead($lang_settings['head_basic_settings']);
	print ($notice);
	print ("<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='savesettings_basic'>");
	tr($lang_settings['row_site_name'], "<input type='text' style=\"width: 300px\" name=SITENAME value='" . ($BASIC["SITENAME"] ? $BASIC["SITENAME"] : "Nexus") . "'> " . $lang_settings['text_site_name_note'], 1);
	tr($lang_settings['row_base_url'], "<input type='text' style=\"width: 300px\" name=BASEURL value='" . ($BASIC["BASEURL"] ? $BASIC["BASEURL"] : $_SERVER["HTTP_HOST"]) . "'> " . $lang_settings['text_it_should_be'] . $_SERVER["HTTP_HOST"] . $lang_settings['text_base_url_note'], 1);
	tr($lang_settings['row_announce_url'], "<input type='text' style=\"width: 300px\" name=announce_url value='" . ($BASIC["announce_url"] ? $BASIC["announce_url"] : $_SERVER["HTTP_HOST"] . "/announce.php") . "'> " . $lang_settings['text_it_should_be'] . $_SERVER["HTTP_HOST"] . "/announce.php", 1);
	tr($lang_settings['row_mysql_host'], "<input type='text' style=\"width: 300px\" name=mysql_host value='" . ($BASIC["mysql_host"] ? $BASIC["mysql_host"] : "localhost") . "'> " . $lang_settings['text_mysql_host_note'], 1);
	tr($lang_settings['row_mysql_user'], "<input type='text' style=\"width: 300px\" name=mysql_user value='" . ($BASIC["mysql_user"] ? $BASIC["mysql_user"] : "root") . "'> " . $lang_settings['text_mysql_user_note'], 1);
	tr($lang_settings['row_mysql_password'], "<input type='password' style=\"width: 300px\" name=mysql_pass value=''> " . $lang_settings['text_mysql_password_note'], 1);
	tr($lang_settings['row_mysql_database_name'], "<input type='text' style=\"width: 300px\" name=mysql_db value='" . ($BASIC["mysql_db"] ? $BASIC["mysql_db"] : "nexus") . "'> " . $lang_settings['text_mysql_database_name_note'], 1);
	tr($lang_settings['row_save_settings'], "<input type='submit' name='save' value='" . $lang_settings['submit_save_settings'] . "'>", 1);
	print ("</form>");
} elseif ($action == 'attachmentsettings') { // basic settings
	stdhead($lang_settings['head_attachment_settings']);
	print ($notice);
	print ("<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='savesettings_attachment'>");
	yesorno($lang_settings['row_enable_attachment'], 'enableattach', $ATTACHMENT["enableattach"], $lang_settings['text_enable_attachment_note']);
	tr($lang_settings['row_attachment_authority'], $lang_settings['text_attachment_authority_note_one'] . "<ul><li>" . classlist('classone', UC_STAFFLEADER, $ATTACHMENT['classone']) . $lang_settings['text_can_upload_at_most'] . "<input type='text' style=\"width: 50px\" name=\"countone\" value='" . ($ATTACHMENT['countone'] ? $ATTACHMENT['countone'] : '') . "'> " . $lang_settings['text_file_size_below'] . "<input type='text' style=\"width: 50px\" name=\"sizeone\" value='" . ($ATTACHMENT['sizeone'] ? $ATTACHMENT['sizeone'] : '') . "'>" . $lang_settings['text_with_extension_name'] . "<input type='text' style=\"width: 200px\" name=\"extone\" value='" . ($ATTACHMENT['extone'] ? $ATTACHMENT['extone'] : '') . "'>" . $lang_settings['text_authority_default_one_one'] . get_user_class_name(UC_USER, false, true, true) . $lang_settings['text_authority_default_one_two'] . "</li><li>" . classlist('classtwo', UC_STAFFLEADER, $ATTACHMENT['classtwo']) . $lang_settings['text_can_upload_at_most'] . "<input type='text' style=\"width: 50px\" name=\"counttwo\" value='" . ($ATTACHMENT['counttwo'] ? $ATTACHMENT['counttwo'] : '') . "'> " . $lang_settings['text_file_size_below'] . "<input type='text' style=\"width: 50px\" name=\"sizetwo\" value='" . ($ATTACHMENT['sizetwo'] ? $ATTACHMENT['sizetwo'] : '') . "'>" . $lang_settings['text_with_extension_name'] . "<input type='text' style=\"width: 200px\" name=\"exttwo\" value='" . ($ATTACHMENT['exttwo'] ? $ATTACHMENT['exttwo'] : '') . "'>" . $lang_settings['text_authority_default_two_one'] . get_user_class_name(UC_POWER_USER, false, true, true) . $lang_settings['text_authority_default_two_two'] . "</li><li>" . classlist('classthree', UC_STAFFLEADER, $ATTACHMENT['classthree']) . $lang_settings['text_can_upload_at_most'] . "<input type='text' style=\"width: 50px\" name=\"countthree\" value='" . ($ATTACHMENT['countthree'] ? $ATTACHMENT['countthree'] : '') . "'> " . $lang_settings['text_file_size_below'] . "<input type='text' style=\"width: 50px\" name=\"sizethree\" value='" . ($ATTACHMENT['sizethree'] ? $ATTACHMENT['sizethree'] : '') . "'>" . $lang_settings['text_with_extension_name'] . "<input type='text' style=\"width: 200px\" name=\"extthree\" value='" . ($ATTACHMENT['extthree'] ? $ATTACHMENT['extthree'] : '') . "'>" . $lang_settings['text_authority_default_three_one'] . get_user_class_name(UC_INSANE_USER, false, true, true) . $lang_settings['text_authority_default_three_two'] . "</li><li>" . classlist('classfour', UC_STAFFLEADER, $ATTACHMENT['classfour']) . $lang_settings['text_can_upload_at_most'] . "<input type='text' style=\"width: 50px\" name=\"countfour\" value='" . ($ATTACHMENT['countfour'] ? $ATTACHMENT['countfour'] : '') . "'> " . $lang_settings['text_file_size_below'] . "<input type='text' style=\"width: 50px\" name=\"sizefour\" value='" . ($ATTACHMENT['sizefour'] ? $ATTACHMENT['sizefour'] : '') . "'>" . $lang_settings['text_with_extension_name'] . "<input type='text' style=\"width: 200px\" name=\"extfour\" value='" . ($ATTACHMENT['extfour'] ? $ATTACHMENT['extfour'] : '') . "'>" . $lang_settings['text_authority_default_four_one'] . get_user_class_name(UC_MODERATOR, false, true, true) . $lang_settings['text_authority_default_four_two'] . "</li></ul>" . $lang_settings['text_attachment_authority_note_two'], 1);
	tr($lang_settings['row_save_directory'], "<input type='text' style=\"width: 300px\" name=\"savedirectory\" value='" . ($ATTACHMENT['savedirectory'] ? $ATTACHMENT['savedirectory'] : "./attachments") . "'> " . $lang_settings['text_save_directory_note'], 1);
	tr($lang_settings['row_http_directory'], "<input type='text' style=\"width: 300px\" name=\"httpdirectory\" value='" . ($ATTACHMENT['httpdirectory'] ? $ATTACHMENT['httpdirectory'] : "attachments") . "'> " . $lang_settings['text_http_directory_note'], 1);
	tr($lang_settings['row_save_directory_type'], "<input type='radio' name='savedirectorytype' value='onedir'" . ($ATTACHMENT['savedirectorytype'] == "onedir" ? " checked=\"checked\"" : "") . ">" . $lang_settings['text_one_directory'] . "<br /><input type='radio' name='savedirectorytype' value='monthdir'" . ($ATTACHMENT['savedirectorytype'] == "monthdir" ? " checked=\"checked\"" : "") . ">" . $lang_settings['text_directories_by_monthes'] . "<br /><input type='radio' name='savedirectorytype' value='daydir'" . ($ATTACHMENT['savedirectorytype'] == "daydir" ? " checked=\"checked\"" : "") . ">" . $lang_settings['text_directories_by_days'] . "<br />" . $lang_settings['text_save_directory_type_note'], 1);
	tr($lang_settings['row_image_thumbnails'], "<input type='radio' name='thumbnailtype' value='no' " . ($ATTACHMENT["thumbnailtype"] == 'no' ? " checked=\"checked\"" : "") . "> " . $lang_settings['text_no_thumbnail'] . "<br><input type='radio' name='thumbnailtype' value='createthumb' " . ($ATTACHMENT["thumbnailtype"] == 'createthumb' ? " checked=\"checked\"" : "") . "> " . $lang_settings['text_create_thumbnail'] . "<br><input type='radio' name='thumbnailtype' value='resizebigimg' " . ($ATTACHMENT["thumbnailtype"] == 'resizebigimg' ? " checked=\"checked\"" : "") . "> " . $lang_settings['text_resize_big_image'] . "<br>" . $lang_settings['text_image_thumbnail_note'], 1);
	tr($lang_settings['row_thumbnail_quality'], "<input type='text' style=\"width: 100px\" name=\"thumbquality\" value='" . ($ATTACHMENT['thumbquality'] ? $ATTACHMENT['thumbquality'] : '80') . "'> " . $lang_settings['text_thumbnail_quality_note'], 1);
	tr($lang_settings['row_thumbnail_size'], "<input type='text' style=\"width: 100px\" name=\"thumbwidth\" value='" . ($ATTACHMENT['thumbwidth'] ? $ATTACHMENT['thumbwidth'] : '500') . "'> * <input type='text' style=\"width: 100px\" name=\"thumbheight\" value='" . ($ATTACHMENT['thumbheight'] ? $ATTACHMENT['thumbheight'] : '500') . "'> " . $lang_settings['text_thumbnail_size_note'], 1);
	tr($lang_settings['row_alternative_thumbnail_size'], "<input type='text' style=\"width: 100px\" name=\"altthumbwidth\" value='" . ($ATTACHMENT['altthumbwidth'] ? $ATTACHMENT['altthumbwidth'] : '180') . "'> * <input type='text' style=\"width: 100px\" name=\"altthumbheight\" value='" . ($ATTACHMENT['altthumbheight'] ? $ATTACHMENT['altthumbheight'] : '135') . "'> " . $lang_settings['text_alternative_thumbnail_size_note'], 1);
	tr($lang_settings['row_watermark'], "<input type='radio' name='watermarkpos' value='no' " . ($ATTACHMENT["watermarkpos"] == 'no' ? " checked=\"checked\"" : "") . "> " . $lang_settings['text_no_watermark'] . "<br><input type='radio' name='watermarkpos' value='1' " . ($ATTACHMENT["watermarkpos"] == '1' ? " checked=\"checked\"" : "") . "> " . $lang_settings['text_left_top'] . "<input type='radio' name='watermarkpos' value='2' " . ($ATTACHMENT["watermarkpos"] == '2' ? " checked=\"checked\"" : "") . "> " . $lang_settings['text_top'] . "<input type='radio' name='watermarkpos' value='3' " . ($ATTACHMENT["watermarkpos"] == '3' ? " checked=\"checked\"" : "") . "> " . $lang_settings['text_right_top'] . "<br><input type='radio' name='watermarkpos' value='4' " . ($ATTACHMENT["watermarkpos"] == '4' ? " checked=\"checked\"" : "") . "> " . $lang_settings['text_left'] . "<input type='radio' name='watermarkpos' value='5' " . ($ATTACHMENT["watermarkpos"] == '5' ? " checked=\"checked\"" : "") . "> " . $lang_settings['text_center'] . "<input type='radio' name='watermarkpos' value='6' " . ($ATTACHMENT["watermarkpos"] == '6' ? " checked=\"checked\"" : "") . "> " . $lang_settings['text_right'] . "<br><input type='radio' name='watermarkpos' value='7' " . ($ATTACHMENT["watermarkpos"] == '7' ? " checked=\"checked\"" : "") . "> " . $lang_settings['text_left_bottom'] . "<input type='radio' name='watermarkpos' value='8' " . ($ATTACHMENT["watermarkpos"] == '8' ? " checked=\"checked\"" : "") . "> " . $lang_settings['text_bottom'] . "<input type='radio' name='watermarkpos' value='9' " . ($ATTACHMENT["watermarkpos"] == '9' ? " checked=\"checked\"" : "") . "> " . $lang_settings['text_right_bottom'] . "<br><input type='radio' name='watermarkpos' value='random' " . ($ATTACHMENT["watermarkpos"] == 'random' ? " checked=\"checked\"" : "") . "> " . $lang_settings['text_random_position'] . "<br>" . $lang_settings['text_watermark_note'], 1);
	tr($lang_settings['row_image_size_for_watermark'], "<input type='text' style=\"width: 100px\" name=\"watermarkwidth\" value='" . ($ATTACHMENT['watermarkwidth'] ? $ATTACHMENT['watermarkwidth'] : '300') . "'> * <input type='text' style=\"width: 100px\" name=\"watermarkheight\" value='" . ($ATTACHMENT['watermarkheight'] ? $ATTACHMENT['watermarkheight'] : '300') . "'> " . $lang_settings['text_watermark_size_note'], 1);
//yesorno($lang_settings['row_add_watermark_to_thumbnail'], 'wmthumb', $ATTACHMENT["wmthumb"], $lang_settings['text_watermark_to_thumbnail_note']);
	tr($lang_settings['row_jpeg_quality_with_watermark'], "<input type='text' style=\"width: 100px\" name=\"watermarkquality\" value='" . ($ATTACHMENT['watermarkquality'] ? $ATTACHMENT['watermarkquality'] : '85') . "'> " . $lang_settings['text_jpeg_watermark_quality_note'], 1);
	tr($lang_settings['row_save_settings'], "<input type='submit' name='save' value='" . $lang_settings['submit_save_settings'] . "'>", 1);
	print ("</form>");
//新增功能开始
//自定义考核
} elseif ($action == 'checksettings') {
	stdhead($lang_settings['head_check_settings']);
	?>
	<script>
		function recheck() {
			var checkstart = document.getElementById("checkstart").value;
			var checkstartold = document.getElementById("checkstart_old").value;
			var checkdate = document.getElementById("checkdate").value;
			var checkdateold = document.getElementById("checkdate_old").value;
			var checktime = document.getElementById("checktime").value;
			var checktimeold = document.getElementById("checktime_old").value;
			var recheckstatus = document.getElementById("recheckstatus").value;
			if (recheckstatus === 'yes') {
				if (checkstart !== checkstartold || checkdate !== checkdateold || checktime !== checktimeold) {
					if (confirm('上一次自定义考核未结束，你确定要这样做吗？此行为可能会导致数据统计错乱！')) {
						return true;
					} else {
						return false;
					}
				}
			}
			return true;
		}
	</script>
	<?php

	print ($notice);
	$maxclass = UC_SYSOP;
	print ("<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "' onsubmit='return recheck();'><input type='hidden' name='action' value='savesettings_check'>");
	yesorno($lang_settings['row_enable_check'], 'enablead', $CHECK['enablead'], $lang_settings['text_enable_check_note']); //自定义考核开关
	yesorno($lang_settings['row_enable_convert'], 'convert', $CHECK['convert'], $lang_settings['text_enable_convert_note']); //禁止流量兑换开关
	tr($lang_settings['row_check_management'], $lang_settings['check_minimum_class'] . classlist('checkmanage', $maxclass, $CHECK['checkmanage']) . $lang_settings['text_default'] . get_user_class_name(UC_VIP, false, true, true) . $lang_settings['text_check_management'], 1); //免考最低等级
	tr($lang_settings['row_check_name'], "<input type='text' name=checkname style=\"width: 120px\" value='" . ($CHECK['checkname'] ? $CHECK['checkname'] : "考核") . "'> " . $lang_settings['text_check_name_note'], 1); //自定义考核名称
	tr($lang_settings['row_check_time'], "<input type='text' id=checktime name=checktime style=\"width: 120px\" value='" . ($CHECK['checktime'] ? $CHECK['checktime'] : 60) . "'> " . $lang_settings['text_check_time_note'], 1); //自定义考核时长
	print("<input type='hidden' id=checktime_old name=checktime_old value='" . ($CHECK['checktime'] ? $CHECK['checktime'] : 60) . "'>"); //原自定义考核时长
	tr($lang_settings['row_check_start'], "<input type='text' id=checkstart name=checkstart style=\"width: 120px\" class=\"Wdate\" onfocus=\"WdatePicker({isShowWeek:'true',minDate:'%y-%M-{%d+1}'})\" value='" . ($CHECK['checkstart'] ? $CHECK['checkstart'] : date("Y-m-d", time() + 86400 * 1)) . "'> " . $lang_settings['text_check_start_note'], 1); //自定义考核开始时间
	print("<input type='hidden' id=checkstart_old name=checkstart_old value='" . ($CHECK['checkstart'] ? $CHECK['checkstart'] : date("Y-m-d", time() + 86400 * 1)) . "'>"); //原自定义考核开始时间
	tr($lang_settings['row_check_date'], "<input type='text' id=checkdate name=checkdate style=\"width: 120px\" class=\"Wdate\" onfocus=\"WdatePicker({isShowWeek:'true',maxDate:'%y-%M-%d'})\" value='" . ($CHECK['checkdate'] ? $CHECK['checkdate'] : date("Y-m-d", time())) . "'> " . $lang_settings['text_check_date_note'], 1); //需要参加自定义考核的注册日
	print("<input type='hidden' id=checkdate_old name=checkdate_old value='" . ($CHECK['checkdate'] ? $CHECK['checkdate'] : date("Y-m-d", time())) . "'>"); //原需要参加自定义考核的注册日
	tr($lang_settings['row_check_ul'], "<input type='text' name=checkul style=\"width: 120px\" value=$CHECK[checkul] /> " . $lang_settings['text_check_ul_note'], 1); //上传增量
	tr($lang_settings['row_check_dl'], "<input type='text' name=checkdl style=\"width: 120px\" value=$CHECK[checkdl] /> " . $lang_settings['text_check_dl_note'], 1); //下载增量
	tr($lang_settings['row_check_bonus'], "<input type='text' name=checkbonus style=\"width: 120px\" value=$CHECK[checkbonus] /> " . $lang_settings['text_check_bonus_note'], 1); //魔力增量
	tr($lang_settings['row_check_fa'], "<input type='text' name=checkfa style=\"width: 120px\" value=$CHECK[checkfa] /> " . $lang_settings['text_check_fa_note'], 1); //发种增量
	tr($lang_settings['row_check_ultime'], "<input type='text' name=checkultime style=\"width: 120px\" value=$CHECK[checkultime] /> " . $lang_settings['text_check_ultime_note'], 1); //做种时间
	tr($lang_settings['row_check_dltime'], "<input type='text' name=checkdltime style=\"width: 120px\" value=$CHECK[checkdltime] /> " . $lang_settings['text_check_dltime_note'], 1); //下载时间
	tr($lang_settings['row_check_radio'], "<input type='text' name=checkradio style=\"width: 120px\" value=$CHECK[checkradio] /> " . $lang_settings['text_check_radio_note'], 1); //增量分享比率
	tr($lang_settings['row_check_tr'], "<input type='text' name=checktr style=\"width: 120px\" value=$CHECK[checktr] /> " . $lang_settings['text_check_tr_note'], 1); //做种/下载增量时间比率
	print("<input type='hidden' id=recheckstatus name=recheckstatus value=$recheckstatus>"); //检查上一次自定义考核是否结束
	tr($lang_settings['row_save_settings'], "<input type='submit' name='save' value='" . $lang_settings['submit_save_settings'] . "'>", 1);
	print ("</form>");
//新增加功能结束
} elseif ($action == 'advertisementsettings') {
	stdhead($lang_settings['head_advertisement_settings']);
	print ($notice);
	print ("<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='savesettings_advertisement'>");
	yesorno($lang_settings['row_enable_advertisement'], 'enablead', $ADVERTISEMENT['enablead'], $lang_settings['text_enable_advertisement_note']);
	tr($lang_settings['row_no_advertisement'], "<input type='checkbox' name='enablenoad' value='yes'" . ($ADVERTISEMENT['enablenoad'] == 'yes' ? " checked='checked'" : "") . " />" . classlist('noad', UC_STAFFLEADER, $ADVERTISEMENT['noad']) . $lang_settings['text_can_choose_no_advertisement'] . get_user_class_name(UC_UPLOADER, false, true, true), 1);
	tr($lang_settings['row_bonus_no_advertisement'], "<input type='checkbox' name='enablebonusnoad' value='yes'" . ($ADVERTISEMENT['enablebonusnoad'] == 'yes' ? " checked='checked'" : "") . " />" . classlist('bonusnoad', UC_STAFFLEADER, $ADVERTISEMENT['bonusnoad']) . $lang_settings['text_no_advertisement_with_bonus'] . get_user_class_name(UC_POWER_USER, false, true, true), 1);
	tr($lang_settings['row_no_advertisement_bonus_price'], $lang_settings['text_it_costs_user'] . "<input type='text' style=\"width: 50px\" name='bonusnoadpoint' value='" . (isset($ADVERTISEMENT["bonusnoadpoint"]) ? $ADVERTISEMENT["bonusnoadpoint"] : 10000 ) . "'>" . $lang_settings['text_bonus_points_to_buy'] . "<input type='text' style=\"width: 50px\" name='bonusnoadtime' value='" . (isset($ADVERTISEMENT["bonusnoadtime"]) ? $ADVERTISEMENT["bonusnoadtime"] : 15 ) . "'>" . $lang_settings['text_days_without_advertisements'], 1);
	tr($lang_settings['row_click_advertisement_bonus'], $lang_settings['text_user_would_get'] . "<input type='text' style=\"width: 50px\" name='adclickbonus' value='" . (isset($ADVERTISEMENT["adclickbonus"]) ? $ADVERTISEMENT["adclickbonus"] : 0 ) . "'>" . $lang_settings['text_points_clicking_on_advertisements'], 1);
	tr($lang_settings['row_save_settings'], "<input type='submit' name='save' value='" . $lang_settings['submit_save_settings'] . "'>", 1);
	print ("</form>");
} elseif ($action == 'codesettings') { // code settings
	stdhead($lang_settings['head_code_settings']);
	print ($notice);
	print ("<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='savesettings_code'>");
	tr($lang_settings['row_main_version'], "<input type='text' style=\"width: 300px\" name=mainversion value='" . ($CODE["mainversion"] ? $CODE["mainversion"] : PROJECTNAME) . "' /> " . $lang_settings['text_main_version_note'], 1);
	tr($lang_settings['row_sub_version'], "<input type='text' style=\"width: 300px\" name=subversion value='" . ($CODE["subversion"] ? $CODE["subversion"] : "1.0") . "' /> " . $lang_settings['text_sub_version_note'], 1);
	tr($lang_settings['row_release_date'], "<input type='text' style=\"width: 300px\" name=releasedate class=\"Wdate\" onfocus=\"WdatePicker({isShowWeek:'true'})\" value='" . ($CODE["releasedate"] ? $CODE["releasedate"] : "2008-12-10") . "' /> " . $lang_settings['text_release_date_note'], 1);
	tr($lang_settings['row_web_site'], "<input type='text' style=\"width: 300px\" name=website value='" . ($CODE["website"] ? $CODE["website"] : "") . "' /> " . $lang_settings['text_web_site_note_one'] . $SITENAME . $lang_settings['text_web_site_note_two'], 1);
	tr($lang_settings['row_save_settings'], "<input type='submit' name='save' value='" . $lang_settings['submit_save_settings'] . "' />", 1);
	print ("</form>");
} elseif ($action == 'bonussettings') {
	stdhead($lang_settings['head_bonus_settings']);
	print ($notice);
	print ("<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='savesettings_bonus'>");
	print("<tr><td colspan=2 align=center><b>" . $lang_settings['text_bonus_by_seeding'] . "</b></td></tr>");
	tr($lang_settings['row_donor_gets_double'], $lang_settings['text_donor_gets'] . "<input type='text' style=\"width: 50px\" name=donortimes value='" . (isset($BONUS["donortimes"]) ? $BONUS["donortimes"] : 2 ) . "'>" . $lang_settings['text_times_as_many'], 1);
	tr($lang_settings['row_basic_seeding_bonus'], $lang_settings['text_user_would_get'] . "<input type='text' style=\"width: 50px\" name=perseeding value='" . (isset($BONUS["perseeding"]) ? $BONUS["perseeding"] : 1 ) . "'>" . $lang_settings['text_bonus_points'] . "<input type='text' style=\"width: 50px\" name=maxseeding value='" . (isset($BONUS["maxseeding"]) ? $BONUS["maxseeding"] : 7 ) . "'>" . $lang_settings['text_torrents_default'], 1);
	tr($lang_settings['row_seeding_formula'], $lang_settings['text_bonus_formula_one'] . "<br />&nbsp;&nbsp;&nbsp;&nbsp;<img src=pic/bonusformulaa.png alt=\"A = sigma( ( 1 - 10 ^ ( - Ti / T0 ) ) * Si * ( 1 + sqrt( 2 ) * 10 ^ ( - ( Ni - 1 ) / ( N0 - 1 ) ) )\" title=\"A = sigma( ( 1 - 10 ^ ( - Ti / T0 ) ) * Si * ( 1 + sqrt( 2 ) * 10 ^ ( - ( Ni - 1 ) / ( N0 - 1 ) ) )\"><br />&nbsp;&nbsp;&nbsp;&nbsp;<img src=pic/bonusformulab.png alt=\"B = B0 * 2 / pi * arctan( A / L )\" title=\"B = B0 * 2 / pi * arctan( A / L )\"><br />" . $lang_settings['text_where'] . "<ul><li>" . $lang_settings['text_bonus_formula_two'] . "</li><li>" . $lang_settings['text_bonus_formula_three'] . "<input type='text' style=\"width: 50px\" name=tzero value='" . (isset($BONUS["tzero"]) ? $BONUS["tzero"] : 4 ) . "'>" . $lang_settings['text_bonus_formula_four'] . "</li><li>" . $lang_settings['text_bonus_formula_five'] . "</li><li>" . $lang_settings['text_bonus_formula_six'] . "<input type='text' style=\"width: 50px\" name=nzero value='" . (isset($BONUS["nzero"]) ? $BONUS["nzero"] : 7 ) . "'>" . $lang_settings['text_bonus_formula_seven'] . "</li><li>" . $lang_settings['text_bonus_formula_eight'] . "</li><li>" . $lang_settings['text_bonus_formula_nine'] . "<input type='text' style=\"width: 50px\" name=bzero value='" . (isset($BONUS["bzero"]) ? $BONUS["bzero"] : 100 ) . "'>" . $lang_settings['text_bonus_formula_ten'] . "</li><li>" . $lang_settings['text_bonus_formula_eleven'] . "<input type='text' style=\"width: 50px\" name=l value='" . (isset($BONUS["l"]) ? $BONUS["l"] : 300 ) . "'>" . $lang_settings['text_bonus_formula_twelve'] . "</li></ul>\n", 1);
	print("<tr><td colspan=2 align=center><b>" . $lang_settings['text_misc_ways_get_bonus'] . "</b></td></tr>");
	tr($lang_settings['row_uploading_torrent'], $lang_settings['text_user_would_get'] . "<input type='text' style=\"width: 50px\" name=uploadtorrent value='" . (isset($BONUS["uploadtorrent"]) ? $BONUS["uploadtorrent"] : 15 ) . "'>" . $lang_settings['text_uploading_torrent_note'], 1);
	tr($lang_settings['row_uploading_subtitle'], $lang_settings['text_user_would_get'] . "<input type='text' style=\"width: 50px\" name=uploadsubtitle value='" . (isset($BONUS["uploadsubtitle"]) ? $BONUS["uploadsubtitle"] : 5 ) . "'>" . $lang_settings['text_uploading_subtitle_note'], 1);
	tr($lang_settings['row_starting_topic'], $lang_settings['text_user_would_get'] . "<input type='text' style=\"width: 50px\" name=starttopic value='" . (isset($BONUS["starttopic"]) ? $BONUS["starttopic"] : 2 ) . "'>" . $lang_settings['text_starting_topic_note'], 1);
	tr($lang_settings['row_making_post'], $lang_settings['text_user_would_get'] . "<input type='text' style=\"width: 50px\" name=makepost value='" . (isset($BONUS["makepost"]) ? $BONUS["makepost"] : 1 ) . "'>" . $lang_settings['text_making_post_note'], 1);
	tr($lang_settings['row_adding_comment'], $lang_settings['text_user_would_get'] . "<input type='text' style=\"width: 50px\" name=addcomment value='" . (isset($BONUS["addcomment"]) ? $BONUS["addcomment"] : 1 ) . "'>" . $lang_settings['text_adding_comment_note'], 1);
	tr($lang_settings['row_voting_on_poll'], $lang_settings['text_user_would_get'] . "<input type='text' style=\"width: 50px\" name=pollvote value='" . (isset($BONUS["pollvote"]) ? $BONUS["pollvote"] : 1 ) . "'>" . $lang_settings['text_voting_on_poll_note'], 1);
	tr($lang_settings['row_voting_on_offer'], $lang_settings['text_user_would_get'] . "<input type='text' style=\"width: 50px\" name=offervote value='" . (isset($BONUS["offervote"]) ? $BONUS["offervote"] : 1 ) . "'>" . $lang_settings['text_voting_on_offer_note'], 1);
	tr($lang_settings['row_voting_on_funbox'], $lang_settings['text_user_would_get'] . "<input type='text' style=\"width: 50px\" name=funboxvote value='" . (isset($BONUS["funboxvote"]) ? $BONUS["funboxvote"] : 1 ) . "'>" . $lang_settings['text_voting_on_funbox_note'], 1);
	tr($lang_settings['row_saying_thanks'], $lang_settings['text_giver_and_receiver_get'] . "<input type='text' style=\"width: 50px\" name=saythanks value='" . (isset($BONUS["saythanks"]) ? $BONUS["saythanks"] : 0.5 ) . "'>" . $lang_settings['text_saying_thanks_and'] . "<input type='text' style=\"width: 50px\" name=receivethanks value='" . (isset($BONUS["receivethanks"]) ? $BONUS["receivethanks"] : 0 ) . "'>" . $lang_settings['text_saying_thanks_default'], 1);
	tr($lang_settings['row_funbox_stuff_reward'], $lang_settings['text_user_would_get'] . "<input type='text' style=\"width: 50px\" name=funboxreward value='" . (isset($BONUS["funboxreward"]) ? $BONUS["funboxreward"] : 5 ) . "'>" . $lang_settings['text_funbox_stuff_reward_note'], 1);
	tr($lang_settings['row_promotion_link_click'], $lang_settings['text_user_would_get'] . "<input type='text' style=\"width: 50px\" name=prolinkpoint value='" . (isset($BONUS["prolinkpoint"]) ? $BONUS["prolinkpoint"] : 0 ) . "'>" . $lang_settings['text_promotion_link_note_one'] . "<input type='text' style=\"width: 50px\" name=prolinktime value='" . (isset($BONUS["prolinktime"]) ? $BONUS["prolinktime"] : 600 ) . "'>" . $lang_settings['text_promotion_link_note_two'], 1);
	print("<tr><td colspan=2 align=center><b>" . $lang_settings['text_things_cost_bonus'] . "</b></td></tr>");
	tr($lang_settings['row_one_gb_credit'], $lang_settings['text_it_costs_user'] . "<input type='text' style=\"width: 50px\" name=onegbupload value='" . (isset($BONUS["onegbupload"]) ? $BONUS["onegbupload"] : 300 ) . "'>" . $lang_settings['text_one_gb_credit_note'], 1);
	tr($lang_settings['row_five_gb_credit'], $lang_settings['text_it_costs_user'] . "<input type='text' style=\"width: 50px\" name=fivegbupload value='" . (isset($BONUS["fivegbupload"]) ? $BONUS["fivegbupload"] : 800 ) . "'>" . $lang_settings['text_five_gb_credit_note'], 1);
	tr($lang_settings['row_ten_gb_credit'], $lang_settings['text_it_costs_user'] . "<input type='text' style=\"width: 50px\" name=tengbupload value='" . (isset($BONUS["tengbupload"]) ? $BONUS["tengbupload"] : 1200 ) . "'>" . $lang_settings['text_ten_gb_credit_note'], 1);
	tr($lang_settings['row_hundred_gb_credit'], $lang_settings['text_it_costs_user'] . "<input type='text' style=\"width: 50px\" name=hundredgbupload value='" . (isset($BONUS["hundredgbupload"]) ? $BONUS["hundredgbupload"] : 10000 ) . "'>" . $lang_settings['text_hundred_gb_credit_note'], 1);
	tr($lang_settings['row_ratio_limit'], $lang_settings['text_user_with_ratio'] . "<input type='text' style=\"width: 50px\" name=ratiolimit value='" . (isset($BONUS["ratiolimit"]) ? $BONUS["ratiolimit"] : 6 ) . "'>" . $lang_settings['text_uploaded_amount_above'] . "<input type='text' style=\"width: 50px\" name=dlamountlimit value='" . (isset($BONUS["dlamountlimit"]) ? $BONUS["dlamountlimit"] : 50 ) . "'>" . $lang_settings['text_ratio_limit_default'], 1);
	tr($lang_settings['row_buy_an_invite'], $lang_settings['text_it_costs_user'] . "<input type='text' style=\"width: 50px\" name=oneinvite value='" . (isset($BONUS["oneinvite"]) ? $BONUS["oneinvite"] : 1000 ) . "'>" . $lang_settings['text_buy_an_invite_note'], 1);
	tr($lang_settings['row_custom_title'], $lang_settings['text_it_costs_user'] . "<input type='text' style=\"width: 50px\" name=customtitle value='" . (isset($BONUS["customtitle"]) ? $BONUS["customtitle"] : 5000 ) . "'>" . $lang_settings['text_custom_title_note'], 1);
	tr($lang_settings['row_vip_status'], $lang_settings['text_it_costs_user'] . "<input type='text' style=\"width: 50px\" name=vipstatus value='" . (isset($BONUS["vipstatus"]) ? $BONUS["vipstatus"] : 8000 ) . "'>" . $lang_settings['text_vip_status_note'], 1);
	//VPN流量购买--开始
	yesorno("VPN开关", 'buyvpn', $BONUS['buyvpn'], "是否开启VPN购买服务（须自行配置FreeRADIUS+MySQL+PPTP），默认'否'。");
	$defaulttraffic = get_single_value("radgroupreply", "value", "WHERE groupname = 'user' AND attribute = 'Max-Monthly-Traffic'"); //默认可用流量
	tr("每月上行流量", "用户将失去<input type='text' style=\"width: 50px\" name=vpn value='" . (isset($BONUS["vpn"]) ? $BONUS["vpn"] : 100000 ) . "'>个魔力值，如果选择交换VPN每月" . mksize($defaulttraffic) . "上行流量的话。默认'100000'。", 1);
	$maxclass = UC_STAFFLEADER;
	tr("最低VPN购买等级", classlist('vpnclass', $maxclass, $BONUS['vpnclass']) . "及以上等级的帐号可以购买VPN使用权限。默认" . get_user_class_name(UC_POWER_USER, false, true, true), 1);
	//VPN流量购买--结束
	yesorno($lang_settings['row_allow_giving_bonus_gift'], 'bonusgift', $BONUS["bonusgift"], $lang_settings['text_giving_bonus_gift_note']);
	tr($lang_settings['row_bonus_gift_tax'], $lang_settings['text_system_charges'] . "<input type='text' style=\"width: 50px\" name='basictax' value='" . (isset($BONUS["basictax"]) ? $BONUS["basictax"] : 5 ) . "'>" . $lang_settings['text_bonus_points_plus'] . "<input type='text' style=\"width: 50px\" name='taxpercentage' value='" . (isset($BONUS["taxpercentage"]) ? $BONUS["taxpercentage"] : 10 ) . "'>" . $lang_settings['text_bonus_gift_tax_note'], 1);
	tr($lang_settings['row_save_settings'], "<input type='submit' name='save' value='" . $lang_settings['submit_save_settings'] . "'>", 1);
	print ("</form>");
} elseif ($action == 'accountsettings') {
	stdhead($lang_settings['head_account_settings']);
	print ($notice);
	$maxclass = UC_VIP;
	print ("<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='savesettings_account'>");
	print("<tr><td colspan=2 align=center><b>" . $lang_settings['text_delete_inactive_accounts'] . "</b></td></tr>");
	tr($lang_settings['row_never_delete'], classlist('neverdelete', $maxclass, $ACCOUNT['neverdelete']) . $lang_settings['text_never_delete'] . get_user_class_name(UC_VETERAN_USER, false, true, true), 1);
	tr($lang_settings['row_never_delete_if_packed'], classlist('neverdeletepacked', $maxclass, $ACCOUNT['neverdeletepacked']) . $lang_settings['text_never_delete_if_packed'] . get_user_class_name(UC_ELITE_USER, false, true, true), 1);
	tr($lang_settings['row_delete_packed'], $lang_settings['text_delete_packed_note_one'] . "<input type='text' style=\"width: 50px\" name=deletepacked value='" . (isset($ACCOUNT["deletepacked"]) ? $ACCOUNT["deletepacked"] : 400 ) . "'>" . $lang_settings['text_delete_packed_note_two'], 1);
	tr($lang_settings['row_delete_unpacked'], $lang_settings['text_delete_unpacked_note_one'] . "<input type='text' style=\"width: 50px\" name=deleteunpacked value='" . (isset($ACCOUNT["deleteunpacked"]) ? $ACCOUNT["deleteunpacked"] : 150 ) . "'>" . $lang_settings['text_delete_unpacked_note_two'], 1);
	tr($lang_settings['row_delete_no_transfer'], $lang_settings['text_delete_transfer_note_one'] . "<input type='text' style=\"width: 50px\" name=deletenotransfer value='" . (isset($ACCOUNT["deletenotransfer"]) ? $ACCOUNT["deletenotransfer"] : 60 ) . "'>" . $lang_settings['text_delete_transfer_note_two'] . "<input type='text' style=\"width: 50px\" name=deletenotransfertwo value='" . (isset($ACCOUNT["deletenotransfertwo"]) ? $ACCOUNT["deletenotransfertwo"] : 0 ) . "'>" . $lang_settings['text_delete_transfer_note_three'], 1);
	print("<tr><td colspan=2 align=center><b>" . $lang_settings['text_user_promotion_demotion'] . "</b></td></tr>");
	if ($ratioless == 'no') {
		tr($lang_settings['row_ban_peasant_one'] . get_user_class_name(UC_PEASANT, false, false, true) . $lang_settings['row_ban_peasant_two'], get_user_class_name(UC_PEASANT, false, true, true) . $lang_settings['text_ban_peasant_note_one'] . "<input type='text' style=\"width: 50px\" name=deletepeasant value='" . (isset($ACCOUNT["deletepeasant"]) ? $ACCOUNT["deletepeasant"] : 30 ) . "'>" . $lang_settings['text_ban_peasant_note_two'], 1);
		tr($lang_settings['row_demoted_to_peasant_one'] . get_user_class_name(UC_PEASANT, false, false, true) . $lang_settings['row_demoted_to_peasant_two'], $lang_settings['text_demoted_peasant_note_one'] . get_user_class_name(UC_PEASANT, false, true, true) . $lang_settings['text_demoted_peasant_note_two'] . "<br /><ul>
		<li>" . $lang_settings['text_downloaded_amount_larger_than'] . "<input type='text' style=\"width: 50px\" name=psdlone value='" . (isset($ACCOUNT["psdlone"]) ? $ACCOUNT["psdlone"] : 50 ) . "'>" . $lang_settings['text_and_ratio_below'] . "<input type='text' style=\"width: 50px\" name=psratioone value='" . (isset($ACCOUNT["psratioone"]) ? $ACCOUNT["psratioone"] : 0.4 ) . "'>" . $lang_settings['text_demote_peasant_default_one'] . "</li>
		<li>" . $lang_settings['text_downloaded_amount_larger_than'] . "<input type='text' style=\"width: 50px\" name=psdltwo value='" . (isset($ACCOUNT["psdltwo"]) ? $ACCOUNT["psdltwo"] : 100 ) . "'>" . $lang_settings['text_and_ratio_below'] . "<input type='text' style=\"width: 50px\" name=psratiotwo value='" . (isset($ACCOUNT["psratiotwo"]) ? $ACCOUNT["psratiotwo"] : 0.5 ) . "'>" . $lang_settings['text_demote_peasant_default_two'] . "</li>
		<li>" . $lang_settings['text_downloaded_amount_larger_than'] . "<input type='text' style=\"width: 50px\" name=psdlthree value='" . (isset($ACCOUNT["psdlthree"]) ? $ACCOUNT["psdlthree"] : 200 ) . "'>" . $lang_settings['text_and_ratio_below'] . "<input type='text' style=\"width: 50px\" name=psratiothree value='" . (isset($ACCOUNT["psratiothree"]) ? $ACCOUNT["psratiothree"] : 0.6 ) . "'>" . $lang_settings['text_demote_peasant_default_three'] . "</li>
		<li>" . $lang_settings['text_downloaded_amount_larger_than'] . "<input type='text' style=\"width: 50px\" name=psdlfour value='" . (isset($ACCOUNT["psdlfour"]) ? $ACCOUNT["psdlfour"] : 400 ) . "'>" . $lang_settings['text_and_ratio_below'] . "<input type='text' style=\"width: 50px\" name=psratiofour value='" . (isset($ACCOUNT["psratiofour"]) ? $ACCOUNT["psratiofour"] : 0.7 ) . "'>" . $lang_settings['text_demote_peasant_default_four'] . "</li>
		<li>" . $lang_settings['text_downloaded_amount_larger_than'] . "<input type='text' style=\"width: 50px\" name=psdlfive value='" . (isset($ACCOUNT["psdlfive"]) ? $ACCOUNT["psdlfive"] : 800 ) . "'>" . $lang_settings['text_and_ratio_below'] . "<input type='text' style=\"width: 50px\" name=psratiofive value='" . (isset($ACCOUNT["psratiofive"]) ? $ACCOUNT["psratiofive"] : 0.8 ) . "'>" . $lang_settings['text_demote_peasant_default_five'] . "</li>
		</ul><br />" . $lang_settings['text_demote_peasant_note'], 1);

		function promotion_criteria($class, $input, $time, $dl, $prratio, $deratio, $defaultInvites = 0) {
			global $lang_settings;
			global $ACCOUNT;
			$inputtime = $input . "timer";
			$inputdl = $input . "dlr";
			$inputprratio = $input . "prratior";
			$inputderatio = $input . "deratior";
			if (!isset($class))
				return;
			tr($lang_settings['row_promote_to_one'] . get_user_class_name($class, false, false, true) . $lang_settings['row_promote_to_two'], $lang_settings['text_member_longer_than'] . "<input type='text' style=\"width: 50px\" name='" . $inputtime . "' value='" . (isset($ACCOUNT[$inputtime]) ? $ACCOUNT[$inputtime] : $time ) . "'>" . $lang_settings['text_downloaded_more_than'] . "<input type='text' style=\"width: 50px\" name='" . $inputdl . "' value='" . (isset($ACCOUNT[$inputdl]) ? $ACCOUNT[$inputdl] : $dl ) . "'>" . $lang_settings['text_with_ratio_above'] . "<input type='text' style=\"width: 50px\" name='" . $inputprratio . "' value='" . (isset($ACCOUNT[$inputprratio]) ? $ACCOUNT[$inputprratio] : $prratio ) . "'>" . $lang_settings['text_be_promoted_to'] . get_user_class_name($class, false, true, true) . $lang_settings['text_promote_to_default_one'] . "'" . $time . "', '" . $dl . "', '" . $prratio . "'.<br />" . $lang_settings['text_demote_with_ratio_below'] . "<input type='text' style=\"width: 50px\" name='" . $inputderatio . "' value='" . (isset($ACCOUNT[$inputderatio]) ? $ACCOUNT[$inputderatio] : $deratio ) . "'>" . $lang_settings['text_promote_to_default_two'] . "'" . $deratio . "'.<br />" . $lang_settings['text_users_get'] . "<input type='text' style=\"width: 50px\" name='getInvitesByPromotion[" . $class . "]' value='" . (isset($ACCOUNT['getInvitesByPromotion'][$class]) ? $ACCOUNT['getInvitesByPromotion'][$class] : $defaultInvites ) . "'>" . $lang_settings['text_invitations_default'] . "'" . $defaultInvites . "'.", 1);
		}

		promotion_criteria(UC_POWER_USER, "pu", 1, 50, 1.05, 0.95, 1);
		promotion_criteria(UC_ELITE_USER, "eu", 2, 150, 1.55, 1.45, 0);
		promotion_criteria(UC_CRAZY_USER, "cu", 4, 300, 2.05, 1.95, 2);
		promotion_criteria(UC_INSANE_USER, "iu", 8, 500, 2.55, 2.45, 0);
		promotion_criteria(UC_VETERAN_USER, "vu", 16, 750, 3.05, 2.95, 3);
		promotion_criteria(UC_EXTREME_USER, "exu", 24, 1024, 3.55, 3.45, 0);
		promotion_criteria(UC_ULTIMATE_USER, "uu", 36, 1536, 4.05, 3.95, 5);
		promotion_criteria(UC_NEXUS_MASTER, "nm", 52, 3072, 4.55, 4.45, 10);
	} else {

		function promotion_criteria($class, $input, $time, $dl, $prratio, $deratio, $defaultInvites = 0) {
			global $lang_settings;
			global $ACCOUNT;
			$inputtime = $input . "time"; //加入时间
			$inputdl = $input . "dl"; //下载量
			$inputprratio = $input . "prratio"; //完成数
			$inputderatio = $input . "deratio"; //需要积分
			if (!isset($class))
				return;
			tr($lang_settings['row_promote_to_one'] . get_user_class_name($class, false, false, true) . $lang_settings['row_promote_to_two'], $lang_settings['text_member_longer_than'] . "<input type='text' style=\"width: 50px\" name='" . $inputtime . "' value='" . (isset($ACCOUNT[$inputtime]) ? $ACCOUNT[$inputtime] : $time ) . "'>" . $lang_settings['text_downloaded_more_than_rl'] . "<input type='text' style=\"width: 50px\" name='" . $inputdl . "' value='" . (isset($ACCOUNT[$inputdl]) ? $ACCOUNT[$inputdl] : $dl ) . "'>" . $lang_settings['text_with_ratio_above_rl'] . "<input type='text' style=\"width: 50px\" name='" . $inputprratio . "' value='" . (isset($ACCOUNT[$inputprratio]) ? $ACCOUNT[$inputprratio] : $prratio ) . "'>" . $lang_settings['text_be_promoted_to_rl'] . get_user_class_name($class, false, true, true) . $lang_settings['text_promote_to_default_one'] . "'" . $time . "', '" . $dl . "', '" . $prratio . "'.<br />" . $lang_settings['text_demote_with_ratio_below_rl'] . "<input type='text' style=\"width: 60px\" name='" . $inputderatio . "' value='" . (isset($ACCOUNT[$inputderatio]) ? $ACCOUNT[$inputderatio] : $deratio ) . "'>" . $lang_settings['text_promote_to_default_two_rl'] . "'" . $deratio . "'.<br />" . $lang_settings['text_users_get'] . "<input type='text' style=\"width: 50px\" name='getInvitesByPromotion[" . $class . "]' value='" . (isset($ACCOUNT['getInvitesByPromotion'][$class]) ? $ACCOUNT['getInvitesByPromotion'][$class] : $defaultInvites ) . "'>" . $lang_settings['text_invitations_default'] . "'" . $defaultInvites . "'.", 1);
		}

		promotion_criteria(UC_POWER_USER, "pu", 1, 512, 25, 100000, 1);
		promotion_criteria(UC_ELITE_USER, "eu", 2, 1024, 50, 250000, 0);
		promotion_criteria(UC_CRAZY_USER, "cu", 4, 1536, 150, 500000, 2);
		promotion_criteria(UC_INSANE_USER, "iu", 8, 2048, 300, 750000, 0);
		promotion_criteria(UC_VETERAN_USER, "vu", 16, 5120, 500, 1000000, 3);
		promotion_criteria(UC_EXTREME_USER, "exu", 24, 10240, 1000, 1250000, 0);
		promotion_criteria(UC_ULTIMATE_USER, "uu", 36, 15360, 2000, 1500000, 5);
		promotion_criteria(UC_NEXUS_MASTER, "nm", 52, 30720, 3000, 3000000, 10);
	}
	tr($lang_settings['row_save_settings'], "<input type='submit' name='save' value='" . $lang_settings['submit_save_settings'] . "'>", 1);
	print ("</form>");
} elseif ($action == 'torrentsettings') {
	stdhead($lang_settings['head_torrent_settings']);
	print ($notice);
	print ("<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='savesettings_torrent'>");

	yesorno($lang_settings['row_promotion_rules'], 'prorules', $TORRENT["prorules"], $lang_settings['text_promotion_rules_note']);
	tr($lang_settings['row_random_promotion'], $lang_settings['text_random_promotion_note_one'] . "<ul><li><input type='text' style=\"width: 50px\" name=randomhalfleech value='" . (isset($TORRENT["randomhalfleech"]) ? $TORRENT["randomhalfleech"] : 5 ) . "'>" . $lang_settings['text_halfleech_chance_becoming'] . "</li><li><input type='text' style=\"width: 50px\" name=randomfree value='" . (isset($TORRENT["randomfree"]) ? $TORRENT["randomfree"] : 2 ) . "'>" . $lang_settings['text_free_chance_becoming'] . "</li><li><input type='text' style=\"width: 50px\" name=randomtwoup value='" . (isset($TORRENT["randomtwoup"]) ? $TORRENT["randomtwoup"] : 2 ) . "'>" . $lang_settings['text_twoup_chance_becoming'] . "</li><li><input type='text' style=\"width: 50px\" name=randomtwoupfree value='" . (isset($TORRENT["randomtwoupfree"]) ? $TORRENT["randomtwoupfree"] : 1 ) . "'>" . $lang_settings['text_freetwoup_chance_becoming'] . "</li><li><input type='text' style=\"width: 50px\" name=randomtwouphalfdown value='" . (isset($TORRENT["randomtwouphalfdown"]) ? $TORRENT["randomtwouphalfdown"] : 0 ) . "'>" . $lang_settings['text_twouphalfleech_chance_becoming'] . "</li><li><input type='text' style=\"width: 50px\" name=randomthirtypercentdown value='" . (isset($TORRENT["randomthirtypercentdown"]) ? $TORRENT["randomthirtypercentdown"] : 0 ) . "'>" . $lang_settings['text_thirtypercentleech_chance_becoming'] . "</li></ul>" . $lang_settings['text_random_promotion_note_two'], 1);
	tr($lang_settings['row_large_torrent_promotion'], $lang_settings['text_torrent_larger_than'] . "<input type='text' style=\"width: 50px\" name=largesize value='" . (isset($TORRENT["largesize"]) ? $TORRENT["largesize"] : 20 ) . "'>" . $lang_settings['text_gb_promoted_to'] . "<select name=largepro>" . promotion_selection((isset($TORRENT['largepro']) ? $TORRENT['largepro'] : 2), 1) . "</select>" . $lang_settings['text_by_system_upon_uploading'] . "<br />" . $lang_settings['text_large_torrent_promotion_note'], 1);
	tr($lang_settings['row_promotion_timeout'], $lang_settings['text_promotion_timeout_note_one'] . "<ul>
<li>" . $lang_settings['text_halfleech_will_become'] . "<select name=halfleechbecome>" . promotion_selection((isset($TORRENT['halfleechbecome']) ? $TORRENT['halfleechbecome'] : 1), 5) . "</select>" . $lang_settings['text_after'] . "<input type='text' style=\"width: 50px\" name=expirehalfleech value='" . (isset($TORRENT["expirehalfleech"]) ? $TORRENT["expirehalfleech"] : 150 ) . "'>" . $lang_settings['text_halfleech_timeout_default'] . "</li>

<li>" . $lang_settings['text_free_will_become'] . "<select name=freebecome>" . promotion_selection((isset($TORRENT['freebecome']) ? $TORRENT['freebecome'] : 1), 2) . "</select>" . $lang_settings['text_after'] . "<input type='text' style=\"width: 50px\" name=expirefree value='" . (isset($TORRENT["expirefree"]) ? $TORRENT["expirefree"] : 60 ) . "'>" . $lang_settings['text_free_timeout_default'] . "</li>

<li>" . $lang_settings['text_twoup_will_become'] . "<select name=twoupbecome>" . promotion_selection((isset($TORRENT['twoupbecome']) ? $TORRENT['twoupbecome'] : 1), 3) . "</select>" . $lang_settings['text_after'] . "<input type='text' style=\"width: 50px\" name=expiretwoup value='" . (isset($TORRENT["expiretwoup"]) ? $TORRENT["expiretwoup"] : 60 ) . "'>" . $lang_settings['text_twoup_timeout_default'] . "</li>

<li>" . $lang_settings['text_freetwoup_will_become'] . "<select name=twoupfreebecome>" . promotion_selection((isset($TORRENT['twoupfreebecome']) ? $TORRENT['twoupfreebecome'] : 1), 4) . "</select>" . $lang_settings['text_after'] . "<input type='text' style=\"width: 50px\" name=expiretwoupfree value='" . (isset($TORRENT["expiretwoupfree"]) ? $TORRENT["expiretwoupfree"] : 30 ) . "'>" . $lang_settings['text_freetwoup_timeout_default'] . "</li>

<li>" . $lang_settings['text_halfleechtwoup_will_become'] . "<select name=twouphalfleechbecome>" . promotion_selection((isset($TORRENT['twouphalfleechbecome']) ? $TORRENT['twouphalfleechbecome'] : 1), 6) . "</select>" . $lang_settings['text_after'] . "<input type='text' style=\"width: 50px\" name=expiretwouphalfleech value='" . (isset($TORRENT["expiretwouphalfleech"]) ? $TORRENT["expiretwouphalfleech"] : 30 ) . "'>" . $lang_settings['text_halfleechtwoup_timeout_default'] . "</li>

<li>" . $lang_settings['text_thirtypercentleech_will_become'] . "<select name=thirtypercentleechbecome>" . promotion_selection((isset($TORRENT['thirtypercentleechbecome']) ? $TORRENT['thirtypercentleechbecome'] : 1), 7) . "</select>" . $lang_settings['text_after'] . "<input type='text' style=\"width: 50px\" name=expirethirtypercentleech value='" . (isset($TORRENT["expirethirtypercentleech"]) ? $TORRENT["expirethirtypercentleech"] : 30 ) . "'>" . $lang_settings['text_thirtypercentleech_timeout_default'] . "</li>

<li>" . $lang_settings['text_normal_will_become'] . "<select name=normalbecome>" . promotion_selection((isset($TORRENT['normalbecome']) ? $TORRENT['normalbecome'] : 1), 0) . "</select>" . $lang_settings['text_after'] . "<input type='text' style=\"width: 50px\" name=expirenormal value='" . (isset($TORRENT["expirenormal"]) ? $TORRENT["expirenormal"] : 0 ) . "'>" . $lang_settings['text_normal_timeout_default'] . "</li>

</ul>" . $lang_settings['text_promotion_timeout_note_two'], 1);
	tr($lang_settings['row_auto_pick_hot'], $lang_settings['text_torrents_uploaded_within'] . "<input type='text' style=\"width: 50px\" name=hotdays value='" . (isset($TORRENT["hotdays"]) ? $TORRENT["hotdays"] : 7 ) . "'>" . $lang_settings['text_days_with_more_than'] . "<input type='text' style=\"width: 50px\" name=hotseeder value='" . (isset($TORRENT["hotseeder"]) ? $TORRENT["hotseeder"] : 10 ) . "'>" . $lang_settings['text_be_picked_as_hot'] . "<br />" . $lang_settings['text_auto_pick_hot_default'], 1);
	tr($lang_settings['row_uploader_get_double'], $lang_settings['text_torrent_uploader_gets'] . "<input type='text' style=\"width: 50px\" name=uploaderdouble value='" . (isset($TORRENT["uploaderdouble"]) ? $TORRENT["uploaderdouble"] : 1 ) . "'>" . $lang_settings['text_times_uploading_credit'] . $lang_settings['text_uploader_get_double_default'], 1);
	tr($lang_settings['row_delete_dead_torrents'], $lang_settings['text_torrents_being_dead_for'] . "<input type='text' style=\"width: 50px\" name=deldeadtorrent value='" . (isset($TORRENT["deldeadtorrent"]) ? $TORRENT["deldeadtorrent"] : 0 ) . "'>" . $lang_settings['text_days_be_deleted'] . "<br />" . $lang_settings['row_delete_dead_torrents_note'], 1);
	tr($lang_settings['row_save_settings'], "<input type='submit' name='save' value='" . $lang_settings['submit_save_settings'] . "'>", 1);
	print ("</form>");
} elseif ($action == 'mainsettings') { // main settings
	stdhead($lang_settings['head_main_settings']);
	print ($notice);
	print ("<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='savesettings_main'>");
	$sh = "qq.com";
	$maxclass = UC_SYSOP;
	yesorno("RatioLess模式", "mode", $MAIN['mode'], "默认'否'。切换系统是否为不考虑分享率的模式。");
	yesorno("自动发布", "autorelease", $MAIN['autorelease'], "默认'是'。设置是否将在候选区里已经做种的资源自动通过审核。");
	yesorno($lang_settings['row_site_online'], 'site_online', $MAIN['site_online'], $lang_settings['text_site_online_note']);
	yesorno($lang_settings['row_enable_invite_system'], 'invitesystem', $MAIN['invitesystem'], $lang_settings['text_invite_system_note']);
//官邀系统
	yesorno("开启官邀系统", 'officialinvites', $MAIN['officialinvites'], "默认'是'。允许用户自主申请官方邀请。");
//博彩系统
	yesorno("博彩系统开关", 'betsystem', $MAIN['betsystem'], "默认'是'。允许用户使用博彩系统。");
	tr("博彩管理员", "<input type=text name=betsadmin style='width:100px' value=$MAIN[betsadmin]> 默认为空。设置指定的非管理员用户管理博彩功能。", 1);
	tr("发帖帐号", "<input type=text name=betsrobotid style='width:100px' value=$MAIN[betsrobotid]> 默认为'2'。设置在论坛博彩板块发布博彩结果的帐号。", 1);
	tr("发帖板块", "<input type=text name=betsforumid style='width:100px' value=$MAIN[betsforumid]> 填写论坛博彩板块的ID。", 1);
//免考邀请开始
	yesorno("免考邀请开关", 'nonassinvitesystem', $MAIN['nonassinvitesystem'], "默认'是'。允许用户发送免除新手考核的邀请。");
	tr("免考邀请所需魔力值", "<input type='text' name=nonassbonus style=\"width: 100px\" value=$MAIN[nonassbonus]> 单位为个。设置用户发送免除新手考核所需要花费的魔力值。默认'10000'。", 1);
//邀请人奖励
	tr("邀请人奖励", "<input type='text' name=reward style=\"width: 100px\" onkeyup=\"this.value = this.value.replace(/\D/g, '');\" value=$MAIN[reward]> 单位为%。默认 '1' ，'0'或空为关闭此项。", 1);
	tr("后宫回馈上限", "<input type='text' name=rewardnum style=\"width: 100px\" value=$MAIN[rewardnum]> 单位为Byte，即1073741824为 1 GB。设置邀请人每月可以得到最大回馈量，默认'1000G'，'0'或空为关闭此项。", 1);
	tr("后宫回馈", "无需回馈等级：" . classlist('rewarduser', $maxclass, $MAIN['rewarduser']) . $lang_settings['text_default'] . get_user_class_name(UC_CRAZY_USER, false, true, true) . "。达到该等级后就无需回馈邀请人。", 1);
//免考邀请结束
	tr($lang_settings['row_initial_uploading_amount'], "<input type='text' name=iniupload style=\"width: 100px\" value=$MAIN[iniupload]> " . $lang_settings['text_initial_uploading_amount_note'], 1);
	tr($lang_settings['row_initial_seedbonus_amount'], "<input type='text' name=iniseedbonus style=\"width: 100px\" value=$MAIN[iniseedbonus]> " . $lang_settings['text_initial_seedbonus_amount_note'], 1);
	tr($lang_settings['row_initial_invites'], "<input type='text' name=invite_count style=\"width: 50px\" value=$MAIN[invite_count]> " . $lang_settings['text_initial_invites_note'], 1);
	tr($lang_settings['row_invite_timeout'], "<input type='text' name=invite_timeout style=\"width: 50px\" value=$MAIN[invite_timeout]> " . $lang_settings['text_invite_timeout_note'], 1);
	yesorno($lang_settings['row_enable_loginadd'], 'loginadd', $MAIN['loginadd'], $lang_settings['row_allow_loginadd']);
	yesorno($lang_settings['row_enable_registration_system'], 'registration', $MAIN['registration'], $lang_settings['row_allow_registrations']);
//高校V6绿色免考注册通道
	yesorno("高校V6免考注册", 'ipv6signup', $MAIN['ipv6signup'], "默认'否'。允许高校IPv6用户注册免考帐号。");
//自定义功能开始
//魔力值充值
	yesorno($lang_settings['row_enable_recharge'], 'rgenablead', $MAIN['rgenablead'], $lang_settings['text_enable_recharge_note']);
//投蓝
	tr("投票免费数量", "<input type='text' name=checkfreenum style=\"width: 100px\" value=$MAIN[checkfreenum]> 设置投票数超过指定数量后则该种子永久免费。默认'200'。", 1);
//强制分享
	yesorno($lang_settings['row_enable_share_system'], 'share', $MAIN['share'], $lang_settings['row_allow_share']);
	tr($lang_settings['row_sharenum'], "<input type='text' name=sharenum style=\"width: 100px\" value=$MAIN[sharenum]> " . $lang_settings['text_sharenum_note'], 1);
	tr($lang_settings['row_sharesize'], "<input type='text' name=sharesize style=\"width: 100px\" value=$MAIN[sharesize]> " . $lang_settings['text_sharesize_note'], 1);
//认领种子
	yesorno("认领种子系统", 'claim', $MAIN['claim'], "开启认领种子系统。默认'是'。");
	tr("弃领需花费的魔力值", "<input type='text' name=claimbonus style=\"width: 100px\" value=$MAIN[claimbonus]> " . "单位为个，设置弃领每个种子时需要花费的魔力值。默认 100 个。", 1);
	tr("认领种子的做种时间", "<input type='text' name=claimtime style=\"width: 100px\" value=$MAIN[claimtime]> " . "单位为小时，认领的种子达标所需的做种时间。默认 300 小时。", 1);
	tr("认领种子的上传量倍数", "<input type='text' name=claimmul style=\"width: 100px\" value=$MAIN[claimmul]> " . "认领的种子达标所需的上传量为种子大小的指定倍数。默认 6 倍。", 1);
	tr("每人可认领种子数量", "<input type='text' name=claimnum style=\"width: 100px\" value=$MAIN[claimnum]> " . "单位为个，每个帐号可以认领种子的最大数量。默认 30 个。", 1);
	tr("每个种子最多认领数量", "<input type='text' name=claimmaxnum style=\"width: 100px\" value=$MAIN[claimmaxnum]> " . "单位为个，每个种子最多可以被认领的数量。默认 10 个。", 1);
//H&R
	yesorno($lang_settings['row_enable_hr_system'], 'hr', $MAIN['hr'], $lang_settings['row_allow_hr']);
	yesorno("严格 H&R 规则", 'stronghr', $MAIN['stronghr'], "开启严格H&R检查。从下载开始时则计算H&R，14天内需要完成做种24小时或单种分享率2的要求，上限2个H&R，且取消魔力值消除H&R和再次累计做种时间消除H&R，需要和\"开启H&R系统\"同时打开。默认'否'。");
	tr($lang_settings['row_hr_default_hour'], "<input type='text' name=hrhour style=\"width: 100px\" value=$MAIN[hrhour]> " . $lang_settings['text_hr_default_hour_note'], 1);
	tr($lang_settings['row_hr_default_day'], "<input type='text' name=hrday style=\"width: 100px\" value=$MAIN[hrday]> " . $lang_settings['text_hr_default_day_note'], 1);
	tr($lang_settings['row_hr_hrhit'], "<input type='text' name=hrhit style=\"width: 100px\" value=$MAIN[hrhit]> " . $lang_settings['text_hr_hrhit_note'], 1);
	tr($lang_settings['row_hr_hrbonus'], "<input type='text' name=hrbonus style=\"width: 100px\" value=$MAIN[hrbonus]> " . $lang_settings['text_hr_hrbonus_note'], 1);
	tr($lang_settings['row_hr_hrradio'], "<input type='text' name=hrradio style=\"width: 100px\" value=$MAIN[hrradio]> " . $lang_settings['text_hr_hrradio_note'], 1);
	tr("H&R触发比例", "<input type='text' name=hrstartradio style=\"width: 100px\" value=$MAIN[hrstartradio]> " . "单位为%，单种下载量超过种子总体积的指定比例时开始计算H&R时间。默认 10。", 1);
//新人考核
	yesorno($lang_settings['row_enable_newuser_system'], 'newuser', $MAIN['newuser'], $lang_settings['row_allow_newuser']);
	tr($lang_settings['row_newuser_kstime'], "<input type='text' name=newuser_kstime style=\"width: 100px\" value=$MAIN[newuser_kstime]> " . $lang_settings['text_newuser_kstime_note'], 1);
	tr($lang_settings['row_newuser_dl'], "<input type='text' name=newuser_dl style=\"width: 100px\" value=$MAIN[newuser_dl]> " . $lang_settings['text_newuser_dl_note'], 1); //下载量
	tr($lang_settings['row_newuser_ul'], "<input type='text' name=newuser_ul style=\"width: 100px\" value=$MAIN[newuser_ul]> " . $lang_settings['text_newuser_ul_note'], 1); //上传量
	tr($lang_settings['row_newuser_sb'], "<input type='text' name=newuser_sb style=\"width: 100px\" value=$MAIN[newuser_sb]> " . $lang_settings['text_newuser_sb_note'], 1); //魔力值
	tr($lang_settings['row_newuser_ra'], "<input type='text' name=newuser_ra style=\"width: 100px\" value=$MAIN[newuser_ra]> " . $lang_settings['text_newuser_ra_note'], 1); //分享率
	tr($lang_settings['row_newuser_dltime'], "<input type='text' name=newuser_dltime style=\"width: 100px\" value=$MAIN[newuser_dltime]> " . $lang_settings['text_newuser_dltime_note'], 1); //下载时间
	tr($lang_settings['row_newuser_ultime'], "<input type='text' name=newuser_ultime style=\"width: 100px\" value=$MAIN[newuser_ultime]> " . $lang_settings['text_newuser_ultime_note'], 1); //做种时间
	tr($lang_settings['row_newuser_tr'], "<input type='text' name=newuser_tr style=\"width: 100px\" value=$MAIN[newuser_tr]> " . $lang_settings['text_newuser_tr_note'], 1); //做种/下载时间比率
	tr($lang_settings['row_newuser_fa'], "<input type='text' name=newuser_fa style=\"width: 100px\" value=$MAIN[newuser_fa]> " . $lang_settings['text_newuser_fa_note'], 1); //发种数
//BUMP
	yesorno($lang_settings['row_enable_bump_system'], 'bump', $MAIN['bump'], $lang_settings['row_allow_bump']);
	tr($lang_settings['row_bump_time'], "<input type='text' name=bumptime style=\"width: 100px\" value=$MAIN[bumptime]> " . $lang_settings['text_bump_time_note'], 1);
	tr($lang_settings['row_bump_award'], "<input type='text' name=bumpaward style=\"width: 100px\" value=$MAIN[bumpaward]> " . $lang_settings['text_bump_award_note'], 1);
	tr($lang_settings['row_bump_timeaward'], "<input type='text' name=bumptimeaward style=\"width: 100px\" value=$MAIN[bumptimeaward]> " . $lang_settings['text_bump_timeaward_note'], 1);
	tr($lang_settings['row_bump_top'], "<input type='text' name=bumptop style=\"width: 100px\" value=$MAIN[bumptop]> " . $lang_settings['text_bump_top_note'], 1);
//发布自动促销
	yesorno($lang_settings['row_enable_cuxiao_system'], 'cuxiao', $MAIN['cuxiao'], $lang_settings['row_allow_cuxiao']);
	tr($lang_settings['row_cuxiao_select'], "<select name=\"cuxiaoselect\" style=\"width: 100px;\">" . promotion_selection($MAIN[cuxiaoselect], 0) . "</select>" . $lang_settings['text_cuxiao_select_note'], 1);
	tr($lang_settings['row_cuxiao_free_time'], "<input type='text' name=cuxiaofree style=\"width: 100px\" value=$MAIN[cuxiaofree]> " . $lang_settings['text_cuxiao_free_time_note'], 1);
	tr($lang_settings['row_cuxiao_sticky_time'], "<input type='text' name=cuxiaosticky style=\"width: 100px\" value=$MAIN[cuxiaosticky]> " . $lang_settings['text_cuxiao_sticky_time_note'], 1);
//系统随机选取并促销（置顶）
	yesorno($lang_settings['row_autoend_system'], 'autoend', $MAIN['autoend'], $lang_settings['row_allow_autoend']);
	tr($lang_settings['row_autoend_time'], "<input type='text' name=autoendtime style=\"width: 100px\" value=$MAIN[autoendtime]> " . $lang_settings['text_autoend_time_note'], 1);
	tr($lang_settings['row_autoend_free_time'], "<input type='text' name=autoendfree style=\"width: 100px\" value=$MAIN[autoendfree]> " . $lang_settings['text_autoend_free_time_note'], 1);
	tr($lang_settings['row_autoend_sticky_time'], "<input type='text' name=autoendsticky style=\"width: 100px\" value=$MAIN[autoendsticky]> " . $lang_settings['text_autoend_sticky_time_note'], 1);
	tr($lang_settings['row_autoend_limit'], "<input type='text' name=autoendlimit style=\"width: 100px\" value=$MAIN[autoendlimit]> " . $lang_settings['text_autoend_limit_note'], 1);
//无做种自动取消促销
	yesorno($lang_settings['row_enable_sticky_system'], 'sticky', $MAIN['sticky'], $lang_settings['row_allow_sticky']);
	tr($lang_settings['row_sticky_time'], "<input type='text' name=stickytime style=\"width: 100px\" value=$MAIN[stickytime]> " . $lang_settings['text_sticky_time_note'], 1);
//自定义功能结束
	//yesorno($lang_settings['row_enable_cardreg_system'], 'cardreg', $MAIN['cardreg'], $lang_settings['row_allow_cardreg']); //开启校内用户注册
	tr($lang_settings['row_verification_type'], "<input type='radio' name='verification'" . ($MAIN["verification"] == "email" ? " checked" : " checked") . " value='email'> " . $lang_settings['text_email'] . " <input type='radio' name='verification'" . ($MAIN["verification"] == "admin" ? " checked" : "") . " value='admin'> " . $lang_settings['text_admin'] . " <input type='radio' name='verification'" . ($MAIN["verification"] == "automatic" ? " checked" : "") . " value='automatic'> " . $lang_settings['text_automatically'] . "<br />" . $lang_settings['text_verification_type_note'], 1);
	yesorno($lang_settings['row_enable_wait_system'], 'waitsystem', $MAIN['waitsystem'], $lang_settings['text_wait_system_note']);
	yesorno($lang_settings['row_enable_max_slots_system'], 'maxdlsystem', $MAIN['maxdlsystem'], $lang_settings['text_max_slots_system_note']);
	yesorno($lang_settings['row_show_polls'], 'showpolls', $MAIN['showpolls'], $lang_settings['text_show_polls_note']);
	yesorno($lang_settings['row_show_stats'], 'showstats', $MAIN['showstats'], $lang_settings['text_show_stats_note']);
	//yesorno($lang_settings['row_show_last_posts'], 'showlastxforumposts', $MAIN['showlastxforumposts'], $lang_settings['text_show_last_posts_note']); //论坛热帖
	//yesorno($lang_settings['row_show_last_torrents'], 'showlastxtorrents', $MAIN['showlastxtorrents'], $lang_settings['text_show_last_torrents_note']); //最近种子
	yesorno($lang_settings['row_show_server_load'], 'showtrackerload', $MAIN['showtrackerload'], $lang_settings['text_show_server_load_note']);
	yesorno($lang_settings['row_show_forum_stats'], 'showforumstats', $MAIN['showforumstats'], $lang_settings['text_show_forum_stats_note']);
	//yesorno($lang_settings['row_show_hot'], 'showhotmovies', $MAIN['showhotmovies'], $lang_settings['text_show_hot_note']); //热门资源
	//yesorno($lang_settings['row_show_classic'], 'showclassicmovies', $MAIN['showclassicmovies'], $lang_settings['text_show_classic_note']); //经典资源
	yesorno($lang_settings['row_enable_imdb_system'], 'showimdbinfo', $MAIN['showimdbinfo'], $lang_settings['text_imdb_system_note']);
	//yesorno($lang_settings['row_enable_nfo'], 'enablenfo', $MAIN['enablenfo'], $lang_settings['text_enable_nfo_note']); //启用NFO
	//yesorno($lang_settings['row_enable_school_system'], 'enableschool', $MAIN['enableschool'], $lang_settings['text_school_system_note']); //学校系统
	yesorno($lang_settings['row_restrict_email_domain'], 'restrictemail', $MAIN['restrictemail'], $lang_settings['text_restrict_email_domain_note']);
	yesorno($lang_settings['row_show_shoutbox'], 'showshoutbox', $MAIN['showshoutbox'], $lang_settings['text_show_shoutbox_note']);
	yesorno($lang_settings['row_show_funbox'], 'showfunbox', $MAIN['showfunbox'], $lang_settings['text_show_funbox_note']);
	//yesorno($lang_settings['row_enable_offer_section'], 'showoffer', $MAIN['showoffer'], $lang_settings['text_offer_section_note']); //原始候选区
	yesorno($lang_settings['row_show_donation'], 'donation', $MAIN['donation'], $lang_settings['text_show_donation_note']);
	yesorno($lang_settings['row_weekend_free_uploading'], 'sptime', $MAIN['sptime'], $lang_settings['text_weekend_free_uploading_note']);
	yesorno($lang_settings['row_enable_helpbox'], 'showhelpbox', $MAIN['showhelpbox'], $lang_settings['text_helpbox_note']);
	yesorno($lang_settings['row_enable_bitbucket'], 'enablebitbucket', $MAIN['enablebitbucket'], $lang_settings['text_bitbucket_note']);
	yesorno($lang_settings['row_enable_small_description'], 'smalldescription', $MAIN['smalldescription'], $lang_settings['text_small_description_note']);
	yesorno($lang_settings['row_use_external_forum'], 'extforum', $MAIN['extforum'], $lang_settings['text_use_external_forum_note']);
	tr($lang_settings['row_external_forum_url'], "<input type='text' style=\"width: 300px\" name=extforumurl value='" . ($MAIN["extforumurl"] ? $MAIN["extforumurl"] : "") . "'> " . $lang_settings['text_external_forum_url_note'], 1);
	$res1 = sql_query("SELECT id, name FROM searchbox") or sqlerr(__FILE__, __LINE__);
	while ($array = mysql_fetch_array($res1)) {
		$bcatlist = "<input type=radio name=browsecat value='" . $array['id'] . "'" . ($MAIN["browsecat"] == $array['id'] ? " checked" : "") . ">" . $array['name'] . "&nbsp;";
	}
	tr($lang_settings['row_torrents_category_mode'], $bcatlist . "<br />" . $lang_settings['text_torrents_category_mode_note'], 1);
	$res2 = sql_query("SELECT * FROM language WHERE site_lang = 1") or sqlerr(__FILE__, __LINE__);
	$langlist = "";
	while ($array = mysql_fetch_array($res2))
		$langlist .= "<input type=radio name=defaultlang value='" . $array['site_lang_folder'] . "'" . ($MAIN["defaultlang"] == $array['site_lang_folder'] ? " checked" : "") . ">" . $array['lang_name'] . "&nbsp;";
	tr($lang_settings['row_default_site_language'], $langlist . "<br />" . $lang_settings['text_default_site_language_note'], 1);
	$res3 = sql_query("SELECT * FROM stylesheets ORDER BY name") or sqlerr(__FILE__, __LINE__);
	$csslist = "<select name=defstylesheet>";
	while ($array = mysql_fetch_array($res3))
		$csslist .= "<option value='" . $array['id'] . "'" . ($MAIN["defstylesheet"] == $array['id'] ? " selected" : "") . ">" . $array['name'] . "</option>";
	$csslist .= "</select>";
	yesorno("界面主题开关", 'stylesheetoff', $MAIN['stylesheetoff'], "默认'是'。设置是否允许用户自主更换界面主题。");
	tr($lang_settings['row_default_stylesheet'], $csslist . "<br />" . $lang_settings['text_default_stylesheet_note'], 1);
	tr($lang_settings['row_site_logo'], "<input type='text' style=\"width: 100px\" name='logo' value='" . ($MAIN["logo"] ? $MAIN["logo"] : "") . "'>" . $lang_settings['text_site_logo_note'], 1);
	tr("LOGO尺寸", "<input type='text' style=\"width: 50px\" name='logowidth' onkeyup=\"this.value = this.value.replace(/\D/g, '');\" value='" . ($MAIN["logowidth"] != 0 ? $MAIN["logowidth"] : "0") . "'>宽 x <input type='text' style=\"width: 50px\" name='logoheight' onkeyup=\"this.value = this.value.replace(/\D/g, '');\" value='" . ($MAIN["logoheight"] != 0 ? $MAIN["logoheight"] : "0") . "'>高，建议LOGO图片实际尺寸除以2，比如实际尺寸是200x100，这里就写100x50。", 1);
	tr($lang_settings['row_max_torrent_size'], "<input type='text' style=\"width: 100px\" name='max_torrent_size' value='" . ($MAIN["max_torrent_size"] ? $MAIN["max_torrent_size"] : 1048576) . "'>" . $lang_settings['text_max_torrent_size_note'], 1);
	tr($lang_settings['row_announce_interval'], $lang_settings['text_announce_interval_note_one'] . "<br /><ul><li>" . $lang_settings['text_announce_default'] . "<input type='text' style=\"width: 100px\" name=announce_interval value='" . ($MAIN["announce_interval"] ? $MAIN["announce_interval"] : 1800) . "'> " . $lang_settings['text_announce_default_default'] . "</li><li>" . $lang_settings['text_for_torrents_older_than'] . "<input type='text' style=\"width: 100px\" name=annintertwoage value='" . ($MAIN["annintertwoage"] ? $MAIN["annintertwoage"] : 7) . "'>" . $lang_settings['text_days'] . "<input type='text' style=\"width: 100px\" name=annintertwo value='" . ($MAIN["annintertwo"] ? $MAIN["annintertwo"] : 2700) . "'> " . $lang_settings['text_announce_two_default'] . "</li><li>" . $lang_settings['text_for_torrents_older_than'] . "<input type='text' style=\"width: 100px\" name=anninterthreeage value='" . ($MAIN["anninterthreeage"] ? $MAIN["anninterthreeage"] : 30) . "'>" . $lang_settings['text_days'] . "<input type='text' style=\"width: 100px\" name=anninterthree value='" . ($MAIN["anninterthree"] ? $MAIN["anninterthree"] : 3600) . "'> " . $lang_settings['text_announce_three_default'] . "</li></ul>" . $lang_settings['text_announce_interval_note_two'], 1);
	tr($lang_settings['row_cleanup_interval'], $lang_settings['text_cleanup_interval_note_one'] . "<br /><ul><li>" . $lang_settings['text_priority_one'] . "<input type='text' style=\"width: 100px\" name=autoclean_interval_one value='" . ($MAIN["autoclean_interval_one"] ? $MAIN["autoclean_interval_one"] : 900) . "'> " . $lang_settings['text_priority_one_note'] . "</li><li>" . $lang_settings['text_priority_two'] . "<input type='text' style=\"width: 100px\" name=autoclean_interval_two value='" . ($MAIN["autoclean_interval_two"] ? $MAIN["autoclean_interval_two"] : 1800) . "'> " . $lang_settings['text_priority_two_note'] . "</li><li>" . $lang_settings['text_priority_three'] . "<input type='text' style=\"width: 100px\" name=autoclean_interval_three value='" . ($MAIN["autoclean_interval_three"] ? $MAIN["autoclean_interval_three"] : 3600) . "'> " . $lang_settings['text_priority_three_note'] . "</li><li>" . $lang_settings['text_priority_four'] . "<input type='text' style=\"width: 100px\" name=autoclean_interval_four value='" . ($MAIN["autoclean_interval_four"] ? $MAIN["autoclean_interval_four"] : 43200) . "'> " . $lang_settings['text_priority_four_note'] . "</li><li>" . $lang_settings['text_priority_five'] . "<input type='text' style=\"width: 100px\" name=autoclean_interval_five value='" . ($MAIN["autoclean_interval_five"] ? $MAIN["autoclean_interval_five"] : 86400) . "'> " . $lang_settings['text_priority_five_note'] . "</li></ul>" . $lang_settings['text_cleanup_interval_note_two'], 1);
	tr($lang_settings['row_signup_timeout'], "<input type='text' style=\"width: 100px\" name=signup_timeout value='" . ($MAIN["signup_timeout"] ? $MAIN["signup_timeout"] : 259200) . "'> " . $lang_settings['text_signup_timeout_note'], 1);
	//tr($lang_settings['row_min_offer_votes'], "<input type='text' style=\"width: 100px\" name=minoffervotes value='" . ($MAIN["minoffervotes"] ? $MAIN["minoffervotes"] : 15) . "'> " . $lang_settings['text_min_offer_votes_note'], 1); //最低候选投票
	//tr($lang_settings['row_offer_vote_timeout'], "<input type='text' style=\"width: 100px\" name=offervotetimeout value='" . (isset($MAIN["offervotetimeout"]) ? $MAIN["offervotetimeout"] : 259200) . "'> " . $lang_settings['text_offer_vote_timeout_note'], 1); //候选投票超时
	//tr($lang_settings['row_offer_upload_timeout'], "<input type='text' style=\"width: 100px\" name=offeruptimeout value='" . (isset($MAIN["offeruptimeout"]) ? $MAIN["offeruptimeout"] : 86400) . "'> " . $lang_settings['text_offer_upload_timeout_note'], 1); //候选发布超时
	tr($lang_settings['row_max_subtitle_size'], "<input type='text' style=\"width: 100px\" name=maxsubsize value='" . (isset($MAIN["maxsubsize"]) ? $MAIN["maxsubsize"] : 3145728) . "'> " . $lang_settings['text_max_subtitle_size_note'], 1);
	tr($lang_settings['row_posts_per_page'], "<input type='text' style=\"width: 100px\" name=postsperpage value='" . ($MAIN["postsperpage"] ? $MAIN["postsperpage"] : 10) . "'> " . $lang_settings['text_posts_per_page_note'], 1);
	tr($lang_settings['row_topics_per_page'], "<input type='text' style=\"width: 100px\" name=topicsperpage value='" . ($MAIN["topicsperpage"] ? $MAIN["topicsperpage"] : 20) . "'> " . $lang_settings['text_topics_per_page_note'], 1);
	tr($lang_settings['row_torrents_per_page'], "<input type='text' style=\"width: 100px\" name=torrentsperpage value='" . ($MAIN["torrentsperpage"] ? $MAIN["torrentsperpage"] : 50) . "'> " . $lang_settings['text_torrents_per_page_note'], 1);
	tr($lang_settings['row_number_of_news'], "<input type='text' style=\"width: 100px\" name=maxnewsnum value='" . ($MAIN["maxnewsnum"] ? $MAIN["maxnewsnum"] : 3) . "'> " . $lang_settings['text_number_of_news_note'], 1);
	tr($lang_settings['row_torrent_dead_time'], "<input type='text' style=\"width: 100px\" name=max_dead_torrent_time value='" . ($MAIN["max_dead_torrent_time"] ? $MAIN["max_dead_torrent_time"] : "21600") . "'> " . $lang_settings['text_torrent_dead_time_note'], 1);
	tr($lang_settings['row_max_users'], "<input type='text' style=\"width: 100px\" name=maxusers value='" . ($MAIN["maxusers"] ? $MAIN["maxusers"] : "2500" ) . "'> " . $lang_settings['text_max_users'], 1);
	tr($lang_settings['row_robot_users'], "<input type='text' style=\"width: 100px\" name=robotusers value='" . ($MAIN["robotusers"] ? $MAIN["robotusers"] : "2" ) . "'> " . $lang_settings['text_robot_users'], 1);
	tr($lang_settings['row_site_accountant_userid'], "<input type='text' style=\"width: 200px\" name=\"ACCOUNTANTID\" value='" . ($MAIN['ACCOUNTANTID'] ? $MAIN['ACCOUNTANTID'] : "") . "'> " . $lang_settings['text_site_accountant_userid_note'], 1);
	tr($lang_settings['row_alipay_account'], "<input type='text' style=\"width: 200px\" name=\"ALIPAYACCOUNT\" value='" . ($MAIN['ALIPAYACCOUNT'] ? $MAIN['ALIPAYACCOUNT'] : "") . "'> " . $lang_settings['text_alipal_account_note'], 1);
	tr($lang_settings['row_paypal_account'], "<input type='text' style=\"width: 200px\" name=PAYPALACCOUNT value='" . ($MAIN["PAYPALACCOUNT"] ? $MAIN["PAYPALACCOUNT"] : "") . "'> " . $lang_settings['text_paypal_account_note'], 1);
	tr($lang_settings['row_site_email'], "<input type='text' style=\"width: 200px\" name=SITEEMAIL value='" . ($MAIN["SITEEMAIL"] ? $MAIN["SITEEMAIL"] : "noreply@" . $sh) . "'> " . $lang_settings['text_site_email_note'], 1);
	tr($lang_settings['row_report_email'], "<input type='text' style=\"width: 200px\" name=reportemail value='" . ($MAIN["reportemail"] ? $MAIN["reportemail"] : "report@" . $sh) . "'> " . $lang_settings['text_report_email_note'], 1);
	tr($lang_settings['row_site_slogan'], "<input type='text' style=\"width: 300px\" name=SLOGAN value='" . ($MAIN["SLOGAN"] ? $MAIN["SLOGAN"] : "") . "'> " . $lang_settings['text_site_slogan_note'], 1);
	tr($lang_settings['row_icp_license'], "<input type='text' style=\"width: 300px\" name=icplicense value='" . ($MAIN["icplicense"] ? $MAIN["icplicense"] : "") . "'> " . $lang_settings['text_icp_license_note'], 1);
	tr($lang_settings['row_torrent_directory'], "<input type='text' style=\"width: 100px\" name=torrent_dir value='" . ($MAIN["torrent_dir"] ? $MAIN["torrent_dir"] : "torrents") . "'> " . $lang_settings['text_torrent_directory'], 1);
	tr($lang_settings['row_bitbucket_directory'], "<input type='text' style=\"width: 100px\" name=bitbucket value='" . ($MAIN["bitbucket"] ? $MAIN["bitbucket"] : "bitbucket") . "'> " . $lang_settings['text_bitbucket_directory_note'], 1);
	tr($lang_settings['row_torrent_name_prefix'], "<input type='text' style=\"width: 100px\" name=torrentnameprefix value='" . ($MAIN["torrentnameprefix"] ? $MAIN["torrentnameprefix"] : "[Nexus]") . "'> " . $lang_settings['text_torrent_name_prefix_note'], 1);
	tr($lang_settings['row_save_settings'], "<input type='submit' name='save' value='" . $lang_settings['submit_save_settings'] . "'>", 1);
	print ("</form>");
} elseif ($action == 'showmenu') { // settings main page
	stdhead($lang_settings['head_website_settings']);
	print ($notice);
	tr($lang_settings['row_basic_settings'], "<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='basicsettings'><input type='submit' value=\"" . $lang_settings['submit_basic_settings'] . "\"> " . $lang_settings['text_basic_settings_note'] . "</form>", 1);
	tr($lang_settings['row_main_settings'], "<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='mainsettings'><input type='submit' value=\"" . $lang_settings['submit_main_settings'] . "\"> " . $lang_settings['text_main_settings_note'] . "</form>", 1);
	tr($lang_settings['row_tweak_settings'], "<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='tweaksettings'><input type='submit' value=\"" . $lang_settings['submit_tweak_settings'] . "\"> " . $lang_settings['text_tweak_settings_note'] . "</form>", 1);
	tr($lang_settings['row_security_settings'], "<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='securitysettings'><input type='submit' value=\"" . $lang_settings['submit_security_settings'] . "\"> " . $lang_settings['text_security_settings_note'] . "</form>", 1);
	tr($lang_settings['row_authority_settings'], "<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='authoritysettings'><input type='submit' value=\"" . $lang_settings['submit_authority_settings'] . "\"> " . $lang_settings['text_authority_settings_note'] . "</form>", 1);
	tr($lang_settings['row_account_settings'], "<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='accountsettings'><input type='submit' value=\"" . $lang_settings['submit_account_settings'] . "\"> " . $lang_settings['text_account_settings_settings'] . "</form>", 1);
	tr($lang_settings['row_torrents_settings'], "<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='torrentsettings'><input type='submit' value=\"" . $lang_settings['submit_torrents_settings'] . "\"> " . $lang_settings['text_torrents_settings_note'] . "</form>", 1);
	tr($lang_settings['row_attachment_settings'], "<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='attachmentsettings'><input type='submit' value=\"" . $lang_settings['submit_attachment_settings'] . "\"> " . $lang_settings['text_attachment_settings_note'] . "</form>", 1);
	tr($lang_settings['row_smtp_settings'], "<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='smtpsettings'><input type='submit' value=\"" . $lang_settings['submit_smtp_settings'] . "\"> " . $lang_settings['text_smtp_settings_note'] . "</form>", 1);
	tr($lang_settings['row_bonus_settings'], "<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='bonussettings'><input type='submit' value=\"" . $lang_settings['submit_bonus_settings'] . "\"> " . $lang_settings['text_bonus_settings_note'] . "</form>", 1);
	tr($lang_settings['row_check_settings'], "<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='checksettings'><input type='submit' value=\"" . $lang_settings['submit_check_settings'] . "\"> " . $lang_settings['text_check_settings_note'] . "</form>", 1);
	tr($lang_settings['row_advertisement_settings'], "<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='advertisementsettings'><input type='submit' value=\"" . $lang_settings['submit_advertisement_settings'] . "\"> " . $lang_settings['text_advertisement_settings_note'] . "</form>", 1);
	tr($lang_settings['row_code_settings'], "<form method='post' action='" . $_SERVER["SCRIPT_NAME"] . "'><input type='hidden' name='action' value='codesettings'><input type='submit' value=\"" . $lang_settings['submit_code_settings'] . "\"> " . $lang_settings['text_code_settings_note'] . "</form>", 1);
}
print("</table>");
stdfoot();
