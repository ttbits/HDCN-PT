<?php

require_once("include/bittorrent.php");
dbconn();
require_once(get_langfile_path());
loggedinorreturn();
checkserver();

function bark($msg) {
	global $lang_takeflush;
	stdhead();
	stdmsg($lang_takeflush['std_failed'], $msg);
	stdfoot();
	exit;
}

$id = 0 + $_GET['userid'];
int_check($id, true);
if (get_user_class() >= UC_MODERATOR || $CURUSER['id'] == $id) {
	//$deadtime = time();
	//sql_query("DELETE FROM peers WHERE last_action < FROM_UNIXTIME($deadtime) AND userid = $id);
	/*
	 * SELECT id FROM (SELECT id FROM peers GROUP BY userid, torrent HAVING COUNT(torrent) > 1) AS a
	 * 上面是列出重复记录中的旧ID记录
	 * SELECT id FROM (SELECT MAX(id) AS id FROM peers GROUP BY userid, torrent HAVING COUNT(torrent) > 1) AS b
	 * 上面是列出重复记录中的新ID纪录
	 */
	sql_query("DELETE FROM peers WHERE id IN (SELECT id FROM (SELECT id FROM peers GROUP BY userid, torrent HAVING COUNT(torrent) > 1) AS a) AND id NOT IN (SELECT id FROM (SELECT MAX(id) AS id FROM peers GROUP BY userid, torrent HAVING COUNT(torrent) > 1) AS b)") or sqlerr(__FILE__, __LINE__);
	$effected = mysql_affected_rows();
	stderr($lang_takeflush['std_success'], "$effected " . $lang_takeflush['std_ghost_torrents_cleaned']);
} else {
	bark($lang_takeflush['std_cannot_flush_others']);
}
