<?php

require_once("include/bencode.php");
$bencode = new BEncode();
require_once("include/bittorrent.php");
if (isset($_SESSION['upload']) && isset($_POST['upload']) && $_POST['upload'] == $_SESSION['upload']) {
	unset($_SESSION['upload']);
} else {
	stderr("失败", "请勿重复提交");
}
ini_set("upload_max_filesize", $max_torrent_size);
dbconn();
require_once(get_langfile_path());
require(get_langfile_path("", true));
loggedinorreturn();
checkserver();

//定时发布
if ($_POST['releasedate'] != '') {
	$is_date = strtotime($_POST['releasedate']) ? TRUE : FALSE;
	if ($is_date == FALSE) {
		stderr("错误", "非法的定时发布日期格式");
	}
	$updateset[] = "releasedate = '$_POST[releasedate]'";
} else {
	$updateset[] = "releasedate = ''";
}

function bark($msg) {
	global $lang_takeupload;
	genbark($msg, $lang_takeupload['std_upload_failed']);
	die;
}

if ($CURUSER["uploadpos"] == 'no')
	die;

foreach (explode(":", "descr:type:name") as $v) {
	if (!isset($_POST[$v]))
		bark($lang_takeupload['std_missing_form_data']);
}

if (!isset($_FILES["file"]))
	bark($lang_takeupload['std_missing_form_data']);

$f = $_FILES["file"];
$fname = unesc($f["name"]);
if (empty($fname))
	bark($lang_takeupload['std_empty_filename']);
if (get_user_class() >= $beanonymous_class && $_POST['uplver'] == 'yes') {
	$anonymous = "yes";
	$anon = "匿名";
} else {
	$anonymous = "no";
	$anon = $CURUSER["username"];
}

$url = parse_imdb_id($_POST['imdburl']);
$dburl = parse_douban_id($_POST['dburl']);

$nfo = '';
if ($enablenfo_main == 'yes') {
	$nfofile = $_FILES['nfo'];
	if ($nfofile['name'] != '') {

		if ($nfofile['size'] == 0)
			bark($lang_takeupload['std_zero_byte_nfo']);

		if ($nfofile['size'] > 65535)
			bark($lang_takeupload['std_nfo_too_big']);

		$nfofilename = $nfofile['tmp_name'];

		if (!is_uploaded_file($nfofilename))
			bark($lang_takeupload['std_nfo_upload_failed']);
		$nfo = str_replace("\x0d\x0d\x0a", "\x0d\x0a", file_get_contents($nfofilename));
	}
}

$small_descr = unesc(RemoveXSS(trim($_POST["small_descr"])));
$descr = unesc(RemoveXSS(trim($_POST["descr"])));
$namecolor = ($_POST['cxcolortitle'] == '' ? "#000000" : $_POST['cxcolortitle']);
$descrcolor = ($_POST['cxcolordesc'] == '' ? "#000000" : $_POST['cxcolordesc']);
if (!$descr) {
	bark($lang_takeupload['std_blank_description']);
}

$catid = (0 + $_POST["type"]);
$sourceid = (0 + $_POST["source_sel"]);
$mediumid = (0 + $_POST["medium_sel"]);
$codecid = (0 + $_POST["codec_sel"]);
$standardid = (0 + $_POST["standard_sel"]);
$processingid = (0 + $_POST["processing_sel"]);
$teamid = (0 + $_POST["team_sel"]);
$audiocodecid = (0 + $_POST["audiocodec_sel"]);
$offerzone = $_POST['offerzone'];

if (!is_valid_id($catid))
	bark($lang_takeupload['std_category_unselected']);

if (!validfilename($fname))
	bark($lang_takeupload['std_invalid_filename']);
if (!preg_match('/^(.+)\.torrent$/si', $fname, $matches))
	bark($lang_takeupload['std_filename_not_torrent']);
$shortfname = $torrent = $matches[1];
if (!empty($_POST["name"]))
	$torrent = unesc(RemoveXSS(trim($_POST["name"])));
if ($f['size'] > $max_torrent_size)
	bark($lang_takeupload['std_torrent_file_too_big'] . number_format($max_torrent_size) . $lang_takeupload['std_remake_torrent_note']);
$tmpname = $f["tmp_name"];
if (!is_uploaded_file($tmpname))
	bark('文件错误');
if (!filesize($tmpname))
	bark($lang_takeupload['std_empty_file']);

$dict = $bencode->bdecode_file($tmpname); //读入种子文件
if (!isset($dict))
	bark($lang_takeupload['std_not_bencoded_file']);
if ($sourceid == 0)
	bark($lang_takeupload['std_category_unlidselected']);
$torrentinfo = $bencode->filelist($dict); //获取种子文件数，总大小和文件列表
$filelist = $torrentinfo['files']; //种子内文件
$totallen = $torrentinfo['total_size']; //种子总大小
$filenum = $torrentinfo['file_count']; //种子文件总数
if ($filenum == 1) {
	$type = 'single';
} else {
	$type = 'multi';
}
$dict['announce'] = get_protocol_prefix() . $announce_urls[0];
$dict['comment'] = PROJECTNAME;
$dict['info']['private'] = 1;
$dict['info']['source'] = $BASEURL; //机器人上传通道，请务必注释该行
/*
  foreach (generate() as $arr) {//随机生成标签码
  $dict['info']]['tag'] = $arr;
  }
  unset($dict['info']['ttg_tag']);
  unset($dict['info']['trackedby']);
  unset($dict['info']['profiles']);
  unset($dict['info']['file-duration']);
  unset($dict['info']['file-media']);
 *
 */
unset($dict['logo']);
unset($dict['labels']);
unset($dict['nodes']);
unset($dict['url-list']);
unset($dict['announce-list']);
unset($dict['libtorrent_resume']);
unset($dict['azureus_properties']);
$dicts = $bencode->bdecode($bencode->bencode($dict));
$infohash = pack("H*", sha1($bencode->bencode($dicts['info'])));

function hex_esc2($matches) {
	return sprintf("%02x", ord($matches[0]));
}

$allowtorrents = user_can_upload("torrents");
$allowspecial = user_can_upload("music");
$catmod = get_single_value("categories", "mode", "WHERE id=" . sqlesc($catid));
$offerid = $_POST['offer'];
$is_offer = false;
if ($browsecatmode != $specialcatmode && $catmod == $specialcatmode) {

} elseif ($catmod == $browsecatmode) {
	if ($offerid) {
		$allowed_offer_count = get_row_count("offers", "WHERE allowed='allowed' AND userid=" . sqlesc($CURUSER["id"]));
		if ($allowed_offer_count && $enableoffer == 'yes') {
			$allowed_offer = get_row_count("offers", "WHERE id=" . sqlesc($offerid) . " AND allowed='allowed' AND userid=" . sqlesc($CURUSER["id"]));
			if ($allowed_offer != 1)
				bark($lang_takeupload['std_uploaded_not_offered']);
			else
				$is_offer = true;
		} else
			bark($lang_takeupload['std_uploaded_not_offered']);
	}
} else
	die("上传到二次元了？！嗯嗯！！");

if ($largesize_torrent && $totallen > ($largesize_torrent * 1073741824)) {
	if ($ratioless == 'yes') {
		switch ($largepro_torrent) {
			case 3://2x
				{
					$sp_state = 3;
					break;
				}
			default: {
					$sp_state = 1;
					break;
				}
		}
	} else {
		switch ($largepro_torrent) {
			case 2://Free
				{
					$sp_state = 2;
					break;
				}
			case 3://2x
				{
					$sp_state = 3;
					break;
				}
			case 4://2xFree
				{
					$sp_state = 4;
					break;
				}
			case 5://50%
				{
					$sp_state = 5;
					break;
				}
			case 6://2x50%
				{
					$sp_state = 6;
					break;
				}
			case 7://30%
				{
					$sp_state = 7;
					break;
				}
			default: {
					$sp_state = 1;
					break;
				}
		}
	}
} elseif ($prorules_torrent == 'yes') {
	if ($ratioless == 'no') {
		$sp_id = mt_rand(1, 100);
		if ($sp_id <= ($probability = $randomtwoupfree_torrent))//2xFree
			$sp_state = 4;
		elseif ($sp_id <= ($probability += $randomtwoup_torrent))//2x
			$sp_state = 3;
		elseif ($sp_id <= ($probability += $randomfree_torrent))//Free
			$sp_state = 2;
		elseif ($sp_id <= ($probability += $randomhalfleech_torrent))//50%
			$sp_state = 5;
		elseif ($sp_id <= ($probability += $randomtwouphalfdown_torrent))//2x50%
			$sp_state = 6;
		elseif ($sp_id <= ($probability += $randomthirtypercentdown_torrent))//30%
			$sp_state = 7;
		else
			$sp_state = 1;
		if ($sp_id <= ($probability += $randomtwoup_torrent))//2x
			$sp_state = 3;
		else
			$sp_state = 1;
	}
} else {
	$sp_state = 1;
}

if ($prorules_torrent == 'yes') {
	foreach ($promotionrules_torrent as $rule) {
		if (!array_key_exists('catid', $rule) || in_array($catid, $rule['catid']))
			if (!array_key_exists('sourceid', $rule) || in_array($sourceid, $rule['sourceid']))
				if (!array_key_exists('mediumid', $rule) || in_array($mediumid, $rule['mediumid']))
					if (!array_key_exists('codecid', $rule) || in_array($codecid, $rule['codecid']))
						if (!array_key_exists('standardid', $rule) || in_array($standardid, $rule['standardid']))
							if (!array_key_exists('processingid', $rule) || in_array($processingid, $rule['processingid']))
								if (!array_key_exists('teamid', $rule) || in_array($teamid, $rule['teamid']))
									if (!array_key_exists('audiocodecid', $rule) || in_array($audiocodecid, $rule['audiocodecid']))
										if (!array_key_exists('pattern', $rule) || preg_match($rule['pattern'], $torrent))
											if (is_numeric($rule['promotion'])) {
												$sp_state = $rule['promotion'];
												break;
											}
	}
}
//$status = get_user_class() < UC_POWER_USER ? "candidate" : "normal";
$status = get_user_class() < $upload_class && $CURUSER['id'] != $robotusers || $_POST['releasedate'] != '' ? "candidate" : "normal";
$visible = get_user_class() >= $upload_class ? "yes" : "no";
$ret = sql_query("INSERT INTO torrents (filename, owner, visible, anonymous, name, size, numfiles, type, dburl, url, small_descr, descr, ori_descr, category, source, medium, codec, audiocodec, standard, processing, team, save_as, sp_state, added, last_action, nfo, info_hash, last_status, status, namecolor, descrcolor) VALUES (" . sqlesc($fname) . ", " . sqlesc($CURUSER["id"]) . ", " . sqlesc($visible) . ", " . sqlesc($anonymous) . ", " . sqlesc($torrent) . ", " . sqlesc($totallen) . ", " . $filenum . ", " . sqlesc($type) . ", " . sqlesc($dburl) . ", " . sqlesc($url) . ", " . sqlesc($small_descr) . ", " . sqlesc($descr) . ", " . sqlesc($descr) . ", " . sqlesc($catid) . ", " . sqlesc($sourceid) . ", " . sqlesc($mediumid) . ", " . sqlesc($codecid) . ", " . sqlesc($audiocodecid) . ", " . sqlesc($standardid) . ", " . sqlesc($processingid) . ", " . sqlesc($teamid) . ", " . sqlesc($dicts['info']['name']) . ", " . sqlesc($sp_state) . ", " . sqlesc(date("Y-m-d H:i:s")) . ", " . sqlesc(date("Y-m-d H:i:s")) . ", " . sqlesc($nfo) . ", " . sqlesc($infohash) . ", " . sqlesc(date("Y-m-d H:i:s")) . ", '$status', " . sqlesc($namecolor) . ", " . sqlesc($descrcolor) . ")");
if (!$ret) {
	if (mysql_errno() == 1062)
		bark($lang_takeupload['std_torrent_existed']);
	bark(mysql_error());
}
$id = mysql_insert_id();
//获取种子文件列表写入files表
sql_query("DELETE FROM files WHERE torrent = $id");
foreach ($filelist as $file) {
	sql_query("INSERT INTO files (torrent, filename, size) VALUES ($id, " . sqlesc($file['name']) . ", " . $file['size'] . ")");
}

//move_uploaded_file($tmpname, "$torrent_dir/$id.torrent");
$fp = fopen("$torrent_dir/$id.torrent", "w");
if ($fp) {
	fwrite($fp, $bencode->bencode($dict), strlen($bencode->bencode($dict)));
	fclose($fp);
}

//设置促销
$place_info = "";
if (get_user_class() >= $torrentonpromotion_class || $CURUSER['id'] == $robotusers && $row['sp_state'] != $_POST['sel_spstate']) {
	if (!isset($_POST["sel_spstate"])) {
		$updateset[] = "sp_state = $sp_state";
	}
	if ($_POST["sel_spstate"] == 1) {
		$updateset[] = "sp_state = $sp_state";
		$place_info .= "取消促销。";
		$notshout = 1;
	} elseif ((0 + $_POST["sel_spstate"]) == 2) {
		$updateset[] = "sp_state = 2";
		$place_info .= "设置为Free";
	} elseif ((0 + $_POST["sel_spstate"]) == 3) {
		$updateset[] = "sp_state = 3";
		$place_info .= "设置为2x";
	} elseif ((0 + $_POST["sel_spstate"]) == 4) {
		$updateset[] = "sp_state = 4";
		$place_info .= "设置为2xFree";
	} elseif ((0 + $_POST["sel_spstate"]) == 5) {
		$updateset[] = "sp_state = 5";
		$place_info .= "设置为50%";
	} elseif ((0 + $_POST["sel_spstate"]) == 6) {
		$updateset[] = "sp_state = 6";
		$place_info .= "设置为2x50%";
	} elseif ((0 + $_POST["sel_spstate"]) == 7) {
		$updateset[] = "sp_state = 7";
		$place_info .= "设置为30%";
	}
}
//设置免费时长
if (isset($_POST['freetime']) && (0 + $_POST['freetime']) < 0) {
	$updateset[] = "endfree = '0000-00-00 00:00:00'";
	$place_info .= "，至【海枯石烂】";
} elseif ($_POST['sel_spstate'] != 1) {
	if (!empty($_POST['freetime']) || !empty($_POST['freetimeh'])) {
		if ($_POST['freetime'] != '') {
			$freetime = 0 + $_POST['freetime'];
		}
		if ($_POST['freetimeh'] != '') {
			$freetimeh = 0 + $_POST['freetimeh'];
		}
		$updateset[] = "endfree = '" . date("Y-m-d H:i:s", time() + 86400 * $freetime + 3600 * $freetimeh) . "'";
		$place_info .= "， 至" . date("Y-m-d H:i:s", time() + 86400 * $freetime + 3600 * $freetimeh) . "";
	} else {
		if (!empty($row['endfree']) && $row['endfree'] != '0000-00-00 00:00:00') {
			$place_info .= "，至" . $row['endfree'];
		}
	}
}

//设置置顶
if (get_user_class() >= $torrentsticky_class || $CURUSER['id'] == $robotusers) {
	if ((0 + $_POST["sel_posstate"]) == 0 && $row['pos_state'] != 'normal') {
		$updateset[] = "pos_state = 'normal'";
		$place_info .= "，取消置顶";
		$notshout = 1;
	} elseif ((0 + $_POST["sel_posstate"]) == 1 && $row['pos_state'] != 'sticky') {
		$updateset[] = "pos_state = 'sticky'";
		$place_info .= "，设置为置顶";
	}
}

//设置置顶时长
if (isset($_POST['stickytime']) && (0 + $_POST['stickytime']) < 0) {
	$updateset[] = "endsticky = '0000-00-00 00:00:00'";
	$place_info .= "，置顶至【海枯石烂】";
} elseif (!empty($_POST["sel_posstate"])) {
	if (!empty($_POST['stickytime']) || !empty($_POST['stickytimeh'])) {
		if ($_POST['stickytime'] != '') {
			$stickytime = 0 + $_POST['stickytime'];
		}
		if ($_POST['stickytimeh'] != '') {
			$stickytimeh = 0 + $_POST['stickytimeh'];
		}
		$updateset[] = "endsticky = '" . date("Y-m-d H:i:s", time() + 86400 * $stickytime + 3600 * $stickytimeh) . "'";
		$place_info .= "， 置顶至" . date("Y-m-d H:i:s", time() + 86400 * $stickytime + 3600 * $stickytimeh) . "";
	} else {//如果设置为置顶且没有设置时效，则默认时效为6小时
		$updateset[] = "endsticky = '" . date("Y-m-d H:i:s", time() + 3600 * 6) . "'";
		$place_info .= "， 置顶至" . date("Y-m-d H:i:s", time() + 3600 * 6) . "";
	}
}

//设置超级置顶
if (get_user_class() >= $torrentsticky_class || $CURUSER['id'] == $robotusers && $row['marrow'] != $_POST['sel_marrow']) {
	if ((0 + $_POST["sel_marrow"]) == 0 && $row['marrow'] != 'normal') {
		$updateset[] = "marrow = 'normal'";
		$place_info .= "，取消超级置顶";
		$notshout = 1;
	} elseif ((0 + $_POST["sel_marrow"]) == 1 && $row['marrow'] != 'marrow1') {
		$updateset[] = "marrow = 'marrow1'";
		$place_info .= "，设置为1级超级置顶";
	} elseif ((0 + $_POST["sel_marrow"]) == 2 && $row['marrow'] != 'marrow2') {
		$updateset[] = "marrow = 'marrow2'";
		$place_info .= "，设置为2级超级置顶";
	} elseif ((0 + $_POST["sel_marrow"]) == 3 && $row['marrow'] != 'marrow3') {
		$updateset[] = "marrow = 'marrow3'";
		$place_info .= "，设置为3级超级置顶";
	}
}

//设置超级置顶时长
if (isset($_POST['marrowtime']) && (0 + $_POST['marrowtime']) < 0) {
	$updateset[] = "endmarrow = '0000-00-00 00:00:00'";
	$place_info .= "，超级置顶至【海枯石烂】";
} elseif (!empty($_POST["sel_marrow"])) {
	if (!empty($_POST['marrowtime']) || !empty($_POST['marrowtimeh'])) {
		if ($_POST['marrowtime'] != '') {
			$marrowtime = 0 + $_POST['marrowtime'];
		}
		if ($_POST['marrowtimeh'] != '') {
			$marrowtimeh = 0 + $_POST['marrowtimeh'];
		}
		$updateset[] = "endmarrow = '" . date("Y-m-d H:i:s", time() + 86400 * $marrowtime + 3600 * $marrowtimeh) . "'";
		$place_info .= "， 超级置顶至" . date("Y-m-d H:i:s", time() + 86400 * $marrowtime + 3600 * $marrowtimeh) . "";
	} else {//如果设置为置顶且没有设置时效，则默认时效为6小时
		$updateset[] = "endmarrow = '" . date("Y-m-d H:i:s", time() + 3600 * 6) . "'";
		$place_info .= "， 置顶至" . date("Y-m-d H:i:s", time() + 3600 * 6) . "";
	}
}

if (!isset($_POST["promotion_time_type"]) || $_POST["promotion_time_type"] < 0) {
	$updateset[] = "promotion_time_type = 0";
	$updateset[] = "promotion_until = '0000-00-00 00:00:00'";
} elseif ($_POST["promotion_time_type"] == 1) {
	$updateset[] = "promotion_time_type = 1";
	$updateset[] = "promotion_until = '0000-00-00 00:00:00'";
} elseif ($_POST["promotion_time_type"] == 2) {
	if ($_POST["promotionuntil"] && strtotime($torrentAddedTimeString) <= strtotime($_POST["promotionuntil"])) {
		$updateset[] = "promotion_time_type = 2";
		$updateset[] = "promotion_until = " . sqlesc($_POST["promotionuntil"]);
	} else {
		$updateset[] = "promotion_time_type = 0";
		$updateset[] = "promotion_until = '0000-00-00 00:00:00'";
	}
}

//增加H&R编辑
if ($hr == 'yes' && get_user_class() >= $torrentonpromotion_class || $CURUSER['id'] == $robotusers) {
	if ((0 + $_POST["hr_posstate"]) == 0) {
		$updateset[] = "hr_state = 'normal'";
		$updateset[] = "hr_time = " . sqlesc($hrhour * 3600);
		$updateset[] = "hr_until = " . sqlesc($hrday * 86400);
	} elseif ((0 + $_POST["hr_posstate"]) == 1) {
		$updateset[] = "hr_state = 'sticky'";
		if ($_POST["hruntil"] && $_POST["hrtime"]) {
			$updateset[] = "hr_time = " . sqlesc(0 + $_POST["hrtime"] * 3600);
			$updateset[] = "hr_until = " . sqlesc(0 + $_POST["hruntil"] * 86400);
		}
	} elseif ((0 + $_POST["hr_posstate"]) == 2) {
		$updateset[] = "hr_state = 'no'";
	}
} else {
	if ((0 + $_POST["hr_posstate"]) == 0) {
		$updateset[] = "hr_state = 'normal'";
		$updateset[] = "hr_time = " . sqlesc($hrhour * 3600);
		$updateset[] = "hr_until = " . sqlesc($hrday * 86400);
	}
}

//增加首发禁转编辑
$updateset[] = "first = '" . ($_POST["first"] ? "yes" : "no") . "'";
$updateset[] = "excl = '" . ($_POST["excl"] ? "yes" : "no") . "'";
$updateset[] = "official = '" . ($_POST["official"] ? "yes" : "no") . "'";
$updateset[] = "limitd = '" . ($_POST["limitd"] ? "yes" : "no") . "'";

if ((get_user_class() >= $torrentmanage_class || $CURUSER['id'] == $robotusers && $CURUSER['picker'] == 'yes') || get_user_class() >= UC_ADMINISTRATOR) {
	if ((0 + $_POST["sel_recmovie"]) == 0 && $row["picktype"] != 'normal') {
		$pick_info = ", 取消推荐。";
		$updateset[] = "picktype = 'normal'";
		$updateset[] = "picktime = '0000-00-00 00:00:00'";
		$notshout = 1;
	} elseif ((0 + $_POST["sel_recmovie"]) == 1 && $row["picktype"] != 'hot') {
		$pick_info = ", 推荐为“热门”。";
		$updateset[] = "picktype = 'hot'";
		$updateset[] = "picktime = " . sqlesc(date("Y-m-d H:i:s"));
	} elseif ((0 + $_POST["sel_recmovie"]) == 2 && $row["picktype"] != 'classic') {
		$pick_info = ", 推荐为“经典”。";
		$updateset[] = "picktype = 'classic'";
		$updateset[] = "picktime = " . sqlesc(date("Y-m-d H:i:s"));
	} elseif ((0 + $_POST["sel_recmovie"]) == 3 && $row["picktype"] != 'recommended') {
		$pick_info = ", 推荐为“推荐”。";
		$updateset[] = "picktype = 'recommended'";
		$updateset[] = "picktime = " . sqlesc(date("Y-m-d H:i:s"));
	}
}

if ($offerzone == 'yes') {
	$updateset[] = "status = 'candidate'";
}

if ($cuxiao == 'yes') {//发布促销开关
	if ($ratioless == 'no') {
		if ($largesize_torrent && $totallen > ($largesize_torrent * 1073741824)) {
			$updateset[] = "sp_state = $cuxiaoselect";
			$updateset[] = "endfree = '0000-00-00 00:00:00'";
		} elseif ($_POST['sel_spstate'] == 1 || $_POST['sel_spstate'] == '') {
			if (!empty($cuxiaofree)) {
				$updateset[] = "sp_state = $cuxiaoselect";
				$updateset[] = "endfree = '" . date("Y-m-d H:i:s", time() + 86400 * $cuxiaofree) . "'";
			}
		}
	} else {
		if (!empty($cuxiaofree)) {
			$updateset[] = "sp_state = $cuxiaoselect";
			$updateset[] = "endfree = '" . date("Y-m-d H:i:s", time() + 86400 * $cuxiaofree) . "'";
		}
	}
	if ($_POST['sel_posstate'] == 0 || $_POST['sel_posstate'] == '') {
		if (!empty($cuxiaosticky)) {
			$updateset[] = "pos_state = 'sticky'";
			$updateset[] = "endsticky = '" . date("Y-m-d H:i:s", time() + 86400 * $cuxiaosticky) . "'";
		}
	}
	sql_query("UPDATE torrents SET " . join(",", $updateset) . " WHERE id = $id") or sqlerr(__FILE__, __LINE__);
} else {
	sql_query("UPDATE torrents SET " . join(",", $updateset) . " WHERE id = $id") or sqlerr(__FILE__, __LINE__);
}
/*
  $shoutbox = $pick_info . $place_info;
  if ($shoutbox && !$notshout) {
  sendshoutbox("管理员将 [url=details.php?id=$id] " . sqlesc($file[0]) . " [/url]" . $shoutbox . " ~快去看看吧o(￣︶￣)o");
  }
 *
 */
KPS("+", $uploadtorrent_bonus, $CURUSER["id"]);
$candidatelog = get_user_class() < $upload_class && $CURUSER['id'] != $robotusers ? "，由于等级不足，种子被列为候选" : "";
write_log("发布：用户 $anon 上传了种子 $candidatelog $id ($torrent) ");
if ($is_offer) {
	$res = sql_query("SELECT `userid` FROM `offervotes` WHERE `userid` != " . $CURUSER["id"] . " AND `offerid` = " . sqlesc($offerid) . " AND `vote` = 'yeah'") or sqlerr(__FILE__, __LINE__);
	while ($row = mysql_fetch_assoc($res)) {
		$pn_msg = $lang_takeupload_target[get_user_lang($row["userid"])]['msg_offer_you_voted'] . $torrent . $lang_takeupload_target[get_user_lang($row["userid"])]['msg_was_uploaded_by'] . $CURUSER["username"] . $lang_takeupload_target[get_user_lang($row["userid"])]['msg_you_can_download'] . "[url=" . get_protocol_prefix() . "$BASEURL/details.php?id=$id&hit=1]" . $lang_takeupload_target[get_user_lang($row["userid"])]['msg_here'] . "[/url]";
		$subject = $lang_takeupload_target[get_user_lang($row["userid"])]['msg_offer'] . $torrent . $lang_takeupload_target[get_user_lang($row["userid"])]['msg_was_just_uploaded'];
		sql_query("INSERT INTO messages (sender, subject, receiver, added, msg) VALUES (0, " . sqlesc($subject) . ", $row[userid], " . sqlesc(date("Y-m-d H:i:s")) . ", " . sqlesc($pn_msg) . ")") or sqlerr(__FILE__, __LINE__);
	}
	sql_query("DELETE FROM offers WHERE id = " . $offerid);
	sql_query("DELETE FROM offervotes WHERE offerid = " . $offerid);
	sql_query("DELETE FROM comments WHERE offer = " . $offerid);
	if ($CURUSER['class'] == UC_USER)
		sql_query("UPDATE users SET class=2 WHERE id = " . $CURUSER['id']);
}

if ($securetracker == 'yes' || $securetracker == 'op')
	$tracker_ssl = true;
elseif ($_COOKIE["c_secure_tracker_ssl"] == base64("yeah"))
	$tracker_ssl = true;
else
	$tracker_ssl = false;
if ($tracker_ssl == true) {
	$ssl_invite = "https://";
} else {
	$ssl_invite = "http://";
}

if ($emailnotify_smtp == 'yes' && $smtptype != 'none') {
	$cat = get_single_value("categories", "name", "WHERE id=" . sqlesc($catid));
	$res = sql_query("SELECT id, email, lang FROM users WHERE enabled='yes' AND parked='no' AND status='confirmed' AND notifs LIKE '%[cat$catid]%' AND notifs LIKE '%[email]%' ORDER BY lang ASC") or sqlerr(__FILE__, __LINE__);
	$uploader = $anon;
	$size = mksize($totallen);
	$description = format_comment($descr);
	$langfolder_array = array("en", "chs", "cht", "ko", "ja");
	$body_arr = array("en" => "", "chs" => "", "cht" => "", "ko" => "", "ja" => "");
	$i = 0;
	foreach ($body_arr as $body) {
		$body_arr[$langfolder_array[$i]] = <<<EOD
{$lang_takeupload_target[$langfolder_array[$i]]['mail_hi']}
{$lang_takeupload_target[$langfolder_array[$i]]['mail_new_torrent']}
{$lang_takeupload_target[$langfolder_array[$i]]['mail_torrent_name']}$torrent
{$lang_takeupload_target[$langfolder_array[$i]]['mail_torrent_size']}$size
{$lang_takeupload_target[$langfolder_array[$i]]['mail_torrent_category']}$cat
{$lang_takeupload_target[$langfolder_array[$i]]['mail_torrent_uppedby']}$uploader
{$lang_takeupload_target[$langfolder_array[$i]]['mail_torrent_description']}
-------------------------------------------------------------------------------------------------------------------------
$description
-------------------------------------------------------------------------------------------------------------------------

{$lang_takeupload_target[$langfolder_array[$i]]['mail_torrent']}<b><a href="$ssl_invite$BASEURL/details.php?id=$id&hit=1">{$lang_takeupload_target[$langfolder_array[$i]]['mail_here']}</a></b><br />

------{$lang_takeupload_target[$langfolder_array[$i]]['mail_yours']}
{$lang_takeupload_target[$langfolder_array[$i]]['mail_team']}
EOD;

		$body_arr[$langfolder_array[$i]] = str_replace("<br />", "<br />", nl2br($body_arr[$langfolder_array[$i]]));
		$i++;
	}

	while ($arr = mysql_fetch_array($res)) {
		$current_lang = $arr["lang"];
		$to = $arr["email"];
		sent_mail($to, $SITENAME, $SITEEMAIL, change_email_encode(validlang($current_lang), $lang_takeupload_target[validlang($current_lang)]['mail_title'] . $torrent), change_email_encode(validlang($current_lang), $body_arr[validlang($current_lang)]), "torrent upload", false, false, '', get_email_encode(validlang($current_lang)), "eYou");
	}
}
header("Location: " . get_protocol_prefix() . "$BASEURL/details.php?id=" . htmlspecialchars($id) . "&uploaded=1");
