<?php
require_once("include/bittorrent.php");
dbconn();
require_once(get_langfile_path());
loggedinorreturn();

$id = 0 + $_GET['id'];
if (!$id)
	die();

$res = sql_query("SELECT torrents.*, categories.mode AS cat_mode FROM torrents LEFT JOIN categories ON category = categories.id WHERE torrents.id = $id");
$row = mysql_fetch_array($res);
if (!$row)
	die();
if ($enablespecial == 'yes' && get_user_class() >= $movetorrent_class)
	$allowmove = true; //enable moving torrent to other section
else
	$allowmove = false;

$sectionmode = $row['cat_mode'];
if ($sectionmode == $browsecatmode) {
	$othermode = $specialcatmode;
	$movenote = $lang_edit['text_move_to_special'];
} else {
	$othermode = $browsecatmode;
	$movenote = $lang_edit['text_move_to_browse'];
}

$showsource = (get_searchbox_value($sectionmode, 'showsource') || ($allowmove && get_searchbox_value($othermode, 'showsource'))); //whether show sources or not
$showmedium = (get_searchbox_value($sectionmode, 'showmedium') || ($allowmove && get_searchbox_value($othermode, 'showmedium'))); //whether show media or not
$showcodec = (get_searchbox_value($sectionmode, 'showcodec') || ($allowmove && get_searchbox_value($othermode, 'showcodec'))); //whether show codecs or not
$showstandard = (get_searchbox_value($sectionmode, 'showstandard') || ($allowmove && get_searchbox_value($othermode, 'showstandard'))); //whether show standards or not
$showprocessing = (get_searchbox_value($sectionmode, 'showprocessing') || ($allowmove && get_searchbox_value($othermode, 'showprocessing'))); //whether show processings or not
$showteam = (get_searchbox_value($sectionmode, 'showteam') || ($allowmove && get_searchbox_value($othermode, 'showteam'))); //whether show teams or not
$showaudiocodec = (get_searchbox_value($sectionmode, 'showaudiocodec') || ($allowmove && get_searchbox_value($othermode, 'showaudiocodec'))); //whether show audio codecs or not

stdhead($lang_edit['head_edit_torrent'] . "\"" . $row["name"] . "\"");
if (!isset($CURUSER) || ($CURUSER["id"] != $row["owner"] && get_user_class() < $torrentmanage_class)) {
	print("<h1 align=\"center\">" . $lang_edit['text_cannot_edit_torrent'] . "</h1>");
	print("<p>" . $lang_edit['text_cannot_edit_torrent_note'] . "</p>");
} else {
	print("<form method=\"post\" id=\"compose\" name=\"edittorrent\" action=\"takeedit.php\" enctype=\"multipart/form-data\">");
	$_SESSION['edit'] = mt_rand(1000000, 9999999);
	print("<input type=hidden name=edit value='" . $_SESSION['edit'] . "' />");
	print("<input type=\"hidden\" name=\"id\" value=\"$id\" />");
	if (isset($_GET["returnto"]))
		print("<input type=\"hidden\" name=\"returnto\" value=\"" . htmlspecialchars($_GET["returnto"]) . "\" />");
	print("<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\" width=\"940\">\n");
	print("<tr><td class='colhead' colspan='2' align='center'>" . htmlspecialchars($row["name"]) . "</td></tr>");
	$torrent_id = isset($_GET["cite_torrent_id"]) ? 0 + $_GET["cite_torrent_id"] : "";
	tr("引用编辑", "<input type=\"text\" id=\"cite_torrent\" name=\"cite_torrent\" value=\"$torrent_id\" /><input type=\"button\" id=\"cite_torrent_btn\" value=\"引用\"/>" . "  填写种子ID", 1);
	tr($lang_edit['row_torrent_name'] . "<font color=\"red\">*</font>", "<input type=\"text\" style=\"width: 650px;\" id=\"name\" name=\"name\" value=\"" . htmlspecialchars($row["name"]) . "\" />" . (get_user_class() >= $torrentmanage_class ? " <input id=\"cxcolortitle\" name=\"cxcolortitle\" class=\"input_cxcolor\" type=\"text\" value=\"" . $row['namecolor'] . "\" style=\"background-color: rgb(0, 0, 0);\" />" : "") . "", 1);
	if ($smalldescription_main == 'yes')
		tr($lang_edit['row_small_description'], "<input type=\"text\" style=\"width: 650px;\" id=\"small_descr\" name=\"small_descr\" value=\"" . htmlspecialchars($row["small_descr"]) . "\" />" . (get_user_class() >= $torrentmanage_class ? " <input id=\"cxcolordesc\" name=\"cxcolordesc\" class=\"input_cxcolor\" type=\"text\" value=\"" . $row['descrcolor'] . "\" style=\"background-color: rgb(0, 0, 0);\" />" : "") . "", 1);

	get_external_tr($row["url"]);
	//get_dbexternal_tr($row["dburl"]);

	if ($enablenfo_main == 'yes')
		tr($lang_edit['row_nfo_file'], "<font class=\"medium\"><input type=\"radio\" name=\"nfoaction\" value=\"keep\" checked=\"checked\" />" . $lang_edit['radio_keep_current'] .
				"<input type=\"radio\" name=\"nfoaction\" value=\"remove\" />" . $lang_edit['radio_remove'] .
				"<input id=\"nfoupdate\" type=\"radio\" name=\"nfoaction\" value=\"update\" />" . $lang_edit['radio_update'] . "</font><br /><input type=\"file\" name=\"nfo\" onchange=\"document.getElementById('nfoupdate').checked=true\" />", 1);
	print("<tr><td class=\"rowhead\">" . $lang_edit['row_description'] . "<font color=\"red\">*</font></td><td class=\"rowfollow\">");
	//textbbcode("edittorrent", "descr", ($row["descr"]), false);
	print("<textarea id='sceditor' name='descr' onkeydown='ctrlenter(event,'compose','qr')' style='width:100%;height:400px;'>" . $row['descr'] . "</textarea>");
	print("</td></tr>");

	$s = "<select name=\"type\" id=\"oricat\" onchange=\"secondtype(this);\">";

	$cats = genrelist($sectionmode);
	foreach ($cats as $subrow) {
		$s .= "<option value=\"" . $subrow["id"] . "\"";
		if ($subrow["id"] == $row["category"])
			$s .= " selected=\"selected\"";
		$s .= ">" . htmlspecialchars($subrow["name"]) . "</option>\n";
	}

	$s .= "</select>\n";
	if ($allowmove) {
		$s2 = "<select name=\"type\" id=newcat disabled>\n";
		$cats2 = genrelist($othermode);
		foreach ($cats2 as $subrow) {
			$s2 .= "<option value=\"" . $subrow["id"] . "\"";
			if ($subrow["id"] == $row["category"])
				$s2 .= " selected=\"selected\"";
			$s2 .= ">" . htmlspecialchars($subrow["name"]) . "</option>\n";
		}
		$s2 .= "</select>\n";
		$movecheckbox = "<input type=\"checkbox\" id=movecheck name=\"movecheck\" value=\"1\" onclick=\"disableother2('oricat','newcat')\" />";
	}
	tr($lang_edit['row_type'] . "<font color=\"red\">*</font>", $s . ($allowmove ? "&nbsp;&nbsp;" . $movecheckbox . $movenote . $s2 : ""), 1);
	if ($showsource || $showmedium || $showcodec || $showaudiocodec || $showstandard || $showprocessing) {
		if ($showsource) {
			$source_select = torrent_selection($lang_edit['text_source'], "source_sel", "sources", $row["source"]);
		} else
			$source_select = "";

		if ($showmedium) {
			$medium_select = torrent_selection($lang_edit['text_medium'], "medium_sel", "media", $row["medium"]);
		} else
			$medium_select = "";

		if ($showcodec) {
			$codec_select = torrent_selection($lang_edit['text_codec'], "codec_sel", "codecs", $row["codec"]);
		} else
			$codec_select = "";

		if ($showaudiocodec) {
			$audiocodec_select = torrent_selection($lang_edit['text_audio_codec'], "audiocodec_sel", "audiocodecs", $row["audiocodec"]);
		} else
			$audiocodec_select = "";

		if ($showstandard) {
			$standard_select = torrent_selection($lang_edit['text_standard'], "standard_sel", "standards", $row["standard"]);
		} else
			$standard_select = "";

		if ($showprocessing) {
			$processing_select = torrent_selection($lang_edit['text_processing'], "processing_sel", "processings", $row["processing"]);
		} else
			$processing_select = "";

		$source_sel = mysql_fetch_array(sql_query("SELECT source FROM torrents WHERE id=$id")) or sqlerr(__FILE__, __LINE__);
		tr($lang_edit['row_quality'] . "<font color=red>*</font>", "<select id='source_sel' name='source_sel'></select>", 1);
	}

	tr($lang_edit['row_info'], $source_select . $medium_select . $codec_select . $audiocodec_select . $standard_select . $processing_select, 1);

	if ($showteam) {
		if ($showteam) {
			$team_select = torrent_selection($lang_edit['text_team'], "team_sel", "teams", $row["team"]);
		} else
			$showteam = "";

		tr($lang_edit['row_content'], $team_select, 1);
	}
//促销开始
	$pickcontent = "<b style=\"color:red\">请注意，“置顶”类设置项的时长不填写时默认为6小时；超级置顶等级1<2<3，建议使用1级超级置顶</b><br />";
	if (get_user_class() >= $torrentonpromotion_class) {
		$pickcontent .= "<b>" . $lang_edit['row_special_torrent'] . ":&nbsp;</b>" . "<select name=\"sel_spstate\" style=\"width: 100px;\">" . promotion_selection($row["sp_state"], 0) . "</select>&nbsp;&nbsp;&nbsp;<b>" . $lang_edit['row_special_torrent_time'] . ":&nbsp;</b><input type=\"text\" name=\"freetime\" size=3 maxlength=3 /><b>天(留空不修改，-1为永久)<input type=\"text\" name=\"freetimeh\" size=3 maxlength=2 />小时，当前至$row[endfree]</b><br />";
	}
	if (get_user_class() >= $torrentsticky_class) {
		$pickcontent .= "<b>" . $lang_edit['row_torrent_position'] . ":&nbsp;</b>" . "<select name=\"sel_posstate\" style=\"width: 100px;\">" .
				"<option" . (($row["pos_state"] == "normal") ? " selected=\"selected\"" : "" ) . " value=\"0\">" . $lang_edit['select_normal'] . "</option>" .
				"<option" . (($row["pos_state"] == "sticky") ? " selected=\"selected\"" : "" ) . " value=\"1\">" . $lang_edit['select_sticky'] . "</option>" .
				"</select>&nbsp;&nbsp;&nbsp;<b>" . $lang_edit['row_torrent_position_time'] . ":&nbsp;</b><input type=\"text\" name=\"stickytime\" size=3 maxlength=3 /><b>天(留空不修改，-1为永久)<input type=\"text\" name=\"stickytimeh\" size=3 maxlength=2 />小时，当前至$row[endsticky]</b><br />";
	}
	//超级置顶
	if (get_user_class() >= $torrentsticky_class) {
		$pickcontent .= "<b>" . $lang_edit['row_marrow'] . ":&nbsp;</b>" . "<select name=\"sel_marrow\" style=\"width: 100px;\">" .
				"<option" . (($row["marrow"] == "normal") ? " selected=\"selected\"" : "" ) . " value=\"0\">" . $lang_edit['select_normal'] . "</option>" .
				"<option" . (($row["marrow"] == "marrow1") ? " selected=\"selected\"" : "" ) . " value=\"1\">" . $lang_edit['select_marrow1'] . "</option>" .
				"<option" . (($row["marrow"] == "marrow2") ? " selected=\"selected\"" : "" ) . " value=\"2\">" . $lang_edit['select_marrow2'] . "</option>" .
				"<option" . (($row["marrow"] == "marrow3") ? " selected=\"selected\"" : "" ) . " value=\"3\">" . $lang_edit['select_marrow3'] . "</option>" .
				"</select>&nbsp;&nbsp;&nbsp;<b>" . $lang_edit['row_torrent_position_time'] . ":&nbsp;</b><input type=\"text\" name=\"marrowtime\" size=3 maxlength=3 /><b>天(留空不修改，-1为永久)<input type=\"text\" name=\"marrowtimeh\" size=3 maxlength=2 />小时，当前至$row[endmarrow]</b><br />";
	}
	//增加自定义H&R
	if ($hr == 'yes' && get_user_class() >= $torrentonpromotion_class) {
		$pickcontent .= "<b>" . $lang_edit['row_torrent_hr'] . ": </b>" . "<select name=\"hr_posstate\" style=\"width: 100px;\">" .
				"<option" . (($row["hr_state"] == "normal") ? " selected=\"selected\"" : "" ) . " value=\"0\">" . $lang_edit['hr_select_normal'] . "</option>" .
				"<option" . (($row["hr_state"] == "sticky") ? " selected=\"selected\"" : "" ) . " value=\"1\">" . $lang_edit['hr_select_sticky'] . "</option>" .
				"<option" . (($row["hr_state"] == "no") ? " selected=\"selected\"" : "" ) . " value=\"2\">" . $lang_edit['hr_select_no'] . "</option>" .
				"</select>   ";
		$pickcontent .= "&nbsp;&nbsp;<b>" . $lang_edit['text_hr_time'] . ": </b>" .
				"<input type=\"text\"  name=\"hrtime\" size=3 value=\"" . ($row["hr_time"] / 3600) . "\" /><b>" . $lang_edit['text_minutes'] . "，</b>";
		$pickcontent .= "<b>" . $lang_edit['text_hr_until'] . ": </b>" .
				"<input type=\"text\"  name=\"hruntil\" size=3 value=\"" . ($row["hr_until"] / 86400) . "\" /><b>" . $lang_edit['text_hours'] . "(留空不修改)</b><br />";
	}
	if (get_user_class() >= $torrentmanage_class) {
		$pickcontent .= "<b>" . $lang_edit['row_recommended_movie'] . ":&nbsp;</b>" . "<select name=\"sel_recmovie\" style=\"width: 100px;\">" .
				"<option" . (($row["picktype"] == "normal") ? " selected=\"selected\"" : "" ) . " value=\"0\">" . $lang_edit['select_normal'] . "</option>" .
				"<option" . (($row["picktype"] == "hot") ? " selected=\"selected\"" : "" ) . " value=\"1\">" . $lang_edit['select_hot'] . "</option>" .
				"<option" . (($row["picktype"] == "classic") ? " selected=\"selected\"" : "" ) . " value=\"2\">" . $lang_edit['select_classic'] . "</option>" .
				"<option" . (($row["picktype"] == "recommended") ? " selected=\"selected\"" : "" ) . " value=\"3\">" . $lang_edit['select_recommended'] . "</option>" .
				"</select>";
	}
	if (get_user_class() >= $torrentmanage_class || $CURUSER['id'] == $robotusers) {
		tr($lang_upload['row_pick'], $pickcontent, 1);
	}
//促销结束
	tr("<font style=\"color:red\">注意</font>", "<font style=\"color:red\">不要随意勾选“首发”和“禁转”，除非这是全网首发或是你的独有资源。</font>" . (get_user_class() >= UC_UPLOADER ? "<font style=\"color:blue\">“限定”资源只有被授权的用户才可以下载。</font>" : ""), 1);
	tr("首发/禁转", "<input type=\"checkbox\" name=\"first\"" . ($row["first"] == "yes" ? " checked=\"checked\"" : "" ) . " value=\"1\" />" . $lang_edit['checkbox_first'] . "<input type=\"checkbox\" name=\"excl\"" . ($row["excl"] == "yes" ? " checked=\"checked\"" : "" ) . " value=\"1\" />" . $lang_edit['checkbox_excl'] . (get_user_class() >= UC_UPLOADER ? "<input type=\"checkbox\" name=\"official\"" . ($row["official"] == "yes" ? " checked=\"checked\"" : "" ) . " value=\"1\" />官方" : "<input type=\"checkbox\" name=\"official\"" . ($row["official"] == "yes" ? " checked=\"checked\"" : "" ) . " value=\"1\" disabled=\"disabled\" />官方") . (get_user_class() >= UC_UPLOADER ? "<input type=\"checkbox\" name=\"limitd\"" . ($row["limitd"] == "yes" ? " checked=\"checked\"" : "" ) . " value=\"1\" />限定" : "<input type=\"checkbox\" name=\"limitd\"" . ($row["limitd"] == "yes" ? " checked=\"checked\"" : "" ) . " value=\"1\" disabled=\"disabled\" />限定"), 1);
	tr("定时发布", "预计发布时间: <input type='text' id=releasedate name=releasedate style=\"width: 120px\" class=\"Wdate\" onfocus=\"WdatePicker({doubleCalendar:true,isShowWeek:'true',minDate:'%y-%M-{%d+1}'})\" value='" . ($row['releasedate'] ? $row['releasedate'] : "") . "' /> 用以同时发布过多种子，导致上传带宽不足出种困难时使用", 1);
	tr($lang_edit['row_check'], "<input type=\"checkbox\" name=\"visible\"" . ($row["visible"] == "yes" ? " checked=\"checked\"" : "" ) . " value=\"1\" /> " . $lang_edit['checkbox_visible'] . "&nbsp;&nbsp;&nbsp;" . (get_user_class() >= $beanonymous_class || get_user_class() >= $torrentmanage_class ? "<input type=\"checkbox\" name=\"anonymous\"" . ($row["anonymous"] == "yes" ? " checked=\"checked\"" : "" ) . " value=\"1\" />" . $lang_edit['checkbox_anonymous_note'] . "&nbsp;&nbsp;&nbsp;" : "") . (get_user_class() >= $torrentmanage_class ? "<input type=\"checkbox\" name=\"banned\"" . (($row["banned"] == "yes") ? " checked=\"checked\"" : "" ) . " value=\"yes\" /> " . $lang_edit['checkbox_banned'] : ""), 1);
	print("<tr><td class=\"toolbox\" colspan=\"2\" align=\"center\">");
	if ($row["status"] != "normal" && get_user_class() >= $torrentmanage_class && $row['releasedate'] == '') {
		print("<input formaction=\"takeedit.php?release=yes\" type=\"submit\" value=\"编辑并发布\" /> ");
	}
	print("<input id=\"qr\" type=\"submit\" value=\"" . $lang_edit['submit_edit_it'] . "\" /> <input type=\"reset\" value=\"" . $lang_edit['submit_revert_changes'] . "\" /></td></tr>\n");
	print("</table>\n");
	print("</form>\n");
	print("<br /><br />");
	print("<form method=\"post\" action=\"delete.php\">\n");
	print("<input type=\"hidden\" name=\"id\" value=\"$id\" />\n");
	if (isset($_GET["returnto"])) {
		print("<input type=\"hidden\" name=\"returnto\" value=\"" . htmlspecialchars($_GET["returnto"]) . "\" />\n");
	}
	if (get_user_class() >= $torrentmanage_class || $row['status'] != 'normal') {//只有种子管理员及以上才有权利删除种子，或在回收站里的
		print("<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">\n");
		print("<tr><td class=\"colhead\" align=\"left\" style='padding-bottom: 3px' colspan=\"2\">" . $lang_edit['text_delete_torrent'] . "</td></tr>");
		tr("<input name=\"reasontype\" type=\"radio\" value=\"1\" />&nbsp;" . $lang_edit['radio_dead'], $lang_edit['text_dead_note'], 1);
		tr("<input name=\"reasontype\" type=\"radio\" value=\"2\" />&nbsp;" . $lang_edit['radio_dupe'], "<input type=\"text\" style=\"width: 200px\" name=\"reason[]\" />", 1);
		tr("<input name=\"reasontype\" type=\"radio\" value=\"3\" />&nbsp;" . $lang_edit['radio_nuked'], "<input type=\"text\" style=\"width: 200px\" name=\"reason[]\" />", 1);
		tr("<input name=\"reasontype\" type=\"radio\" value=\"4\" />&nbsp;" . $lang_edit['radio_rules'], "<input type=\"text\" style=\"width: 200px\" name=\"reason[]\" />" . $lang_edit['text_req'], 1);
		tr("<input name=\"reasontype\" type=\"radio\" value=\"5\" checked=\"checked\" />&nbsp;" . $lang_edit['radio_other'], "<input type=\"text\" style=\"width: 200px\" name=\"reason[]\" />" . $lang_edit['text_req'], 1);
		print("<tr><td class=\"toolbox\" colspan=\"2\" align=\"center\">");
		print("<input type=\"submit\" formaction=\"delete.php?recycle_mode=delete\" value=\"彻底删除\"> ");
		print("<input type=\"submit\" formaction=\"delete.php?recycle_mode=recycle\" value=\"移入回收站\">");
	}
	print("</td></tr>\n</table>");
	?>
	<script type="text/javascript">
		$(document).ready(function () {
			//var oricat = $("#source_sel").val();
			secondtype(document.getElementById("oricat"));
	<?php print("$(\"#source_sel\").val(" . $source_sel['source'] . ");"); ?>
		});
		function uplist(name, list) {
			var childRet = document.getElementById(name);
			for (var i = childRet.childNodes.length - 1; i >= 0; i--) {
				childRet.removeChild(childRet.childNodes.item(i));
			}
			for (var j = 0; j < list.length; j++) {
				var ret = document.createDocumentFragment();
				var newop = document.createElement("option");
				newop.id = list[j][0];
				newop.value = list[j][0];
				newop.appendChild(document.createTextNode(list[j][1]));
				ret.appendChild(newop);
				document.getElementById(name).appendChild(ret);
			}
		}

		//引用发布功能开始
		if ($("#cite_torrent").val() != "") {
			citeTorrent();
		}
		$("#cite_torrent_btn").click(function () {
			citeTorrent();
		});
		function citeTorrent() {
			var id = $("#cite_torrent").val();
			//需要判断ID是否合法
			if (id != '') {
				if ($("#cite_hint").length > 0) {
					$("#cite_hint").remove();
				}
				$.getJSON("citetorrent.php?torrent_id=" + id, function (data) {
					if (data["exist"] == "yes") {
						$("#oricat").val(data["category"]);
						secondtype(document.getElementById("oricat"));
						$("#source_sel").val(data["source"]);
						//$("#name").val(data["name"]);
						$("input[name=small_descr]").val(data["small_descr"]);
						$("input[name=imdburl]").val(data["url"]);
						$("input[name=dburl]").val(data["dburl"]);
						//$("#descr").text(data["descr"]);//原始编辑器
						$('#sceditor').sceditor('instance').val("");//引用种子时先清空编辑器
						$('#sceditor').sceditor('instance').insertText(data["descr"]);
					} else {
						$("#cite_torrent_btn").after("<span id=\"cite_hint\">所引用的种子不存在...<span>");
					}
				});
			}
		}
		//引用发布功能结束

		function secondtype(value) {
	<?php
	$catss = genrelist($browsecatmode);
	foreach ($catss as $row) {
		$catsid = $row['id'];
		$secondtype = searchbox_item_list("sources", $catsid);
		$secondsize = count($secondtype, 0);
		print("var lid" . $catsid . " = new Array(");
		for ($i = 0; $i < $secondsize; $i++) {
			print("['" . $secondtype[$i]['id'] . "','" . $secondtype[$i]['name'] . "']");
			if ($i < $secondsize - 1)
				print(",");
		}
		print(");\n");
	}
	print("switch(value.value){\n");
	foreach ($catss as $row) {
		$catsid = $row['id'];
		print("\tcase \"" . $catsid . "\": ");
		print("uplist(\"source_sel\",lid" . $catsid . ");");
		print("break;\n");
	}
	print("}\n");
	?>
		}
	</script>
	<?php
	print("</form>\n");
}
stdfoot();
