<?php require_once("include/bittorrent.php"); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>上传图片 - 贴图库</title>
		<script type="text/javascript" src="jquerylib/tietuku/tietuku.jquery.min.js"></script>
		<script type="text/javascript" src="jquerylib/tietuku/tietuku.dialog.js"></script>
		<link rel="stylesheet" href="jquerylib/tietuku/style.css">
		<style>#jqContextMenu li{text-align:center;}</style>
		<script>
			var token = '<?= $ttktoken ?>';
			var t2 = '';
			var successpid = '';
			var gqtime = '';
			$(function () {
				if (gqtime) {
					$.dialog.showFuncLayer(430, "", "您的 token 过期了，请到 http://open.tietuku.cn/createtoken 重新获取！", function () {
						top.location.href = 'http://open.tietuku.cn/createtoken';
					});
				}
				$('.left_return').on('dblclick', 'input', function () {
					btncopy(this.value);
				});
				$('.uploadBtn').contextMenu('up_btn', {bindings: {'upbtn': function (t) {
							$('.state-ready').click();
						}
					}
				});
			})
		</script>
	</head>
	<body>
		<div class="left">
			<div class="upinfo">支持格式：jpeg/jpg/gif/png</div>
		</div>
		<div class="left_return">
			<textarea id="url" style="display: none" onmouseover='$(this).select();' wrap="off"></textarea>
		</div>
		<div class="left_tips" id="left_tips"><center>温馨提示</center>
			<ul>
				<li>图片格式支持JPG、JPEG、GIF、PNG;</li>
				<li>一次可添加上传300张图片;</li>
				<li>单张图片不可超过10M;</li>
				<li><b style="color:red">严禁发布色情、淫秽、反动、诋毁、造谣的文字和图片。</b></li>
			</ul>
		</div>
		<div class="right">
			<div class="upload_c_b">
				<div id="uploadcontainer">
					<div id="uploader">
						<div class="queueList">
							<div id="dndArea" class="placeholder">
								<div id="filePicker"></div>
								<p style="margin-top:30px;">或将照片拖到这里，单次最多可选300张</p>
								<p style="margin-top:10px;display:none"><button id="img_url_btn" type="submit" class="btn btn-default">Submit</button></p>
							</div>
						</div>
						<div class="statusBar" style="display:none;">
							<div class="progress">
								<span class="text">0%</span>
								<span class="percentage"></span>
							</div><div class="info"></div>
							<div class="btns">
								<div id="filePicker2"></div><div class="uploadBtn">开始上传</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class='show'><img src='#'/></div>
		<script>
			function showpic(obj) {
				$('.imgWrap').css('z-index', '-1');
				$('.success').css('z-index', '-1');
				$('.progress').css('z-index', '-1');
				$('.show').show();
				$('.show img').attr('src', $(obj).find('#turl').val());
			}
			function hidepic() {
				$('.imgWrap').css('z-index', '2');
				$('.success').css('z-index', '200');
				$('.progress').css('z-index', '50');
				$('.show').hide();
			}
		</script>
		<script type="text/javascript" src="jquerylib/tietuku/webuploader.min.js"></script>
		<script type="text/javascript" src="jquerylib/tietuku/upload.js"></script>
		<div class="contextMenu" id="up_btn">
			<ul>
				<li id="upbtn">开始上传</li>
			</ul>
		</div>
	</body>
</html>