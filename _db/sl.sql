-- phpMyAdmin SQL Dump
-- version 4.4.15
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2016-10-21 15:22:36
-- 服务器版本： 10.1.8-MariaDB
-- PHP Version: 5.3.29-upupw

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sl`
--
CREATE DATABASE IF NOT EXISTS `sl` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `sl`;

-- --------------------------------------------------------

--
-- 表的结构 `2048`
--

DROP TABLE IF EXISTS `2048`;
CREATE TABLE IF NOT EXISTS `2048` (
  `id` int(11) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `username` varchar(20) NOT NULL,
  `score` int(10) unsigned NOT NULL,
  `times` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `2048_rank`
--

DROP TABLE IF EXISTS `2048_rank`;
CREATE TABLE IF NOT EXISTS `2048_rank` (
  `id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `times` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `adclicks`
--

DROP TABLE IF EXISTS `adclicks`;
CREATE TABLE IF NOT EXISTS `adclicks` (
  `id` int(11) unsigned NOT NULL,
  `adid` int(11) unsigned DEFAULT NULL,
  `userid` int(11) unsigned DEFAULT NULL,
  `added` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `adminpanel`
--

DROP TABLE IF EXISTS `adminpanel`;
CREATE TABLE IF NOT EXISTS `adminpanel` (
  `id` tinyint(3) unsigned NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `info` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `adminpanel`
--

INSERT INTO `adminpanel` (`id`, `name`, `url`, `info`) VALUES
(1, '被警告用户', 'warned.php', '查看被Tracker警告的用户'),
(2, '添加新用户', 'adduser.php', '添加一个新用户'),
(3, '重置用户密码', 'reset.php', '当用户密码忘记时给用户重置密码'),
(4, '全站发站内短信', 'staffmess.php', '给全站用户发送站内短信(PM)'),
(5, '全站发电子邮件', 'massmail.php', '给全站用户发送电子邮件(E-Mail)'),
(6, '查看投票', 'polloverview.php', '查看投票统计信息'),
(7, '全站促销', 'freeleech.php', '设定全站种子为指定促销状态'),
(8, '广告管理', 'admanage.php', '添加站点广告'),
(9, '论坛管理', 'forummanage.php', '编辑/删除论坛版块'),
(10, '答疑管理', 'faqmanage.php', '编辑答疑页面'),
(11, '规则管理', 'modrules.php', '编辑规则页面'),
(12, '分类管理', 'catmanage.php', '管理种子分类'),
(13, '删除无用附件和字幕文件', 'delattachments.php', '删除不被使用的附件和字幕文件'),
(14, '删除搜索关键字', 'delkeywords.php', '删除用户搜索的关键字记录'),
(15, '删除被禁止用户', 'deletedisabled.php', '删除所有被禁止用户'),
(16, '禁止注册邮件域', 'bannedemails.php', '限制指定后缀的邮箱不可注册'),
(17, '设定允许邮件域', 'allowedemails.php', '允许注册的邮件域范围'),
(18, '更新Tracker状态', 'docleanup.php', '更新Tracker状态'),
(19, '清理所有服务器缓存', 'flushmem.php', '清理所有memcached缓存'),
(20, '清理指定服务器缓存', 'clearcache.php', '清理指定memcached缓存'),
(21, '查看服务器缓存', 'mem.php', '查看memcached缓存'),
(22, '数据库状态', 'mysql_stats.php', '查看数据库状态'),
(23, 'IP范围限速', 'location.php', '管理指定IP段的下载上传速度'),
(24, 'IP禁止管理', 'bans.php', '设定IP禁止访问站点'),
(25, '增加上传量', 'amountupload.php', '给指定用户组增加上传量'),
(26, '添加魔力值', 'amountbonus.php', '为指定用户或组添加魔力值'),
(27, '管理员考核', 'moderators.php', '查看管理员考核状态');

-- --------------------------------------------------------

--
-- 表的结构 `advertisements`
--

DROP TABLE IF EXISTS `advertisements`;
CREATE TABLE IF NOT EXISTS `advertisements` (
  `id` mediumint(8) unsigned NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `type` enum('bbcodes','xhtml','text','image','flash') NOT NULL,
  `position` enum('header','footer','belownav','belowsearchbox','torrentdetail','comment','interoverforums','forumpost','popup','shoutindex','shoutlogin') NOT NULL,
  `displayorder` tinyint(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `parameters` text NOT NULL,
  `code` text NOT NULL,
  `starttime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `agent_allowed_exception`
--

DROP TABLE IF EXISTS `agent_allowed_exception`;
CREATE TABLE IF NOT EXISTS `agent_allowed_exception` (
  `family_id` tinyint(3) unsigned NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `peer_id` varchar(20) NOT NULL,
  `agent` varchar(100) NOT NULL,
  `comment` varchar(200) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `agent_allowed_family`
--

DROP TABLE IF EXISTS `agent_allowed_family`;
CREATE TABLE IF NOT EXISTS `agent_allowed_family` (
  `id` tinyint(3) unsigned NOT NULL,
  `family` varchar(50) NOT NULL DEFAULT '',
  `start_name` varchar(100) NOT NULL DEFAULT '',
  `peer_id_pattern` varchar(200) NOT NULL,
  `peer_id_match_num` tinyint(3) unsigned NOT NULL,
  `peer_id_matchtype` enum('dec','hex') NOT NULL DEFAULT 'dec',
  `peer_id_start` varchar(20) NOT NULL,
  `agent_pattern` varchar(200) NOT NULL,
  `agent_match_num` tinyint(3) unsigned NOT NULL,
  `agent_matchtype` enum('dec','hex') NOT NULL DEFAULT 'dec',
  `agent_start` varchar(100) NOT NULL,
  `exception` enum('yes','no') NOT NULL DEFAULT 'no',
  `allowhttps` enum('yes','no') NOT NULL DEFAULT 'no',
  `comment` varchar(200) NOT NULL DEFAULT '',
  `hits` mediumint(8) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `agent_allowed_family`
--

INSERT INTO `agent_allowed_family` (`id`, `family`, `start_name`, `peer_id_pattern`, `peer_id_match_num`, `peer_id_matchtype`, `peer_id_start`, `agent_pattern`, `agent_match_num`, `agent_matchtype`, `agent_start`, `exception`, `allowhttps`, `comment`, `hits`) VALUES
(1, 'Transmission', 'Transmission', '/^-TR([0-9])+-/', 8, 'dec', '-TR0000-', '/^Transmission/', 12, 'dec', 'Transmission', 'yes', 'yes', '', 0),
(2, 'Azureus', 'Azureus/Vuze', '/^-AZ([0-9])+-/', 8, 'dec', '-AZ0000-', '/^Azureus/', 7, 'dec', 'Azureus', 'yes', 'yes', '', 0),
(3, 'Deluge', 'Deluge', '/^-DE([0-9A-Z])+-/', 8, 'dec', '-DE0000-', '/^Deluge/', 6, 'dec', 'Deluge', 'yes', 'yes', '', 0),
(4, 'uTorrentMac', 'uTorrentMac', '/^-UM([0-9])+-/', 8, 'dec', '-UM0000-', '/^uTorrentMac/', 1, 'dec', 'uTorrentMac', 'yes', 'yes', '', 0),
(5, 'uTorrent', 'uTorrent', '/^-UT([0-9])+-/', 8, 'dec', '-UT0000-', '/^uTorrent/', 8, 'dec', 'uTorrent', 'yes', 'yes', '', 0),
(6, 'rTorrent', 'rTorrent', '/^-lt([0-9A-Z])+-/', 8, 'dec', '-lt0000-', '/^rtorrent/', 8, 'dec', 'rtorrent', 'yes', 'yes', '', 0),
(7, 'qBittorrent', 'qBittorrent', '/^-qB([0-9A-Z])+-/', 8, 'dec', '-qB0000-', '/^qBittorrent/', 11, 'dec', 'qBittorrent', 'yes', 'yes', '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `album`
--

DROP TABLE IF EXISTS `album`;
CREATE TABLE IF NOT EXISTS `album` (
  `id` int(10) unsigned NOT NULL,
  `userid` int(10) unsigned NOT NULL,
  `name` varchar(80) NOT NULL COMMENT '专辑名称',
  `small_descr` mediumtext NOT NULL COMMENT '专辑描述',
  `visited` enum('yes','no') NOT NULL DEFAULT 'yes',
  `added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `albumfav`
--

DROP TABLE IF EXISTS `albumfav`;
CREATE TABLE IF NOT EXISTS `albumfav` (
  `cid` int(10) unsigned NOT NULL,
  `id` int(10) unsigned NOT NULL COMMENT '专辑ID',
  `userid` int(10) unsigned NOT NULL COMMENT '用户ID',
  `name` varchar(80) NOT NULL,
  `small_descr` mediumtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `albumseries`
--

DROP TABLE IF EXISTS `albumseries`;
CREATE TABLE IF NOT EXISTS `albumseries` (
  `id` int(10) unsigned NOT NULL,
  `albumid` int(10) unsigned NOT NULL,
  `torrentid` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `allowedemails`
--

DROP TABLE IF EXISTS `allowedemails`;
CREATE TABLE IF NOT EXISTS `allowedemails` (
  `id` int(10) NOT NULL,
  `value` mediumtext NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `attachments`
--

DROP TABLE IF EXISTS `attachments`;
CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(10) unsigned NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `width` smallint(6) unsigned NOT NULL DEFAULT '0',
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `filename` varchar(255) NOT NULL DEFAULT '',
  `dlkey` char(32) NOT NULL,
  `filetype` varchar(50) NOT NULL DEFAULT '',
  `filesize` bigint(20) unsigned NOT NULL DEFAULT '0',
  `location` varchar(255) NOT NULL,
  `downloads` mediumint(8) NOT NULL DEFAULT '0',
  `isimage` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `thumb` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `inuse` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `audiocodecs`
--

DROP TABLE IF EXISTS `audiocodecs`;
CREATE TABLE IF NOT EXISTS `audiocodecs` (
  `id` tinyint(3) unsigned NOT NULL,
  `lid` tinyint(3) NOT NULL,
  `name` varchar(30) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_index` tinyint(3) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `audiocodecs`
--

INSERT INTO `audiocodecs` (`id`, `lid`, `name`, `image`, `sort_index`) VALUES
(1, 0, 'FLAC', '', 0),
(2, 0, 'APE', '', 0),
(3, 0, 'DTS', '', 0),
(4, 0, 'MP3', '', 0),
(5, 0, 'OGG', '', 0),
(6, 0, 'AAC', '', 0),
(7, 0, 'Other', '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `avps`
--

DROP TABLE IF EXISTS `avps`;
CREATE TABLE IF NOT EXISTS `avps` (
  `id` tinyint(3) unsigned NOT NULL,
  `arg` varchar(20) NOT NULL DEFAULT '',
  `value_s` text NOT NULL,
  `value_i` int(11) NOT NULL DEFAULT '0',
  `value_u` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `avps`
--

INSERT INTO `avps` (`id`, `arg`, `value_s`, `value_i`, `value_u`) VALUES
(1, 'lastcleantime1', '', 900, 1475726401),
(2, 'lastcleantime2', '', 1800, 1475726401),
(3, 'lastcleantime3', '', 3600, 1475726401),
(4, 'lastcleantime4', '', 43200, 1475726401),
(5, 'lastcleantime5', '', 648000, 1475683201);

-- --------------------------------------------------------

--
-- 表的结构 `bannedemails`
--

DROP TABLE IF EXISTS `bannedemails`;
CREATE TABLE IF NOT EXISTS `bannedemails` (
  `id` int(10) NOT NULL,
  `value` mediumtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `bans`
--

DROP TABLE IF EXISTS `bans`;
CREATE TABLE IF NOT EXISTS `bans` (
  `id` smallint(5) unsigned NOT NULL,
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `addedby` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `comment` varchar(255) NOT NULL DEFAULT '',
  `first` bigint(20) NOT NULL,
  `last` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `betgames`
--

DROP TABLE IF EXISTS `betgames`;
CREATE TABLE IF NOT EXISTS `betgames` (
  `id` int(11) NOT NULL,
  `heading` varchar(50) NOT NULL DEFAULT '',
  `undertext` varchar(150) NOT NULL DEFAULT '',
  `endtime` int(11) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `sort` int(1) NOT NULL DEFAULT '0',
  `creator` int(10) NOT NULL,
  `fix` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `betlog`
--

DROP TABLE IF EXISTS `betlog`;
CREATE TABLE IF NOT EXISTS `betlog` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL,
  `bonus` int(11) NOT NULL DEFAULT '0',
  `msg` varchar(150) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `betoptions`
--

DROP TABLE IF EXISTS `betoptions`;
CREATE TABLE IF NOT EXISTS `betoptions` (
  `id` int(11) NOT NULL,
  `gameid` int(11) NOT NULL DEFAULT '0',
  `text` varchar(100) NOT NULL DEFAULT '',
  `odds` double NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `bets`
--

DROP TABLE IF EXISTS `bets`;
CREATE TABLE IF NOT EXISTS `bets` (
  `id` int(11) NOT NULL,
  `gameid` int(11) NOT NULL DEFAULT '0',
  `bonus` int(11) NOT NULL DEFAULT '0',
  `optionid` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `date` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `bettop`
--

DROP TABLE IF EXISTS `bettop`;
CREATE TABLE IF NOT EXISTS `bettop` (
  `userid` int(11) NOT NULL DEFAULT '0',
  `bonus` int(11) NOT NULL DEFAULT '0',
  `winbonus` int(11) NOT NULL DEFAULT '0',
  `lossbonus` int(11) NOT NULL DEFAULT '0',
  `winnum` int(11) NOT NULL DEFAULT '0',
  `lossnum` int(11) NOT NULL COMMENT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `bitbucket`
--

DROP TABLE IF EXISTS `bitbucket`;
CREATE TABLE IF NOT EXISTS `bitbucket` (
  `id` int(10) unsigned NOT NULL,
  `owner` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `public` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `blackjack`
--

DROP TABLE IF EXISTS `blackjack`;
CREATE TABLE IF NOT EXISTS `blackjack` (
  `userid` int(11) NOT NULL DEFAULT '0',
  `points` int(11) NOT NULL DEFAULT '0',
  `status` enum('playing','waiting') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'playing',
  `cards` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `date` int(11) DEFAULT '0',
  `gameover` enum('yes','no') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `blocks`
--

DROP TABLE IF EXISTS `blocks`;
CREATE TABLE IF NOT EXISTS `blocks` (
  `id` int(10) unsigned NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `blockid` mediumint(8) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `blue`
--

DROP TABLE IF EXISTS `blue`;
CREATE TABLE IF NOT EXISTS `blue` (
  `id` int(10) unsigned NOT NULL,
  `torrentid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `bonusapp`
--

DROP TABLE IF EXISTS `bonusapp`;
CREATE TABLE IF NOT EXISTS `bonusapp` (
  `userid` int(10) NOT NULL,
  `namecharge` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `bonusapp`
--

INSERT INTO `bonusapp` (`userid`, `namecharge`) VALUES
(1, 40000);

-- --------------------------------------------------------

--
-- 表的结构 `bookmarks`
--

DROP TABLE IF EXISTS `bookmarks`;
CREATE TABLE IF NOT EXISTS `bookmarks` (
  `id` int(10) unsigned NOT NULL,
  `torrentid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `cards`
--

DROP TABLE IF EXISTS `cards`;
CREATE TABLE IF NOT EXISTS `cards` (
  `id` int(11) NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0',
  `pic` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `cards`
--

INSERT INTO `cards` (`id`, `points`, `pic`) VALUES
(1, 2, '2p.bmp'),
(2, 3, '3p.bmp'),
(3, 4, '4p.bmp'),
(4, 5, '5p.bmp'),
(5, 6, '6p.bmp'),
(6, 7, '7p.bmp'),
(7, 8, '8p.bmp'),
(8, 9, '9p.bmp'),
(9, 10, '10p.bmp'),
(10, 10, 'vp.bmp'),
(11, 10, 'dp.bmp'),
(12, 10, 'kp.bmp'),
(13, 1, 'tp.bmp'),
(14, 2, '2b.bmp'),
(15, 3, '3b.bmp'),
(16, 4, '4b.bmp'),
(17, 5, '5b.bmp'),
(18, 6, '6b.bmp'),
(19, 7, '7b.bmp'),
(20, 8, '8b.bmp'),
(21, 9, '9b.bmp'),
(22, 10, '10b.bmp'),
(23, 10, 'vb.bmp'),
(24, 10, 'db.bmp'),
(25, 10, 'kb.bmp'),
(26, 1, 'tb.bmp'),
(27, 2, '2k.bmp'),
(28, 3, '3k.bmp'),
(29, 4, '4k.bmp'),
(30, 5, '5k.bmp'),
(31, 6, '6k.bmp'),
(32, 7, '7k.bmp'),
(33, 8, '8k.bmp'),
(34, 9, '9k.bmp'),
(35, 10, '10k.bmp'),
(36, 10, 'vk.bmp'),
(37, 10, 'dk.bmp'),
(38, 10, 'kk.bmp'),
(39, 1, 'tk.bmp'),
(40, 2, '2c.bmp'),
(41, 3, '3c.bmp'),
(42, 4, '4c.bmp'),
(43, 5, '5c.bmp'),
(44, 6, '6c.bmp'),
(45, 7, '7c.bmp'),
(46, 8, '8c.bmp'),
(47, 9, '9c.bmp'),
(48, 10, '10c.bmp'),
(49, 10, 'vc.bmp'),
(50, 10, 'dc.bmp'),
(51, 10, 'kc.bmp'),
(52, 1, 'tc.bmp');

-- --------------------------------------------------------

--
-- 表的结构 `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` smallint(5) unsigned NOT NULL,
  `mode` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `class_name` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(30) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_index` smallint(5) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=425 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `categories`
--

INSERT INTO `categories` (`id`, `mode`, `class_name`, `name`, `image`, `sort_index`) VALUES
(401, 1, 'c_movies', '电影', 'movies_dvdr.png', 0),
(402, 1, 'c_tvseries', '剧集', 'tvseries_xvid.png', 1),
(403, 1, 'c_tvshows', '综艺', 'tvshows.png', 2),
(404, 1, 'c_doc', '纪录片', 'doc.png', 8),
(405, 1, 'c_anime', '动漫', 'anime.png', 3),
(406, 1, 'c_mv', 'MV', 'musicvideos.png', 9),
(407, 1, 'c_sports', '体育', 'sports.png', 6),
(408, 1, 'c_hqaudio', '软件', 'appz.png', 9),
(409, 1, 'c_misc', '其他', 'misc.png', 19),
(410, 1, 'c_game', '游戏', 'games_pc.png', 10),
(411, 1, 'c_doucument', '学习', 'study.png', 16),
(423, 1, '', '原创', 'movies_bd.png', 17),
(414, 1, '', '音乐', 'music.png', 4),
(424, 1, '', '限制', 'limit.png', 20);

-- --------------------------------------------------------

--
-- 表的结构 `caticons`
--

DROP TABLE IF EXISTS `caticons`;
CREATE TABLE IF NOT EXISTS `caticons` (
  `id` tinyint(3) unsigned NOT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `folder` varchar(255) NOT NULL,
  `cssfile` varchar(255) NOT NULL DEFAULT '',
  `multilang` enum('yes','no') NOT NULL DEFAULT 'no',
  `secondicon` enum('yes','no') NOT NULL DEFAULT 'no',
  `designer` varchar(50) NOT NULL DEFAULT '',
  `comment` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `caticons`
--

INSERT INTO `caticons` (`id`, `name`, `folder`, `cssfile`, `multilang`, `secondicon`, `designer`, `comment`) VALUES
(1, '标准', 'scenetorrents/', '', 'yes', 'no', 'HDCN', 'HDCN 设计');

-- --------------------------------------------------------

--
-- 表的结构 `cheaters`
--

DROP TABLE IF EXISTS `cheaters`;
CREATE TABLE IF NOT EXISTS `cheaters` (
  `id` mediumint(8) unsigned NOT NULL,
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `torrentid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `uploaded` bigint(20) unsigned NOT NULL DEFAULT '0',
  `downloaded` bigint(20) unsigned NOT NULL DEFAULT '0',
  `anctime` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `seeders` mediumint(5) unsigned NOT NULL DEFAULT '0',
  `leechers` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `hit` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `dealtby` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `dealtwith` tinyint(1) NOT NULL DEFAULT '0',
  `comment` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `chronicle`
--

DROP TABLE IF EXISTS `chronicle`;
CREATE TABLE IF NOT EXISTS `chronicle` (
  `id` mediumint(8) unsigned NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `txt` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `claim`
--

DROP TABLE IF EXISTS `claim`;
CREATE TABLE IF NOT EXISTS `claim` (
  `id` bigint(20) unsigned NOT NULL,
  `userid` bigint(20) unsigned NOT NULL,
  `torrentid` bigint(20) unsigned NOT NULL,
  `seedtime` bigint(20) unsigned NOT NULL,
  `uploaded` bigint(20) unsigned NOT NULL,
  `added` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `codecs`
--

DROP TABLE IF EXISTS `codecs`;
CREATE TABLE IF NOT EXISTS `codecs` (
  `id` tinyint(3) unsigned NOT NULL,
  `lid` tinyint(3) NOT NULL,
  `name` varchar(30) NOT NULL,
  `sort_index` tinyint(3) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `codecs`
--

INSERT INTO `codecs` (`id`, `lid`, `name`, `sort_index`) VALUES
(1, 0, 'H.264', 0),
(2, 0, 'VC-1', 0),
(3, 0, 'Xvid', 0),
(4, 0, 'MPEG-2', 0),
(5, 0, 'Other', 0);

-- --------------------------------------------------------

--
-- 表的结构 `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL,
  `user` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `torrent` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `text` text,
  `ori_text` text,
  `editedby` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `editdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `offer` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `request` mediumint(9) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `flagpic` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `countries`
--

INSERT INTO `countries` (`id`, `name`, `flagpic`) VALUES
(1, '中国', 'china.gif'),
(2, 'United States of America', 'usa.gif'),
(3, 'Russia', 'russia.gif'),
(4, 'Finland', 'finland.gif'),
(5, 'Canada', 'canada.gif'),
(6, 'France', 'france.gif'),
(7, 'Germany', 'germany.gif'),
(8, 'Sweden', 'sweden.gif'),
(9, 'Italy', 'italy.gif'),
(10, 'Denmark', 'denmark.gif'),
(11, 'Norway', 'norway.gif'),
(12, 'United Kingdom', 'uk.gif'),
(13, 'Ireland', 'ireland.gif'),
(14, 'Poland', 'poland.gif'),
(15, 'Netherlands', 'netherlands.gif'),
(16, 'Belgium', 'belgium.gif'),
(17, 'Japan', 'japan.gif'),
(18, 'Brazil', 'brazil.gif'),
(19, 'Argentina', 'argentina.gif'),
(20, 'Australia', 'australia.gif'),
(21, 'New Zealand', 'newzealand.gif'),
(23, 'Spain', 'spain.gif'),
(24, 'Portugal', 'portugal.gif'),
(25, 'Mexico', 'mexico.gif'),
(26, 'Singapore', 'singapore.gif'),
(70, 'India', 'india.gif'),
(65, 'Albania', 'albania.gif'),
(29, 'South Africa', 'southafrica.gif'),
(30, 'South Korea', 'southkorea.gif'),
(31, 'Jamaica', 'jamaica.gif'),
(32, 'Luxembourg', 'luxembourg.gif'),
(34, 'Belize', 'belize.gif'),
(35, 'Algeria', 'algeria.gif'),
(36, 'Angola', 'angola.gif'),
(37, 'Austria', 'austria.gif'),
(38, 'Yugoslavia', 'yugoslavia.gif'),
(39, 'Western Samoa', 'westernsamoa.gif'),
(40, 'Malaysia', 'malaysia.gif'),
(41, 'Dominican Republic', 'dominicanrep.gif'),
(42, 'Greece', 'greece.gif'),
(43, 'Guatemala', 'guatemala.gif'),
(44, 'Israel', 'israel.gif'),
(45, 'Pakistan', 'pakistan.gif'),
(46, 'Czech Republic', 'czechrep.gif'),
(47, 'Serbia', 'serbia.gif'),
(48, 'Seychelles', 'seychelles.gif'),
(50, 'Puerto Rico', 'puertorico.gif'),
(51, 'Chile', 'chile.gif'),
(52, 'Cuba', 'cuba.gif'),
(53, 'Congo', 'congo.gif'),
(54, 'Afghanistan', 'afghanistan.gif'),
(55, 'Turkey', 'turkey.gif'),
(56, 'Uzbekistan', 'uzbekistan.gif'),
(57, 'Switzerland', 'switzerland.gif'),
(58, 'Kiribati', 'kiribati.gif'),
(59, 'Philippines', 'philippines.gif'),
(60, 'Burkina Faso', 'burkinafaso.gif'),
(61, 'Nigeria', 'nigeria.gif'),
(62, 'Iceland', 'iceland.gif'),
(63, 'Nauru', 'nauru.gif'),
(64, 'Slovenia', 'slovenia.gif'),
(66, 'Turkmenistan', 'turkmenistan.gif'),
(67, 'Bosnia Herzegovina', 'bosniaherzegovina.gif'),
(68, 'Andorra', 'andorra.gif'),
(69, 'Lithuania', 'lithuania.gif'),
(71, 'Netherlands Antilles', 'nethantilles.gif'),
(72, 'Ukraine', 'ukraine.gif'),
(73, 'Venezuela', 'venezuela.gif'),
(74, 'Hungary', 'hungary.gif'),
(75, 'Romania', 'romania.gif'),
(76, 'Vanuatu', 'vanuatu.gif'),
(77, 'Vietnam', 'vietnam.gif'),
(78, 'Trinidad & Tobago', 'trinidadandtobago.gif'),
(79, 'Honduras', 'honduras.gif'),
(80, 'Kyrgyzstan', 'kyrgyzstan.gif'),
(81, 'Ecuador', 'ecuador.gif'),
(82, 'Bahamas', 'bahamas.gif'),
(83, 'Peru', 'peru.gif'),
(84, 'Cambodia', 'cambodia.gif'),
(85, 'Barbados', 'barbados.gif'),
(86, 'Bangladesh', 'bangladesh.gif'),
(87, 'Laos', 'laos.gif'),
(88, 'Uruguay', 'uruguay.gif'),
(89, 'Antigua Barbuda', 'antiguabarbuda.gif'),
(90, 'Paraguay', 'paraguay.gif'),
(93, 'Thailand', 'thailand.gif'),
(92, 'Union of Soviet Socialist Republics', 'ussr.gif'),
(94, 'Senegal', 'senegal.gif'),
(95, 'Togo', 'togo.gif'),
(96, 'North Korea', 'northkorea.gif'),
(97, 'Croatia', 'croatia.gif'),
(98, 'Estonia', 'estonia.gif'),
(99, 'Colombia', 'colombia.gif'),
(100, 'Lebanon', 'lebanon.gif'),
(101, 'Latvia', 'latvia.gif'),
(102, 'Costa Rica', 'costarica.gif'),
(103, 'Egypt', 'egypt.gif'),
(104, 'Bulgaria', 'bulgaria.gif'),
(105, 'Isla de Muerte', 'jollyroger.gif'),
(107, 'Pirates', 'jollyroger.gif');

-- --------------------------------------------------------

--
-- 表的结构 `cui`
--

DROP TABLE IF EXISTS `cui`;
CREATE TABLE IF NOT EXISTS `cui` (
  `clientipaddress` varchar(15) NOT NULL DEFAULT '',
  `callingstationid` varchar(50) NOT NULL DEFAULT '',
  `username` varchar(64) NOT NULL DEFAULT '',
  `cui` varchar(32) NOT NULL DEFAULT '',
  `creationdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastaccounting` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `downloadspeed`
--

DROP TABLE IF EXISTS `downloadspeed`;
CREATE TABLE IF NOT EXISTS `downloadspeed` (
  `id` tinyint(3) unsigned NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `downloadspeed`
--

INSERT INTO `downloadspeed` (`id`, `name`) VALUES
(4, '512Kbps'),
(5, '1Mbps'),
(6, '2Mbps'),
(7, '3Mbps'),
(8, '4Mbps'),
(9, '5Mbps'),
(10, '6Mbps'),
(12, '10Mbps'),
(14, '12Mbps'),
(16, '20Mbps'),
(17, '50Mbps'),
(18, '100Mbps');

-- --------------------------------------------------------

--
-- 表的结构 `drawlottery`
--

DROP TABLE IF EXISTS `drawlottery`;
CREATE TABLE IF NOT EXISTS `drawlottery` (
  `id` int(10) NOT NULL,
  `num1` int(10) NOT NULL DEFAULT '0',
  `num2` int(10) NOT NULL DEFAULT '0',
  `num3` int(10) NOT NULL DEFAULT '0',
  `num4` int(10) NOT NULL DEFAULT '0',
  `num5` int(10) NOT NULL DEFAULT '0',
  `drawtime` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `faq`
--

DROP TABLE IF EXISTS `faq`;
CREATE TABLE IF NOT EXISTS `faq` (
  `id` smallint(5) unsigned NOT NULL,
  `link_id` smallint(5) unsigned NOT NULL,
  `lang_id` smallint(5) unsigned NOT NULL DEFAULT '6',
  `type` enum('categ','item') NOT NULL DEFAULT 'item',
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `flag` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `categ` smallint(5) unsigned NOT NULL DEFAULT '0',
  `order` smallint(5) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `faq`
--

INSERT INTO `faq` (`id`, `link_id`, `lang_id`, `type`, `question`, `answer`, `flag`, `categ`, `order`) VALUES
(1, 1, 1, 'categ', '基本问题', '', 1, 0, 1),
(2, 2, 1, 'categ', '用户信息', '', 1, 0, 2),
(3, 3, 1, 'categ', '数据统计', '', 1, 0, 3),
(4, 4, 1, 'categ', '发布', '', 1, 0, 4),
(5, 5, 1, 'categ', '下载', '', 1, 0, 5),
(6, 6, 1, 'categ', '我的网络提供商（ISP）使用了透明代理。我应该怎么做？', '', 1, 0, 6),
(7, 7, 1, 'categ', '为什么我连不上？难道我被网站屏蔽了？', '', 1, 0, 7),
(8, 8, 1, 'categ', '如果从这里找不到我的问题的解答，我该怎么办？', '', 1, 0, 8),
(9, 1, 1, 'item', '什么是PT？', 'PT（Private Tracker）下载其实也是BT下载的一种，在有限的范围内，下载的用户基本都可以达到自己带宽的上限。PT下载还通过论坛制度的约束机制将BT的设计理念现实化，真正让用户做到下载的过程中努力上传。因此，PT下载的速度很快，能够让用户带宽得到最大程度的使用，而且种子时效长。', 1, 1, 1),
(10, 2, 1, 'item', '我忘记了我的用户名/密码了，能帮我找回来么？', '请填写<a class="faqlink" href="recover.php">这个表单</a>，我们将把登录详情发还给你。', 1, 1, 2),
(11, 3, 1, 'item', '那么，什么是分享率（Ratio）？以及如何快速提高分享率？', '分享率，即上传量和下载量的比值。分享率过低时，可能会对你的帐号带来不利的影响，甚至被封禁，所以请保持一个良好的分享率。\r\n<br /><br />\r\n你可能看到两种符号来代替数字：<br />\r\n"Inf."，这个是无限（Infinity）的缩写，意思是你的下载量为0字节，而上传量则是任意一个非零的值(上传量/下载量=无穷大)；<br />\r\n"---"，应该被视为“不可用”，这说明了你的下载量和上传量都是0字节。(上传量/下载量=0/0 这是一个不确定的量)。\r\n<br /><br />\r\n下载置顶的免费热门种子并保种一段时间是提高分享率的好方法', 1, 2, 1),
(12, 4, 1, 'item', '提升和降级又是怎样被执行的呢？', '<table cellspacing="3" cellpadding="0">\r\n	<tr>\r\n		<td class="embedded" width="200" valign="top"><b class="Peasant_Name">小主</b></td>\r\n		<td class="embedded" width="5"></td>\r\n		<td class="embedded" valign="top">当以下情况时将被自动降至本级：<br />\r\n			1.如果你已经下载了超过50GB，你应该有大于0.4的分享率。<br />\r\n			2.如果你已经下载了超过100GB，你应该有大于0.5的分享率。<br />\r\n			3.如果你已经下载了超过200GB，你应该有大于0.6的分享率。<br />\r\n			4.如果你已经下载了超过400GB，你应该有大于0.7的分享率。<br />\r\n			5.如果你已经下载了超过800GB，你应该有大于0.8的分享率。</td>\r\n	</tr>\r\n	<tr>\r\n		<td class="embedded" valign="top"><b class="PowerUser_Name">常在</b></td>\r\n		<td class="embedded"></td>\r\n		<td class="embedded" valign="top">必须注册至少1周，并且下载至少10G，分享率大于1.05。<br />\r\n			当条件符合时将被自动提升。注意，无论何时，如果你的分享率低于0.95，你将自动降级。</td>\r\n	</tr>\r\n	<tr>\r\n		<td class="embedded" valign="top"><b class="EliteUser_Name">贵人</b></td>\r\n		<td class="embedded"></td>\r\n		<td class="embedded" valign="top">必须注册至少2周，并且下载至少30G，分享率大于1.55。<br />\r\n			当条件符合时将被自动提升。注意，无论何时，如果你的分享率低于1.45，你将自动降级。</td>\r\n	</tr>\r\n	<tr>\r\n		<td class="embedded" valign="top"><b class="CrazyUser_Name">嫔</b></td>\r\n		<td class="embedded"></td>\r\n		<td class="embedded" valign="top">必须注册至少4周，并且下载至少60G，分享率大于2.05。<br />\r\n			当条件符合时将被自动提升。注意，无论何时，如果你的分享率低于1.95，你将自动降级。</td>\r\n	</tr>\r\n	<tr>\r\n		<td class="embedded" valign="top"><b class="InsaneUser_Name">贵嫔</b></td>\r\n		<td class="embedded"></td>\r\n		<td class="embedded" valign="top">必须注册至少8周，并且下载至少100G，分享率大于2.55。<br />\r\n			当条件符合时将被自动提升。注意，无论何时，如果你的分享率低于2.45，你将自动降级。</td>\r\n	</tr>\r\n	<tr>\r\n		<td class="embedded" valign="top"><b class="VeteranUser_Name">妃</b></td>\r\n		<td class="embedded"></td><td class="embedded" valign="top">必须注册至少16周，并且下载至少400G，分享率大于3.05。<br />\r\n			当条件符合时将被自动提升。注意，无论何时，如果你的分享率低于2.95，你将自动降级。</td>\r\n	</tr>\r\n	<tr><td class="embedded" valign="top"><b class="ExtremeUser_Name">贵妃</b></td>\r\n		<td class="embedded"></td>\r\n		<td class="embedded" valign="top">必须注册至少24周，并且下载至少1T，分享率大于3.55。<br />\r\n			当条件符合时将被自动提升。注意，无论何时，如果你的分享率低于3.45，你将自动降级。</td>\r\n	</tr>\r\n	<tr>\r\n		<td class="embedded" valign="top"><b class="UltimateUser_Name">皇妃</b></td>\r\n		<td class="embedded"></td>\r\n		<td class="embedded" valign="top">必须注册至少36周，并且下载至少1.5T，分享率大于4.05。<br />\r\n			当条件符合时将被自动提升。注意，无论何时，如果你的分享率低于3.95，你将自动降级。</td>\r\n	</tr>\r\n	<tr><td class="embedded" valign="top"><b class="NexusMaster_Name">皇后</b></td>\r\n		<td class="embedded"></td>\r\n		<td class="embedded" valign="top">必须注册至少52周，并且下载至少3T，分享率大于4.55。<br />\r\n			当条件符合时将被自动提升。注意，无论何时，如果你的分享率低于4.45，你将自动降级。</td>\r\n	</tr>\r\n	<tr>\r\n		<td class="embedded" valign="top"><img class="star" src="pic/trans.gif" alt="Star" /></td>\r\n		<td class="embedded"></td>\r\n		<td class="embedded">只需捐款，详见<a class="faqlink" href="donate.php">这里</a>。</td>\r\n	</tr>\r\n	<tr>\r\n		<td class="embedded" valign="top"><b class="VIP_Name">客卿(VIP)</b></td>\r\n		<td class="embedded"></td>\r\n		<td class="embedded" valign="top">由管理员仔细斟酌后分配给他们认为对于站点某方面有特殊贡献的用户。<br />\r\n			（任何索取客卿等级的要求将被自动无视）</td>\r\n	</tr>\r\n	<tr>\r\n		<td class="embedded" valign="top"><b class="User_Name">其它</b></td>\r\n		<td class="embedded"></td>\r\n		<td class="embedded">用户使用魔力值兑换，或由管理员仔细斟酌后授权。</td>\r\n	</tr>\r\n	<tr>\r\n		<td class="embedded" valign="top"><b class="Retiree_Name">致仕(Retiree)</b></td>\r\n		<td class="embedded"></td>\r\n		<td class="embedded">由管理员授予。</td>\r\n	</tr>\r\n	<tr>\r\n		<td class="embedded" valign="top"><b class="Uploader_Name">发布员(Uploader)</b></td>\r\n		<td class="embedded"></td>\r\n		<td class="embedded">由管理员分配(参见''''发布''''部分以了解详情)。</td>\r\n	</tr>\r\n	<tr>\r\n		<td class="embedded" valign="top"><b class="Forwarder_Name">转载员(Forwarder)</b></td>\r\n		<td class="embedded"></td>\r\n		<td class="embedded">由管理员分配(参见''''发布''''部分以了解详情)。</td>\r\n	</tr>\r\n	<tr>\r\n		<td class="embedded" valign="top"><b class="Downloader_Name">保种员(Downloader)</b></td>\r\n		<td class="embedded"></td>\r\n		<td class="embedded">由管理员分配(参见''''发布''''部分以了解详情)。</td>\r\n	</tr>\r\n</table>', 2, 2, 2),
(13, 5, 1, 'item', '为什么我的朋友不能加入？', '用户数目有所限制（你可以查看“首页-&gt;站点数据-&gt;上限”）。\r\n当达到那个数目时，我们会停止接受新成员。<br />\r\n由于非活跃用户(长期不使用账号的用户)引起账号资源浪费，因此我们将清理该部分用户，为其他想加入用户腾出空间。<br />\r\n关于非活跃用户清理规则，参考<a class="faqlink" href="rules.php">规则</a>。', 1, 2, 3),
(14, 6, 1, 'item', '怎样自定义头像？', '首先，找一个你喜欢的图片，当然了它不能违反我们的<a class="faqlink" href="rules.php">规则</a>。然后你要找一个存放它的地方，比方说我们自己的<a class="faqlink" href="bitbucket-upload.php">上传器</a>。出于服务器负载的考虑，我们更希望你将图片上传到别的网站，然后将其URL粘贴到你的<a class="faqlink" href="usercp.php?action=personal">控制面板</a>的头像URL部分。 \r\n<br /><br />\r\n请不要为了仅仅测试头像而发帖。如果一切顺利，你将在你的详情页面看到它。', 1, 2, 4),
(15, 7, 1, 'item', '我可以任意选择BT客户端软件么？', '目前本站Tracker<b>只允许</b>使用以下的BT客户端软件。\n<br />\n<br />\n<ul>\n<li><a class="faqlink" rel="nofollow" href="http://www.vuze.com" target="_blank">Azureus/Vuze</a>: 全部正式稳定版</li>\n<li><a class="faqlink" rel="nofollow" href="http://www.utorrent.com" target="_blank">uTorrent</a>: 全部正式稳定版</li>\n<li><a class="faqlink" rel="nofollow" href="http://www.utorrent.com" target="_blank">uTorrentMac</a>: 全部正式稳定版</li>\n<li><a class="faqlink" rel="nofollow" href="http://www.transmissionbt.com" target="_blank">Transmission</a>: 全部正式稳定版</li>\n<li><a class="faqlink" rel="nofollow" href="http://dev.deluge-torrent.org/wiki/Download" target="_blank">Deluge</a>: 全部正式稳定版</li>\n<li><a class="faqlink" rel="nofollow" href="http://libtorrent.rakshasa.no/" target="_blank">rTorrent</a>: 全部正式稳定版</li>\n<li><a class="faqlink" rel="nofollow" href="http://qbittorrent.com/" target="_blank">qBittorrent</a>: 全部正式稳定版</li>\n</ul>\n<br />\n同时请尽量避免使用处于测试期的客户端软件。\n为了从本站得到最好的下载体验，我们高度推荐<a class="faqlink" rel="nofollow" href="http://www.utorrent.com" target="_blank">uTorrent</a> 以及<a class="faqlink" rel="nofollow" href="http://www.vuze.com" target="_blank">Azureus/Vuze</a>这两个软件的最新稳定版。', 2, 3, 1),
(16, 8, 1, 'item', '多IP（我可以从不同的电脑登录/下载吗？）', '是的，目前Tracker服务器支持单个用户从任意个数的IP地址同时访问/下载。<br />\r\n然而，对于单种是有限制的。对于每个种子，最多只允许3个做种的连接，最多只允许1个下载的连接（这意味着，对于某个种子你一次只能在一个地方下载或三个地方做种）。', 2, 3, 2),
(17, 9, 1, 'item', '我需要满足哪些条件才能加入发布或转载小组呢？', '你所必须满足的条件：\r\n<ul>\r\n<li>具有稳定的资源来源</li>\r\n<li>平均每月资源发布数量不少于5个</li>\r\n</ul>\r\n你必须确保发布的文件符合以下特征：\r\n<ul>\r\n<li>不超过7天之前</li>\r\n<li>你必须要有做种的能力，或者可以确保至少有效供种24小时。</li>\r\n<li>你需要有至少达到2MBit的上传带宽。</li>\r\n</ul>\r\n如果你认为你符合以上条件，那么不要犹豫，<a class="faqlink" href="contactstaff.php">联系管理组</a>吧。<br />\r\n<b>记住！</b> 仔细填写你的申请！确保申请中包含你的上传速度以及你计划发布的内容的类型。<br />\r\n只有仔细填写并且经过慎重考虑的申请才会纳入我们的视野。', 2, 4, 1),
(18, 10, 1, 'item', '我可以发布你们这里的种子到别的Tracker服务器么？', '不能。我们是一个封闭的、限制用户数的社区。只有注册用户才能够使用我们的Tracker。将我们的种子文件发布到其他Tracker服务器是徒劳的，因为绝大多数试图下载这些文件的人将无法连接到我们的服务器。将我们的种子发布到其他Tracker只让那里的用户恼怒（因为无法下载）并且对我们产生敌对情绪，所以这种行为是我们是不能容忍的。\r\n<br /><br />\r\n如果其他网站的管理员向我们举报我们的种子被发布在他们的站点上，对此负责的用户将因此被移出我们的社区。\r\n<br /><br />\r\n但是对于从我们这里下载得到的文件，你可以随意操控它们（发布者明确说明为<b>独占</b>或<b>禁转</b>的资源除外）。你可以制作另一个种子，写入其他Tracker的地址，并发布到你喜欢的站点上。', 1, 4, 2),
(19, 11, 1, 'item', '续种及辅种教程', '首先，感谢你有想做种跟大家分享你电脑上资源的想法，感谢你对本站的支持。<br />\r\n众所周知，如果随便发补相同的种子的话，会造成你不必要的浪费及管理员的工作量。辅种或者说补种是说如果你手上有某资源，但是别人已经发布过相同的资源在PT上，那么你就不需要利用UT重新制作种子，只需要“补种”或者“辅种”即可。<br />\r\n这样做和自己发布种子效果是一样的，只是发种人不是你而已，PT的目的是分享，这也是我们的宗旨。rnrn续种和辅种的步骤都是一样的，具体如下：\r\n<ul>\r\n<li>下载种子文件。</li>\r\n<li>打开种子文件，“保存位置”选文件现在的位置，并且<b>不要勾选</b>“跳过散列检测”</li>\r\n<li>μTorrent的“状态”一栏提示“已检查 ××%”</li>\r\n<li>检查完成，“Tracker状态”一栏提示“工作中”，表示操作成功</li>\r\n<li>若没有提示“已检查 ××%”，请确保文件相同，并且位置没有选错。</li>\r\n</ul>\r\n以上步骤的前提是你的资源和已经发布过的资源是完全相同的<br/><br/>\r\n<b>也有可能你没开UT的时候...无人做种...别人点了请求续种<br/>这样的话你只需要打开UT让种子开始做种就行了</b>', 1, 4, 3),
(20, 12, 1, 'item', '为什么我的下载有时候停在99%？', '你所下载到的片段越多，寻找拥有你所缺少的片段的同伴就将变得越困难。这就是为什么有时候在下载即将完成的时候，速度会变得非常慢，甚至完全停止了。只要耐心等候，或早或晚剩下的部分一定能够下载到的。<br />\r\n偶尔情况下会出现不能下载的状况<br />\r\n可以点击UT面板上的文件<br />\r\n找到红色的，即未下载完成的<br />\r\n向发布者私下索要此文件放进去校验', 1, 5, 1),
(21, 13, 1, 'item', '那么这个呢？ “IO错误 - [错误号13] 许可被拒绝”？', '如果你只是想要解决这个问题的话，重新启动你的电脑应该就可以了。否则的话，继续读下去。<br /><br />\r\nIO错误指的是输入-输出错误，这是一个文件系统错误，而非来自我们的Tracker服务器。当你的客户端由于某些原因不能打开种子中未下载完成的文件时，这个错误就会发生。 通常原因是客户端的两个实例同时在运行：当上一次关闭客户端时因为某种原因它没有被真正关闭，从而始终在后台运行。因此文件被锁定，使得新的实例不能打开这个文件。<br /><br />\r\n另外一个不常出现的原因是因为老旧的FAT文件系统。某些系统崩溃可能导致未下载完成的文件损坏，接着就出现了这个错误。运行一次scandisk应该可以解决这个问题。注意，只有当你使用Windows 9X操作系统（只支持FAT文件系统）或者在Windows NT/2000/XP中使用FAT文件系统的时候才比较可能出现这个问题。NTFS文件系统要健壮许多，不太可能出现这样的问题。', 1, 5, 2),
(22, 14, 1, 'item', '为什么我无法下载本站资源？', '在网络通畅的前提下，若做种数为0则说明无人做种，因此暂时无法下载。需要做种的人打开UT开始做种的时候才会有速度。可以请求续种或者试试别的资源；若做种数非0还是无法下载说明你的客户端有问题，试试这个客户端吧！<a  class="faqlink" href=uTorrent.zip>uTorrent.zip</a>。\r\n<br /><br />\r\n注意：下载完成后请 不要 立即关闭客户端，这样你在分享他人的资源时别人也可以分享你的资源，同时你的用户等级也会慢慢提升；反之，只知索取却不愿奉献，你将被自动降级直至无法下载任何资源。对待生活亦应如此。\r\n<br /><br />\r\n新用户下载千万不要右键另存为<br />\r\n点击左键会出现提醒分享率的页面。点击已阅读方可正常下载', 1, 5, 3),
(23, 15, 1, 'item', '上传下载没有速度或速度很慢', '首先请确定UT右下角显示的是绿色图标.<br />\r\n就在显示上传下载速度附近。绿色是提示网络正常<br /><br />\r\n黄色或者红色<br />\r\n请关闭系统自带防火墙<br />\r\n并请点击设置->连接->随机端口<br />\r\n确定并且等待一段时间\r\n<br /><br />\r\n然后请确定种子有人上传<br />\r\n点击种子详情下方有做种状态<br />\r\n最后请确定上传者可连接<br />\r\n请私信上传者检查第一步', 1, 5, 4),
(24, 16, 1, 'item', '什么是代理？', '基本上说，代理就是中间人。当你通过代理访问某个站点的时候，你的请求将首先被发送到代理处，然后由代理转发到你所请求的站点，而不是你直接连通到网站。下面是几种典型的代理分类（术语不是那么的规范）：\r\n<br /><br />\r\n<table cellspacing="3" cellpadding="0">\r\n<tr>\r\n<td class="embedded" valign="top" width="100">透明代理</td>\r\n<td class="embedded" width="10"></td>\r\n<td class="embedded" valign="top">透明代理在客户端无需配置。他将自动把80端口的信息重定向到代理(有时候也作为非匿名代理的同义词)；</td>\r\n</tr>\r\n<tr>\r\n<td class="embedded" valign="top">显式代理</td>\r\n<td class="embedded" width="10"></td>\r\n<td class="embedded" valign="top">浏览器必须经过配置才能使用代理；</td>\r\n</tr>\r\n<tr>\r\n<td class="embedded" valign="top">匿名代理</td>\r\n<td class="embedded" width="10"></td>\r\n<td class="embedded" valign="top">代理将不会把客户标记发送至服务器端 (HTTP_X_FORWARDED_FOR头标志将不被发送。服务器也因此看不到你的IP)；</td>\r\n</tr>\r\n<tr>\r\n<td class="embedded" valign="top">高度匿名代理</td>\r\n<td class="embedded" width="10"></td>\r\n<td class="embedded" valign="top">代理将不会把客户标记或代理标记发送至服务器端 (HTTP_X_FORWARDED_FOR和HTTP_VIA和HTTP_PROXY_CONNECTION头标志将不被发送。服务器看不到你的IP，甚至不知道你正在使用代理)；</td>\r\n</tr>\r\n<tr>\r\n<td class="embedded" valign="top">公用</td>\r\n<td class="embedded" width="10"></td>\r\n<td class="embedded" valign="top">(不言自明了)。</td>\r\n</tr>\r\n</table>\r\n<br />\r\n透明代理可能是匿名的，也可能是非匿名的，且匿名也有相应的等级。', 1, 6, 1),
(25, 17, 1, 'item', '也许是因为我的IP地址被列入黑名单了？', '这个网站有屏蔽被禁用户或攻击者的IP地址的功能。该功能在Tengine/PHP层面上起作用，且只能屏蔽从这些地址<i>登录</i>网站。这不会阻止你<i>连接</i>到网站，更无法从底层的协议层面进行屏蔽。即便你的地址已经被列入了黑名单，你应该仍能够通过ping/traceroute命令连同服务器。如果你不能连通服务器，那么问题的原因在别处。\r\n<br /><br />\r\n如果你的IP地址因某种原因错误地被我们屏蔽了，请联系我们。', 1, 7, 1),
(26, 18, 1, 'item', '你的ISP服务商屏蔽了本站的地址', '首先，这一般不像你的ISP的所作所为。DNS域名解析以及/或者网络问题是常见的真凶。<br />\r\n对此我们无能为力。你应该联系你的ISP（或者换一个）。记住你仍然可以通过代理访问本站，参看此常见问题关于代理的部分。在这种情况下，代理是否匿名或者使用哪个监听端口都不重要了。\r\n<br /><br />\r\n注意，你的“可连接状态”将因此一直被列为“否”，因为此时Tracker服务器无法检查你的BT客户端接收连接的状态。', 1, 7, 2),
(27, 19, 1, 'item', '不妨试试这个', '通常情况下你能得到友善而有用的帮助。这里给你一些基本的指导：\r\n<ul>\r\n<li>确保你的问题的确不在这篇FAQ中。这样的发帖导致的结果就是返回这篇FAQ。</li>\r\n<li>在发帖之前，看看置顶。很多时候，还没有被我们的FAQ所收录的最新的信息都可以在那里被找到。</li>\r\n<li>帮助我们也就是帮助你自己。不要仅仅说“嘿我这里出问题了！！”提供一些详情，这样我们就不用猜测或者浪费时间来询问了。你使用的是什么客户端？什么操作系统？怎样的网络设置？如果发生了错误，是什么样的具体错误信息？有问题的种子文件是哪个？你所提供的信息越多，对你的帮助也就越容易，你的帖子也就更加容易得到回复。</li>\r\n<li>不需要说的是：保持礼貌。趾高气扬得命令别人帮助你很少会有用。</li>\r\n</ul>', 1, 8, 1),
(28, 20, 1, 'item', '什么是密钥系统？它是怎么工作的？什么时候我需要重置我的密钥？', '密钥系统在BT客户端连接Tracker服务器时起到验证身份的作用。每一个用户都有一个系统随机生成的密钥。当用户下载某个种子文件时，他的私人密钥被添加到种子文件的Tracker服务器URL中。通过这种方式，你可以在家中或者办公室使用不同的IP来做种某个文件。\r\n<ul>\r\n<li>如果你的密钥被泄漏，且别的用户正在使用这个密钥（即使用你的帐户）下载文件。在这种情况下，你能在你的详情页面看到你并未下载或上传的种子。</li>\r\n<li>当你的客户端崩溃，或者你的连接并没有正常被终止。在这样的情况下，就算你已经关闭了你的客户端，你仍然在你的详情页面看到正在上传/下载的记录。通常情况下这些“冗余种子”将在30分钟之内被自动清除，但是如果你马上继续你的下载而服务器又提示“You already are downloading the same torrent”的错误，那么你需要重置你的密钥，然后重新下载种子文件，之后继续下载过程。 </li>\r\n</ul>', 1, 2, 5),
(29, 21, 1, 'item', '为什么我下载量和分享率以及注册时间都达到升级标准却没有升级呢？', '这个时候你要做的是多点耐心，然后睡个好觉，第二天早上醒来，你会发现你的帐号已然升级，这是因为服务器对用户等级的判定是每天一次的！', 1, 2, 6),
(30, 22, 1, 'item', '什么是“等待”？', '等待，就是需要等待指定时间后才可以进行下载，属于系统限制低分享率用户的措施\r\n<br /><br />\r\n目前规则：\r\n<ul>\r\n<li>下载量超过10G，并且分享率低于<b>0.2</b>的，需要等待<font style="color:red">24小时</font>。</li>\r\n<li>下载量超过10G，并且分享率低于<b>0.3</b>的，需要等待<font style="color:red">12小时</font>。</li>\r\n<li>下载量超过10G，并且分享率低于<b>0.4</b>的，需要等待<font style="color:red">6小时</font>。</li>\r\n<li>下载量超过10G，并且分享率低于<b>0.5</b>的，需要等待<font style="color:red">3小时</font>。</li>\r\n</ul>', 3, 5, 5),
(31, 23, 1, 'item', '什么是“连接数”？', '连接数，就是你可以同时下载的资源数量，低分享率用户将会受到此限制\r\n<br /><br />\r\n目前规则：\r\n<ul>\r\n<li>下载量超过10G，并且分享率低于<b>0.2</b>的，同时最多只能下载<font style="color:red">1个资源</font>。</li>\r\n<li>下载量超过10G，并且分享率低于<b>0.3</b>的，同时最多只能下载<font style="color:red">2个资源</font>。</li>\r\n<li>下载量超过10G，并且分享率低于<b>0.4</b>的，同时最多只能下载<font style="color:red">3个资源</font>。</li>\r\n<li>下载量超过10G，并且分享率低于<b>0.5</b>的，同时最多只能下载<font style="color:red">4个资源</font>。</li>\r\n</ul>', 3, 5, 6);

-- --------------------------------------------------------

--
-- 表的结构 `files`
--

DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(10) unsigned NOT NULL,
  `torrent` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL DEFAULT '',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `forummods`
--

DROP TABLE IF EXISTS `forummods`;
CREATE TABLE IF NOT EXISTS `forummods` (
  `id` smallint(5) unsigned NOT NULL,
  `forumid` smallint(5) unsigned NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `forums`
--

DROP TABLE IF EXISTS `forums`;
CREATE TABLE IF NOT EXISTS `forums` (
  `id` smallint(5) unsigned NOT NULL,
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0',
  `name` varchar(60) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  `minclassread` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `minclasswrite` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `postcount` int(10) unsigned NOT NULL DEFAULT '0',
  `topiccount` int(10) unsigned NOT NULL DEFAULT '0',
  `minclasscreate` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `forid` smallint(5) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `friends`
--

DROP TABLE IF EXISTS `friends`;
CREATE TABLE IF NOT EXISTS `friends` (
  `id` int(10) unsigned NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL,
  `friendid` mediumint(8) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `fun`
--

DROP TABLE IF EXISTS `fun`;
CREATE TABLE IF NOT EXISTS `fun` (
  `id` mediumint(8) unsigned NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `body` text,
  `title` varchar(255) NOT NULL DEFAULT '',
  `status` enum('normal','dull','notfunny','funny','veryfunny','banned') NOT NULL DEFAULT 'normal'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `funds`
--

DROP TABLE IF EXISTS `funds`;
CREATE TABLE IF NOT EXISTS `funds` (
  `id` int(10) unsigned NOT NULL,
  `usd` decimal(8,2) NOT NULL DEFAULT '0.00',
  `cny` decimal(8,2) NOT NULL DEFAULT '0.00',
  `user` mediumint(8) unsigned NOT NULL,
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `memo` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `funvotes`
--

DROP TABLE IF EXISTS `funvotes`;
CREATE TABLE IF NOT EXISTS `funvotes` (
  `funid` mediumint(8) unsigned NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL,
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `vote` enum('fun','dull') NOT NULL DEFAULT 'fun'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `gift`
--

DROP TABLE IF EXISTS `gift`;
CREATE TABLE IF NOT EXISTS `gift` (
  `id` int(10) unsigned NOT NULL,
  `userid` int(10) unsigned NOT NULL,
  `cards` varchar(255) NOT NULL,
  `num` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `bonus` decimal(20,8) NOT NULL,
  `lose` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `giftlog`
--

DROP TABLE IF EXISTS `giftlog`;
CREATE TABLE IF NOT EXISTS `giftlog` (
  `id` int(10) unsigned NOT NULL,
  `userid` int(10) unsigned NOT NULL,
  `cards` varchar(255) NOT NULL,
  `bonus` decimal(20,8) NOT NULL,
  `time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `givebonus`
--

DROP TABLE IF EXISTS `givebonus`;
CREATE TABLE IF NOT EXISTS `givebonus` (
  `id` int(10) unsigned NOT NULL,
  `bonusfromuserid` mediumint(8) unsigned NOT NULL,
  `bonustotorrentid` mediumint(8) unsigned NOT NULL,
  `bonus` decimal(10,1) unsigned NOT NULL,
  `type` int(1) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `invitebox`
--

DROP TABLE IF EXISTS `invitebox`;
CREATE TABLE IF NOT EXISTS `invitebox` (
  `Id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(30) NOT NULL DEFAULT '',
  `school` varchar(30) DEFAULT NULL,
  `grade` varchar(20) DEFAULT NULL,
  `web` varchar(50) DEFAULT NULL,
  `disk` varchar(50) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `self_introduction` varchar(255) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  `dealt_by` varchar(20) NOT NULL DEFAULT 'no',
  `ip` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `invites`
--

DROP TABLE IF EXISTS `invites`;
CREATE TABLE IF NOT EXISTS `invites` (
  `id` int(10) unsigned NOT NULL,
  `inviter` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `invitee` varchar(80) NOT NULL DEFAULT '',
  `hash` char(32) NOT NULL,
  `time_invited` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nonass` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `iplog`
--

DROP TABLE IF EXISTS `iplog`;
CREATE TABLE IF NOT EXISTS `iplog` (
  `id` bigint(20) unsigned NOT NULL,
  `ip` varbinary(64) NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL,
  `access` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `isp`
--

DROP TABLE IF EXISTS `isp`;
CREATE TABLE IF NOT EXISTS `isp` (
  `id` tinyint(3) unsigned NOT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `isp`
--

INSERT INTO `isp` (`id`, `name`) VALUES
(1, '中国电信'),
(2, '中国移动'),
(3, '中国联通'),
(4, '中国教育网'),
(5, 'Other');

-- --------------------------------------------------------

--
-- 表的结构 `language`
--

DROP TABLE IF EXISTS `language`;
CREATE TABLE IF NOT EXISTS `language` (
  `id` smallint(5) unsigned NOT NULL,
  `lang_name` varchar(50) NOT NULL,
  `flagpic` varchar(255) NOT NULL DEFAULT '',
  `sub_lang` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `rule_lang` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `site_lang` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `site_lang_folder` varchar(255) NOT NULL DEFAULT '',
  `trans_state` enum('','outdate','incomplete','need-new','') NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `language`
--

INSERT INTO `language` (`id`, `lang_name`, `flagpic`, `sub_lang`, `rule_lang`, `site_lang`, `site_lang_folder`, `trans_state`) VALUES
(1, '简体中文', 'china.gif', 1, 1, 1, 'chs', ''),
(2, '繁體中文', 'hongkong.gif', 1, 0, 0, '', ''),
(3, 'English', 'uk.gif', 1, 0, 0, '', ''),
(4, '日本語', 'japan.gif', 1, 0, 0, '', ''),
(5, '한국어', 'southkorea.gif', 1, 0, 0, '', ''),
(6, 'Bulgarian', 'bulgaria.gif', 1, 0, 0, '', ''),
(7, 'Croatian', 'croatia.gif', 1, 0, 0, '', ''),
(8, 'Czech', 'czechrep.gif', 1, 0, 0, '', ''),
(9, 'Danish', 'denmark.gif', 1, 0, 0, '', ''),
(10, 'Dutch', 'netherlands.gif', 1, 0, 0, '', ''),
(11, 'Estonian', 'estonia.gif', 1, 0, 0, '', ''),
(12, 'Finnish', 'finland.gif', 1, 0, 0, '', ''),
(13, 'French', 'france.gif', 1, 0, 0, '', ''),
(14, 'German', 'germany.gif', 1, 0, 0, '', ''),
(15, 'Greek', 'greece.gif', 1, 0, 0, '', ''),
(16, 'Hebrew', 'israel.gif', 1, 0, 0, '', ''),
(17, 'Hungarian', 'hungary.gif', 1, 0, 0, '', ''),
(18, 'Italian', 'italy.gif', 1, 0, 0, '', ''),
(19, 'Norwegian', 'norway.gif', 1, 0, 0, '', ''),
(20, 'Other', 'jollyroger.gif', 1, 0, 0, '', ''),
(21, 'Polish', 'poland.gif', 1, 0, 0, '', ''),
(22, 'Portuguese', 'portugal.gif', 1, 0, 0, '', ''),
(23, 'Romanian', 'romania.gif', 1, 0, 0, '', ''),
(24, 'Russian', 'russia.gif', 1, 0, 0, '', ''),
(25, 'Serbian', 'serbia.gif', 1, 0, 0, '', ''),
(26, 'Slovak', 'slovakia.gif', 1, 0, 0, '', ''),
(27, 'Spanish', 'spain.gif', 1, 0, 0, '', ''),
(28, 'Swedish', 'sweden.gif', 1, 0, 0, '', ''),
(29, 'Turkish', 'turkey.gif', 1, 0, 0, '', ''),
(30, 'Slovenian', 'slovenia.gif', 1, 0, 0, '', ''),
(31, 'Thai', 'thailand.gif', 1, 0, 0, '', '');

-- --------------------------------------------------------

--
-- 表的结构 `limitinvite`
--

DROP TABLE IF EXISTS `limitinvite`;
CREATE TABLE IF NOT EXISTS `limitinvite` (
  `id` bigint(20) unsigned NOT NULL,
  `userid` bigint(20) unsigned NOT NULL,
  `invites` bigint(20) unsigned NOT NULL,
  `limited` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `limitsuggest`
--

DROP TABLE IF EXISTS `limitsuggest`;
CREATE TABLE IF NOT EXISTS `limitsuggest` (
  `id` int(10) unsigned NOT NULL,
  `keywords` varchar(255) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `adddate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `links`
--

DROP TABLE IF EXISTS `links`;
CREATE TABLE IF NOT EXISTS `links` (
  `id` tinyint(3) unsigned NOT NULL,
  `name` varchar(30) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(50) NOT NULL DEFAULT '',
  `range` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `location_main` varchar(200) NOT NULL,
  `location_sub` varchar(200) NOT NULL,
  `flagpic` varchar(50) DEFAULT NULL,
  `start_ip` varchar(20) NOT NULL,
  `end_ip` varchar(20) NOT NULL,
  `theory_upspeed` int(10) unsigned NOT NULL DEFAULT '10',
  `practical_upspeed` int(10) unsigned NOT NULL DEFAULT '10',
  `theory_downspeed` int(10) unsigned NOT NULL DEFAULT '10',
  `practical_downspeed` int(10) unsigned NOT NULL DEFAULT '10',
  `hit` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `loginattempts`
--

DROP TABLE IF EXISTS `loginattempts`;
CREATE TABLE IF NOT EXISTS `loginattempts` (
  `id` int(10) unsigned NOT NULL,
  `ip` varchar(64) NOT NULL DEFAULT '',
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `banned` enum('yes','no') NOT NULL DEFAULT 'no',
  `attempts` smallint(5) unsigned NOT NULL DEFAULT '0',
  `type` enum('login','recover') NOT NULL DEFAULT 'login'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `lottery`
--

DROP TABLE IF EXISTS `lottery`;
CREATE TABLE IF NOT EXISTS `lottery` (
  `id` int(10) NOT NULL,
  `ownerid` int(10) DEFAULT NULL,
  `selfkey` text NOT NULL,
  `drawid` int(10) NOT NULL DEFAULT '0',
  `num1` int(10) NOT NULL DEFAULT '0',
  `num2` int(10) NOT NULL DEFAULT '0',
  `num3` int(10) NOT NULL DEFAULT '0',
  `num4` int(10) NOT NULL DEFAULT '0',
  `num5` int(10) NOT NULL DEFAULT '0',
  `shoptime` date NOT NULL DEFAULT '0000-00-00',
  `isencase` int(1) NOT NULL DEFAULT '0',
  `encasetime` date NOT NULL DEFAULT '0000-00-00',
  `multiple` int(10) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- 表的结构 `media`
--

DROP TABLE IF EXISTS `media`;
CREATE TABLE IF NOT EXISTS `media` (
  `id` tinyint(3) unsigned NOT NULL,
  `lid` tinyint(3) NOT NULL,
  `name` varchar(30) NOT NULL,
  `sort_index` tinyint(3) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `media`
--

INSERT INTO `media` (`id`, `lid`, `name`, `sort_index`) VALUES
(1, 0, 'BluRay', 0),
(2, 0, 'HD DVD', 1),
(4, 0, 'MiniBD', 4),
(5, 0, 'HDTV', 5),
(6, 0, 'DVDR', 6),
(7, 0, 'Encode', 3),
(3, 0, 'Remux', 2),
(8, 0, 'CD', 7),
(9, 0, 'Track', 9);

-- --------------------------------------------------------

--
-- 表的结构 `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) unsigned NOT NULL,
  `sender` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `receiver` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `subject` varchar(128) NOT NULL DEFAULT '',
  `msg` text,
  `unread` enum('yes','no') NOT NULL DEFAULT 'yes',
  `location` smallint(6) NOT NULL DEFAULT '1',
  `saved` enum('no','yes') NOT NULL DEFAULT 'no',
  `goto` tinyint(1) unsigned zerofill NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `modpanel`
--

DROP TABLE IF EXISTS `modpanel`;
CREATE TABLE IF NOT EXISTS `modpanel` (
  `id` tinyint(3) unsigned NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `info` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `modpanel`
--

INSERT INTO `modpanel` (`id`, `name`, `url`, `info`) VALUES
(1, '搜索用户', 'searchuser.php', '搜索用户'),
(2, '查看限定资源用户', 'limitduser.php', '查看限定资源用户'),
(3, '查看未验证用户', 'unco.php', '查看未验证用户'),
(4, '发布转载组考核', 'uploaders.php', '查看发布转载组考核状态'),
(5, '流量异常监控', 'cheaters.php', '查看流量异常的账户'),
(6, '重复IP地址检测', 'ipcheck.php', '检查IP地址重复的用户'),
(7, '当前在线客户端', 'allagents.php', '查看当前在线（上传/下载/做种）的客户端'),
(8, '添加限时邀请', 'invitelimi.php', '为指定用户组添加限时邀请'),
(9, '批量发送邀请', 'autoinvite.php', '批量发送邀请'),
(10, '失败登录记录', 'maxlogin.php', '查看密码错误尝试记录'),
(11, '用户头像管理', 'bitbucketlog.php', '管理用户上传头像'),
(12, 'IP地址检测', 'testip.php', '检测IP是否被禁止锁定'),
(13, '充值卡管理', 'genrecharge.php', '生成/删除充值卡'),
(14, 'Tracker服务器状态', 'stats.php', '查看Tracker状态');

-- --------------------------------------------------------

--
-- 表的结构 `nas`
--

DROP TABLE IF EXISTS `nas`;
CREATE TABLE IF NOT EXISTS `nas` (
  `id` int(10) NOT NULL,
  `nasname` varchar(128) NOT NULL,
  `shortname` varchar(32) DEFAULT NULL,
  `type` varchar(30) DEFAULT 'other',
  `ports` int(5) DEFAULT NULL,
  `secret` varchar(60) NOT NULL DEFAULT 'secret',
  `server` varchar(64) DEFAULT NULL,
  `community` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT 'RADIUS Client'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` mediumint(8) unsigned NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `body` text,
  `title` varchar(255) NOT NULL DEFAULT '',
  `notify` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `notice`
--

DROP TABLE IF EXISTS `notice`;
CREATE TABLE IF NOT EXISTS `notice` (
  `id` tinyint(4) NOT NULL,
  `noticeoff` enum('no','yes') NOT NULL DEFAULT 'no',
  `notice` varchar(10240) NOT NULL,
  `bydata` datetime NOT NULL,
  `byuser` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `offers`
--

DROP TABLE IF EXISTS `offers`;
CREATE TABLE IF NOT EXISTS `offers` (
  `id` mediumint(8) unsigned NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `name` varchar(225) NOT NULL,
  `descr` text,
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `allowedtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `yeah` smallint(5) unsigned NOT NULL DEFAULT '0',
  `against` smallint(5) unsigned NOT NULL DEFAULT '0',
  `category` smallint(5) unsigned NOT NULL DEFAULT '0',
  `comments` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `allowed` enum('allowed','pending','denied') NOT NULL DEFAULT 'pending'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `offervotes`
--

DROP TABLE IF EXISTS `offervotes`;
CREATE TABLE IF NOT EXISTS `offervotes` (
  `id` int(10) unsigned NOT NULL,
  `offerid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `vote` enum('yeah','against') NOT NULL DEFAULT 'yeah'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `overforums`
--

DROP TABLE IF EXISTS `overforums`;
CREATE TABLE IF NOT EXISTS `overforums` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(60) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  `minclassview` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `peers`
--

DROP TABLE IF EXISTS `peers`;
CREATE TABLE IF NOT EXISTS `peers` (
  `id` int(10) unsigned NOT NULL,
  `torrent` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `peer_id` binary(20) NOT NULL,
  `ip` varbinary(64) NOT NULL DEFAULT '',
  `ipv6` varbinary(64) NOT NULL DEFAULT '',
  `port` smallint(5) unsigned NOT NULL DEFAULT '0',
  `uploaded` bigint(20) unsigned NOT NULL DEFAULT '0',
  `downloaded` bigint(20) unsigned NOT NULL DEFAULT '0',
  `to_go` bigint(20) unsigned NOT NULL DEFAULT '0',
  `seeder` enum('yes','no') NOT NULL DEFAULT 'no',
  `started` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_action` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `prev_action` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `connectable` enum('yes','no') NOT NULL DEFAULT 'yes',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `agent` varchar(60) NOT NULL DEFAULT '',
  `finishedat` int(10) unsigned NOT NULL DEFAULT '0',
  `downloadoffset` bigint(20) unsigned NOT NULL DEFAULT '0',
  `uploadoffset` bigint(20) unsigned NOT NULL DEFAULT '0',
  `passkey` char(32) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- 表的结构 `pmboxes`
--

DROP TABLE IF EXISTS `pmboxes`;
CREATE TABLE IF NOT EXISTS `pmboxes` (
  `id` mediumint(8) unsigned NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL,
  `boxnumber` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `name` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `pollanswers`
--

DROP TABLE IF EXISTS `pollanswers`;
CREATE TABLE IF NOT EXISTS `pollanswers` (
  `id` int(10) unsigned NOT NULL,
  `pollid` mediumint(8) unsigned NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL,
  `selection` tinyint(3) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `polls`
--

DROP TABLE IF EXISTS `polls`;
CREATE TABLE IF NOT EXISTS `polls` (
  `id` mediumint(8) unsigned NOT NULL,
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `question` varchar(255) NOT NULL DEFAULT '',
  `option0` varchar(40) NOT NULL DEFAULT '',
  `option1` varchar(40) NOT NULL DEFAULT '',
  `option2` varchar(40) NOT NULL DEFAULT '',
  `option3` varchar(40) NOT NULL DEFAULT '',
  `option4` varchar(40) NOT NULL DEFAULT '',
  `option5` varchar(40) NOT NULL DEFAULT '',
  `option6` varchar(40) NOT NULL DEFAULT '',
  `option7` varchar(40) NOT NULL DEFAULT '',
  `option8` varchar(40) NOT NULL DEFAULT '',
  `option9` varchar(40) NOT NULL DEFAULT '',
  `option10` varchar(40) NOT NULL DEFAULT '',
  `option11` varchar(40) NOT NULL DEFAULT '',
  `option12` varchar(40) NOT NULL DEFAULT '',
  `option13` varchar(40) NOT NULL DEFAULT '',
  `option14` varchar(40) NOT NULL DEFAULT '',
  `option15` varchar(40) NOT NULL DEFAULT '',
  `option16` varchar(40) NOT NULL DEFAULT '',
  `option17` varchar(40) NOT NULL DEFAULT '',
  `option18` varchar(40) NOT NULL DEFAULT '',
  `option19` varchar(40) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL,
  `topicid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `body` text,
  `ori_body` text,
  `editedby` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `editdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sendlog` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `processings`
--

DROP TABLE IF EXISTS `processings`;
CREATE TABLE IF NOT EXISTS `processings` (
  `id` tinyint(3) unsigned NOT NULL,
  `lid` tinyint(3) NOT NULL,
  `name` varchar(30) NOT NULL,
  `sort_index` tinyint(3) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `processings`
--

INSERT INTO `processings` (`id`, `lid`, `name`, `sort_index`) VALUES
(1, 0, 'Raw', 0),
(2, 0, 'Encode', 0),
(3, 0, 'Other', 0);

-- --------------------------------------------------------

--
-- 表的结构 `prolinkclicks`
--

DROP TABLE IF EXISTS `prolinkclicks`;
CREATE TABLE IF NOT EXISTS `prolinkclicks` (
  `id` int(10) unsigned NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(64) NOT NULL DEFAULT '',
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `radacct`
--

DROP TABLE IF EXISTS `radacct`;
CREATE TABLE IF NOT EXISTS `radacct` (
  `radacctid` bigint(21) NOT NULL,
  `acctsessionid` varchar(64) NOT NULL DEFAULT '',
  `acctuniqueid` varchar(32) NOT NULL DEFAULT '',
  `username` varchar(64) NOT NULL DEFAULT '',
  `groupname` varchar(64) NOT NULL DEFAULT '',
  `realm` varchar(64) DEFAULT '',
  `nasipaddress` varchar(15) NOT NULL DEFAULT '',
  `nasportid` varchar(15) DEFAULT NULL,
  `nasporttype` varchar(32) DEFAULT NULL,
  `acctstarttime` datetime DEFAULT NULL,
  `acctstoptime` datetime DEFAULT NULL,
  `acctsessiontime` int(12) DEFAULT NULL,
  `acctauthentic` varchar(32) DEFAULT NULL,
  `connectinfo_start` varchar(50) DEFAULT NULL,
  `connectinfo_stop` varchar(50) DEFAULT NULL,
  `acctinputoctets` bigint(20) DEFAULT NULL,
  `acctoutputoctets` bigint(20) DEFAULT NULL,
  `calledstationid` varchar(50) NOT NULL DEFAULT '',
  `callingstationid` varchar(50) NOT NULL DEFAULT '',
  `acctterminatecause` varchar(32) NOT NULL DEFAULT '',
  `servicetype` varchar(32) DEFAULT NULL,
  `framedprotocol` varchar(32) DEFAULT NULL,
  `framedipaddress` varchar(15) NOT NULL DEFAULT '',
  `acctstartdelay` int(12) DEFAULT NULL,
  `acctstopdelay` int(12) DEFAULT NULL,
  `xascendsessionsvrkey` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `radcheck`
--

DROP TABLE IF EXISTS `radcheck`;
CREATE TABLE IF NOT EXISTS `radcheck` (
  `id` int(11) unsigned NOT NULL,
  `username` varchar(64) NOT NULL DEFAULT '',
  `attribute` varchar(64) NOT NULL DEFAULT '',
  `op` char(2) NOT NULL DEFAULT '==',
  `value` varchar(253) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `radgroupcheck`
--

DROP TABLE IF EXISTS `radgroupcheck`;
CREATE TABLE IF NOT EXISTS `radgroupcheck` (
  `id` int(11) unsigned NOT NULL,
  `groupname` varchar(64) NOT NULL DEFAULT '',
  `attribute` varchar(64) NOT NULL DEFAULT '',
  `op` char(2) NOT NULL DEFAULT '==',
  `value` varchar(253) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `radgroupcheck`
--

INSERT INTO `radgroupcheck` (`id`, `groupname`, `attribute`, `op`, `value`) VALUES
(1, 'user', 'Simultaneous-Use', ':=', '1'),
(2, 'planA', 'Simultaneous-Use', ':=', '1'),
(3, 'planB', 'Simultaneous-Use', ':=', '1'),
(4, 'planC', 'Simultaneous-Use', ':=', '1');

-- --------------------------------------------------------

--
-- 表的结构 `radgroupreply`
--

DROP TABLE IF EXISTS `radgroupreply`;
CREATE TABLE IF NOT EXISTS `radgroupreply` (
  `id` int(11) unsigned NOT NULL,
  `groupname` varchar(64) NOT NULL DEFAULT '',
  `attribute` varchar(64) NOT NULL DEFAULT '',
  `op` char(2) NOT NULL DEFAULT '=',
  `value` varchar(253) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `radgroupreply`
--

INSERT INTO `radgroupreply` (`id`, `groupname`, `attribute`, `op`, `value`) VALUES
(1, 'user', 'Auth-Type', ':=', 'Local'),
(2, 'user', 'Service-Type', ':=', 'Framed-User'),
(3, 'user', 'Framed-IP-Address', ':=', '255.255.255.255'),
(4, 'user', 'Framed-IP-Netmask', ':=', '255.255.255.0'),
(5, 'user', 'Acct-Interim-Interval', ':=', '300'),
(6, 'user', 'Max-Monthly-Traffic', ':=', '209715200'),
(7, 'user', 'Framed-Protocol', ':=', 'PPP'),
(8, 'user', 'Framed-MTU', ':=', '1500'),
(9, 'user', 'Framed-Compression', ':=', 'Van-Jacobson-TCP-IP'),
(10, 'planA', 'Auth-Type', ':=', 'Local'),
(11, 'planA', 'Service-Type', ':=', 'Framed-User'),
(12, 'planA', 'Framed-IP-Address', ':=', '255.255.255.255'),
(13, 'planA', 'Framed-IP-Netmask', ':=', '255.255.255.0'),
(14, 'planA', 'Acct-Interim-Interval', ':=', '300'),
(15, 'planA', 'Max-Monthly-Traffic', ':=', '314572800'),
(16, 'planA', 'Framed-Protocol', ':=', 'PPP'),
(17, 'planA', 'Framed-MTU', ':=', '1500'),
(18, 'planA', 'Framed-Compression', ':=', 'Van-Jacobson-TCP-IP'),
(19, 'planA-D', 'Bonus', ':=', '50000'),
(20, 'planA-D', 'Plan', ':=', '100M 流量加油包'),
(21, 'planB', 'Auth-Type', ':=', 'Local'),
(22, 'planB', 'Service-Type', ':=', 'Framed-User'),
(23, 'planB', 'Framed-IP-Address', ':=', '255.255.255.255'),
(24, 'planB', 'Framed-IP-Netmask', ':=', '255.255.255.0'),
(25, 'planB', 'Acct-Interim-Interval', ':=', '300'),
(26, 'planB', 'Max-Monthly-Traffic', ':=', '419430400'),
(27, 'planB', 'Framed-Protocol', ':=', 'PPP'),
(28, 'planB', 'Framed-MTU', ':=', '1500'),
(29, 'planB', 'Framed-Compression', ':=', 'Van-Jacobson-TCP-IP'),
(30, 'planB-D', 'Bonus', ':=', '80000'),
(31, 'planB-D', 'Plan', ':=', '200M 流量加油包'),
(32, 'planC', 'Auth-Type', ':=', 'Local'),
(33, 'planC', 'Service-Type', ':=', 'Framed-User'),
(34, 'planC', 'Framed-IP-Address', ':=', '255.255.255.255'),
(35, 'planC', 'Framed-IP-Netmask', ':=', '255.255.255.0'),
(36, 'planC', 'Acct-Interim-Interval', ':=', '300'),
(37, 'planC', 'Max-Monthly-Traffic', ':=', '524288000'),
(38, 'planC', 'Framed-Protocol', ':=', 'PPP'),
(39, 'planC', 'Framed-MTU', ':=', '1500'),
(40, 'planC', 'Framed-Compression', ':=', 'Van-Jacobson-TCP-IP'),
(41, 'planC-D', 'Bonus', ':=', '120000'),
(42, 'planC-D', 'Plan', ':=', '300M 流量加油包');

-- --------------------------------------------------------

--
-- 表的结构 `radhuntgroup`
--

DROP TABLE IF EXISTS `radhuntgroup`;
CREATE TABLE IF NOT EXISTS `radhuntgroup` (
  `id` int(11) unsigned NOT NULL,
  `groupname` varchar(64) NOT NULL DEFAULT '',
  `nasipaddress` varchar(15) NOT NULL DEFAULT '',
  `nasportid` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `radippool`
--

DROP TABLE IF EXISTS `radippool`;
CREATE TABLE IF NOT EXISTS `radippool` (
  `id` int(11) unsigned NOT NULL,
  `pool_name` varchar(30) NOT NULL,
  `framedipaddress` varchar(15) NOT NULL DEFAULT '',
  `nasipaddress` varchar(15) NOT NULL DEFAULT '',
  `calledstationid` varchar(30) NOT NULL,
  `callingstationid` varchar(30) NOT NULL,
  `expiry_time` datetime DEFAULT NULL,
  `username` varchar(64) NOT NULL DEFAULT '',
  `pool_key` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `radpostauth`
--

DROP TABLE IF EXISTS `radpostauth`;
CREATE TABLE IF NOT EXISTS `radpostauth` (
  `id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL DEFAULT '',
  `pass` varchar(64) NOT NULL DEFAULT '',
  `reply` varchar(32) NOT NULL DEFAULT '',
  `authdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `radreply`
--

DROP TABLE IF EXISTS `radreply`;
CREATE TABLE IF NOT EXISTS `radreply` (
  `id` int(11) unsigned NOT NULL,
  `username` varchar(64) NOT NULL DEFAULT '',
  `attribute` varchar(64) NOT NULL DEFAULT '',
  `op` char(2) NOT NULL DEFAULT '=',
  `value` varchar(253) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `radusergroup`
--

DROP TABLE IF EXISTS `radusergroup`;
CREATE TABLE IF NOT EXISTS `radusergroup` (
  `id` int(11) unsigned NOT NULL,
  `username` varchar(64) NOT NULL DEFAULT '',
  `groupname` varchar(64) NOT NULL DEFAULT '',
  `priority` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `readposts`
--

DROP TABLE IF EXISTS `readposts`;
CREATE TABLE IF NOT EXISTS `readposts` (
  `id` int(10) unsigned NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `topicid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lastpostread` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `recharge`
--

DROP TABLE IF EXISTS `recharge`;
CREATE TABLE IF NOT EXISTS `recharge` (
  `id` int(10) NOT NULL,
  `users` bigint(20) unsigned NOT NULL,
  `cards` char(255) NOT NULL,
  `num` int(10) NOT NULL,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `bonus` decimal(20,8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `rechargelog`
--

DROP TABLE IF EXISTS `rechargelog`;
CREATE TABLE IF NOT EXISTS `rechargelog` (
  `id` int(10) NOT NULL,
  `userid` int(10) NOT NULL,
  `cards` varchar(255) NOT NULL,
  `time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `regimages`
--

DROP TABLE IF EXISTS `regimages`;
CREATE TABLE IF NOT EXISTS `regimages` (
  `id` mediumint(8) unsigned NOT NULL,
  `imagehash` varchar(32) NOT NULL DEFAULT '',
  `imagestring` varchar(8) NOT NULL DEFAULT '',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `reports`
--

DROP TABLE IF EXISTS `reports`;
CREATE TABLE IF NOT EXISTS `reports` (
  `id` mediumint(8) unsigned NOT NULL,
  `addedby` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reportid` int(10) unsigned NOT NULL DEFAULT '0',
  `type` enum('torrent','user','offer','request','post','comment','subtitle') NOT NULL DEFAULT 'torrent',
  `reason` varchar(255) NOT NULL DEFAULT '',
  `dealtby` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `dealtwith` tinyint(1) NOT NULL DEFAULT '0',
  `reported` mediumint(11) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `req`
--

DROP TABLE IF EXISTS `req`;
CREATE TABLE IF NOT EXISTS `req` (
  `id` int(11) NOT NULL,
  `catid` int(11) NOT NULL DEFAULT '401',
  `name` varchar(255) DEFAULT NULL,
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `introduce` text,
  `ori_introduce` text,
  `amount` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `ori_amount` int(11) NOT NULL DEFAULT '0',
  `comments` int(11) NOT NULL DEFAULT '0',
  `finish` enum('yes','no','cancel') NOT NULL DEFAULT 'no',
  `finished_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `resetdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `resreq`
--

DROP TABLE IF EXISTS `resreq`;
CREATE TABLE IF NOT EXISTS `resreq` (
  `id` int(11) NOT NULL,
  `reqid` int(11) NOT NULL DEFAULT '0',
  `torrentid` int(11) NOT NULL DEFAULT '0',
  `chosen` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `rules`
--

DROP TABLE IF EXISTS `rules`;
CREATE TABLE IF NOT EXISTS `rules` (
  `id` smallint(5) unsigned NOT NULL,
  `lang_id` smallint(5) unsigned NOT NULL DEFAULT '6',
  `title` varchar(255) NOT NULL,
  `text` text
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `rules`
--

INSERT INTO `rules` (`id`, `lang_id`, `title`, `text`) VALUES
(1, 1, '总则 - <font class=striking>不遵守这些将导致帐号被封！</font>', '[*]请不要做管理员明文禁止的事情。\r\n[*]不允许发送垃圾信息。\r\n[*]账号保留规则：\r\n    1.[b]妃 (Veteran User)[/b]及以上等级用户会永远保留；\r\n    2.[b]贵人 (Elite User)[/b]及以上等级用户封存账号（在[url=usercp.php?action=personal]控制面板[/url]）后不会被删除帐号；\r\n    3.封存账号的用户连续400天不登录将被删除帐号；\r\n    4.未封存账号的用户连续150天不登录将被删除帐号；\r\n    5.没有流量的用户（即上传/下载数据都为0）连续7天不登录或注册满30天后将被删除帐号；\r\n    6.没有达到H&R要求的。\r\n[*]一切作弊的帐号会被封，请勿心存侥幸。\r\n[*]注册多个[site]账号的用户将被禁止。\r\n[*]不要把本站的种子文件上传到其他Tracker！\r\n[*]第一次在论坛或服务器中的捣乱行为会受到警告，第二次您将永远无缘[site] 。\r\n[*]H&R规则：\r\n    1.系统[color=red]如果[/color]开启了H&R，则默认的H&R规则为：7天内需要做种至少12小时(暂定)；\r\n    2.特定规则在种子详情页会提示，并只适用于该种子；\r\n    3.下载超过种子总大小的 [color=red]10%（暂定）[/color] 后开始触发计算H&R；\r\n    4.单种分享率超过1.1时（暂定）则不计算H&R。\r\n    5.在累计得到指定数量的H&R后，帐号将会被系统自动禁用。'),
(2, 1, '违规处罚规则 - <font class=striking>违规将会失去下载权限！</font>', '[*][color=darkred]封禁[/color]\r\n    [*]上传政治敏感话题资源。\r\n    [*]被警告3次自动封禁。\r\n[*][color=red]警告[/color]\r\n    [*]群聊区刷屏、发布广告、发布垃圾、发布色情、发布政治敏感话题等信息，警告1周。\r\n    [*]发表攻击、挑动他人的言辞﹑多次恶意灌水，警告1周。\r\n    [*]发布不规范种子被删除后不做修改仍然发布不规范种子，警告1周。\r\n    [*]不尊重发布者，警告1周。\r\n    [*]在标题或正文使用脏话﹑多次发表纯表情、沙发等无意义的评论，警告1周。\r\n    [*]对为赚取积分等目的恶意上传不合格字幕的用户，或是恶意举报的用户，警告1周。\r\n    [*]任何形式向管理员挑衅、辱骂管理员，警告2周。\r\n    [*]讨论禁忌、政治敏感或当地法律禁止的话题，警告2周。\r\n    [*]发表攻击、挑动他人的言辞情节严重，警告2周。\r\n    [*]发布资源与标题简介严重不符，弄虚作假，警告一周。\r\n[*][color=darkyellow]删除[/color]\r\n    [*]发布不符合上传规则的资源。\r\n    [*]删除者请注明原因。\r\n    [*]发布不符合评论规则的评论。\r\n    [*]发布不符合字幕规则的字幕。\r\n    [*]群聊区灌水刷屏等违反群聊区规定的内容。'),
(3, 1, '论坛总则 - <font class=striking>请遵循以下的守则，违规会受到警告！</font>', '[*]禁止攻击、挑动他人的言辞。\r\n[*]禁止恶意灌水或发布垃圾信息。请勿在论坛非水区版块发布无意义主题或回复（如纯表情）等。\r\n[*]不要为了获取魔力值而大肆灌水。\r\n[*]禁止在标题或正文使用脏话。\r\n[*]不要讨论禁忌、政治敏感或当地法律禁止的话题。\r\n[*]禁止任何基于种族、国家、民族、肤色、宗教、性别、年龄、性取向、身体或精神障碍的歧视性言辞。违规将导致账号永久性禁用。\r\n[*]禁止挖坟（所有挖坟帖都要被删掉）。\r\n[*]禁止重复发帖。\r\n[*]请确保问题发布在相对应的板块。\r\n[*]365天无新回复的主题将被系统自动锁定。\r\n'),
(4, 1, '群聊区版规 - <font class=striking>请尽量遵守以下规则</font>', '[*]论坛总版规均适用于群聊区。\r\n[*]群聊区禁止刷屏。刷屏者将会被警告，刷屏内容会被删除而没有提示。\r\n[*]群聊区禁止求外站邀请码。求邀请码内容会被删除而没有提示，请去邀请交流区发帖。\r\n[*]群聊区禁止求种。求种信息会被删除而没有提示，求种请去求种区或论坛求种板块。\r\n'),
(5, 1, '上传规则 - <font class=striking> 谨记: 违规的种子将不经提醒而直接删除 </font>', '请遵守规则。如果你对规则有任何不清楚或不理解的地方，请[url=contactstaff.php]咨询管理组[/url]。[b]管理组保留裁决的权力。[/b]\r\n\r\n[b]一 上传总则[/b]\r\n    [*]上传者必须对上传的文件拥有合法的传播权。\r\n    [*]上传者必须保证上传速度与做种时间。如果在其他人完成前撤种或做种时间不足24小时，或者故意低速上传，上传者将会被警告甚至取消上传权限。\r\n    [*]对于自己发布的种子，发布者将获得双倍的上传量。\r\n    [*]如果你有一些违规但却有价值的资源，请将详细情况告知管理组，我们可能破例允许其发布。\r\n    [*]请勿夹带种子文件或者广告。尤其是水印严重的视频等。\r\n    [*]请勿违反删种规则。\r\n\r\n[b][color=blue]二  不允许的资源和文件：[/color][/b]\r\n    [*][color=darkred]违反发布规则的一律删除处理[/color]\r\n    [*][color=red]禁止发布政治敏感话题![/color]\r\n    [*]所有版块禁止发布xv/qsv等需要专属播放器才能播放的视频文件。\r\n    [*]同一资源(如同一部电影等)已有高清版本发布且非死种，将不允许发布此资源的低质量非高清版本(如rmvb格式资源)。\r\n    [*]某些分类资源高清版发布后，管理员有权力酌情删除其低质量非高清版本。剧集资源合集发布三天后删除单集资源。\r\n    [*]【电影】【蓝光】【剧集】【MV】【动漫】【纪录片】版块禁止发布flv、f4v及其相应的自转格式及rar压缩文件。\r\n    [*]【动漫】版块禁止发布猪猪字幕组资源。\r\n    [*]除【软件】【PC游戏】【掌机】【其他】其他版块禁止发布rar压缩包文件。\r\n    [*]标清视频upscale或部分upscale而成的视频文件(包括修改文件名称和格式混淆参数的)；\r\n    [*]采用分卷压缩的游戏或其他资源；\r\n    [*]属于标清级别但质量较差的视频文件，包括CAM等；\r\n    [*]完全无来源和编码信息的影片(如果是你自己压制的，请勇敢的写出自己名字，但请确保质量)；\r\n    [*]RealVideo编码的视频（通常封装于RMVB或RM）、flv文件；[游戏视频等一些特殊文件除外]\r\n    [*]单独的样片（样片请和正片一起上传）；\r\n    [*]无正确cue表单的多轨音频文件；\r\n    [*]涉及禁忌或敏感内容（如敏感政治话题等）的资源；\r\n    [*]损坏的文件，指在读取或回放过程中出现错误的文件；\r\n    [*]垃圾文件，如病毒、木马、网站链接、广告文档、种子中包含的种子文件等，或无关文件。\r\n\r\n[b]三  资源打包（并非打压缩包）规则（暂定）[/b]\r\n   原则上只允许以下资源打包：\r\n    [*]按套装售卖的高清电影合集（如The Ultimate Matrix Collection BluRay Box）；\r\n    [*]整季的电视剧/综艺节目/动漫；\r\n    [*]同一专题的纪录片；\r\n    [*]同一艺术家的MV\r\n        [*]标清MV只允许按DVD打包，且不允许单曲MV单独发布；\r\n        [*]分辨率相同的高清MV；\r\n    [*]同一艺术家的音乐\r\n        [*]两年内发售的专辑可以单独发布；\r\n        [*]打包时应剔除站内已有的资源，或者将它们都包括进来；\r\n    [*]分卷发售的动漫剧集、角色歌、广播剧等；\r\n    [*]发布组打包发布的资源。\r\n    打包发布的视频资源必须来源于相同类型的媒介（如蓝光原碟），有相同的分辨率水平（如720p），编码格式一致（如x264），但预告片例外。对于电影合集，发布组也必须统一。打包发布的音频资源必须编码格式一致（如全部为分轨FLAC）。打包发布后，将视情况删除相应单独的种子。\r\n\r\n'),
(6, 1, '评论总则 - <font class=striking>永远尊重上传者！</font>', '[*]无论如何，请尊重上传者！\r\n[*]所有不尊重或者攻击上传者的言论都将会被警告，请点击“举报”\r\n[*]所有论坛发帖的规则同样适用于评论。\r\n[*]如果你没有下载的意向，请不要随便发表否定性的评论。'),
(7, 1, '管理组成员退休待遇', '满足以下条件可获得的退休待遇：\r\n[b]对于[color=#DC143C]发布员 (Uploaders)[/color]：[/b]\r\n成为[color=#1cc6d5][b]致仕 (Retiree)[/b]：[/color]\r\n    升职一年以上；上传过200个以上的种子资源(特殊情况如原碟发布，0day更新等可以由管理组投票表决；须被认定为作出过重大及持久的贡献)。\r\n成为[color=#009F00][b]客卿 (VIP)[/b]：[/color]\r\n    升职6个月以上；上传过100个以上的种子资源(特殊情况如原碟发布，0day更新等可以由管理组投票表决)。\r\n其他：\r\n    成为[color=#F88C00][b]贵妃(Extreme User)[/b][/color](如果你的条件满足[color=#F88C00][b]贵妃(Extreme User)[/b][/color]及以上，则成为[color=#38ACEC][b]皇后(Nexus Master)[/b][/color])。\r\n\r\n[b]对于[color=#6495ED]带刀侍卫 (Moderators)[/color]：[/b]\r\n成为[color=#1cc6d5][b]致仕 (Retiree)[/b]：[/color]\r\n    升职一年以上；参加过至少2次站务组正式会议；参与过 规则/答疑 的修订工作。\r\n成为[color=#009F00][b]客卿 (VIP)[/b]：[/color]\r\n    若不满足成为[color=#1cc6d5][b]致仕 (Retiree)[/b][/color]的条件，你可以[b]无条件[/b]成为[color=#009F00][b]客卿 (VIP)[/b][/color]。\r\n\r\n[b]对于[color=#4b0082]太监总管 (Administrators)[/color]及以上等级：[/b]\r\n    可以[b]直接[/b]成为[color=#1cc6d5][b]致仕 (Retiree)[/b][/color]。\r\n'),
(8, 1, '字幕区规则 - <font class=striking>违规字幕将被删除</font>', '[b]总则：[/b]\r\n    [*]所有上传的字幕必须符合规则（即合格的）。不合格的字幕将被删除。\r\n    [*]允许上传的文件格式为srt/ssa/ass/cue/zip/rar。\r\n    [*]如果你打算上传的字幕是Vobsub格式（idx+sub）或其它格式，或者是合集（如电视剧整季的字幕），请将它们打包为zip/rar后再上传。\r\n    [*]字幕区开放音轨对应cue表单文件的上传。如有多个cue，请将它们打包起来。\r\n    [*]不允许lrc歌词或其它非字幕/cue文件的上传。上传的无关文件将被直接删除。\r\n\r\n[b]不合格字幕/cue文件判定：被判定为不合格的字幕/cue文件将被直接删除。[/b]\r\n    出现以下情况之一的字幕/cue文件将被判定为不合格：\r\n    [*]与相应种子不匹配。\r\n    [*]与相应的视频/音频文件不同步。\r\n    [*]打包错误。\r\n    [*]包含无关文件或垃圾信息。\r\n    [*]编码错误。\r\n    [*]cue文件错误。\r\n    [*]语种标识错误。\r\n    [*]标题命名不明确或包含冗余信息或字符。\r\n    [*]被判定为重复。\r\n    [*]接到多个用户举报并被证实有其它问题的。\r\n    [b]管理组保留裁定和处理不合格字幕的权力。[/b]\r\n\r\n[b]字幕奖惩：[/b]\r\n    [*]欢迎举报不合格的字幕和恶意发布不合格字幕的用户。举报不合格字幕请在字幕区点击相应字幕的“举报”按钮。举报用户请点击相应用户详细信息页面底部的“举报”按钮。\r\n    [*]对每一例不合格字幕的举报，确认后将奖励举报者50点魔力值（三天内发放）。\r\n    [*]被确定为不合格的字幕将被删除，而在每一例中，相应的字幕上传者将被扣除100点魔力值。\r\n    [*]对为赚取积分等目的恶意上传不合格字幕的用户，或是恶意举报的用户，将视情节轻重扣除额外的魔力值甚至给予警告。\r\n'),
(9, 1, '促销及置顶规则 - <font class=striking>申请促销或置顶请联系管理组</font>', '[*][color=red]下载置顶的免费热门种子并保种是提高分享率的捷径[/color]。\r\n[*][color=darkyellow]种子促销规则：[/color]\r\n    [*]如果开启随机促销后，则[color=red]随机[/color]促销（种子上传后系统自动随机设为促销）：\r\n    [*]8%的概率成为“50%”；\r\n    [*]5%的概率成为“Free”；\r\n    [*]9%的概率成为“2x”；\r\n    [*]1%的概率成为“2xFree”；\r\n    [*]7%的概率成为“2x50%”；\r\n    [*]6%的概率成为“30%”。\r\n        [*]文件总体积大于20GB的种子将自动成为“Free”。\r\n        [*]关注度高的种子将被设置为促销（由管理员定夺）。\r\n[*]我们也将不定期开启全站免费，届时尽情狂欢吧~\r\n\r\n\r\n[*][color=darkyellow]种子置顶规则：[/color]\r\n总则：置顶资源要求标题符合规则、格式，介绍详尽、准确。严格按照规范做种\r\n[*]【电影】\r\n    1.最新的电影，格式为mkv且影片质量最低为DVDRip；\r\n    2.最新的电影且IMDB评分在7.0以上；\r\n    3.优秀电影合集；\r\n    4.一些比较经典且不易获取到的电影；\r\n    5.一个月前发的种子且做种数小于3的优秀资源；\r\n符合以上任意一条的，即可置顶，置顶时间由管理员斟酌。\r\n[*]【综艺】\r\n    1.下载量高的资源、周更新资源、优秀晚会典礼、高清资源等给予置顶显示。\r\n    2.完整热门合集资源（且＞30G），由发种人申请或管理员酌情限时置顶，永久免费。\r\n    3.特别优秀、原创或者难收集到的资源，由会员申请，管理员加入置顶。\r\n    4.置顶时间：周更新资源高亮一天；优秀晚会典礼置顶3~5天，热门周更新高清资源（720P等）置顶两天；高清晚会（720P）置顶五天，永久免费。\r\n[*]【剧集】\r\n    1.置顶资源可以为DVD介质，原则上片源要求为HDTV、HD-DVD及BluRay。\r\n    2.新剧第一集以及热播新剧，画质720P以上，置顶1天。\r\n    3.画质720P以上的热播电视剧合集由管理员酌情，置顶5天。\r\n    4.经典国产电视剧合集或者IMDB评分7.0以上的合集，由管理员酌情置顶，置顶3-5天。\r\n[*]【动漫】\r\n    1.新番第一集置顶一天，新番合集置顶两天。\r\n    2.高质量经典动漫合集可申请置顶\r\n[*]【音乐】/【MV】\r\n    1.  最新专辑置顶3天\r\n    2.  经典专辑完整合集可申请置顶\r\n[*]【体育】\r\n    1.热门体育联赛的最新比赛，画质720P以上，置顶1天\r\n    2.足球、篮球以外的关注度较高的体育比赛，画质720P以上，管理员酌情置顶。\r\n    3.高清的有纪念意义的经典比赛、比赛精彩镜头剪辑及其他体育节目由管理员酌情置顶\r\n[*]【学习】\r\n    本站鼓励发布和下载学习类资源\r\n        1.学习类资源原则上均做一定促销\r\n        2.学习类资源由管理员斟酌后选择优秀有意义的置顶\r\n        3.请发布者联系管理员申请\r\n[*]【纪录片】\r\n    1.期待度较高的最新纪录片优先考虑置顶。\r\n    2.各大影视网站评分较高的优先考虑置顶。\r\n     3.内容经典：视角独特、观点新颖、内容真实、客观，较有人文关怀、主题有独到见解，深入挖掘、画面唯美、绚丽、让人赏心悦目的优先考虑置顶。\r\n[*]【软件】、【PC游戏】、【掌机】\r\n    一般不考虑置顶，特殊情况由管理员斟酌。\r\n\r\n[*]资源置顶的决定权、解释权归管理员所有，各版主有权调整置顶内容及时间。\r\n'),
(10, 1, '<span id=guize>标题命名规则</span>', '所有种子都应该有描述性的标题，必要的介绍以及其他信息。\r\n\r\n    [*][b]标题命名总则[/b]\r\n[b][color=Red][size=2][color=Sienna]\r\n    (1)不要使用全角符号标点和数字，如【 】〖 〗『 』１２３等。\r\n    (2)不要出现广告等与种子无关的信息。\r\n    (3)不要在名称中加入个人ID等信息。\r\n    (4)不要出现吸引眼球的文字，符号，噱头，个人评价等。\r\n        如“超清晰”，"经典"，"不看后悔"，"十大必看影片"，"好看", "同性"，"同志", "美女"等词汇。      \r\n    (5)不要包含"求种"，"求置顶"，“推荐”等词汇。\r\n    (6)不要出现没必要的空格。\r\n    (7)种子名称应该能够让人了解种子的内容。[/color][/color][/b]\r\n\r\n[*]注意事项\r\n    [*]管理员会根据规范对种子信息进行编辑。\r\n    [*]请勿改变或去除管理员对种子信息作出的修改（但上传者可以修正一些错误）。\r\n    [*]种子信息不符合规范的种子可能会被删除，视种子信息的规范程度而定。\r\n    [*]如果资源的原始发布信息基本符合规范，请尽量使用原始发布信息。\r\n规范命名绝对不是增加麻烦。对下载者来说，可以方便查找、下载，更好地实现资源交流。对发种者来说，完整详细命名是种子的重要介绍，有助于吸引大家下载。');

-- --------------------------------------------------------

--
-- 表的结构 `searchbox`
--

DROP TABLE IF EXISTS `searchbox`;
CREATE TABLE IF NOT EXISTS `searchbox` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `showsubcat` tinyint(1) NOT NULL DEFAULT '0',
  `showsource` tinyint(1) NOT NULL DEFAULT '0',
  `showmedium` tinyint(1) NOT NULL DEFAULT '0',
  `showcodec` tinyint(1) NOT NULL DEFAULT '0',
  `showstandard` tinyint(1) NOT NULL DEFAULT '0',
  `showprocessing` tinyint(1) NOT NULL DEFAULT '0',
  `showteam` tinyint(1) NOT NULL DEFAULT '0',
  `showaudiocodec` tinyint(1) NOT NULL DEFAULT '0',
  `catsperrow` smallint(5) unsigned NOT NULL DEFAULT '7',
  `catpadding` smallint(5) unsigned NOT NULL DEFAULT '25'
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `searchbox`
--

INSERT INTO `searchbox` (`id`, `name`, `showsubcat`, `showsource`, `showmedium`, `showcodec`, `showstandard`, `showprocessing`, `showteam`, `showaudiocodec`, `catsperrow`, `catpadding`) VALUES
(1, 'CHD', 1, 0, 0, 0, 1, 0, 1, 0, 7, 3);

-- --------------------------------------------------------

--
-- 表的结构 `seckenapi`
--

DROP TABLE IF EXISTS `seckenapi`;
CREATE TABLE IF NOT EXISTS `seckenapi` (
  `id` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL COMMENT '用户ID',
  `username` varchar(40) NOT NULL COMMENT '用户名',
  `secken_uid` varchar(64) NOT NULL COMMENT '洋葱ID'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `secondicons`
--

DROP TABLE IF EXISTS `secondicons`;
CREATE TABLE IF NOT EXISTS `secondicons` (
  `id` smallint(5) unsigned NOT NULL,
  `source` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `medium` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `codec` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `standard` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `processing` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `team` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `audiocodec` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(30) NOT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `secondicons`
--

INSERT INTO `secondicons` (`id`, `source`, `medium`, `codec`, `standard`, `processing`, `team`, `audiocodec`, `name`, `class_name`, `image`) VALUES
(1, 0, 1, 1, 0, 0, 0, 0, 'BluRay/H.264', NULL, 'bdh264.png'),
(2, 0, 1, 2, 0, 0, 0, 0, 'BluRay/VC-1', NULL, 'bdh264.png'),
(3, 0, 1, 4, 0, 0, 0, 0, 'BluRay/MPEG-2', NULL, 'bdh264.png'),
(4, 0, 2, 1, 0, 0, 0, 0, 'HD DVD/H.264', NULL, 'bdh264.png'),
(5, 0, 2, 2, 0, 0, 0, 0, 'HD DVD/VC-1', NULL, 'bdh264.png'),
(6, 0, 2, 4, 0, 0, 0, 0, 'HD DVD/MPEG-2', NULL, 'bdh264.png'),
(7, 0, 3, 1, 0, 0, 0, 0, 'Remux/H.264', NULL, 'bdh264.png'),
(8, 0, 3, 2, 0, 0, 0, 0, 'Remux/VC-1', NULL, 'bdh264.png'),
(9, 0, 3, 4, 0, 0, 0, 0, 'Remux/MPEG-2', NULL, 'bdh264.png'),
(10, 0, 4, 0, 0, 0, 0, 0, 'AVCHD', NULL, 'bdh264.png'),
(11, 0, 5, 1, 0, 0, 0, 0, 'HDTV/H.264', NULL, 'bdh264.png'),
(12, 0, 5, 4, 0, 0, 0, 0, 'HDTV/MPEG-2', NULL, 'bdh264.png'),
(13, 0, 6, 0, 0, 0, 0, 0, 'DVDR', NULL, 'bdh264.png'),
(14, 0, 7, 1, 0, 0, 0, 0, 'Rip/H.264', NULL, 'bdh264.png'),
(15, 0, 7, 3, 0, 0, 0, 0, 'Rip/Xvid', NULL, 'bdh264.png'),
(16, 0, 8, 5, 0, 0, 0, 0, 'CD/FLAC', NULL, 'bdh264.png'),
(17, 0, 8, 6, 0, 0, 0, 0, 'CD/APE', NULL, 'bdh264.png'),
(18, 0, 8, 7, 0, 0, 0, 0, 'CD/DTS', NULL, 'bdh264.png'),
(19, 0, 8, 9, 0, 0, 0, 0, 'CD/Other', NULL, 'bdh264.png'),
(20, 0, 9, 5, 0, 0, 0, 0, 'Extract/FLAC', NULL, 'bdh264.png'),
(21, 0, 9, 7, 0, 0, 0, 0, 'Extract/DTS', NULL, 'bdh264.png'),
(22, 0, 9, 8, 0, 0, 0, 0, 'Extract/AC-3', NULL, 'bdh264.png');

-- --------------------------------------------------------

--
-- 表的结构 `shoutbox`
--

DROP TABLE IF EXISTS `shoutbox`;
CREATE TABLE IF NOT EXISTS `shoutbox` (
  `id` int(10) NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `type` enum('sb','hb') NOT NULL DEFAULT 'sb',
  `ip` varbinary(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `sitelog`
--

DROP TABLE IF EXISTS `sitelog`;
CREATE TABLE IF NOT EXISTS `sitelog` (
  `id` int(10) unsigned NOT NULL,
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `txt` text NOT NULL,
  `security_level` enum('normal','mod') NOT NULL DEFAULT 'normal'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `snatched`
--

DROP TABLE IF EXISTS `snatched`;
CREATE TABLE IF NOT EXISTS `snatched` (
  `id` int(10) NOT NULL,
  `torrentid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(64) NOT NULL DEFAULT '',
  `port` smallint(5) unsigned NOT NULL DEFAULT '0',
  `uploaded` bigint(20) unsigned NOT NULL DEFAULT '0',
  `downloaded` bigint(20) unsigned NOT NULL DEFAULT '0',
  `to_go` bigint(20) unsigned NOT NULL DEFAULT '0',
  `seedtime` int(10) unsigned NOT NULL DEFAULT '0',
  `leechtime` int(10) unsigned NOT NULL DEFAULT '0',
  `last_action` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `startdat` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `completedat` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `finished` enum('yes','no') NOT NULL DEFAULT 'no',
  `hr` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `sources`
--

DROP TABLE IF EXISTS `sources`;
CREATE TABLE IF NOT EXISTS `sources` (
  `id` tinyint(3) unsigned NOT NULL,
  `lid` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `sort_index` tinyint(3) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `sources`
--

INSERT INTO `sources` (`id`, `lid`, `name`, `sort_index`) VALUES
(1, 414, '音乐-欧美', 4),
(2, 414, '音乐-华语', 3),
(3, 401, '电影-1080', 1),
(4, 401, '电影-WEB-DL', 6),
(9, 414, '音乐-古典', 6),
(8, 414, '音乐-日韩', 5),
(7, 401, '电影-枪版&预告片', 8),
(10, 414, '音乐-无损', 1),
(11, 410, '游戏-单机', 1),
(12, 410, '游戏-网络', 2),
(13, 410, '游戏-其他', 4),
(51, 406, 'MV-单集', 2),
(15, 403, '综艺-综合', 1),
(16, 403, '综艺-晚会', 3),
(17, 403, '综艺-科教', 5),
(18, 403, '综艺-娱乐', 2),
(19, 403, '综艺-典礼', 4),
(20, 407, '体育-篮球', 1),
(21, 407, '体育-足球', 2),
(22, 407, '体育-其他', 4),
(23, 402, '剧集-大陆', 1),
(24, 402, '剧集-港台', 4),
(25, 402, '剧集-美剧', 2),
(26, 402, '剧集-日剧', 5),
(27, 402, '剧集-韩剧', 3),
(29, 411, '专业学科', 2),
(30, 411, '讲座演讲', 4),
(31, 411, '期刊书籍', 5),
(32, 411, '外语学习', 1),
(50, 408, '软件-Linux', 3),
(34, 404, '纪录片-国家地理', 1),
(35, 404, '纪录片-探索频道', 4),
(37, 404, '纪录片-CCTV', 2),
(49, 414, '音乐-其他', 7),
(39, 404, '纪录片-BBC', 46),
(40, 404, '纪录片-历史频道', 5),
(41, 404, '纪录片-其他', 6),
(42, 408, '软件-Windows', 1),
(43, 408, '软件-手机', 4),
(44, 408, '软件-其他', 5),
(45, 405, '动漫-连载', 2),
(46, 405, '动漫-漫画', 4),
(47, 405, '动漫-其他', 6),
(52, 410, '游戏-掌机', 3),
(62, 409, '其他-游戏视频', 1),
(54, 423, '个人整理合集', 3),
(55, 401, '电影-原盘', 4),
(56, 423, '个人录制视频', 2),
(58, 411, '考研资料', 3),
(59, 406, 'MV-合集', 3),
(60, 406, 'MV-演唱会', 1),
(63, 409, '其他-电子文档', 2),
(66, 405, '动漫-完结', 1),
(65, 414, '音乐-原声', 2),
(67, 405, '动漫-OVA剧场版', 3),
(68, 409, '其他-其他视频', 3),
(70, 402, '剧集-其他', 7),
(71, 423, '个人原创作品', 1),
(78, 407, '体育-教学', 3),
(73, 423, '晚会活动视频', 4),
(75, 411, '其他', 6),
(76, 405, '动漫-音乐', 5),
(77, 404, '纪录片-NHK', 3),
(79, 408, '软件-Mac', 2),
(80, 424, '成人-无码', 1),
(81, 424, '成人-有码', 2),
(82, 424, '禁片-高清', 3),
(83, 424, '禁片-原盘', 4),
(84, 424, 'H漫画', 6),
(85, 424, 'H动漫', 5),
(86, 424, '写真', 7),
(87, 424, '其它', 8),
(88, 401, '电影-Remux', 5),
(89, 401, '电影-标清', 7),
(90, 402, '剧集-英剧', 6),
(91, 401, '电影-720', 2),
(92, 401, '电影-4K', 3),
(93, 401, '电影-DIY', 5);

-- --------------------------------------------------------

--
-- 表的结构 `staffmessages`
--

DROP TABLE IF EXISTS `staffmessages`;
CREATE TABLE IF NOT EXISTS `staffmessages` (
  `id` mediumint(8) unsigned NOT NULL,
  `sender` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `msg` text,
  `subject` varchar(128) NOT NULL DEFAULT '',
  `answeredby` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `answered` tinyint(1) NOT NULL DEFAULT '0',
  `answer` text,
  `goto` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `standards`
--

DROP TABLE IF EXISTS `standards`;
CREATE TABLE IF NOT EXISTS `standards` (
  `id` tinyint(3) unsigned NOT NULL,
  `lid` tinyint(3) NOT NULL,
  `name` varchar(30) NOT NULL,
  `sort_index` tinyint(3) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `standards`
--

INSERT INTO `standards` (`id`, `lid`, `name`, `sort_index`) VALUES
(1, 0, '1080', 0),
(2, 0, '720', 0),
(3, 0, '560', 0),
(4, 0, '480', 0),
(5, 0, '4K', 0),
(6, 0, 'Other', 0);

-- --------------------------------------------------------

--
-- 表的结构 `stylesheets`
--

DROP TABLE IF EXISTS `stylesheets`;
CREATE TABLE IF NOT EXISTS `stylesheets` (
  `id` tinyint(3) unsigned NOT NULL,
  `uri` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(64) NOT NULL DEFAULT '',
  `addicode` text,
  `designer` varchar(50) NOT NULL DEFAULT '',
  `comment` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `stylesheets`
--

INSERT INTO `stylesheets` (`id`, `uri`, `name`, `addicode`, `designer`, `comment`) VALUES
(1, 'styles/default/', '默认', '', 'HDCN', 'HDCN'),
(2, 'styles/black/', '黑色', '', 'HDCN', 'HDCN'),
(3, 'styles/green/', '绿色', '', 'HDCN', 'HDCN');

-- --------------------------------------------------------

--
-- 表的结构 `subs`
--

DROP TABLE IF EXISTS `subs`;
CREATE TABLE IF NOT EXISTS `subs` (
  `id` int(10) unsigned NOT NULL,
  `torrent_id` mediumint(8) unsigned NOT NULL,
  `lang_id` smallint(5) unsigned NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `filename` varchar(255) NOT NULL DEFAULT '',
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0',
  `uppedby` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `anonymous` enum('yes','no') NOT NULL DEFAULT 'no',
  `hits` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ext` varchar(10) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `suggest`
--

DROP TABLE IF EXISTS `suggest`;
CREATE TABLE IF NOT EXISTS `suggest` (
  `id` int(10) unsigned NOT NULL,
  `keywords` varchar(255) NOT NULL DEFAULT '',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `adddate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `sysoppanel`
--

DROP TABLE IF EXISTS `sysoppanel`;
CREATE TABLE IF NOT EXISTS `sysoppanel` (
  `id` tinyint(3) unsigned NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `info` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `teams`
--

DROP TABLE IF EXISTS `teams`;
CREATE TABLE IF NOT EXISTS `teams` (
  `id` tinyint(3) unsigned NOT NULL,
  `lid` tinyint(3) NOT NULL,
  `name` varchar(30) NOT NULL,
  `sort_index` tinyint(3) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `teams`
--

INSERT INTO `teams` (`id`, `lid`, `name`, `sort_index`) VALUES
(1, 0, 'HDCN', 0),
(2, 0, 'CHD', 0),
(3, 0, 'TTG', 0),
(4, 0, 'CMCT', 0),
(5, 0, 'HDWinG', 0),
(6, 0, 'beAst', 0),
(7, 0, 'Other', 0);

-- --------------------------------------------------------

--
-- 表的结构 `thanks`
--

DROP TABLE IF EXISTS `thanks`;
CREATE TABLE IF NOT EXISTS `thanks` (
  `id` int(10) unsigned NOT NULL,
  `torrentid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `bonus` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `topics`
--

DROP TABLE IF EXISTS `topics`;
CREATE TABLE IF NOT EXISTS `topics` (
  `id` mediumint(8) unsigned NOT NULL,
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(128) NOT NULL,
  `locked` enum('yes','no') NOT NULL DEFAULT 'no',
  `forumid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `firstpost` int(10) unsigned NOT NULL DEFAULT '0',
  `lastpost` int(10) unsigned NOT NULL DEFAULT '0',
  `sticky` enum('no','yes') NOT NULL DEFAULT 'no',
  `hlcolor` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `views` int(10) unsigned NOT NULL DEFAULT '0',
  `onlyauthor` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `torrents`
--

DROP TABLE IF EXISTS `torrents`;
CREATE TABLE IF NOT EXISTS `torrents` (
  `id` mediumint(8) unsigned NOT NULL,
  `info_hash` binary(20) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `filename` varchar(255) NOT NULL DEFAULT '',
  `save_as` varchar(255) NOT NULL DEFAULT '',
  `descr` mediumtext,
  `small_descr` varchar(255) NOT NULL DEFAULT '',
  `ori_descr` mediumtext,
  `category` smallint(5) unsigned NOT NULL DEFAULT '0',
  `source` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `medium` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `codec` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `standard` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `processing` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `team` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `audiocodec` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0',
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` enum('single','multi') NOT NULL DEFAULT 'single',
  `numfiles` smallint(5) unsigned NOT NULL DEFAULT '0',
  `comments` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `views` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `times_completed` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `leechers` mediumint(8) NOT NULL DEFAULT '0',
  `seeders` mediumint(8) NOT NULL DEFAULT '0',
  `last_action` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `visible` enum('yes','no') NOT NULL DEFAULT 'yes',
  `banned` enum('yes','no') NOT NULL DEFAULT 'no',
  `owner` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `nfo` blob,
  `sp_state` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `promotion_time_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `promotion_until` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `anonymous` enum('yes','no') NOT NULL DEFAULT 'no',
  `url` int(10) unsigned NOT NULL,
  `dburl` int(10) unsigned NOT NULL,
  `pos_state` enum('normal','sticky') NOT NULL DEFAULT 'normal',
  `cache_stamp` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `picktype` enum('hot','classic','recommended','normal') NOT NULL DEFAULT 'normal',
  `picktime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_reseed` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `endfree` datetime NOT NULL,
  `endsticky` datetime NOT NULL,
  `status` enum('normal','recycle','candidate') NOT NULL DEFAULT 'normal',
  `last_status` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hr_state` enum('normal','sticky','no') NOT NULL DEFAULT 'normal',
  `hr_time` int(10) unsigned NOT NULL DEFAULT '7200',
  `hr_until` int(10) unsigned NOT NULL DEFAULT '604800',
  `first` enum('yes','no') NOT NULL DEFAULT 'no',
  `excl` enum('yes','no') NOT NULL DEFAULT 'no',
  `bumptime` enum('yes','no') NOT NULL DEFAULT 'no',
  `limitd` enum('yes','no') NOT NULL DEFAULT 'no',
  `official` enum('yes','no') NOT NULL DEFAULT 'no',
  `marrow` enum('normal','marrow1','marrow2','marrow3') NOT NULL DEFAULT 'normal',
  `endmarrow` datetime NOT NULL,
  `namecolor` varchar(7) NOT NULL DEFAULT '#000000',
  `descrcolor` varchar(7) NOT NULL DEFAULT '#000000',
  `albumid` int(10) unsigned NOT NULL,
  `bumpoff` enum('yes','no') NOT NULL DEFAULT 'no',
  `releasedate` tinytext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `torrents_state`
--

DROP TABLE IF EXISTS `torrents_state`;
CREATE TABLE IF NOT EXISTS `torrents_state` (
  `global_sp_state` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `global_endfree` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `torrents_state`
--

INSERT INTO `torrents_state` (`global_sp_state`, `global_endfree`) VALUES
(1, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `truckmarks`
--

DROP TABLE IF EXISTS `truckmarks`;
CREATE TABLE IF NOT EXISTS `truckmarks` (
  `id` int(10) unsigned NOT NULL,
  `torrentid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `added` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `udlog`
--

DROP TABLE IF EXISTS `udlog`;
CREATE TABLE IF NOT EXISTS `udlog` (
  `id` bigint(20) unsigned NOT NULL,
  `userid` bigint(20) unsigned NOT NULL,
  `class` int(10) unsigned NOT NULL,
  `invited_by` bigint(20) unsigned NOT NULL,
  `uploaded` bigint(20) unsigned NOT NULL,
  `downloaded` bigint(20) unsigned NOT NULL,
  `seedtime` bigint(20) unsigned NOT NULL,
  `leechtime` bigint(20) unsigned NOT NULL,
  `seedbonus` decimal(20,8) NOT NULL,
  `date` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `uploadspeed`
--

DROP TABLE IF EXISTS `uploadspeed`;
CREATE TABLE IF NOT EXISTS `uploadspeed` (
  `id` tinyint(3) unsigned NOT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `uploadspeed`
--

INSERT INTO `uploadspeed` (`id`, `name`) VALUES
(4, '512Kbps'),
(5, '1Mbps'),
(6, '2Mbps'),
(7, '3Mbps'),
(8, '4Mbps'),
(9, '5Mbps'),
(10, '6Mbps'),
(12, '10Mbps'),
(14, '12Mbps'),
(16, '20Mbps'),
(17, '50Mbps'),
(18, '100Mbps');

-- --------------------------------------------------------

--
-- 表的结构 `userinfo`
--

DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE IF NOT EXISTS `userinfo` (
  `id` int(11) unsigned NOT NULL,
  `username` varchar(128) DEFAULT NULL,
  `firstname` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `creationdate` datetime DEFAULT '0000-00-00 00:00:00',
  `creationby` varchar(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `username` varchar(40) NOT NULL DEFAULT '',
  `passhash` varchar(32) NOT NULL DEFAULT '',
  `secret` varbinary(20) NOT NULL,
  `email` varchar(80) NOT NULL DEFAULT '',
  `status` enum('pending','confirmed') NOT NULL DEFAULT 'pending',
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_access` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_home` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_offer` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `forum_access` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_staffmsg` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_pm` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_comment` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_post` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_browse` int(10) unsigned NOT NULL DEFAULT '0',
  `limit_last_browse` int(10) unsigned NOT NULL DEFAULT '0',
  `last_music` int(10) unsigned NOT NULL DEFAULT '0',
  `last_catchup` int(10) unsigned NOT NULL DEFAULT '0',
  `editsecret` varbinary(20) NOT NULL,
  `privacy` enum('strong','normal','low') NOT NULL DEFAULT 'normal',
  `stylesheet` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `caticon` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fontsize` enum('small','medium','large') NOT NULL DEFAULT 'medium',
  `info` text,
  `acceptpms` enum('yes','friends','no') NOT NULL DEFAULT 'yes',
  `commentpm` enum('yes','no') NOT NULL DEFAULT 'yes',
  `ip` varbinary(64) NOT NULL DEFAULT '',
  `class` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `max_class_once` tinyint(3) NOT NULL DEFAULT '1',
  `avatar` varchar(255) NOT NULL DEFAULT '',
  `uploaded` bigint(20) unsigned NOT NULL DEFAULT '0',
  `downloaded` bigint(20) unsigned NOT NULL DEFAULT '0',
  `seedtime` bigint(20) unsigned NOT NULL DEFAULT '0',
  `leechtime` bigint(20) unsigned NOT NULL DEFAULT '0',
  `title` varchar(30) NOT NULL DEFAULT '',
  `country` smallint(5) unsigned NOT NULL DEFAULT '107',
  `notifs` varchar(500) DEFAULT '[incldead=0]',
  `modcomment` text,
  `enabled` enum('yes','no') NOT NULL DEFAULT 'yes',
  `avatars` enum('yes','no') NOT NULL DEFAULT 'yes',
  `donor` enum('yes','no') NOT NULL DEFAULT 'no',
  `donated` decimal(8,2) NOT NULL DEFAULT '0.00',
  `donated_cny` decimal(8,2) NOT NULL DEFAULT '0.00',
  `donoruntil` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `warned` enum('yes','no') NOT NULL DEFAULT 'no',
  `warneduntil` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `noad` enum('yes','no') NOT NULL DEFAULT 'no',
  `noaduntil` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `torrentsperpage` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `topicsperpage` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `postsperpage` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `clicktopic` enum('firstpage','lastpage') NOT NULL DEFAULT 'firstpage',
  `deletepms` enum('yes','no') NOT NULL DEFAULT 'yes',
  `savepms` enum('yes','no') NOT NULL DEFAULT 'no',
  `showhot` enum('yes','no') NOT NULL DEFAULT 'yes',
  `showclassic` enum('yes','no') NOT NULL DEFAULT 'yes',
  `support` enum('yes','no') NOT NULL DEFAULT 'no',
  `picker` enum('yes','no') NOT NULL DEFAULT 'no',
  `stafffor` varchar(255) NOT NULL,
  `supportfor` varchar(255) NOT NULL,
  `pickfor` varchar(255) NOT NULL,
  `supportlang` varchar(50) NOT NULL,
  `passkey` varchar(32) NOT NULL DEFAULT '',
  `promotion_link` varchar(32) DEFAULT NULL,
  `uploadpos` enum('yes','no') NOT NULL DEFAULT 'yes',
  `forumpost` enum('yes','no') NOT NULL DEFAULT 'yes',
  `downloadpos` enum('yes','no') NOT NULL DEFAULT 'yes',
  `clientselect` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `signatures` enum('yes','no') NOT NULL DEFAULT 'yes',
  `signature` varchar(800) NOT NULL DEFAULT '',
  `lang` smallint(5) unsigned NOT NULL DEFAULT '6',
  `cheat` smallint(6) NOT NULL DEFAULT '0',
  `download` int(10) unsigned NOT NULL DEFAULT '0',
  `upload` int(10) unsigned NOT NULL DEFAULT '0',
  `isp` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `invites` bigint(20) NOT NULL DEFAULT '0',
  `invited_by` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `gender` enum('Male','Female','N/A') NOT NULL DEFAULT 'N/A',
  `vip_added` enum('yes','no') NOT NULL DEFAULT 'no',
  `vip_until` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `seedbonus` decimal(20,8) NOT NULL DEFAULT '0.00000000',
  `charity` decimal(10,1) NOT NULL DEFAULT '0.0',
  `bonuscomment` text,
  `parked` enum('yes','no') NOT NULL DEFAULT 'no',
  `leechwarn` enum('yes','no') NOT NULL DEFAULT 'no',
  `leechwarnuntil` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastwarned` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `timeswarned` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `warnedby` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `sbnum` smallint(5) unsigned NOT NULL DEFAULT '70',
  `sbrefresh` smallint(5) unsigned NOT NULL DEFAULT '120',
  `hidehb` enum('yes','no') DEFAULT 'no',
  `showimdb` enum('yes','no') DEFAULT 'yes',
  `showdescription` enum('yes','no') DEFAULT 'yes',
  `showcomment` enum('yes','no') DEFAULT 'yes',
  `showclienterror` enum('yes','no') NOT NULL DEFAULT 'no',
  `showdlnotice` tinyint(1) NOT NULL DEFAULT '1',
  `tooltip` enum('minorimdb','medianimdb','off') NOT NULL DEFAULT 'off',
  `shownfo` enum('yes','no') DEFAULT 'yes',
  `timetype` enum('timeadded','timealive') DEFAULT 'timealive',
  `appendsticky` enum('yes','no') DEFAULT 'yes',
  `appendnew` enum('yes','no') DEFAULT 'yes',
  `appendpromotion` enum('highlight','word','icon','off') DEFAULT 'icon',
  `appendpicked` enum('yes','no') DEFAULT 'yes',
  `dlicon` enum('yes','no') DEFAULT 'yes',
  `bmicon` enum('yes','no') DEFAULT 'yes',
  `showsmalldescr` enum('yes','no') NOT NULL DEFAULT 'yes',
  `showcomnum` enum('yes','no') DEFAULT 'yes',
  `showlastcom` enum('yes','no') DEFAULT 'no',
  `showlastpost` enum('yes','no') NOT NULL DEFAULT 'no',
  `pmnum` tinyint(3) unsigned NOT NULL DEFAULT '10',
  `school` smallint(5) unsigned NOT NULL DEFAULT '35',
  `showfb` enum('yes','no') NOT NULL DEFAULT 'yes',
  `cardnum` varchar(14) NOT NULL,
  `salarynum` int(11) NOT NULL,
  `salary` date NOT NULL,
  `stealtime` time DEFAULT NULL,
  `stealstatus` smallint(11) DEFAULT '0',
  `hr` int(11) NOT NULL DEFAULT '0',
  `newuser` enum('yes','no') NOT NULL DEFAULT 'yes',
  `bjwins` int(10) NOT NULL DEFAULT '0',
  `bjlosses` int(10) NOT NULL DEFAULT '0',
  `exuploaded` bigint(20) unsigned NOT NULL DEFAULT '0',
  `exdownloaded` bigint(20) unsigned NOT NULL DEFAULT '0',
  `exseedtime` bigint(20) unsigned NOT NULL DEFAULT '0',
  `exleechtime` bigint(20) unsigned NOT NULL DEFAULT '0',
  `exseedbonus` decimal(20,8) NOT NULL DEFAULT '0.00000000',
  `exclass` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `asspass` enum('yes','no') NOT NULL DEFAULT 'no',
  `nonass` enum('yes','no') NOT NULL DEFAULT 'no',
  `extorrent` int(10) unsigned NOT NULL,
  `limitd` enum('yes','no') NOT NULL DEFAULT 'no',
  `onlimit` enum('yes','no') NOT NULL DEFAULT 'no',
  `onlimitdate` datetime NOT NULL,
  `big` bigint(20) unsigned NOT NULL DEFAULT '0',
  `anon` enum('yes','no') NOT NULL DEFAULT 'no',
  `clearnew` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `danmu` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `secken` enum('yes','no') NOT NULL DEFAULT 'no',
  `page` varchar(20) NOT NULL,
  `tovip` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `seclogin` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `users`
--

INSERT INTO `users` (`id`, `username`, `passhash`, `secret`, `email`, `status`, `added`, `last_login`, `last_access`, `last_home`, `last_offer`, `forum_access`, `last_staffmsg`, `last_pm`, `last_comment`, `last_post`, `last_browse`, `limit_last_browse`, `last_music`, `last_catchup`, `editsecret`, `privacy`, `stylesheet`, `caticon`, `fontsize`, `info`, `acceptpms`, `commentpm`, `ip`, `class`, `max_class_once`, `avatar`, `uploaded`, `downloaded`, `seedtime`, `leechtime`, `title`, `country`, `notifs`, `modcomment`, `enabled`, `avatars`, `donor`, `donated`, `donated_cny`, `donoruntil`, `warned`, `warneduntil`, `noad`, `noaduntil`, `torrentsperpage`, `topicsperpage`, `postsperpage`, `clicktopic`, `deletepms`, `savepms`, `showhot`, `showclassic`, `support`, `picker`, `stafffor`, `supportfor`, `pickfor`, `supportlang`, `passkey`, `promotion_link`, `uploadpos`, `forumpost`, `downloadpos`, `clientselect`, `signatures`, `signature`, `lang`, `cheat`, `download`, `upload`, `isp`, `invites`, `invited_by`, `gender`, `vip_added`, `vip_until`, `seedbonus`, `charity`, `bonuscomment`, `parked`, `leechwarn`, `leechwarnuntil`, `lastwarned`, `timeswarned`, `warnedby`, `sbnum`, `sbrefresh`, `hidehb`, `showimdb`, `showdescription`, `showcomment`, `showclienterror`, `showdlnotice`, `tooltip`, `shownfo`, `timetype`, `appendsticky`, `appendnew`, `appendpromotion`, `appendpicked`, `dlicon`, `bmicon`, `showsmalldescr`, `showcomnum`, `showlastcom`, `showlastpost`, `pmnum`, `school`, `showfb`, `cardnum`, `salarynum`, `salary`, `stealtime`, `stealstatus`, `hr`, `newuser`, `bjwins`, `bjlosses`, `exuploaded`, `exdownloaded`, `exseedtime`, `exleechtime`, `exseedbonus`, `exclass`, `asspass`, `nonass`, `extorrent`, `onlimit`, `onlimitdate`, `big`, `anon`, `clearnew`, `danmu`, `secken`, `page`, `tovip`, `seclogin`) VALUES
(1, 'admin', '9f7ea982527d005b98ad84e96cc57044', 0x7268646a666f7874726a6b737865707067786676, 'hdcninfo@gmail.com', 'confirmed', '2014-05-01 00:00:00', '0000-00-00 00:00:00', '2014-05-01 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, '', 'normal', 1, 1, 'medium', NULL, 'yes', 'yes', '', 18, 1, '', 0, 0, 0, 0, '', 0, '[incldead=0]', NULL, 'yes', 'yes', 'no', '0.00', '0.00', '0000-00-00 00:00:00', 'no', '0000-00-00 00:00:00', 'no', '0000-00-00 00:00:00', 0, 0, 0, 'firstpage', 'yes', 'no', 'yes', 'yes', 'no', 'no', '', '', '', '', '', NULL, 'yes', 'yes', 'yes', 0, 'yes', '', 1, 0, 0, 0, 0, 0, 0, 'N/A', 'no', '0000-00-00 00:00:00', '0.00000000', '0.0', NULL, 'no', 'no', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 70, 120, 'no', 'yes', 'yes', 'yes', 'no', 1, 'off', 'yes', 'timealive', 'yes', 'yes', 'icon', 'yes', 'yes', 'yes', 'yes', 'yes', 'no', 'no', 10, 35, 'yes', '', 0, '0000-00-00', NULL, 0, 0, 'yes', 0, 0, 0, 0, 0, 0, '0.00000000', 18, 'no', 'no', 0, 'no', '0000-00-00 00:00:00', 0, 'yes', 0, 0, 'no', '', 18, 'no'),
(2, 'robot', '53102bd0f500773c9b6295152d837081', 0x6568667469656b696d706f6e65676f656b686b75, 'admin@shenzhan.in', 'confirmed', '2014-05-01 00:00:00', '0000-00-00 00:00:00', '2014-05-01 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, '', 'normal', 1, 1, 'medium', NULL, 'yes', 'yes', '', 11, 1, '', 0, 0, 0, 0, '', 0, '[incldead=0]', NULL, 'yes', 'yes', 'no', '0.00', '0.00', '0000-00-00 00:00:00', 'no', '0000-00-00 00:00:00', 'no', '0000-00-00 00:00:00', 0, 0, 0, 'firstpage', 'yes', 'no', 'yes', 'yes', 'no', 'no', '', '', '', '', '', NULL, 'yes', 'yes', 'yes', 0, 'yes', '', 1, 0, 0, 0, 0, 0, 0, 'N/A', 'no', '0000-00-00 00:00:00', '0.00000000', '0.0', NULL, 'no', 'no', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 70, 120, 'no', 'yes', 'yes', 'yes', 'no', 1, 'off', 'yes', 'timealive', 'yes', 'yes', 'icon', 'yes', 'yes', 'yes', 'yes', 'yes', 'no', 'no', 10, 35, 'yes', '', 0, '0000-00-00', NULL, 0, 0, 'yes', 0, 0, 0, 0, 0, 0, '0.00000000', 11, 'no', 'no', 0, 'no', '0000-00-00 00:00:00', 0, 'no', 0, 1, 'no', '', 11, 'no');

-- --------------------------------------------------------

--
-- 表的结构 `users_log`
--

DROP TABLE IF EXISTS `users_log`;
CREATE TABLE IF NOT EXISTS `users_log` (
  `id` int(11) NOT NULL,
  `op_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(40) NOT NULL,
  `op` varchar(20) NOT NULL,
  `detail` text NOT NULL,
  `op_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `wimax`
--

DROP TABLE IF EXISTS `wimax`;
CREATE TABLE IF NOT EXISTS `wimax` (
  `id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL DEFAULT '',
  `authdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `spi` varchar(16) NOT NULL DEFAULT '',
  `mipkey` varchar(400) NOT NULL DEFAULT '',
  `lifetime` int(12) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `2048`
--
ALTER TABLE `2048`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `2048_rank`
--
ALTER TABLE `2048_rank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adclicks`
--
ALTER TABLE `adclicks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adminpanel`
--
ALTER TABLE `adminpanel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advertisements`
--
ALTER TABLE `advertisements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agent_allowed_exception`
--
ALTER TABLE `agent_allowed_exception`
  ADD KEY `family_id` (`family_id`);

--
-- Indexes for table `agent_allowed_family`
--
ALTER TABLE `agent_allowed_family`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD FULLTEXT KEY `name` (`name`);
ALTER TABLE `album`
  ADD FULLTEXT KEY `small_descr` (`small_descr`);

--
-- Indexes for table `albumfav`
--
ALTER TABLE `albumfav`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `albumseries`
--
ALTER TABLE `albumseries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`albumid`,`torrentid`);

--
-- Indexes for table `allowedemails`
--
ALTER TABLE `allowedemails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pid` (`userid`,`id`),
  ADD KEY `dateline` (`added`,`isimage`,`downloads`),
  ADD KEY `dlkey` (`dlkey`);

--
-- Indexes for table `audiocodecs`
--
ALTER TABLE `audiocodecs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `avps`
--
ALTER TABLE `avps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bannedemails`
--
ALTER TABLE `bannedemails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bans`
--
ALTER TABLE `bans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `first_last` (`first`,`last`);

--
-- Indexes for table `betgames`
--
ALTER TABLE `betgames`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `betlog`
--
ALTER TABLE `betlog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `userid_2` (`userid`,`bonus`);

--
-- Indexes for table `betoptions`
--
ALTER TABLE `betoptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gameid` (`gameid`);

--
-- Indexes for table `bets`
--
ALTER TABLE `bets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gameid` (`gameid`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `bettop`
--
ALTER TABLE `bettop`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `bitbucket`
--
ALTER TABLE `bitbucket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blackjack`
--
ALTER TABLE `blackjack`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userfriend` (`userid`,`blockid`);

--
-- Indexes for table `blue`
--
ALTER TABLE `blue`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `torrentid_id` (`torrentid`,`id`),
  ADD KEY `torrentid_userid` (`torrentid`,`userid`);

--
-- Indexes for table `bonusapp`
--
ALTER TABLE `bonusapp`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `bookmarks`
--
ALTER TABLE `bookmarks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid_torrentid` (`userid`,`torrentid`);

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mode_sort` (`mode`,`sort_index`);

--
-- Indexes for table `caticons`
--
ALTER TABLE `caticons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cheaters`
--
ALTER TABLE `cheaters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chronicle`
--
ALTER TABLE `chronicle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `added` (`added`);

--
-- Indexes for table `claim`
--
ALTER TABLE `claim`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`,`torrentid`);

--
-- Indexes for table `codecs`
--
ALTER TABLE `codecs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `torrent_id` (`torrent`,`id`),
  ADD KEY `offer_id` (`offer`,`id`),
  ADD FULLTEXT KEY `text` (`text`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cui`
--
ALTER TABLE `cui`
  ADD PRIMARY KEY (`username`,`clientipaddress`,`callingstationid`);

--
-- Indexes for table `downloadspeed`
--
ALTER TABLE `downloadspeed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drawlottery`
--
ALTER TABLE `drawlottery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `torrent` (`torrent`);

--
-- Indexes for table `forummods`
--
ALTER TABLE `forummods`
  ADD PRIMARY KEY (`id`),
  ADD KEY `forumid` (`forumid`);

--
-- Indexes for table `forums`
--
ALTER TABLE `forums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userfriend` (`userid`,`friendid`);

--
-- Indexes for table `fun`
--
ALTER TABLE `fun`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `funds`
--
ALTER TABLE `funds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `funvotes`
--
ALTER TABLE `funvotes`
  ADD PRIMARY KEY (`funid`,`userid`);

--
-- Indexes for table `gift`
--
ALTER TABLE `gift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `giftlog`
--
ALTER TABLE `giftlog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `givebonus`
--
ALTER TABLE `givebonus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invitebox`
--
ALTER TABLE `invitebox`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `invites`
--
ALTER TABLE `invites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hash` (`hash`(3));

--
-- Indexes for table `iplog`
--
ALTER TABLE `iplog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `isp`
--
ALTER TABLE `isp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `limitinvite`
--
ALTER TABLE `limitinvite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `limitsuggest`
--
ALTER TABLE `limitsuggest`
  ADD PRIMARY KEY (`id`),
  ADD KEY `keywords` (`keywords`(4)),
  ADD KEY `adddate` (`adddate`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loginattempts`
--
ALTER TABLE `loginattempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lottery`
--
ALTER TABLE `lottery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lotteryowner` (`ownerid`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `receiver` (`receiver`),
  ADD KEY `sender` (`sender`);

--
-- Indexes for table `modpanel`
--
ALTER TABLE `modpanel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nas`
--
ALTER TABLE `nas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nasname` (`nasname`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `added` (`added`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notice` (`notice`(333));

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `offervotes`
--
ALTER TABLE `offervotes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `overforums`
--
ALTER TABLE `overforums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peers`
--
ALTER TABLE `peers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `torrent` (`torrent`);

--
-- Indexes for table `pmboxes`
--
ALTER TABLE `pmboxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pollanswers`
--
ALTER TABLE `pollanswers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pollid` (`pollid`),
  ADD KEY `selection` (`selection`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `polls`
--
ALTER TABLE `polls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `topicid_id` (`topicid`,`id`),
  ADD KEY `added` (`added`),
  ADD FULLTEXT KEY `body` (`body`);

--
-- Indexes for table `processings`
--
ALTER TABLE `processings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prolinkclicks`
--
ALTER TABLE `prolinkclicks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `radacct`
--
ALTER TABLE `radacct`
  ADD PRIMARY KEY (`radacctid`),
  ADD KEY `username` (`username`),
  ADD KEY `framedipaddress` (`framedipaddress`),
  ADD KEY `acctsessionid` (`acctsessionid`),
  ADD KEY `acctsessiontime` (`acctsessiontime`),
  ADD KEY `acctuniqueid` (`acctuniqueid`),
  ADD KEY `acctstarttime` (`acctstarttime`),
  ADD KEY `acctstoptime` (`acctstoptime`),
  ADD KEY `nasipaddress` (`nasipaddress`);

--
-- Indexes for table `radcheck`
--
ALTER TABLE `radcheck`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`(32));

--
-- Indexes for table `radgroupcheck`
--
ALTER TABLE `radgroupcheck`
  ADD PRIMARY KEY (`id`),
  ADD KEY `groupname` (`groupname`(32));

--
-- Indexes for table `radgroupreply`
--
ALTER TABLE `radgroupreply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `groupname` (`groupname`(32));

--
-- Indexes for table `radhuntgroup`
--
ALTER TABLE `radhuntgroup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nasipaddress` (`nasipaddress`);

--
-- Indexes for table `radippool`
--
ALTER TABLE `radippool`
  ADD PRIMARY KEY (`id`),
  ADD KEY `radippool_poolname_expire` (`pool_name`,`expiry_time`),
  ADD KEY `framedipaddress` (`framedipaddress`),
  ADD KEY `radippool_nasip_poolkey_ipaddress` (`nasipaddress`,`pool_key`,`framedipaddress`);

--
-- Indexes for table `radpostauth`
--
ALTER TABLE `radpostauth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `radreply`
--
ALTER TABLE `radreply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`(32));

--
-- Indexes for table `radusergroup`
--
ALTER TABLE `radusergroup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`(32));

--
-- Indexes for table `readposts`
--
ALTER TABLE `readposts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `topicid` (`topicid`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `recharge`
--
ALTER TABLE `recharge`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cards` (`cards`);

--
-- Indexes for table `rechargelog`
--
ALTER TABLE `rechargelog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cards` (`cards`);

--
-- Indexes for table `regimages`
--
ALTER TABLE `regimages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `req`
--
ALTER TABLE `req`
  ADD PRIMARY KEY (`id`),
  ADD KEY `finish` (`finish`,`name`,`added`,`amount`,`introduce`(10));

--
-- Indexes for table `resreq`
--
ALTER TABLE `resreq`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reqid` (`reqid`,`chosen`);

--
-- Indexes for table `rules`
--
ALTER TABLE `rules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `searchbox`
--
ALTER TABLE `searchbox`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seckenapi`
--
ALTER TABLE `seckenapi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `secondicons`
--
ALTER TABLE `secondicons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shoutbox`
--
ALTER TABLE `shoutbox`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sitelog`
--
ALTER TABLE `sitelog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `added` (`added`);

--
-- Indexes for table `snatched`
--
ALTER TABLE `snatched`
  ADD PRIMARY KEY (`id`),
  ADD KEY `torrentid_userid` (`torrentid`,`userid`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `sources`
--
ALTER TABLE `sources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staffmessages`
--
ALTER TABLE `staffmessages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `standards`
--
ALTER TABLE `standards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stylesheets`
--
ALTER TABLE `stylesheets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subs`
--
ALTER TABLE `subs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `torrentid_langid` (`torrent_id`,`lang_id`);

--
-- Indexes for table `suggest`
--
ALTER TABLE `suggest`
  ADD PRIMARY KEY (`id`),
  ADD KEY `keywords` (`keywords`(4)),
  ADD KEY `adddate` (`adddate`);

--
-- Indexes for table `sysoppanel`
--
ALTER TABLE `sysoppanel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `thanks`
--
ALTER TABLE `thanks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `torrentid_id` (`torrentid`,`id`),
  ADD KEY `torrentid_userid` (`torrentid`,`userid`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `subject` (`subject`),
  ADD KEY `forumid_lastpost` (`forumid`,`lastpost`),
  ADD KEY `forumid_sticky_lastpost` (`forumid`,`sticky`,`lastpost`);

--
-- Indexes for table `torrents`
--
ALTER TABLE `torrents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `info_hash` (`info_hash`),
  ADD KEY `owner` (`owner`),
  ADD KEY `visible_pos_id` (`visible`,`pos_state`,`id`),
  ADD KEY `url` (`url`),
  ADD KEY `category_visible_banned` (`category`,`visible`,`banned`),
  ADD KEY `visible_banned_pos_id` (`visible`,`banned`,`pos_state`,`id`),
  ADD KEY `endfree` (`endfree`),
  ADD KEY `endsticky` (`endsticky`),
  ADD KEY `pos_state` (`pos_state`),
  ADD KEY `sp_state` (`sp_state`),
  ADD KEY `marrow` (`marrow`),
  ADD KEY `endmarrow` (`endmarrow`),
  ADD FULLTEXT KEY `name` (`name`);
ALTER TABLE `torrents`
  ADD FULLTEXT KEY `descr` (`descr`);

--
-- Indexes for table `torrents_state`
--
ALTER TABLE `torrents_state`
  ADD UNIQUE KEY `global_sp_state` (`global_sp_state`);

--
-- Indexes for table `truckmarks`
--
ALTER TABLE `truckmarks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid_torrentid` (`userid`,`torrentid`);

--
-- Indexes for table `udlog`
--
ALTER TABLE `udlog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`,`invited_by`);

--
-- Indexes for table `uploadspeed`
--
ALTER TABLE `uploadspeed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userinfo`
--
ALTER TABLE `userinfo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `status_added` (`status`,`added`),
  ADD KEY `ip` (`ip`),
  ADD KEY `uploaded` (`uploaded`),
  ADD KEY `downloaded` (`downloaded`),
  ADD KEY `country` (`country`),
  ADD KEY `last_access` (`last_access`),
  ADD KEY `enabled` (`enabled`),
  ADD KEY `warned` (`warned`),
  ADD KEY `cheat` (`cheat`),
  ADD KEY `class` (`class`),
  ADD KEY `passkey` (`passkey`(8));

--
-- Indexes for table `users_log`
--
ALTER TABLE `users_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wimax`
--
ALTER TABLE `wimax`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`),
  ADD KEY `spi` (`spi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `2048`
--
ALTER TABLE `2048`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `2048_rank`
--
ALTER TABLE `2048_rank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `adclicks`
--
ALTER TABLE `adclicks`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `adminpanel`
--
ALTER TABLE `adminpanel`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `advertisements`
--
ALTER TABLE `advertisements`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `agent_allowed_family`
--
ALTER TABLE `agent_allowed_family`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `albumfav`
--
ALTER TABLE `albumfav`
  MODIFY `cid` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `albumseries`
--
ALTER TABLE `albumseries`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `allowedemails`
--
ALTER TABLE `allowedemails`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `audiocodecs`
--
ALTER TABLE `audiocodecs`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `avps`
--
ALTER TABLE `avps`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `bannedemails`
--
ALTER TABLE `bannedemails`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bans`
--
ALTER TABLE `bans`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `betgames`
--
ALTER TABLE `betgames`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `betlog`
--
ALTER TABLE `betlog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `betoptions`
--
ALTER TABLE `betoptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bets`
--
ALTER TABLE `bets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bitbucket`
--
ALTER TABLE `bitbucket`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blocks`
--
ALTER TABLE `blocks`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blue`
--
ALTER TABLE `blue`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bookmarks`
--
ALTER TABLE `bookmarks`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=425;
--
-- AUTO_INCREMENT for table `caticons`
--
ALTER TABLE `caticons`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cheaters`
--
ALTER TABLE `cheaters`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `chronicle`
--
ALTER TABLE `chronicle`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `claim`
--
ALTER TABLE `claim`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `codecs`
--
ALTER TABLE `codecs`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT for table `downloadspeed`
--
ALTER TABLE `downloadspeed`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `drawlottery`
--
ALTER TABLE `drawlottery`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forummods`
--
ALTER TABLE `forummods`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forums`
--
ALTER TABLE `forums`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `friends`
--
ALTER TABLE `friends`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fun`
--
ALTER TABLE `fun`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `funds`
--
ALTER TABLE `funds`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gift`
--
ALTER TABLE `gift`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `giftlog`
--
ALTER TABLE `giftlog`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `givebonus`
--
ALTER TABLE `givebonus`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invitebox`
--
ALTER TABLE `invitebox`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invites`
--
ALTER TABLE `invites`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `iplog`
--
ALTER TABLE `iplog`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `isp`
--
ALTER TABLE `isp`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `limitinvite`
--
ALTER TABLE `limitinvite`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `limitsuggest`
--
ALTER TABLE `limitsuggest`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `loginattempts`
--
ALTER TABLE `loginattempts`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lottery`
--
ALTER TABLE `lottery`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `modpanel`
--
ALTER TABLE `modpanel`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `nas`
--
ALTER TABLE `nas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `offervotes`
--
ALTER TABLE `offervotes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `overforums`
--
ALTER TABLE `overforums`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `peers`
--
ALTER TABLE `peers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pmboxes`
--
ALTER TABLE `pmboxes`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pollanswers`
--
ALTER TABLE `pollanswers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `polls`
--
ALTER TABLE `polls`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `processings`
--
ALTER TABLE `processings`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `prolinkclicks`
--
ALTER TABLE `prolinkclicks`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `radacct`
--
ALTER TABLE `radacct`
  MODIFY `radacctid` bigint(21) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `radcheck`
--
ALTER TABLE `radcheck`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `radgroupcheck`
--
ALTER TABLE `radgroupcheck`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `radgroupreply`
--
ALTER TABLE `radgroupreply`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `radhuntgroup`
--
ALTER TABLE `radhuntgroup`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `radippool`
--
ALTER TABLE `radippool`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `radpostauth`
--
ALTER TABLE `radpostauth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `radreply`
--
ALTER TABLE `radreply`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `radusergroup`
--
ALTER TABLE `radusergroup`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `readposts`
--
ALTER TABLE `readposts`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `recharge`
--
ALTER TABLE `recharge`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rechargelog`
--
ALTER TABLE `rechargelog`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `regimages`
--
ALTER TABLE `regimages`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `req`
--
ALTER TABLE `req`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `resreq`
--
ALTER TABLE `resreq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rules`
--
ALTER TABLE `rules`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `searchbox`
--
ALTER TABLE `searchbox`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `seckenapi`
--
ALTER TABLE `seckenapi`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `secondicons`
--
ALTER TABLE `secondicons`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `shoutbox`
--
ALTER TABLE `shoutbox`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sitelog`
--
ALTER TABLE `sitelog`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `snatched`
--
ALTER TABLE `snatched`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sources`
--
ALTER TABLE `sources`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `staffmessages`
--
ALTER TABLE `staffmessages`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `standards`
--
ALTER TABLE `standards`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `stylesheets`
--
ALTER TABLE `stylesheets`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `subs`
--
ALTER TABLE `subs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `suggest`
--
ALTER TABLE `suggest`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sysoppanel`
--
ALTER TABLE `sysoppanel`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `thanks`
--
ALTER TABLE `thanks`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `torrents`
--
ALTER TABLE `torrents`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `truckmarks`
--
ALTER TABLE `truckmarks`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `udlog`
--
ALTER TABLE `udlog`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uploadspeed`
--
ALTER TABLE `uploadspeed`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `userinfo`
--
ALTER TABLE `userinfo`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users_log`
--
ALTER TABLE `users_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wimax`
--
ALTER TABLE `wimax`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
