<?php

require "include/bittorrent.php";
dbconn();
loggedinorreturn();

if (get_user_class() < UC_ADMINISTRATOR) {
	stderr("错误", "权限不足");
}

if ($_GET[action] == "reorder") {
	foreach ($_POST[order] as $id => $position)
		sql_query("UPDATE `faq` SET `order`=" . sqlesc($position) . " WHERE id=" . sqlesc($id)) or sqlerr();
	header("Location: " . get_protocol_prefix() . "$BASEURL/faqmanage.php");
	die;
}

// ACTION: edit - edit a section or item
elseif ($_GET[action] == "edit" && isset($_GET[id])) {
	stdhead("FAQ管理");
	begin_main_frame();
	print("<h1 align=\"center\">编辑</h1>");

	$res = sql_query("SELECT * FROM faq WHERE id=" . sqlesc($_GET[id]) . " LIMIT 1");
	while ($arr = mysql_fetch_array($res, MYSQL_BOTH)) {
		$arr[question] = htmlspecialchars($arr[question]);
		$arr[answer] = htmlspecialchars($arr[answer]);
		if ($arr[type] == "item") {
			$lang_id = $arr['lang_id'];
			print("<form method=\"post\" action=\"faqactions.php?action=edititem\">");
			print("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" align=\"center\">\n");
			print("<tr><td>ID:</td><td>$arr[id] <input type=\"hidden\" name=\"id\" value=\"$arr[id]\" /></td></tr>\n");
			print("<tr><td>标题:</td><td><input style=\"width: 600px;\" type=\"text\" name=\"question\" value=\"$arr[question]\" /></td></tr>\n");
			print("<tr><td style=\"vertical-align: top;\">内容:</td><td><textarea rows=20 style=\"width: 600px; height=600px;\" name=\"answer\">$arr[answer]</textarea></td></tr>\n");
			if ($arr[flag] == "0")
				print("<tr><td>状态:</td><td><select name=\"flag\" style=\"width: 110px;\"><option value=\"0\" style=\"color: #FF0000;\" selected=\"selected\">隐藏</option><option value=\"1\" style=\"color: #000000;\">显示</option><option value=\"2\" style=\"color: #0000FF;\">更新</option><option value=\"3\" style=\"color: #008000;\">新的</option></select></td></tr>");
			elseif ($arr[flag] == "2")
				print("<tr><td>状态:</td><td><select name=\"flag\" style=\"width: 110px;\"><option value=\"0\" style=\"color: #FF0000;\">隐藏</option><option value=\"1\" style=\"color: #000000;\">显示</option><option value=\"2\" style=\"color: #0000FF;\" selected=\"selected\">更新</option><option value=\"3\" style=\"color: #008000;\">新的</option></select></td></tr>");
			elseif ($arr[flag] == "3")
				print("<tr><td>状态:</td><td><select name=\"flag\" style=\"width: 110px;\"><option value=\"0\" style=\"color: #FF0000;\">隐藏</option><option value=\"1\" style=\"color: #000000;\">显示</option><option value=\"2\" style=\"color: #0000FF;\">更新</option><option value=\"3\" style=\"color: #008000;\" selected=\"selected\">新的</option></select></td></tr>");
			else
				print("<tr><td>状态:</td><td><select name=\"flag\" style=\"width: 110px;\"><option value=\"0\" style=\"color: #FF0000;\">隐藏</option><option value=\"1\" style=\"color: #000000;\" selected=\"selected\">显示</option><option value=\"2\" style=\"color: #0000FF;\">更新</option><option value=\"3\" style=\"color: #008000;\">新的</option></select></td></tr>");
			print("<tr><td>分类:</td><td><select style=\"width: 400px;\" name=\"categ\" />");
			$res2 = sql_query("SELECT `id`, `question`, `link_id` FROM `faq` WHERE `type`='categ' AND `lang_id` = " . sqlesc($lang_id) . " ORDER BY `order` ASC");
			while ($arr2 = mysql_fetch_array($res2, MYSQL_BOTH)) {
				$selected = ($arr2[link_id] == $arr[categ]) ? " selected=\"selected\"" : "";
				print("<option value=\"$arr2[link_id]\"" . $selected . ">$arr2[question]</option>");
			}
			print("</td></tr>\n");
			print("<tr><td colspan=\"2\" align=\"center\"><input type=\"submit\" name=\"edit\" value=\"编辑\" style=\"width: 60px;\"></td></tr>\n");
			print("</table>");
		} elseif ($arr['type'] == "categ") {
			$lang_arr = mysql_fetch_array(sql_query("SELECT lang_name FROM language WHERE id=" . sqlesc($arr['lang_id']) . " LIMIT 1"));
			$lang_name = $lang_arr['lang_name'];
			print("<form method=\"post\" action=\"faqactions.php?action=editsect\">");
			print("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" align=\"center\">\n");
			print("<tr><td>ID:</td><td>$arr[id] <input type=\"hidden\" name=\"id\" value=\"$arr[id]\" /></td></tr>\n");
			print("<tr><td>语言:</td><td>$lang_name</td></tr>\n");
			print("<tr><td>标题:</td><td><input style=\"width: 300px;\" type=\"text\" name=\"title\" value=\"$arr[question]\" /></td></tr>\n");
			if ($arr['flag'] == "0")
				print("<tr><td>状态:</td><td><select name=\"flag\" style=\"width: 110px;\"><option value=\"0\" style=\"color: #FF0000;\" selected=\"selected\">隐藏</option><option value=\"1\" style=\"color: #000000;\">显示</option></select></td></tr>");
			else
				print("<tr><td>状态:</td><td><select name=\"flag\" style=\"width: 110px;\"><option value=\"0\" style=\"color: #FF0000;\">隐藏</option><option value=\"1\" style=\"color: #000000;\" selected=\"selected\">显示</option></select></td></tr>");
			print("<tr><td colspan=\"2\" align=\"center\"><input type=\"submit\" name=\"edit\" value=\"编辑\" style=\"width: 60px;\"></td></tr>\n");
			print("</table>");
		}
	}

	end_main_frame();
	stdfoot();
}

// subACTION: edititem - edit an item
elseif ($_GET[action] == "edititem" && $_POST[id] != NULL && $_POST[question] != NULL && $_POST[answer] != NULL && $_POST[flag] != NULL && $_POST[categ] != NULL) {
	$question = $_POST[question];
	$answer = $_POST[answer];
	sql_query("UPDATE `faq` SET `question`=" . sqlesc($question) . ", `answer`=" . sqlesc($answer) . ", `flag`=" . sqlesc($_POST[flag]) . ", `categ`=" . sqlesc($_POST[categ]) . " WHERE id=" . sqlesc($_POST[id])) or sqlerr();
	header("Location: " . get_protocol_prefix() . "$BASEURL/faqmanage.php");
	die;
}

// subACTION: editsect - edit a section
elseif ($_GET[action] == "editsect" && $_POST[id] != NULL && $_POST[title] != NULL && $_POST[flag] != NULL) {
	$title = $_POST[title];
	sql_query("UPDATE `faq` SET `question`=" . sqlesc($title) . ", `answer`='', `flag`=" . sqlesc($_POST[flag]) . ", `categ`='0' WHERE id=" . sqlesc($_POST[id])) or sqlerr();
	header("Location: " . get_protocol_prefix() . "$BASEURL/faqmanage.php");
	die;
}

// ACTION: delete - delete a section or item
elseif ($_GET[action] == "delete" && isset($_GET[id])) {
	if ($_GET[confirm] == "yes") {
		sql_query("DELETE FROM `faq` WHERE `id`=" . sqlesc(0 + $_GET[id]) . " LIMIT 1") or sqlerr();
		header("Location: " . get_protocol_prefix() . "$BASEURL/faqmanage.php");
		die;
	} else {
		stdhead("FAQ管理");
		begin_main_frame();
		print("<h1 align=\"center\">确定要删除吗？</h1>");
		print("<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" width=\"95%\">\n<tr><td align=\"center\">点击 <a href=\"faqactions.php?action=delete&id=$_GET[id]&confirm=yes\">这里</a> 确认</td></tr>\n</table>\n");
		end_main_frame();
		stdfoot();
	}
}

// ACTION: additem - add a new item
elseif ($_GET[action] == "additem" && $_GET[inid] && $_GET[langid]) {
	stdhead("FAQ管理");
	begin_main_frame();
	print("<h1 align=\"center\">增加项目</h1>");
	print("<form method=\"post\" action=\"faqactions.php?action=addnewitem\">");
	print("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" align=\"center\">\n");
	print("<tr><td>问题：</td><td><input style=\"width: 600px;\" type=\"text\" name=\"question\" value=\"\" /></td></tr>\n");
	print("<tr><td style=\"vertical-align: top;\">回答</td><td><textarea rows=20 style=\"width: 600px; height=600px;\" name=\"answer\"></textarea></td></tr>\n");
	print("<tr><td>状态</td><td><select name=\"flag\" style=\"width: 110px;\"><option value=\"0\" style=\"color: #FF0000;\">隐藏</option><option value=\"1\" style=\"color: #000000;\" selected=\"selected\">显示</option><option value=\"2\" style=\"color: #0000FF;\">更新</option><option value=\"3\" style=\"color: #008000;\">新的</option></select></td></tr>");
	print("<input type=hidden name=categ value=\"" . (0 + $_GET[inid]) . "\">");
	print("<input type=hidden name=langid value=\"" . (0 + $_GET[langid]) . "\">");
	print("<tr><td colspan=\"2\" align=\"center\"><input type=\"submit\" value=\"添加\" style=\"width: 60px;\"></td></tr>\n");
	print("</table></form>");
	end_main_frame();
	stdfoot();
}

// ACTION: addsection - add a new section
elseif ($_GET[action] == "addsection") {
	stdhead("FAQ管理");
	begin_main_frame();
	print("<h1 align=\"center\">增加</h1>");
	print("<form method=\"post\" action=\"faqactions.php?action=addnewsect\">");
	print("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" align=\"center\">\n");
	print("<tr><td>标题:</td><td><input style=\"width: 300px;\" type=\"text\" name=\"title\" value=\"\" /></td></tr>\n");
	$s = "<select name=language>";
	$langs = langlist("rule_lang");
	foreach ($langs as $row) {
		if ($row["site_lang_folder"] == $deflang)
			$se = " selected";
		else
			$se = "";
		$s .= "<option value=" . $row["id"] . $se . ">" . htmlspecialchars($row["lang_name"]) . "</option>\n";
	}
	$s .= "</select>";
	print("<tr><td>语言:</td><td>" . $s . "</td></tr>");
	print("<tr><td>状态:</td><td><select name=\"flag\" style=\"width: 110px;\"><option value=\"0\" style=\"color: #FF0000;\">隐藏</option><option value=\"1\" style=\"color: #000000;\" selected=\"selected\">显示</option></select></td></tr>");
	print("<tr><td colspan=\"2\" align=\"center\"><input type=\"submit\" name=\"edit\" value=\"添加\" style=\"width: 60px;\"></td></tr>\n");
	print("</table>");
	end_main_frame();
	stdfoot();
}

// subACTION: addnewitem - add a new item to the db
elseif ($_GET[action] == "addnewitem" && $_POST[question] != NULL && $_POST[answer] != NULL) {
	$question = $_POST[question];
	$answer = $_POST[answer];
	$categ = 0 + $_POST[categ];
	$langid = 0 + $_POST[langid];
	$res = sql_query("SELECT MAX(`order`) AS maxorder, MAX(`link_id`) AS maxlinkid FROM `faq` WHERE `type`='item' AND `categ`=" . sqlesc($categ) . " AND lang_id=" . sqlesc($langid));
	while ($arr = mysql_fetch_array($res, MYSQL_BOTH)) {
		$order = $arr['maxorder'] + 1;
		$link_id = $arr['maxlinkid'] + 1;
	}
	sql_query("INSERT INTO `faq` (`link_id`, `type`, `lang_id`, `question`, `answer`, `flag`, `categ`, `order`) VALUES ('$link_id', 'item', " . sqlesc($langid) . ", " . sqlesc($question) . ", " . sqlesc($answer) . ", " . sqlesc(0 + $_POST[flag]) . ", " . sqlesc($categ) . ", " . sqlesc($order) . ")") or sqlerr();
	header("Location: " . get_protocol_prefix() . "$BASEURL/faqmanage.php");
	die;
}

// subACTION: addnewsect - add a new section to the db
elseif ($_GET[action] == "addnewsect" && $_POST[title] != NULL && $_POST[flag] != NULL) {
	$title = $_POST[title];
	$language = 0 + $_POST['language'];
	$res = sql_query("SELECT MAX(`order`) AS maxorder, MAX(`link_id`) AS maxlinkid FROM `faq` WHERE `type`='categ' AND `lang_id` = " . sqlesc($language));
	while ($arr = mysql_fetch_array($res, MYSQL_BOTH)) {
		$order = $arr['maxorder'] + 1;
		$link_id = $arr['maxlinkid'] + 1;
	}
	sql_query("INSERT INTO `faq` (`link_id`,`type`,`lang_id`, `question`, `answer`, `flag`, `categ`, `order`) VALUES (" . sqlesc($link_id) . ",'categ', " . sqlesc($language) . ", " . sqlesc($title) . ", '', " . sqlesc($_POST[flag]) . ", '0', " . sqlesc($order) . ")") or sqlerr();
	header("Location: " . get_protocol_prefix() . "$BASEURL/faqmanage.php");
	die;
} else {
	header("Location: " . get_protocol_prefix() . "$BASEURL/faqmanage.php");
	die;
}
