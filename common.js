function postvalid(form) {
	$('#qr').attr({
		'disabled': 'disabled'
	});
	return true;
}

var showopen = 0;

function dropmenu(obj) {
	var listid = '#' + obj.id + 'list';
	if (showopen) {
		$(listid).hide();
		showopen = 0;
	} else {
		$(listid).show();
		showopen = 1;
	}
}

function quick_reply_to(userid) {
	var ttt = parent.document.getElementById("replaytext");
	ttt.value = userid + "  " + ttt.value;
	if (document.selection) {
		range.moveStart('character', ttt.value.length);
		range.moveEnd('character', ttt.value.length);
		range.select();
	} else {
		ttt.setSelectionRange(ttt.value.length, ttt.value.length);
		ttt.focus();
	}
}

function confirm_delete(id, note, addon) {
	if (confirm(note)) {
		self.location.href = '?action=del' + (addon ? '&' + addon : '') + '&id=' + id;
	}
}

//viewfilelist.js
function viewfilelist(torrentid) {
	var result = ajax.gets('viewfilelist.php?id=' + torrentid);
	document.getElementById("showfl").style.display = 'none';
	document.getElementById("hidefl").style.display = 'block';
	showlist(result);
}

function showlist(filelist) {
	document.getElementById("filelist").innerHTML = filelist;
}

function hidefilelist() {
	document.getElementById("hidefl").style.display = 'none';
	document.getElementById("showfl").style.display = 'block';
	document.getElementById("filelist").innerHTML = "";
}

//viewpeerlist.js
function viewpeerlist(torrentid) {
	var list = ajax.gets('viewpeerlist.php?id=' + torrentid);
	document.getElementById("showpeer").style.display = 'none';
	document.getElementById("hidepeer").style.display = 'block';
	document.getElementById("peercount").style.display = 'none';
	document.getElementById("peerlist").innerHTML = list;
}

function hidepeerlist() {
	document.getElementById("hidepeer").style.display = 'none';
	document.getElementById("peerlist").innerHTML = "";
	document.getElementById("showpeer").style.display = 'block';
	document.getElementById("peercount").style.display = 'block';
}

// smileit.js
function SmileIT(smile, form, text) {
	$("[name='" + text + "']").insertAtCaret(smile);
	$("[name='" + text + "']").focus();
}

// saythanks.js
function saythanks(torrentid) {
	var list = ajax.post('thanks.php', '', 'id=' + torrentid);
	document.getElementById("thanksbutton").innerHTML = document.getElementById("thanksadded").innerHTML;
	document.getElementById("nothanks").innerHTML = "";
	document.getElementById("addcuruser").innerHTML = document.getElementById("curuser").innerHTML;
}

// modified by SamuraiMe,2013.05.16
function thanksBonus(torrentid, bonus) {
	//先行用js统计魔力值总数
	if ($("#bonus_sum").length > 0) {
		var currentBonusSum = new Number($("#bonus_sum").text());
		$("#bonus_sum").text(currentBonusSum + bonus);
	} else {
		$("td").has("#nothanks").prev().append("</br>总计<span id=\"bonus_sum\">" + bonus + "</span>魔力值");
	}

	$.post("mybonus.php?action=exchange", {
		torrent_id: torrentid,
		bonusgift: bonus,
		option: 8,
		username: $("#owner_name").val(),
		where: "[url=details.php?id=" + torrentid + "]" + $("#top").text() + "[/url]"
	}, function () {
		$("#donate, .saythanks").remove();
	});

	document.getElementById("thanksbutton").innerHTML = document.getElementById("thanksadded").innerHTML;
	document.getElementById("nothanks").innerHTML = "";
	document.getElementById("addcuruser").innerHTML = document.getElementById("curuser").innerHTML;
	$("#addcuruser span").after("(" + bonus + ")");
}

// preview.js

function preview(obj) {
	var poststr = encodeURIComponent($('#sceditor').sceditor('instance').val());
	var result = ajax.posts('preview.php', 'body=' + poststr);
	document.getElementById("previewouter").innerHTML = result;
	document.getElementById("previewouter").style.display = 'block';
	document.getElementById("editorouter").style.display = 'none';
	document.getElementById("unpreviewbutton").style.display = 'block';
	document.getElementById("previewbutton").style.display = 'none';
}

function unpreview(obj) {
	document.getElementById("previewouter").style.display = 'none';
	document.getElementById("editorouter").style.display = 'block';
	document.getElementById("unpreviewbutton").style.display = 'none';
	document.getElementById("previewbutton").style.display = 'block';
}

//preview.torrent
function preview_torrent() {
	var poststr = encodeURIComponent($('#sceditor').sceditor('instance').val());
	var result = ajax.posts('preview.php', 'body=' + poststr);
	var textareaTable = $('#sceditor').parents('td.rowfollow');
	textareaTable.after('<td id="pre_div" class="rowfollow">' + result + '</td>');
	$('#preDIv').hide();
	$('#pre_div>table').css('border', 'none');
	textareaTable.hide();
	$('#EditDIv').show();
}

function edit_torrent() {
	$('#EditDIv').hide();
	$('#pre_div').remove();
	$('#sceditor').parents('td.rowfollow').show();
	$('#preDIv').show();
}

// java_klappe.js

function klappe(id) {
	var klappText = document.getElementById('k' + id);
	var klappBild = document.getElementById('pic' + id);

	if (klappText.style.display == 'none') {
		klappText.style.display = 'block';
		// klappBild.src = 'pic/blank.gif';
	} else {
		klappText.style.display = 'none';
		// klappBild.src = 'pic/blank.gif';
	}
}

function klappe_news(id) {
	var klappText = document.getElementById('k' + id);
	var klappBild = document.getElementById('pic' + id);
	if (klappText.style.display == 'none') {
		klappText.style.display = '';
		klappBild.className = 'minus';
	} else {
		klappText.style.display = 'none';
		klappBild.className = 'plus';
	}
}

function klappe_ext(id) {
	var klappText = document.getElementById('k' + id);
	var klappBild = document.getElementById('pic' + id);
	var klappPoster = document.getElementById('poster' + id);
	if (klappText.style.display == 'none') {
		klappText.style.display = 'block';
		klappPoster.style.display = 'block';
		klappBild.className = 'minus';
	} else {
		klappText.style.display = 'none';
		klappPoster.style.display = 'none';
		klappBild.className = 'plus';
	}
}

// disableother.js

function disableother(select, target) {
	if (document.getElementById(select).value == 0)
		document.getElementById(target).disabled = false;
	else {
		document.getElementById(target).disabled = true;
		document.getElementById(select).disabled = false;
	}
}

function disableother2(oricat, newcat) {
	if (document.getElementById("movecheck").checked == true) {
		document.getElementById(oricat).disabled = true;
		document.getElementById(newcat).disabled = false;
	} else {
		document.getElementById(oricat).disabled = false;
		document.getElementById(newcat).disabled = true;
	}
}

// ctrlenter.js
var submitted = false;

function ctrlenter(event, formname, submitname) {
	if (submitted == false) {
		var keynum;
		if (event.keyCode) {
			keynum = event.keyCode;
		} else if (event.which) {
			keynum = event.which;
		}
		if (event.ctrlKey && keynum == 13) {
			submitted = true;
			document.getElementById(formname).submit();
		}
	}
}

function gotothepage(page) {
	var url = window.location.href;
	var end = url.lastIndexOf("page");
	url = url.replace(/#[0-9]+/g, "");
	if (end == -1) {
		if (url.lastIndexOf("?") == -1)
			window.location.href = url + "?page=" + page;
		else
			window.location.href = url + "&page=" + page;
	} else {
		url = url.replace(/page=.+/g, "");
		window.location.href = url + "page=" + page;
	}
}

function changepage(event) {
	var gotopage;
	var keynum;
	var altkey;
	if (navigator.userAgent.toLowerCase().indexOf('presto') != -1)
		altkey = event.shiftKey;
	else
		altkey = event.altKey;
	if (event.keyCode) {
		keynum = event.keyCode;
	} else if (event.which) {
		keynum = event.which;
	}
	if (altkey && keynum == 33) {
		if (currentpage <= 0)
			return;
		gotopage = currentpage - 1;
		gotothepage(gotopage);
	} else if (altkey && keynum == 34) {
		if (currentpage >= maxpage)
			return;
		gotopage = currentpage + 1;
		gotothepage(gotopage);
	}
}
if (window.document.addEventListener) {
	window.addEventListener("keydown", changepage, false);
} else {
	window.attachEvent("onkeydown", changepage, false);
}

// bookmark.js
function bookmark(torrentid, counter, styles) {
	var result = ajax.gets('bookmark.php?torrentid=' + torrentid);
	bmicon(result, counter, torrentid, styles);
}

function bmicon(status, counter, torrentid, styles) {
	if (styles != 1) {
		if (status == "added") {
			document.getElementById("bookmark" + counter).innerHTML = "<img class=\"bookmark\" src=\"pic/trans.gif\" alt=\"Bookmarked\" />";
			//document.getElementById("tid_" + torrentid).style.backgroundColor = "#FFB6C1";
			//document.getElementById("ti_" + torrentid).style.backgroundColor = "#FFB6C1";
			//document.getElementById("t_" + torrentid).style.backgroundColor = "#FFB6C1";
			$("#tid_" + torrentid).addClass("torrent_table bookmarks");
		} else if (status == "deleted") {
			document.getElementById("bookmark" + counter).innerHTML = "<img class=\"delbookmark\" src=\"pic/trans.gif\" alt=\"Unbookmarked\" />";
			//$("#tid_" + torrentid).removeAttr("style");
			//document.getElementById("tid_" + torrentid).style.backgroundColor = "";
			//document.getElementById("ti_" + torrentid).style.backgroundColor = "";
			//document.getElementById("t_" + torrentid).style.backgroundColor = "";
			$("#tid_" + torrentid).removeClass("bookmarks");
		}
	} else {
		if (status == "added") {
			document.getElementById("bookmark" + counter).innerHTML = "<img class=\"bookmark\" src=\"pic/trans.gif\" alt=\"Bookmarked\" />";
			//document.getElementById("tid_" + torrentid).style.background = "url(styles/default/bookmarks.jpg) repeat transparent";
			//document.getElementById("ti_" + torrentid).style.background = "url(styles/default/bookmarks.jpg) repeat transparent";
			//document.getElementById("t_" + torrentid).style.background = "url(styles/default/bookmarks.jpg) repeat transparent";
			$("#tid_" + torrentid).addClass("torrent_table bookmarks");
		} else if (status == "deleted") {
			document.getElementById("bookmark" + counter).innerHTML = "<img class=\"delbookmark\" src=\"pic/trans.gif\" alt=\"Unbookmarked\" />";
			//$("#tid_" + torrentid).removeAttr("style");
			//document.getElementById("tid_" + torrentid).style.background = "";
			//document.getElementById("ti_" + torrentid).style.background = "";
			//document.getElementById("t_" + torrentid).style.background = "";
			$("#tid_" + torrentid).removeClass("bookmarks");
		}
	}
}

// truck.js
function truckmark(torrentid, counter, styles) {
	var result = ajax.gets('truckmark.php?torrentid=' + torrentid);
	tmicon(result, counter, torrentid, styles);
}

function tmicon(status, counter, torrentid, styles) {
	if (styles != 1) {
		if (status == "added") {
			document.getElementById("truckmark" + counter).innerHTML = "<img class=\"truckmark\" src=\"pic/trans.gif\" alt=\"Truckmarked\" />";
			//document.getElementById("tid_" + torrentid).style.backgroundColor = "#66CDAA";
			//document.getElementById("ti_" + torrentid).style.backgroundColor = "#66CDAA";
			//document.getElementById("t_" + torrentid).style.backgroundColor = "#66CDAA";
			$("#tid_" + torrentid).addClass("torrent_table truckmarks");
		} else if (status == "deleted") {
			document.getElementById("truckmark" + counter).innerHTML = "<img class=\"truckmark\" src=\"pic/trans.gif\" alt=\"Untruckmarked\" />";
			//$("#tid_" + torrentid).removeAttr("style");
			//document.getElementById("tid_" + torrentid).style.backgroundColor = "";
			//document.getElementById("ti_" + torrentid).style.backgroundColor = "";
			//document.getElementById("t_" + torrentid).style.backgroundColor = "";
			$("#tid_" + torrentid).removeClass("truckmarks");
		}
	} else {
		if (status == "added") {
			document.getElementById("truckmark" + counter).innerHTML = "<img class=\"truckmark\" src=\"pic/trans.gif\" alt=\"Truckmarked\" />";
			//document.getElementById("tid_" + torrentid).style.background = "url(styles/default/truckmarks.jpg) repeat transparent";
			//document.getElementById("ti_" + torrentid).style.background = "url(styles/default/truckmarks.jpg) repeat transparent";
			//document.getElementById("t_" + torrentid).style.background = "url(styles/default/truckmarks.jpg) repeat transparent";
			$("#tid_" + torrentid).addClass("torrent_table truckmarks");
		} else if (status == "deleted") {
			document.getElementById("truckmark" + counter).innerHTML = "<img class=\"truckmark\" src=\"pic/trans.gif\" alt=\"Untruckmarked\" />";
			//$("#tid_" + torrentid).removeAttr("style");
			//document.getElementById("tid_" + torrentid).style.background = "";
			//document.getElementById("ti_" + torrentid).style.background = "";
			//document.getElementById("t_" + torrentid).style.background = "";
			$("#tid_" + torrentid).removeClass("truckmarks");
		}
	}
}

function truckmarks(torrentid) {
	var result = ajax.gets('truckmark.php?torrentid=' + torrentid);
	tmicons(result, torrentid);
}

function tmicons(status) {
	if (status == "added") {
		alert('成功加入小货车');
	} else if (status == "deleted") {
		alert('成功移出小货车');
	}
}

//pushfree.js
function pushfree(torrentid) {
	var result = ajax.gets('pushfree.php?torrentid=' + torrentid);
	pfstatus(result);
}

function pfstatus(status) {
	if (status == "pushfree") {
		document.getElementById("pushfree").innerHTML = "<input type=\"button\" name=\"button\" value=\"感谢投蓝\" disabled=\"disabled\" />";
		document.getElementById("pushfreeadduser").innerHTML = document.getElementById("pushfreeuser").innerHTML;
		document.getElementById("nopushfree").innerHTML = "";
	}
}

//claim.js
function claim(torrentid) {
	var result = ajax.gets('claim.php?torrentid=' + torrentid);
	if (result != "claim") {
		alert('手太慢了，认领失败！');
	} else {
		cmstatus(result);
	}
}

function cmstatus(status) {
	if (status == "claim") {
		document.getElementById("claim").innerHTML = "<input type=\"button\" name=\"button\" value=\"认领成功\" disabled=\"disabled\" />";
		document.getElementById("claimadduser").innerHTML = document.getElementById("claimuser").innerHTML;
		document.getElementById("noclaim").innerHTML = "";
	}
}

// check.js
var checkflag = "false";

function check(field, checkall_name, uncheckall_name) {
	if (checkflag == "false") {
		for (i = 0; i < field.length; i++) {
			field[i].checked = true;
		}
		checkflag = "true";
		return uncheckall_name;
	} else {
		for (i = 0; i < field.length; i++) {
			field[i].checked = false;
		}
		checkflag = "false";
		return checkall_name;
	}
}

// in torrents.php
var form = 'searchbox';

function SetChecked(chkName, ctrlName, checkall_name, uncheckall_name, start, count) {
	dml = document.forms[form];
	len = dml.elements.length;
	var begin;
	var end;
	if (start == -1) {
		begin = 0;
		end = len;
	} else {
		begin = start;
		end = start + count;
	}
	var check_state;
	for (i = 0; i < len; i++) {
		if (dml.elements[i].name == ctrlName) {
			if (dml.elements[i].value == checkall_name) {
				dml.elements[i].value = uncheckall_name;
				check_state = 1;
			} else {
				dml.elements[i].value = checkall_name;
				check_state = 0;
			}
		}

	}
	for (i = begin; i < end; i++) {
		if (dml.elements[i].name.indexOf(chkName) != -1) {
			dml.elements[i].checked = check_state;
		}
	}
}

// funvote.js
function funvote(funid, yourvote) {
	var result = ajax.gets('fun.php?action=vote&id=' + funid + "&yourvote=" + yourvote);
	voteaccept(yourvote);
}

function voteaccept(yourvote) {
	if (yourvote == "fun" || yourvote == "dull") {
		document.getElementById("funvote").style.display = 'none';
		document.getElementById("voteaccept").style.display = 'block';
	}
}

// in upload.php
function getname() {
	var filename = document.getElementById("torrent").value;
	var filename = filename.toString();
	var lowcase = filename.toLowerCase();
	var start = lowcase.lastIndexOf("\\"); //for Google Chrome on windows
	if (start == -1) {
		start = lowcase.lastIndexOf("\/"); //for Google Chrome on linux
		if (start == -1)
			start == 0;
		else
			start = start + 1;
	} else
		start = start + 1;
	var end = lowcase.lastIndexOf("torrent");
	var noext = filename.substring(start, end - 1);
	noext = noext.replace(/[Bb][Ll][Uu]-[Rr][Aa][Yy]/g, "BluRay");
	noext = noext.replace(/[Bb][Ll][Uu][Rr][Aa][Yy]/g, "BluRay");
	noext = noext.replace(/[Hh]_264/g, "H.264");
	noext = noext.replace(/[Hh]_265/g, "H.265");
	noext = noext.replace(/7[_\ ]1/g, "7.1");
	noext = noext.replace(/5[_\ ]1/g, "5.1");
	noext = noext.replace(/4[_\ ]1/g, "4.1");
	noext = noext.replace(/2[_\ ]1/g, "2.1");
	noext = noext.replace(/2[_\ ]0/g, "2.0");
	noext = noext.replace(/\ /g, ".");
	document.getElementById("name").value = noext;
}

// in userdetails.php
function getusertorrentlistajax(userid, type, blockid) {
	if (document.getElementById(blockid).innerHTML == "") {
		var infoblock = ajax.gets('getusertorrentlistajax.php?userid=' + userid + '&type=' + type);
		document.getElementById(blockid).innerHTML = infoblock;
	}
	return true;
}

// in functions.php
function get_ext_info_ajax(blockid, url, cache, type) {
	if (document.getElementById(blockid).innerHTML == "") {
		var infoblock = ajax.gets('getextinfoajax.php?url=' + url + '&cache=' + cache + '&type=' + type);
		document.getElementById(blockid).innerHTML = infoblock;
	}
	return true;
}

// in userdetails.php
function enabledel(msg) {
	document.deluser.submit.disabled = document.deluser.submit.checked;
	alert(msg);
}

function disabledel() {
	document.deluser.submit.disabled = !document.deluser.submit.checked;
}

// in mybonus.php
function customgift() {
	if (document.getElementById("giftselect").value == '0') {
		document.getElementById("giftselect").disabled = true;
		document.getElementById("giftcustom").disabled = false;
	}
}