<?php
require_once("include/bittorrent.php");
dbconn();
loggedinorreturn();
parked();
if ($CURUSER['class'] < UC_MODERATOR) {
	stderr("抱歉...", "您的等级太低");
	exit;
}

stdhead("查看限定资源授权用户");
?>
<div>
	<form method="POST">
		<?php echo "<font style='color:green'>添加</font>授权用户ID：" ?><input type="text" name="userid1" style="width: 100px" onkeyup="this.value = this.value.replace(/\D/g, '');" />
		<input type="submit" name="submit1" value="提交" /><br /><br />
	</form>
	<form method="POST">
		<?php echo "<font style='color:red'>取消</font>授权用户ID：" ?><input type="text" name="userid2" style="width: 100px" onkeyup="this.value = this.value.replace(/\D/g, '');" />
		<input type="submit" name="submit2" value="提交" /><br /><br />
	</form>
</div>
<?php
if ($_POST['submit1']) {
	if (!empty($_POST['userid1'])) {
		$userid = $_POST['userid1'];
		int_check($userid);
	} else {
		echo "<script>alert('用户ID不能为空！');location.href='limitduser.php';</script>";
	}
	sql_query("UPDATE users SET limitd = 'yes' WHERE id = $userid");
	sendMessage(0, $userid, "恭喜，你已获得限定资源下载资格", "请注意限定资源的规则，否则将会取消下载限定资源的资格。");
	echo "<script>alert('赋予授权成功！');location.href='limitduser.php';</script>";
}
if ($_POST['submit2']) {
	if (!empty($_POST['userid2'])) {
		$userid = $_POST['userid2'];
		int_check($userid);
	} else {
		echo "<script>alert('用户ID不能为空！');location.href='limitduser.php';</script>";
	}
	sql_query("UPDATE users SET limitd = 'no' WHERE id = $userid");
	sendMessage(0, $userid, "抱歉，已取消你限定资源下载资格", "由于违规转载，加工等行为，我们决定取消你限定资源的下载资格。");
	echo "<script>alert('取消授权成功！');location.href='limitduser.php';</script>";
}

function begin_table_newuser($fullwidth = false, $padding = 5) {
	$width = "";
	if ($fullwidth)
		$width .= " width=50%";
	return("<table class='main" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'><tr align=\"center\"></tr>");
}

function end_table_newuser() {
	return("<tr align=\"center\"></tr></table>");
}

function begin_frame_newuser($caption = "", $center = false, $padding = 5, $width = "100%", $caption_center = "left") {
	$tdextra = "";
	if ($center)
		$tdextra .= " align='center'";
	return(($caption ? "<h2 align='" . $caption_center . "'>" . $caption . "</h2>" : "") . "<table width='" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'>" . "<tr><td class='text' $tdextra>");
}

function end_frame_newuser() {
	return("</td></tr></table>");
}

function bjtable_newuser($res, $frame_caption) {
	$htmlout = '';
	$htmlout .= begin_frame_newuser($frame_caption, true);
	$htmlout .= begin_table_newuser();
	$htmlout .= "<tr><td class='colhead'>ID</td><td class='colhead' align='left'>用户名</td><td class='colhead' align='right'>等级</td><td class='colhead' align='right'>邀请人</td><td class='colhead' align='right'>地理位置</td><td class='colhead' align='right'>注册时间</td><td class='colhead' align='right'>注册邮箱</td><td class='colhead' align='right'>IP</td><td class='colhead' align='right'>上传量</td><td class='colhead' align='right'>下载量</td><td class='colhead' align='right'><a href='?bonus=1'>魔力值</a></td></tr>";
	while ($a = mysql_fetch_assoc($res)) {
		list($loc_pub) = get_ip_location($a['ip']);
		if (school_ip_location($a['ip']) == '') {
			$school = "未知";
		} else {
			$school = school_ip_location($a['ip']);
		}
		$htmlout .= "<tr class='torrent_table'><td>$a[id]</td>" . //ID
				"<td align='left'>" . get_username($a['id'], FALSE, TRUE, TRUE, TRUE) . "</td>" . //用户名
				"<td align='right'>" . get_user_class_name_zh($a['class'], FALSE, TRUE, TRUE) . "</td>" . //等级
				"<td align='right'>" . ($a['invited_by'] != 0 ? get_username($a['invited_by'], FALSE, TRUE, TRUE, TRUE) : "无") . "</td>" . //邀请人
				"<td align='right'>" . (!ip2long($a['ip']) ? "$school" : "$loc_pub") . "</td>" . //地理位置
				"<td align='right'>" . $a['added'] . "</td>" . //注册时间
				"<td align='right'>" . $a['email'] . "</td>" . //注册邮箱
				"<td align='right'>" . $a['ip'] . "</td>" . //IP
				"<td align='right'>" . mksize($a['uploaded']) . "</td>" . //上传量
				"<td align='right'>" . mksize($a['downloaded']) . "</td>" . //下载量
				"<td align='right'>" . round($a['seedbonus']) . "</td>" . //魔力值
				"</tr>";
	}
	$htmlout .= end_table_newuser();
	$htmlout .= end_frame_newuser();
	return $htmlout;
}

$HTMLOUT .= "<h1 align='center'>查看限定资源授权用户信息</h1>";
if ($_GET['bonus'] == 1) {
	$count = get_row_count("users", "WHERE limitd = 'yes'");
	list ($pagertop, $pagerbottom, $limit) = pager(25, $count, "?bonus=1&");
	$res = sql_query("SELECT * FROM users WHERE limitd = 'yes' ORDER BY seedbonus DESC $limit") or sqlerr(__FILE__, __LINE__); //降序排列
} else {
	$count = get_row_count("users", "WHERE limitd = 'yes'");
	list($pagertop, $pagerbottom, $limit) = pager(25, $count, "?");
	$res = sql_query("SELECT * FROM users WHERE limitd = 'yes' ORDER BY id DESC $limit") or sqlerr(__FILE__, __LINE__); //降序排列
}
$HTMLOUT .= bjtable_newuser($res, "用户信息", "Users");
$HTMLOUT .= "<br /><br />";
if ($count) {
	print($pagertop);
	print $HTMLOUT;
	print($pagerbottom);
} else {
	print $HTMLOUT;
}
stdfoot();
