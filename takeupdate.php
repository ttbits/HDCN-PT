<?php

require_once("include/bittorrent.php");

function bark($msg) {
	stdhead();
	stdmsg("错误", $msg);
	stdfoot();
	exit;
}

dbconn();
loggedinorreturn();
if (get_user_class() < $staffmem_class)
	permissiondenied();
if ($_POST['setdealt']) {
	$res = sql_query("SELECT * FROM reports WHERE dealtwith = 0 AND id IN (" . implode(", ", $_POST[delreport]) . ")");
	$num = $_POST['bonus'];
	$num2 = $_POST['bonus2'];
	while ($arr = mysql_fetch_assoc($res)) {
		switch ($arr['type']) {
			case "torrent":
				$arr['type'] = "种子";
				$res = mysql_fetch_array(sql_query("SELECT name FROM torrents WHERE id = $arr[reporid]"));
				$reportid = $res['name'];
				break;
			case "user":
				$arr['type'] = "用户";
				$res = mysql_fetch_array(sql_query("SELECT username FROM users WHERE id = $arr[reporid]"));
				$reportid = $res['username'];
				break;
			case "offer":
				$arr['type'] = "候选";
				$res = mysql_fetch_array(sql_query("SELECT name FROM offers WHERE id = $arr[reporid]"));
				$reportid = $res['name'];
				break;
			case "request":
				$arr['type'] = "求种";
				$res = mysql_fetch_array(sql_query("SELECT name FROM req WHERE id = $arr[reporid]"));
				$reportid = $res['name'];
				break;
			case "post":
				$arr['type'] = "帖子";
				$reportid = "";
				break;
			case "comment":
				$arr['type'] = "评论";
				$reportid = "";
				break;
			case "subtitle":
				$arr['type'] = "字幕";
				$res = mysql_fetch_array(sql_query("SELECT title FROM subs WHERE id = $arr[reporid]"));
				$reportid = $res['title'];
				break;
			default:
				break;
		}
		if ($CURUSER['id'] == $arr['reported'] || $CURUSER['id'] == $arr['addedby']) {
			bark("你不能处理自己举报或关于自己的举报信息");
		}
		sql_query("UPDATE reports SET dealtwith = 1, dealtby = $CURUSER[id] WHERE id = $arr[id]") or sqlerr(__FILE__, __LINE__);
		if ($num == 0) {
			break;
		}
		addBonus($arr['addedby'], $num);
		writeBonusComment($arr['addedby'], "因举报而被改变魔力值 $num 个");
		sendMessage(0, $arr['addedby'], "因举报而被改变魔力值 $num 个", "您在 $arr[added] 举报 $arr[type] {$reportid}，原因是 $arr[reason]\n管理员审核之后决定对您的魔力值做出 $num 的变动，请继续积极合理举报不良现象，维护本站良好氛围");
		if ($num2 == 0) {
			break;
		}
		addBonus($arr['reported'], $num2);
		writeBonusComment($arr['reported'], "因举报而被改变魔力值 $num2 个");
		sendMessage(0, $arr['reported'], "因被举报而被改变魔力值 $num2 个", "您的 $arr[type] $reportid 被用户举报 ，原因是 $arr[reason]\n管理员审核之后决定对您的魔力值做出 $num2 的变动，请积极合理举报不良现象，维护本站良好氛围");
	}
	$Cache->delete_value('staff_new_report_count');
} elseif ($_POST['delete']) {
	$res = sql_query("SELECT * FROM reports WHERE id IN (" . implode(", ", $_POST[delreport]) . ")");
	while ($arr = mysql_fetch_assoc($res)) {
		if ($CURUSER['id'] == $arr['reported'] || $CURUSER['id'] == $arr['addedby'])
			bark("你不能处理自己举报或关于自己的举报信息");
		sql_query("DELETE FROM reports WHERE id = $arr[id]") or sqlerr(__FILE__, __LINE__);
	}
	$Cache->delete_value('staff_new_report_count');
	$Cache->delete_value('staff_report_count');
}
header("Refresh: 0; url=reports.php");
