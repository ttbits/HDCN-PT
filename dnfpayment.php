<?php
require_once("include/bittorrent.php");
dbconn();
loggedinorreturn();
parked();
if ($_GET['pay'] == '1') {
	stderr("恭喜", "充值成功！");
}
$bonus = $CURUSER['seedbonus']; //魔力值
$big = $CURUSER['big']; //荣誉值
$class = $CURUSER['class']; //等级
$itemname = '毒奶粉';
$username = $_POST['username'];
if ($_POST['bonus']) {
	if ($username == '') {
		echo "<script>alert('错误！请输入毒奶粉用户名！');location.href='dnfpayment.php';</script>";
	}
	$check = 0;
	if ($_POST['bonusnum'] == '' || $_POST['bonusnum'] < 100) {
		echo "<script>alert('错误！请输入大于 100 的正整数，且为 100 的倍数！');location.href='dnfpayment.php';</script>";
		$check = 1;
	}
	if ($_POST['bonusnum'] % 100 != 0) {
		echo "<script>alert('错误！请输入大于 100 的正整数，且为 100 的倍数！');location.href='dnfpayment.php';</script>";
		$check = 1;
	}
	switch ($class) {
		case UC_POWER_USER:
			$rate = "1.3";
			break;
		case UC_ELITE_USER:
			$rate = "1.4";
			break;
		case UC_CRAZY_USER:
			$rate = "1.5";
			break;
		case UC_INSANE_USER:
			$rate = "1.6";
			break;
		case UC_VETERAN_USER:
			$rate = "1.7";
			break;
		case UC_EXTREME_USER:
			$rate = "1.8";
			break;
		case UC_ULTIMATE_USER:
			$rate = "1.9";
			break;
		case UC_NEXUS_MASTER:
			$rate = "2";
			break;
		default :
			$rate = "1";
	}
	if ($class >= UC_VIP) {
		$rate = "2";
	}
	$PayMoney = $_POST['bonusnum'];
	$PayGold = $PayMoney / 100 * $rate;
	if ($PayMoney > $bonus) {
		echo "<script>alert('错误！魔力值不足，请重新输入！');location.href='dnfpayment.php';</script>";
	} else {
		$do = 1;
	}
	if ($do == 1 && $check == 0) {
		//充值逻辑
		$pushkey['pushkey'] = encrypt(json_encode(array("username" => "$username", "number" => "$PayGold")));
		$url = "http://115.182.84.121/dnfpayto.php";
		$ch = curl_init();
		$timeout = 30;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//POST数据
		curl_setopt($ch, CURLOPT_POST, 1);
		//POST变量
		curl_setopt($ch, CURLOPT_POSTFIELDS, $pushkey);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$file_contents = curl_exec($ch);
		curl_close($ch);
		if ($file_contents == 'true') {
			$ok = 1;
		} else {
			echo "<script>alert('抱歉，查无此人！');location.href='dnfpayment.php';</script>";
		}
	}
	if ($ok == 1) {
		//sql_query("UPDATE users SET seedbonus = seedbonus - $PayMoney WHERE id = " . $CURUSER['id'] . "");
		writeBonusComment($CURUSER['id'], "用于充值《{$itemname}》游戏代币消耗了 $PayMoney 个魔力值");
		echo "<script>alert('恭喜，成功充值 $PayGold 个《{$itemname}》游戏代币！');location.href='dnfpayment.php?pay=1';</script>";
	} else {
		echo "<script>alert('抱歉，充值失败！');location.href='dnfpayment.php';</script>";
	}
} elseif ($_POST['big']) {
	if ($username == '') {
		echo "<script>alert('错误！请输入毒奶粉用户名！');location.href='dnfpayment.php';</script>";
	}
	$check = 0;
	if ($_POST['bignum'] == '' || $_POST['bignum'] < 1) {
		echo "<script>alert('错误！请输入大于 1 的正整数');location.href='dnfpayment.php';</script>";
		$check = 1;
	}
	switch ($class) {
		case UC_POWER_USER:
			$rate = "1.3";
			break;
		case UC_ELITE_USER:
			$rate = "1.4";
			break;
		case UC_CRAZY_USER:
			$rate = "1.5";
			break;
		case UC_INSANE_USER:
			$rate = "1.6";
			break;
		case UC_VETERAN_USER:
			$rate = "1.7";
			break;
		case UC_EXTREME_USER:
			$rate = "1.8";
			break;
		case UC_ULTIMATE_USER:
			$rate = "1.9";
			break;
		case UC_NEXUS_MASTER:
			$rate = "2";
			break;
		default :
			$rate = "1";
	}
	if ($class >= UC_VIP) {
		$rate = "2";
	}
	$PayMoney = $_POST['bignum'];
	$PayGold = $PayMoney * $rate;
	if ($PayMoney > $bonus) {
		echo "<script>alert('错误！荣誉值不足，请重新输入！');location.href='dnfpayment.php';</script>";
	} else {
		$do = 1;
	}
	if ($do == 1 && $check == 0) {
		//充值逻辑
		$pushkey['pushkey'] = encrypt(json_encode(array("username" => "$username", "number" => "$PayGold")));
		$url = "http://115.182.84.121/dnfpayto.php";
		$ch = curl_init();
		$timeout = 30;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//POST数据
		curl_setopt($ch, CURLOPT_POST, 1);
		//POST变量
		curl_setopt($ch, CURLOPT_POSTFIELDS, $pushkey);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$file_contents = curl_exec($ch);
		curl_close($ch);
		if ($file_contents == 'true') {
			$ok = 1;
		} else {
			echo "<script>alert('抱歉，查无此人！');location.href='dnfpayment.php';</script>";
		}
	}
	if ($ok == 1) {
		//sql_query("UPDATE users SET seedbonus = seedbonus - $PayMoney WHERE id = " . $CURUSER['id'] . "");
		writeBonusComment($CURUSER['id'], "用于充值《{$itemname}》游戏代币消耗了 $PayMoney 个荣誉值");
		echo "<script>alert('恭喜，成功充值 $PayGold 个《{$itemname}》游戏代币！');location.href='dnfpayment.php?pay=1';</script>";
	} else {
		echo "<script>alert('抱歉，充值失败！');location.href='dnfpayment.php';</script>";
	}
}
stdhead("毒奶粉充值系统");
?>
<div>
	<h1 style="width: 95%">毒奶粉充值系统</h1>
	<table border="1" cellpadding="10" cellspacing="0" width="95%"><tbody><tr><td>
					<ul>
						<li>本系统可支付毒奶粉<?= PROJECTNAME ?>社区服的游戏代币购买</li>
						<li><font style="color: #5D478B">魔力值</font>兑换游戏代币比例为100：1，即100点魔力值=1点游戏代币</li>
						<li><font style="color: #CD0000">荣誉值</font>兑换游戏代币比例为1：1，即1点荣誉值=1点游戏代币</li>
						<li><?= get_user_class_name_zh(3, FALSE, TRUE) ?>兑换倍率为 1.3，以后等级依次加 0.1，最高为 2；<?= get_user_class_name_zh(11, FALSE, TRUE) ?>及以上倍率一律为 2</li>
					</ul>
				</td></tr></tbody></table>
</div><br />
<div>
	<form method="POST" onsubmit="if (confirm('你确定要这样做吗？')) {
				return true;
			} else {
				return false;
			}">
		<?php echo "毒奶粉用户名：" ?><input type="text" name="username" style="width: 100px" /><br /><br />
		<?php echo "魔力充值数量：" ?><input type="text" name="bonusnum" style="width: 100px" onkeyup="this.value = this.value.replace(/\D/g, '');" />
		<input type="submit" name="bonus" value="魔力充值" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<?php echo "荣誉充值数量：" ?><input type="text" name="bignum" style="width: 100px" onkeyup="this.value = this.value.replace(/\D/g, '');" />
		<input type="submit" name="big" value="荣誉充值" /><br /><br />
	</form>
</div>
<?php
stdfoot();
