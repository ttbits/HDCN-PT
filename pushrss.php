<?php

require "include/bittorrent.php";
dbconn();

function hex_esc($matches) {
	return sprintf("%02x", ord($matches[0]));
}

//$url = explode(",", decrypt($_GET['pushkey']));//字符串解密传输
$url = json_decode(decrypt($_GET['pushkey']));
$dllink = false;
$passkey = $url->passkey;
if ($passkey) {
	$res = sql_query("SELECT id, enabled, parked FROM users WHERE passkey=" . sqlesc(mysql_real_escape_string($passkey)) . " LIMIT 1");
	$user = mysql_fetch_array($res);
	if (!$user)
		die("无效的PassKey");
	elseif ($user['enabled'] == 'no' || $user['parked'] == 'yes')
		die("帐号被禁用");
	elseif ($url->linktype == 'dl')
		$dllink = true;
	$url = get_protocol_prefix() . $BASEURL;
	$year = substr($datefounded, 0, 4);
	$yearfounded = ($year ? $year : 2007);
	$copyright = "Copyright (c) " . $SITENAME . " " . (date("Y") != $yearfounded ? $yearfounded . "-" : "") . date("Y") . ", all rights reserved";
	header("Content-type: text/xml");
	print("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
	print('<rss version="2.0">');
	print('<channel><title>' . addslashes($SITENAME) . '</title><link><![CDATA[' . $url . ']]></link><description><![CDATA[' . addslashes('Latest torrents from ' . $SITENAME . ' - ' . htmlspecialchars($SLOGAN)) . ']]></description><language>zh-cn</language><copyright>' . $copyright . '</copyright><managingEditor>' . $SITENAME . ' Admin</managingEditor><webMaster>' . $SITENAME . ' Webmaster</webMaster><pubDate>' . date('r') . '</pubDate><generator>' . PROJECTNAME . ' RSS Generator</generator><docs><![CDATA[http://www.rssboard.org/rss-specification]]></docs><ttl>5</ttl><image><url><![CDATA[' . $url . '/pic/rss_logo.jpg' . ']]></url><title>' . addslashes($SITENAME) . '</title><link><![CDATA[' . $url . ']]></link><width>100</width><height>100</height><description>' . addslashes($SITENAME) . '</description></image>');
	print('');
	$ress = sql_query("SELECT torrents.id, torrents.category, torrents.name, torrents.small_descr, torrents.descr, torrents.info_hash, torrents.size, torrents.added, torrents.anonymous, users.username AS username, categories.id AS cat_id, categories.name AS cat_name FROM torrents LEFT JOIN categories ON category = categories.id LEFT JOIN users ON torrents.owner = users.id WHERE torrents.id IN (SELECT torrentid FROM truckmarks WHERE userid =" . $user['id'] . ")") or sqlerr(__FILE__, __LINE__);
	while ($row = mysql_fetch_array($ress)) {
		$title = "";
		if ($row['anonymous'] == 'yes')
			$author = 'anonymous';
		else
			$author = $row['username'];
		$itemurl = $url . "/details.php?id=" . $row['id'];
		if ($dllink)
			$itemdlurl = $url . "/download.php?id=" . $row['id'] . "&amp;passkey=" . rawurlencode($passkey);
		else
			$itemdlurl = $url . "/download.php?id=" . $row['id'];
		if ($_GET['icat'])
			$title .= "[" . $row['cat_name'] . "]";
		$title .= $row['name'];
		if ($_GET['ismalldescr'] && $row['small_descr'])
			$title .= "[" . $row['small_descr'] . "]";
		if ($_GET['isize'])
			$title .= "[" . mksize($row['size']) . "]";
		if ($_GET['iuplder'])
			$title .= "[" . $author . "]";
		$content = format_comment($row['descr'], true, false, false, false);
		print('<item><title><![CDATA[' . $title . ']]></title><link>' . $itemurl . '</link><description><![CDATA[' . $content . ']]></description>');
		print('<author>' . $author . '@' . $_SERVER['HTTP_HOST'] . ' (' . $author . ')</author>');
		print('<category domain="' . $url . '/torrents.php?cat=' . $row['cat_id'] . '">' . $row['cat_name'] . '</category><comments><![CDATA[' . $url . '/details.php?id=' . $row['id'] . '&cmtpage=0#startcomments]]></comments><enclosure url="' . $itemdlurl . '" length="' . $row['size'] . '" type="application/x-bittorrent" /><guid isPermaLink="false">' . preg_replace_callback('/./s', 'hex_esc', hash_pad($row['info_hash'])) . '</guid><pubDate>' . date('r', strtotime($row['added'])) . '</pubDate></item>');
	}
	print('</channel></rss>');
}