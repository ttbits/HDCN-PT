<?php
require "include/bittorrent.php";
dbconn();
loggedinorreturn();
require_once(get_langfile_path());
if (get_user_class() < UC_ADMINISTRATOR)
	stderr("错误", "权限不足");
$done = false;
$clientdone = false;
if ($_POST['single']) {
	$cachename = $_POST["cachename"];
	if ($cachename == "") {
		stderr("错误", "必须输入缓存名");
	} else {
		if ($_POST['multilang'] == 'yes') {
			$Cache->delete_value($cachename, true);
		} else {
			$Cache->delete_value($cachename);
		}
		$done = true;
	}
}
if ($_POST['client']) {
	if ($_POST['clearallowclient'] == 'yes') {
		$count = get_row_count("agent_allowed_family");
		for ($i = 1; $i <= $count; $i++) {
			$Cache->delete_value("allowed_client_exception_family_{$i}_list");
		}
		$clientdone = true;
	}
}
stdhead();
?>
<h1><?php echo $lang_clearcache['head_clearcache'] ?></h1>
<?php
if ($done) {
	print ("<p align=center><font class=striking>缓存已清除</font></p>");
} elseif ($clientdone) {
	print ("<p align=center><font class=striking>已更新允许的BT客户端</font></p>");
}
?>
<form method=post>
	<table border=1 cellspacing=0 cellpadding=5>
		<tr><td class=rowhead><?php echo $lang_clearcache['text_cachename'] ?></td><td><input type=text name=cachename size=40></td></tr>
		<tr><td class=rowhead><?php echo $lang_clearcache['text_multilang'] ?></td><td><input type=checkbox name=multilang><?php echo $lang_clearcache['text_yes'] ?></td></tr>
		<tr><td colspan=2 align=center><input type=submit value="<?php echo $lang_clearcache['submit_ok'] ?>" name="single" class=btn></td></tr>
	</table><br />
	<table border=1 cellspacing=0 cellpadding=5>
		<tr><td class=rowhead>更新允许的BT客户端</td></tr>
		<tr><td colspan=2 align=center><input type="hidden" name="clearallowclient" value="yes"><input type=submit value="<?php echo $lang_clearcache['submit_ok'] ?>" name="client" class=btn></td></tr>
	</table>
</form>
<?php
stdfoot();
