<?php

require "include/bittorrent.php";
dbconn();
require(get_langfile_path("", true));
loggedinorreturn();
checkserver();

function puke() {
	global $CURUSER;
	$msg = "User " . $CURUSER["username"] . " (ID：" . $CURUSER["id"] . ")在试图攻击系统 IP : " . getip();
	write_log($msg, 'mod');
	stderr("错误", "权限拒绝");
}

if (get_user_class() < $prfmanage_class) {
	puke();
}

$action = $_POST["action"];
if ($action == "confirmuser") {
	$userid = $_POST["userid"];
	$confirm = $_POST["confirm"];
	sql_query('UPDATE `users` SET `status` = \'' . mysql_real_escape_string($confirm) . '\', `info` = NULL WHERE `id` = ' . mysql_real_escape_string($userid) . ' LIMIT 1;') or sqlerr(__FILE__, __LINE__);
	header("Location: " . get_protocol_prefix() . "$BASEURL/unco.php?status=1");
	die;
}
if ($action == "edituser") {
	$userid = $_POST["userid"];
	$class = 0 + $_POST["class"];
	$vip_added = ($_POST["vip_added"] == 'yes' ? 'yes' : 'no');
	$vip_until = ($_POST["vip_until"] ? $_POST["vip_until"] : '0000-00-00 00:00:00');

	$warned = $_POST["warned"];
	$warnlength = 0 + $_POST["warnlength"];
	$warnpm = $_POST["warnpm"];
	$title = $_POST["title"];
	$avatar = $_POST["avatar"];
	$signature = $_POST["signature"];

	$enabled = $_POST["enabled"];
	$uploadpos = $_POST["uploadpos"];
	$downloadpos = $_POST["downloadpos"];
	$noad = $_POST["noad"];
	$noaduntil = $_POST["noaduntil"];
	$privacy = $_POST["privacy"];
	$forumpost = $_POST["forumpost"];
	$chpassword = $_POST["chpassword"];
	$passagain = $_POST["passagain"];

	$supportlang = $_POST["supportlang"];
	$support = $_POST["support"];
	$supportfor = $_POST["supportfor"];

	$moviepicker = $_POST["moviepicker"];
	$pickfor = $_POST["pickfor"];
	$stafffor = $_POST["staffduties"];

	if (!is_valid_id($userid) || !is_valid_user_class($class)) {
		stderr("错误", "不存在的用户ID或级别");
	}
	if (get_user_class() <= $class && get_user_class() != UC_STAFFLEADER) {
		stderr("错误", "你没有权限改变用户级别" . get_user_class_name_zh($class, false, false, true) . "");
	}
	$res = sql_query("SELECT * FROM users WHERE id = $userid") or sqlerr(__FILE__, __LINE__);
	$arr = mysql_fetch_assoc($res) or puke();

	$curenabled = $arr["enabled"];
	$curparked = $arr["parked"];
	$curuploadpos = $arr["uploadpos"];
	$curdownloadpos = $arr["downloadpos"];
	$curforumpost = $arr["forumpost"];
	$curclass = $arr["class"];
	$curwarned = $arr["warned"];

	$updateset[] = "stafffor = " . sqlesc($stafffor);
	$updateset[] = "pickfor = " . sqlesc($pickfor);
	$updateset[] = "picker = " . sqlesc($moviepicker);
	//$updateset[] = "enabled = " . sqlesc($enabled);
	$updateset[] = "uploadpos = " . sqlesc($uploadpos);
	$updateset[] = "downloadpos = " . sqlesc($downloadpos);
	$updateset[] = "forumpost = " . sqlesc($forumpost);
	$updateset[] = "avatar = " . sqlesc($avatar);
	$updateset[] = "signature = " . sqlesc($signature);
	$updateset[] = "title = " . sqlesc($title);
	$updateset[] = "support = " . sqlesc($support);
	$updateset[] = "supportfor = " . sqlesc($supportfor);
	$updateset[] = "supportlang = " . sqlesc($supportlang);

	if (get_user_class <= $cruprfmanage_class) {
		$modcomment = $arr["modcomment"];
	}
	if (get_user_class() >= $cruprfmanage_class) {
		$email = $_POST["cuemail"];
		$cuhr = $_POST["cuhr"];
		$username = $_POST["cusername"];
		$modcomment = $_POST["modcomment"];
		$downloaded = $_POST["downloaded"];
		$ori_downloaded = $_POST["ori_downloaded"];
		$uploaded = $_POST["uploaded"];
		$ori_uploaded = $_POST["ori_uploaded"];
		$bonus = $_POST["bonus"];
		$ori_bonus = $_POST["ori_bonus"];
		$invites = $_POST["invites"];
		$added = sqlesc(date("Y-m-d H:i:s"));
		if ($arr['hr'] != $cuhr) {
			$updateset[] = "hr = " . sqlesc($cuhr);
			$modcomment = date("Y-m-d") . " - H&R数量被 $CURUSER[username] 从 $arr[hr] 改成 $cuhr\n" . $modcomment;
			$subject = sqlesc("H&R数量被改变");
			$msg = sqlesc("你的H&R数量被管理员从" . $arr['hr'] . "改为" . $cuhr . "。管理员：" . $CURUSER[username]);
			sql_query("INSERT INTO messages (sender, receiver, subject, msg, added) VALUES(0, $userid, $subject, $msg, $added)") or sqlerr(__FILE__, __LINE__);
		}
		if ($arr['email'] != $email) {
			$updateset[] = "email = " . sqlesc($email);
			$modcomment = date("Y-m-d") . " - 邮箱被 $CURUSER[username] 从 $arr[email] 改成 $email\n" . $modcomment;
			$subject = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_email_change']);
			$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_your_email_changed_from'] . $arr['email'] . $lang_modtask_target[get_user_lang($userid)]['msg_to_new'] . $email . $lang_modtask_target[get_user_lang($userid)]['msg_by'] . $CURUSER[username]);
			sql_query("INSERT INTO messages (sender, receiver, subject, msg, added) VALUES(0, $userid, $subject, $msg, $added)") or sqlerr(__FILE__, __LINE__);
		}
		if ($arr['username'] != $username) {
			$updateset[] = "username = " . sqlesc($username);
			$modcomment = date("Y-m-d") . " - 用户名被 $CURUSER[username] 从 $arr[username] 改成 $username\n" . $modcomment;
			$subject = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_username_change']);
			$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_your_username_changed_from'] . $arr['username'] . $lang_modtask_target[get_user_lang($userid)]['msg_to_new'] . $username . $lang_modtask_target[get_user_lang($userid)]['msg_by'] . $CURUSER[username]);
			sql_query("INSERT INTO messages (sender, receiver, subject, msg, added) VALUES(0, $userid, $subject, $msg, $added)") or sqlerr(__FILE__, __LINE__);
			record_op_log($CURUSER['id'], $arr['id'], htmlspecialchars($arr['username']), "change", $arr['username'] . " --更名为--" . $username);
		}
		if ($ori_downloaded != $downloaded) {
			$updateset[] = "downloaded = " . sqlesc($downloaded);
			$modcomment = date("Y-m-d") . " - 下载量被 $CURUSER[username] 从 $arr[downloaded] 改成 $downloaded\n" . $modcomment;
			$subject = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_downloaded_change']);
			$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_your_downloaded_changed_from'] . mksize($arr['downloaded']) . $lang_modtask_target[get_user_lang($userid)]['msg_to_new'] . mksize($downloaded) . $lang_modtask_target[get_user_lang($userid)]['msg_by'] . $CURUSER[username]);
			sql_query("INSERT INTO messages (sender, receiver, subject, msg, added) VALUES(0, $userid, $subject, $msg, $added)") or sqlerr(__FILE__, __LINE__);
		}
		if ($ori_uploaded != $uploaded) {
			$updateset[] = "uploaded = " . sqlesc($uploaded);
			$modcomment = date("Y-m-d") . " - 上传量被 $CURUSER[username] 从 $arr[uploaded] 改成 $uploaded\n" . $modcomment;
			$subject = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_uploaded_change']);
			$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_your_uploaded_changed_from'] . mksize($arr['uploaded']) . $lang_modtask_target[get_user_lang($userid)]['msg_to_new'] . mksize($uploaded) . $lang_modtask_target[get_user_lang($userid)]['msg_by'] . $CURUSER[username]);
			sql_query("INSERT INTO messages (sender, receiver, subject, msg, added) VALUES(0, $userid, $subject, $msg, $added)") or sqlerr(__FILE__, __LINE__);
		}
		if ($ori_bonus != $bonus) {
			$updateset[] = "seedbonus = " . sqlesc($bonus);
			$modcomment = date("Y-m-d") . " - 魔力值被 $CURUSER[username] 从 $arr[seedbonus] 改成 $bonus\n" . $modcomment;
			$subject = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_bonus_change']);
			$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_your_bonus_changed_from'] . $arr['seedbonus'] . $lang_modtask_target[get_user_lang($userid)]['msg_to_new'] . $bonus . $lang_modtask_target[get_user_lang($userid)]['msg_by'] . $CURUSER[username]);
			sql_query("INSERT INTO messages (sender, receiver, subject, msg, added) VALUES(0, $userid, $subject, $msg, $added)") or sqlerr(__FILE__, __LINE__);
		}
		if ($arr['invites'] != $invites) {
			$updateset[] = "invites = " . sqlesc($invites);
			if ($invites < 0) {
				$invites = '∞';
			}
			$modcomment = date("Y-m-d") . " - 邀请数被 $CURUSER[username] 从 $arr[invites] 改成 $invites\n" . $modcomment;
			$subject = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_invite_change']);
			$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_your_invite_changed_from'] . $arr['invites'] . $lang_modtask_target[get_user_lang($userid)]['msg_to_new'] . $invites . $lang_modtask_target[get_user_lang($userid)]['msg_by'] . $CURUSER[username]);
			sql_query("INSERT INTO messages (sender, receiver, subject, msg, added) VALUES(0, $userid, $subject, $msg, $added)") or sqlerr(__FILE__, __LINE__);
		}
	}
	if (get_user_class() >= UC_MODERATOR) {
		$nonass = $_POST['newuser'];
		$updateset[] = "newuser = " . sqlesc($nonass);
	}
	if (get_user_class() == UC_STAFFLEADER) {
		$donor = $_POST["donor"];
		$donated = $_POST["donated"];
		$donated_cny = $_POST["donated_cny"];
		$this_donated_usd = $donated - $arr["donated"];
		$this_donated_cny = $donated_cny - $arr["donated_cny"];
		$memo = sqlesc(htmlspecialchars($_POST["donation_memo"]));

		if ($donated != $arr[donated] || $donated_cny != $arr[donated_cny]) {
			$added = sqlesc(date("Y-m-d H:i:s"));
			sql_query("INSERT INTO funds (usd, cny, user, added, memo) VALUES ($this_donated_usd, $this_donated_cny, $userid, $added, $memo)") or sqlerr(__FILE__, __LINE__);
			$updateset[] = "donated = " . sqlesc($donated);
			$updateset[] = "donated_cny = " . sqlesc($donated_cny);
		}

		$updateset[] = "donor = " . sqlesc($donor);
	}

	if ($chpassword != "" AND $passagain != "") {
		unset($passupdate);
		$passupdate = false;

		if ($chpassword == $username OR strlen($chpassword) > 40 OR strlen($chpassword) < 6 OR $chpassword != $passagain)
			$passupdate = false;
		else
			$passupdate = true;
	}

	if ($passupdate) {
		$sec = mksecret();
		$passhash = md5($sec . $chpassword . $sec);
		$updateset[] = "secret = " . sqlesc($sec);
		$updateset[] = "passhash = " . sqlesc($passhash);
	}
	if (get_user_class() != UC_STAFFLEADER) {
		if ($curclass >= get_user_class()) {
			puke();
		}
	}

	if ($curclass != $class) {
		$what = ($class > $curclass ? $lang_modtask_target[get_user_lang($userid)]['msg_promoted'] : $lang_modtask_target[get_user_lang($userid)]['msg_demoted']);
		$subject = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_class_change']);
		$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_you_have_been'] . $what . $lang_modtask_target[get_user_lang($userid)]['msg_to'] . get_user_class_name_zh($class) . $lang_modtask_target[get_user_lang($userid)]['msg_by'] . $CURUSER[username]);
		$added = sqlesc(date("Y-m-d H:i:s"));
		sql_query("INSERT INTO messages (sender, receiver, subject, msg, added) VALUES(0, $userid, $subject, $msg, $added)") or sqlerr(__FILE__, __LINE__);
		$updateset[] = "class = $class";
		$what = ($class > $curclass ? "升级" : "降级");
		$modcomment = date("Y-m-d") . " - $what 至 '" . get_user_class_name_zh($class) . "'，操作人：$CURUSER[username]\n" . $modcomment;
	}
	if ($class == UC_VIP) {
		$updateset[] = "vip_added = " . sqlesc($vip_added);
		if ($vip_added == 'yes')
			$updateset[] = "vip_until = " . sqlesc($vip_until);
		$subject = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_your_vip_status_changed']);
		$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_vip_status_changed_by'] . $CURUSER[username]);
		$added = sqlesc(date("Y-m-d H:i:s"));
		sql_query("INSERT INTO messages (sender, receiver, subject, msg, added) VALUES (0, $userid, $subject, $msg, $added)") or sqlerr(__FILE__, __LINE__);
		$modcomment = date("Y-m-d") . " - 被 $CURUSER[username] 升级到VIP" . ($vip_added == 'yes' ? " 直到：" . $vip_until : "") . "\n" . $modcomment;
	}

	if ($warned && $curwarned != $warned) {
		$updateset[] = "warned = " . sqlesc($warned);
		$updateset[] = "warneduntil = '0000-00-00 00:00:00'";

		if ($warned == 'no') {
			$modcomment = date("Y-m-d") . " - 警告被 $CURUSER[username] 解除\n" . $modcomment;
			$subject = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_warn_removed']);
			$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_your_warning_removed_by'] . $CURUSER['username'] . ".");
		}

		$added = sqlesc(date("Y-m-d H:i:s"));
		sql_query("INSERT INTO messages (sender, receiver, subject, msg, added) VALUES (0, $userid, $subject, $msg, $added)") or sqlerr(__FILE__, __LINE__);
	} elseif ($warnlength) {
		if ($warnlength == 255) {
			$modcomment = date("Y-m-d") . " - 被 " . $CURUSER['username'] . " 警告，原因为： $warnpm\n" . $modcomment;
			$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_you_are_warned_by'] . $CURUSER[username] . "." . ($warnpm ? $lang_modtask_target[get_user_lang($userid)]['msg_reason'] . $warnpm : ""));
			$updateset[] = "warneduntil = '0000-00-00 00:00:00'";
		} elseif ($warnlength == 33) {
			$warneduntil = date("Y-m-d H:i:s", (strtotime(date("Y-m-d H:i:s")) + 86400 * 3));
			$dur = "3天";
			$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_you_are_warned_for'] . $dur . $lang_modtask_target[get_user_lang($userid)]['msg_by'] . $CURUSER['username'] . "." . ($warnpm ? $lang_modtask_target[get_user_lang($userid)]['msg_reason'] . $warnpm : ""));
			$modcomment = date("Y-m-d") . " - 被 " . $CURUSER['username'] . " 警告 $dur ，原因为： $warnpm\n" . $modcomment;
			$updateset[] = "warneduntil = '$warneduntil'";
		} elseif ($warnlength == 11) {
			$warneduntil = date("Y-m-d H:i:s", (strtotime(date("Y-m-d H:i:s")) + 86400));
			$dur = "1天";
			$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_you_are_warned_for'] . $dur . $lang_modtask_target[get_user_lang($userid)]['msg_by'] . $CURUSER['username'] . "." . ($warnpm ? $lang_modtask_target[get_user_lang($userid)]['msg_reason'] . $warnpm : ""));
			$modcomment = date("Y-m-d") . " - 被 " . $CURUSER['username'] . " 警告 $dur ，原因为： $warnpm\n" . $modcomment;
			$updateset[] = "warneduntil = '$warneduntil'";
		} else {
			$warneduntil = date("Y-m-d H:i:s", (strtotime(date("Y-m-d H:i:s")) + $warnlength * 604800));
			$dur = $warnlength . $lang_modtask_target[get_user_lang($userid)]['msg_week'] . ($warnlength > 1 ? $lang_modtask_target[get_user_lang($userid)]['msg_s'] : "");
			$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_you_are_warned_for'] . $dur . $lang_modtask_target[get_user_lang($userid)]['msg_by'] . $CURUSER['username'] . "." . ($warnpm ? $lang_modtask_target[get_user_lang($userid)]['msg_reason'] . $warnpm : ""));
			$modcomment = date("Y-m-d") . " - 被 " . $CURUSER['username'] . " 警告 $dur ，原因为： $warnpm\n" . $modcomment;
			$updateset[] = "warneduntil = '$warneduntil'";
		}
		$subject = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_you_are_warned']);
		$added = sqlesc(date("Y-m-d H:i:s"));
		sql_query("INSERT INTO messages (sender, receiver, subject, msg, added) VALUES (0, $userid, $subject, $msg, $added)") or sqlerr(__FILE__, __LINE__);
		$updateset[] = "warned = 'yes', timeswarned = timeswarned + 1, lastwarned = $added, warnedby = $CURUSER[id]";
	}
	/*
	  //封禁用户功能挪到delactadmin.php中执行
	  if ($enabled != $curenabled) {
	  if ($enabled == 'yes') {
	  $modcomment = date("Y-m-d") . " - 账号被 " . $CURUSER['username'] . " 复活\n" . $modcomment;
	  if (get_single_value("users", "class", "WHERE id = " . sqlesc($userid)) == UC_PEASANT) {
	  $length = 30 * 86400;
	  $until = sqlesc(date("Y-m-d H:i:s", (strtotime(date("Y-m-d H:i:s")) + $length)));
	  sql_query("UPDATE users SET enabled = 'yes', leechwarn = 'yes', leechwarnuntil = $until WHERE id = " . sqlesc($userid));
	  } else {
	  sql_query("UPDATE users SET enabled = 'yes', leechwarn = 'no' WHERE id = " . sqlesc($userid)) or sqlerr(__FILE__, __LINE__);
	  }
	  } else {
	  $modcomment = date("Y-m-d") . " - 帐号被 " . $CURUSER['username'] . " 禁用\n" . $modcomment;
	  }
	  }
	 *
	 */

	if ($arr['noad'] != $noad) {
		$updateset[] = 'noad = ' . sqlesc($noad);
		$modcomment = date("Y-m-d") . " - 被 " . $CURUSER['username'] . " 设置为不显示广告\n" . $modcomment;
	}
	if ($arr['noaduntil'] != $noaduntil) {
		$updateset[] = 'noaduntil = ' . sqlesc($noaduntil);
		$modcomment = date("Y-m-d") . " - 被 " . $CURUSER['username'] . " 设置为不显示广告，直到 $noaduntil\n" . $modcomment;
	}
	if ($privacy == "low" OR $privacy == "normal" OR $privacy == "strong")
		$updateset[] = "privacy = " . sqlesc($privacy);

	if ($_POST["resetkey"] == "yes") {
		$newpasskey = md5($arr['username'] . date("Y-m-d H:i:s") . $arr['passhash']);
		$updateset[] = "passkey = " . sqlesc($newpasskey);
	}
	if ($forumpost != $curforumpost) {
		if ($forumpost == 'yes') {
			$modcomment = date("Y-m-d") . " - 论坛发帖权限被 " . $CURUSER['username'] . " 开启\n" . $modcomment;
			$subject = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_posting_rights_restored']);
			$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_your_posting_rights_restored'] . $CURUSER['username'] . $lang_modtask_target[get_user_lang($userid)]['msg_you_can_post']);
			$added = sqlesc(date("Y-m-d H:i:s"));
			sql_query("INSERT INTO messages (sender, receiver, subject, msg, added) VALUES (0, $userid, $subject, $msg, $added)") or sqlerr(__FILE__, __LINE__);
		} else {
			$modcomment = date("Y-m-d") . " - 论坛发帖权限被 " . $CURUSER['username'] . " 禁用\n" . $modcomment;
			$subject = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_posting_rights_removed']);
			$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_your_posting_rights_removed'] . $CURUSER['username'] . $lang_modtask_target[get_user_lang($userid)]['msg_probable_reason']);
			$added = sqlesc(date("Y-m-d H:i:s"));
			sql_query("INSERT INTO messages (sender, receiver, subject, msg, added) VALUES (0, $userid, $subject, $msg, $added)") or sqlerr(__FILE__, __LINE__);
		}
	}
	if ($uploadpos != $curuploadpos) {
		if ($uploadpos == 'yes') {
			$modcomment = date("Y-m-d") . " - 上传权限被 " . $CURUSER['username'] . " 开启\n" . $modcomment;
			$subject = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_upload_rights_restored']);
			$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_your_upload_rights_restored'] . $CURUSER['username'] . $lang_modtask_target[get_user_lang($userid)]['msg_you_upload_can_upload']);
			$added = sqlesc(date("Y-m-d H:i:s"));
			sql_query("INSERT INTO messages (sender, receiver, subject, msg, added) VALUES (0, $userid, $subject, $msg, $added)") or sqlerr(__FILE__, __LINE__);
		} else {
			$modcomment = date("Y-m-d") . " - 上传权限被 " . $CURUSER['username'] . " 禁用\n" . $modcomment;
			$subject = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_upload_rights_removed']);
			$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_your_upload_rights_removed'] . $CURUSER['username'] . $lang_modtask_target[get_user_lang($userid)]['msg_probably_reason_two']);
			$added = sqlesc(date("Y-m-d H:i:s"));
			sql_query("INSERT INTO messages (sender, receiver, subject, msg, added) VALUES (0, $userid, $subject, $msg, $added)") or sqlerr(__FILE__, __LINE__);
		}
	}
	if ($downloadpos != $curdownloadpos) {
		if ($downloadpos == 'yes') {
			$modcomment = date("Y-m-d") . " - 下载权限被 " . $CURUSER['username'] . " 开启\n" . $modcomment;
			$subject = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_download_rights_restored']);
			$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_your_download_rights_restored'] . $CURUSER['username'] . $lang_modtask_target[get_user_lang($userid)]['msg_you_can_download']);
			$added = sqlesc(date("Y-m-d H:i:s"));
			sql_query("INSERT INTO messages (sender, receiver, subject, msg, added) VALUES (0, $userid, $subject, $msg, $added)") or sqlerr(__FILE__, __LINE__);
		} else {
			$modcomment = date("Y-m-d") . " - 下载权限被 " . $CURUSER['username'] . " 禁用\n" . $modcomment;
			$subject = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_download_rights_removed']);
			$msg = sqlesc($lang_modtask_target[get_user_lang($userid)]['msg_your_download_rights_removed'] . $CURUSER['username'] . $lang_modtask_target[get_user_lang($userid)]['msg_probably_reason_three']);
			$added = sqlesc(date("Y-m-d H:i:s"));
			sql_query("INSERT INTO messages (sender, receiver, subject, msg, added) VALUES (0, $userid, $subject, $msg, $added)") or sqlerr(__FILE__, __LINE__);
		}
	}

	$updateset[] = "modcomment = " . sqlesc($modcomment);

	sql_query("UPDATE users SET " . implode(", ", $updateset) . " WHERE id = $userid") or sqlerr(__FILE__, __LINE__);

	$returnto = htmlspecialchars($_POST["returnto"]);
	header("Location: " . get_protocol_prefix() . "$BASEURL/$returnto");
	die;
}
puke();
