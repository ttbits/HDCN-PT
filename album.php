<?php
require "include/bittorrent.php";
require_once("memcache.php");
dbconn();
loggedinorreturn();
secrity();
if ($showextinfo['imdb'] == 'yes') {
	require_once ("imdb/imdb.class.php");
	require_once("imdb/douban.php");
}

if (isset($_SESSION['albumsub']) && isset($_POST['albumsub']) && $_POST['albumsub'] == $_SESSION['albumsub']) {
	unset($_SESSION['albumsub']);
}
//添加专辑
if ($_POST['albumsubmit']) {
	if ($_POST['albumname'] == '') {
		stderr("错误", "请填写专辑名称");
	}
	$name = $_POST['albumname'];
	$small_descr = sqlesc($_POST['albumsmall_descr']);
	$checked = sqlesc($_POST['visited']);
	$res = sql_query("SELECT name FROM album");
	while ($row = mysql_fetch_array($res)) {
		if ($row['name'] == $name) {
			stderr("错误", "该专辑名称已经存在");
		}
	}
	sql_query("INSERT INTO album (userid, name, small_descr, visited, added) VALUE ($CURUSER[id], " . sqlesc($name) . ", $small_descr, $checked, " . sqlesc(date("Y-m-d H:i:s", time())) . ")") or sqlerr(__FILE__, __LINE__);
	echo "<script>alert('添加专辑成功');location.href='?action=my';</script>";
}
//编辑专辑
if ($_POST['albumedit']) {
	if ($_POST['albumname'] == '') {
		stderr("错误", "请填写专辑名称");
	}
	$name = $_POST['albumname'];
	$small_descr = sqlesc($_POST['albumsmall_descr']);
	$checked = sqlesc($_POST['visited']);
	$cid = $_POST['albumid'];
	sql_query("UPDATE album SET name = " . sqlesc($name) . ", small_descr = $small_descr, visited = $checked WHERE id = $cid") or sqlerr(__FILE__, __LINE__);
	echo "<script>alert('编辑专辑成功');location.href='?action=view&cid=$cid';</script>";
}
stdhead("专辑");
?>
<script type="text/javascript">
	function checkAll()
	{
		$(".checkbox").each(function () {
			this.checked = true;
		});
	}

	function reverseCheck()
	{
		$(".checkbox").each(function () {
			this.checked = !this.checked;
		});
	}

	function albumfav(albumid) {
		var result = ajax.gets('albumfav.php?albumid=' + albumid);
		afstatus(result, albumid);
	}
	function afstatus(status, albumid) {
		if (status == "added") {
			document.getElementById("album_" + albumid).innerHTML = "<img class = \"bookmark\" src=\"pic/trans.gif\" alt=\"Bookmarked\" />";
			$("#tid_" + albumid).addClass("torrent_table bookmarks");
		} else if (status == "deleted") {
			document.getElementById("album_" + albumid).innerHTML = "<img class=\"delbookmark\" src=\"pic/trans.gif\" alt=\"Unbookmarked\" />";
			$("#tid_" + albumid).removeClass("bookmarks");
		}
	}
</script>
<div>
	<form method="get">专辑名：<input type="text" name="album" style="width: 400px"><input type="submit" value="查询"></form><br />
	<table style="width: 95%"><tr><a href="album.php">全部专辑</a>&nbsp; | &nbsp;<a href="?action=add">添加专辑</a>&nbsp; | &nbsp;<a href="?action=fav">收藏专辑</a>&nbsp; | &nbsp;<a href="?action=my">我的专辑</a>&nbsp;</tr></table>
</div>
<?php
if (get_user_class() >= UC_MODERATOR) {

	function begin_table_album($fullwidth = false, $padding = 5) {
		$width = "";
		if ($fullwidth)
			$width .= " width=50%";
		return("<table class='main" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'><tr align=\"center\"><form method=\"POST\" onsubmit=\"if(confirm('确定要删除吗？')){return true;}else{return false;}\"></tr>");
	}

	function end_table_album() {
		return("<tr align=\"center\"><td class=\"rowfollow\" colspan=\"10\"><input type=\"button\" value=\"全选\" onclick=\"checkAll();\"><input type=\"button\" value=\"反选\" onclick=\"reverseCheck();\"> | <input type=\"submit\" id=\"delete\" value=\"删除\"></form></td></tr></table>");
	}

	function bjtable_album($res, $frame_caption) {
		global $CURUSER, $viewanonymous_class;
		if (mysql_num_rows($res) <= 0) {
			stdmsg("错误", "暂无专辑");
			stdfoot();
			die;
		}
		$htmlout = '';
		$htmlout .= begin_frame_album($frame_caption, true);
		$htmlout .= begin_table_album();
		$htmlout .="<tr><td class='colhead' align='left'>专辑名称</td><td class='colhead' align='center'>所属用户</td><td class='colhead' align='right'>公开</td><td class='colhead' align='right'>行为</td></tr>";
		while ($a = mysql_fetch_assoc($res)) {
			$row = sql_query("SELECT cid FROM albumfav WHERE id = $a[id] AND userid = $CURUSER[id]");
			$anonymous = mysql_fetch_array(sql_query("SELECT anon FROM users WHERE id = " . $a['userid']));
			$act = "<a id='album_$a[id]' href='javascript:albumfav($a[id])'>" . get_torrent_album_state($CURUSER['id'], $a['id']) . "</a>";
			if (mysql_num_rows($row) == 1) {
				$htmlout .="<tr id='tid_$a[id]' class='torrent_table bookmarks'>";
			} else {
				$htmlout .="<tr id='tid_$a[id]' class='torrent_table'>";
			}
			$htmlout .= "<td style='height:35px;width:750px'><a href='?action=view&cid=$a[id]'><b style='font-size:10pt'>$a[name]</b></a><span style='float:right'>" . ($a['userid'] == $CURUSER['id'] || get_user_class() >= UC_MODERATOR ? "<a href='?action=edit&cid=$a[id]'>[编辑]</a>&nbsp;" : "") . "{$act}</span></td>"//专辑名称
					. "<td align=center>" . ($anonymous['anon'] == 'no' ? get_username($a['userid']) : ($anonymous['anon'] == 'yes' && get_user_class() >= $viewanonymous_class ? "<i>匿名</i><br />(" . get_username($a['userid']) . ")" : "<i>匿名</i>")) . "</td>"//所属用户
					. "<td align=center>" . ($a['visited'] == 'yes' ? "是" : "否") . "</td>"//公开
					. "<td class=\"rowfollow\"><input type=\"checkbox\" value=\"{$a['id']}\" name=\"albumids[]\" class=\"checkbox\"></td>"//行为
					. "</tr>";
		}
		$htmlout .= end_table_album();
		$htmlout .= end_frame_album();
		return $htmlout;
	}

} else {

	function begin_table_album($fullwidth = false, $padding = 5) {
		$width = "";
		if ($fullwidth)
			$width .= " width=50%";
		return("<table class='main" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'><tr align=\"center\"></tr>");
	}

	function end_table_album() {
		return("<tr align=\"center\"></tr></table>");
	}

	function bjtable_album($res, $frame_caption) {
		global $CURUSER, $viewanonymous_class;
		if (mysql_num_rows($res) <= 0) {
			stdmsg("错误", "暂无专辑");
			stdfoot();
			die;
		}
		$htmlout = '';
		$htmlout .= begin_frame_album($frame_caption, true);
		$htmlout .= begin_table_album();
		$htmlout .="<tr><td class='colhead' align='left'>专辑名称</td><td class='colhead' align='center'>所属用户</td><td class='colhead' align='right'>公开</td></tr>";
		while ($a = mysql_fetch_assoc($res)) {
			$row = sql_query("SELECT cid FROM albumfav WHERE id = $a[id] AND userid = $CURUSER[id]");
			$anonymous = mysql_fetch_array(sql_query("SELECT anon FROM users WHERE id = " . $a['userid']));
			$act = "<a id='album_$a[id]' href='javascript:albumfav($a[id])'>" . get_torrent_album_state($CURUSER['id'], $a['id']) . "</a>";
			if (mysql_num_rows($row) == 1) {
				$htmlout .="<tr id='tid_$a[id]' class='torrent_table bookmarks'>";
			} else {
				$htmlout .="<tr id='tid_$a[id]' class='torrent_table'>";
			}
			$htmlout .= "<td style='height:35px;width:750px'><a href='?action=view&cid=$a[id]'><b style='font-size:10pt'>$a[name]</b></a><span style='float:right'>" . ($a['userid'] == $CURUSER['id'] || get_user_class() >= UC_MODERATOR ? "<a href='?action=edit&cid=$a[id]'>[编辑]</a>&nbsp;" : "") . "{$act}</span></td>"//专辑名称
					. "<td align=center>" . ($anonymous['anon'] == 'no' ? get_username($a['userid']) : ($anonymous['anon'] == 'yes' && get_user_class() >= $viewanonymous_class ? "<i>匿名</i><br />(" . get_username($a['userid']) . ")" : "<i>匿名</i>")) . "</td>"//所属用户
					. "<td align=center>" . ($a['visited'] == 'yes' ? "是" : "否") . "</td>"//公开
					. "</tr>";
		}
		$htmlout .= end_table_album();
		$htmlout .= end_frame_album();
		return $htmlout;
	}

}

function begin_frame_album($caption = "", $center = false, $padding = 5, $width = "100%", $caption_center = "left") {
	$tdextra = "";
	if ($center)
		$tdextra .= " align='center'";
	return(($caption ? "<h2 align='" . $caption_center . "'>" . $caption . "</h2>" : "") . "<table width='" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'>" . "<tr><td class='text' $tdextra>");
}

function end_frame_album() {
	return("</td></tr></table>");
}

$action = $_GET['action'];
$cid = 0 + $_GET['cid'];
if ($cid) {
	int_check($cid, FALSE);
}
if ($action == 'add') {//添加专辑
	$_SESSION['albumsub'] = mt_rand(1000000, 9999999);
	print("<form method=post><hr>公开显示：<input type=radio name=visited value=yes checked />是<input type=radio name=visited value=no />否<pre>专辑名称</pre><input type=text name=albumname style='width: 400px'><pre>专辑描述</pre><textarea id='sceditor' name='albumsmall_descr' onkeydown='ctrlenter(event,'compose','qr')' style='width: 750px;height: 300px'></textarea><br /><input type=hidden name=albumsub value=$_SESSION[albumsub]><input type=submit name=albumsubmit value=添加专辑></form>");
	print("<br />温馨提示：系统会自动清理新建7天后无任何内容的专辑哦~");
} elseif ($action == 'edit') {//编辑专辑
	$_SESSION['albumsub'] = mt_rand(1000000, 9999999);
	$row = mysql_fetch_array(sql_query("SELECT name, small_descr, visited FROM album WHERE id = $cid"));
	print("<form method=post><hr>公开显示：<input type=radio name=visited " . ($row['visited'] == 'yes' ? "checked" : "") . " value=yes />是<input type=radio name=visited " . ($row['visited'] == 'no' ? "checked" : "") . " value=no />否<pre>专辑名称</pre><input type=text name=albumname value='$row[name]' style='width: 400px'><pre>专辑描述</pre><textarea id='sceditor' name='albumsmall_descr' onkeydown='ctrlenter(event,'compose','qr')' style='width: 750px;height: 300px'>$row[small_descr]</textarea><br /><input type=hidden name=albumsub value=$_SESSION[albumsub]><input type=hidden name=albumid value=$cid><input type=submit name=albumedit value=编辑专辑></form>");
	print("<br />温馨提示：系统会自动清理新建7天后无任何内容的专辑哦~");
} elseif ($action == 'fav') {//收藏专辑
	$count = get_row_count("albumfav", "WHERE userid = " . $CURUSER['id']);
	list($pagertop, $pagerbottom, $limit) = pager(25, $count, "?");
	$res = sql_query("SELECT * FROM albumfav WHERE userid = $CURUSER[id] ORDER BY cid DESC $limit") or sqlerr(__FILE__, __LINE__);
	$HTMLOUT .= bjtable_album($res, "收藏专辑");
	$HTMLOUT .="<br /><br />";
	if ($count) {
		print($pagertop);
		print $HTMLOUT;
		print($pagerbottom);
	} else {
		print $HTMLOUT;
	}
} elseif ($action == 'my') {//我的专辑
	$count = get_row_count("album");
	list($pagertop, $pagerbottom, $limit) = pager(25, $count, "?");
	$res = sql_query("SELECT * FROM album WHERE userid = $CURUSER[id] ORDER BY id DESC $limit") or sqlerr(__FILE__, __LINE__);
	$HTMLOUT .= bjtable_album($res, "我的专辑");
	$HTMLOUT .="<br /><br />";
	if ($count) {
		print($pagertop);
		print $HTMLOUT;
		print($pagerbottom);
	} else {
		print $HTMLOUT;
	}
} elseif ($action == 'view') {//查看专辑
	$count = get_row_count("albumseries", "WHERE albumid = $cid");
	list($pagertop, $pagerbottom, $limit) = pager(25, $count, "?action=view&cid=$cid&");
	$res = sql_query("SELECT * FROM torrents WHERE id IN (SELECT torrentid FROM albumseries WHERE albumid = $cid) ORDER BY id DESC $limit") or sqlerr(__FILE__, __LINE__);
	$descr = mysql_fetch_array(sql_query("SELECT id, userid, name, small_descr, visited FROM album WHERE id = $cid"));
	if (empty($descr['id'])) {
		stdmsg("错误", "该专辑不存在");
		stdfoot();
		exit;
	}
	$act = "<a id='album_$cid' href='javascript:albumfav($cid)'>" . get_torrent_album_state($CURUSER['id'], $cid) . "</a>";
	$edit = ($descr['userid'] == $CURUSER['id'] || get_user_class() >= UC_MODERATOR) ? "<a href='?action=edit&cid=$cid'>[编辑专辑]</a>" : "";
	if ($descr['visited'] == 'yes' || $descr['userid'] == $CURUSER['id'] || get_user_class() >= UC_MODERATOR) {
		if (mysql_num_rows($res) > 0) {
			print("<hr>");
			print("<div style=\"margin-top: 10px; width: 98%\" align=\"center\">");
			if ($count) {
				print("<form method=\"POST\" style=\"width:80%; float:left\" onsubmit=\"if(confirm('确定要移除吗？')){return true;}else{return false;}\">");
				if ($descr['userid'] == $CURUSER['id']) {
					print("<input type=hidden name=userid style='width:10px' value=$descr[userid] />");
				}
				//print($pagertop);
				trucktorrenttable($res, "torrents");
				print($pagerbottom);
				print("</form>");
			} else {
				print("<form method=\"POST\" style=\"width:80%; float:left\" onsubmit=\"if(confirm('确定要移除吗？')){return true;}else{return false;}\">");
				if ($descr['userid'] == $CURUSER['id']) {
					print("<input type=hidden name=userid style='width:10px' value=$descr[userid] />");
				}
				trucktorrenttable($res, "torrents");
				print("</form>");
			}
			?>
			<div style="width: 250px; float: right; margin-left: 5px; padding-right: 5px">
				<?php if ($descr['userid'] == $CURUSER['id'] || get_user_class() >= UC_MODERATOR) { ?>
					<table style="width: 100%">
						<tr>
							<td class="colhead" style="font-size: 12pt">&nbsp;添加种子</td>
						</tr>
						<tr>
						<form method="post">
							<td align="center" style="height: 30px">种子ID：<input type="text" name="torrent_id"><input type="hidden" name="albumid" value="<?= $cid ?>"><input type="hidden" name="visited" value="<?= $descr['visited'] ?>"><input type="submit" value="添加"></td>
						</form>
						</tr>
					</table><br />
				<?php } ?>
				<table style="width: 100%">
					<tr>
						<td class="colhead" style="font-size: 12pt">&nbsp;专辑名称&nbsp;<?= $edit . $act ?></td>
					</tr>
					<tr>
						<td><p style="margin: 3px 3px 3px 3px; font-size: 9pt"><?= format_comment($descr['name']) ?></p></td>
					</tr>
				</table><br />
				<table style="width: 100%">
					<tr>
						<td class="colhead" style="font-size: 12pt">&nbsp;专辑简介</td>
					</tr>
					<tr>
						<td><p style="margin: 3px 3px 3px 3px; font-size: 9pt"><?= format_comment($descr['small_descr']) ?></p></td>
					</tr>
				</table>
			</div>
			</div>
			<?php
		} else {
			?>
			<hr>
			<div style="margin-top: 10px; width: 98%" align="center">
				<div style="width: 80%; float: left; padding-left: 5px">
					<p style="text-align: center">暂无种子</p>
				</div>
				<div style="width: 250px; float: right; margin-left: 5px; padding-right: 5px">
					<?php if ($descr['userid'] == $CURUSER['id'] || get_user_class() >= UC_MODERATOR) { ?>
						<table style="width: 100%">
							<tr>
								<td class="colhead" style="font-size: 12pt">&nbsp;添加种子</td>
							</tr>
							<tr>
							<form method="post">
								<td align="center" style="height: 30px">种子ID：<input type="text" name="torrent_id"><input type="hidden" name="albumid" value="<?= $cid ?>"><input type="hidden" name="visited" value="<?= $descr['visited'] ?>"><input type="submit" value="添加"></td>
							</form>
							</tr>
						</table><br />
					<?php } ?>
					<table style="width: 100%">
						<tr>
							<td class="colhead" style="font-size: 12pt">&nbsp;专辑名称&nbsp;<?= $edit . $act ?></td>
						</tr>
						<tr>
							<td><p style="margin: 3px 3px 3px 3px; font-size: 9pt"><?= format_comment($descr['name']) ?></p></td>
						</tr>
					</table><br />
					<table style="width: 100%">
						<tr>
							<td class="colhead" style="font-size: 12pt">&nbsp;专辑简介</td>
						</tr>
						<tr>
							<td><p style="margin: 3px 3px 3px 3px; font-size: 9pt"><?= format_comment($descr['small_descr']) ?></p></td>
						</tr>
					</table>
				</div>
			</div>
			<?php
		}
	} else {
		stdmsg("错误", "该专辑是非公开的");
		stdfoot();
		die;
	}
} elseif ($action != '' || $_GET['album'] == '') {
	$count = get_row_count("album");
	list($pagertop, $pagerbottom, $limit) = pager(25, $count, "?");
	if (get_user_class() >= UC_MODERATOR) {
		$res = sql_query("SELECT * FROM album ORDER BY id DESC $limit") or sqlerr(__FILE__, __LINE__);
	} else {
		$res = sql_query("SELECT * FROM album WHERE visited = 'yes' ORDER BY id DESC $limit") or sqlerr(__FILE__, __LINE__);
	}
	$HTMLOUT .= bjtable_album($res, "全部专辑");
	$HTMLOUT .="<br /><br />";
	if ($count) {
		print($pagertop);
		print $HTMLOUT;
		print($pagerbottom);
	} else {
		print $HTMLOUT;
	}
}

//批量删除专辑（管理员及以上可用）
if ($_POST['albumids'] != '' && get_user_class() >= UC_MODERATOR) {
	$albumids = implode(",", $_POST['albumids']);
	$res = sql_query("SELECT visited FROM album WHERE id IN ($albumids)") or sqlerr(__FILE__, __LINE__);
	sql_query("DELETE FROM album WHERE id IN ($albumids)") or sqlerr(__FILE__, __LINE__);
	while ($row = mysql_fetch_array($res)) {
		if ($row['visited'] == 'yes') {
			sql_query("UPDATE torrents SET albumid = '' WHERE albumid IN ($albumids)") or sqlerr(__FILE__, __LINE__);
		}
	}
	echo "<script>location.href='album.php';</script>";
}

//批量删除专辑内容
if ($_POST['id'] != '' && get_user_class() >= UC_MODERATOR || $_POST['userid'] != '') {
	$id = implode(",", $_POST['id']);
	$res = mysql_fetch_array(sql_query("SELECT visited FROM album WHERE id = $cid")) or sqlerr(__FILE__, __LINE__);
	sql_query("DELETE FROM albumseries WHERE albumid = $cid AND torrentid IN (" . $id . ")") or sqlerr(__FILE__, __LINE__);
	if ($res['visited'] == 'yes') {
		sql_query("UPDATE torrents SET albumid = '' WHERE id IN ($id)") or sqlerr(__FILE__, __LINE__);
	}
	echo "<script>location.href='?action=view&cid=$cid';</script>";
} elseif ($_POST['id'] != '') {
	echo "<script>alert('权限不足');</script>";
}

//搜索专辑
if ($_GET['album'] != '') {
	$count = get_row_count("album");
	list($pagertop, $pagerbottom, $limit) = pager(25, $count, "?");
	if (get_user_class() < UC_MODERATOR) {
		$res = sql_query("SELECT * FROM album WHERE name LIKE '%" . $_GET['album'] . "%' AND visited = 'yes' ORDER BY id DESC $limit");
	} else {
		$res = sql_query("SELECT * FROM album WHERE name LIKE '%" . $_GET['album'] . "%' ORDER BY id DESC $limit");
	}
	$HTMLOUT .= bjtable_album($res, "搜索专辑");
	$HTMLOUT .="<br /><br />";
	if ($count) {
		print($pagertop);
		print $HTMLOUT;
		print($pagerbottom);
	} else {
		print $HTMLOUT;
	}
}

//添加专辑内容
if ($_POST['albumid'] && $_POST['torrent_id'] != '') {
	$albumid = $_POST['albumid'];
	$torrentid = $_POST['torrent_id'];
	$res1 = sql_query("SELECT id FROM torrents WHERE id = $torrentid") or sqlerr(__FILE__, __LINE__);
	$res2 = sql_query("SELECT torrentid FROM albumseries WHERE albumid = $albumid AND torrentid = $torrentid") or sqlerr(__FILE__, __LINE__);
	$res3 = mysql_fetch_array(sql_query("SELECT albumid FROM torrents WHERE id = $torrentid")) or sqlerr(__FILE__, __LINE__);
	if (mysql_num_rows($res1) == 0) {
		echo "<script>alert('该ID的种子不存在');</script>";
		echo "<script>location.href='?action=view&cid=$cid';</script>";
	} elseif (mysql_numrows($res2) > 0) {
		echo "<script>alert('该ID的种子已经被添加进该专辑');</script>";
		echo "<script>location.href='?action=view&cid=$cid';</script>";
	} elseif ($_POST['visited'] == 'no') {
		sql_query("INSERT INTO albumseries (albumid, torrentid) VALUE ($albumid, $torrentid)") or sqlerr(__FILE__, __LINE__);
		echo "<script>location.href='?action=view&cid=$cid';</script>";
	} elseif (!empty($res3['albumid'])) {
		echo "<script>alert('该ID的种子已经被添加进其它专辑');</script>";
		echo "<script>location.href='?action=view&cid=$cid';</script>";
	} else {
		sql_query("INSERT INTO albumseries (albumid, torrentid) VALUE ($albumid, $torrentid)") or sqlerr(__FILE__, __LINE__);
		sql_query("UPDATE torrents SET albumid = $albumid WHERE id = $torrentid") or sqlerr(__FILE__, __LINE__);
		echo "<script>location.href='?action=view&cid=$cid';</script>";
	}
}
stdfoot();
