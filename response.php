<?php

foreach ($_POST as $k => $v) {
	$upload_data[$k] = $v;
}

$upload_data['file'] = '@' . $_FILES['file'][tmp_name];

$url = "http://up.tietuku.cn";
$r = Curl($url, $upload_data);
echo $r;

function Curl($url = '', $post_data = array()) {
	if ($url == '') {
		return NULL;
	}
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, false);
	//启用时会发送一个常规的POST请求，类型为：application/x-www-form-urlencoded，就像表单提交的一样。
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url);
	$info = curl_exec($ch);
	curl_close($ch);
	return ($info);
}
