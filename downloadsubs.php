<?php

require "include/bittorrent.php";
dbconn();
if (!$CURUSER) {
	Header("Location: " . get_protocol_prefix() . "$BASEURL/");
	die;
}

$filename = 0 + $_GET["subid"];
$dirname = 0 + $_GET["torrentid"];

if (!$filename || !$dirname) {
	die("File name missing\n");
}

$res = sql_query("SELECT * FROM subs WHERE id = $filename") or sqlerr(__FILE__, __LINE__);
$arr = mysql_fetch_assoc($res);
if (!$arr) {
	die("未找到相应字幕文件\n");
}

sql_query("UPDATE subs SET hits=hits+1 WHERE id = $filename") or sqlerr(__FILE__, __LINE__);
$file = "$SUBSPATH/$dirname/$filename.$arr[ext]";

if (!is_file($file))
	die("未找到相应字幕文件\n");
$f = fopen($file, "rb");
if (!$f)
	die("不能打开文件\n");
header("Content-Length: " . filesize($file));
header("Content-Type: application/octet-stream");
header("Pragma: public");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Transfer-Encoding: binary");

if (str_replace("Gecko", "", $_SERVER['HTTP_USER_AGENT']) != $_SERVER['HTTP_USER_AGENT']) {
	header("Content-Disposition: attachment; filename=\"$arr[filename]\" ; charset=utf-8");
} else if (str_replace("Firefox", "", $_SERVER['HTTP_USER_AGENT']) != $_SERVER['HTTP_USER_AGENT']) {
	header("Content-Disposition: attachment; filename=\"$arr[filename]\" ; charset=utf-8");
} else if (str_replace("Opera", "", $_SERVER['HTTP_USER_AGENT']) != $_SERVER['HTTP_USER_AGENT']) {
	header("Content-Disposition: attachment; filename=\"$arr[filename]\" ; charset=utf-8");
} else if (str_replace("IE", "", $_SERVER['HTTP_USER_AGENT']) != $_SERVER['HTTP_USER_AGENT']) {
	header("Content-Disposition: attachment; filename=" . str_replace("+", "%20", rawurlencode($arr[filename])));
} else {
	header("Content-Disposition: attachment; filename=" . str_replace("+", "%20", rawurlencode($arr[filename])));
}

do {
	$s = fread($f, 4096);
	print($s);
} while (!feof($f));
//closefile($f);
exit;
