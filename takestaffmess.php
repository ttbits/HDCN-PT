<?php

require "include/bittorrent.php";
if ($_SERVER["REQUEST_METHOD"] != "POST")
	stderr("错误", "权限不足");
dbconn();
loggedinorreturn();
checkserver();

if (get_user_class() < UC_ADMINISTRATOR)
	stderr("错误", "权限不足");

$sender_id = ($_POST['sender'] == 'system' ? 0 : (int) $CURUSER['id']);
$dt = sqlesc(date("Y-m-d H:i:s"));
$msg = trim($_POST['msg']);
if (!$msg)
	stderr("错误", "不要留空");
$updateset = $_POST['clases'];
if (is_array($updateset)) {
	foreach ($updateset as $class) {
		if (!is_valid_id($class) && $class != 0)
			stderr("错误", "不存在的用户组");
	}
}else {
	if (!is_valid_id($updateset) && $updateset != 0)
		stderr("错误", "不存在的用户组");
}
$subject = trim($_POST['subject']);
$query = sql_query("SELECT id FROM users WHERE class IN (" . implode(",", $updateset) . ")");
while ($dat = mysql_fetch_assoc($query)) {
	sql_query("INSERT INTO messages (sender, receiver, added,  subject, msg) VALUES ($sender_id, $dat[id], $dt, " . sqlesc($subject) . ", " . sqlesc($msg) . ")") or sqlerr(__FILE__, __LINE__);
}

header("Refresh: 0; url=staffmess.php?sent=1");
