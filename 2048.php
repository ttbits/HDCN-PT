<?php
require "include/bittorrent.php";
require 'memcache.php';
dbconn();
$time = date('Y-m-d', time());
$cssupdatedate = ($cssdate_tweak ? "?v=" . htmlspecialchars($cssdate_tweak) : "");
/*
  $res = mysql_fetch_array(sql_query("SELECT times FROM `2048` WHERE uid = " . $CURUSER['id'] . " AND username = '" . $CURUSER['username'] . "'"));
  if ($time == $res['times']) {
  stderr("错误", "What Are You 弄啥嘞？！你今天已经玩儿过了！！！");
  }
 * 允许一直玩儿，但是只有每天的第一次会获得魔力值奖励
 */
if ($memcache->get($CURUSER['username'] . '_2048_' . $CURUSER['id']) < $time || $memcache->get($CURUSER['username'] . '_2048_' . $CURUSER['id']) == '') {
	$memcache->set($CURUSER['username'] . '_2048_' . $CURUSER['id'], 1);
}
loggedinorreturn();
stdhead();
?>
<html>
	<head>
		<meta charset = "UTF-8"/>
		<title>2048</title>
		<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=yes"/>
		<link rel = "stylesheet" type = "text/css" href = "game/2048/css/2048.css<?= $cssupdatedate ?>">
		<script type = "text/javascript" src = "game/2048/js/main2048.js<?= $cssupdatedate ?>"></script>
		<script type = "text/javascript" src = "game/2048/js/showanimation2048.js<?= $cssupdatedate ?>"></script>
		<script type = "text/javascript" src = "game/2048/js/support2048.js<?= $cssupdatedate ?>"></script>
	</head>
	<body>
		<header>
			<!--<a href ="javascript:newgame()" id = "newgame">新游戏</a>-->
			<p>分数：<span id = "score">0</span>&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 9pt">每天的第一次游戏可以获得分数的1%作为魔力值奖励</span></p>
		</header>
		<div id = "grid-container">
			<div class = "grid-cell" id = "grid-cell-0-0"></div>
			<div class = "grid-cell" id = "grid-cell-0-1"></div>
			<div class = "grid-cell" id = "grid-cell-0-2"></div>
			<div class = "grid-cell" id = "grid-cell-0-3"></div>

			<div class = "grid-cell" id = "grid-cell-1-0"></div>
			<div class = "grid-cell" id = "grid-cell-1-1"></div>
			<div class = "grid-cell" id = "grid-cell-1-2"></div>
			<div class = "grid-cell" id = "grid-cell-1-3"></div>

			<div class = "grid-cell" id = "grid-cell-2-0"></div>
			<div class = "grid-cell" id = "grid-cell-2-1"></div>
			<div class = "grid-cell" id = "grid-cell-2-2"></div>
			<div class = "grid-cell" id = "grid-cell-2-3"></div>

			<div class = "grid-cell" id = "grid-cell-3-0"></div>
			<div class = "grid-cell" id = "grid-cell-3-1"></div>
			<div class = "grid-cell" id = "grid-cell-3-2"></div>
			<div class = "grid-cell" id = "grid-cell-3-3"></div>
			<!--<div class = "dialog-success">
				<div class = "success">卧槽？！你赢了！</div>
				<div><a href ="javascript:again()" id = "newgame">重新开始</a></div>
				<div><a href ="javascript:conti()" id = "continue">继续挑战</a></div>
			</div>-->
			<div class = "dialog-fail">
				<!--<div class = "fail">哈哈！！你输了！</div>-->
				<div class = "fail">啊哦！！没有能移动的方块了！</div>
				<div><a href ="javascript:again()" id = "newgame">重新开始</a></div>
			</div>
		</div>
		<a href="2048_rank.php" target="_blank"><b style="font-size: 11pt">智商排行榜</b></a>
		<ul style="width: 550px">
			<li style="font-size: 9pt; text-align: left">操作方法：使用键盘的方向键控制方块走向</li>
			<li style="font-size: 9pt; text-align: left">玩法说明：2个相同数字的方块合并成一个高级的方块，同时数字是合成方块数字的2倍</li>
			<li style="font-size: 9pt; text-align: left">温馨提示：当你每小时能获取超过80个魔力值时则可以获得分数的<b style="color: red">10%</b>作为魔力值奖励</li>
			<!--<li style="font-size: 9pt; text-align: left">输赢判定：没有可以合并的方块时，棋盘内有 '2048' 字样的方块为赢，反之为输</li>-->
		</ul>
	</body>
</html>
<?= stdfoot(); ?>