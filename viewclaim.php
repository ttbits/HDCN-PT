<?php
require "include/bittorrent.php";
require_once("memcache.php");
dbconn();
loggedinorreturn();
secrity();
if ($showextinfo['imdb'] == 'yes') {
	require_once ("imdb/imdb.class.php");
	require_once("imdb/douban.php");
}
stdhead("认领种子管理");
?>
<script type="text/javascript">
	function checkAll()
	{
		$(".checkbox").each(function () {
			this.checked = true;
		});
	}

	function reverseCheck()
	{
		$(".checkbox").each(function () {
			this.checked = !this.checked;
		});
	}
</script>
<div>
	<table border="1" cellpadding="10" cellspacing="0" width="95%"><tbody><tr><td>
					<h1 style="width: 95%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;认领规则</h1>
					<ul>
						<li>1. 所有会员，无论等级，均能认领种子。</li>
						<li>2. 每位会员最多可认领<?= $claimnum ?>个种子。</li>
						<li>3. 每个种子最多允许<?= $claimmaxnum ?>名会员认领。</li>
						<li>4. 要求：每一个认领的种子月做种时间<?= $claimtime ?>小时及以上或者月上传量是种子大小的<?= $claimmul ?>倍及以上。</li>
						<li>5. 系统在每个月1日凌晨统计会员上个月每一个认领种子的做种时间和上传量，结算后并将数据清零。</li>
						<li>6. 做种时间或者上传量到达要求，并且种子的大小超过200M，按照奖励标准发放魔力值。</li>
						<li>7. 没有达到要求，分两种情况：<br />
							1) 认领时间超过一个月（30天），自动弃领该种子，名额空出，可由其他会员继续认领。<br />
							2) 认领时间不到一个月（30天），保留认领资格。</li>
						<li>8. 允许会员放弃认领的种子，每个每次需支付<?= $claimbonus ?>个魔力值（注：被系统自动取消时不会扣魔力值）。</li>
						<li>9. 各数据统计均以点击认领且成功认领时开始计算。</li>
					</ul>
					<h1 style="width: 95%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;奖励标准</h1>
					<ul>
						<li>1. 种子分为以下三种类别：<br />
							1) 【小】所有200M以上，10G以下的。<br />
							2) 【中】所有10G以上，30G以下的。<br />
							3) 【大】所有30G以上。
						</li>
						<li>
							2. 奖励基数以【小】为标准(按实际上传量计算)：<br />
							1) 【小】，每月 <b>200个 + 每G上传量 x 15个</b> 魔力值。<br />
							2) 【中】，每月 <b>400个 + 每G上传量 x 20个</b> 魔力值。<br />
							3) 【大】，每月 <b>600个 + 每G上传量 x 30个</b> 魔力值。
						</li>
						<li>注意：未达标的认领种子只能得到每G上传量奖励，无基础奖励魔力值。</li>
					</ul>
				</td></tr></tbody></table>
</div>
<?php
$count = get_row_count("claim", "WHERE userid = " . $CURUSER['id']);
list($pagertop, $pagerbottom, $limit) = pager(25, $count, "?");
$res = sql_query("SELECT * FROM torrents WHERE id IN (SELECT torrentid FROM claim WHERE userid =" . $CURUSER['id'] . ") ORDER BY id DESC $limit") or sqlerr(__FILE__, __LINE__);
if ($_POST['id'] != '') {
	$id = implode(",", $_POST['id']);
	$count = count($id);
	sql_query("DELETE FROM claim WHERE torrentid IN (" . $id . ")") or sqlerr(__FILE__, __LINE__);
	sql_query("UPDATE users SET seedbonus = seedbonus - $count * $claimbonus WHERE id = " . $CURUSER['id']) or sqlerr(__FILE__, __LINE__);
	writeBonusComment($CURUSER['id'], "因弃领种子共花费 " . ($count * $claimbonus) . " 个魔力值");
	echo "<script>location.href='viewclaim.php';</script>";
}
print("<h1 style=\"width: 95%\">认领种子管理</h1>");
if (mysql_num_rows($res) > 0) {
	if ($count) {
		print($pagertop);
		print("<form method=\"POST\" style=\"width:95%\" onsubmit=\"if(confirm('确定要弃领吗？')){return true;}else{return false;}\">");
		trucktorrenttable($res, "torrents");
		print("</form>");
		print($pagerbottom);
	} else {
		print("<form method=\"POST\" style=\"width:95%\" onsubmit=\"if(confirm('确定要弃领吗？')){return true;}else{return false;}\">");
		trucktorrenttable($res, "torrents");
		print("</form>");
	}
}
stdfoot();
