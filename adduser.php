<?php
require "include/bittorrent.php";
dbconn();
loggedinorreturn();
require_once(get_langfile_path());
if (get_user_class() < UC_ADMINISTRATOR)
	stderr("错误", "权限不足");
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if ($_POST["username"] == "" || $_POST["password"] == "" || $_POST["email"] == "") {
		stderr("错误", "请填写全部项目");
	}
	if ($_POST["password"] != $_POST["password2"]) {
		stderr("错误", "两次输入的密码不一致");
	}
	$email = safe_email(htmlspecialchars(trim($_POST["email"])));
	if (!check_email($email)) {
		stderr("错误", "无效的邮箱地址");
	}
	$username = $_POST["username"];
	if (!validusername($username)) {
		stderr("错误", "无效的用户名");
	}
	$usernamearr = mysql_fetch_row(sql_query("SELECT id FROM users WHERE username = " . sqlesc($username) . ""));
	if ($usernamearr) {
		stderr("错误", "用户名已存在");
	}
	$password = $_POST["password"];
	$emailarr = mysql_fetch_row(sql_query("SELECT id FROM users WHERE email = " . sqlesc($_POST["email"]) . ""));
	if ($emailarr) {
		stderr("错误", "邮箱正在被使用");
	}
	$secret = mksecret();
	$passhash = sqlesc(md5($secret . $password . $secret));
	sql_query("INSERT INTO users (added, last_access, secret, username, passhash, status, stylesheet, class, email) VALUES (NOW(), NOW(), " . sqlesc($secret) . ", " . sqlesc($username) . ", $passhash, 'confirmed', " . $defcss . ", " . $defaultclass_class . ", " . sqlesc($_POST["email"]) . ")") or sqlerr(__FILE__, __LINE__);
	$arr = mysql_fetch_row(sql_query("SELECT id FROM users WHERE username = " . sqlesc($username) . ""));
	if (!$arr)
		stderr("错误", "无法创建帐号，用户名可能已经被使用");
	header("Location: " . get_protocol_prefix() . "$BASEURL/userdetails.php?id=" . htmlspecialchars($arr[0]));
	die;
}
stdhead("新建用户");
?>
<h1><?php echo $lang_adduser['head_adduser'] ?></h1>
<form method=post action=adduser.php>
	<table border=1 cellspacing=0 cellpadding=5>
		<tr><td class=rowhead><?php echo $lang_adduser['text_username'] ?></td><td><input type=text name=username size=40></td></tr>
		<tr><td class=rowhead><?php echo $lang_adduser['text_passwd'] ?></td><td><input type=password name=password size=40></td></tr>
		<tr><td class=rowhead><?php echo $lang_adduser['text_repasswd'] ?></td><td><input type=password name=password2 size=40></td></tr>
		<tr><td class=rowhead><?php echo $lang_adduser['text_email'] ?></td><td><input type=text name=email size=40></td></tr>
		<tr><td colspan=2 align=center><input type=submit value="<?php echo $lang_adduser['submit_add_user'] ?>" class=btn></td></tr>
	</table>
</form>
<?php
stdfoot();
