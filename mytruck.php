<?php
require "include/bittorrent.php";
require_once("memcache.php");
dbconn();
loggedinorreturn();
secrity();
if ($showextinfo['imdb'] == 'yes') {
	require_once ("imdb/imdb.class.php");
	require_once("imdb/douban.php");
}
stdhead("我的小货车");
$row = mysql_fetch_array(sql_query("SELECT passkey FROM users WHERE id= " . $CURUSER['id']));
//$url = "pushrss.php?pushkey=" . encrypt("dl,$row[passkey]");//字符串加密传输
$url = "pushrss.php?pushkey=" . encrypt(json_encode(array("linktype" => "dl", "passkey" => "" . $row['passkey'] . "")));
?>
<script type="text/javascript">
	function checkAll()
	{
		$(".checkbox").each(function () {
			this.checked = true;
		});
	}

	function reverseCheck()
	{
		$(".checkbox").each(function () {
			this.checked = !this.checked;
		});
	}
</script>
<div>
	<textarea style="height:50px;width:65%;overflow:hidden;" onmouseover="this.select()"><?php echo get_protocol_prefix() . $BASEURL . "/" . $url ?></textarea>
	<h1 style="width: 95%">小货车是什么？</h1>
	<table border="1" cellpadding="10" cellspacing="0" width="95%"><tbody><tr><td>
					<ul>
						<li>下载小货车是一个利用RSS实现的自动推送系统。</li>
						<li>首先把上面的RSS链接加入你的下载客户端，建立RSS订阅并打开<b>自动下载</b>选项，之后无论你在什么地方，当你在种子浏览页面把一个种子加入你的小货车，你的下载客户端就会自动收到这个种子的消息并进行下载。</li>
						<li>注意，该RSS订阅是利用你的PassKey生成的随机加密码，因此请不要透露给其他人使用，如果怀疑该链接泄漏，请立即更换PassKey，此RSS订阅会重新生成。</li>
						<li>虽然每次的链接都不相同，但是我们保证链接所包含的信息是一致的，因此还是要注意<b><font style="color: #FF0000">不要泄漏此链接</font></b>。</li>
						<li>在种子列表页面，字幕、收藏夹等小图标的后面，又多了一个的图标<img title="小货车" alt="小货车" src="styles/img/truck.png" />，点击这个图标你就可以把种子加入小货车，或者从小货车中删除种子。</li>
						<li>加入了小货车的种子，在你的种子浏览页面会拥有与其他种子不同的背景颜色，因此你可以方便的识别出来哪些种子是已经加入了小货车的，而哪些种子没有。</li>
						<li>你可以任意把种子加入小货车，或者从小货车中删除不需要的种子，但请注意，如果你的下载客户端已经开始下载该种子，你可能还需要从下载客户端中手动删除该种子。</li>
						<li>你可以在这个页面对你所有的小货车货物进行查看和管理。</li>
						<li>小货车系统会自动清理加入小货车10天以上的种子。如果你确实需要，你可以在10天以后重新把种子加入小货车。</li>
						<li>备注：程序自动刷新RSS Tracker的默认时间为5分钟，请稍安勿躁~<br />经测试，群辉的原生DS不允许添加空RSS，也就是说推送RSS内必须有至少一个内容。</li>
					</ul>
				</td></tr></tbody></table>
</div><br />
<?php
$count = get_row_count("truckmarks", "WHERE userid = " . $CURUSER['id']);
list($pagertop, $pagerbottom, $limit) = pager(25, $count, "?");
$res = sql_query("SELECT * FROM torrents WHERE id IN (SELECT torrentid FROM truckmarks WHERE userid =" . $CURUSER['id'] . ") ORDER BY id DESC $limit") or sqlerr(__FILE__, __LINE__);
if ($_POST['id'] != '') {
	$id = implode(",", $_POST['id']);
	sql_query("DELETE FROM truckmarks WHERE torrentid IN (" . $id . ")") or sqlerr(__FILE__, __LINE__);
	echo "<script>location.href='mytruck.php';</script>";
}
if (mysql_num_rows($res) > 0) {
	if ($count) {
		print($pagertop);
		print("<form method=\"POST\" style=\"width:95%\" onsubmit=\"if(confirm('确定要从小货车中移出吗？')){return true;}else{return false;}\">");
		trucktorrenttable($res, "torrents");
		print("</form>");
		print($pagerbottom);
	} else {
		print("<form method=\"POST\" style=\"width:95%\" onsubmit=\"if(confirm('确定要从小货车中移出吗？')){return true;}else{return false;}\">");
		trucktorrenttable($res, "torrents");
		print("</form>");
	}
}
stdfoot();
