<?php
require "include/bittorrent.php";
dbconn();
require_once(get_langfile_path("takeinvite.php", "", ""));
loggedinorreturn();
parked();

if ($CURUSER ['class'] < UC_MODERATOR)
	stderr("错误", "您没有权限");

function curl_file_get_contents($url) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_USERAGENT, _USERAGENT_);
	curl_setopt($ch, CURLOPT_REFERER, _REFERER_);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	$r = curl_exec($ch);
	curl_close($ch);
	return $r;
}

function getEmail($str) {
	$emailArr = '';
	//$pattern = "/([a-zA-Z0-9]+[a-zA-Z0-9\-_\.]+[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9\-_\.]+[a-zA-Z0-9]+)/";
	$pattern = "/[A-Za-z0-9][A-Za-z0-9_.+\-]*@[A-Za-z0-9][A-Za-z0-9_+\-]*(\.[A-Za-z0-9][A-Za-z0-9_+\-]*)+/";
	preg_match_all($pattern, $str, $emailArr);
	return array_unique($emailArr[0]);
}

function check_url($url) {
	if (preg_match('/^((https?):\/\/)?([a-z]([a-z0-9\-]*[\.。])+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel)|(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))(\/[a-z0-9_\-\.~]+)*(\/([a-z0-9_\-\.]*)(\?[a-z0-9+_\-\.%=&]*)?)?(#[a-z][a-z0-9_]*)?[#]*$/', $url))
		return true;
	else
		return false;
}

if ($_POST['submit']) {
	if (!check_url($_POST['url'])) {
		echo "<script>alert('输入的网址不正确');</script>";
	} else {
		$url = $_POST['url'];
	}
	$email = implode("\n", getEmail(curl_file_get_contents($url)));
}
stdhead("批量发送邀请");
if ($_POST['emailsubmit']) {
	$row = explode("\n", $_POST['email']);
	foreach ($row as $arr) {
		$send = unesc(htmlspecialchars(trim($arr)));
		invite($send);
	}
	echo "<script>location.href='autoinvite.php';</script>";
}
?>
<div>
	<h2 style="width: 95%">输入发邀地址链接</h2><br />
	<form method="POST">
		<?php echo "链接地址：" ?><input type="text" name="url" onmouseover="this.select()" style ="width: 600px" /><input type="submit" name="submit" value="提取邮箱" /><br /><br />
	</form>
	<h2 style="width: 95%">邮箱列表</h2><br />
	<?php echo "一行一个，可手动输入" ?>
	<form method="POST">
		<textarea name="email" id="email" style="height:200px;width:20%;"><?php echo $email ?></textarea><br />
		<input type="submit" name="emailsubmit" value="发送邀请" /><br /><br />
	</form>
</div>
<?php
stdfoot();
