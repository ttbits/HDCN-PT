<?php

$lang_takesignup_target = array
	(
	'en' => array
		(
		'msg_invited_user_has_registered' => "Invited user has registered",
		'msg_user_you_invited' => "The user you invited ",
		'msg_has_registered' => " has registered just now.",
	),
	'chs' => array
		(
		'msg_invited_user_has_registered' => "你邀请的用户已注册",
		'msg_user_you_invited' => "你邀请的用户 ",
		'msg_has_registered' => " 刚刚已注册。",
	),
);
