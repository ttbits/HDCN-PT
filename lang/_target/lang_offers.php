<?php

$lang_offers_target = array
	(
	'en' => array
		(
		'msg_has_allowed' => " has allowed you to upload ",
		'msg_find_offer_option' => "You will find a new option on the upload page.",
		'msg_your_offer_allowed' => "Your Offer has been allowed",
		'msg_offer_voted_on' => "Your Offer has been voted on. you are allowed to upload ",
		'msg_offer_voted_off' => "Your Offer has been voted off. You are not allowed to upload ",
		'msg_offer_deleted' => "Your offer will be deleted.",
		'msg_your_offer' => "Your offer ",
		'msg_voted_on' => " has been voted on",
		'msg_offer_deleted' => "Your offer was deleted",
		'msg_your_offer' => "Your offer '",
		'msg_was_deleted_by' => "' was deleted by ",
		'msg_blank' => ".",
		'msg_you_must_upload_in' => "Please upload the offer within ",
		'msg_hours_otherwise' => " hours. Otherwise the offer would be deleted.",
		'msg_reason_is' => "The reason: ",
	),
	'chs' => array
		(
		'msg_has_allowed' => "允许你上传 ",
		'msg_find_offer_option' => "你会在上传区找到新的选项。",
		'msg_your_offer_allowed' => "你的候选被允许",
		'msg_offer_voted_on' => "你的候选被投票允许。你可以上传 ",
		'msg_offer_voted_off' => "你的候选被投票拒绝。你不可以上传 ",
		'msg_offer_deleted' => "你的候选被删除",
		'msg_your_offer' => "你的候选",
		'msg_voted_on' => "被投票允许",
		'msg_offer_deleted' => "候选被删除",
		'msg_your_offer' => "你的候选'",
		'msg_was_deleted_by' => "'被管理员",
		'msg_blank' => "删除。",
		'msg_you_must_upload_in' => "请在",
		'msg_hours_otherwise' => "小时内上传通过候选的内容。否则该候选将被删除。",
		'msg_reason_is' => "原因：",
	),
);
