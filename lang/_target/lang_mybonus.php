<?php

$lang_mybonus_target = array
	(
	'en' => array
		(
		'msg_someone_loves_you' => "Someone Loves you",
		'msg_you_have_been_given' => "You have been given a gift of ",
		'msg_after_tax' => " (after tax it's ",
		'msg_karma_points_by' => ") Karma points by ",
		'msg_personal_message_from' => "Personal message from ",
		'msg_colon' => ": ",
	),
	'chs' => array
		(
		'msg_someone_loves_you' => "收到礼物",
		'msg_you_have_been_given' => "你收到",
		'msg_after_tax' => "（扣取手续费后为",
		'msg_karma_points_by' => "）个魔力值的礼物。祝福来自",
		'msg_personal_message_from' => "",
		'msg_colon' => "说：",
	),
);
