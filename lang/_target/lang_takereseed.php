<?php

$lang_takereseed_target = array
	(
	'en' => array
		(
		'msg_reseed_request' => "Reseed Request",
		'msg_speed_request' => "Speed Request",
		'msg_reseed_user' => "User ",
		'msg_ask_reseed' => " asked for a reseed on torrent ",
		'msg_ask_speed' => " asked for a speed on torrent ",
		'msg_thank_you' => " !\nThank You!",
	),
	'chs' => array
		(
		'msg_reseed_request' => "续种请求",
		'msg_speed_request' => "加速请求",
		'msg_reseed_user' => "用户",
		'msg_ask_reseed' => "请求你续种该种子 - ",
		'msg_ask_speed' => "请求你加速该种子 - ",
		'msg_thank_you' => " ！谢谢你！",
	),
);
