<?php

$lang_fastdelete_target = array
	(
	'en' => array
		(
		'msg_torrent_deleted' => "Your torrent was deleted",
		'msg_the_torrent_you_uploaded' => "The torrent you uploaded '",
		'msg_was_deleted_by' => "' was delete by ",
		'msg_blank' => ".",
	),
	'chs' => array
		(
		'msg_torrent_deleted' => "种子被删除",
		'msg_the_torrent_you_uploaded' => "你上传的种子'",
		'msg_was_deleted_by' => "'被管理员",
		'msg_blank' => "删除。",
	),
);
