<?php

$lang_subtitles_target = array
	(
	'en' => array
		(
		'msg_deleted_your_sub' => " deleted the subtitle that you uploaded. ",
		'msg_your_sub_deleted' => "Your subtitle was deleted",
		'msg_reason_is' => "The reason: ",
	),
	'chs' => array
		(
		'msg_deleted_your_sub' => "删除了你上传的字幕。",
		'msg_your_sub_deleted' => "字幕被删除",
		'msg_reason_is' => "原因：",
	),
);
