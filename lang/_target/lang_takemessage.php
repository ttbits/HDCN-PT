<?php

$lang_takemessage_target = array
	(
	'en' => array
		(
		'mail_dear' => "Dear ",
		'mail_you_received_a_pm' => "You have received a PM.",
		'mail_sender' => "Sender	",
		'mail_subject' => "Subject	",
		'mail_date' => "Date		",
		'mail_use_following_url' => "You can click&nbsp;",
		'mail_use_following_url_1' => "&nbsp;to view the message (you may have to login).",
		'mail_yours' => "<br />Yours,",
		'mail_the_site_team' => "The " . $SITENAME . " Team.",
		'mail_received_pm_from' => "You have received a PM from ",
		'mail_here' => "HERE",
		'msg_system' => "System",
		'msg_original_message_from' => "Original Message from ",
	),
	'chs' => array
		(
		'mail_dear' => "尊敬的",
		'mail_you_received_a_pm' => "你收到了一条新消息。",
		'mail_sender' => "发送者	",
		'mail_subject' => "主题	",
		'mail_date' => "日期		",
		'mail_use_following_url' => "你可以点击",
		'mail_use_following_url_1' => "来查看该消息（你可能需要登录）",
		'mail_yours' => "",
		'mail_the_site_team' => $SITENAME . "网站",
		'mail_received_pm_from' => "你有新消息，来自",
		'mail_here' => "这里",
		'msg_system' => "系统",
		'msg_original_message_from' => "原消息来自",
	),
);
