<?php

$lang_comment_target = array
	(
	'en' => array(
		'msg_new_comment' => "New comment",
		'msg_torrent_receive_comment' => "You have received a comment on your torrent ",
		'msg_offer_receive_comment' => "You have received a comment on your offer ",
		'msg_request_receive_comment' => "You have received a comment on your request "
	),
	'chs' => array(
		'msg_new_comment' => "新评论",
		'msg_torrent_receive_comment' => "你发布的种子收到了新评论 ",
		'msg_offer_receive_comment' => "你发布的候选收到了新评论 ",
		'msg_request_receive_comment' => "你发布的求种收到了新评论 "
	),
);
