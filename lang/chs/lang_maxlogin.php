<?php

$lang_maxlogin = array
	(
	'head_failed' => "失败登录尝试记录",
	'text_id' => "序号",
	'text_ip' => "IP地址",
	'text_searchip' => "搜索IP地址：",
	'text_action_time' => "登录时间",
	'text_attempts_time' => "尝试次数",
	'text_attempts_type' => "尝试类型",
	'text_status' => "状态",
	'text_notice' => "确定删除此记录吗？",
	'text_nothing' => "没有找到相关记录！",
	'type_recover' => "<font color=orange>恢复密码</font>",
	'type_login' => "<font color=brown>账号登录</font>",
	'status_banned' => "被禁止",
	'status_notbanned' => "未禁止",
	'action_unban' => "解禁",
	'action_ban' => "禁止",
	'action_delete' => "删除",
	'action_edit' => "编辑",
	'submit_save' => "保存",
);
