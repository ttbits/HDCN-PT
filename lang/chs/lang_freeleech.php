<?php

$lang_freeleech = array
	(
	'head_freeleech' => "请选择要进行的操作",
	'text_free' => "设置所有种子为<font color=#000099>Free</font>：",
	'text_2xup' => "设置所有种子为<font color=#009900>2x</font>：",
	'text_2xup_free' => "设置所有种子为<font color=#009999>2xFree</font>：",
	'text_halfdown' => "设置所有种子为<font color=#990000>50%</font>：",
	'text_2xup_halfdown' => "设置所有种子为<font color=#999900>2x50%</font>：",
	'text_30down' => "设置所有种子为<font color=#8B4789>30%</font>：",
	'text_normal' => "设置所有种子为正常状态：",
	'text_go' => "GO",
);
