<?php

$lang_takeflush = array
	(
	'std_failed' => "失败",
	'std_success' => "成功",
	//'std_ghost_torrents_cleaned' => "个种子被成功清除。等待自动更新或执行手动更新Tracker以显示种子状态。",
	'std_ghost_torrents_cleaned' => "个冗余种子被成功清除",
	'std_cannot_flush_others' => "你只能清除自己的种子"
);
