<?php

$lang_uploaders = array
	(
	'text_no_uploaders_yet' => "没有找到发布转载组成员！",
	'head_uploaders' => "发布转载组",
	'text_uploaders' => "发布转载组",
	'col_username' => "用户名",
	'col_torrents_size' => "种子大小",
	'col_torrents_num' => "种子数量",
	'col_last_upload_time' => "最近发布时间",
	'col_last_upload' => "最近发布种子",
	'text_not_available' => "无",
	'submit_go' => "查找",
	'text_select_month' => "选择月份：",
	'text_order_by' => "排序",
	'text_username' => "用户名",
	'text_torrent_size' => "种子大小",
	'text_torrent_num' => "种子数量",
);
