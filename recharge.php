<?php
require_once("include/bittorrent.php");
dbconn();
loggedinorreturn();
parked();
if ($recharge == 'no') {
	if (get_user_class() < UC_MODERATOR) {
		$msg = "访问关闭中的魔力充值页面：Username: " . $CURUSER["username"] . " - UserID: " . $CURUSER["id"] . " - UserIP : " . getip();
		write_log($msg, 'mod');
		stderr("抱歉", "当前魔力值充值系统已经关闭。");
	}
}
stdhead("魔力值充值");
?>
<div>
	<form method="POST">
		<?php echo "请输入充值卡密：" ?><input type="text" name="card" style="width: 250px" value="" />
		<input type="submit" name="submit" value="充值" />
	</form>
</div>
<?php
if ($_POST['submit']) {
	$kami = $_POST['card'];
	$now = date("Y-m-d H:i:s", time());
	$res1 = sql_query("SELECT * FROM recharge WHERE cards = '$kami'") or sqlerr(__FILE__, __LINE__);
	$res2 = sql_query("SELECT * FROM rechargelog WHERE cards = '$kami' AND userid = $CURUSER[id]") or sqlerr(__FILE__, __LINE__);
	if (mysql_num_rows($res2) > 0) {
		echo "<script>alert('您已经使用过该充值卡密！');location.href = 'recharge.php';</script>";
		die();
	}
	if (mysql_num_rows($res1) > 0) {//匹配卡密
		while ($a = mysql_fetch_assoc($res1)) {
			if ($a['users'] == 0) {
				if ($a['num'] == '-1' || $a['num'] > 0) {//如果卡密使用次数等于-1或大于0
					if ($a['date'] == '0000-00-00' || time() <= strtotime("" . $a['date'] . " + 1 day")) {//如果卡密过期时间+1后等于空或当前时间小于等于过期时间
						sql_query("UPDATE users SET seedbonus = seedbonus + $a[bonus] WHERE id = $CURUSER[id]") or sqlerr(__FILE__, __LINE__); //更新魔力值
						sql_query("UPDATE recharge SET num = num - 1 WHERE num > 0 AND id = $a[id]") or sqlerr(__FILE__, __LINE__); //充值卡密使用次数-1
						sql_query("INSERT INTO rechargelog (userid, cards, time) VALUE ('$CURUSER[id]', '$a[cards]', '$now')") or sqlerr(__FILE__, __LINE__); //插入充值记录到日志表
						writeBonusComment($CURUSER['id'], "因充值卡密获得 " . round($a['bonus'], 3) . " 个魔力值"); //更新用户魔力值变更记录
						echo "<script>alert('成功充值" . round($a['bonus'], 3) . "个魔力值！');location.href = 'recharge.php';</script>";
					} else {
						echo "<script>alert('充值卡密已经过期！');location.href = 'recharge.php';</script>";
					}
				} else {
					echo "<script>alert('充值卡密的充值次数不足！');location.href = 'recharge.php';</script>";
				}
			} elseif ($a['users'] == $CURUSER['id']) {
				if ($a['date'] == '0000-00-00' || time() <= strtotime("" . $a['date'] . " + 1 day")) {//如果卡密过期时间+1后等于空或当前时间小于等于过期时间
					sql_query("UPDATE users SET seedbonus = seedbonus + $a[bonus] WHERE id = $CURUSER[id]") or sqlerr(__FILE__, __LINE__); //更新魔力值
					sql_query("UPDATE recharge SET num = num - 1 WHERE num > 0 AND id = $a[id]") or sqlerr(__FILE__, __LINE__); //充值卡密使用次数-1
					sql_query("INSERT INTO rechargelog (userid, cards, time) VALUE ('$CURUSER[id]', '$a[cards]', '$now')") or sqlerr(__FILE__, __LINE__); //插入充值记录到日志表
					writeBonusComment($CURUSER['id'], "因充值卡密获得 " . round($a['bonus'], 3) . " 个魔力值"); //更新用户魔力值变更记录
					echo "<script>alert('成功充值" . round($a['bonus'], 3) . "个魔力值！');location.href = 'recharge.php';</script>";
				} else {
					echo "<script>alert('充值卡密已经过期！');location.href = 'recharge.php';</script>";
				}
			} else {
				echo "<script>alert('此充值卡是定向卡，只有指定用户可以使用！');location.href = 'recharge.php';</script>";
			}
		}
	} else {
		echo "<script>alert('无效的充值卡密！');location.href = 'recharge.php';</script>";
	}
}

function begin_table_recharge($fullwidth = false, $padding = 5) {
	$width = "";
	if ($fullwidth)
		$width .= " width=50%";
	return("<table class='main" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'>");
}

function end_table_recharge() {
	return("</table>");
}

function begin_frame_recharge($caption = "", $center = false, $padding = 5, $width = "100%", $caption_center = "left") {
	$tdextra = "";
	if ($center)
		$tdextra .= " align='center'";
	return(($caption ? "<h2 align='" . $caption_center . "'>" . $caption . "</h2>" : "") . "<table width='" . $width . "' border='1' cellspacing='0' cellpadding='" . $padding . "'>" . "<tr><td class='text' $tdextra>");
}

function end_frame_recharge() {
	return("</td></tr></table>");
}

function bjtable_recharge($res, $frame_caption) {
	$htmlout = '';
	$htmlout .= begin_frame_recharge($frame_caption, true);
	$htmlout .= begin_table_recharge();
	$htmlout .="<tr><td class='colhead'>序号</td><td class='colhead' align='left'>充值卡密</td><td class='colhead' align='right'>可使用次数</td><td class='colhead' align='right'>已使用次数</td><td class='colhead' align='right'>充值卡面值</td><td class='colhead' align='right'>过期时间</td><td class='colhead' align='right'>使用时间</td></tr>";
	$num = 0;
	while ($a = mysql_fetch_assoc($res)) {
		++$num;
		$number = get_row_count("rechargelog", "WHERE cards = '" . $a['cards'] . "'");
		$htmlout .="<tr class='torrent_table'><td>$num</td><td align='left'>" .
				"<b>" . $a['cards'] . "</b></td>" .
				"<td align='right'>" . ($a['num'] == '-1' ? '∞' : $a['num']) . "</td>" .
				"<td align='right'>$number</td>" .
				"<td align='right'>" . round($a['bonus'], 3) . "</td>" .
				"<td align='right'>" . $a['date'] . "</td>" .
				"<td align='right'>" . $a['time'] . "</td>" .
				"</tr>";
	}
	$htmlout .= end_table_recharge();
	$htmlout .= end_frame_recharge();
	return $htmlout;
}

$HTMLOUT .="<h1 align='center'>充值记录统计</h1>";
$res = sql_query("SELECT * FROM recharge INNER JOIN rechargelog ON recharge.cards = rechargelog.cards WHERE rechargelog.userid = " . $CURUSER['id'] . " ORDER BY recharge.id DESC") or sqlerr(__FILE__, __LINE__); //降序排列，最新的最先显示
$HTMLOUT .= bjtable_recharge($res, "我的充值记录", "Users");
$HTMLOUT .="<br /><br />";
print $HTMLOUT;
stdfoot();
